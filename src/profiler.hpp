#pragma once

#include "gpu/core.hpp"
#include <string>
#include <vector>

namespace framework
{
	class command_buffer_manager;

	struct profiler_create_info
	{
		::framework::gpu::device * device;
		::framework::gpu::queue * queue;
		::framework::gpu::physical_device_features const * physical_device_features;
		::framework::gpu::physical_device_properties const * physical_device_properties;
		::framework::gpu::physical_device_memory_properties const * physical_device_memory_properties;
		::framework::command_buffer_manager * command_buffer_manager;
		::std::uint32_t max_statistic_count;
		::std::uint32_t readback_buffer_initial_queue_family_index;
		::std::uint32_t frame_in_flight_count;
	};

	class profiler
	{
	public:
		struct statistics
		{
			::std::string label;
			::std::uint32_t depth;
			::std::uint32_t delta_timestamp;
		};

		profiler(::framework::profiler_create_info const & create_info);

		~profiler();

		void reset(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t frame_in_flight_index);

		::std::uint32_t begin_timestamp(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags pipeline_stage, ::std::string const & label);

		void end_timestamp(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags pipeline_stage, ::std::uint32_t timestamp_index);

		void copy_query_pool_results(::framework::gpu::command_buffer * command_buffer);

		void get_query_pool_results(::std::uint32_t frame_in_flight_index);

		void increment_current_depth() noexcept;

		void decrement_current_depth() noexcept;

		::std::vector<struct ::framework::profiler::statistics> const & get_statistics(::std::uint32_t frame_in_flight_index) const noexcept;

		bool get_query_pool_host_commands() const noexcept;

		struct scoped_timestamp
		{
			scoped_timestamp() = delete;

			scoped_timestamp(::framework::profiler & profiler, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags begin_timestamp_pipeline_stage, ::framework::gpu::pipeline_stage_flags end_timestamp_pipeline_stage, ::std::string const & label);

			~scoped_timestamp();

			::std::uint32_t get_statistics_index() const noexcept;

			::framework::profiler & profiler;
			::framework::gpu::command_buffer * const command_buffer;
			::framework::gpu::pipeline_stage_flags const end_timestamp_pipeline_stage;
			::std::uint32_t const timestamp_index;
		};
		
		struct manual_timestamp
		{
			manual_timestamp() = delete;

			manual_timestamp(::framework::profiler & profiler, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags begin_timestamp_pipeline_stage, ::framework::gpu::pipeline_stage_flags end_timestamp_pipeline_stage, ::std::string const && label);

			void begin_timestamp();

			void end_timestamp();

			::std::uint32_t get_statistics_index() const noexcept;

			::framework::profiler & profiler;
			::framework::gpu::command_buffer * const command_buffer;
			::framework::gpu::pipeline_stage_flags const begin_timestamp_pipeline_stage;
			::framework::gpu::pipeline_stage_flags const end_timestamp_pipeline_stage;
			::std::string const label;
			::std::uint32_t timestamp_index;
		};

	private:
		struct frame_data
		{
			::std::vector<struct ::framework::profiler::statistics> statistics;
		};

		::framework::gpu::device * device;
		::framework::gpu::query_pool * query_pool;
		::framework::gpu::buffer * query_pool_buffer;
		::framework::gpu::device_memory * query_pool_device_memory;
		void * query_pool_mapped_data;
		float queue_timestamp_period;
		bool query_pool_host_commands;
		float timestamp_period;
		::std::uint32_t max_statistic_count;
		::std::uint32_t frame_in_flight_index;
		::std::uint32_t frame_in_flight_count;
		::std::uint32_t current_depth;
		::std::vector<struct ::framework::profiler::frame_data> frame_data;
	};
}

#include "profiler.inl"