inline ::framework::gpu::fence * (::framework::fence::get_fence)() const noexcept
{
	return this->device_fence;
}

inline ::framework::fence_pool::fence_pool(::framework::gpu_context * gpu_context, ::framework::coroutine::static_thread_pool * static_thread_pool) :
	gpu_context(gpu_context),
	static_thread_pool(static_thread_pool)
{
	this->wait_for_completion_task = this->wait_for_fences();
}

inline framework::fence_pool::~fence_pool()
{
	for (::framework::fence * const fence : this->fence_pool_vector)
	{
		this->gpu_context->get_device()->destroy_fence(fence->get_fence(), nullptr);
	}
}

inline ::framework::gpu_context * (::framework::fence_pool::get_gpu_context)() const noexcept
{
	return this->gpu_context;
}

template<typename T>
inline ::framework::coroutine::immediate_task<void> framework::command_context::update_buffer(::framework::gpu::buffer * dst_buffer, T const & data)
{
	return this->update_buffer(dst_buffer, &data, 0, sizeof(T));
}

template<typename T>
inline ::framework::coroutine::immediate_task<void>(::framework::command_context::update_buffer)(::framework::gpu::buffer * dst_buffer, T const & data, ::std::uint32_t index)
{
	return this->update_buffer(dst_buffer, &data, sizeof(T) * index, sizeof(T));
}

inline ::framework::command_queue & ::framework::command_context::get_command_queue() const noexcept
{
	return this->command_queue;
}

inline ::framework::gpu::command_buffer * (::framework::command_context::get_command_buffer)() const noexcept
{
	return this->command_buffer;
}

inline ::framework::gpu::command_pool * ::framework::command_context::get_command_pool() const noexcept
{
	return this->command_pool;
}

inline ::framework::linear_allocator & (::framework::command_context::get_linear_allocator)() noexcept
{
	return this->linear_allocator;
}

inline auto ::framework::fence::operator co_await() const noexcept
{
	return this->async_manual_reset_event.operator co_await();
}

inline ::framework::coroutine::self_destructible_thread_pool_coroutine(::framework::fence::signal)(::framework::coroutine::static_thread_pool & static_thread_pool) noexcept
{
	this->async_manual_reset_event.set();
	co_return;
}

inline void ::framework::fence::reset() noexcept
{
	this->async_manual_reset_event.reset();
}

inline ::framework::gpu::semaphore * (::framework::command_context::get_semaphore)() const noexcept
{
	return this->semaphore;
}

inline ::framework::command_buffer_manager const & (::framework::command_queue::get_command_buffer_manager)() const noexcept
{
	return this->command_buffer_manager;
}

inline ::std::uint32_t(::framework::command_queue::get_queue_family_index)() const noexcept
{
	return this->queue_family_index;
}

inline ::framework::coroutine::mutex & (::framework::command_queue::get_queue_mutex)() noexcept
{
	return this->queue_mutex;
}

inline ::framework::gpu_context * (::framework::command_buffer_manager::get_gpu_context)() const noexcept
{
	return this->gpu_context;
}

inline ::framework::linear_allocator_page_manager * (::framework::command_buffer_manager::get_linear_allocator_page_manager)() const noexcept
{
	return this->linear_allocator_page_manager;
}

inline ::framework::fence_pool & ::framework::command_buffer_manager::get_fence_pool() const noexcept
{
	return *this->fence_pool.get();
}

inline ::framework::command_queue * ::framework::command_buffer_manager::get_graphics_queue() const noexcept
{
	return this->graphics_queue.get();
}

inline ::framework::command_queue * ::framework::command_buffer_manager::get_compute_queue() const noexcept
{
	return this->compute_queue.get();
}

inline ::framework::command_queue * ::framework::command_buffer_manager::get_transfer_queue() const noexcept
{
	return this->transfer_queue.get();
}