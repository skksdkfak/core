#pragma once

[[vk::binding(0, GPU_LOG_DESCRIPTOR_SET_INDEX)]]
RWByteAddressBuffer gpu_log_buffer : register(u0, GPU_LOG_REGISTER_SPACE);
[[vk::binding(1, GPU_LOG_DESCRIPTOR_SET_INDEX)]]
RWByteAddressBuffer gpu_log_counter_buffer : register(u1, GPU_LOG_REGISTER_SPACE);

namespace framework
{
	template<typename valute_type>
	uint get_type_id_mask(valute_type)
	{
		return 0;
	}

	template<>
	uint get_type_id_mask(float)
	{
		return 1;
	}

	template<>
	uint get_type_id_mask(float2)
	{
		return 1 | 1 << 2;
	}

	template<>
	uint get_type_id_mask(float3)
	{
		return 1 | 1 << 2 | 1 << 4;
	}

	template<>
	uint get_type_id_mask(float4)
	{
		return 1 | 1 << 2 | 1 << 4 | 1 << 6;
	}

	template<>
	uint get_type_id_mask(int)
	{
		return 2;
	}

	template<>
	uint get_type_id_mask(int2)
	{
		return 2 | 2 << 2;
	}

	template<>
	uint get_type_id_mask(int3)
	{
		return 2 | 2 << 2 | 2 << 4;
	}

	template<>
	uint get_type_id_mask(int4)
	{
		return 2 | 2 << 2 | 2 << 4 | 2 << 6;
	}

	template<>
	uint get_type_id_mask(uint)
	{
		return 3;
	}

	template<>
	uint get_type_id_mask(uint2)
	{
		return 3 | 3 << 2;
	}

	template<>
	uint get_type_id_mask(uint3)
	{
		return 3 | 3 << 2 | 3 << 4;
	}

	template<>
	uint get_type_id_mask(uint4)
	{
		return 3 | 3 << 2 | 3 << 4 | 3 << 6;
	}

	uint get_type_component_count(float)
	{
		return 1;
	}

	uint get_type_component_count(int)
	{
		return 1;
	}
	
	uint get_type_component_count(uint)
	{
		return 1;
	}
	
	template<typename component_type, int component_count>
	uint get_type_component_count(vector<component_type, component_count>)
	{
		return component_count;
	}

	struct gpu_log
	{
		uint3 current_invocation_index;
		uint3 target_invocation_index;
		bool print_force;

		void set_current_invocation_index(uint3 invocation_index)
		{
			this.current_invocation_index = invocation_index;
		}
		
		void set_target_invocation_index(uint3 invocation_index)
		{
			this.target_invocation_index = invocation_index;
		}

		void set_print_force(bool print_force)
		{
			this.print_force = print_force;
		}

		uint get_log_max_size()
		{
			return 1024u;
		}

		bool begin_log_entry(uint32_t element_count, uint32_t element_type_mask, out uint log_index)
		{
			if (any(this.current_invocation_index != this.target_invocation_index) && !this.print_force)
			{
				return false;
			}

			const uint entry_size = 1 + element_count;
			
			gpu_log_counter_buffer.InterlockedAdd(0, entry_size, log_index);
			if (log_index + entry_size > this.get_log_max_size())
			{
				return false;
			}

			gpu_log_buffer.Store(log_index++ * 4u, (element_count - 1) | (element_type_mask << 4u));

			return true;
		}

		template<typename value_0_t>
		void print(value_0_t value_0)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t element_count = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
		}

		template<typename value_0_t, typename value_1_t>
		void print(value_0_t value_0, value_1_t value_1)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t element_count = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t element_count = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t, typename value_3_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2, value_3_t value_3)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t value_3_component_offset = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t element_count = value_3_component_offset + ::framework::get_type_component_count(value_3);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2) |
				::framework::get_type_id_mask(value_3) << (value_3_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
			gpu_log_buffer.Store((log_index + value_3_component_offset) * 4u, value_3);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t, typename value_3_t, typename value_4_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2, value_3_t value_3, value_4_t value_4)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t value_3_component_offset = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t value_4_component_offset = value_3_component_offset + ::framework::get_type_component_count(value_3);
			const uint32_t element_count = value_4_component_offset + ::framework::get_type_component_count(value_4);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2) |
				::framework::get_type_id_mask(value_3) << (value_3_component_offset * 2) |
				::framework::get_type_id_mask(value_4) << (value_4_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
			gpu_log_buffer.Store((log_index + value_3_component_offset) * 4u, value_3);
			gpu_log_buffer.Store((log_index + value_4_component_offset) * 4u, value_4);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t, typename value_3_t, typename value_4_t, typename value_5_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2, value_3_t value_3, value_4_t value_4, value_5_t value_5)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t value_3_component_offset = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t value_4_component_offset = value_3_component_offset + ::framework::get_type_component_count(value_3);
			const uint32_t value_5_component_offset = value_4_component_offset + ::framework::get_type_component_count(value_5);
			const uint32_t element_count = value_5_component_offset + ::framework::get_type_component_count(value_5);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2) |
				::framework::get_type_id_mask(value_3) << (value_3_component_offset * 2) |
				::framework::get_type_id_mask(value_4) << (value_4_component_offset * 2) |
				::framework::get_type_id_mask(value_5) << (value_5_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
			gpu_log_buffer.Store((log_index + value_3_component_offset) * 4u, value_3);
			gpu_log_buffer.Store((log_index + value_4_component_offset) * 4u, value_4);
			gpu_log_buffer.Store((log_index + value_5_component_offset) * 4u, value_5);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t, typename value_3_t, typename value_4_t, typename value_5_t, typename value_6_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2, value_3_t value_3, value_4_t value_4, value_5_t value_5, value_6_t value_6)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t value_3_component_offset = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t value_4_component_offset = value_3_component_offset + ::framework::get_type_component_count(value_3);
			const uint32_t value_5_component_offset = value_4_component_offset + ::framework::get_type_component_count(value_5);
			const uint32_t value_6_component_offset = value_5_component_offset + ::framework::get_type_component_count(value_6);
			const uint32_t element_count = value_6_component_offset + ::framework::get_type_component_count(value_6);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2) |
				::framework::get_type_id_mask(value_3) << (value_3_component_offset * 2) |
				::framework::get_type_id_mask(value_4) << (value_4_component_offset * 2) |
				::framework::get_type_id_mask(value_5) << (value_5_component_offset * 2) |
				::framework::get_type_id_mask(value_6) << (value_6_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
			gpu_log_buffer.Store((log_index + value_3_component_offset) * 4u, value_3);
			gpu_log_buffer.Store((log_index + value_4_component_offset) * 4u, value_4);
			gpu_log_buffer.Store((log_index + value_5_component_offset) * 4u, value_5);
			gpu_log_buffer.Store((log_index + value_6_component_offset) * 4u, value_6);
		}

		template<typename value_0_t, typename value_1_t, typename value_2_t, typename value_3_t, typename value_4_t, typename value_5_t, typename value_6_t, typename value_7_t>
		void print(value_0_t value_0, value_1_t value_1, value_2_t value_2, value_3_t value_3, value_4_t value_4, value_5_t value_5, value_6_t value_6, value_7_t value_7)
		{
			const uint32_t value_0_component_offset = 0;
			const uint32_t value_1_component_offset = value_0_component_offset + ::framework::get_type_component_count(value_0);
			const uint32_t value_2_component_offset = value_1_component_offset + ::framework::get_type_component_count(value_1);
			const uint32_t value_3_component_offset = value_2_component_offset + ::framework::get_type_component_count(value_2);
			const uint32_t value_4_component_offset = value_3_component_offset + ::framework::get_type_component_count(value_3);
			const uint32_t value_5_component_offset = value_4_component_offset + ::framework::get_type_component_count(value_5);
			const uint32_t value_6_component_offset = value_5_component_offset + ::framework::get_type_component_count(value_6);
			const uint32_t value_7_component_offset = value_6_component_offset + ::framework::get_type_component_count(value_7);
			const uint32_t element_count = value_7_component_offset + ::framework::get_type_component_count(value_7);
			const uint32_t element_type_mask = ::framework::get_type_id_mask(value_0) << (value_0_component_offset * 2) |
				::framework::get_type_id_mask(value_1) << (value_1_component_offset * 2) |
				::framework::get_type_id_mask(value_2) << (value_2_component_offset * 2) |
				::framework::get_type_id_mask(value_3) << (value_3_component_offset * 2) |
				::framework::get_type_id_mask(value_4) << (value_4_component_offset * 2) |
				::framework::get_type_id_mask(value_5) << (value_5_component_offset * 2) |
				::framework::get_type_id_mask(value_6) << (value_6_component_offset * 2) |
				::framework::get_type_id_mask(value_7) << (value_7_component_offset * 2);
			uint log_index;
			if (!this.begin_log_entry(element_count, element_type_mask, log_index))
			{
				return;
			}

			gpu_log_buffer.Store((log_index + value_0_component_offset) * 4u, value_0);
			gpu_log_buffer.Store((log_index + value_1_component_offset) * 4u, value_1);
			gpu_log_buffer.Store((log_index + value_2_component_offset) * 4u, value_2);
			gpu_log_buffer.Store((log_index + value_3_component_offset) * 4u, value_3);
			gpu_log_buffer.Store((log_index + value_4_component_offset) * 4u, value_4);
			gpu_log_buffer.Store((log_index + value_5_component_offset) * 4u, value_5);
			gpu_log_buffer.Store((log_index + value_6_component_offset) * 4u, value_6);
			gpu_log_buffer.Store((log_index + value_7_component_offset) * 4u, value_7);
		}
	};
}

static ::framework::gpu_log gpu_log;