#include "command_context.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "profiler.hpp"
#include "gpu/utility.hpp"
#include <cassert>

::framework::profiler::profiler(::framework::profiler_create_info const & create_info) :
	device(create_info.device),
	query_pool_host_commands(create_info.physical_device_features->query_pool_host_commands),
	timestamp_period(create_info.physical_device_properties->limits.timestamp_period),
	max_statistic_count(create_info.max_statistic_count),
	frame_in_flight_count(create_info.frame_in_flight_count),
	frame_data(create_info.frame_in_flight_count),
	current_depth(0)
{
	::std::uint32_t const query_count = create_info.max_statistic_count * 2 * create_info.frame_in_flight_count;

	::framework::gpu::query_pool_create_info query_pool_create_info;
	query_pool_create_info.flags = ::framework::gpu::query_pool_create_flags::none;
	query_pool_create_info.query_type = ::framework::gpu::query_type::timestamp;
	query_pool_create_info.query_count = query_count;
	query_pool_create_info.pipeline_statistics = ::framework::gpu::query_pipeline_statistic_flags::none;
	this->device->create_query_pool(&query_pool_create_info, nullptr, &this->query_pool);

	if (!this->query_pool_host_commands)
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint64_t) * query_count;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = create_info.readback_buffer_initial_queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->query_pool_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->query_pool_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(*create_info.physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->query_pool_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->query_pool_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->query_pool_buffer;
		bind_buffer_memory_info.memory = this->query_pool_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->device->map_memory(this->query_pool_device_memory, 0, buffer_create_info.size, ::framework::gpu::memory_map_flags::none, &this->query_pool_mapped_data));
	}

	assert_framework_gpu_result(create_info.queue->get_timestamp_period(&this->queue_timestamp_period));

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		::framework::command_context * const graphics_command_context = co_await create_info.command_buffer_manager->get_graphics_queue()->acquire_command_context();
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));
		graphics_command_context->get_command_buffer()->reset_query_pool(this->query_pool, 0, query_count);
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
		co_await create_info.command_buffer_manager->get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await create_info.command_buffer_manager->get_graphics_queue()->release_command_context(graphics_command_context);
	}());
}

::framework::profiler::~profiler()
{
}

void ::framework::profiler::reset(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t frame_in_flight_index)
{
	command_buffer->reset_query_pool(this->query_pool, frame_in_flight_index * this->max_statistic_count, this->max_statistic_count);
	this->frame_data[frame_in_flight_index].statistics.clear();
	this->frame_in_flight_index = frame_in_flight_index;
}

::std::uint32_t(::framework::profiler::begin_timestamp)(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags pipeline_stage, ::std::string const & label)
{
	struct ::framework::profiler::frame_data & frame_data = this->frame_data[this->frame_in_flight_index];
	::std::uint32_t const timestamp_index = static_cast<::std::uint32_t>(frame_data.statistics.size()) * 2u;

	command_buffer->write_timestamp(pipeline_stage, this->query_pool, this->frame_in_flight_index * this->max_statistic_count + timestamp_index);

	struct ::framework::profiler::statistics & statistic = frame_data.statistics.emplace_back();
	assert(frame_data.statistics.size() <= this->max_statistic_count);
	statistic.label = label;
	statistic.depth = this->current_depth;

	return timestamp_index;
}

void ::framework::profiler::end_timestamp(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags pipeline_stage, ::std::uint32_t timestamp_index)
{
	command_buffer->write_timestamp(pipeline_stage, this->query_pool, this->frame_in_flight_index * this->max_statistic_count + timestamp_index + 1);
}

void ::framework::profiler::copy_query_pool_results(::framework::gpu::command_buffer * command_buffer)
{
	if (!this->query_pool_host_commands)
	{
		struct ::framework::profiler::frame_data & frame_data = this->frame_data[this->frame_in_flight_index];
		::std::uint32_t const timestamp_index = static_cast<::std::uint32_t>(frame_data.statistics.size()) * 2u;

		command_buffer->copy_query_pool_results(this->query_pool, this->frame_in_flight_index * this->max_statistic_count, timestamp_index, this->query_pool_buffer, 0, sizeof(::uint64_t), ::framework::gpu::query_result_flags::query_result_64_bit);
	}
}

void ::framework::profiler::get_query_pool_results(::std::uint32_t frame_in_flight_index)
{
	struct ::framework::profiler::frame_data & frame_data = this->frame_data[frame_in_flight_index];
	::std::uint32_t const query_counter = static_cast<::std::uint32_t>(frame_data.statistics.size()) * 2u;

	if (query_counter > 0)
	{
		if (this->query_pool_host_commands)
		{
			::std::vector<::std::uint64_t> query_pool_data(static_cast<::std::size_t>(query_counter));
			assert_framework_gpu_result(this->device->get_query_pool_results(this->query_pool, frame_in_flight_index * this->max_statistic_count, query_counter, sizeof(::std::uint64_t) * query_counter, query_pool_data.data(), sizeof(::std::uint64_t), ::framework::gpu::query_result_flags::query_result_64_bit));

			for (::std::uint32_t i = 0; i < query_counter; i += 2)
			{
				frame_data.statistics[i >> 1].delta_timestamp = (query_pool_data[i + 1] - query_pool_data[i]) * this->timestamp_period * this->queue_timestamp_period;
			}
		}
		else
		{
			::std::uint64_t * query_pool_data = static_cast<::std::uint64_t *>(this->query_pool_mapped_data);

			for (::std::uint32_t i = 0; i < query_counter; i += 2)
			{
				frame_data.statistics[i >> 1].delta_timestamp = (query_pool_data[i + 1] - query_pool_data[i]) * this->timestamp_period * this->queue_timestamp_period;
			}
		}
	}
}