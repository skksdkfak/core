#pragma once

#include "coroutine/async_manual_reset_event.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/single_consumer_event.hpp"
#include "coroutine/task.hpp"
#include "coroutine/thread_pool_task.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "linear_allocator.hpp"
#include <list>
#include <memory>
#include <queue>
#include <vector>

namespace framework::coroutine
{
	class static_thread_pool;
}

namespace framework
{
	class fence_pool;
	class command_queue;
	class command_buffer_manager;

	class fence
	{
	public:
		fence(::framework::fence_pool * fence_pool);

		fence(::framework::gpu::fence * fence);

		auto operator co_await() const noexcept;

		::framework::coroutine::self_destructible_thread_pool_coroutine signal(::framework::coroutine::static_thread_pool & static_thread_pool) noexcept;

		void reset() noexcept;

		::framework::gpu::fence * get_fence() const noexcept;

	private:
		bool non_pool;
		::framework::gpu::fence * device_fence;
		::framework::coroutine::async_manual_reset_event async_manual_reset_event;
	};

	class fence_pool
	{
	public:
		fence_pool(::framework::gpu_context * gpu_context, ::framework::coroutine::static_thread_pool * static_thread_pool);

		~fence_pool();

		::framework::coroutine::immediate_task<::framework::fence *> request_fence();

		::framework::coroutine::immediate_task<void> discard_fence(::framework::fence * fence);

		::framework::coroutine::self_destructible_coroutine retire_fence(::framework::fence * fence);

		::framework::gpu_context * get_gpu_context() const noexcept;

	private:
		::framework::coroutine::immediate_task<void> wait_for_fences();

		::framework::gpu_context * gpu_context;
		::framework::coroutine::static_thread_pool * static_thread_pool;
		::std::vector<::framework::fence *> fence_pool_vector;
		::std::queue<::framework::fence *> available_fences;
		::std::vector<::framework::fence *> retired_fences;
		::framework::coroutine::single_consumer_event single_consumer_event;
		::framework::coroutine::mutex mutex;
		::framework::coroutine::mutex retired_fences_mutex;
		::framework::coroutine::immediate_task<void> wait_for_completion_task;
	};

	class command_context
	{
	public:
		command_context(::framework::command_queue & command_queue);

		~command_context();

		template<typename T>
		::framework::coroutine::immediate_task<void> update_buffer(::framework::gpu::buffer * dst_buffer, T const & data);

		template<typename T>
		::framework::coroutine::immediate_task<void> update_buffer(::framework::gpu::buffer * dst_buffer, T const & data, ::std::uint32_t index);

		::framework::coroutine::immediate_task<void> update_buffer(::framework::gpu::buffer * dst_buffer, void const * data, ::framework::gpu::device_size offset, ::framework::gpu::device_size size);

		::framework::coroutine::immediate_task<struct ::framework::linear_allocator::allocation> read_buffer(::framework::linear_allocator & cpu_read_linear_allocator, ::framework::gpu::buffer * src_buffer, ::framework::gpu::device_size offset, ::framework::gpu::device_size size);

		::framework::command_queue & get_command_queue() const noexcept;

		::framework::gpu::command_buffer * get_command_buffer() const noexcept;

		::framework::gpu::command_pool * get_command_pool() const noexcept;

		::framework::linear_allocator & get_linear_allocator() noexcept;

		::framework::gpu::semaphore * get_semaphore() const noexcept;

	private:
		::framework::command_queue & command_queue;
		::framework::gpu::command_pool * command_pool;
		::framework::gpu::command_buffer * command_buffer;
		::framework::linear_allocator linear_allocator;
		::framework::gpu::semaphore * semaphore;
	};

	class command_queue
	{
	public:
		command_queue(::framework::command_buffer_manager & command_buffer_manager, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index);

		~command_queue();

		::framework::coroutine::immediate_task<class ::framework::command_context *> acquire_command_context();

		::framework::coroutine::immediate_task<void> release_command_context(class ::framework::command_context * command_context);

		::framework::coroutine::immediate_task<void> reset_command_context(class ::framework::command_context * command_context);

		::framework::coroutine::immediate_task<void> submit(class ::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos, ::framework::gpu::fence * fence);
		
		::framework::coroutine::immediate_task<void> submit(class ::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos);
		
		::framework::coroutine::immediate_task<void> submit_and_wait(class ::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos);
		
		::framework::coroutine::immediate_task<::framework::gpu::result> submit(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence);

		::framework::coroutine::immediate_task<::framework::gpu::result> present(::framework::gpu::present_info const * present_info);

		::framework::coroutine::immediate_task<void> signal_semaphores(::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos);

		::framework::coroutine::immediate_task<void> signal_semaphore(::framework::gpu::semaphore * semaphore);

		::framework::command_buffer_manager const & get_command_buffer_manager() const noexcept;

		::std::uint32_t get_queue_family_index() const noexcept;

		::framework::coroutine::mutex & get_queue_mutex() noexcept;

	private:
		friend class ::framework::command_context;

		::framework::command_buffer_manager & command_buffer_manager;
		::framework::coroutine::mutex queue_mutex;
		::framework::coroutine::mutex command_pool_mutex;
		::std::vector<class ::framework::command_context *> command_context_pool;
		::std::queue<class ::framework::command_context *> ready_command_contexts;
		::std::uint32_t queue_family_index;
		::std::uint32_t queue_index;
		::framework::gpu::queue * queue;
	};

	class command_buffer_manager
	{
	public:
		command_buffer_manager(::framework::gpu_context * gpu_context, ::framework::linear_allocator_page_manager * linear_allocator_page_manager, ::framework::coroutine::static_thread_pool * static_thread_pool);

		~command_buffer_manager();

		::framework::coroutine::immediate_task<void> wait_idle();

		::framework::gpu_context * get_gpu_context() const noexcept;

		::framework::linear_allocator_page_manager * get_linear_allocator_page_manager() const noexcept;

		::framework::fence_pool & get_fence_pool() const noexcept;

		::framework::command_queue * get_graphics_queue() const noexcept;

		::framework::command_queue * get_compute_queue() const noexcept;

		::framework::command_queue * get_transfer_queue() const noexcept;

	private:
		::framework::gpu_context * gpu_context;
		::framework::linear_allocator_page_manager * linear_allocator_page_manager;
		::std::unique_ptr<::framework::fence_pool> fence_pool;
		::std::unique_ptr<::framework::command_queue> graphics_queue;
		::std::unique_ptr<::framework::command_queue> compute_queue;
		::std::unique_ptr<::framework::command_queue> transfer_queue;
	};
}

#include "command_context.inl"