#include "math.hlsl"

struct push_constants_t
{
	uint32_t dims;
	uint32_t stride;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = global_invocation_id % push_constants.stride;
	const uint inter_elem_idx = global_invocation_id / push_constants.stride;
	if (inter_elem_idx >= size || intra_elem_idx >= push_constants.dims)
	{
		return;
	}

	const ELEMENT_TYPE loss_gradient = loss_gradient_buffer[global_invocation_id - intra_elem_idx];
	const ELEMENT_TYPE gradient = gradients_buffer[global_invocation_id] * loss_gradient;
	gradients_buffer[global_invocation_id] = isinf(gradient) || isnan(gradient) ? ELEMENT_TYPE(0.0) : gradient;
}
