#include "numeric.hpp"
#include "normalizing_flow.hpp"

inline ::framework::gpu::buffer * ::framework::normalizing_flows::normalizing_flow::get_x_buffer() const
{
	return this->x_buffer;
}

inline ::framework::gpu::buffer * ::framework::normalizing_flows::normalizing_flow::get_features_buffer() const
{
	return this->encoded_features_buffer;
}

inline ::std::uint32_t (::framework::normalizing_flows::normalizing_flow::get_features_stride)() const
{
	return this->features_stride;
}

inline ::std::uint32_t (::framework::normalizing_flows::normalizing_flow::get_output_stride)() const
{
	return this->output_stride;
}

inline ::framework::gpu::buffer * ::framework::normalizing_flows::normalizing_flow::get_log_jacobian_buffer() const
{
	return this->log_jacobian_buffer;
}

inline ::framework::gpu::device_size(::framework::normalizing_flows::normalizing_flow::get_coupling_transform_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = 0u; /* [0] - coupling transform */
	return dispatch_indirect_command_offset;
}

inline ::framework::gpu::device_size framework::normalizing_flows::normalizing_flow::get_coupling_transform_no_grad_dispatch_indirect_command_offset() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_coupling_transform_dispatch_indirect_command_offset() + sizeof(::framework::gpu::dispatch_indirect_command); /* [1] - coupling transform no_grad */
	return dispatch_indirect_command_offset;
}

inline ::framework::gpu::device_size (::framework::normalizing_flows::normalizing_flow::get_calc_log_prob_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_coupling_transform_dispatch_indirect_command_offset() + sizeof(::framework::gpu::dispatch_indirect_command); /* [1] - calc_log_prob */
	return dispatch_indirect_command_offset;
}

inline ::framework::gpu::device_size (::framework::normalizing_flows::normalizing_flow::get_loss_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_calc_log_prob_dispatch_indirect_command_offset() + sizeof(::framework::gpu::dispatch_indirect_command); /* [2] - loss */
	return dispatch_indirect_command_offset;
}

inline ::framework::gpu::device_size(::framework::normalizing_flows::normalizing_flow::get_chain_loss_grad_dispatch_indirect_command_offset)() const noexcept
{
	return this->get_loss_dispatch_indirect_command_offset();
}

inline ::framework::gpu::device_size(::framework::normalizing_flows::normalizing_flow::get_one_blob_encoding_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_chain_loss_grad_dispatch_indirect_command_offset() + sizeof(::framework::gpu::dispatch_indirect_command);
	::framework::gpu::device_size const dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(dispatch_indirect_command_offset, this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);
	return dispatch_indirect_command_aligned_offset;
}

inline ::framework::gpu::device_size(::framework::normalizing_flows::normalizing_flow::get_x_a_one_blob_encoding_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_one_blob_encoding_dispatch_indirect_command_offset() + this->one_blob_execution_policy->get_dispatch_indirect_command_size();
	::framework::gpu::device_size const dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(dispatch_indirect_command_offset, this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);
	return dispatch_indirect_command_aligned_offset;
}

inline ::framework::gpu::device_size (::framework::normalizing_flows::normalizing_flow::get_encoding_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset() + this->x_a_one_blob_execution_policy->get_dispatch_indirect_command_size();
	::framework::gpu::device_size const dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(dispatch_indirect_command_offset, this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);
	return dispatch_indirect_command_aligned_offset;
}