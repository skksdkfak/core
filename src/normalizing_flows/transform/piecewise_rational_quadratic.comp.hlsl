#include "algorithm/threadgroup/scan.hlsl"
#include "algorithm/threadgroup/sum.hlsl"
#include "gpu_log.hlsl"
#include "linalg.hlsl"
#include "numbers.hlsl"

[[vk::constant_id(0)]] const int x_a_layout = 0;
[[vk::constant_id(1)]] const uint32_t x_a_row_index = 0;
[[vk::constant_id(2)]] const uint32_t x_a_row_count = 1;
[[vk::constant_id(3)]] const uint bin_count = 1;
[[vk::constant_id(4)]] const uint bin_count_padded = 1;
[[vk::constant_id(5)]] const uint transform_parameters_stride = 1;

static const float min_bin_width = 1e-3f;
static const float min_bin_height = 1e-3f;
static const float min_derivative = 1e-3f;
static const bool softplus_uses_threshold = true;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);

namespace framework
{
	struct sum_executor
	{
		static ::framework::sum_executor create(vector<float, 1> data)
		{
			::framework::sum_executor sum_executor;
			sum_executor.data = data;
			return sum_executor;
		}

		inline vector<float, 1> load(uint global_index, uint local_index)
		{
			return this.data[local_index];
		}

		inline void store(vector<float, 1> data)
		{
			this.data = data;
		}

		vector<float, 1> data;
	};

	struct scan_executor
	{
		static ::framework::scan_executor create(vector<float, 1> data)
		{
			::framework::scan_executor scan_executor;
			scan_executor.data = data;
			return scan_executor;
		}

		inline uint get_vectorized_size()
		{
			return 0;
		}

		inline vector<float, 1> load_local(uint global_index, uint local_index)
		{
			return this.data_local;
		}

		inline void store_local(uint global_index, uint local_index, vector<float, 1> data)
		{
			this.data_local = data;
		}

		inline vector<float, 1> load(uint global_index, uint local_index)
		{
			return this.data;
		}

		inline void store(uint global_index, uint local_index, vector<float, 1> data)
		{
			this.data = data;
		}

		vector<float, 1> data;
		vector<float, 1> data_local;
	};
}

groupshared ::framework::algorithm::threadgroup::sum<float, 1u, WORKGROUP_SIZE, 1u> threadgroup_sum;
groupshared ::framework::algorithm::threadgroup::scan<float, 1u, WORKGROUP_SIZE, 1u> threadgroup_scan;

groupshared float exp_w_groupshared[WORKGROUP_SIZE];
groupshared float bin_width_cumsum[WORKGROUP_SIZE];
groupshared uint bin_index_groupshared;
groupshared float bin_width_left_integral_b;
groupshared float bin_height_left_integral_b;
groupshared float bin_width_b;
groupshared float bin_height_b;
groupshared float exp_wb;
groupshared float exp_hb;

float softplus(float x, float beta, float threshold)
{
	return x * beta > threshold && ::softplus_uses_threshold ? x : log(1 + exp(beta * x)) / beta;
}

[numthreads(WORKGROUP_SIZE, 1, 1)]
[shader("compute")]
void forward_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = group_thread_id.x;
	const uint inter_elem_idx = group_id.x;
	if (inter_elem_idx >= size)
	{
		return;
	}

	const uint32_t k = ::bin_count;
	const float min_bin_width_weight = 1 - min_bin_width * k;
	const float min_bin_height_weight = 1 - min_bin_height * k;
	const float d_beta = 1.0f;
	const float d_threshold = 20.0f;

	const uint32_t x_index = inter_elem_idx * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const float w = transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + intra_elem_idx];
	const float h = transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + ::bin_count_padded + intra_elem_idx];
	const float exp_w = exp(w);
	const float exp_h = exp(h);

	exp_w_groupshared[intra_elem_idx] = exp_w;

	::framework::sum_executor w_norm_sum_executor = ::framework::sum_executor::create(exp_w);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, w_norm_sum_executor);

	::framework::sum_executor h_norm_sum_executor = ::framework::sum_executor::create(exp_h);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, h_norm_sum_executor);
	
	const float w_norm = w_norm_sum_executor.data;
	const float h_norm = h_norm_sum_executor.data;
	const float bin_width = min_bin_width + min_bin_width_weight * (exp_w / w_norm);
	const float bin_height = min_bin_height + min_bin_height_weight * (exp_h / h_norm);

	::framework::scan_executor bin_width_scan_executor = ::framework::scan_executor::create(bin_width);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, bin_width_scan_executor);

	bin_width_cumsum[intra_elem_idx] = bin_width_scan_executor.data;

	::framework::scan_executor bin_height_scan_executor = ::framework::scan_executor::create(bin_height);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, bin_height_scan_executor);

	const float bin_width_left_integral = bin_width_scan_executor.data;
	const float bin_height_left_integral = bin_height_scan_executor.data;
	// Note: bin_width_left_integral_next = bin_width_left_integral + bin_width leads to false positive evaluations, where two neighboring threads may both detect the bin_index for themselves.
	// It is also faster than the solution involving InterlockedMin(bin_index) to the shared memory.
	const float bin_width_left_integral_next = intra_elem_idx == ::bin_count - 1 ? 1.0f : bin_width_cumsum[intra_elem_idx + 1];
	const uint32_t bin_index = bin_width_left_integral <= x_b && (bin_width_left_integral_next > x_b || intra_elem_idx == ::bin_count - 1) ? intra_elem_idx : -1;

	if (bin_index != -1)
	{
		bin_index_groupshared = bin_index;
		bin_width_left_integral_b = bin_width_left_integral;
		bin_height_left_integral_b = bin_height_left_integral;
		bin_width_b = bin_width;
		bin_height_b = bin_height;
		exp_wb = exp_w;
		exp_hb = exp_h;
	}

	GroupMemoryBarrierWithGroupSync();

	const bool db_tail = bin_index_groupshared == 0 || bin_index_groupshared == k - 1;
	const bool db_next_tail = bin_index_groupshared + 1 >= k - 1;
	const float tail_derivative = log(exp(1.0f - min_derivative) - 1);
	const float raw_input_derivative = db_tail ? tail_derivative : transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + 2 * ::bin_count_padded + bin_index_groupshared - 1];
	const float raw_input_derivative_plus_one = db_next_tail ? tail_derivative : transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + 2 * ::bin_count_padded + bin_index_groupshared];
	const float input_derivative = min_derivative + ::softplus(raw_input_derivative, d_beta, d_threshold);
	const float input_derivative_plus_one = min_derivative + ::softplus(raw_input_derivative_plus_one, d_beta, d_threshold);
	const float input_delta = bin_height_b / bin_width_b;
	const float x_b_minus_input_cumwidth = x_b - bin_width_left_integral_b;
	const float theta = x_b_minus_input_cumwidth / bin_width_b;
	const float one_minus_theta = 1 - theta;
	const float theta_one_minus_theta = theta * one_minus_theta;
	const float theta_sqr = theta * theta;
	const float one_minus_theta_sqr = one_minus_theta * one_minus_theta;
	const float w_norm_sqr = w_norm * w_norm;
	const float h_norm_sqr = h_norm * h_norm;
	const float input_bin_width_sqr = bin_width_b * bin_width_b;
	const float input_delta_sqr = input_delta * input_delta;

	const float numerator = bin_height_b * (input_delta * theta_sqr + input_derivative * theta_one_minus_theta);
	const float denominator_a = input_derivative + input_derivative_plus_one - 2 * input_delta;
	const float denominator = input_delta + denominator_a * theta_one_minus_theta;
	const float y_b = bin_height_left_integral_b + numerator / denominator;

	const float derivative_numerator_b = input_derivative_plus_one * theta_sqr + 2 * input_delta * theta_one_minus_theta + input_derivative * one_minus_theta_sqr;
	const float derivative_numerator = input_delta_sqr * derivative_numerator_b;
	const float logabsdet = log(derivative_numerator) - 2 * log(denominator);

	if (bin_index != -1)
	{
		x_buffer[x_a_index] = y_b;
		x_buffer[x_b_index] = x_a;
		const uint x_a_column_index = inter_elem_idx;
		const uint x_a_column_count = size;
		const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
		x_a_buffer[y_b_index] = y_b;
		log_jacobian_buffer[inter_elem_idx * transform_parameters_stride] += logabsdet;
	}

	const float dloss_norm_weight = 1e-4f;
	const float dloss_w_norm_difference = k - w_norm;
	const float dloss_h_norm_difference = k - h_norm;

	const float exp_wi = exp_w;
	const float dw_norm_dwi = exp_wi;
	const float dexp_wb_dwi = (intra_elem_idx == bin_index_groupshared ? exp_wb : 0.0f);
	const float dinput_bin_width_dwi = min_bin_width_weight * ((w_norm * dexp_wb_dwi - exp_wb * dw_norm_dwi) / w_norm_sqr);
	const float dinput_delta_dwi = (-bin_height_b * dinput_bin_width_dwi) / input_bin_width_sqr;
	const float dinput_delta_sqr_dwi = 2 * input_delta * dinput_delta_dwi;
	float dinput_cumwidth_dwi = 0.0f;
	for (uint32_t j = 0; j < bin_index_groupshared; j++)
	{
		const float exp_wj = exp_w_groupshared[j];
		const float dexp_wj_dwi = (intra_elem_idx == j ? exp_wj : 0.0f);
		const float dwj_dwi = min_bin_width_weight * ((w_norm * dexp_wj_dwi - exp_wj * dw_norm_dwi) / w_norm_sqr);
		dinput_cumwidth_dwi += dwj_dwi;
	}
	const float dx_b_minus_input_cumwidth_dwi = -dinput_cumwidth_dwi;
	const float dtheta_dwi = (bin_width_b * dx_b_minus_input_cumwidth_dwi - x_b_minus_input_cumwidth * dinput_bin_width_dwi) / input_bin_width_sqr;
	const float done_minus_theta_dwi = -dtheta_dwi;
	const float done_minus_theta_sqr_dwi = 2 * one_minus_theta * done_minus_theta_dwi;
	const float dtheta_sqr_dwi = 2 * theta * dtheta_dwi;
	const float dtheta_one_minus_theta_dwi = theta * done_minus_theta_dwi + dtheta_dwi * one_minus_theta;
	const float dderivative_numerator_b_dwi = input_derivative_plus_one * dtheta_sqr_dwi + 2 * (input_delta * dtheta_one_minus_theta_dwi + dinput_delta_dwi * theta_one_minus_theta) + input_derivative * done_minus_theta_sqr_dwi;
	const float dderivative_numerator_dwi = input_delta_sqr * dderivative_numerator_b_dwi + dinput_delta_sqr_dwi * derivative_numerator_b;
	const float ddenominator_a_dwi = -2 * dinput_delta_dwi;
	const float ddenominator_dwi = dinput_delta_dwi + denominator_a * dtheta_one_minus_theta_dwi + ddenominator_a_dwi * theta_one_minus_theta;
	const float dlogabsdet_dwi = dderivative_numerator_dwi / derivative_numerator - 2 * ddenominator_dwi / denominator;
	// loss that keeps w_norm within the k range to prevent over- or underflow
	const float dloss_w_norm_dwi = dloss_norm_weight * sign(dloss_w_norm_difference) * dw_norm_dwi;
	gradients_buffer[inter_elem_idx * transform_parameters_stride + intra_elem_idx] = ELEMENT_TYPE(dlogabsdet_dwi + dloss_w_norm_dwi);

	const float exp_hi = exp_h;
	const float dh_norm_dhi = exp_hi;
	const float dexp_hb_dhi = (intra_elem_idx == bin_index_groupshared ? exp_hb : 0.0f);
	const float dinput_height_dhi = min_bin_height_weight * ((h_norm * dexp_hb_dhi - exp_hb * dh_norm_dhi) / h_norm_sqr);
	const float dinput_delta_dhi = dinput_height_dhi / bin_width_b;
	const float dinput_delta_sqr_dhi = 2 * input_delta * dinput_delta_dhi;
	const float dderivative_numerator_b_dhi = 2 * (dinput_delta_dhi * theta_one_minus_theta);
	const float dderivative_numerator_dhi = input_delta_sqr * dderivative_numerator_b_dhi + dinput_delta_sqr_dhi * derivative_numerator_b;
	const float ddenominator_a_dhi = -2 * dinput_delta_dhi;
	const float ddenominator_dhi = dinput_delta_dhi + ddenominator_a_dhi * theta_one_minus_theta;
	const float dlogabsdet_dhi = dderivative_numerator_dhi / derivative_numerator - 2 * ddenominator_dhi / denominator;
	// loss that keeps h_norm within the k range to prevent over- or underflow
	const float dloss_h_norm_dhi = dloss_norm_weight * sign(dloss_h_norm_difference) * dh_norm_dhi;
	gradients_buffer[inter_elem_idx * transform_parameters_stride + k + intra_elem_idx] = ELEMENT_TYPE(dlogabsdet_dhi + dloss_h_norm_dhi);

	const float exp_db_beta = exp(raw_input_derivative * d_beta);
	const float exp_db_next_beta = exp(raw_input_derivative_plus_one * d_beta);
	const float dinput_derivative_ddi = db_tail ? 0 : (intra_elem_idx == bin_index_groupshared - 1 ? (raw_input_derivative * d_beta > d_threshold && ::softplus_uses_threshold ? 1.0f : exp_db_beta / (exp_db_beta + 1.0f)) : 0.0f);
	const float dinput_derivative_plus_one_ddi = db_next_tail ? 0 : (intra_elem_idx == bin_index_groupshared ? (raw_input_derivative_plus_one * d_beta > d_threshold && ::softplus_uses_threshold ? 1.0f : exp_db_next_beta / (exp_db_next_beta + 1.0f)) : 0.0f);
	const float ddenominator_a_ddi = dinput_derivative_ddi + dinput_derivative_plus_one_ddi;
	const float dderivative_numerator_b_ddi = dinput_derivative_plus_one_ddi * theta_sqr + dinput_derivative_ddi * one_minus_theta_sqr;
	const float dderivative_numerator_ddi = input_delta_sqr * dderivative_numerator_b_ddi;
	const float ddenominator_ddi = ddenominator_a_ddi * theta_one_minus_theta;
	const float dlogabsdet_ddi = dderivative_numerator_ddi / derivative_numerator - 2 * ddenominator_ddi / denominator;
	if (intra_elem_idx < ::bin_count - 1)
	{
		gradients_buffer[inter_elem_idx * transform_parameters_stride + 2 * k + intra_elem_idx] = ELEMENT_TYPE(dlogabsdet_ddi);
	}
}

[numthreads(WORKGROUP_SIZE, 1, 1)]
[shader("compute")]
void inverse_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = group_thread_id.x;
	const uint inter_elem_idx = group_id.x;
	if (inter_elem_idx >= size)
	{
		return;
	}

	const uint32_t k = BIN_COUNT;
	const float min_bin_width_weight = 1 - min_bin_width * k;
	const float min_bin_height_weight = 1 - min_bin_height * k;
	const float d_beta = 1.0f;
	const float d_threshold = 20.0f;

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[k];
	float h[k];
	float w_norm = float(0.0);
	float h_norm = float(0.0);
	float bin_width_left_integral = float(0.0);
	float bin_height_left_integral = float(0.0);
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		h[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + k + i]);
		w_norm += w[i];
		h_norm += h[i];
	}
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = min_bin_width + min_bin_width_weight * (w[i] / w_norm);
		h[i] = min_bin_height + min_bin_height_weight * (h[i] / h_norm);
	}
	uint bin_index = 0;
	for (uint i = 0; i < k - 1; i++)
	{
		const float input_cumheight_next = bin_height_left_integral + h[i];
		if (input_cumheight_next > x_b)
		{
			break;
		}

		bin_index = i + 1;
		bin_width_left_integral += w[i];
		bin_height_left_integral = input_cumheight_next;
	}

	const float bin_width_b = w[bin_index];
	const float bin_height_b = h[bin_index];
	const bool db_tail = bin_index == 0 || bin_index == k - 1;
	const bool db_next_tail = bin_index + 1 >= k - 1;
	const float tail_derivative = log(exp(1.0f - min_derivative) - 1);
	const float raw_input_derivative = db_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + bin_index - 1];
	const float raw_input_derivative_plus_one = db_next_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + bin_index];
	const float input_derivative = min_derivative + ::softplus(raw_input_derivative, d_beta, d_threshold);
	const float input_derivative_plus_one = min_derivative + ::softplus(raw_input_derivative_plus_one, d_beta, d_threshold);
	const float input_delta = bin_height_b / bin_width_b;
	const float x_b_minus_input_cumheight = x_b - bin_height_left_integral;
	const float input_delta_sqr = input_delta * input_delta;
	const float a = x_b_minus_input_cumheight * (input_derivative + input_derivative_plus_one - 2 * input_delta) + bin_height_b * (input_delta - input_derivative);
	const float b = bin_height_b * input_derivative - x_b_minus_input_cumheight * (input_derivative + input_derivative_plus_one - 2 * input_delta);
	const float c = -input_delta * x_b_minus_input_cumheight;
	const float discriminant = b * b - 4 * a * c;
	const float root = (2 * c) / (-b - sqrt(discriminant));
	//const float root = (-b + sqrt(discriminant)) / (2 * a);
	const float one_minus_root = 1 - root;
	const float root_one_minus_root = root * one_minus_root;
	const float root_sqr = root * root;
	const float one_minus_root_sqr = one_minus_root * one_minus_root;
	const float y_b = root * bin_width_b + bin_width_left_integral;
	const float denominator_a = input_derivative + input_derivative_plus_one - 2 * input_delta;
	const float denominator = input_delta + denominator_a * root_one_minus_root;
	const float derivative_numerator_b = input_derivative_plus_one * root_sqr + 2 * input_delta * root_one_minus_root + input_derivative * one_minus_root_sqr;
	const float derivative_numerator = input_delta_sqr * derivative_numerator_b;
	const float logabsdet = log(derivative_numerator) - 2 * log(denominator);

	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += -logabsdet;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void forward_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t k = BIN_COUNT;
	const float min_bin_width_weight = 1 - min_bin_width * k;
	const float min_bin_height_weight = 1 - min_bin_height * k;
	const float d_beta = 1.0f;
	const float d_threshold = 20.0f;

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[k];
	float h[k];
	float w_norm = 0.0f;
	float h_norm = 0.0f;
	float d_sum = 0.0f;
	float bin_width_left_integral = 0.0f;
	float bin_height_left_integral = 0.0f;
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		h[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + k + i]);
		w_norm += w[i];
		h_norm += h[i];
	}
	for (uint32_t i = 0; i < k - 1; i++)
	{
		d_sum += transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + i];
	}
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = min_bin_width + min_bin_width_weight * (w[i] / w_norm);
		h[i] = min_bin_height + min_bin_height_weight * (h[i] / h_norm);
	}
	uint b = 0;
	for (uint i = 0; i < k - 1; i++)
	{
		const float input_cumwidth_next = bin_width_left_integral + w[i];
		if (input_cumwidth_next > x_b)
		{
			break;
		}

		b = i + 1;
		bin_width_left_integral = input_cumwidth_next;
		bin_height_left_integral += h[i];
	}

	const float bin_width_b = w[b];
	const float bin_height_b = h[b];
	const bool db_tail = b == 0 || b == k - 1;
	const bool db_next_tail = b + 1 >= k - 1;
	const float tail_derivative = log(exp(1.0f - min_derivative) - 1);
	const float raw_input_derivative = db_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + b - 1];
	const float raw_input_derivative_plus_one = db_next_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + b];
	const float input_derivative = min_derivative + ::softplus(raw_input_derivative, d_beta, d_threshold);
	const float input_derivative_plus_one = min_derivative + ::softplus(raw_input_derivative_plus_one, d_beta, d_threshold);
	const float input_delta = bin_height_b / bin_width_b;
	const float x_b_minus_input_cumwidth = x_b - bin_width_left_integral;
	const float theta = x_b_minus_input_cumwidth / bin_width_b;
	const float one_minus_theta = 1 - theta;
	const float theta_one_minus_theta = theta * one_minus_theta;
	const float theta_sqr = theta * theta;
	const float one_minus_theta_sqr = one_minus_theta * one_minus_theta;
	const float w_norm_sqr = w_norm * w_norm;
	const float h_norm_sqr = h_norm * h_norm;
	const float input_bin_width_sqr = bin_width_b * bin_width_b;
	const float input_delta_sqr = input_delta * input_delta;

	const float numerator = bin_height_b * (input_delta * theta_sqr + input_derivative * theta_one_minus_theta);
	const float denominator_a = input_derivative + input_derivative_plus_one - 2 * input_delta;
	const float denominator = input_delta + denominator_a * theta_one_minus_theta;
	const float y_b = bin_height_left_integral + numerator / denominator;

	const float derivative_numerator_b = input_derivative_plus_one * theta_sqr + 2 * input_delta * theta_one_minus_theta + input_derivative * one_minus_theta_sqr;
	const float derivative_numerator = input_delta_sqr * derivative_numerator_b;
	const float logabsdet = log(derivative_numerator) - 2 * log(denominator);

	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += logabsdet;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void inverse_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t k = BIN_COUNT;
	const float min_bin_width_weight = 1 - min_bin_width * k;
	const float min_bin_height_weight = 1 - min_bin_height * k;
	const float d_beta = 1.0f;
	const float d_threshold = 20.0f;

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[k];
	float h[k];
	float w_norm = float(0.0);
	float h_norm = float(0.0);
	float bin_width_left_integral = float(0.0);
	float bin_height_left_integral = float(0.0);
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		h[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + k + i]);
		w_norm += w[i];
		h_norm += h[i];
	}
	for (uint32_t i = 0; i < k; i++)
	{
		w[i] = min_bin_width + min_bin_width_weight * (w[i] / w_norm);
		h[i] = min_bin_height + min_bin_height_weight * (h[i] / h_norm);
	}
	uint bin_index = 0;
	for (uint i = 0; i < k - 1; i++)
	{
		const float input_cumheight_next = bin_height_left_integral + h[i];
		if (input_cumheight_next > x_b)
		{
			break;
		}

		bin_index = i + 1;
		bin_width_left_integral += w[i];
		bin_height_left_integral = input_cumheight_next;
	}

	const float bin_width_b = w[bin_index];
	const float bin_height_b = h[bin_index];
	const bool db_tail = bin_index == 0 || bin_index == k - 1;
	const bool db_next_tail = bin_index + 1 >= k - 1;
	const float tail_derivative = log(exp(1.0f - min_derivative) - 1);
	const float raw_input_derivative = db_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + bin_index - 1];
	const float raw_input_derivative_plus_one = db_next_tail ? tail_derivative : transform_parameters_buffer[global_invocation_id * transform_parameters_stride + 2 * k + bin_index];
	const float input_derivative = min_derivative + ::softplus(raw_input_derivative, d_beta, d_threshold);
	const float input_derivative_plus_one = min_derivative + ::softplus(raw_input_derivative_plus_one, d_beta, d_threshold);
	const float input_delta = bin_height_b / bin_width_b;
	const float x_b_minus_input_cumheight = x_b - bin_height_left_integral;
	const float input_delta_sqr = input_delta * input_delta;
	const float a = x_b_minus_input_cumheight * (input_derivative + input_derivative_plus_one - 2 * input_delta) + bin_height_b * (input_delta - input_derivative);
	const float b = bin_height_b * input_derivative - x_b_minus_input_cumheight * (input_derivative + input_derivative_plus_one - 2 * input_delta);
	const float c = -input_delta * x_b_minus_input_cumheight;
	const float discriminant = b * b - 4 * a * c;
	const float root = (2 * c) / (-b - sqrt(discriminant));
	//const float root = (-b + sqrt(discriminant)) / (2 * a);
	const float one_minus_root = 1 - root;
	const float root_one_minus_root = root * one_minus_root;
	const float root_sqr = root * root;
	const float one_minus_root_sqr = one_minus_root * one_minus_root;
	const float y_b = root * bin_width_b + bin_width_left_integral;
	const float denominator_a = input_derivative + input_derivative_plus_one - 2 * input_delta;
	const float denominator = input_delta + denominator_a * root_one_minus_root;
	const float derivative_numerator_b = input_derivative_plus_one * root_sqr + 2 * input_delta * root_one_minus_root + input_derivative * one_minus_root_sqr;
	const float derivative_numerator = input_delta_sqr * derivative_numerator_b;
	const float logabsdet = log(derivative_numerator) - 2 * log(denominator);

	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += -logabsdet;
}