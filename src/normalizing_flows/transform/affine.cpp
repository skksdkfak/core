#include "gpu/utility.hpp"
#include "normalizing_flows/transform/affine.hpp"
#include "resource/resource_manager.hpp"
#include <string>

namespace framework::local
{
	namespace
	{
		struct affine_transform_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t pad0;
			::std::uint32_t pad1;
			::std::uint32_t pad2;
		};
	}
}

::framework::normalizing_flows::transforms::affine::affine(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type component_type, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::linalg::matrix_layout x_a_layout, ::std::uint32_t x_a_row_index, ::std::uint32_t x_a_row_count)
{
	char const * type_name = ::framework::gpu::utility::get_component_type_name(component_type);

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", type_name }
		};

		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/transform/affine_inverse_transform.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) },
		{ 2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t) }
	};

	::std::uint32_t const specialization_data[]
	{
		static_cast<::std::int32_t>(x_a_layout),
		x_a_row_index,
		x_a_row_count
	};

	::framework::gpu::specialization_info specialization_info;
	specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_info.map_entries = specialization_map_entries;
	specialization_info.data_size = sizeof(specialization_data);
	specialization_info.data = specialization_data;

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->pipeline));
	}
}

void ::framework::normalizing_flows::transforms::affine::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	struct ::framework::local::affine_transform_push_constants push_constants
	{
		.count = batch_size
	};

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 0, nullptr);
	command_buffer->push_constants(pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((batch_size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::normalizing_flows::transforms::affine::forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
}

void ::framework::normalizing_flows::transforms::affine::inverse(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
}

void ::framework::normalizing_flows::transforms::affine::inverse_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
}

void ::framework::normalizing_flows::transforms::affine::forward_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	struct ::framework::local::affine_transform_push_constants push_constants
	{
		.count = batch_size
	};

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 0, nullptr);
	command_buffer->push_constants(pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((batch_size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::normalizing_flows::transforms::affine::forward_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
}

void ::framework::normalizing_flows::transforms::affine::inverse_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
}

void ::framework::normalizing_flows::transforms::affine::inverse_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
}