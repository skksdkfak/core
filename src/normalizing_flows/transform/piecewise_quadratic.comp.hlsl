#include "algorithm/threadgroup/scan.hlsl"
#include "algorithm/threadgroup/sum.hlsl"
#include "gpu_log.hlsl"
#include "linalg.hlsl"
#include "numbers.hlsl"

[[vk::constant_id(0)]] const int x_a_layout = 0;
[[vk::constant_id(1)]] const uint32_t x_a_row_index = 0;
[[vk::constant_id(2)]] const uint32_t x_a_row_count = 1;
[[vk::constant_id(3)]] const uint bin_count = 1;
[[vk::constant_id(4)]] const uint bin_count_padded = 1;
[[vk::constant_id(5)]] const uint transform_parameters_stride = 1;

static const float min_bin_width = 1e-7f;
static const float min_bin_height = 1e-7f;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);

float clamp_gradient(float dydx, float normalization_factor, float target_normalization_factor)
{
	const float deviation_factor = 0.05f;
	const float lower_deviation_factor = 1.0f - deviation_factor;
	const float upper_deviation_factor = 1.0f + deviation_factor;
	return normalization_factor < target_normalization_factor * upper_deviation_factor ? (normalization_factor > target_normalization_factor * lower_deviation_factor ? dydx : max(dydx, 0.0f)) : min(dydx, 0.0f);
}

namespace framework
{
	struct sum_executor
	{
		static ::framework::sum_executor create(vector<float, 1> data)
		{
			::framework::sum_executor sum_executor;
			sum_executor.data = data;
			return sum_executor;
		}

		inline vector<float, 1> load(uint global_index, uint local_index)
		{
			return this.data[local_index];
		}

		inline void store(vector<float, 1> data)
		{
			this.data = data;
		}

		vector<float, 1> data;
	};

	struct scan_executor
	{
		static ::framework::scan_executor create(vector<float, 1> data)
		{
			::framework::scan_executor scan_executor;
			scan_executor.data = data;
			return scan_executor;
		}

		inline uint get_vectorized_size()
		{
			return WORKGROUP_SIZE;
		}

		inline vector<float, 1> load_local(uint global_index, uint local_index)
		{
			return this.data_local;
		}

		inline void store_local(uint global_index, uint local_index, vector<float, 1> data)
		{
			this.data_local = data;
		}

		inline vector<float, 1> load(uint global_index, uint local_index)
		{
			return this.data;
		}

		inline void store(uint global_index, uint local_index, vector<float, 1> data)
		{
			this.data = data;
		}

		vector<float, 1> data;
		vector<float, 1> data_local;
	};
}

groupshared ::framework::algorithm::threadgroup::sum<float, 1u, WORKGROUP_SIZE, 1u> threadgroup_sum;
groupshared ::framework::algorithm::threadgroup::scan<float, 1u, WORKGROUP_SIZE, 1u> threadgroup_scan;

groupshared float bin_width_cumsum[WORKGROUP_SIZE];
groupshared float v_median_groupshared[WORKGROUP_SIZE];
groupshared float exp_w_groupshared[WORKGROUP_SIZE];
groupshared uint bin_index_groupshared;
groupshared float bin_width_b;
groupshared float exp_wb;
groupshared float exp_vb;
groupshared float exp_vb_next;
groupshared float alpha_numerator_groupshared;
groupshared float alpha_groupshared;
groupshared float vb_len_groupshared;
groupshared float inv_pdf_groupshared;

[numthreads(WORKGROUP_SIZE, 1, 1)]
[shader("compute")]
void forward_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = group_thread_id.x;
	const uint inter_elem_idx = group_id.x;
	if (inter_elem_idx >= size)
	{
		return;
	}

	const float w = intra_elem_idx < ::bin_count ? transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + intra_elem_idx] : 0.0f;
	const float exp_w = intra_elem_idx < ::bin_count ? exp(w) : 0.0f;
	const float v = transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + ::bin_count_padded + intra_elem_idx];
	const float exp_v = exp(v);
	const float v_next = intra_elem_idx < ::bin_count ? transform_parameters_buffer[inter_elem_idx * transform_parameters_stride + ::bin_count_padded + intra_elem_idx + 1] : 0.0f;
	const float exp_v_next = intra_elem_idx < ::bin_count ? exp(v_next) : 0.0f;
	
	exp_w_groupshared[intra_elem_idx] = exp_w;

	::framework::sum_executor w_norm_sum_executor = ::framework::sum_executor::create(exp_w);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, w_norm_sum_executor);

	::framework::sum_executor v_sum_executor = ::framework::sum_executor::create(exp_v);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, v_sum_executor);
	
	const float w_norm = w_norm_sum_executor.data;
	const float v_sum = v_sum_executor.data;
	const float bin_width = exp_w / w_norm;
	const float v_median = (exp_v + exp_v_next) / 2.0f;

	v_median_groupshared[intra_elem_idx] = v_median;

	::framework::scan_executor bin_width_scan_executor = ::framework::scan_executor::create(bin_width);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, bin_width_scan_executor);

	bin_width_cumsum[intra_elem_idx] = bin_width_scan_executor.data;

	::framework::sum_executor v_norm_sum_executor = ::framework::sum_executor::create(intra_elem_idx < ::bin_count ? v_median * bin_width : 0.0f);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, v_norm_sum_executor);

	const uint32_t x_index = inter_elem_idx * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const float x_a = float(x_buffer[x_a_index]);
	const float x_b = float(x_buffer[x_b_index]) * bin_width_cumsum[::bin_count]; // Rescale to account for the floating point precision error.

	const float v_norm = v_norm_sum_executor.data;
	const float bin_height = exp_v / v_norm;
	const float bin_height_next = exp_v_next / v_norm;

	const float bin_width_left_integral = bin_width_scan_executor.data;
	// Note: bin_width_left_integral_next = bin_width_left_integral + bin_width leads to false positive evaluations, where two neighboring threads may both detect the bin_idx for themselves.
	// It is also faster than the solution involving InterlockedMin(bin_idx) to the shared memory.
	const float bin_width_left_integral_next = intra_elem_idx >= ::bin_count - 1 ? 1.0f : bin_width_cumsum[intra_elem_idx + 1];
	const uint32_t bin_index = intra_elem_idx < ::bin_count && bin_width_left_integral <= x_b && (bin_width_left_integral_next > x_b || intra_elem_idx == ::bin_count - 1) ? intra_elem_idx : -1;

	if (bin_index != -1)
	{
		bin_index_groupshared = bin_index;
		bin_width_b = bin_width;
		exp_wb = exp_w;
		exp_vb = exp_v;
		exp_vb_next = exp_v_next;
	}

	::framework::scan_executor bin_cdf_scan_executor = ::framework::scan_executor::create((bin_height + bin_height_next) / 2.0f * bin_width);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, bin_cdf_scan_executor);

	bin_width_cumsum[intra_elem_idx] = bin_width;

	if (bin_index != -1)
	{
		const float height_left_integral = bin_cdf_scan_executor.data;
		const float bin_width_b = bin_width;
		const float bin_height_b = bin_height;
		const float bin_height_b_next = bin_height_next;
		const float alpha_numerator = x_b - bin_width_left_integral;
		const float alpha = alpha_numerator / bin_width_b;
		const float vb_len = bin_height_b_next - bin_height_b;
		const float y_b = alpha * alpha / 2.0f * vb_len * bin_width_b + alpha * bin_height_b * bin_width_b + height_left_integral;
		const float pdf = bin_height_b + alpha * vb_len;
		const float inv_pdf = 1.0f / pdf;

		alpha_numerator_groupshared = alpha_numerator;
		alpha_groupshared = alpha;
		vb_len_groupshared = vb_len;
		inv_pdf_groupshared = inv_pdf;
		x_buffer[x_a_index] = y_b;
		x_buffer[x_b_index] = x_a;
		const uint x_a_column_index = inter_elem_idx;
		const uint x_a_column_count = size;
		const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
		x_a_buffer[y_b_index] = y_b;
		log_jacobian_buffer[inter_elem_idx * transform_parameters_stride] += log(pdf);
	}

	GroupMemoryBarrierWithGroupSync();

	const float v_norm_sqr = v_norm * v_norm;
	const float w_norm_sqr = w_norm * w_norm;
	const float wb_sqr = bin_width_b * bin_width_b;
	
	const float exp_vi = exp_v;
	const float exp_wi = exp_w;
	const float dw_norm_dwi = exp_wi;
	float dv_norm_dwi = 0.0f;
	for (uint32_t j = 0; j < ::bin_count; j++)
	{
		const float exp_wj = exp_w_groupshared[j];
		const float dexp_wj_dwi = (j == intra_elem_idx ? exp_wi : 0.0f);
		const float dwj_dwi = (w_norm * dexp_wj_dwi - exp_wj * dw_norm_dwi) / w_norm_sqr;
		dv_norm_dwi += v_median_groupshared[j] * dwj_dwi;
	}
	const float dvb_dwi = (-exp_vb * dv_norm_dwi) / v_norm_sqr;
	const float dvb_next_dwi = (-exp_vb_next * dv_norm_dwi) / v_norm_sqr;
	const float dvb_len_dwi = dvb_next_dwi - dvb_dwi;
	const float dwb_numerator_dwi = (intra_elem_idx == bin_index ? exp_wb : 0.0f);
	const float dwb_dwi = (w_norm * dwb_numerator_dwi - exp_wb * dw_norm_dwi) / w_norm_sqr;
	float dw_left_integral_dwi = 0.0f;
	for (uint32_t j = 0; j < bin_index_groupshared; j++)
	{
		const float exp_wj = exp_w_groupshared[j];
		const float dwj_numerator_dwi = (j == intra_elem_idx ? exp_wi : 0.0f);
		const float dwj_dwi = (w_norm * dwj_numerator_dwi - exp_wj * dw_norm_dwi) / w_norm_sqr;
		dw_left_integral_dwi += dwj_dwi;
	}
	const float dalpha_numerator_dwi = -dw_left_integral_dwi;
	const float dalpha_dwi = (bin_width_b * dalpha_numerator_dwi - alpha_numerator_groupshared * dwb_dwi) / wb_sqr;
	const float dpdf_dwi = dvb_dwi + dvb_len_dwi * alpha_groupshared + vb_len_groupshared * dalpha_dwi;
	const float dlog_pdf_dwi = dpdf_dwi * inv_pdf_groupshared;
	gradients_buffer[inter_elem_idx * transform_parameters_stride + intra_elem_idx] = ELEMENT_TYPE(intra_elem_idx < ::bin_count ? ::clamp_gradient(dlog_pdf_dwi, w_norm, ::bin_count) : 0.0f);

	const float dvb_numerator_dvi = intra_elem_idx == bin_index_groupshared ? exp_vb : 0.0f;
	const float dvb_next_numerator_dvi = intra_elem_idx == bin_index_groupshared + 1 ? exp_vb_next : 0.0f;
	const float dv_norm_dvi = ((intra_elem_idx == ::bin_count ? 0.0f : bin_width) + (intra_elem_idx == 0 ? 0.0f : bin_width_cumsum[intra_elem_idx - 1])) * exp_vi / 2.0f;
	const float dvb_dvi = (v_norm * dvb_numerator_dvi - exp_vb * dv_norm_dvi) / v_norm_sqr;
	const float dvb_next_dvi = (v_norm * dvb_next_numerator_dvi - exp_vb_next * dv_norm_dvi) / v_norm_sqr;
	const float dvb_len_dvi = dvb_next_dvi - dvb_dvi;
	const float dpdf_dvi = dvb_dvi + alpha_groupshared * dvb_len_dvi;
	const float dlog_pdf_dvi = dpdf_dvi * inv_pdf_groupshared;
	gradients_buffer[inter_elem_idx * transform_parameters_stride + ::bin_count_padded + intra_elem_idx] = ELEMENT_TYPE(::clamp_gradient(dlog_pdf_dvi, v_sum, ::bin_count + 1));
}

[numthreads(WORKGROUP_SIZE, 1, 1)]
[shader("compute")]
void inverse_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[BIN_COUNT];
	float v[BIN_COUNT + 1];
	float v_medians[BIN_COUNT];
	float w_norm = 0.0f;
	float v_norm = 0.0f;
	float width_left_integral = 0.0f;
	float height_left_integral = 0.0f;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		w_norm += w[i];
	}
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = w[i] / w_norm;
	}
	v[0] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + 0]);
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		v[i + 1] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + i + 1]);
		v_medians[i] = (v[i + 0] + v[i + 1]) / 2.0f;
		v_norm += v_medians[i] * w[i];
	}
	for (uint32_t i = 0; i <= ::bin_count; i++)
	{
		v[i] = v[i] / v_norm;
	}
	uint bin_index = 0;
	for (uint i = 0; i < ::bin_count - 1; i++)
	{
		const float height_left_integral_next = height_left_integral + (v[i] + v[i + 1]) / 2.0f * w[i];
		if (height_left_integral_next > x_b)
		{
			break;
		}

		bin_index = i + 1;
		width_left_integral += w[i];
		height_left_integral = height_left_integral_next;
	}

	const float vb_len = v[bin_index + 1] - v[bin_index];
	const float a = 0.5f * vb_len * w[bin_index];
	const float b = v[bin_index] * w[bin_index];
	const float c = height_left_integral - x_b;
	const float discriminant = b * b - 4 * a * c;
	//const float alpha = (-b + sqrt(discriminant)) / (2 * a);
	const float alpha = (2 * c) / (-b - sqrt(discriminant));
	const float y_b = alpha * w[bin_index] + width_left_integral;
	const float pdf = v[bin_index] + alpha * vb_len;
	const float logabsdet = -log(pdf);
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += logabsdet;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void forward_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[BIN_COUNT];
	float v[BIN_COUNT + 1];
	float v_medians[BIN_COUNT];
	float w_norm = 0.0f;
	float v_norm = 0.0f;
	float v_sum = 0.0f;
	float width_left_integral = 0.0f;
	float height_left_integral = 0.0f;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		w_norm += w[i];
	}
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = w[i] / w_norm;
	}
	v[0] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + 0]);
	v_sum += v[0];
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		v[i + 1] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + i + 1]);
		v_sum += v[i + 1];
		v_medians[i] = (v[i + 0] + v[i + 1]) / 2.0f;
		v_norm += v_medians[i] * w[i];
	}
	for (uint32_t i = 0; i <= ::bin_count; i++)
	{
		v[i] = v[i] / v_norm;
	}
	uint bin_index = 0;
	for (uint i = 0; i < ::bin_count - 1; i++)
	{
		const float w_left_integral_next = width_left_integral + w[i];
		if (w_left_integral_next > x_b)
		{
			break;
		}

		bin_index = i + 1;
		width_left_integral = w_left_integral_next;
		height_left_integral += (v[i] + v[i + 1]) / 2.0f * w[i];
	}

	const float alpha_numerator = x_b - width_left_integral;
	const float alpha = alpha_numerator / w[bin_index];
	const float vb_len = v[bin_index + 1] - v[bin_index];
	const float y_b = alpha * alpha / 2.0f * vb_len * w[bin_index] + alpha * v[bin_index] * w[bin_index] + height_left_integral;
	const float pdf = v[bin_index] + alpha * vb_len;
	const float inv_pdf = 1.0f / pdf;
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += log(pdf);
}

[numthreads(128, 1, 1)]
[shader("compute")]
void inverse_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	float w[BIN_COUNT];
	float v[BIN_COUNT + 1];
	float v_medians[BIN_COUNT];
	float w_norm = 0.0f;
	float v_norm = 0.0f;
	float width_left_integral = 0.0f;
	float height_left_integral = 0.0f;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + i]);
		w_norm += w[i];
	}
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		w[i] = w[i] / w_norm;
	}
	v[0] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + 0]);
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		v[i + 1] = exp(transform_parameters_buffer[global_invocation_id * transform_parameters_stride + ::bin_count_padded + i + 1]);
		v_medians[i] = (v[i + 0] + v[i + 1]) / 2.0f;
		v_norm += v_medians[i] * w[i];
	}
	for (uint32_t i = 0; i <= ::bin_count; i++)
	{
		v[i] = v[i] / v_norm;
	}
	uint bin_index = 0;
	for (uint i = 0; i < ::bin_count - 1; i++)
	{
		const float height_left_integral_next = height_left_integral + (v[i] + v[i + 1]) / 2.0f * w[i];
		if (height_left_integral_next > x_b)
		{
			break;
		}

		bin_index = i + 1;
		width_left_integral += w[i];
		height_left_integral = height_left_integral_next;
	}

	const float vb_len = v[bin_index + 1] - v[bin_index];
	const float a = 0.5f * vb_len * w[bin_index];
	const float b = v[bin_index] * w[bin_index];
	const float c = height_left_integral - x_b;
	const float discriminant = b * b - 4 * a * c;
	//const float alpha = (-b + sqrt(discriminant)) / (2 * a);
	const float alpha = (2 * c) / (-b - sqrt(discriminant));
	const float y_b = alpha * w[bin_index] + width_left_integral;
	const float pdf = v[bin_index] + alpha * vb_len;
	const float logabsdet = -log(pdf);
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * transform_parameters_stride] += logabsdet;
}