#include "algorithm/threadgroup/scan.hlsl"
#include "algorithm/threadgroup/sum.hlsl"
#include "gpu_log.hlsl"
#include "linalg.hlsl"
#include "numbers.hlsl"

[[vk::constant_id(0)]] const int x_a_layout = 0;
[[vk::constant_id(1)]] const uint32_t x_a_row_index = 0;
[[vk::constant_id(2)]] const uint32_t x_a_row_count = 1;
[[vk::constant_id(3)]] const uint bin_count = 1;

static const float min_bin_height = 1e-3f;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);

namespace framework
{
    struct sum_executor
    {
        static ::framework::sum_executor create(vector<float, 1> data)
        {
            ::framework::sum_executor sum_executor;
            sum_executor.data = data;
            return sum_executor;
        }

        inline vector<float, 1> load(uint global_index, uint local_index)
        {
            return this.data[local_index];
        }

        inline void store(vector<float, 1> data)
        {
            this.data = data;
        }

        vector<float, 1> data;
    };

    struct scan_executor
    {
        static ::framework::scan_executor create(vector<float, 1> data)
        {
            ::framework::scan_executor scan_executor;
            scan_executor.data = data;
            return scan_executor;
        }

        inline uint get_vectorized_size()
        {
            return BIN_COUNT;
        }

        inline vector<float, 1> load_local(uint global_index, uint local_index)
        {
			return this.data_local;
        }

        inline void store_local(uint global_index, uint local_index, vector<float, 1> data)
        {
			this.data_local = data;
        }
		
        inline vector<float, 1> load(uint global_index, uint local_index)
        {
            return this.data;
        }

        inline void store(uint global_index, uint local_index, vector<float, 1> data)
        {
            this.data = data;
        }

        vector<float, 1> data;
        vector<float, 1> data_local;
    };
}

groupshared::framework::algorithm::threadgroup::sum<float, 1u, BIN_COUNT, 1u> threadgroup_sum;
groupshared::framework::algorithm::threadgroup::scan<float, 1u, BIN_COUNT, 1u> threadgroup_scan;

groupshared float exp_h_b;
groupshared float absdet;

[numthreads(BIN_COUNT, 1, 1)]
[shader("compute")]
void forward_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = group_thread_id.x;
	const uint inter_elem_idx = group_id.x;
	if (inter_elem_idx >= size)
	{
		return;
	}

	const float min_bin_height_weight = 1 - min_bin_height * ::bin_count;
	const uint32_t x_index = inter_elem_idx * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const float w = 1.0f / ::bin_count;
    const float h = transform_parameters_buffer[global_invocation_id];
    const float exp_h = exp(h);

    ::framework::sum_executor sum_executor = ::framework::sum_executor::create(exp_h);
    ::threadgroup_sum.workgroup_sum(group_thread_id.x, sum_executor);

    const float h_norm = sum_executor.data;
	const float inv_h_norm = 1.0f / h_norm;
    const float bin_height = min_bin_height + min_bin_height_weight * (exp_h * inv_h_norm);

    const uint32_t bin_index = clamp(floor(::bin_count * x_b), 0, ::bin_count - 1);
	const float slope = bin_height;

	if (intra_elem_idx == bin_index)
	{
		exp_h_b = exp_h;
		absdet = slope * ::bin_count;
	}

	::framework::scan_executor scan_executor = ::framework::scan_executor::create(bin_height);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, scan_executor);

	const float dloss_norm_difference = ::bin_count - h_norm;
	const float dloss_norm_weight = 1e-4f;
	const float dh_norm_dhi = exp_h;
    const float h_norm_sqr = h_norm * h_norm;
	const float dhb_exp_dhi = bin_index == intra_elem_idx ? exp_h : 0.0f;
	const float dhb_dhi = min_bin_height_weight * ((h_norm * dhb_exp_dhi - exp_h_b * dh_norm_dhi) / h_norm_sqr);
	const float dslope_dhi = dhb_dhi;
	const float dabsdet_dhi = dslope_dhi * ::bin_count;
	const float dlogabsdet_dhi = dabsdet_dhi / absdet;
	//const float dlogabsdet_dhi = (bin_index == intra_elem_idx ? h_norm - exp_h : -exp_h) / h_norm;
	// loss that keeps h_norm within the ::bin_count range to prevent over- or underflow
	const float dloss_norm_dhi = dloss_norm_weight * sign(dloss_norm_difference) * dh_norm_dhi;
	gradients_buffer[global_invocation_id] = ELEMENT_TYPE(dlogabsdet_dhi + dloss_norm_dhi);

    if (intra_elem_idx == bin_index)
    {
		const float bin_height_left_integral = scan_executor.data;
		const float alpha = x_b * ::bin_count - floor(::bin_count * x_b);
		const float y_b = alpha * slope + bin_height_left_integral;
		const float logabsdet = log(absdet);
        x_buffer[x_a_index] = y_b;
        x_buffer[x_b_index] = x_a;
        const uint x_a_column_index = inter_elem_idx;
        const uint x_a_column_count = size;
        const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
        x_a_buffer[y_b_index] = y_b;
        log_jacobian_buffer[inter_elem_idx * ::bin_count] += logabsdet;
    }
}

groupshared float bin_height_cumsum[BIN_COUNT];
groupshared float bin_height_b;

[numthreads(BIN_COUNT, 1, 1)]
[shader("compute")]
void inverse_main(uint3 dispatch_thread_id : SV_DispatchThreadID, uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint intra_elem_idx = group_thread_id.x;
	const uint inter_elem_idx = group_id.x;
	if (inter_elem_idx >= size)
	{
		return;
	}

	const float min_bin_height_weight = 1 - min_bin_height * ::bin_count;
	const uint32_t x_index = inter_elem_idx * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const float w = 1.0f / ::bin_count;
	const float h = transform_parameters_buffer[global_invocation_id];
	const float exp_h = exp(h);

	::framework::sum_executor sum_executor = ::framework::sum_executor::create(exp_h);
	::threadgroup_sum.workgroup_sum(group_thread_id.x, sum_executor);

	const float h_norm = sum_executor.data;
	const float inv_h_norm = 1.0f / h_norm;
	const float bin_height = min_bin_height + min_bin_height_weight * (exp_h * inv_h_norm);

	::framework::scan_executor scan_executor = ::framework::scan_executor::create(bin_height);
	::threadgroup_scan.workgroup_exclusive_prefix_sum(group_thread_id.x, scan_executor);

	bin_height_cumsum[intra_elem_idx] = scan_executor.data;

	GroupMemoryBarrierWithGroupSync();

	const float bin_height_left_integral = scan_executor.data;
	// Note: bin_height_left_integral_next = bin_height_left_integral + bin_height leads to false positive evaluations, where two neighboring threads may both detect the inv_bin_index for themselves.
	// It is also faster than the solution involving InterlockedMin(inv_bin_index) to the shared memory.
	const float bin_height_left_integral_next = intra_elem_idx == ::bin_count - 1 ? 1.0f : bin_height_cumsum[intra_elem_idx + 1];
	const uint32_t inv_bin_index = bin_height_left_integral <= x_b && (bin_height_left_integral_next > x_b || intra_elem_idx == ::bin_count - 1) ? intra_elem_idx : -1;

	if (inv_bin_index != -1)
	{
		exp_h_b = exp_h;
		bin_height_b = bin_height;
	}
	
	GroupMemoryBarrierWithGroupSync();
	
	const float slope = bin_height_b * ::bin_count;
	const float inv_h_norm_sqr = 1.0f / (h_norm * h_norm);
	const float exp_hi = exp_h;
	const float dh_norm_dhi = exp_hi;
	const float dexp_hb_dhi = (inv_bin_index == intra_elem_idx ? exp_hi : 0.0f);
	const float dhb_dhi = min_bin_height_weight * ((h_norm * dexp_hb_dhi - exp_h_b * dh_norm_dhi) * inv_h_norm_sqr);
	const float dslope_dhi = dhb_dhi * ::bin_count;
	const float dlogabsdet_dhi = -dslope_dhi / slope;
	gradients_buffer[global_invocation_id] = ELEMENT_TYPE(dlogabsdet_dhi);

	if (intra_elem_idx == inv_bin_index)
	{
		const float slope = bin_height * ::bin_count;
		const float offset = bin_height_left_integral + bin_height - bin_height * (inv_bin_index + 1);
		const float y_b = (x_b - offset) / slope;
		const float logabsdet = -log(slope);
		x_buffer[x_a_index] = y_b;
		x_buffer[x_b_index] = x_a;
		const uint x_a_column_index = inter_elem_idx;
		const uint x_a_column_count = size;
		const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
		x_a_buffer[y_b_index] = y_b;
		log_jacobian_buffer[inter_elem_idx * ::bin_count] += logabsdet;
	}
}

[numthreads(128, 1, 1)]
[shader("compute")]
void forward_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const float min_bin_height_weight = 1 - min_bin_height * ::bin_count;
	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const float w = 1.0f / ::bin_count;
	float q[BIN_COUNT];
	float h_norm = 0.0f;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		q[i] = exp(transform_parameters_buffer[global_invocation_id * ::bin_count + i]);
		h_norm += q[i];
	}
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		q[i] = min_bin_height + min_bin_height_weight * (q[i] / h_norm);
	}
	const uint32_t bin_index = clamp(floor(::bin_count * x_b), 0, ::bin_count - 1);
	float bin_height_left_integral = 0.0f;
	for (int32_t i = 0; i < bin_index; i++)
	{
		bin_height_left_integral += q[i];
	}
	const float alpha = x_b * ::bin_count - floor(::bin_count * x_b);
	const float slope = q[bin_index];
	const float y_b = alpha * slope + bin_height_left_integral;
	const float absdet = slope * ::bin_count;
	const float logabsdet = log(absdet);
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * ::bin_count] += logabsdet;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void inverse_no_grad_main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const float min_bin_height_weight = 1 - min_bin_height * ::bin_count;
	const uint32_t x_index = global_invocation_id * 2;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const float w = 1.0f / ::bin_count;
	float q[BIN_COUNT];
	float h_norm = 0.0f;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		q[i] = exp(transform_parameters_buffer[global_invocation_id * ::bin_count + i]);
		h_norm += q[i];
	}
	const float inv_h_norm = 1.0f / h_norm;
	for (uint32_t i = 0; i < ::bin_count; i++)
	{
		q[i] = min_bin_height + min_bin_height_weight * (q[i] * inv_h_norm);
	}

	uint32_t inv_bin_index = 0;
	float bin_height_left_integral = 0.0f;
	for (uint i = 0; i < ::bin_count - 1; i++)
	{
		const float bin_height_left_integral_next = bin_height_left_integral + q[i];
		if (bin_height_left_integral_next > x_b)
		{
			break;
		}

		inv_bin_index = i + 1;
		bin_height_left_integral = bin_height_left_integral_next;
	}

	const float slope = q[inv_bin_index] * ::bin_count;
	const float offset = bin_height_left_integral + q[inv_bin_index] - q[inv_bin_index] * (inv_bin_index + 1);
	const float y_b = (x_b - offset) / slope;
	const float logabsdet = -log(slope);
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	const uint x_a_column_index = global_invocation_id;
	const uint x_a_column_count = size;
	const uint y_b_index = (::framework::linalg::matrix_layout)x_a_layout == ::framework::linalg::matrix_layout::row_major_ ? x_a_column_index + x_a_row_index * x_a_column_count : x_a_row_index + x_a_column_index * x_a_row_count;
	x_a_buffer[y_b_index] = y_b;
	log_jacobian_buffer[global_invocation_id * ::bin_count] += logabsdet;
}