#include "gpu/utility.hpp"
#include "normalizing_flows/transform/piecewise_rational_quadratic.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include <string>

::framework::normalizing_flows::transforms::piecewise_rational_quadratic::piecewise_rational_quadratic(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type component_type, ::std::uint32_t bin_count, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::linalg::matrix_layout x_a_layout, ::std::uint32_t x_a_row_index, ::std::uint32_t x_a_row_count)
{
	::std::uint32_t const transform_parameters_size = bin_count * 3 - 1;
	::std::uint32_t const transform_parameters_stride = ::std::bit_ceil(transform_parameters_size);
	::std::uint32_t const workgroup_size = ::std::bit_ceil(bin_count);
	char const * type_name = ::framework::gpu::utility::get_component_type_name(component_type);
	::std::string const bin_count_string = ::std::to_string(bin_count);
	::std::string const workgroup_size_string = ::std::to_string(workgroup_size);

	::framework::resource::preprocessor_define const preprocessor_defines[]
	{
		{ "ELEMENT_TYPE", type_name },
		{ "BIN_COUNT", bin_count_string.c_str() },
		{ "WORKGROUP_SIZE", workgroup_size_string.c_str() },
		{ "GPU_LOG_DESCRIPTOR_SET_INDEX", "2" },
		{ "GPU_LOG_REGISTER_SPACE", "space2" }
	};

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/transform/piecewise_rational_quadratic.comp.hlsl";
		hlsl_source_info.source_entry_point = "forward_main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "forward_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->forward_transform_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/transform/piecewise_rational_quadratic.comp.hlsl";
		hlsl_source_info.source_entry_point = "inverse_main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "inverse_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->inverse_transform_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/transform/piecewise_rational_quadratic.comp.hlsl";
		hlsl_source_info.source_entry_point = "forward_no_grad_main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "forward_no_grad_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->forward_transform_no_grad_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/transform/piecewise_rational_quadratic.comp.hlsl";
		hlsl_source_info.source_entry_point = "inverse_no_grad_main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "inverse_no_grad_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->inverse_transform_no_grad_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) },
		{ 2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t) },
		{ 3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t) },
		{ 4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t) },
		{ 5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t) }
	};

	::std::uint32_t const specialization_data[]
	{
		static_cast<::std::int32_t>(x_a_layout),
		x_a_row_index,
		x_a_row_count,
		bin_count,
		workgroup_size,
		transform_parameters_stride
	};

	::framework::gpu::specialization_info specialization_info;
	specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_info.map_entries = specialization_map_entries;
	specialization_info.data_size = sizeof(specialization_data);
	specialization_info.data = specialization_data;

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->forward_transform_shader_module;
		pipeline_shader_stage_create_info.name = "forward_main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->forward_transform_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->inverse_transform_shader_module;
		pipeline_shader_stage_create_info.name = "inverse_main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->inverse_transform_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->forward_transform_no_grad_shader_module;
		pipeline_shader_stage_create_info.name = "forward_no_grad_main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->forward_transform_no_grad_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->inverse_transform_no_grad_shader_module;
		pipeline_shader_stage_create_info.name = "inverse_no_grad_main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->inverse_transform_no_grad_pipeline));
	}
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->forward_transform_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 3, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(batch_size, 1, 1);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->forward_transform_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::inverse(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->inverse_transform_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(batch_size, 1, 1);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::inverse_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->inverse_transform_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::forward_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->forward_transform_no_grad_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(::framework::numeric::div_round_up(batch_size, 128u), 1, 1);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::forward_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->forward_transform_no_grad_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::inverse_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->inverse_transform_no_grad_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(::framework::numeric::div_round_up(batch_size, 128u), 1, 1);
}

void ::framework::normalizing_flows::transforms::piecewise_rational_quadratic::inverse_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->inverse_transform_no_grad_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, pipeline_layout, 0, 2, descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}