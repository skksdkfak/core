[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);


[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint i = dispatch_thread_id.x;
	if (i >= size)
	{
		return;
	}

	const uint32_t stride = 16;
	const uint32_t x_index = i * 2;
	const uint32_t index = i * stride;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const ELEMENT_TYPE s = transform_parameters_buffer[index + 0];
	const ELEMENT_TYPE t = transform_parameters_buffer[index + 1];
	const ELEMENT_TYPE x_a = x_buffer[x_a_index];
	const ELEMENT_TYPE x_b = x_buffer[x_b_index];
	const ELEMENT_TYPE x_b_translated = x_b - t;
	const ELEMENT_TYPE y_b = exp(-s) * x_b_translated;
	x_buffer[x_a_index] = y_b;
	x_buffer[x_b_index] = x_a;
	x_a_buffer[index] = y_b;
	log_jacobian_buffer[index] += -s;

	const bool compute_gradient = true;
	if (compute_gradient)
	{
		// assuming latent_prior is a standard normal distribution
		const float dl_ds = x_b_translated * x_b_translated * exp(-2.0f * s) - 1.0f;
		const float dl_dt = exp(-2.0f * s) * x_b_translated;
		gradients_buffer[index + 0] = ELEMENT_TYPE(dl_ds);
		gradients_buffer[index + 1] = ELEMENT_TYPE(dl_dt);
	}
}
