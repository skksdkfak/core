#pragma once

#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "linalg.hpp"
#include "normalizing_flows/transform.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::normalizing_flows::transforms
{
	class piecewise_linear : public ::framework::normalizing_flows::transform
	{
	public:
		piecewise_linear(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type component_type, ::std::uint32_t bin_count, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::linalg::matrix_layout x_a_layout, ::std::uint32_t x_a_row_index, ::std::uint32_t x_a_row_count);

		void forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) override;

		void forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

		void inverse(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) override;

		void inverse_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;
		
		void forward_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) override;

		void forward_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

		void inverse_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) override;

		void inverse_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

	private:
		::framework::gpu::shader_module * forward_transform_shader_module;
		::framework::gpu::shader_module * inverse_transform_shader_module;
		::framework::gpu::shader_module * forward_transform_no_grad_shader_module;
		::framework::gpu::shader_module * inverse_transform_no_grad_shader_module;
		::framework::gpu::pipeline * forward_transform_pipeline;
		::framework::gpu::pipeline * inverse_transform_pipeline;
		::framework::gpu::pipeline * forward_transform_no_grad_pipeline;
		::framework::gpu::pipeline * inverse_transform_no_grad_pipeline;
	};
}