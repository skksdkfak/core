#include "gpu/utility.hpp"
#include "linalg.hpp"
#include "normalizing_flows/normalizing_flow.hpp"
#include "normalizing_flows/transform.hpp"
#include "normalizing_flows/transform/affine.hpp"
#include "normalizing_flows/transform/piecewise_linear.hpp"
#include "normalizing_flows/transform/piecewise_rational_quadratic.hpp"
#include "normalizing_flows/transform/piecewise_quadratic.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include <bit>
#include <utility>

::framework::gpu::component_type constexpr grid_gradient_type = ::framework::gpu::component_type::float32;
::std::uint32_t const feature_count = 32u;
::framework::nn::grid::grid_type const grid_type = ::framework::nn::grid::grid_type::hash;
::framework::nn::grid::hash_type const hash_type = ::framework::nn::grid::hash_type::coherent_prime;
::framework::nn::grid::interpolation_type const interpolation_type = ::framework::nn::grid::interpolation_type::smoothstep;
::std::uint32_t const log2_hashmap_size = 19u;
::std::uint32_t const base_resolution = 1u << 7;
float const per_level_scale = 1.5f;
bool const stochastic_interpolation = true;
::std::uint32_t const features_per_level = 4u;

namespace framework::local
{
	namespace
	{
		struct concatenate_features_push_constants
		{
			::std::uint32_t offset;
			::std::uint32_t pad0;
		};
		
		struct calc_log_prob_push_constants
		{
			::std::uint32_t stride;
			::std::uint32_t pad0;
		};

		struct chain_loss_grad_push_constants
		{
			::std::uint32_t dims;
			::std::uint32_t stride;
		};

		struct initialize_dispatch_indirect_command_push_constants
		{
			::uint32_t batch_size_offset;
			::framework::gpu::bool32_t inference;
		};
	}
}

::framework::normalizing_flows::normalizing_flow::normalizing_flow(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log, ::framework::gpu::descriptor_pool * descriptor_pool, ::framework::normalizing_flows::transform_type transform_type, ::std::uint32_t coupling_layer_count, ::std::uint32_t spatial_feature_count, ::std::uint32_t spatial_feature_stride, ::std::uint32_t one_blob_encode_feature_count, ::std::uint32_t one_blob_encode_feature_stride, ::std::uint32_t hidden_width, ::std::uint32_t hidden_layer_count, float learning_rate, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, ::std::uint32_t batch_alignment, ::std::uint32_t inference_batch_alignment, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type pdf_type, ::framework::gpu::component_type target_type, ::framework::gpu::component_type accumulator_type, ::framework::gpu::buffer * spatial_features_buffer, ::framework::gpu::buffer * one_blob_encode_features_buffer, ::framework::gpu::buffer * pdf_buffer, ::framework::gpu::buffer * target_buffer, ::framework::gpu::buffer * batch_size_buffer) :
	gpu_context(gpu_context), resource_manager(resource_manager), profiler(profiler), descriptor_pool(descriptor_pool), transform_type(transform_type), coupling_layer_count(coupling_layer_count)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[5];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// x_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// features_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// log_jacobian_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// loss_gradient_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->normalizing_flow_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// transform_parameters_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// gradients_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->coupling_layers_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// batch_size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->normalizing_flow_descriptor_set_layout,
			this->coupling_layers_descriptor_set_layout,
			resource_manager->default_resources.gpu_log_device_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = 16;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->initialize_dispatch_indirect_command_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::framework::local::initialize_dispatch_indirect_command_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline_layout));
	}

	this->coupling_layers.resize(this->coupling_layer_count);

	char const * type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);
	::std::size_t const type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::uint32_t const one_blob_encoding_bins = 4u;
	::std::uint32_t const x_a_one_blob_encoding_bins = 8u;
	::std::uint32_t const one_blob_encoded_input_size = one_blob_encode_feature_count * one_blob_encoding_bins;
	::std::uint32_t const x_a_one_blob_encoded_input_size = 1 * x_a_one_blob_encoding_bins;
	this->features_stride = ::std::bit_ceil(feature_count + one_blob_encoded_input_size + 1 + x_a_one_blob_encoded_input_size);
	::framework::linalg::matrix_layout const input_layout = ::framework::linalg::matrix_layout::column_major;
	::framework::linalg::matrix_layout const neural_network_input_layout = ::framework::linalg::matrix_layout::row_major;
	::framework::linalg::matrix_layout const output_layout = ::framework::linalg::matrix_layout::column_major;
	::framework::linalg::matrix_layout const x_a_layout = neural_network_input_layout;
	::std::uint32_t const x_a_row_index = feature_count + one_blob_encoded_input_size;
	::std::uint32_t const x_a_row_count = this->features_stride;
	::std::uint32_t const x_a_encoded_row_offset = feature_count + one_blob_encoded_input_size + 1;
	float const weight_clipping_magnitude = 0.0f;
	float loss_scale;
	switch (parameter_type)
	{
	case ::framework::gpu::component_type::float16:
		loss_scale = 128.0f;
		break;
	case ::framework::gpu::component_type::float32:
		loss_scale = 1.0f;
		break;
	default:
		assert(!"Error: Unsupported prediction_type.");
		break;
	}

	switch (transform_type)
	{
	case ::framework::normalizing_flows::transform_type::affine:
	{
		this->output_size = 2;
		this->output_stride = 16;
		this->coupling_transform = ::std::make_unique<::framework::normalizing_flows::transforms::affine>(this->gpu_context, resource_manager, parameter_type, this->pipeline_layout, x_a_layout, x_a_row_index, x_a_row_count);
	}
		break;
	case ::framework::normalizing_flows::transform_type::piecewise_linear:
	{
		::std::uint32_t const k = 64;
		this->output_size = k;
		this->output_stride = k;
		this->coupling_transform = ::std::make_unique<::framework::normalizing_flows::transforms::piecewise_linear>(this->gpu_context, resource_manager, parameter_type, k, this->pipeline_layout, x_a_layout, x_a_row_index, x_a_row_count);
	}
		break;
	case ::framework::normalizing_flows::transform_type::piecewise_quadratic:
	{
		::std::uint32_t const k = 63;
		::std::uint32_t const wv_size = k * 2 + 1;
		::std::uint32_t const wv_size_padded = ::std::bit_ceil(wv_size);
		this->output_size = wv_size_padded; // The parameters are not tightly packed, so we must set the size that spans to the last parameter.
		this->output_stride = wv_size_padded;
		this->coupling_transform = ::std::make_unique<::framework::normalizing_flows::transforms::piecewise_quadratic>(this->gpu_context, resource_manager, parameter_type, k, this->pipeline_layout, x_a_layout, x_a_row_index, x_a_row_count);
	}
		break;
	case ::framework::normalizing_flows::transform_type::piecewise_rational_quadratic:
	{
		::std::uint32_t const k = 32;
		::std::uint32_t const whd_size = k * 3 - 1;
		::std::uint32_t const whd_size_padded = ::std::bit_ceil(whd_size);
		this->output_size = whd_size;
		this->output_stride = whd_size_padded;
		this->coupling_transform = ::std::make_unique<::framework::normalizing_flows::transforms::piecewise_rational_quadratic>(this->gpu_context, resource_manager, parameter_type, k, this->pipeline_layout, x_a_layout, x_a_row_index, x_a_row_count);
	}
		break;
	default:
		::std::unreachable();
		break;
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 2 * ::std::max(max_batch_size, max_inference_batch_size) * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::vertex_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->x_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->x_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->x_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->x_buffer;
		bind_buffer_memory_info.memory = this->x_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = max_batch_size * this->features_stride * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dloss_dnetwork_input_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dloss_dnetwork_input_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dloss_dnetwork_input_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dloss_dnetwork_input_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dloss_dnetwork_input_buffer;
		bind_buffer_memory_info.memory = this->dloss_dnetwork_input_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->features_stride * ::std::max(max_batch_size, max_inference_batch_size) * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->encoded_features_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->encoded_features_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->encoded_features_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->encoded_features_buffer;
		bind_buffer_memory_info.memory = this->encoded_features_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->output_stride * ::std::max(max_batch_size, max_inference_batch_size) * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->log_jacobian_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->log_jacobian_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->log_jacobian_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->log_jacobian_buffer;
		bind_buffer_memory_info.memory = this->log_jacobian_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->output_stride * max_batch_size * type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->loss_gradient_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->loss_gradient_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->loss_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->loss_gradient_buffer;
		bind_buffer_memory_info.memory = this->loss_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->output_stride * max_batch_size * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->loss_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->loss_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &loss_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->loss_buffer;
		bind_buffer_memory_info.memory = loss_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	this->xavier_uniform = ::std::make_unique<::framework::algorithm::xavier_uniform>(this->gpu_context, resource_manager, ::framework::gpu::component_type::float32, parameter_type);
	this->one_blob_execution_policy = ::std::make_unique<::framework::nn::one_blob::execution_policy>(this->gpu_context, resource_manager, command_buffer_manager, parameter_type, one_blob_encoding_bins, one_blob_encode_feature_count, ::framework::linalg::matrix_layout::column_major, neural_network_input_layout, 0, one_blob_encode_feature_stride, feature_count, this->features_stride);
	this->one_blob_encoding = ::std::make_unique<::framework::nn::one_blob::encoding>(this->one_blob_execution_policy.get());
	this->x_a_one_blob_execution_policy = ::std::make_unique<::framework::nn::one_blob::execution_policy>(this->gpu_context, resource_manager, command_buffer_manager, parameter_type, x_a_one_blob_encoding_bins, 1, neural_network_input_layout, neural_network_input_layout, x_a_row_index, this->features_stride, x_a_encoded_row_offset, this->features_stride);
	this->x_a_one_blob_encoding = ::std::make_unique<::framework::nn::one_blob::encoding>(this->x_a_one_blob_execution_policy.get());
	this->encoding_execution_policy = ::std::make_unique<::framework::nn::grid::execution_policy>(this->gpu_context, resource_manager, command_buffer_manager, parameter_type, grid_gradient_type, grid_type, hash_type, interpolation_type, ::std::max(max_batch_size, max_inference_batch_size), feature_count, log2_hashmap_size, base_resolution, per_level_scale, stochastic_interpolation, features_per_level, spatial_feature_count, spatial_feature_stride);
	this->fully_fused_mlp_execution_policy = ::std::make_unique<::framework::nn::fully_fused_mlp::execution_policy>(this->gpu_context, resource_manager, gpu_log, parameter_type, accumulator_type, neural_network_input_layout, output_layout, this->features_stride, this->output_size, this->features_stride, this->output_stride, hidden_width, hidden_layer_count, ::framework::nn::activation::sigmoid, ::framework::nn::activation::none, batch_alignment, inference_batch_alignment, max_batch_size, max_inference_batch_size, true);
	::std::uint32_t const parameter_count = this->fully_fused_mlp_execution_policy->get_parameter_count();

	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->coupling_layers[i].encoding = ::std::make_unique<::framework::nn::grid::encoding>(this->encoding_execution_policy.get(), this->gpu_context, resource_manager, parameter_type, grid_gradient_type);
		this->coupling_layers[i].neural_network = ::std::make_unique<::framework::nn::fully_fused_mlp::network>(*this->fully_fused_mlp_execution_policy, this->gpu_context, resource_manager, gpu_log, parameter_type, accumulator_type, max_batch_size, max_inference_batch_size, this->encoded_features_buffer, this->dloss_dnetwork_input_buffer, this->descriptor_pool);
		this->coupling_layers[i].optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, resource_manager, parameter_type, parameter_type, parameter_count, true, false, weight_clipping_magnitude, loss_scale, learning_rate, 1e-07f);
		this->coupling_layers[i].encoding_optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, resource_manager, parameter_type, grid_gradient_type, this->encoding_execution_policy->get_parameter_count(), false, false, weight_clipping_magnitude, loss_scale, learning_rate, 0.0f);
	}
	this->loss = ::std::make_unique<::framework::nn::cross_entropy>(this->gpu_context, resource_manager, parameter_type, pdf_type, target_type, this->output_stride, 1, pdf_buffer != nullptr, true, loss_scale);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->get_encoding_dispatch_indirect_command_offset() + this->encoding_execution_policy->get_dispatch_indirect_command_size();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dispatch_indirect_command_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dispatch_indirect_command_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dispatch_indirect_command_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dispatch_indirect_command_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dispatch_indirect_command_buffer;
		bind_buffer_memory_info.memory = this->dispatch_indirect_command_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", type_name }
		};

		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/calc_log_prob.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->calc_log_prob_shader_module = resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "ELEMENT_TYPE", type_name },
			{ "GPU_LOG_DESCRIPTOR_SET_INDEX", "2" },
			{ "GPU_LOG_REGISTER_SPACE", "space2" }
		};

		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/chain_loss_grad.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->chain_loss_grad_shader_module = resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "normalizing_flows/initialize_dispatch_indirect_command.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->initialize_dispatch_indirect_command_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->calc_log_prob_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->calc_log_prob_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->chain_loss_grad_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->chain_loss_grad_pipeline));
	}

	{
		::framework::gpu::specialization_map_entry specialization_map_entries[]
		{
			{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) }
		};

		::std::uint32_t specialization_data[::std::size(specialization_map_entries)]
		{
			this->output_stride
		};

		::framework::gpu::specialization_info specialization_info;
		specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
		specialization_info.map_entries = specialization_map_entries;
		specialization_info.data_size = sizeof(specialization_data);
		specialization_info.data = specialization_data;

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->initialize_dispatch_indirect_command_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->initialize_dispatch_indirect_command_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline));
	}

	{
		::std::vector<::framework::gpu::descriptor_set_layout *> descriptor_set_layouts(this->coupling_layer_count * 7u + 9u);
		for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
		{
			descriptor_set_layouts[i * 7 + 0] = this->xavier_uniform->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 1] = this->xavier_uniform->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 2] = this->encoding_execution_policy->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 3] = this->fully_fused_mlp_execution_policy->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 4] = this->coupling_layers[i].optimizer->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 5] = this->coupling_layers[i].encoding_optimizer->get_descriptor_set_layout();
			descriptor_set_layouts[i * 7 + 6] = this->coupling_layers_descriptor_set_layout;
		}
		descriptor_set_layouts[this->coupling_layer_count * 7 + 0] = this->encoding_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 1] = this->fully_fused_mlp_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 2] = this->loss->get_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 3] = this->normalizing_flow_descriptor_set_layout;
		descriptor_set_layouts[this->coupling_layer_count * 7 + 4] = this->initialize_dispatch_indirect_command_descriptor_set_layout;
		descriptor_set_layouts[this->coupling_layer_count * 7 + 5] = this->one_blob_execution_policy->get_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 6] = this->one_blob_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 7] = this->x_a_one_blob_execution_policy->get_descriptor_set_layout();
		descriptor_set_layouts[this->coupling_layer_count * 7 + 8] = this->x_a_one_blob_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();

		::std::vector<::framework::gpu::descriptor_set *> descriptor_sets(::std::size(descriptor_set_layouts));

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts.data();
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets.data()));

		for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
		{
			this->coupling_layers[i].grid_encoding_initialize_parameters_descriptor_set = descriptor_sets[i * 7 + 0];
			this->coupling_layers[i].network_initialize_parameters_descriptor_set = descriptor_sets[i * 7 + 1];
			this->coupling_layers[i].encoding_descriptor_set = descriptor_sets[i * 7 + 2];
			this->coupling_layers[i].network_descriptor_set = descriptor_sets[i * 7 + 3];
			this->coupling_layers[i].optimizer_descriptor_set = descriptor_sets[i * 7 + 4];
			this->coupling_layers[i].encoding_optimizer_descriptor_set = descriptor_sets[i * 7 + 5];
			this->coupling_layers[i].coupling_layers_descriptor_set = descriptor_sets[i * 7 + 6];
		}
		this->encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 0];
		this->network_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 1];
		this->loss_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 2];
		this->normalizing_flow_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 3];
		this->initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 4];
		this->one_blob_encoding_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 5];
		this->one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 6];
		this->x_a_one_blob_encoding_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 7];
		this->x_a_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[this->coupling_layer_count * 7 + 8];
	}

	{
		::std::vector<::framework::gpu::descriptor_buffer_info> descriptor_buffer_infos(7 + this->coupling_layer_count * 2);
		// size_buffer
		descriptor_buffer_infos[0].buffer = batch_size_buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = 4;
		descriptor_buffer_infos[0].stride = 2;
		// x_buffer
		descriptor_buffer_infos[1].buffer = this->x_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;
		// features_buffer
		descriptor_buffer_infos[2].buffer = this->encoded_features_buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;
		// log_jacobian_buffer
		descriptor_buffer_infos[3].buffer = this->log_jacobian_buffer;
		descriptor_buffer_infos[3].offset = 0;
		descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[3].stride = 2;
		// loss_gradient_buffer
		descriptor_buffer_infos[4].buffer = this->loss_gradient_buffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = 2;
		// batch_size_buffer
		descriptor_buffer_infos[5].buffer = batch_size_buffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[5].stride = 2;
		// dispatch_indirect_command_buffer
		descriptor_buffer_infos[6].buffer = this->dispatch_indirect_command_buffer;
		descriptor_buffer_infos[6].offset = 0;
		descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[6].stride = 2;
		for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
		{
			// transform_parameters_buffer
			descriptor_buffer_infos[7 + i].buffer = this->coupling_layers[i].neural_network->get_output_buffer();
			descriptor_buffer_infos[7 + i].offset = 0;
			descriptor_buffer_infos[7 + i].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[7 + i].stride = 2;
			// gradients_buffer
			descriptor_buffer_infos[7 + this->coupling_layer_count + i].buffer = this->coupling_layers[i].neural_network->get_dloss_doutput_buffer();
			descriptor_buffer_infos[7 + this->coupling_layer_count + i].offset = 0;
			descriptor_buffer_infos[7 + this->coupling_layer_count + i].range = ::framework::gpu::whole_size;
			descriptor_buffer_infos[7 + this->coupling_layer_count + i].stride = 2;
		}

		::std::vector<::framework::gpu::write_descriptor_set> write_descriptor_sets(7 + this->coupling_layer_count * 2);
		// size_buffer
		write_descriptor_sets[0].dst_set = this->normalizing_flow_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// x_buffer
		write_descriptor_sets[1].dst_set = this->normalizing_flow_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// features_buffer
		write_descriptor_sets[2].dst_set = this->normalizing_flow_descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// log_jacobian_buffer
		write_descriptor_sets[3].dst_set = this->normalizing_flow_descriptor_set;
		write_descriptor_sets[3].dst_binding = 3;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		// loss_gradient_buffer
		write_descriptor_sets[4].dst_set = this->normalizing_flow_descriptor_set;
		write_descriptor_sets[4].dst_binding = 4;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[4].image_info = nullptr;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[4].texel_buffer_view = nullptr;
		write_descriptor_sets[4].acceleration_structures = nullptr;
		// batch_size_buffer
		write_descriptor_sets[5].dst_set = this->initialize_dispatch_indirect_command_descriptor_set;
		write_descriptor_sets[5].dst_binding = 0;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[5].image_info = nullptr;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
		write_descriptor_sets[5].texel_buffer_view = nullptr;
		write_descriptor_sets[5].acceleration_structures = nullptr;
		// dispatch_indirect_command_buffer
		write_descriptor_sets[6].dst_set = this->initialize_dispatch_indirect_command_descriptor_set;
		write_descriptor_sets[6].dst_binding = 1;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].image_info = nullptr;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[6];
		write_descriptor_sets[6].texel_buffer_view = nullptr;
		write_descriptor_sets[6].acceleration_structures = nullptr;
		for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
		{
			// transform_parameters_buffer
			write_descriptor_sets[7 + i].dst_set = this->coupling_layers[i].coupling_layers_descriptor_set;
			write_descriptor_sets[7 + i].dst_binding = 0;
			write_descriptor_sets[7 + i].dst_array_element = 0;
			write_descriptor_sets[7 + i].descriptor_count = 1;
			write_descriptor_sets[7 + i].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
			write_descriptor_sets[7 + i].image_info = nullptr;
			write_descriptor_sets[7 + i].buffer_info = &descriptor_buffer_infos[7 + i];
			write_descriptor_sets[7 + i].texel_buffer_view = nullptr;
			write_descriptor_sets[7 + i].acceleration_structures = nullptr;
			// gradients_buffer
			write_descriptor_sets[7 + this->coupling_layer_count + i].dst_set = this->coupling_layers[i].coupling_layers_descriptor_set;
			write_descriptor_sets[7 + this->coupling_layer_count + i].dst_binding = 1;
			write_descriptor_sets[7 + this->coupling_layer_count + i].dst_array_element = 0;
			write_descriptor_sets[7 + this->coupling_layer_count + i].descriptor_count = 1;
			write_descriptor_sets[7 + this->coupling_layer_count + i].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
			write_descriptor_sets[7 + this->coupling_layer_count + i].image_info = nullptr;
			write_descriptor_sets[7 + this->coupling_layer_count + i].buffer_info = &descriptor_buffer_infos[7 + this->coupling_layer_count + i];
			write_descriptor_sets[7 + this->coupling_layer_count + i].texel_buffer_view = nullptr;
			write_descriptor_sets[7 + this->coupling_layer_count + i].acceleration_structures = nullptr;
		}
		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets.data(), 0, nullptr);
	}

	::std::uint32_t const batch_size_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

	this->loss->update_descriptor_sets(this->gpu_context, batch_size_buffer, batch_size_offset, this->log_jacobian_buffer, pdf_buffer ? pdf_buffer : target_buffer, target_buffer, this->loss_buffer, this->loss_gradient_buffer, this->loss_descriptor_set);
	this->one_blob_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), batch_size_buffer, this->dispatch_indirect_command_buffer, this->one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set);
	this->one_blob_encoding->update_descriptor_sets(this->gpu_context->get_device(), batch_size_buffer, one_blob_encode_features_buffer, this->dloss_dnetwork_input_buffer, this->encoded_features_buffer, this->one_blob_encoding_descriptor_set);
	this->x_a_one_blob_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), batch_size_buffer, this->dispatch_indirect_command_buffer, this->x_a_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set);
	this->x_a_one_blob_encoding->update_descriptor_sets(this->gpu_context->get_device(), batch_size_buffer, this->encoded_features_buffer, this->dloss_dnetwork_input_buffer, this->encoded_features_buffer, this->x_a_one_blob_encoding_descriptor_set);
	this->encoding_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), batch_size_buffer, this->dispatch_indirect_command_buffer, this->encoding_initialize_dispatch_indirect_command_descriptor_set);
	this->fully_fused_mlp_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context, batch_size_buffer, this->network_initialize_dispatch_indirect_command_descriptor_set);
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->coupling_layers[i].encoding->get_full_precision_parameter_buffer(), this->coupling_layers[i].encoding->get_parameter_buffer(), this->coupling_layers[i].grid_encoding_initialize_parameters_descriptor_set);
		this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->coupling_layers[i].neural_network->get_full_precision_weights_matrices_buffer(), this->coupling_layers[i].neural_network->get_weight_matrices_buffer(), this->coupling_layers[i].network_initialize_parameters_descriptor_set);
		this->coupling_layers[i].encoding->update_descriptor_sets(this->gpu_context->get_device(), batch_size_buffer, spatial_features_buffer, this->dloss_dnetwork_input_buffer, this->encoded_features_buffer, this->coupling_layers[i].encoding_descriptor_set);
		this->coupling_layers[i].neural_network->update_descriptor_sets(this->gpu_context, this->encoded_features_buffer, this->dloss_dnetwork_input_buffer, batch_size_buffer, this->coupling_layers[i].network_descriptor_set);
		this->coupling_layers[i].optimizer->update_descriptor_sets(this->gpu_context, this->coupling_layers[i].neural_network->get_full_precision_weights_matrices_buffer(), this->coupling_layers[i].neural_network->get_weight_matrices_buffer(), this->coupling_layers[i].neural_network->get_gradient_matrices_buffer(), this->coupling_layers[i].optimizer_descriptor_set);
		this->coupling_layers[i].encoding_optimizer->update_descriptor_sets(this->gpu_context, this->coupling_layers[i].encoding->get_full_precision_parameter_buffer(), this->coupling_layers[i].encoding->get_parameter_buffer(), this->coupling_layers[i].encoding->get_gradient_buffer(), this->coupling_layers[i].encoding_optimizer_descriptor_set);
	}
}

::framework::coroutine::immediate_task<void>(::framework::normalizing_flows::normalizing_flow::initialize)(::framework::command_context * command_context, ::std::uint32_t batch_size, ::std::uint32_t inference_batch_size)
{
	::framework::gpu::command_buffer * const command_buffer = command_context->get_command_buffer();
	command_buffer->bind_descriptor_pool(this->descriptor_pool);
	co_await this->fully_fused_mlp_execution_policy->update_parameters(this->gpu_context, command_context, batch_size, inference_batch_size);
	::std::uint32_t const encoding_parameter_count = this->encoding_execution_policy->get_parameter_count();
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->xavier_uniform->execute(command_buffer, this->coupling_layers[i].grid_encoding_initialize_parameters_descriptor_set, 0, encoding_parameter_count, encoding_parameter_count);
		this->coupling_layers[i].neural_network->initialize_xavier_uniform(command_buffer, this->xavier_uniform.get(), this->coupling_layers[i].network_initialize_parameters_descriptor_set);
		this->coupling_layers[i].optimizer->initialize(command_buffer, this->coupling_layers[i].optimizer_descriptor_set);
		this->coupling_layers[i].encoding_optimizer->initialize(command_buffer, this->coupling_layers[i].encoding_optimizer_descriptor_set);
	}
	::glm::uint const half2_fill_value = ::glm::packHalf2x16(::glm::vec2(0.0f));
	command_buffer->fill_buffer(this->loss_gradient_buffer, 0, ::framework::gpu::whole_size, half2_fill_value, 0, {}, {});
}

void ::framework::normalizing_flows::normalizing_flow::initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size_dynamic_offset, bool inference)
{
	{
		::std::uint32_t const dynamic_offsets[] = { batch_size_dynamic_offset, this->get_one_blob_encoding_dispatch_indirect_command_offset() };
		this->one_blob_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
	}
	{
		::std::uint32_t const dynamic_offsets[] = { batch_size_dynamic_offset, this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset() };
		this->x_a_one_blob_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->x_a_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
	}
	{
		::std::uint32_t const dynamic_offsets[] = { batch_size_dynamic_offset, this->get_encoding_dispatch_indirect_command_offset() };
		this->encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
	}

	::framework::local::initialize_dispatch_indirect_command_push_constants push_constants;
	push_constants.batch_size_offset = batch_size_dynamic_offset;
	push_constants.inference = inference;

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &this->initialize_dispatch_indirect_command_descriptor_set, 0, nullptr);
	command_buffer->push_constants(this->initialize_dispatch_indirect_command_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch(1, 1, 1);

	this->fully_fused_mlp_execution_policy->initialize_dispatch_indirect_command(this->gpu_context, command_buffer, this->network_initialize_dispatch_indirect_command_descriptor_set, inference);

	::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
	buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
	buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::indirect_command_read_bit;
	buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::draw_indirect_bit;
	buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
	buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::indirect_argument_bit;
	buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
	buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
	buffer_memory_barrier.buffer = this->dispatch_indirect_command_buffer;
	buffer_memory_barrier.offset = 0;
	buffer_memory_barrier.size = ::framework::gpu::whole_size;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = 0;
	dependency_info.memory_barriers = nullptr;
	dependency_info.buffer_memory_barrier_count = 1;
	dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;
	command_buffer->pipeline_barrier(&dependency_info);
}

void ::framework::normalizing_flows::normalizing_flow::forward(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size)
{
	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	{
		::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward::one_blob_encoding::forward");
		this->one_blob_encoding->forward(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
	}
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward::x_a_one_blob_encoding::forward");
			this->x_a_one_blob_encoding->forward(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward::encoding::forward");
			this->coupling_layers[i].encoding->forward(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, batch_size);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward::neural_network::forward");

			this->coupling_layers[i].neural_network->forward(command_buffer, this->coupling_layers[i].network_descriptor_set, batch_size);
		}
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward::coupling_transform::forward");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->coupling_transform->forward(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, batch_size);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	this->calc_log_prob(command_buffer, dynamic_offset, batch_size);
	this->train(command_buffer, batch_size);
}

void ::framework::normalizing_flows::normalizing_flow::forward_indirect(::framework::gpu::command_buffer * command_buffer)
{
	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	{
		::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward_indirect::one_blob_encoding::forward_indirect");
		this->one_blob_encoding->forward_indirect(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_one_blob_encoding_dispatch_indirect_command_offset());
	}
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward_indirect::x_a_one_blob_encoding::forward_indirect");
			this->x_a_one_blob_encoding->forward_indirect(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset());
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward_indirect::encoding::forward_indirect");
			this->coupling_layers[i].encoding->forward_indirect(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_encoding_dispatch_indirect_command_offset());
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward_indirect::neural_network::forward_indirect");
			this->coupling_layers[i].neural_network->forward_indirect(command_buffer, this->coupling_layers[i].network_descriptor_set);
		}
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::forward_indirect::coupling_transform::forward_indirect");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->coupling_transform->forward_indirect(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_coupling_transform_dispatch_indirect_command_offset());
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	this->calc_log_prob_indirect(command_buffer, dynamic_offset);
	this->train_indirect(command_buffer);
}

void ::framework::normalizing_flows::normalizing_flow::backward(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size)
{
	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	this->one_blob_encoding->forward(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].encoding->forward(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].neural_network->forward(command_buffer, this->coupling_layers[i].network_descriptor_set, batch_size);
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set
		};
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::backward::coupling_transform::inverse");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->coupling_transform->inverse(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, batch_size);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	this->calc_log_prob(command_buffer, dynamic_offset, batch_size);
	this->train(command_buffer, batch_size);
}

void ::framework::normalizing_flows::normalizing_flow::backward_indirect(::framework::gpu::command_buffer * command_buffer)
{
	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	this->one_blob_encoding->forward_indirect(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_one_blob_encoding_dispatch_indirect_command_offset());
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward_indirect(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset());
		this->coupling_layers[i].encoding->forward_indirect(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_encoding_dispatch_indirect_command_offset());
		this->coupling_layers[i].neural_network->forward_indirect(command_buffer, this->coupling_layers[i].network_descriptor_set);
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set
		};
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->coupling_transform->inverse_indirect(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_coupling_transform_dispatch_indirect_command_offset());
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	this->calc_log_prob_indirect(command_buffer, dynamic_offset);
	this->train_indirect(command_buffer);
}

void ::framework::normalizing_flows::normalizing_flow::forward_without_gradient(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size)
{
	::std::uint32_t const dynamic_offset = 0;
	this->one_blob_encoding->forward(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].encoding->forward(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].neural_network->inference(command_buffer, this->coupling_layers[i].network_descriptor_set, batch_size);
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->coupling_transform->forward_no_grad(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	this->calc_log_prob(command_buffer, dynamic_offset, batch_size);
}

void ::framework::normalizing_flows::normalizing_flow::forward_without_gradient_indirect(::framework::gpu::command_buffer * command_buffer)
{
	::std::uint32_t const dynamic_offset = 0;
	this->one_blob_encoding->forward_indirect(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_one_blob_encoding_dispatch_indirect_command_offset());
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward_indirect(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset());
		this->coupling_layers[i].encoding->forward_indirect(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_encoding_dispatch_indirect_command_offset());
		this->coupling_layers[i].neural_network->inference_indirect(command_buffer, this->coupling_layers[i].network_descriptor_set);
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->coupling_transform->forward_no_grad_indirect(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_coupling_transform_no_grad_dispatch_indirect_command_offset());
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	this->calc_log_prob_indirect(command_buffer, dynamic_offset);
}

void ::framework::normalizing_flows::normalizing_flow::backward_without_gradient(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size)
{
	::std::uint32_t const dynamic_offset = 0;
	this->one_blob_encoding->forward(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].encoding->forward(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].neural_network->inference(command_buffer, this->coupling_layers[i].network_descriptor_set, batch_size);
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::backward_without_gradient::coupling_transform::inverse");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->coupling_transform->inverse_no_grad(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, batch_size);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	this->calc_log_prob(command_buffer, dynamic_offset, batch_size);
}

void ::framework::normalizing_flows::normalizing_flow::backward_without_gradient_indirect(::framework::gpu::command_buffer * command_buffer)
{
	::std::uint32_t const dynamic_offset = 0;
	this->one_blob_encoding->forward_indirect(command_buffer, this->one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_one_blob_encoding_dispatch_indirect_command_offset());
	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		this->x_a_one_blob_encoding->forward_indirect(command_buffer, this->x_a_one_blob_encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_x_a_one_blob_encoding_dispatch_indirect_command_offset());
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::backward_without_gradient_indirect::encoding::forward_indirect");
			this->coupling_layers[i].encoding->forward_indirect(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_encoding_dispatch_indirect_command_offset());
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::backward_without_gradient_indirect::neural_network::inference_indirect");
			this->coupling_layers[i].neural_network->inference_indirect(command_buffer, this->coupling_layers[i].network_descriptor_set);
		}
		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::backward_without_gradient_indirect::coupling_transform::inverse_indirect");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->coupling_transform->inverse_no_grad_indirect(command_buffer, this->pipeline_layout, descriptor_sets, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_coupling_transform_no_grad_dispatch_indirect_command_offset());
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	this->calc_log_prob_indirect(command_buffer, dynamic_offset);
}

void ::framework::normalizing_flows::normalizing_flow::calc_log_prob(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size)
{
	if (this->transform_type != ::framework::normalizing_flows::transform_type::affine)
	{
		return;
	}

	struct ::framework::local::calc_log_prob_push_constants push_constants
	{
		.stride = this->output_stride
	};

	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		this->normalizing_flow_descriptor_set,
		this->coupling_layers[0].coupling_layers_descriptor_set
	};

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->calc_log_prob_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((batch_size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::normalizing_flows::normalizing_flow::calc_log_prob_indirect(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t dynamic_offset)
{
	if (this->transform_type != ::framework::normalizing_flows::transform_type::affine)
	{
		return;
	}

	struct ::framework::local::calc_log_prob_push_constants push_constants
	{
		.stride = this->output_stride
	};

	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		this->normalizing_flow_descriptor_set,
		this->coupling_layers[0].coupling_layers_descriptor_set
	};

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->calc_log_prob_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch_indirect(this->dispatch_indirect_command_buffer, this->get_calc_log_prob_dispatch_indirect_command_offset());
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::normalizing_flows::normalizing_flow::train(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size)
{
	command_buffer->fill_buffer(this->dloss_dnetwork_input_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});

	this->loss->evaluate(command_buffer, this->loss_descriptor_set, this->output_stride * batch_size);

	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		struct ::framework::local::chain_loss_grad_push_constants push_constants
		{
			.dims = this->output_size,
			.stride = this->output_stride
		};

		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chain_loss_grad_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
		command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((batch_size * this->output_stride + 128 - 1) / 128, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		this->coupling_layers[i].neural_network->backward(command_buffer, this->coupling_layers[i].network_descriptor_set, batch_size);
		this->coupling_layers[i].optimizer->step(command_buffer, this->coupling_layers[i].optimizer_descriptor_set);
		this->coupling_layers[i].encoding->backward(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, batch_size);
		this->coupling_layers[i].encoding_optimizer->step(command_buffer, this->coupling_layers[i].encoding_optimizer_descriptor_set);
	}
}

void ::framework::normalizing_flows::normalizing_flow::train_indirect(::framework::gpu::command_buffer * command_buffer)
{
	command_buffer->fill_buffer(this->dloss_dnetwork_input_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});

	{
		::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::train_indirect::loss::evaluate_indirect");
		this->loss->evaluate_indirect(command_buffer, this->loss_descriptor_set, this->dispatch_indirect_command_buffer, this->get_loss_dispatch_indirect_command_offset());
	}

	::std::uint32_t const dynamic_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

	for (::std::uint32_t i = 0; i < this->coupling_layer_count; i++)
	{
		struct ::framework::local::chain_loss_grad_push_constants push_constants
		{
			.dims = this->output_size,
			.stride = this->output_stride
		};

		::framework::gpu::descriptor_set * const descriptor_sets[]
		{
			this->normalizing_flow_descriptor_set,
			this->coupling_layers[i].coupling_layers_descriptor_set,
			this->resource_manager->default_resources.gpu_log_device_descriptor_set
		};

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::train_indirect::chain_loss_grad_pipeline");
			command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chain_loss_grad_pipeline);
			command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
			command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
			command_buffer->dispatch_indirect(this->dispatch_indirect_command_buffer, this->get_chain_loss_grad_dispatch_indirect_command_offset());
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::train_indirect::neural_network::backward_indirect");
			this->coupling_layers[i].neural_network->backward_indirect(command_buffer, this->coupling_layers[i].network_descriptor_set);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::train_indirect::optimizer::step");
			this->coupling_layers[i].optimizer->step(command_buffer, this->coupling_layers[i].optimizer_descriptor_set);
		}

		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "normalizing_flow::train_indirect::encoding::backward_indirect");
			this->coupling_layers[i].encoding->backward_indirect(command_buffer, this->coupling_layers[i].encoding_descriptor_set, dynamic_offset, this->dispatch_indirect_command_buffer, this->get_encoding_dispatch_indirect_command_offset());
			this->coupling_layers[i].encoding_optimizer->step(command_buffer, this->coupling_layers[i].encoding_optimizer_descriptor_set);
		}
	}
}