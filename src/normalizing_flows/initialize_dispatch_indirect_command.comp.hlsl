#include "gpu/gpu.hlsl"
#include "numeric.hlsl"

[[vk::constant_id(0)]] const uint output_stride = 1;

struct push_constants_t
{
	uint32_t batch_size_offset;
	bool inference;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] ByteAddressBuffer batch_size_buffer : register(t0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u0, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint batch_size = batch_size_buffer.Load(push_constants.batch_size_offset * uint(!push_constants.inference));

	// coupling_transform
	dispatch_indirect_command_buffer[0] = ::framework::gpu::dispatch_indirect_command::create(batch_size, 1, 1);
	// calc_log_prob
	dispatch_indirect_command_buffer[1] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(batch_size, 128u), 1, 1);
	if (!push_constants.inference)
	{
		// loss & chain_loss_grad
		dispatch_indirect_command_buffer[2] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(batch_size * output_stride, 128u), 1, 1);
	}
}