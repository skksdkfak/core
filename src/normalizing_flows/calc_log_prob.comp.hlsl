#include "numbers.hlsl"

struct push_constants_t
{
	uint32_t stride;
	uint32_t pad0;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<ELEMENT_TYPE> x_a_buffer : register(u1, space0);

[[vk::binding(3, 0)]] RWStructuredBuffer<ELEMENT_TYPE> log_jacobian_buffer : register(u2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<ELEMENT_TYPE> loss_gradient_buffer : register(t0, space0);

[[vk::binding(0, 1)]] StructuredBuffer<ELEMENT_TYPE> transform_parameters_buffer : register(t0, space1);

[[vk::binding(1, 1)]] RWStructuredBuffer<ELEMENT_TYPE> gradients_buffer : register(u0, space1);


float2 get_uniform_distribution_log_prob(float2 p)
{
	return 0.0f;
}

float2 get_standard_normal_distribution_log_prob(float2 p)
{
	return -(p * p) / 2.0f - ::framework::numbers::log_sqrt_2_pi;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint i = dispatch_thread_id.x;
	if (i >= size)
	{
		return;
	}

	const uint32_t x_index = i * 2;
	const uint32_t index = i * push_constants.stride;
	const uint32_t x_a_index = x_index + 0;
	const uint32_t x_b_index = x_index + 1;
	const float2 z = float2(x_buffer[x_a_index], x_buffer[x_b_index]);
	const float2 latent_prior_log_prob = ::get_standard_normal_distribution_log_prob(z);
	//const float2 latent_prior_log_prob = ::get_uniform_distribution_log_prob(z);
	const float log_jacobian = float(log_jacobian_buffer[index]);
	const float logqx = log_jacobian + latent_prior_log_prob.x + latent_prior_log_prob.y;
	log_jacobian_buffer[index] = ELEMENT_TYPE(logqx);
}
