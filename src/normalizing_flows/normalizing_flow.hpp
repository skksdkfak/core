#pragma once

#include "algorithm/xavier_uniform.hpp"
#include "command_context.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "nn/losses/cross_entropy.hpp"
#include "nn/losses/variance_is.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "nn/grid/encoding.hpp"
#include "nn/mlp/network.hpp"
#include "nn/one_blob/encoding.hpp"
#include "nn/optimizers/adam.hpp"
#include "normalizing_flows/transform.hpp"
#include "profiler.hpp"
#include <memory>
#include <vector>

namespace framework::normalizing_flows
{
	class normalizing_flow
	{
	public:
		normalizing_flow(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::profiler * profiler, ::framework::gpu_log * gpu_log, ::framework::gpu::descriptor_pool * descriptor_pool, ::framework::normalizing_flows::transform_type transform_type, ::std::uint32_t coupling_layer_count, ::std::uint32_t spatial_feature_count, ::std::uint32_t spatial_feature_stride, ::std::uint32_t one_blob_encode_feature_count, ::std::uint32_t one_blob_encode_feature_stride, ::std::uint32_t hidden_width, ::std::uint32_t hidden_layer_count, float learning_rate, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, ::std::uint32_t batch_alignment, ::std::uint32_t inference_batch_alignment, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type pdf_type, ::framework::gpu::component_type target_type, ::framework::gpu::component_type accumulator_type, ::framework::gpu::buffer * spatial_features_buffer, ::framework::gpu::buffer * one_blob_encode_features_buffer, ::framework::gpu::buffer * pdf_buffer, ::framework::gpu::buffer * target_buffer, ::framework::gpu::buffer * batch_size_buffer);

		::framework::coroutine::immediate_task<void> initialize(::framework::command_context * command_context, ::std::uint32_t batch_size, ::std::uint32_t inference_batch_size);

		void initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size_dynamic_offset, bool inference);

		void forward(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size);

		void forward_indirect(::framework::gpu::command_buffer * command_buffer);

		void backward(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size);

		void backward_indirect(::framework::gpu::command_buffer * command_buffer);

		void forward_without_gradient(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size);

		void forward_without_gradient_indirect(::framework::gpu::command_buffer * command_buffer);

		void backward_without_gradient(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size);

		void backward_without_gradient_indirect(::framework::gpu::command_buffer * command_buffer);

		::framework::gpu::buffer * get_x_buffer() const;
		
		::framework::gpu::buffer * get_features_buffer() const;

		::framework::gpu::buffer * get_log_jacobian_buffer() const;

		::std::uint32_t get_features_stride() const;

		::std::uint32_t get_output_stride() const;

	private:
		void calc_log_prob(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size);

		void calc_log_prob_indirect(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t dynamic_offset);

		void train(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t batch_size);

		void train_indirect(::framework::gpu::command_buffer * command_buffer);

		::framework::gpu::device_size get_coupling_transform_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_coupling_transform_no_grad_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_calc_log_prob_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_loss_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_chain_loss_grad_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_one_blob_encoding_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_x_a_one_blob_encoding_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_encoding_dispatch_indirect_command_offset() const noexcept;

		struct coupling_layer_data
		{
			::std::unique_ptr<::framework::nn::grid::encoding> encoding;
			::std::unique_ptr<::framework::nn::fully_fused_mlp::network> neural_network;
			::std::unique_ptr<::framework::nn::adam> optimizer;
			::std::unique_ptr<::framework::nn::adam> encoding_optimizer;
			::framework::gpu::descriptor_set * grid_encoding_initialize_parameters_descriptor_set;
			::framework::gpu::descriptor_set * encoding_descriptor_set;
			::framework::gpu::descriptor_set * network_descriptor_set;
			::framework::gpu::descriptor_set * network_initialize_parameters_descriptor_set;
			::framework::gpu::descriptor_set * optimizer_descriptor_set;
			::framework::gpu::descriptor_set * encoding_optimizer_descriptor_set;
			::framework::gpu::descriptor_set * coupling_layers_descriptor_set;
		};

		::framework::gpu_context * gpu_context;
		::framework::resource::resource_manager * resource_manager;
		::framework::profiler * profiler;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::normalizing_flows::transform_type transform_type;
		::std::uint32_t coupling_layer_count;
		::framework::gpu::shader_module * calc_log_prob_shader_module;
		::framework::gpu::shader_module * chain_loss_grad_shader_module;
		::framework::gpu::shader_module * initialize_dispatch_indirect_command_shader_module;
		::framework::gpu::descriptor_set_layout * normalizing_flow_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * coupling_layers_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * initialize_dispatch_indirect_command_descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_layout * initialize_dispatch_indirect_command_pipeline_layout;
		::framework::gpu::pipeline * calc_log_prob_pipeline;
		::framework::gpu::pipeline * chain_loss_grad_pipeline;
		::framework::gpu::pipeline * initialize_dispatch_indirect_command_pipeline;
		::std::unique_ptr<::framework::algorithm::xavier_uniform> xavier_uniform;
		::std::unique_ptr<::framework::nn::one_blob::execution_policy> one_blob_execution_policy;
		::std::unique_ptr<::framework::nn::one_blob::encoding> one_blob_encoding;
		::std::unique_ptr<::framework::nn::one_blob::execution_policy> x_a_one_blob_execution_policy;
		::std::unique_ptr<::framework::nn::one_blob::encoding> x_a_one_blob_encoding;
		::std::unique_ptr<::framework::nn::grid::execution_policy> encoding_execution_policy;
		::std::unique_ptr<::framework::nn::fully_fused_mlp::execution_policy> fully_fused_mlp_execution_policy;
		::std::unique_ptr<::framework::normalizing_flows::transform> coupling_transform;
		::std::vector<::framework::normalizing_flows::normalizing_flow::coupling_layer_data> coupling_layers;
		::std::unique_ptr<::framework::nn::cross_entropy> loss;
		::framework::gpu::descriptor_set * loss_descriptor_set;
		::framework::gpu::descriptor_set * normalizing_flow_descriptor_set;
		::framework::gpu::descriptor_set * initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * one_blob_encoding_descriptor_set;
		::framework::gpu::descriptor_set * one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * x_a_one_blob_encoding_descriptor_set;
		::framework::gpu::descriptor_set * x_a_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * network_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::buffer * x_buffer;
		::framework::gpu::device_memory * x_device_memory;
		::framework::gpu::buffer * dloss_dnetwork_input_buffer;
		::framework::gpu::device_memory * dloss_dnetwork_input_device_memory;
		::framework::gpu::buffer * encoded_features_buffer;
		::framework::gpu::device_memory * encoded_features_device_memory;
		::framework::gpu::buffer * log_jacobian_buffer;
		::framework::gpu::device_memory * log_jacobian_device_memory;
		::framework::gpu::buffer * loss_gradient_buffer;
		::framework::gpu::device_memory * loss_gradient_device_memory;
		::framework::gpu::buffer * loss_buffer;
		::framework::gpu::device_memory * loss_device_memory;
		::framework::gpu::buffer * dispatch_indirect_command_buffer;
		::framework::gpu::device_memory * dispatch_indirect_command_device_memory;
		::std::uint32_t output_size;
		::std::uint32_t features_stride;
		::std::uint32_t output_stride;
	};
}

#include "normalizing_flows/normalizing_flow.inl"