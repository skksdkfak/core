#pragma once

#include "gpu/core.hpp"

namespace framework::normalizing_flows
{
	enum class transform_type
	{
		affine,
		piecewise_linear,
		piecewise_quadratic,
		piecewise_rational_quadratic
	};

	class transform
	{
	public:
		virtual void forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) = 0;

		virtual void forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;

		virtual void inverse(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) = 0;

		virtual void inverse_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;

		virtual void forward_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) = 0;

		virtual void forward_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;

		virtual void inverse_no_grad(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::std::uint32_t batch_size) = 0;

		virtual void inverse_no_grad_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;
	};
}