#pragma once

#include "graphics/scene_view.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace framework::input
{
	class my_input_listner;
}

namespace framework::camera
{
	class scene_view_controller
	{
	public:
		scene_view_controller(::framework::graphics::scene_view * scene_view, ::framework::input::my_input_listner * my_input_listner);

		void update(float dt);

		void slow_movement(bool enable) { this->fine_movement = enable; }

		void slow_rotation(bool enable) { this->fine_rotation = enable; }

		void enable_momentum(bool enable) { this->momentum = enable; }

		void set_move_speed(float movespeed) { this->move_speed = movespeed; }

	private:
		void apply_momentum(float & old_value, float & new_value, float delta_time);

		::framework::graphics::scene_view * scene_view;
		::framework::input::my_input_listner * my_input_listner;
		::glm::quat rotation;
		float move_speed;
		float strafe_speed;
		float mouse_sensitivity_x;
		float mouse_sensitivity_y;
		bool fine_movement;
		bool fine_rotation;
		bool momentum;
		float last_yaw;
		float last_pitch;
		float last_forward;
		float last_strafe;
		float last_ascent;
	};
}