#include "camera/scene_view_controller.h"
#include "input/my_input_listner.hpp"
#include "engine_core.h"
#include <glm/gtc/quaternion.hpp>
#include <algorithm>

::framework::camera::scene_view_controller::scene_view_controller(::framework::graphics::scene_view * scene_view, ::framework::input::my_input_listner * my_input_listner) :
	scene_view(scene_view),
	my_input_listner(my_input_listner)
{
	this->rotation = ::glm::quat(0.737319, 0.00614446, -0.675494, -0.00562916);

	this->move_speed = 10.f;
	this->strafe_speed = 10.f;
	this->mouse_sensitivity_x = 1.0f;
	this->mouse_sensitivity_y = 1.0f;

	this->fine_movement = false;
	this->fine_rotation = false;
	this->momentum = true;

	this->last_yaw = 0.0f;
	this->last_pitch = 0.0f;
	this->last_forward = 0.0f;
	this->last_strafe = 0.0f;
	this->last_ascent = 0.0f;
}

void ::framework::camera::scene_view_controller::update(float delta_time)
{
	this->fine_movement = !this->my_input_listner->get_key_state(::framework::input::key::key_left_shift);

	//if (GameInput::IsFirstPressed(GameInput::kRThumbClick))
	//	this->fine_rotation = !this->fine_rotation;

	float speedScale = this->fine_movement ? 1.f : 10.0f;
	float panScale = this->fine_rotation ? 0.5f : 0.1f;

	::std::int16_t mouse_wheel_delta;
	this->my_input_listner->get_mouse_wheel_delta(mouse_wheel_delta);
	this->move_speed = ::std::max(0.0f, this->move_speed + mouse_wheel_delta * this->move_speed * 0.1f / ::framework::engine_core::connection_properties.wheel_delta);
	this->strafe_speed = ::std::max(0.0f, this->strafe_speed + mouse_wheel_delta * this->strafe_speed * 0.1f / ::framework::engine_core::connection_properties.wheel_delta);

	float yaw = 0.0f/*GameInput::GetTimeCorrectedAnalogInput(GameInput::kAnalogRightStickX) * m_HorizontalLookSensitivity * panScale*/;
	float pitch = 0.0f/*GameInput::GetTimeCorrectedAnalogInput(GameInput::kAnalogRightStickY) * m_VerticalLookSensitivity * panScale*/;
	float forward = this->move_speed * speedScale * (
		(this->my_input_listner->get_key_state(::framework::input::key::key_w) ? delta_time : 0.0f) +
		(this->my_input_listner->get_key_state(::framework::input::key::key_s) ? -delta_time : 0.0f)
		);
	float strafe = this->strafe_speed * speedScale * (
		(this->my_input_listner->get_key_state(::framework::input::key::key_d) ? delta_time : 0.0f) +
		(this->my_input_listner->get_key_state(::framework::input::key::key_a) ? -delta_time : 0.0f)
		);
	float ascent = this->strafe_speed * speedScale * (
		(this->my_input_listner->get_key_state(::framework::input::key::key_e) ? delta_time : 0.0f) +
		(this->my_input_listner->get_key_state(::framework::input::key::key_q) ? -delta_time : 0.0f)
		);

	if (this->momentum)
	{
		this->apply_momentum(this->last_yaw, yaw, delta_time);
		this->apply_momentum(this->last_pitch, pitch, delta_time);
		this->apply_momentum(this->last_forward, forward, delta_time);
		this->apply_momentum(this->last_strafe, strafe, delta_time);
		this->apply_momentum(this->last_ascent, ascent, delta_time);
	}

	if (this->my_input_listner->get_button_state(::framework::input::button::button_3))
	{
		::std::int16_t x, y;
		this->my_input_listner->get_mouse_delta(x, y);

		yaw += x / 180.0f * this->mouse_sensitivity_x;
		pitch -= y / 180.0f * this->mouse_sensitivity_y;
	}

	const float PI = atan(1) * 4;
	const ::glm::vec3 WORLD_XAXIS = ::glm::vec3(1.0f, 0.0f, 0.0f);
	const ::glm::vec3 WORLD_YAXIS = ::glm::vec3(0.0f, 1.0f, 0.0f);
	const ::glm::vec3 WORLD_ZAXIS = ::glm::vec3(0.0f, 0.0f, 1.0f);
	auto from_axis_angle = [](::glm::vec3 const axis, float const radians) -> ::glm::quat
	{
		float half_theta = radians / 2.0;
		float s = ::std::sin(half_theta);
		return ::glm::quat(::std::cos(half_theta), axis * s);
	};

	::glm::quat rotation;
	::glm::quat & orientation = /*from_axis_angle(::glm::normalize(scene_view->get_center() - scene_view->get_eye()), 0.0f)*/this->rotation;
	if (pitch != 0)
	{
		rotation = from_axis_angle(WORLD_XAXIS, pitch);
		orientation = rotation * orientation;
		orientation = ::glm::normalize(orientation);
	}

	if (yaw != 0)
	{
		rotation = from_axis_angle(WORLD_YAXIS, yaw);
		orientation = orientation * rotation;
		orientation = ::glm::normalize(orientation);
	}

	auto rotate = ::glm::mat4_cast(orientation);
	::glm::vec3 m_xaxis = ::glm::vec3(rotate[0][0], rotate[1][0], rotate[2][0]);
	::glm::vec3 m_yaxis = ::glm::vec3(rotate[0][1], rotate[1][1], rotate[2][1]);
	::glm::vec3 m_zaxis = ::glm::vec3(rotate[0][2], rotate[1][2], rotate[2][2]);

	::glm::vec3 translation = this->scene_view->get_eye();
	translation += m_xaxis * (-strafe);
	translation += m_yaxis * ascent;
	translation += m_zaxis * forward;

	this->scene_view->set_center(translation + m_zaxis);
	this->scene_view->set_eye(translation);
	this->scene_view->update();
}

void ::framework::camera::scene_view_controller::apply_momentum(float & old_value, float & new_value, float delta_time)
{
	float blended_value;
	if (::std::abs(new_value) > ::std::abs(old_value))
		blended_value = ::std::lerp(new_value, old_value, ::std::pow(0.6f, delta_time * 60.0f));
	else
		blended_value = ::std::lerp(new_value, old_value, ::std::pow(0.8f, delta_time * 60.0f));
	old_value = blended_value;
	new_value = blended_value;
}