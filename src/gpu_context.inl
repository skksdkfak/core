inline ::framework::gpu::physical_device * (::framework::gpu_context::get_physical_device)() const noexcept
{
	return this->physical_device;
}

inline ::framework::gpu::physical_device_features const & (::framework::gpu_context::get_physical_device_features)() const noexcept
{
	return this->physical_device_features;
}

inline ::framework::gpu::physical_device_properties const & (::framework::gpu_context::get_physical_device_properties)() const noexcept
{
	return this->physical_device_properties;
}

inline ::framework::gpu::physical_device_memory_properties const & (::framework::gpu_context::get_physical_device_memory_properties)() const noexcept
{
	return this->physical_device_memory_properties;
}

inline ::framework::gpu::device * (::framework::gpu_context::get_device)() const noexcept
{
	return this->device;
}

inline auto const & (::framework::gpu_context::get_queues)() const noexcept
{
	return this->queues;
}

inline auto const & (::framework::gpu_context::get_queue_family_indices)() const noexcept
{
	return this->queue_family_indices;
}