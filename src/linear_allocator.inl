#include <cassert>

inline ::framework::gpu::buffer * (::framework::linear_allocation_page::get_buffer)() const noexcept
{
	return this->buffer;
}

inline ::framework::gpu::device_memory * (::framework::linear_allocation_page::get_device_memory)() const noexcept
{
	return this->device_memory;
}

inline void * (::framework::linear_allocation_page::get_mapped_buffer_data)() const noexcept
{
	return this->mapped_buffer_data;
}

inline ::framework::linear_allocator_page_manager::linear_allocator_page_manager(::framework::gpu_context * gpu_context, ::framework::linear_allocation_page::type const page_type) :
	gpu_context(gpu_context),
	page_type(page_type),
	page_size(0x200000)
{
}

inline ::framework::linear_allocator_page_manager::~linear_allocator_page_manager()
{
}

inline ::framework::gpu::device_size(::framework::linear_allocator_page_manager::get_page_size)() const noexcept
{
	return this->page_size;
}

inline ::framework::gpu_context * (::framework::linear_allocator_page_manager::get_gpu_context)() const noexcept
{
	return this->gpu_context;
}

inline ::framework::linear_allocator::linear_allocator(::framework::linear_allocator_page_manager & linear_allocator_page_manager) :
	linear_allocator_page_manager(linear_allocator_page_manager),
	current_linear_allocation_page(nullptr),
	current_offset(~0ull)
{
}

inline ::framework::linear_allocator::~linear_allocator()
{
	assert(this->retired_pages.empty());
}