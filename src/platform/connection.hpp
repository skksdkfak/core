#pragma once

#include "platform/window.hpp"
#include "input/input_listner.hpp"
#include <string>

namespace framework::platform
{
	struct connection_properties
	{
		::std::uint16_t wheel_delta;
	};

	class connection
	{
	public:
		virtual void create_window(::framework::platform::window_create_info const * create_info, ::framework::platform::window ** window) = 0;

		virtual void open_file_dialog(::std::wstring & file_name) = 0;

		virtual void poll_for_event(::framework::input::input_listner * input_listner) = 0;

		virtual void get_properties(::framework::platform::connection_properties & connection_properties) = 0;

		virtual void set_process_dpi_awareness() = 0;
	};
}