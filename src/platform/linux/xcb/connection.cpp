#include "input/input_listner.hpp"
#include "window.h"
#include "connection.h"
#include <stdexcept>
#include <cstring>

static ::framework::input::input_listner * input_listner;

const unsigned char evdev_to_hid_key[256] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0, 41, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, 45, 46, 42, 43, 20, 26,  8, 21, 23, 28, 24, 12,
    18, 19, 47, 48, 40,224,  4, 22,  7,  9, 10, 11, 13, 14, 15, 51,
    52, 53,225, 49, 29, 27,  6, 25,  5, 17, 16, 54, 55, 56,229, 85,
    226, 44, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 83, 71, 95,
    96, 97, 86, 92, 93, 94, 87, 89, 90, 91, 98, 99,  0,  0,100, 68,
    69,  0,  0,  0,  0,  0,  0,  0, 88,228, 84, 70,230,  0, 74, 82,
    75, 80, 79, 77, 81, 78, 73, 76,  0,127,128,129,  0,103,  0, 72,
    0,  0,  0,  0,  0,227,231,118,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,104,
    105,106,107,108,109,110,111,112,113,114,115,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

const ::framework::input::button evdev_to_hid_button[10]
{
    ::framework::input::button::button_none, ::framework::input::button::button_1, ::framework::input::button::button_2, ::framework::input::button::button_3,
    ::framework::input::button::button_4, ::framework::input::button::button_5, ::framework::input::button::button_6, ::framework::input::button::button_7,
    ::framework::input::button::button_8, ::framework::input::button::button_9
};

static inline ::xcb_intern_atom_reply_t * intern_atom_helper(::xcb_connection_t * xcb_connection, bool only_if_exists, char const * str)
{
    ::xcb_intern_atom_cookie_t xcb_intern_atom_cookie = ::xcb_intern_atom(xcb_connection, only_if_exists, ::std::strlen(str), str);
    return ::xcb_intern_atom_reply(xcb_connection, xcb_intern_atom_cookie, NULL);
}

::framework::platform::linux_::xcb::connection::connection()
{
    ::xcb_setup_t const * setup;
    ::xcb_screen_iterator_t iter;
    int scr;

    this->xcb_connection = ::xcb_connect(NULL, &scr);
    if (this->xcb_connection == NULL)
        throw ::std::runtime_error("Could not find a compatible Vulkan ICD!\n");

    setup = ::xcb_get_setup(this->xcb_connection);
    iter = ::xcb_setup_roots_iterator(setup);
    while (scr-- > 0)
        ::xcb_screen_next(&iter);
    xcb_screen = iter.data;
}

::framework::platform::linux_::xcb::connection::~connection()
{
    ::xcb_disconnect(this->xcb_connection);
}

void ::framework::platform::linux_::xcb::connection::create_window(::framework::platform::window_create_info const * create_info, ::framework::platform::window ** window)
{
    *window = new ::framework::platform::linux_::xcb::window(this, create_info);
}

void ::framework::platform::linux_::xcb::connection::poll_for_event(::framework::input::input_listner * input_listner)
{
    ::input_listner = input_listner;
    ::xcb_generic_event_t * event;
    while (event = ::xcb_poll_for_event(this->xcb_connection))
    {
        this->handle_event(event);
        ::free(event);
    }
}

void ::framework::platform::linux_::xcb::connection::open_file_dialog(::std::wstring & file_name)
{

}

void ::framework::platform::linux_::xcb::connection::get_properties(::framework::platform::connection_properties & connection_properties)
{
    connection_properties.wheel_delta = 120; //todo
}

void ::framework::platform::linux_::xcb::connection::handle_event(::xcb_generic_event_t const * event)
{
    switch (event->response_type & ~0x80)
    {
        case XCB_EXPOSE:
        {
            ::xcb_expose_event_t *expose = (::xcb_expose_event_t *)event;

            /*printf ("Window %"PRIu32" exposed. Region to be redrawn at location (%"PRIu16",%"PRIu16"), with dimension (%"PRIu16",%"PRIu16")\n",
                    expose->window, expose->x, expose->y, expose->width, expose->height );*/
            break;
        }
        case XCB_BUTTON_PRESS:
        {
            ::xcb_button_press_event_t *bp = (::xcb_button_press_event_t *)event;
            ::input_listner->button_down(evdev_to_hid_button[bp->detail]);
        }
        break;
        case XCB_BUTTON_RELEASE:
        {
            ::xcb_button_release_event_t *br = (::xcb_button_release_event_t *)event;
            ::input_listner->button_up(evdev_to_hid_button[br->detail]);
        }
        break;
        case XCB_MOTION_NOTIFY:
        {
            ::xcb_motion_notify_event_t * motion = (::xcb_motion_notify_event_t *)event;
            ::input_listner->mouse_move(motion->event_x, motion->event_y);
            break;
        }
        case XCB_ENTER_NOTIFY:
        {
            ::xcb_enter_notify_event_t *enter = (::xcb_enter_notify_event_t *)event;
            break;
        }
        case XCB_LEAVE_NOTIFY:
        {
            ::xcb_leave_notify_event_t *leave = (::xcb_leave_notify_event_t *)event;
            break;
        }
        case XCB_KEY_PRESS:
        {
            ::xcb_key_press_event_t * xcb_key_press_event = (::xcb_key_press_event_t *)event;
            ::input_listner->key_down((::framework::input::key)evdev_to_hid_key[xcb_key_press_event->detail]);
            break;
        }
        case XCB_KEY_RELEASE:
        {
            ::xcb_key_release_event_t * xcb_key_release_event = (::xcb_key_release_event_t *)event;
            ::input_listner->key_up((::framework::input::key)evdev_to_hid_key[xcb_key_release_event->detail]);
            break;
        }
        default:
            break;
    }
}
