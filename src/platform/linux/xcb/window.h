#pragma once

#include "platform/window.hpp"
#include "platform/connection.hpp"
#include <xcb/xcb.h>

namespace framework::platform::linux_::xcb
{
    class connection;

    class window : public ::framework::platform::window
    {
    public:
		~window();

        ::std::uint16_t get_width() override { return 0; }

        ::std::uint16_t get_height()  override { return 0; }

		::xcb_window_t get_xcb_window() const { return this->xcb_window; }

        ::framework::platform::linux_::xcb::connection * get_connection() const { return this->connection; }

    private:
        friend class ::framework::platform::linux_::xcb::connection;

        window(::framework::platform::linux_::xcb::connection * connection, ::framework::platform::window_create_info const * create_info);

        ::xcb_window_t xcb_window;
        ::xcb_intern_atom_reply_t * atom_wm_delete_window;
        ::framework::platform::linux_::xcb::connection * connection;
    };
}