#pragma once

#include "platform/connection.hpp"
#include <xcb/xcb.h>

namespace framework::platform::linux_::xcb
{
    class connection : public ::framework::platform::connection
    {
    public:
        connection();

        ~connection();

        void create_window(::framework::platform::window_create_info const * create_info, ::framework::platform::window ** window) override;

        void open_file_dialog(::std::wstring & file_name) override;

        void poll_for_event(::framework::input::input_listner * input_listner) override;

        void get_properties(::framework::platform::connection_properties & connection_properties) override;

        ::xcb_connection_t * get_xcb_connection() const { return this->xcb_connection; }

		::xcb_screen_t * get_xcb_screen() const { return this->xcb_screen; }

    private:
		void handle_event(const ::xcb_generic_event_t *event);

        ::xcb_connection_t * xcb_connection;
        ::xcb_screen_t * xcb_screen;
    };
}