#include "window.h"
#include "connection.h"
#include "../../connection.hpp"
#include <string>
#include <cstring>

static inline xcb_intern_atom_reply_t* intern_atom_helper(::xcb_connection_t * xcb_connection, bool only_if_exists, char const * str)
{
    ::xcb_intern_atom_cookie_t xcb_intern_atom_cookie = ::xcb_intern_atom(xcb_connection, only_if_exists, ::std::strlen(str), str);
    return ::xcb_intern_atom_reply(xcb_connection, xcb_intern_atom_cookie, NULL);
}

::framework::platform::linux_::xcb::window::window(::framework::platform::linux_::xcb::connection * connection, ::framework::platform::window_create_info const * create_info) :
    connection(connection)
{
    std::string title = "EngineFramework";
    xcb_screen_iterator_t iter;

    uint32_t value_mask, value_list[32];

    this->xcb_window = xcb_generate_id(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection());

    value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    value_list[0] = static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_screen()->black_pixel;
    value_list[1] =
            XCB_EVENT_MASK_KEY_RELEASE |
            XCB_EVENT_MASK_KEY_PRESS |
            XCB_EVENT_MASK_EXPOSURE |
            XCB_EVENT_MASK_STRUCTURE_NOTIFY |
            XCB_EVENT_MASK_POINTER_MOTION |
            XCB_EVENT_MASK_BUTTON_PRESS |
            XCB_EVENT_MASK_BUTTON_RELEASE;

    ::xcb_create_window(
            static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(),
            XCB_COPY_FROM_PARENT,
            this->xcb_window,
            static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_screen()->root,
            create_info->x, create_info->y,
            create_info->width, create_info->height,
            create_info->border_width,
            ::XCB_WINDOW_CLASS_INPUT_OUTPUT,
            static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_screen()->root_visual,
            value_mask, value_list
    );

    /* Magic code that will send notification when window is destroyed */
    xcb_intern_atom_reply_t* reply = intern_atom_helper(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), true, "WM_PROTOCOLS");
    atom_wm_delete_window = intern_atom_helper(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), false, "WM_DELETE_WINDOW");

    xcb_change_property(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), XCB_PROP_MODE_REPLACE,
                        this->xcb_window, (*reply).atom, 4, 32, 1,
                        &(*atom_wm_delete_window).atom);

    std::string windowTitle = "my title";
    xcb_change_property(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), XCB_PROP_MODE_REPLACE,
                        this->xcb_window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
                        title.size(), windowTitle.c_str());

    free(reply);

    if (create_info->full_screen)
    {
        xcb_intern_atom_reply_t *atom_wm_state = intern_atom_helper(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), false, "_NET_WM_STATE");
        xcb_intern_atom_reply_t *atom_wm_fullscreen = intern_atom_helper(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), false, "_NET_WM_STATE_FULLSCREEN");
        xcb_change_property(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(),
                            XCB_PROP_MODE_REPLACE,
                            this->xcb_window, atom_wm_state->atom,
                            XCB_ATOM_ATOM, 32, 1,
                            &(atom_wm_fullscreen->atom));
        free(atom_wm_fullscreen);
        free(atom_wm_state);
    }

    xcb_map_window(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection(), this->xcb_window);

    xcb_flush(static_cast<const ::framework::platform::linux_::xcb::connection *>(connection)->get_xcb_connection());
}

::framework::platform::linux_::xcb::window::~window()
{
    ::xcb_destroy_window(this->connection->get_xcb_connection(), this->xcb_window);
}