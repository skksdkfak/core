#include "platform/win32/connection.hpp"
#include "platform/win32/window.hpp"

::framework::platform::win32::window::window(::framework::platform::win32::connection * connection, ::framework::platform::window_create_info const * create_info) :
	connection(connection)
{
	::DWORD ex_style;
	::DWORD style;
	if (create_info->full_screen)
	{
		ex_style = WS_EX_APPWINDOW;
		style = WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		this->width = ::GetSystemMetrics(SM_CXSCREEN);
		this->height = ::GetSystemMetrics(SM_CYSCREEN);
	}
	else
	{
		ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		style = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		this->width = create_info->width;
		this->height = create_info->height;
	}

	::RECT window_rect;
	window_rect.left = 0L;
	window_rect.top = 0L;
	window_rect.right = this->width;
	window_rect.bottom = this->height;

	::AdjustWindowRectEx(&window_rect, style, FALSE, ex_style);

	this->hwnd = CreateWindowEx(
		NULL,
		"engine",
		"engine",
		style | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		0,
		0,
		window_rect.right - window_rect.left,
		window_rect.bottom - window_rect.top,
		NULL,
		NULL,
		static_cast<::framework::platform::win32::connection *>(this->connection)->get_hmodule(),
		NULL
	);

	SetWindowLongPtr(this->hwnd, GWLP_USERDATA, reinterpret_cast<::LONG_PTR>(this));

	::ShowWindow(this->hwnd, SW_SHOW);
}

::framework::platform::win32::window::~window()
{
	::DestroyWindow(this->hwnd);
}