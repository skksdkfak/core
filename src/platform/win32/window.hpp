#pragma once

#include "platform/window.hpp"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <windows.h>

namespace framework::platform::win32
{
	class connection;

	class window : public ::framework::platform::window
	{
	public:
		~window();

		::framework::platform::win32::connection const * get_connection() const;

		::std::uint16_t get_width() override;

		::std::uint16_t get_height()  override;

		::HWND get_hwnd() const;

	private:
		friend class ::framework::platform::win32::connection;

		::framework::platform::win32::window(::framework::platform::win32::connection * connection, ::framework::platform::window_create_info const * create_info);

		::framework::platform::win32::connection * connection;
		::HWND hwnd;
		::std::uint16_t width;
		::std::uint16_t height;
	};
}

#include "platform/win32/window.inl"