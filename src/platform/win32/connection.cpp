#include "platform/win32/connection.hpp"
#include "platform/win32/window.hpp"
#include <ShellScalingAPI.h>
#include <shobjidl.h> 
#include <stdexcept>
#include <Winuser.h>

::HMODULE(::framework::platform::win32::connection::hmodule) = NULL;

static ::framework::input::input_listner * input_listner;

/*::framework::input::key*/int const win32_to_hid_key[256]
{
	0,  0,  0,  0,  0,  0,  0,  0, 42, 43,  0,  0,  0, 40,  0,  0,    // 16
	225,224,226, 72, 57,  0,  0,  0,  0,  0,  ::framework::input::key::key_escape, 41,  0,  0,  0,  0,    // 32
	44, 75, 78, 77, 74, 80, 82, 79, 81,  0,  0,  0, 70, 73, 76,  0,    // 48
	39, 30, 31, 32, 33, 34, 35, 36, 37, 38,  0,  0,  0,  0,  0,  0,    // 64
	0,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18,    // 80
	19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,  0,  0,  0,  0,  0,    // 96
	98, 89, 90, 91, 92, 93, 94, 95, 96, 97, 85, 87,  0, 86, 99, 84,    //112
	58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69,104,105,106,107,    //128
	108,109,110,111,112,113,114,115,  0,  0,  0,  0,  0,  0,  0,  0,    //144
	83, 71,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,    //160
	225,229,224,228,226,230,  0,  0,  0,  0,  0,  0,  0,127,128,129,    //176    L/R shift/ctrl/alt  mute/vol+/vol-
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 51, 46, 54, 45, 55, 56,    //192
	53,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,    //208
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 47, 49, 48, 52,  0,    //224
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,    //240
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0     //256
};

::framework::input::button const win32_to_hid_button[10]
{
	::framework::input::button::button_none, ::framework::input::button::button_8, ::framework::input::button::button_9, ::framework::input::button::button_none,
	::framework::input::button::button_none, ::framework::input::button::button_none, ::framework::input::button::button_none, ::framework::input::button::button_none,
	::framework::input::button::button_none, ::framework::input::button::button_none
};

::framework::platform::win32::connection::connection()
{
	::framework::platform::win32::connection::hmodule = GetModuleHandle(0);

	::WNDCLASSEXA wndclassexa;
	wndclassexa.cbSize = sizeof(::WNDCLASSEXA);
	wndclassexa.style = CS_HREDRAW | CS_VREDRAW;
	wndclassexa.lpfnWndProc = ::framework::platform::win32::connection::wnd_proc;
	wndclassexa.cbClsExtra = 0;
	wndclassexa.cbWndExtra = 0;
	wndclassexa.hInstance = ::framework::platform::win32::connection::hmodule;
	wndclassexa.hIcon = LoadIcon(::framework::platform::win32::connection::hmodule, IDI_WINLOGO);
	wndclassexa.hIconSm = wndclassexa.hIcon;
	wndclassexa.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclassexa.hbrBackground = (::HBRUSH)::GetStockObject(BLACK_BRUSH);
	wndclassexa.lpszMenuName = NULL;
	wndclassexa.lpszClassName = "Engine";
	wndclassexa.cbSize = sizeof(::WNDCLASSEX);

	if (!RegisterClassEx(&wndclassexa))
	{
		throw ::std::runtime_error("failed to register class");
	}
}

void ::framework::platform::win32::connection::create_window(::framework::platform::window_create_info const * create_info, ::framework::platform::window ** window)
{
	*window = new ::framework::platform::win32::window(this, create_info);
}

void ::framework::platform::win32::connection::open_file_dialog(::std::wstring & file_name)
{
	if (SUCCEEDED(::CoInitializeEx(NULL, ::COINIT::COINIT_APARTMENTTHREADED | ::COINIT::COINIT_DISABLE_OLE1DDE)))
	{
		::IFileOpenDialog * file_open_dialog;
		if (SUCCEEDED(::CoCreateInstance(::CLSID_FileOpenDialog, NULL, CLSCTX_ALL, ::IID_IFileOpenDialog, reinterpret_cast<void **>(&file_open_dialog))))
		{
			if (SUCCEEDED(file_open_dialog->Show(NULL)))
			{
				::IShellItem * shell_item;
				if (SUCCEEDED(file_open_dialog->GetResult(&shell_item)))
				{
					::PWSTR file_path;
					if (SUCCEEDED(shell_item->GetDisplayName(::SIGDN::SIGDN_FILESYSPATH, &file_path)))
					{
						file_name = file_path;
						::CoTaskMemFree(file_path);
					}
					shell_item->Release();
				}
			}
			file_open_dialog->Release();
		}
		::CoUninitialize();
	}
}

void ::framework::platform::win32::connection::poll_for_event(::framework::input::input_listner * input_listner)
{
	::input_listner = input_listner;
	::MSG msg;
	while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{
		::TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void ::framework::platform::win32::connection::get_properties(::framework::platform::connection_properties & connection_properties)
{
	connection_properties.wheel_delta = WHEEL_DELTA;
}

void ::framework::platform::win32::connection::set_process_dpi_awareness()
{
	::HRESULT const hresult = ::SetProcessDpiAwareness(::PROCESS_DPI_AWARENESS::PROCESS_PER_MONITOR_DPI_AWARE);
	if (FAILED(hresult))
	{
		throw ::std::runtime_error("failed to SetProcessDpiAwareness");
	}
}

::LRESULT(::framework::platform::win32::connection::wnd_proc)(::HWND hwnd, ::UINT message, ::WPARAM wparam, ::LPARAM lparam)
{
	if (::input_listner)
	{
		switch (message)
		{
		case WM_CREATE:
			return 0;
		case WM_CLOSE:
			return 0;
		case WM_ACTIVATE:
			return 0;
		case WM_MOVE:
			return 0;
		case WM_SIZE:
		{
			::framework::platform::win32::window * window = (::framework::platform::win32::window *)GetWindowLongPtr(hwnd, GWLP_USERDATA);
			window->width = LOWORD(lparam);
			window->height = HIWORD(lparam);
			if (wparam == SIZE_MINIMIZED)
			{
			}
			else if (wparam == SIZE_MAXIMIZED)
			{
			}
			else if (wparam == SIZE_RESTORED)
			{
			}
			return 0;
		}
		case WM_MOUSEMOVE:
			::input_listner->mouse_move(LOWORD(lparam), HIWORD(lparam));
			return 0;
		case WM_LBUTTONDOWN:
			::input_listner->button_down(::framework::input::button::button_1);
			return 0;
		case WM_MBUTTONDOWN:
			::input_listner->button_down(::framework::input::button::button_2);
			return 0;
		case WM_RBUTTONDOWN:
			::input_listner->button_down(::framework::input::button::button_3);
			return 0;
		case WM_LBUTTONUP:
			::input_listner->button_up(::framework::input::button::button_1);
			return 0;
		case WM_MBUTTONUP:
			::input_listner->button_up(::framework::input::button::button_2);
			return 0;
		case WM_RBUTTONUP:
			::input_listner->button_up(::framework::input::button::button_3);
			return 0;
		case WM_XBUTTONDOWN:
			::input_listner->button_down(win32_to_hid_button[GET_XBUTTON_WPARAM(wparam)]);
			return 0;
		case WM_XBUTTONUP:
			::input_listner->button_up(win32_to_hid_button[GET_XBUTTON_WPARAM(wparam)]);
			return 0;
		case WM_KEYDOWN:
			::input_listner->key_down((::framework::input::key)win32_to_hid_key[wparam]);
			return 0;
		case WM_KEYUP:
			::input_listner->key_up((::framework::input::key)win32_to_hid_key[wparam]);
			return 0;
		case WM_MOUSEWHEEL:
			::input_listner->mouse_wheel(GET_WHEEL_DELTA_WPARAM(wparam));
			break;
		}
	}

	return ::DefWindowProcW(hwnd, message, wparam, lparam);
}