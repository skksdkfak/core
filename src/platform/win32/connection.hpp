#pragma once

#include "platform/connection.hpp"
#include "input/input_listner.hpp"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <windows.h>

namespace framework::platform::win32
{
	class connection : public ::framework::platform::connection
	{
	public:
		connection();

		void create_window(::framework::platform::window_create_info const * create_info, ::framework::platform::window ** window) override;

		void open_file_dialog(::std::wstring & file_name) override;

		void poll_for_event(::framework::input::input_listner * input_listner) override;

		void get_properties(::framework::platform::connection_properties & connection_properties) override;

		void set_process_dpi_awareness() override;

		static ::HMODULE get_hmodule() { return ::framework::platform::win32::connection::hmodule; }

	private:
		static ::LRESULT CALLBACK wnd_proc(::HWND hwnd, ::UINT nMsg, ::WPARAM wParam, ::LPARAM lParam);

		static ::HMODULE hmodule;
	};
}