inline ::framework::platform::win32::connection const * (::framework::platform::win32::window::get_connection)() const
{
	return this->connection;
}

inline ::std::uint16_t (::framework::platform::win32::window::get_width)()
{
	return this->width;
}

inline ::std::uint16_t (::framework::platform::win32::window::get_height)()
{
	return this->height;
}

inline ::HWND (::framework::platform::win32::window::get_hwnd)() const
{
	return this->hwnd;
}