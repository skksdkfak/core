#pragma once

#include <cstdint>

namespace framework::platform
{
    struct window_create_info
    {
        ::std::int16_t  x;
        ::std::int16_t  y;
        ::std::uint16_t width;
        ::std::uint16_t height;
        ::std::uint16_t border_width;
        bool            full_screen;
    };

    class window
    {
    public:
        virtual ::std::uint16_t get_width() = 0;

        virtual ::std::uint16_t get_height() = 0;
    };
}