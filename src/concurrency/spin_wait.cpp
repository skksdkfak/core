#include "concurrency/spin_wait.hpp"
#include <thread>

namespace framework::local
{
	namespace
	{
		static constexpr ::std::uint32_t yield_threshold = 10;
	}
}

::framework::concurrency::spin_wait::spin_wait() noexcept
{
	this->reset();
}

bool ::framework::concurrency::spin_wait::next_spin_will_yield() const noexcept
{
	return this->count >= ::framework::local::yield_threshold;
}

void ::framework::concurrency::spin_wait::reset() noexcept
{
	static ::std::uint32_t const initial_count = ::std::thread::hardware_concurrency() > 1 ? 0 : ::framework::local::yield_threshold;
	this->count = initial_count;
}

void ::framework::concurrency::spin_wait::spin_one() noexcept
{
	if (this->next_spin_will_yield())
	{
		::std::this_thread::yield();
	}

	++this->count;
	if (this->count == 0)
	{
		// Don't wrap around to zero as this would go back to
		// busy-waiting.
		this->count = ::framework::local::yield_threshold;
	}
}
