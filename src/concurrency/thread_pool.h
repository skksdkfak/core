#pragma once

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <atomic>

namespace framework
{
	namespace concurrency
	{
		namespace job
		{
			//typedef void entry_point(uintptr_t param);

			enum  priority
			{
				low,
				normal,
				high,
				critical
			};

			class counter
			{
			public:
				void up()
				{
					++m_counter;
				}

				void down()
				{
					--m_counter;
				}

				void wait()
				{
					while (m_counter.load(::std::memory_order::acquire))
						::std::this_thread::yield();
				}

			private:
				::std::atomic_uint m_counter = 0;
				//::std::list<fiber::fiber> wait_list;
			};

			struct declaration
			{
				::std::function<void(::std::uintptr_t param)>	entry_point;
				::std::uintptr_t		param;
				::framework::concurrency::job::priority		priority;
                ::framework::concurrency::job::counter *		counter;
			};
		};

		class job_system
		{
		public:
			void set_thread_count(::std::uint32_t count)
			{
				workers.clear();
				for (::std::uint32_t i = 0; i < count; i++)
				{
					workers.push_back(::std::make_unique<worker_thread>(*this));
				}

				for (::std::uint32_t i = 0; i < 256; i++)
				{
					//fiber_pool.emplace();
				}
			}

			void kick_job(job::declaration const & declaration)
			{
				if (declaration.counter)
					declaration.counter->up();

				::std::lock_guard<::std::mutex> lock(queue_mutex);
				job_queue.push(declaration);
				condition.notify_one();
			}

			job::declaration acquire_job()
			{
				job::declaration job_declaration;

				::std::unique_lock<::std::mutex> lock(queue_mutex);
				condition.wait(lock, [this] { return !job_queue.empty(); });
				job_declaration = job_queue.front();
				job_queue.pop();

				return job_declaration;
			}

		private:
			class worker_thread
			{
			private:
				::framework::concurrency::job_system & job_system;
				::std::thread thread;
				//fiber::fiber worker_fiber;

				void queue_loop()
				{
					while (true)
					{
						job::declaration job = job_system.acquire_job();

						job.entry_point(job.param);

						if (job.counter)
							job.counter->down();
					}
				}

			public:
				worker_thread(::framework::concurrency::job_system & job_system) : job_system(job_system)
				{
					thread = ::std::thread(&worker_thread::queue_loop, this);
				}

				~worker_thread()
				{
					thread.join();
				}
			};

			::std::vector<::std::unique_ptr<worker_thread>> workers;
			::std::queue<job::declaration> job_queue;
			::std::mutex queue_mutex;
			::std::condition_variable condition;
			//::std::queue<fiber::fiber> fiber_pool;
		};
	}
}