#pragma once

#include <cstdint>

namespace framework::concurrency
{
	class spin_wait
	{
	public:
		spin_wait() noexcept;

		bool next_spin_will_yield() const noexcept;

		void spin_one() noexcept;

		void reset() noexcept;

	private:
		::std::uint32_t count;
	};
}
