#include "concurrency/spin_mutex.hpp"
#include "concurrency/spin_wait.hpp"

::framework::concurrency::spin_mutex::spin_mutex() noexcept :
	is_locked(false)
{
}

bool ::framework::concurrency::spin_mutex::spin_mutex::try_lock() noexcept
{
	return !this->is_locked.exchange(true, ::std::memory_order::acquire);
}

void ::framework::concurrency::spin_mutex::spin_mutex::lock() noexcept
{
	::framework::concurrency::spin_wait spin_wait;
	while (!this->try_lock())
	{
		while (this->is_locked.load(::std::memory_order::relaxed))
		{
			spin_wait.spin_one();
		}
	}
}

void ::framework::concurrency::spin_mutex::spin_mutex::unlock() noexcept
{
	this->is_locked.store(false, ::std::memory_order::release);
}