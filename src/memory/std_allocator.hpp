#pragma once

#include "memory/allocator.hpp"

namespace framework::memory
{
	class std_allocator : public ::framework::memory::allocator
	{
	public:
		void * allocate(::std::size_t size) override;

		void * allocate(::std::size_t size, ::std::size_t alignment) override;

		void * reallocate(void * memory, ::std::size_t size) override;

		void * reallocate(void * memory, ::std::size_t size, ::std::size_t alignment) override;

		void deallocate(void * memory) override;
	};
}

#include "memory/std_allocator.inl"