#pragma once

#include "memory/allocator.hpp"
#include <cstdint>

namespace framework::memory
{
	class linear_allocator : public ::framework::memory::allocator
	{
	public:
		explicit linear_allocator(::std::size_t size);

		~linear_allocator();

		void * allocate(::std::size_t size) override;

		void * allocate(::std::size_t size, ::std::size_t alignment) override;

		void * reallocate(void * memory, ::std::size_t size) override;

		void * reallocate(void * memory, ::std::size_t size, ::std::size_t alignment) override;

		void deallocate(void * memory) override;

		::std::uintptr_t get_pointer() const;

		void free(::std::uintptr_t pointer);

		void clear();

	private:
		::std::byte * base;
		::std::uintptr_t offset;
		::std::size_t size;
	};

	class linear_allocator_janitor
	{
	public:
		explicit linear_allocator_janitor(::framework::memory::linear_allocator & allocator);

		~linear_allocator_janitor();

	private:
		::framework::memory::linear_allocator & stack_allocator;
		::std::size_t stack_pointer;
	};
}

#include "memory/linear_allocator.inl"