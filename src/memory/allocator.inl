template <typename T>
inline T * ::framework::memory::allocator::allocate(::std::size_t size)
{
	return reinterpret_cast<T *>(this->allocate(size * sizeof(T)));
}

template <typename T>
inline T * ::framework::memory::allocator::allocate(::std::size_t size, ::std::size_t alignment)
{
	return reinterpret_cast<T *>(this->allocate(size * sizeof(T), alignment));
}

template <typename T>
inline T * ::framework::memory::allocator::reallocate(void * memory, ::std::size_t size)
{
	return reinterpret_cast<T *>(this->reallocate(memory, size * sizeof(T)));
}

template <typename T>
inline T * ::framework::memory::allocator::reallocate(void * memory, ::std::size_t size, ::std::size_t alignment)
{
	return reinterpret_cast<T *>(this->reallocate(memory, size * sizeof(T), alignment));
}