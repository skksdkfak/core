#include <cstdlib>
#include <exception>

inline void * (::framework::memory::std_allocator::allocate)(::std::size_t size)
{
	return ::std::malloc(size);
}

inline void * (::framework::memory::std_allocator::allocate)(::std::size_t size, ::std::size_t alignment)
{
	throw ::std::exception("nuimplemented");
}

inline void * (::framework::memory::std_allocator::reallocate)(void * memory, ::std::size_t size)
{
	return ::std::realloc(memory, size);
}

inline void * (::framework::memory::std_allocator::reallocate)(void * memory, ::std::size_t size, ::std::size_t alignment)
{
	throw ::std::exception("nuimplemented");
}

inline void(::framework::memory::std_allocator::deallocate)(void * memory)
{
	return ::std::free(memory);
}
