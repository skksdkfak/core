#pragma once

#include <cstddef>

namespace framework::memory
{
	class allocator
	{
	public:
		virtual void * allocate(::std::size_t size) = 0;

		virtual void * allocate(::std::size_t size, ::std::size_t alignment) = 0;

		virtual void * reallocate(void * memory, ::std::size_t size) = 0;

		virtual void * reallocate(void * memory, ::std::size_t size, ::std::size_t alignment) = 0;

		virtual void deallocate(void * memory) = 0;

		template <typename T>
		T * allocate(::std::size_t size);

		template <typename T>
		T * allocate(::std::size_t size, ::std::size_t alignment);

		template <typename T>
		T * reallocate(void * memory, ::std::size_t size);

		template <typename T>
		T * reallocate(void * memory, ::std::size_t size, ::std::size_t alignment);
	};
}

#include "memory/allocator.inl"