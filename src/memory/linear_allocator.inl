#include <cstdlib>

inline ::framework::memory::linear_allocator::linear_allocator(::std::size_t size) :
	offset(0),
	size(size)
{
	this->base = reinterpret_cast<::std::byte *>(::std::malloc(size));
}

inline ::framework::memory::linear_allocator::~linear_allocator()
{
	::std::free(base);
}

inline void * ::framework::memory::linear_allocator::allocate(::std::size_t size)
{
	void * address = this->base + this->offset;
	this->offset += size;
	return address;
}

inline void * ::framework::memory::linear_allocator::allocate(::std::size_t size, ::std::size_t alignment)
{
	::std::size_t const align_mask = alignment - 1;
	void * address = reinterpret_cast<::std::byte *>((reinterpret_cast<::std::uintptr_t>(this->base) + this->offset + align_mask) & ~align_mask);
	this->offset += size;
	return address;
}

inline void * ::framework::memory::linear_allocator::reallocate(void * memory, ::std::size_t size)
{
	this->offset = static_cast<::std::byte *>(memory) + size - this->base;
	return memory;
}

inline void * ::framework::memory::linear_allocator::reallocate(void * memory, ::std::size_t size, ::std::size_t alignment)
{
	this->offset = static_cast<::std::byte *>(memory) + size - this->base;
	return memory;
}

inline void ::framework::memory::linear_allocator::deallocate(void * memory)
{
	if (memory < this->base + this->offset)
	{
		this->offset = static_cast<::std::byte *>(memory) - this->base;
	}
}

inline ::std::uintptr_t(::framework::memory::linear_allocator::get_pointer)() const
{
	return this->offset;
}

inline void(::framework::memory::linear_allocator::free)(::std::uintptr_t pointer)
{
	this->offset = pointer;
}

inline void(::framework::memory::linear_allocator::clear)()
{
	this->offset = 0;
}

inline ::framework::memory::linear_allocator_janitor::linear_allocator_janitor(::framework::memory::linear_allocator & allocator) :
	stack_allocator(allocator),
	stack_pointer(allocator.get_pointer())
{
}

inline ::framework::memory::linear_allocator_janitor::~linear_allocator_janitor()
{
	this->stack_allocator.free(this->stack_pointer);
}