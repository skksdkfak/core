#pragma once

#include <cstdint>

namespace framework::numeric
{
	template <typename T>
	constexpr T div_round_up(T value, T divisor);

	template <typename T>
	constexpr T previous_multiple(T value, T divisor);

	template <typename T>
	constexpr T next_multiple(T value, T divisor);

	template <typename T>
	constexpr T align_up(T value, T alignment);

	template <typename T>
	constexpr T powi(T base, T exponent);
}

#include "numeric.inl"