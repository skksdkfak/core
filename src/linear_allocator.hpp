#pragma once

#include "coroutine/task.hpp"
#include "coroutine/mutex.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include <cstddef>
#include <memory>
#include <queue>
#include <string>
#include <vector>

namespace framework
{
	class linear_allocator_page_manager;

	class linear_allocation_page
	{
	public:
		enum class type
		{
			cpu_read,
			cpu_write
		};

		linear_allocation_page(::framework::linear_allocator_page_manager & linear_allocator_page_manager, ::framework::linear_allocation_page::type page_type, ::framework::gpu::device_size page_size);

		~linear_allocation_page();

		::framework::gpu::buffer * get_buffer() const noexcept;

		::framework::gpu::device_memory * get_device_memory() const noexcept;

		void * get_mapped_buffer_data() const noexcept;

	private:
		::framework::linear_allocator_page_manager & linear_allocator_page_manager;
		::framework::gpu::buffer * buffer;
		::framework::gpu::device_memory * device_memory;
		void * mapped_buffer_data;
	};

	class linear_allocator_page_manager
	{
	public:
		linear_allocator_page_manager(::framework::gpu_context * gpu_context, ::framework::linear_allocation_page::type const page_type);

		~linear_allocator_page_manager();

		::framework::coroutine::immediate_task<::framework::linear_allocation_page *> request_page();

		::framework::linear_allocation_page * create_large_page(::framework::gpu::device_size page_size);

		::framework::coroutine::immediate_task<void> discard_pages(::std::vector<::framework::linear_allocation_page *> const & retired_pages);

		::framework::gpu::device_size get_page_size() const noexcept;

		::framework::gpu_context * get_gpu_context() const noexcept;

	private:
		::framework::gpu_context * gpu_context;
		::framework::linear_allocation_page::type page_type;
		::framework::gpu::device_size page_size;
		::std::vector<::std::unique_ptr<::framework::linear_allocation_page>> page_pool;
		::std::queue<::framework::linear_allocation_page *> available_pages;
		::framework::coroutine::mutex mutex;
	};

	class linear_allocator
	{
	public:
		struct allocation
		{
			::framework::gpu::buffer * buffer;
			::framework::gpu::device_memory * device_memory;
			::framework::gpu::device_size offset;
			::framework::gpu::device_size size;
			void * mapped_buffer_data;
		};

		linear_allocator(::framework::linear_allocator_page_manager & linear_allocator_page_manager);

		~linear_allocator();

		::framework::coroutine::immediate_task<struct ::framework::linear_allocator::allocation> allocate(::framework::gpu::device_size size, ::framework::gpu::device_size alignment);

		::framework::coroutine::immediate_task<void> cleanup_used_pages();

	private:
		::framework::linear_allocator_page_manager & linear_allocator_page_manager;
		::framework::linear_allocation_page * current_linear_allocation_page;
		::std::vector<::framework::linear_allocation_page *> retired_pages;
		::std::vector<::std::unique_ptr<::framework::linear_allocation_page>> large_pages;
		::framework::gpu::device_size current_offset;
	};
}

#include "linear_allocator.inl"