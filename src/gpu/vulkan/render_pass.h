#pragma once

#include "gpu/render_pass.h"

namespace framework::gpu::vulkan
{
	class render_pass final : public ::framework::gpu::render_pass
	{
	public:
		render_pass(::framework::gpu::vulkan::device * device, ::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkRenderPass get_vk_render_pass() const { return this->vk_render_pass; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::VkRenderPass vk_render_pass;
	};
}