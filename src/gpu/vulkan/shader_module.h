#pragma once

#include "gpu/shader_module.h"

namespace framework::gpu::vulkan
{
	class shader_module final : public ::framework::gpu::shader_module
	{
	public:
		shader_module(::framework::gpu::vulkan::device * device, ::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkShaderModule get_vk_shader_module() const { return this->vk_shader_module; }

	protected:
		friend class ::framework::gpu::vulkan::device;

		::VkShaderModule vk_shader_module;
	};
}