#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::sampler::sampler(::framework::gpu::vulkan::device * device, ::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkSamplerCreateFlags vk_sampler_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::sampler_create_flags::subsampled_bit))
        vk_sampler_create_flags = ::VkSamplerCreateFlagBits::VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT;
	if (!!(create_info->flags & ::framework::gpu::sampler_create_flags::subsampled_coarse_reconstruction_bit))
        vk_sampler_create_flags = ::VkSamplerCreateFlagBits::VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT;

	::VkSamplerMipmapMode mip_map_mode;
	switch (create_info->mip_map_mode)
	{
	case ::framework::gpu::sampler_mipmap_mode::nearest:
		mip_map_mode = ::VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_NEAREST;
		break;
	case ::framework::gpu::sampler_mipmap_mode::linear:
		mip_map_mode = ::VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_LINEAR;
		break;
	}

	::VkBorderColor border_color;
	switch (create_info->border_color)
	{
	case ::framework::gpu::border_color::float_transparent_black:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
		break;
	case ::framework::gpu::border_color::int_transparent_black:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
		break;
	case ::framework::gpu::border_color::float_opaque_black:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
		break;
	case ::framework::gpu::border_color::int_opaque_black:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		break;
	case ::framework::gpu::border_color::float_opaque_white:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		break;
	case ::framework::gpu::border_color::int_opaque_white:
		border_color = ::VkBorderColor::VK_BORDER_COLOR_INT_OPAQUE_WHITE;
		break;
	}

	::VkSamplerCreateInfo vk_sampler_create_info;
	vk_sampler_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    vk_sampler_create_info.pNext = nullptr;
    vk_sampler_create_info.flags = vk_sampler_create_flags;
    vk_sampler_create_info.magFilter = ::framework::gpu::vulkan::get_vk_filter(create_info->mag_filter);
    vk_sampler_create_info.minFilter = ::framework::gpu::vulkan::get_vk_filter(create_info->min_filter);
    vk_sampler_create_info.mipmapMode = mip_map_mode;
    vk_sampler_create_info.addressModeU = ::framework::gpu::vulkan::get_vk_sampler_address_mode(create_info->address_mode_u);
    vk_sampler_create_info.addressModeV = ::framework::gpu::vulkan::get_vk_sampler_address_mode(create_info->address_mode_v);
    vk_sampler_create_info.addressModeW = ::framework::gpu::vulkan::get_vk_sampler_address_mode(create_info->address_mode_w);
    vk_sampler_create_info.mipLodBias = create_info->mip_lod_bias;
    vk_sampler_create_info.maxAnisotropy = create_info->max_anisotropy;
    vk_sampler_create_info.anisotropyEnable = create_info->anisotropy_enable;
    vk_sampler_create_info.compareEnable = create_info->compare_enable;
    vk_sampler_create_info.compareOp = ::framework::gpu::vulkan::get_vk_compare_op(create_info->compare_op);
    vk_sampler_create_info.minLod = create_info->min_lod;
    vk_sampler_create_info.maxLod = create_info->max_lod;
    vk_sampler_create_info.borderColor = border_color;
    vk_sampler_create_info.unnormalizedCoordinates = create_info->unnormalized_coordinates;

    result = ::framework::gpu::vulkan::throw_result(::vkCreateSampler(device->get_vk_device(), &vk_sampler_create_info, nullptr, &this->vk_sampler));
}