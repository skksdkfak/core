#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::shader_module::shader_module(::framework::gpu::vulkan::device * device, ::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkResult vk_result;

	::VkShaderModuleCreateFlags vk_shader_module_create_flags = 0;

	::VkShaderModuleCreateInfo vk_shader_module_create_info;
	vk_shader_module_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	vk_shader_module_create_info.pNext = nullptr;
	vk_shader_module_create_info.flags = vk_shader_module_create_flags;
	vk_shader_module_create_info.codeSize = create_info->code_size;
	vk_shader_module_create_info.pCode = static_cast<::std::uint32_t const *>(create_info->code);

	vk_result = ::vkCreateShaderModule(device->get_vk_device(), &vk_shader_module_create_info, nullptr, &this->vk_shader_module);

	result = ::framework::gpu::vulkan::get_result(vk_result);
}