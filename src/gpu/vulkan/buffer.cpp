#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::buffer::buffer(::framework::gpu::vulkan::device * device, ::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkResult vk_result;

	::VkBufferCreateFlags vk_buffer_create_flag_bits = 0;
	if (!!(create_info->flags & ::framework::gpu::buffer_create_flags::sparse_binding_bit))
		vk_buffer_create_flag_bits = ::VkBufferCreateFlagBits::VK_BUFFER_CREATE_SPARSE_BINDING_BIT;
	if (!!(create_info->flags & ::framework::gpu::buffer_create_flags::sparse_residency_bit))
		vk_buffer_create_flag_bits = ::VkBufferCreateFlagBits::VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT;
	if (!!(create_info->flags & ::framework::gpu::buffer_create_flags::sparse_aliased_bit))
		vk_buffer_create_flag_bits = ::VkBufferCreateFlagBits::VK_BUFFER_CREATE_SPARSE_ALIASED_BIT;
	if (!!(create_info->flags & ::framework::gpu::buffer_create_flags::protected_bit))
		vk_buffer_create_flag_bits = ::VkBufferCreateFlagBits::VK_BUFFER_CREATE_PROTECTED_BIT;
	if (!!(create_info->flags & ::framework::gpu::buffer_create_flags::device_address_capture_replay_bit))
		vk_buffer_create_flag_bits = ::VkBufferCreateFlagBits::VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_EXT;

	::VkSharingMode vk_sharing_mode;
	switch (create_info->sharing_mode)
	{
	case ::framework::gpu::sharing_mode::exclusive:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
		break;
	case ::framework::gpu::sharing_mode::concurrent:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_CONCURRENT;
		break;
	}

	::VkBufferOpaqueCaptureAddressCreateInfo vk_buffer_opaque_capture_address_create_info;
	vk_buffer_opaque_capture_address_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO;
	vk_buffer_opaque_capture_address_create_info.pNext = nullptr;
	vk_buffer_opaque_capture_address_create_info.opaqueCaptureAddress = create_info->opaque_capture_address;

	::VkBufferCreateInfo vk_buffer_create_info;
	vk_buffer_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vk_buffer_create_info.pNext = &vk_buffer_opaque_capture_address_create_info;
	vk_buffer_create_info.flags = vk_buffer_create_flag_bits;
	vk_buffer_create_info.size = create_info->size;
	vk_buffer_create_info.usage = ::framework::gpu::vulkan::get_vk_buffer_usage_flags(create_info->usage);
	vk_buffer_create_info.sharingMode = vk_sharing_mode;
	vk_buffer_create_info.queueFamilyIndexCount = create_info->queue_family_index_count;
	vk_buffer_create_info.pQueueFamilyIndices = create_info->queue_family_indices;

	vk_result = ::vkCreateBuffer(device->get_vk_device(), &vk_buffer_create_info, nullptr, &this->vk_buffer);

	result = ::framework::gpu::vulkan::get_result(vk_result);
}