#pragma once

#include "gpu/query_pool.h"

namespace framework::gpu::vulkan
{
	class query_pool final : public ::framework::gpu::query_pool
	{
	public:
		query_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkQueryPool get_vk_query_pool() const { return this->vk_query_pool; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkQueryPool vk_query_pool;
	};
}