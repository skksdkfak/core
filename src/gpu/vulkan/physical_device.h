#pragma once

#include "gpu/physical_device.h"

namespace framework::gpu::vulkan
{
	class physical_device final : public ::framework::gpu::physical_device
	{
	public:
		physical_device(::framework::gpu::vulkan::instance * instance, ::framework::gpu::allocation_callbacks const & allocator, ::VkPhysicalDevice vk_physical_device);

		::framework::gpu::result create_device(::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device ** device) override;

		::framework::gpu::result enumerate_device_extension_properties(char const * layer_name, ::std::uint32_t * property_count, ::framework::gpu::extension_properties * properties) override;

		void get_features(::framework::gpu::physical_device_features * features) override;

		void get_properties(::framework::gpu::physical_device_properties * properties) override;

		void get_queue_family_properties(::std::uint32_t * queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties) override;

		void get_memory_properties(::framework::gpu::physical_device_memory_properties * physical_device_memory_properties) override;

		::framework::gpu::result get_surface_support(::std::uint32_t queue_family_index, ::framework::gpu::surface * surface, ::framework::gpu::bool32_t * supported) override;

		::framework::gpu::result get_surface_capabilities(::framework::gpu::surface * surface, ::framework::gpu::surface_capabilities * surface_capabilities) override;

		::framework::gpu::result get_surface_formats(::framework::gpu::surface * surface, ::std::uint32_t * surface_format_count, ::framework::gpu::surface_format * surface_formats) override;

		::framework::gpu::result get_cooperative_matrix_properties(::std::uint32_t * property_count, ::framework::gpu::cooperative_matrix_properties * properties) override;

		::VkPhysicalDevice get_vk_physical_device() const { return this->vk_physical_device; }

		::framework::gpu::vulkan::instance * get_instance() const { return this->instance; }

	private:
		friend class ::framework::gpu::vulkan::instance;

		::framework::gpu::vulkan::instance * instance;
		::framework::gpu::allocation_callbacks const & allocator;
		::VkPhysicalDevice vk_physical_device;
	};
}