#pragma once

#include "gpu/pipeline.h"

namespace framework::gpu::vulkan
{
	class pipeline : public ::framework::gpu::pipeline
	{
	public:
		pipeline(::framework::gpu::vulkan::device * device, ::VkPipeline vk_pipeline) noexcept;

		::VkPipeline get_vk_pipeline() const { return this->vk_pipeline; }

	protected:
		friend class ::framework::gpu::vulkan::device;

		::VkPipeline vk_pipeline;
	};
}