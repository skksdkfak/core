#pragma once

#include "gpu/queue.h"

namespace framework::gpu::vulkan
{
	class queue final : public ::framework::gpu::queue
	{
	public:
		queue(::framework::gpu::vulkan::device * device, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index, float queue_priority);

		::framework::gpu::result submit(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence) override;

		::framework::gpu::result present(::framework::gpu::present_info const * present_info) override;

		::framework::gpu::result wait_idle() override;

		::framework::gpu::result get_timestamp_period(float * timestamp_period) override;

		::VkQueue get_vk_queue() const { return this->vk_queue; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::vulkan::device * device;
		::VkQueue vk_queue;
	};
}