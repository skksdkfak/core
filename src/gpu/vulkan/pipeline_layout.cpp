#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::pipeline_layout::pipeline_layout(::framework::gpu::vulkan::device * device, ::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	if (!!(create_info->flags & ::framework::gpu::pipeline_layout_create_flags::local_bit))
	{
		result = ::framework::gpu::result::success;

		this->vk_pipeline_layout = VK_NULL_HANDLE;

		return;
	}

	::VkPipelineLayoutCreateFlags vk_pipeline_layout_create_flags = 0;

	::framework::gpu::common::unique_ptr<::VkDescriptorSetLayout[]> vk_descriptor_set_layout = ::framework::gpu::common::make_unique<::VkDescriptorSetLayout[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->descriptor_set_layout_count);
	for (::std::uint32_t i = 0; i < create_info->descriptor_set_layout_count; i++)
	{
		vk_descriptor_set_layout[i] = static_cast<::framework::gpu::vulkan::descriptor_set_layout *>(create_info->descriptor_set_layouts[i])->get_vk_descriptor_set_layout();
	}

	::framework::gpu::common::unique_ptr<::VkPushConstantRange[]> vk_pushconstant_ranges = ::framework::gpu::common::make_unique<::VkPushConstantRange[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->push_constant_range_count);
	for (::std::uint32_t i = 0; i < create_info->push_constant_range_count; i++)
	{
		m_pPushConstantRangeOffsets[create_info->push_constant_ranges[i].binding] = create_info->push_constant_ranges[i].offset;

		vk_pushconstant_ranges[i].stageFlags = get_vk_shader_stage_flags(create_info->push_constant_ranges[i].stage_flags);
		vk_pushconstant_ranges[i].offset = create_info->push_constant_ranges[i].offset;
		vk_pushconstant_ranges[i].size = create_info->push_constant_ranges[i].size;
	}

	::VkPipelineLayoutCreateInfo vk_pipeline_layout_create_info;
	vk_pipeline_layout_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	vk_pipeline_layout_create_info.pNext = nullptr;
	vk_pipeline_layout_create_info.flags = vk_pipeline_layout_create_flags;
	vk_pipeline_layout_create_info.setLayoutCount = create_info->descriptor_set_layout_count;
	vk_pipeline_layout_create_info.pSetLayouts = vk_descriptor_set_layout.get();
	vk_pipeline_layout_create_info.pushConstantRangeCount = create_info->push_constant_range_count;
	vk_pipeline_layout_create_info.pPushConstantRanges = vk_pushconstant_ranges.get();

	::VkResult vk_result = ::vkCreatePipelineLayout(device->get_vk_device(), &vk_pipeline_layout_create_info, nullptr, &this->vk_pipeline_layout);

	result = ::framework::gpu::vulkan::throw_result(vk_result);
}