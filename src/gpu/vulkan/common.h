#pragma once

namespace framework::gpu::vulkan
{
	class instance;
	class physical_device;
	class device;
	class queue;
	class semaphore;
	class command_buffer;
	class fence;
	class device_memory;
	class buffer;
	class image;
	class buffer_view;
	class image_view;
	class shader_module;
	class pipeline_cache;
	class pipeline_layout;
	class render_pass;
	class pipeline;
	class descriptor_set_layout;
	class sampler;
	class descriptor_pool;
	class descriptor_set;
	class frame_buffer;
	class command_pool;
	class swap_chain;
	class surface;
	class acceleration_structure_geometry_set;
	class acceleration_structure;
}