#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::graphics_pipeline::graphics_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::graphics_pipeline_create_info const * create_info, ::VkPipeline vk_pipeline) :
	::framework::gpu::vulkan::pipeline(device, vk_pipeline)
{
}