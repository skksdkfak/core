#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::ray_tracing_pipeline::ray_tracing_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::ray_tracing_pipeline_create_info const * create_info, ::VkPipeline vk_pipeline) :
	::framework::gpu::vulkan::pipeline(device, vk_pipeline)
{
}