#pragma once

#include "gpu/sampler.h"

namespace framework::gpu::vulkan
{
	class sampler final : public ::framework::gpu::sampler
	{
	public:
		sampler(::framework::gpu::vulkan::device * device, ::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkSampler get_vk_sampler() const { return this->vk_sampler; }
		
	public:
		friend class ::framework::gpu::vulkan::device;

		::VkSampler vk_sampler;
	};
}