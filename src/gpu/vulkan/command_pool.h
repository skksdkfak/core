#pragma once

#include "gpu/command_pool.h"
#include <unordered_set>

namespace framework::gpu::vulkan
{
	class command_buffer;

	class command_pool final : public ::framework::gpu::command_pool
	{
	public:
		command_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

        ~command_pool();

        void allocate_command_buffer(::framework::gpu::vulkan::command_buffer * command_buffer);

		void free_command_buffer(::framework::gpu::vulkan::command_buffer * command_buffer);

		::VkCommandPool get_vk_command_pool() const { return this->vk_command_pool; }

		::framework::gpu::allocation_callbacks const & get_allocation_callbacks() { return this->allocator; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::VkCommandPool vk_command_pool;
        ::std::unordered_set<::framework::gpu::vulkan::command_buffer *, ::std::hash<::framework::gpu::vulkan::command_buffer *>, ::std::equal_to<>, ::framework::gpu::common::allocator<::framework::gpu::vulkan::command_buffer *>> allocated_command_buffers;
	};
}