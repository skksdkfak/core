#include "gpu/vulkan/core.hpp"
#include <cstdint>

::framework::gpu::vulkan::acceleration_structure::acceleration_structure(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
    allocator(allocator)
{
    ::VkResult vk_result;

    ::VkAccelerationStructureCreateInfoKHR vk_acceleration_structure_create_info;
    vk_acceleration_structure_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    vk_acceleration_structure_create_info.pNext = nullptr;
    vk_acceleration_structure_create_info.createFlags = ::framework::gpu::vulkan::get_vk_acceleration_structure_create_flags(create_info->create_flags);
    vk_acceleration_structure_create_info.buffer = static_cast<::framework::gpu::vulkan::buffer *>(create_info->buffer)->get_vk_buffer();
    vk_acceleration_structure_create_info.offset = create_info->offset;
    vk_acceleration_structure_create_info.size = create_info->size;
    vk_acceleration_structure_create_info.type = ::framework::gpu::vulkan::get_vk_acceleration_structure_type(create_info->type);
    vk_acceleration_structure_create_info.deviceAddress = create_info->device_address;

    vk_result = device->get_dynamic_library().vkCreateAccelerationStructureKHR(device->get_vk_device(), &vk_acceleration_structure_create_info, nullptr, &this->vk_acceleration_structure);

    result = ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::vulkan::acceleration_structure::~acceleration_structure()
{
}