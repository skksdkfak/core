#include "gpu/vulkan/core.hpp"
#include <cstring>

::framework::gpu::vulkan::swap_chain::swap_chain(::framework::gpu::vulkan::device * device, ::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	device(device),
	images(nullptr, { *this })
{
	::VkSwapchainCreateFlagsKHR vk_swapchain_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::swap_chain_create_flags::split_instance_bind_regions_bit))
		vk_swapchain_create_flags |= ::VkSwapchainCreateFlagBitsKHR::VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR;
	if (!!(create_info->flags & ::framework::gpu::swap_chain_create_flags::protected_bit))
		vk_swapchain_create_flags |= ::VkSwapchainCreateFlagBitsKHR::VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR;
	if (!!(create_info->flags & ::framework::gpu::swap_chain_create_flags::mutable_format_bit))
		vk_swapchain_create_flags |= ::VkSwapchainCreateFlagBitsKHR::VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR;

	::VkSharingMode vk_sharing_mode;
	switch (create_info->image_sharing_mode)
	{
	case ::framework::gpu::sharing_mode::exclusive:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
		break;
	case ::framework::gpu::sharing_mode::concurrent:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_CONCURRENT;
		break;
	default:
		assert(false);
		break;
	}

	::VkSurfaceTransformFlagBitsKHR vk_surface_transform_flag_bits;
	switch (create_info->pre_transform)
	{
	case ::framework::gpu::surface_transform_flags::identity_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::rotate_90_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::rotate_180_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::rotate_270_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::horizontal_mirror_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_90_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_180_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_270_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR;
		break;
	case ::framework::gpu::surface_transform_flags::inherit_bit:
		vk_surface_transform_flag_bits = ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR;
		break;
	default:
		assert(false);
		break;
	}

	::VkCompositeAlphaFlagBitsKHR vk_composite_alpha_flag_bits;
	switch (create_info->composite_alpha)
	{
	case ::framework::gpu::composite_alpha_flags::opaque_bit:
		vk_composite_alpha_flag_bits = ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		break;
	case ::framework::gpu::composite_alpha_flags::pre_multiplied_bit:
		vk_composite_alpha_flag_bits = ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR;
		break;
	case ::framework::gpu::composite_alpha_flags::post_multiplied_bit:
		vk_composite_alpha_flag_bits = ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR;
		break;
	case ::framework::gpu::composite_alpha_flags::inherit_bit:
		vk_composite_alpha_flag_bits = ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
		break;
	default:
		assert(false);
		break;
	}

	::VkPresentModeKHR vk_present_mode;
	switch (create_info->present_mode)
	{
	case ::framework::gpu::present_mode::immediate:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_IMMEDIATE_KHR;
		break;
	case ::framework::gpu::present_mode::mailbox:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_MAILBOX_KHR;
		break;
	case ::framework::gpu::present_mode::fifo:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_FIFO_KHR;
		break;
	case ::framework::gpu::present_mode::fifo_relaxed:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_FIFO_RELAXED_KHR;
		break;
	case ::framework::gpu::present_mode::shared_demand_refresh:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR;
		break;
	case ::framework::gpu::present_mode::shared_continuous_refresh:
		vk_present_mode = ::VkPresentModeKHR::VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR;
		break;
	default:
		assert(false);
		break;
	}

	::VkSwapchainCreateInfoKHR vk_swapchain_create_info_khr;
	vk_swapchain_create_info_khr.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	vk_swapchain_create_info_khr.pNext = nullptr;
	vk_swapchain_create_info_khr.flags = vk_swapchain_create_flags;
	vk_swapchain_create_info_khr.surface = static_cast<::framework::gpu::vulkan::surface *>(create_info->surface)->get_vk_surface();
	vk_swapchain_create_info_khr.minImageCount = create_info->min_image_count;
	vk_swapchain_create_info_khr.imageFormat = ::framework::gpu::vulkan::get_vk_format(create_info->image_format);
	vk_swapchain_create_info_khr.imageColorSpace = ::framework::gpu::vulkan::get_vk_color_space(create_info->image_color_space);
	vk_swapchain_create_info_khr.imageExtent = { create_info->image_extent.width, create_info->image_extent.height };
	vk_swapchain_create_info_khr.imageArrayLayers = create_info->image_array_layers;
	vk_swapchain_create_info_khr.imageUsage = ::framework::gpu::vulkan::get_vk_image_usage_flags(create_info->image_usage);
	vk_swapchain_create_info_khr.imageSharingMode = vk_sharing_mode;
	vk_swapchain_create_info_khr.queueFamilyIndexCount = create_info->queue_family_index_count;
	vk_swapchain_create_info_khr.pQueueFamilyIndices = create_info->queue_family_indices;
	vk_swapchain_create_info_khr.preTransform = vk_surface_transform_flag_bits;
	vk_swapchain_create_info_khr.compositeAlpha = vk_composite_alpha_flag_bits;
	vk_swapchain_create_info_khr.presentMode = vk_present_mode;
	vk_swapchain_create_info_khr.clipped = static_cast<::VkBool32>(create_info->clipped);
	vk_swapchain_create_info_khr.oldSwapchain = create_info->old_swap_chain ? static_cast<::framework::gpu::vulkan::swap_chain *>(create_info->old_swap_chain)->get_vk_swapchain() : VK_NULL_HANDLE;
	result = ::framework::gpu::vulkan::throw_result(::vkCreateSwapchainKHR(static_cast<::framework::gpu::vulkan::device const *>(device)->get_vk_device(), &vk_swapchain_create_info_khr, nullptr, &this->vk_swapchain));

	result = ::framework::gpu::vulkan::throw_result(::vkGetSwapchainImagesKHR(static_cast<::framework::gpu::vulkan::device const *>(device)->get_vk_device(), this->vk_swapchain, &this->image_count, nullptr));

	::framework::gpu::common::unique_ptr<::VkImage[]> vk_images = ::framework::gpu::common::make_unique<::VkImage[]>(allocator, ::framework::gpu::system_allocation_scope::command, this->image_count);
	result = ::framework::gpu::vulkan::throw_result(::vkGetSwapchainImagesKHR(static_cast<::framework::gpu::vulkan::device const *>(device)->get_vk_device(), this->vk_swapchain, &this->image_count, vk_images.get()));

	this->images.reset(::new (::framework::gpu::common::allocate<::framework::gpu::vulkan::image * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->image_count)) ::framework::gpu::vulkan::image * [this->image_count]{});

	for (::std::uint32_t i = 0; i < this->image_count; i++)
	{
		::framework::gpu::vulkan::image * image;

		::framework::gpu::image_create_info image_create_info;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = create_info->image_format;
		image_create_info.extent.width = create_info->image_extent.width;
		image_create_info.extent.height = create_info->image_extent.height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = create_info->image_array_layers;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = create_info->image_usage;
		image_create_info.sharing_mode = create_info->image_sharing_mode;
		image_create_info.queue_family_index_count = create_info->queue_family_index_count;
		image_create_info.queue_family_indices = create_info->queue_family_indices;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		result = device->create_image(&image_create_info, vk_images[i], &allocator, &this->images[i]);
	}
}

void ::framework::gpu::vulkan::swap_chain::get_images(::std::uint32_t * image_count, ::framework::gpu::image ** images) const
{
	if (images)
	{
		::std::memcpy(images, this->images.get(), sizeof(::framework::gpu::image *) * *image_count);
	}
	else
	{
		*image_count = this->image_count;
	}
}