#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::frame_buffer::frame_buffer(::framework::gpu::vulkan::device * device, ::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkFramebufferCreateFlags vk_framebuffer_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::frame_buffer_create_flags::imageless_bit))
		vk_framebuffer_create_flags |= ::VkFramebufferCreateFlagBits::VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT;

	::framework::gpu::common::unique_ptr<::VkImageView[]> attachments = ::framework::gpu::common::make_unique<::VkImageView[]>(allocator, ::framework::gpu::system_allocation_scope::command, create_info->attachment_count);
	for (::std::uint32_t i = 0; i < create_info->attachment_count; i++)
	{
		attachments[i] = static_cast<::framework::gpu::vulkan::image_view const *>(create_info->attachments[i].view)->get_vk_image_view();
	}

	::VkFramebufferCreateInfo vk_framebuffer_create_info;
	vk_framebuffer_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	vk_framebuffer_create_info.pNext = nullptr;
	vk_framebuffer_create_info.flags = vk_framebuffer_create_flags;
	vk_framebuffer_create_info.renderPass = static_cast<::framework::gpu::vulkan::render_pass const *>(create_info->render_pass)->get_vk_render_pass();
	vk_framebuffer_create_info.attachmentCount = create_info->attachment_count;
	vk_framebuffer_create_info.pAttachments = attachments.get();
	vk_framebuffer_create_info.width = create_info->width;
	vk_framebuffer_create_info.height = create_info->height;
	vk_framebuffer_create_info.layers = create_info->layers;
	result = ::framework::gpu::vulkan::throw_result(::vkCreateFramebuffer(device->get_vk_device(), &vk_framebuffer_create_info, nullptr, &vk_framebuffer));
}