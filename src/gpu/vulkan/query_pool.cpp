#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::query_pool::query_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkResult vk_result;

	::VkQueryPoolCreateFlags vk_query_pool_create_flags = 0;

	::VkQueryType vk_query_type;
	switch (create_info->query_type)
	{
	case ::framework::gpu::query_type::occlusion:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_OCCLUSION;
		break;
	case ::framework::gpu::query_type::pipeline_statistics:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_PIPELINE_STATISTICS;
		break;
	case ::framework::gpu::query_type::timestamp:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_TIMESTAMP;
		break;
	case ::framework::gpu::query_type::transform_feedback_stream:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_TRANSFORM_FEEDBACK_STREAM_EXT;
		break;
	case ::framework::gpu::query_type::performance_query:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_PERFORMANCE_QUERY_KHR;
		break;
	case ::framework::gpu::query_type::acceleration_structure_compacted_size:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR;
		break;
	case ::framework::gpu::query_type::acceleration_structure_serialization_size:
		vk_query_type = ::VkQueryType::VK_QUERY_TYPE_ACCELERATION_STRUCTURE_SERIALIZATION_SIZE_KHR;
		break;
	}

	::VkQueryPipelineStatisticFlags vk_query_pipeline_statistic_flags = 0;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::input_assembly_vertices_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::input_assembly_primitives_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::vertex_shader_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::geometry_shader_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::geometry_shader_primitives_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::clipping_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::clipping_primitives_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::fragment_shader_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::tessellation_control_shader_patches_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::tessellation_evaluation_shader_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT;
	if (!!(create_info->pipeline_statistics & ::framework::gpu::query_pipeline_statistic_flags::compute_shader_invocations_bit))
		vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT;

	::VkQueryPoolCreateInfo vk_query_pool_create_info;
	vk_query_pool_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
	vk_query_pool_create_info.pNext = nullptr;
	vk_query_pool_create_info.flags = vk_query_pool_create_flags;
	vk_query_pool_create_info.queryType = vk_query_type;
	vk_query_pool_create_info.queryCount = create_info->query_count;
	vk_query_pool_create_info.pipelineStatistics = vk_query_pipeline_statistic_flags;

	vk_result = ::vkCreateQueryPool(device->get_vk_device(), &vk_query_pool_create_info, nullptr, &this->vk_query_pool);

	result = ::framework::gpu::vulkan::get_result(vk_result);
}