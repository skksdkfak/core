#pragma once

#include "gpu/deferred_operation.h"

namespace framework::gpu::vulkan
{
	class deferred_operation final : public ::framework::gpu::deferred_operation
	{
	public:
		deferred_operation(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::result & result);

		~deferred_operation();

		::VkDeferredOperationKHR get_vk_deferred_operation() const { return this->vk_deferred_operation; }
		
	public:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks allocator;
		::VkDeferredOperationKHR vk_deferred_operation;
	};
}