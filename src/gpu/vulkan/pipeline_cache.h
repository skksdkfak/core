#pragma once

#include "gpu/pipeline_cache.h"

namespace framework::gpu::vulkan
{
	class pipeline_cache : public ::framework::gpu::pipeline_cache
	{
	public:
		pipeline_cache(::framework::gpu::vulkan::device * device, ::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkPipelineCache get_vk_pipeline_cache() const { return this->vk_pipeline_cache; }

	protected:
		friend class ::framework::gpu::vulkan::device;

		::VkPipelineCache vk_pipeline_cache;
	};
}