#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::render_pass_state::render_pass_state(::framework::gpu::vulkan::device * device, ::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	this->info.render_pass = static_cast<::framework::gpu::vulkan::render_pass const *>(create_info->render_pass);
	this->info.clear_values = ::framework::gpu::common::make_unique<::VkClearValue[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->max_clear_value_count);

	result = ::framework::gpu::result::success;
}

void ::framework::gpu::vulkan::render_pass_state::set_state(::framework::gpu::render_pass_state_info const & render_pass_state_info)
{
	this->info.frame_buffer = static_cast<::framework::gpu::vulkan::frame_buffer const *>(render_pass_state_info.frame_buffer);
	this->info.render_area.offset.x = render_pass_state_info.render_area.offset.x;
	this->info.render_area.offset.y = render_pass_state_info.render_area.offset.y;
	this->info.render_area.extent.width = render_pass_state_info.render_area.extent.width;
	this->info.render_area.extent.height = render_pass_state_info.render_area.extent.height;
	this->info.clear_value_count = render_pass_state_info.clear_value_count;
	for (::std::uint32_t i = 0; i < render_pass_state_info.clear_value_count; i++)
	{
		this->info.clear_values[i] = ::framework::gpu::vulkan::get_vk_clear_value(render_pass_state_info.clear_values[i]);
	}
}