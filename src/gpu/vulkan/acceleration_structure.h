#pragma once

#include "gpu/acceleration_structure.h"

namespace framework::gpu::vulkan
{
	class acceleration_structure final : public ::framework::gpu::acceleration_structure
	{
	public:
		acceleration_structure(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~acceleration_structure();

		::VkAccelerationStructureKHR get_vk_acceleration_structure() const { return this->vk_acceleration_structure; }
		
	public:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::VkAccelerationStructureKHR vk_acceleration_structure;
	};
}