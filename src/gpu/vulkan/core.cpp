#include "gpu/vulkan/core.hpp"

::framework::gpu::result(::framework::gpu::vulkan::get_result)(::VkResult vk_result)
{
	::framework::gpu::result result;

	switch (vk_result)
	{
	case ::VkResult::VK_SUCCESS:
		result = ::framework::gpu::result::success;
		break;
	case ::VkResult::VK_NOT_READY:
		result = ::framework::gpu::result::not_ready;
		break;
	case ::VkResult::VK_TIMEOUT:
		result = ::framework::gpu::result::timeout;
		break;
	case ::VkResult::VK_EVENT_SET:
		result = ::framework::gpu::result::event_set;
		break;
	case ::VkResult::VK_EVENT_RESET:
		result = ::framework::gpu::result::event_reset;
		break;
	case ::VkResult::VK_INCOMPLETE:
		result = ::framework::gpu::result::incomplete;
		break;
	case ::VkResult::VK_ERROR_OUT_OF_HOST_MEMORY:
		result = ::framework::gpu::result::error_out_of_host_memory;
		break;
	case ::VkResult::VK_ERROR_OUT_OF_DEVICE_MEMORY:
		result = ::framework::gpu::result::error_out_of_device_memory;
		break;
	case ::VkResult::VK_ERROR_INITIALIZATION_FAILED:
		result = ::framework::gpu::result::error_initialization_failed;
		break;
	case ::VkResult::VK_ERROR_DEVICE_LOST:
		result = ::framework::gpu::result::error_device_lost;
		break;
	case ::VkResult::VK_ERROR_MEMORY_MAP_FAILED:
		result = ::framework::gpu::result::error_memory_map_failed;
		break;
	case ::VkResult::VK_ERROR_LAYER_NOT_PRESENT:
		result = ::framework::gpu::result::error_layer_not_present;
		break;
	case ::VkResult::VK_ERROR_EXTENSION_NOT_PRESENT:
		result = ::framework::gpu::result::error_extension_not_present;
		break;
	case ::VkResult::VK_ERROR_FEATURE_NOT_PRESENT:
		result = ::framework::gpu::result::error_feature_not_present;
		break;
	case ::VkResult::VK_ERROR_INCOMPATIBLE_DRIVER:
		result = ::framework::gpu::result::error_incompatible_driver;
		break;
	case ::VkResult::VK_ERROR_TOO_MANY_OBJECTS:
		result = ::framework::gpu::result::error_too_many_objects;
		break;
	case ::VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED:
		result = ::framework::gpu::result::error_format_not_supported;
		break;
	case ::VkResult::VK_ERROR_FRAGMENTED_POOL:
		result = ::framework::gpu::result::error_fragmented_pool;
		break;
	case ::VkResult::VK_ERROR_SURFACE_LOST_KHR:
		result = ::framework::gpu::result::error_surface_lost;
		break;
	case ::VkResult::VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
		result = ::framework::gpu::result::error_native_window_in_use;
		break;
	case ::VkResult::VK_SUBOPTIMAL_KHR:
		result = ::framework::gpu::result::suboptimal;
		break;
	case ::VkResult::VK_ERROR_OUT_OF_DATE_KHR:
		result = ::framework::gpu::result::error_out_of_date;
		break;
	case ::VkResult::VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
		result = ::framework::gpu::result::error_incompatible_display;
		break;
	case ::VkResult::VK_ERROR_VALIDATION_FAILED_EXT:
		result = ::framework::gpu::result::error_validation_failed;
		break;
	case ::VkResult::VK_ERROR_INVALID_SHADER_NV:
		result = ::framework::gpu::result::error_invalid_shader;
		break;
	case ::VkResult::VK_ERROR_OUT_OF_POOL_MEMORY_KHR:
		result = ::framework::gpu::result::error_out_of_pool_memory;
		break;
	case ::VkResult::VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR:
		result = ::framework::gpu::result::error_invalid_external_handle;
		break;
	case ::VkResult::VK_ERROR_NOT_PERMITTED_EXT:
		result = ::framework::gpu::result::error_not_permitted;
		break;
	default:
		result = ::framework::gpu::result::success;
		break;
	}

	return result;
}

::VkFormat(::framework::gpu::vulkan::get_vk_format)(::framework::gpu::format format)
{
	switch (format)
	{
	case ::framework::gpu::format::undefined:
		return ::VkFormat::VK_FORMAT_UNDEFINED;
	case ::framework::gpu::format::r4g4_unorm_pack8:
		break;
	case ::framework::gpu::format::r4g4b4a4_unorm_pack16:
		break;
	case ::framework::gpu::format::b4g4r4a4_unorm_pack16:
		break;
	case ::framework::gpu::format::r5g6b5_unorm_pack16:
		break;
	case ::framework::gpu::format::b5g6r5_unorm_pack16:
		break;
	case ::framework::gpu::format::r5g5b5a1_unorm_pack16:
		break;
	case ::framework::gpu::format::b5g5r5a1_unorm_pack16:
		break;
	case ::framework::gpu::format::a1r5g5b5_unorm_pack16:
		break;
	case ::framework::gpu::format::r8_unorm:
		return ::VkFormat::VK_FORMAT_R8_UNORM;
	case ::framework::gpu::format::r8_snorm:
		return ::VkFormat::VK_FORMAT_R8_SNORM;
	case ::framework::gpu::format::r8_uscaled:
		return ::VkFormat::VK_FORMAT_R8_USCALED;
	case ::framework::gpu::format::r8_sscaled:
		return ::VkFormat::VK_FORMAT_R8_SSCALED;
	case ::framework::gpu::format::r8_uint:
		return ::VkFormat::VK_FORMAT_R8_UINT;
	case ::framework::gpu::format::r8_sint:
		return ::VkFormat::VK_FORMAT_R8_SINT;
	case ::framework::gpu::format::r8_srgb:
		return ::VkFormat::VK_FORMAT_R8_SRGB;
	case ::framework::gpu::format::r8g8_unorm:
		break;
	case ::framework::gpu::format::r8g8_snorm:
		break;
	case ::framework::gpu::format::r8g8_uscaled:
		break;
	case ::framework::gpu::format::r8g8_sscaled:
		break;
	case ::framework::gpu::format::r8g8_uint:
		break;
	case ::framework::gpu::format::r8g8_sint:
		break;
	case ::framework::gpu::format::r8g8_srgb:
		break;
	case ::framework::gpu::format::r8g8b8_unorm:
		return ::VkFormat::VK_FORMAT_R8G8B8_UNORM;
	case ::framework::gpu::format::r8g8b8_snorm:
		break;
	case ::framework::gpu::format::r8g8b8_uscaled:
		break;
	case ::framework::gpu::format::r8g8b8_sscaled:
		break;
	case ::framework::gpu::format::r8g8b8_uint:
		break;
	case ::framework::gpu::format::r8g8b8_sint:
		break;
	case ::framework::gpu::format::r8g8b8_srgb:
		break;
	case ::framework::gpu::format::b8g8r8_unorm:
		break;
	case ::framework::gpu::format::b8g8r8_snorm:
		break;
	case ::framework::gpu::format::b8g8r8_uscaled:
		break;
	case ::framework::gpu::format::b8g8r8_sscaled:
		break;
	case ::framework::gpu::format::b8g8r8_uint:
		break;
	case ::framework::gpu::format::b8g8r8_sint:
		break;
	case ::framework::gpu::format::b8g8r8_srgb:
		break;
	case ::framework::gpu::format::r8g8b8a8_unorm:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_UNORM;
	case ::framework::gpu::format::r8g8b8a8_snorm:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_SNORM;
	case ::framework::gpu::format::r8g8b8a8_uscaled:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_USCALED;
	case ::framework::gpu::format::r8g8b8a8_sscaled:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_SSCALED;
	case ::framework::gpu::format::r8g8b8a8_uint:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_UINT;
	case ::framework::gpu::format::r8g8b8a8_sint:
		return ::VkFormat::VK_FORMAT_R8G8B8A8_SINT;
	case ::framework::gpu::format::r8g8b8a8_srgb:
		return ::VkFormat::VK_FORMAT_B8G8R8A8_SRGB;
	case ::framework::gpu::format::b8g8r8a8_unorm:
		return ::VkFormat::VK_FORMAT_B8G8R8A8_UNORM;
	case ::framework::gpu::format::b8g8r8a8_snorm:
		break;
	case ::framework::gpu::format::b8g8r8a8_uscaled:
		break;
	case ::framework::gpu::format::b8g8r8a8_sscaled:
		break;
	case ::framework::gpu::format::b8g8r8a8_uint:
		break;
	case ::framework::gpu::format::b8g8r8a8_sint:
		break;
	case ::framework::gpu::format::b8g8r8a8_srgb:
		break;
	case ::framework::gpu::format::a8b8g8r8_unorm_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_snorm_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_uscaled_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_sscaled_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_uint_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_sint_pack32:
		break;
	case ::framework::gpu::format::a8b8g8r8_srgb_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_unorm_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_snorm_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_uscaled_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_sscaled_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_uint_pack32:
		break;
	case ::framework::gpu::format::a2r10g10b10_sint_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_unorm_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_snorm_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_uscaled_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_sscaled_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_uint_pack32:
		break;
	case ::framework::gpu::format::a2b10g10r10_sint_pack32:
		break;
	case ::framework::gpu::format::r16_unorm:
		return ::VkFormat::VK_FORMAT_R16_UNORM;
	case ::framework::gpu::format::r16_snorm:
		return ::VkFormat::VK_FORMAT_R16_SNORM;
	case ::framework::gpu::format::r16_uscaled:
		return ::VkFormat::VK_FORMAT_R16_USCALED;
	case ::framework::gpu::format::r16_sscaled:
		return ::VkFormat::VK_FORMAT_R16_SSCALED;
	case ::framework::gpu::format::r16_uint:
		return ::VkFormat::VK_FORMAT_R16_UINT;
	case ::framework::gpu::format::r16_sint:
		return ::VkFormat::VK_FORMAT_R16_SINT;
	case ::framework::gpu::format::r16_sfloat:
		return ::VkFormat::VK_FORMAT_R16_SFLOAT;
	case ::framework::gpu::format::r16g16_unorm:
		return ::VkFormat::VK_FORMAT_R16G16_UNORM;
	case ::framework::gpu::format::r16g16_snorm:
		return ::VkFormat::VK_FORMAT_R16G16_SNORM;
	case ::framework::gpu::format::r16g16_uscaled:
		return ::VkFormat::VK_FORMAT_R16G16_USCALED;
	case ::framework::gpu::format::r16g16_sscaled:
		return ::VkFormat::VK_FORMAT_R16G16_SSCALED;
	case ::framework::gpu::format::r16g16_uint:
		return ::VkFormat::VK_FORMAT_R16G16_UINT;
	case ::framework::gpu::format::r16g16_sint:
		return ::VkFormat::VK_FORMAT_R16G16_SINT;
	case ::framework::gpu::format::r16g16_sfloat:
		return ::VkFormat::VK_FORMAT_R16G16_SFLOAT;
	case ::framework::gpu::format::r16g16b16_unorm:
		return ::VkFormat::VK_FORMAT_R16G16B16_UNORM;
	case ::framework::gpu::format::r16g16b16_snorm:
		return ::VkFormat::VK_FORMAT_R16G16B16_SNORM;
	case ::framework::gpu::format::r16g16b16_uscaled:
		return ::VkFormat::VK_FORMAT_R16G16B16_USCALED;
	case ::framework::gpu::format::r16g16b16_sscaled:
		return ::VkFormat::VK_FORMAT_R16G16B16_SSCALED;
	case ::framework::gpu::format::r16g16b16_uint:
		return ::VkFormat::VK_FORMAT_R16G16B16_UINT;
	case ::framework::gpu::format::r16g16b16_sint:
		return ::VkFormat::VK_FORMAT_R16G16B16_SINT;
	case ::framework::gpu::format::r16g16b16_sfloat:
		return ::VkFormat::VK_FORMAT_R16G16B16_SFLOAT;
	case ::framework::gpu::format::r16g16b16a16_unorm:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_UNORM;
	case ::framework::gpu::format::r16g16b16a16_snorm:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_SNORM;
	case ::framework::gpu::format::r16g16b16a16_uscaled:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_USCALED;
	case ::framework::gpu::format::r16g16b16a16_sscaled:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_SSCALED;
	case ::framework::gpu::format::r16g16b16a16_uint:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_UINT;
	case ::framework::gpu::format::r16g16b16a16_sint:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_SINT;
	case ::framework::gpu::format::r16g16b16a16_sfloat:
		return ::VkFormat::VK_FORMAT_R16G16B16A16_SFLOAT;
	case ::framework::gpu::format::r32_uint:
		return ::VkFormat::VK_FORMAT_R32_UINT;
	case ::framework::gpu::format::r32_sint:
		return ::VkFormat::VK_FORMAT_R32_SINT;
	case ::framework::gpu::format::r32_sfloat:
		return ::VkFormat::VK_FORMAT_R32_SFLOAT;
	case ::framework::gpu::format::r32g32_uint:
		return ::VkFormat::VK_FORMAT_R32G32_UINT;
	case ::framework::gpu::format::r32g32_sint:
		return ::VkFormat::VK_FORMAT_R32G32_SINT;
	case ::framework::gpu::format::r32g32_sfloat:
		return ::VkFormat::VK_FORMAT_R32G32_SFLOAT;
	case ::framework::gpu::format::r32g32b32_uint:
		break;
	case ::framework::gpu::format::r32g32b32_sint:
		break;
	case ::framework::gpu::format::r32g32b32_sfloat:
		return ::VkFormat::VK_FORMAT_R32G32B32_SFLOAT;
		break;
	case ::framework::gpu::format::r32g32b32a32_uint:
		return ::VkFormat::VK_FORMAT_R32G32B32A32_UINT;
		break;
	case ::framework::gpu::format::r32g32b32a32_sint:
		break;
	case ::framework::gpu::format::r32g32b32a32_sfloat:
		return ::VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT;
	case ::framework::gpu::format::r64_uint:
		break;
	case ::framework::gpu::format::r64_sint:
		break;
	case ::framework::gpu::format::r64_sfloat:
		break;
	case ::framework::gpu::format::r64g64_uint:
		break;
	case ::framework::gpu::format::r64g64_sint:
		break;
	case ::framework::gpu::format::r64g64_sfloat:
		break;
	case ::framework::gpu::format::r64g64b64_uint:
		break;
	case ::framework::gpu::format::r64g64b64_sint:
		break;
	case ::framework::gpu::format::r64g64b64_sfloat:
		break;
	case ::framework::gpu::format::r64g64b64a64_uint:
		break;
	case ::framework::gpu::format::r64g64b64a64_sint:
		break;
	case ::framework::gpu::format::r64g64b64a64_sfloat:
		break;
	case ::framework::gpu::format::b10g11r11_ufloat_pack32:
		break;
	case ::framework::gpu::format::e5b9g9r9_ufloat_pack32:
		break;
	case ::framework::gpu::format::d16_unorm:
        return ::VkFormat::VK_FORMAT_D16_UNORM;
	case ::framework::gpu::format::x8_d24_unorm_pack32:
        return ::VkFormat::VK_FORMAT_X8_D24_UNORM_PACK32;
	case ::framework::gpu::format::d32_sfloat:
		return ::VkFormat::VK_FORMAT_D32_SFLOAT;
	case ::framework::gpu::format::s8_uint:
        return ::VkFormat::VK_FORMAT_S8_UINT;
	case ::framework::gpu::format::d16_unorm_s8_uint:
        return ::VkFormat::VK_FORMAT_D16_UNORM_S8_UINT;
	case ::framework::gpu::format::d24_unorm_s8_uint:
        return ::VkFormat::VK_FORMAT_D24_UNORM_S8_UINT;
	case ::framework::gpu::format::d32_sfloat_s8_uint:
		return ::VkFormat::VK_FORMAT_D32_SFLOAT_S8_UINT;
	case ::framework::gpu::format::bc1_rgb_unorm_block:
		return ::VkFormat::VK_FORMAT_BC1_RGB_UNORM_BLOCK;
	case ::framework::gpu::format::bc1_rgb_srgb_block:
		return ::VkFormat::VK_FORMAT_BC1_RGB_SRGB_BLOCK;
	case ::framework::gpu::format::bc1_rgba_unorm_block:
		return ::VkFormat::VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
	case ::framework::gpu::format::bc1_rgba_srgb_block:
		return ::VkFormat::VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
	case ::framework::gpu::format::bc2_unorm_block:
		return ::VkFormat::VK_FORMAT_BC2_UNORM_BLOCK;
	case ::framework::gpu::format::bc2_srgb_block:
		return ::VkFormat::VK_FORMAT_BC2_SRGB_BLOCK;
	case ::framework::gpu::format::bc3_unorm_block:
		return ::VkFormat::VK_FORMAT_BC3_UNORM_BLOCK;
	case ::framework::gpu::format::bc3_srgb_block:
		return ::VkFormat::VK_FORMAT_BC3_SRGB_BLOCK;
	case ::framework::gpu::format::bc4_unorm_block:
		return ::VkFormat::VK_FORMAT_BC4_UNORM_BLOCK;
	case ::framework::gpu::format::bc4_snorm_block:
		return ::VkFormat::VK_FORMAT_BC4_SNORM_BLOCK;
	case ::framework::gpu::format::bc5_unorm_block:
		return ::VkFormat::VK_FORMAT_BC5_UNORM_BLOCK;
	case ::framework::gpu::format::bc5_snorm_block:
		return ::VkFormat::VK_FORMAT_BC5_SNORM_BLOCK;
	case ::framework::gpu::format::bc6h_ufloat_block:
		return ::VkFormat::VK_FORMAT_BC6H_UFLOAT_BLOCK;
	case ::framework::gpu::format::bc6h_sfloat_block:
		return ::VkFormat::VK_FORMAT_BC6H_SFLOAT_BLOCK;
	case ::framework::gpu::format::bc7_unorm_block:
		return ::VkFormat::VK_FORMAT_BC7_UNORM_BLOCK;
	case ::framework::gpu::format::bc7_srgb_block:
		return ::VkFormat::VK_FORMAT_BC7_SRGB_BLOCK;
	default:
		return ::VkFormat::VK_FORMAT_UNDEFINED;
	}
}