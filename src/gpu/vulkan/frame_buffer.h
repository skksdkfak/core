#pragma once

#include "gpu/frame_buffer.h"

namespace framework::gpu::vulkan
{
	class frame_buffer final : public ::framework::gpu::frame_buffer
	{
	public:
		frame_buffer(::framework::gpu::vulkan::device * device, ::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkFramebuffer get_vk_framebuffer() const { return this->vk_framebuffer; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkFramebuffer vk_framebuffer;
	};
}