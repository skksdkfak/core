#pragma once

#include "gpu/pipeline_layout.h"
#include <unordered_map>

namespace framework::gpu::vulkan
{
	class pipeline_layout final : public ::framework::gpu::pipeline_layout
	{
	public:
		pipeline_layout(::framework::gpu::vulkan::device * device, ::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkPipelineLayout get_vk_pipeline_layout() const { return this->vk_pipeline_layout; }

		::std::uint32_t get_push_constants_global_offset(::std::uint32_t binding) const { return m_pPushConstantRangeOffsets.at(binding); }

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::VkPipelineLayout vk_pipeline_layout;
		::std::unordered_map<::std::uint32_t, ::std::uint32_t> m_pPushConstantRangeOffsets;
	};
}