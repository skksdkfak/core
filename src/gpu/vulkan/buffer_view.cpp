#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::buffer_view::buffer_view(::framework::gpu::vulkan::device * device, ::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkBufferCreateFlags vk_buffer_create_flags = 0;

	::VkBufferViewCreateInfo vk_buffer_view_create_info;
	vk_buffer_view_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
	vk_buffer_view_create_info.pNext = nullptr;
	vk_buffer_view_create_info.flags = vk_buffer_create_flags;
	vk_buffer_view_create_info.buffer = static_cast<::framework::gpu::vulkan::buffer const *>(create_info->buffer)->get_vk_buffer();
	vk_buffer_view_create_info.format = ::framework::gpu::vulkan::get_vk_format(create_info->format);
	vk_buffer_view_create_info.offset = create_info->offset;
	vk_buffer_view_create_info.range = create_info->range;
	result = ::framework::gpu::vulkan::throw_result(::vkCreateBufferView(device->get_vk_device(), &vk_buffer_view_create_info, nullptr, &this->vk_buffer_view));
}