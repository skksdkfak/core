#pragma once

#include "pipeline.h"

namespace framework::gpu::vulkan
{
	class compute_pipeline final : public ::framework::gpu::vulkan::pipeline
	{
	public:
		compute_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::compute_pipeline_create_info const * create_info, VkPipeline vk_pipeline) noexcept;

	private:
		friend class ::framework::gpu::vulkan::device;
	};
}