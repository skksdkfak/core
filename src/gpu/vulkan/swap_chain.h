#pragma once

#include "gpu/swap_chain.h"
#include <vector>

namespace framework::gpu::vulkan
{
	class swap_chain final : public ::framework::gpu::swap_chain
	{
	public:
		swap_chain(::framework::gpu::vulkan::device * device, ::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		void get_images(::std::uint32_t * image_count, class ::framework::gpu::image ** images) const;

		::VkSwapchainKHR get_vk_swapchain() const { return this->vk_swapchain; }

	private:
		friend class ::framework::gpu::vulkan::device;

		struct images_deleter
		{
			::framework::gpu::vulkan::swap_chain const & swap_chain;

			void operator()(::framework::gpu::vulkan::image ** images) const noexcept
			{
				if (images)
				{
					for (::std::uint32_t i = 0; i < this->swap_chain.image_count; i++)
					{
						DELETE(images[i], this->swap_chain.allocator);
					}
				}
			}
		};
		::framework::gpu::allocation_callbacks const allocator;
		::framework::gpu::vulkan::device * device;
		::VkSwapchainKHR vk_swapchain;
		::std::unique_ptr<::framework::gpu::vulkan::image * [], ::framework::gpu::vulkan::swap_chain::images_deleter> images;
		::std::uint32_t image_count;
	};
}