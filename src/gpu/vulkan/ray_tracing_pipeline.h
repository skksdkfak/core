#pragma once

#include "pipeline.h"

namespace framework::gpu::vulkan
{
	class ray_tracing_pipeline final : public ::framework::gpu::vulkan::pipeline
	{
	public:
		ray_tracing_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::ray_tracing_pipeline_create_info const * create_info, ::VkPipeline vk_pipeline);

	private:
		friend class ::framework::gpu::vulkan::device;
	};
}