#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::deferred_operation::deferred_operation(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::result & result) :
    allocator(allocator ? *allocator : device->get_allocation_callbacks())
{
    result = ::framework::gpu::result::success;
}

::framework::gpu::vulkan::deferred_operation::~deferred_operation()
{
}