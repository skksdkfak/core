#include "gpu/vulkan/core.hpp"
#include <cstring>

::framework::gpu::vulkan::physical_device::physical_device(::framework::gpu::vulkan::instance * instance, ::framework::gpu::allocation_callbacks const & allocator, ::VkPhysicalDevice vk_physical_device) :
	instance(instance),
	allocator(allocator),
	vk_physical_device(vk_physical_device)
{
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::create_device)(::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, class ::framework::gpu::device ** device)
{
	::framework::gpu::result result;

	try
	{
		*device = new ::framework::gpu::vulkan::device(this, create_info, allocator ? *allocator : this->allocator, result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::enumerate_device_extension_properties)(char const * layer_name, ::std::uint32_t * property_count, ::framework::gpu::extension_properties * properties)
{
	::framework::gpu::result result;

	try
	{
		::std::uint32_t const ext_property_count = 1;
		if (properties)
		{
			::std::uint32_t ext_property_index = *property_count - ext_property_count;
			::framework::gpu::common::unique_ptr<::VkExtensionProperties[]> vk_extension_properties = ::framework::gpu::common::make_unique<::VkExtensionProperties[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, ext_property_index);
			::VkResult vk_result = ::vkEnumerateDeviceExtensionProperties(this->vk_physical_device, layer_name, &ext_property_index, vk_extension_properties.get());

			result = ::framework::gpu::vulkan::throw_result(vk_result);

			for (::std::uint32_t i = 0; i < ext_property_index; i++)
			{
				::std::strncpy(properties[i].extension_name, vk_extension_properties[i].extensionName, ::framework::gpu::max_extension_name_size);
				properties[i].spec_version = vk_extension_properties[i].specVersion;
			}
			::std::strncpy(properties[ext_property_index].extension_name, ::framework::gpu::spirv_shader_extension_name, ::framework::gpu::max_extension_name_size);
			properties[ext_property_index].spec_version = 1;
		}
		else
		{
			::VkResult vk_result = ::vkEnumerateDeviceExtensionProperties(this->vk_physical_device, layer_name, property_count, nullptr);

			result = ::framework::gpu::vulkan::throw_result(vk_result);

			*property_count += ext_property_count;
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::physical_device::get_features(::framework::gpu::physical_device_features * features)
{
	::VkPhysicalDeviceBufferDeviceAddressFeatures vk_physical_device_buffer_device_address_features;
	vk_physical_device_buffer_device_address_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
	vk_physical_device_buffer_device_address_features.pNext = nullptr;

	::VkPhysicalDeviceRayTracingPipelineFeaturesKHR vk_physical_device_ray_tracing_pipeline_features;
	vk_physical_device_ray_tracing_pipeline_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
	vk_physical_device_ray_tracing_pipeline_features.pNext = &vk_physical_device_buffer_device_address_features;

	::VkPhysicalDeviceAccelerationStructureFeaturesKHR vk_physical_device_acceleration_structure_features;
	vk_physical_device_acceleration_structure_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
	vk_physical_device_acceleration_structure_features.pNext = &vk_physical_device_ray_tracing_pipeline_features;

	::VkPhysicalDeviceCooperativeMatrixFeaturesKHR vk_physical_device_cooperative_matrix_features;
	vk_physical_device_cooperative_matrix_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_KHR;
	vk_physical_device_cooperative_matrix_features.pNext = &vk_physical_device_acceleration_structure_features;

	::VkPhysicalDeviceRayQueryFeaturesKHR vk_physical_device_ray_query_features;
	vk_physical_device_ray_query_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
	vk_physical_device_ray_query_features.pNext = &vk_physical_device_cooperative_matrix_features;

	::VkPhysicalDeviceShaderAtomicFloatFeaturesEXT vk_physical_device_shader_atomic_float_features;
	vk_physical_device_shader_atomic_float_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
	vk_physical_device_shader_atomic_float_features.pNext = &vk_physical_device_ray_query_features;

	::VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT vk_physical_device_shader_atomic_float2_features;
	vk_physical_device_shader_atomic_float2_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT;
	vk_physical_device_shader_atomic_float2_features.pNext = &vk_physical_device_shader_atomic_float_features;

	::VkPhysicalDeviceVulkan13Features vk_physical_device_vulkan_1_3_features;
	vk_physical_device_vulkan_1_3_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
	vk_physical_device_vulkan_1_3_features.pNext = &vk_physical_device_shader_atomic_float2_features;

	::VkPhysicalDeviceFeatures2 vk_physical_device_features2;
	vk_physical_device_features2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
	vk_physical_device_features2.pNext = &vk_physical_device_vulkan_1_3_features;

	::vkGetPhysicalDeviceFeatures2(this->vk_physical_device, &vk_physical_device_features2);

	features->depth_stencil_view_combine_all_access_types = true;
	features->clear_color_image_requires_views = false;
	features->any_image_initial_layout = false;
	features->shader_visible_host_descriptors = true;
	features->query_pool_host_commands = true;
	features->geometry_shader = vk_physical_device_features2.features.geometryShader;
	features->dynamic_state_depth_bias = true;
	features->texture_compression_etc_2 = vk_physical_device_features2.features.textureCompressionETC2;
	features->texture_compression_astc_ldr = vk_physical_device_features2.features.textureCompressionASTC_LDR;
	features->texture_compression_bc = vk_physical_device_features2.features.textureCompressionBC;
	features->texture_compression_bc_unaligned = true;
	features->buffer_device_address = vk_physical_device_buffer_device_address_features.bufferDeviceAddress;
	features->buffer_device_address_capture_replay = vk_physical_device_buffer_device_address_features.bufferDeviceAddressCaptureReplay;
	features->buffer_device_address_multi_device = vk_physical_device_buffer_device_address_features.bufferDeviceAddressMultiDevice;
	features->ray_tracing_pipeline = vk_physical_device_ray_tracing_pipeline_features.rayTracingPipeline;
	features->ray_tracing_pipeline_shader_group_handle_capture_replay = vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineShaderGroupHandleCaptureReplay;
	features->ray_tracing_pipeline_shader_group_handle_capture_replay_mixed = vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineShaderGroupHandleCaptureReplayMixed;
	features->ray_tracing_pipeline_trace_rays_indirect = vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineTraceRaysIndirect;
	features->ray_traversal_primitive_culling = vk_physical_device_ray_tracing_pipeline_features.rayTraversalPrimitiveCulling;
	features->acceleration_structure = vk_physical_device_acceleration_structure_features.accelerationStructure;
	features->acceleration_structure_capture_replay = vk_physical_device_acceleration_structure_features.accelerationStructureCaptureReplay;
	features->acceleration_structure_indirect_build = vk_physical_device_acceleration_structure_features.accelerationStructureIndirectBuild;
	features->acceleration_structure_host_commands = vk_physical_device_acceleration_structure_features.accelerationStructureHostCommands;
	features->descriptor_binding_acceleration_structure_update_after_bind = vk_physical_device_acceleration_structure_features.descriptorBindingAccelerationStructureUpdateAfterBind;
	features->ray_query = vk_physical_device_ray_query_features.rayQuery;
	features->cooperative_matrix = vk_physical_device_cooperative_matrix_features.cooperativeMatrix;
	features->cooperative_matrix_robust_buffer_access = vk_physical_device_cooperative_matrix_features.cooperativeMatrixRobustBufferAccess;
	features->shader_buffer_float32_atomics = vk_physical_device_shader_atomic_float_features.shaderBufferFloat32Atomics;
	features->shader_buffer_float32_atomic_add = vk_physical_device_shader_atomic_float_features.shaderBufferFloat32AtomicAdd;
	features->shader_buffer_float64_atomics = vk_physical_device_shader_atomic_float_features.shaderBufferFloat64Atomics;
	features->shader_buffer_float64_atomic_add = vk_physical_device_shader_atomic_float_features.shaderBufferFloat64AtomicAdd;
	features->shader_shared_float32_atomics = vk_physical_device_shader_atomic_float_features.shaderSharedFloat32Atomics;
	features->shader_shared_float32_atomic_add = vk_physical_device_shader_atomic_float_features.shaderSharedFloat32AtomicAdd;
	features->shader_shared_float64_atomics = vk_physical_device_shader_atomic_float_features.shaderSharedFloat64Atomics;
	features->shader_shared_float64_atomic_add = vk_physical_device_shader_atomic_float_features.shaderSharedFloat64AtomicAdd;
	features->shader_image_float32_atomics = vk_physical_device_shader_atomic_float_features.shaderImageFloat32Atomics;
	features->shader_image_float32_atomic_add = vk_physical_device_shader_atomic_float_features.shaderImageFloat32AtomicAdd;
	features->sparse_image_float32_atomics = vk_physical_device_shader_atomic_float_features.sparseImageFloat32Atomics;
	features->sparse_image_float32_atomic_add = vk_physical_device_shader_atomic_float_features.sparseImageFloat32AtomicAdd;
	features->shader_buffer_float16_atomics = vk_physical_device_shader_atomic_float2_features.shaderBufferFloat16Atomics;
	features->shader_buffer_float16_atomic_add = vk_physical_device_shader_atomic_float2_features.shaderBufferFloat16AtomicAdd;
	features->shader_buffer_float16_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderBufferFloat16AtomicMinMax;
	features->shader_buffer_float32_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderBufferFloat32AtomicMinMax;
	features->shader_buffer_float64_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderBufferFloat64AtomicMinMax;
	features->shader_shared_float16_atomics = vk_physical_device_shader_atomic_float2_features.shaderSharedFloat16Atomics;
	features->shader_shared_float16_atomic_add = vk_physical_device_shader_atomic_float2_features.shaderSharedFloat16AtomicAdd;
	features->shader_shared_float16_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderSharedFloat16AtomicMinMax;
	features->shader_shared_float32_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderSharedFloat32AtomicMinMax;
	features->shader_shared_float64_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderSharedFloat64AtomicMinMax;
	features->shader_image_float32_atomic_min_max = vk_physical_device_shader_atomic_float2_features.shaderImageFloat32AtomicMinMax;
	features->sparse_image_float32_atomic_min_max = vk_physical_device_shader_atomic_float2_features.sparseImageFloat32AtomicMinMax;
	features->subgroup_size_control = vk_physical_device_vulkan_1_3_features.subgroupSizeControl;
}

void ::framework::gpu::vulkan::physical_device::get_properties(::framework::gpu::physical_device_properties * properties)
{
	::VkPhysicalDeviceRayTracingPipelinePropertiesKHR vk_physical_device_ray_tracing_pipeline_properties;
	vk_physical_device_ray_tracing_pipeline_properties.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
	vk_physical_device_ray_tracing_pipeline_properties.pNext = nullptr;

	::VkPhysicalDeviceAccelerationStructurePropertiesKHR vk_physical_device_acceleration_structure_properties;
	vk_physical_device_acceleration_structure_properties.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR;
	vk_physical_device_acceleration_structure_properties.pNext = &vk_physical_device_ray_tracing_pipeline_properties;

	::VkPhysicalDeviceVulkan13Properties vk_physical_device_vulkan_13_properties;
	vk_physical_device_vulkan_13_properties.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_PROPERTIES;
	vk_physical_device_vulkan_13_properties.pNext = &vk_physical_device_acceleration_structure_properties;

	::VkPhysicalDeviceProperties2 vk_physical_device_properties_2;
	vk_physical_device_properties_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
	vk_physical_device_properties_2.pNext = &vk_physical_device_vulkan_13_properties;

	::vkGetPhysicalDeviceProperties2(this->vk_physical_device, &vk_physical_device_properties_2);

	properties->device_type = ::framework::gpu::vulkan::get_physical_device_type(vk_physical_device_properties_2.properties.deviceType);
	::std::memcpy(properties->device_name, vk_physical_device_properties_2.properties.deviceName, sizeof(::framework::gpu::physical_device_properties::device_name));
	properties->min_subgroup_size = vk_physical_device_vulkan_13_properties.minSubgroupSize;
	properties->max_subgroup_size = vk_physical_device_vulkan_13_properties.maxSubgroupSize;
	properties->max_compute_workgroup_subgroups = vk_physical_device_vulkan_13_properties.maxComputeWorkgroupSubgroups;
	properties->required_subgroup_size_stages = ::framework::gpu::vulkan::get_shader_stage_flags(vk_physical_device_vulkan_13_properties.requiredSubgroupSizeStages);
	properties->limits.max_geometry_shader_invocations = vk_physical_device_properties_2.properties.limits.maxGeometryShaderInvocations;
	properties->limits.max_compute_shared_memory_size = vk_physical_device_properties_2.properties.limits.maxComputeSharedMemorySize;
	properties->limits.max_compute_workgroup_count[0] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupCount[0];
	properties->limits.max_compute_workgroup_count[1] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupCount[1];
	properties->limits.max_compute_workgroup_count[2] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupCount[2];
	properties->limits.max_compute_workgroup_invocations = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupInvocations;
	properties->limits.max_compute_workgroup_size[0] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupSize[0];
	properties->limits.max_compute_workgroup_size[1] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupSize[1];
	properties->limits.max_compute_workgroup_size[2] = vk_physical_device_properties_2.properties.limits.maxComputeWorkGroupSize[2];
	properties->limits.min_texel_buffer_offset_alignment = vk_physical_device_properties_2.properties.limits.minTexelBufferOffsetAlignment;
	properties->limits.min_uniform_buffer_offset_alignment = vk_physical_device_properties_2.properties.limits.minUniformBufferOffsetAlignment;
	properties->limits.min_storage_buffer_offset_alignment = vk_physical_device_properties_2.properties.limits.minStorageBufferOffsetAlignment;
	properties->limits.timestamp_period = vk_physical_device_properties_2.properties.limits.timestampPeriod;
	properties->limits.optimal_buffer_copy_offset_alignment = vk_physical_device_properties_2.properties.limits.optimalBufferCopyOffsetAlignment;
	properties->limits.optimal_buffer_copy_row_pitch_alignment = vk_physical_device_properties_2.properties.limits.optimalBufferCopyRowPitchAlignment;
	properties->limits.viewport_y_up = false;
	properties->ray_tracing_pipeline_properties.shader_group_handle_size = vk_physical_device_ray_tracing_pipeline_properties.shaderGroupHandleSize;
	properties->ray_tracing_pipeline_properties.max_ray_recursion_depth = vk_physical_device_ray_tracing_pipeline_properties.maxRayRecursionDepth;
	properties->ray_tracing_pipeline_properties.max_shader_group_stride = vk_physical_device_ray_tracing_pipeline_properties.maxShaderGroupStride;
	properties->ray_tracing_pipeline_properties.shader_group_base_alignment = vk_physical_device_ray_tracing_pipeline_properties.shaderGroupBaseAlignment;
	properties->ray_tracing_pipeline_properties.shader_group_handle_capture_replay_size = vk_physical_device_ray_tracing_pipeline_properties.shaderGroupHandleCaptureReplaySize;
	properties->ray_tracing_pipeline_properties.max_ray_dispatch_invocation_count = vk_physical_device_ray_tracing_pipeline_properties.maxRayDispatchInvocationCount;
	properties->ray_tracing_pipeline_properties.shader_group_handle_alignment = vk_physical_device_ray_tracing_pipeline_properties.shaderGroupHandleAlignment;
	properties->ray_tracing_pipeline_properties.max_ray_hit_attribute_size = vk_physical_device_ray_tracing_pipeline_properties.maxRayHitAttributeSize;
	properties->acceleration_structure_properties.max_geometry_count = vk_physical_device_acceleration_structure_properties.maxGeometryCount;
	properties->acceleration_structure_properties.max_instance_count = vk_physical_device_acceleration_structure_properties.maxInstanceCount;
	properties->acceleration_structure_properties.max_primitive_count = vk_physical_device_acceleration_structure_properties.maxPrimitiveCount;
	properties->acceleration_structure_properties.max_per_stage_descriptor_acceleration_structures = vk_physical_device_acceleration_structure_properties.maxPerStageDescriptorAccelerationStructures;
	properties->acceleration_structure_properties.max_per_stage_descriptor_update_after_bind_acceleration_structures = vk_physical_device_acceleration_structure_properties.maxPerStageDescriptorUpdateAfterBindAccelerationStructures;
	properties->acceleration_structure_properties.max_descriptor_set_acceleration_structures = vk_physical_device_acceleration_structure_properties.maxDescriptorSetAccelerationStructures;
	properties->acceleration_structure_properties.max_descriptor_set_update_after_bind_acceleration_structures = vk_physical_device_acceleration_structure_properties.maxDescriptorSetUpdateAfterBindAccelerationStructures;
	properties->acceleration_structure_properties.min_acceleration_structure_scratch_offset_alignment = vk_physical_device_acceleration_structure_properties.minAccelerationStructureScratchOffsetAlignment;
}

void ::framework::gpu::vulkan::physical_device::get_queue_family_properties(::std::uint32_t * queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties)
{
	try
	{
		if (queue_family_properties)
		{
			::framework::gpu::common::unique_ptr<::VkQueueFamilyProperties[]> vk_queue_family_properties = ::framework::gpu::common::make_unique<::VkQueueFamilyProperties[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, *queue_family_property_count);
			::vkGetPhysicalDeviceQueueFamilyProperties(this->vk_physical_device, queue_family_property_count, vk_queue_family_properties.get());

			for (::std::uint32_t i = 0; i < *queue_family_property_count; i++)
			{
				::framework::gpu::queue_flags queue_flags = ::framework::gpu::queue_flags::none;
				if (vk_queue_family_properties[i].queueFlags & VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT)
					queue_flags |= ::framework::gpu::queue_flags::graphics_bit;
				if (vk_queue_family_properties[i].queueFlags & VkQueueFlagBits::VK_QUEUE_COMPUTE_BIT)
					queue_flags |= ::framework::gpu::queue_flags::compute_bit;
				if (vk_queue_family_properties[i].queueFlags & VkQueueFlagBits::VK_QUEUE_TRANSFER_BIT)
					queue_flags |= ::framework::gpu::queue_flags::transfer_bit;
				if (vk_queue_family_properties[i].queueFlags & VkQueueFlagBits::VK_QUEUE_SPARSE_BINDING_BIT)
					queue_flags |= ::framework::gpu::queue_flags::sparse_binding_bit;

				queue_family_properties[i].queue_flags = queue_flags;
				queue_family_properties[i].queue_count = vk_queue_family_properties[i].queueCount;
				queue_family_properties[i].timestamp_valid_bits = vk_queue_family_properties[i].timestampValidBits;
				queue_family_properties[i].min_image_transfer_granularity.width = vk_queue_family_properties[i].minImageTransferGranularity.width;
				queue_family_properties[i].min_image_transfer_granularity.height = vk_queue_family_properties[i].minImageTransferGranularity.height;
				queue_family_properties[i].min_image_transfer_granularity.depth = vk_queue_family_properties[i].minImageTransferGranularity.depth;
			}
		}
		else
		{
			::vkGetPhysicalDeviceQueueFamilyProperties(this->vk_physical_device, queue_family_property_count, nullptr);
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::physical_device::get_memory_properties(::framework::gpu::physical_device_memory_properties * physical_device_memory_properties)
{
	::VkPhysicalDeviceMemoryProperties vk_physical_device_memory_properties;
	::vkGetPhysicalDeviceMemoryProperties(this->vk_physical_device, &vk_physical_device_memory_properties);

	physical_device_memory_properties->memory_type_count = vk_physical_device_memory_properties.memoryTypeCount;
	for (::std::uint32_t i = 0; i < vk_physical_device_memory_properties.memoryTypeCount; i++)
	{
		physical_device_memory_properties->memory_types[i].property_flags = ::framework::gpu::vulkan::get_memory_property_flags(vk_physical_device_memory_properties.memoryTypes[i].propertyFlags);
		physical_device_memory_properties->memory_types[i].heap_index = vk_physical_device_memory_properties.memoryTypes[i].heapIndex;
	}
	physical_device_memory_properties->memory_heap_count = vk_physical_device_memory_properties.memoryHeapCount;
	for (::std::uint32_t i = 0; i < vk_physical_device_memory_properties.memoryHeapCount; i++)
	{
		physical_device_memory_properties->memory_heaps[i].size = vk_physical_device_memory_properties.memoryHeaps[i].size;
		physical_device_memory_properties->memory_heaps[i].flags = ::framework::gpu::vulkan::get_memory_heap_flags(vk_physical_device_memory_properties.memoryHeaps[i].flags);
	}
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::get_surface_support)(::std::uint32_t queue_family_index, ::framework::gpu::surface * surface, ::framework::gpu::bool32_t * supported)
{
	::VkResult vk_result;
	::VkBool32 vk_bool32;

	vk_result = ::vkGetPhysicalDeviceSurfaceSupportKHR(this->vk_physical_device, queue_family_index, static_cast<::framework::gpu::vulkan::surface *>(surface)->get_vk_surface(), &vk_bool32);

	*supported = static_cast<::framework::gpu::bool32_t>(vk_bool32);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::get_surface_capabilities)(::framework::gpu::surface * surface, ::framework::gpu::surface_capabilities * surface_capabilities)
{
	::VkResult vk_result;

	::VkSurfaceCapabilitiesKHR vk_surface_capabilities;
	vk_result = ::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(this->vk_physical_device, static_cast<::framework::gpu::vulkan::surface *>(surface)->get_vk_surface(), &vk_surface_capabilities);

	surface_capabilities->min_image_count = vk_surface_capabilities.minImageCount;
	surface_capabilities->max_image_count = vk_surface_capabilities.maxImageCount;
	surface_capabilities->current_extent.width = vk_surface_capabilities.currentExtent.width;
	surface_capabilities->current_extent.height = vk_surface_capabilities.currentExtent.height;
	surface_capabilities->min_image_extent.width = vk_surface_capabilities.minImageExtent.width;
	surface_capabilities->min_image_extent.height = vk_surface_capabilities.minImageExtent.height;
	surface_capabilities->max_image_extent.width = vk_surface_capabilities.maxImageExtent.width;
	surface_capabilities->max_image_extent.height = vk_surface_capabilities.maxImageExtent.height;
	surface_capabilities->max_image_array_layers = vk_surface_capabilities.maxImageArrayLayers;
	surface_capabilities->supported_transforms = ::framework::gpu::vulkan::get_surface_transform_flags(vk_surface_capabilities.supportedTransforms);
	surface_capabilities->current_transform = ::framework::gpu::vulkan::get_surface_transform_flags(vk_surface_capabilities.currentTransform);
	surface_capabilities->supported_composite_alpha = ::framework::gpu::vulkan::get_composite_alpha_flags(vk_surface_capabilities.supportedCompositeAlpha);
	surface_capabilities->supported_usage_flags = ::framework::gpu::vulkan::get_image_usage_flags(vk_surface_capabilities.supportedUsageFlags);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::get_surface_formats)(::framework::gpu::surface * surface, ::std::uint32_t * surface_format_count, ::framework::gpu::surface_format * surface_formats)
{
	::framework::gpu::result result;

	try
	{
		if (surface_formats)
		{
			::framework::gpu::common::unique_ptr<::VkSurfaceFormatKHR[]> vk_surface_formats = ::framework::gpu::common::make_unique<::VkSurfaceFormatKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, *surface_format_count);

			result = ::framework::gpu::vulkan::throw_result(::vkGetPhysicalDeviceSurfaceFormatsKHR(this->vk_physical_device, static_cast<::framework::gpu::vulkan::surface *>(surface)->get_vk_surface(), surface_format_count, vk_surface_formats.get()));

			for (::std::uint32_t i = 0; i < *surface_format_count; i++)
			{
				surface_formats[i].format = ::framework::gpu::vulkan::get_format(vk_surface_formats[i].format);
				surface_formats[i].color_space = ::framework::gpu::vulkan::get_color_space(vk_surface_formats[i].colorSpace);
			}
		}
		else
		{
			result = ::framework::gpu::vulkan::throw_result(::vkGetPhysicalDeviceSurfaceFormatsKHR(this->vk_physical_device, static_cast<::framework::gpu::vulkan::surface *>(surface)->get_vk_surface(), surface_format_count, nullptr));
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::physical_device::get_cooperative_matrix_properties)(::std::uint32_t * property_count, ::framework::gpu::cooperative_matrix_properties * properties)
{
	::framework::gpu::result result;

	try
	{
		if (properties)
		{
			::framework::gpu::common::unique_ptr<::VkCooperativeMatrixPropertiesKHR[]> vk_cooperative_matrix_properties = ::framework::gpu::common::make_unique<::VkCooperativeMatrixPropertiesKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, *property_count);
			for (::std::uint32_t i = 0; i < *property_count; i++)
			{
				vk_cooperative_matrix_properties[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_KHR;
				vk_cooperative_matrix_properties[i].pNext = nullptr;
			}

			result = ::framework::gpu::vulkan::throw_result(this->instance->get_dynamic_library().vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(this->vk_physical_device, property_count, vk_cooperative_matrix_properties.get()));

			for (::std::uint32_t i = 0; i < *property_count; i++)
			{
				properties[i].m_size = vk_cooperative_matrix_properties[i].MSize;
				properties[i].n_size = vk_cooperative_matrix_properties[i].NSize;
				properties[i].k_size = vk_cooperative_matrix_properties[i].KSize;
				properties[i].a_type = ::framework::gpu::vulkan::get_component_type(vk_cooperative_matrix_properties[i].AType);
				properties[i].b_type = ::framework::gpu::vulkan::get_component_type(vk_cooperative_matrix_properties[i].BType);
				properties[i].c_type = ::framework::gpu::vulkan::get_component_type(vk_cooperative_matrix_properties[i].CType);
				properties[i].result_type = ::framework::gpu::vulkan::get_component_type(vk_cooperative_matrix_properties[i].ResultType);
				properties[i].saturating_accumulation = static_cast<::framework::gpu::bool32_t>(vk_cooperative_matrix_properties[i].saturatingAccumulation);
				properties[i].scope = ::framework::gpu::vulkan::get_scope(vk_cooperative_matrix_properties[i].scope);
			}
		}
		else
		{
			result = ::framework::gpu::vulkan::throw_result(this->instance->get_dynamic_library().vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(this->vk_physical_device, property_count, nullptr));
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}
