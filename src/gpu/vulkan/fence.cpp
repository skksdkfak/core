#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::fence::fence(::framework::gpu::vulkan::device * device, ::framework::gpu::fence_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkFenceCreateFlags vk_fence_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::fence_create_flags::signaled_bit))
        vk_fence_create_flags |= ::VkFenceCreateFlagBits::VK_FENCE_CREATE_SIGNALED_BIT;

	::VkFenceCreateInfo vk_fence_create_info;
	vk_fence_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	vk_fence_create_info.pNext = nullptr;
	vk_fence_create_info.flags = vk_fence_create_flags;
    result = ::framework::gpu::vulkan::throw_result(::vkCreateFence(device->get_vk_device(), &vk_fence_create_info, nullptr, &this->vk_fence));
}