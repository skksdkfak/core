#pragma once

#include "gpu/acceleration_structure_geometry_set.h"

namespace framework::gpu::vulkan
{
	class acceleration_structure_geometry_set final : public ::framework::gpu::acceleration_structure_geometry_set
	{
	public:
		acceleration_structure_geometry_set(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~acceleration_structure_geometry_set();

		void write_data(::framework::gpu::write_acceleration_structure_geometry const & write_info) override;

		::std::uint32_t get_geometry_count() const { return this->geometry_count; }

		void init_geometries(::std::uint32_t first_geometry, ::VkAccelerationStructureGeometryKHR const *& geometries_array, ::VkAccelerationStructureGeometryKHR const * const *& geometries_array_of_pointers) const;

		void init_build_range_infos(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::VkAccelerationStructureBuildRangeInfoKHR * build_range_infos) const;

		void init_max_primitive_counts(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::std::uint32_t * max_primitive_counts) const;

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::framework::gpu::acceleration_structure_geometry_set_layout layout;
		::std::uint32_t geometry_count;
		union
		{
			::VkAccelerationStructureGeometryKHR * geometries_array;
			::VkAccelerationStructureGeometryKHR ** geometries_array_of_pointers;
		};
		union
		{
			::VkAccelerationStructureBuildRangeInfoKHR * build_range_infos_array;
			::VkAccelerationStructureBuildRangeInfoKHR ** build_range_infos_array_of_pointers;
		};
	};
}