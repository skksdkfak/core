#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::device_memory::device_memory(::framework::gpu::vulkan::device * device, ::framework::gpu::memory_allocate_info const & allocate_info, ::VkDeviceMemory vk_device_memory) :
	vk_device_memory(vk_device_memory)
{
}