#pragma once

#include "gpu/semaphore.h"

namespace framework::gpu::vulkan
{
	class semaphore final : public ::framework::gpu::semaphore
	{
	public:
		semaphore(::framework::gpu::vulkan::device * device, ::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkSemaphore get_vk_semaphore() const { return this->vk_semaphore; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkSemaphore vk_semaphore;
	};
}