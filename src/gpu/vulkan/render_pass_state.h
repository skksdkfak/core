#pragma once

#include "gpu/render_pass_state.h"

namespace framework::gpu::vulkan
{
	class render_pass_state final : public ::framework::gpu::render_pass_state
	{
	public:
		render_pass_state(::framework::gpu::vulkan::device * device, ::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		void set_state(::framework::gpu::render_pass_state_info const & render_pass_state_info) override;

		struct render_pass_state_info
		{
			::framework::gpu::vulkan::render_pass const * render_pass;
			::framework::gpu::vulkan::frame_buffer const * frame_buffer;
			::VkRect2D render_area;
			::std::uint32_t clear_value_count;
			::framework::gpu::common::unique_ptr<::VkClearValue[]> clear_values;
		};

		::framework::gpu::vulkan::render_pass_state::render_pass_state_info const & get_render_pass_state_info() const
		{
			return this->info;
		}

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::framework::gpu::vulkan::render_pass_state::render_pass_state_info info;
	};
}