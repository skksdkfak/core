#include "gpu/vulkan/core.hpp"
#include <memory>
#include <sstream>
#include <iostream>
#include <cstring>

::framework::gpu::result(::framework::gpu::vulkan::instance::create_instance)(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks default_allocation
		{
			.pfn_allocation = ::framework::gpu::vulkan::default_allocation,
			.pfn_reallocation = ::framework::gpu::vulkan::default_reallocation,
			.pfn_free = ::framework::gpu::vulkan::default_free,
		};

		::framework::gpu::allocation_callbacks selected_allocator = allocator ? *allocator : default_allocation;

		*instance = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::instance>(default_allocation, ::framework::gpu::system_allocation_scope::object, create_info, default_allocation, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::instance::destroy_instance(::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks default_allocation
	{
		.pfn_allocation = ::framework::gpu::vulkan::default_allocation,
		.pfn_reallocation = ::framework::gpu::vulkan::default_reallocation,
		.pfn_free = ::framework::gpu::vulkan::default_free,
	};

	DELETE(static_cast<::framework::gpu::vulkan::instance *>(instance), allocator ? *allocator : default_allocation);
}

::framework::gpu::vulkan::instance::instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	::std::vector<char const *> vk_enabled_layer_names;
	::std::vector<char const *> vk_enabled_extension_names;

	for (::std::uint32_t i = 0; i < create_info->enabled_layer_count; i++)
	{
		if (::std::strcmp(::framework::gpu::layer_standard_validation_layer_name, create_info->enabled_layer_names[i]) == 0)
		{
			vk_enabled_layer_names.push_back("VK_LAYER_KHRONOS_validation");
		}
	}

	bool enable_vk_debug_utils = false;
	bool enable_vk_validation_features = false;
	for (::std::uint32_t i = 0; i < create_info->enabled_extension_count; i++)
	{
		if (::std::strcmp(::framework::gpu::surface_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			vk_enabled_extension_names.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
		}
#ifdef VK_USE_PLATFORM_WIN32_KHR
		else if (::std::strcmp(::framework::gpu::win32_surface_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			vk_enabled_extension_names.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
		}
#endif
#ifdef VK_USE_PLATFORM_XCB_KHR
		else if (::std::strcmp(::framework::gpu::xcb_surface_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			vk_enabled_extension_names.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
		}
#endif
		else if (::std::strcmp(::framework::gpu::debug_utils_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			enable_vk_debug_utils = true;
			vk_enabled_extension_names.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}
		else if (::std::strcmp(::framework::gpu::validation_features_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			enable_vk_validation_features = true;
			vk_enabled_extension_names.push_back(VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME);
		}
	}

	void * next = nullptr;

	::VkValidationFeatureEnableEXT vk_validation_feature_enable[]
	{
		//::VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT,
		//::VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT,
		//::VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
		::VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT,
		//::VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT
	};

	::VkValidationFeaturesEXT vk_validation_features;
	if (enable_vk_validation_features)
	{
		vk_validation_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
		vk_validation_features.pNext = next;
		vk_validation_features.enabledValidationFeatureCount = static_cast<::std::uint32_t>(::std::size(vk_validation_feature_enable));
		vk_validation_features.pEnabledValidationFeatures = vk_validation_feature_enable;
		vk_validation_features.disabledValidationFeatureCount = 0;
		vk_validation_features.pDisabledValidationFeatures = nullptr;

		next = &vk_validation_features;
	}

	::framework::gpu::common::unique_ptr<::VkApplicationInfo> vk_application_info;
	if (create_info->application_info)
	{
		vk_application_info = ::framework::gpu::common::make_unique<::VkApplicationInfo>(this->allocator, ::framework::gpu::system_allocation_scope::object);
		vk_application_info->sType = ::VkStructureType::VK_STRUCTURE_TYPE_APPLICATION_INFO;
		vk_application_info->pNext = nullptr;
		vk_application_info->pApplicationName = create_info->application_info->application_name;
		vk_application_info->applicationVersion = create_info->application_info->application_version;
		vk_application_info->pEngineName = create_info->application_info->engine_name;
		vk_application_info->engineVersion = create_info->application_info->engine_version;
		vk_application_info->apiVersion = VK_API_VERSION_1_3;
	}

	::VkInstanceCreateInfo vk_instance_create_info;
	vk_instance_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	vk_instance_create_info.pNext = next;
	vk_instance_create_info.flags = 0;
	vk_instance_create_info.pApplicationInfo = vk_application_info.get();
	vk_instance_create_info.enabledLayerCount = static_cast<::std::uint32_t>(vk_enabled_layer_names.size());
	vk_instance_create_info.ppEnabledLayerNames = vk_enabled_layer_names.data();
	vk_instance_create_info.enabledExtensionCount = static_cast<::std::uint32_t>(vk_enabled_extension_names.size());
	vk_instance_create_info.ppEnabledExtensionNames = vk_enabled_extension_names.data();

	::VkResult vk_result;

	vk_result = ::vkCreateInstance(&vk_instance_create_info, nullptr, &this->vk_instance);

	::vk::UniqueInstance vk_unique_instance(::vk::Instance(this->vk_instance), ::vk::UniqueDevice::ObjectDestroy(nullptr));

	result = ::framework::gpu::vulkan::throw_result(vk_result);

	this->dynamic_library.vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR = (PFN_vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR)::vkGetInstanceProcAddr(this->vk_instance, "vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR");

	vk_result = ::vkEnumeratePhysicalDevices(this->vk_instance, &this->physical_device_count, nullptr);

	result = ::framework::gpu::vulkan::throw_result(vk_result);

	if (this->physical_device_count)
	{
		::framework::gpu::common::unique_ptr<::VkPhysicalDevice[]> vk_physical_devices = ::framework::gpu::common::make_unique<::VkPhysicalDevice[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, this->physical_device_count);

		vk_result = ::vkEnumeratePhysicalDevices(this->vk_instance, &this->physical_device_count, vk_physical_devices.get());

		result = ::framework::gpu::vulkan::throw_result(vk_result);

		this->physical_devices = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::physical_device>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->physical_device_count);
		for (::std::uint32_t i = 0; i < this->physical_device_count; i++)
		{
			this->physical_devices[i] = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::physical_device>(this->allocator, ::framework::gpu::system_allocation_scope::object, this, this->allocator, vk_physical_devices[i]);
		}
	}

	vk_unique_instance.release();
}

::framework::gpu::vulkan::instance::~instance()
{
	::vkDestroyInstance(this->vk_instance, nullptr);
}

void ::framework::gpu::vulkan::instance::enumerate_physical_devices(::std::uint32_t * physical_device_count, ::framework::gpu::physical_device ** physical_devices)
{
	if (physical_devices)
	{
		for (::std::uint32_t i = 0; i < this->physical_device_count; i++)
		{
			physical_devices[i] = this->physical_devices[i].get();
		}
	}
	else
	{
		*physical_device_count = this->physical_device_count;
	}
}

void ::framework::gpu::vulkan::instance::create_surface(::framework::gpu::surface_create_info const * create_info, ::framework::gpu::surface ** surface)
{
	*surface = new ::framework::gpu::vulkan::surface(this, create_info);
}

void ::framework::gpu::vulkan::instance::destroy_surface(class ::framework::gpu::surface * surface)
{
	::vkDestroySurfaceKHR(this->vk_instance, static_cast<::framework::gpu::vulkan::surface *>(surface)->get_vk_surface(), nullptr);
	delete static_cast<::framework::gpu::vulkan::surface *>(surface);
}