#pragma once

#include "gpu/buffer_view.h"

namespace framework::gpu::vulkan
{
	class buffer_view final : public ::framework::gpu::buffer_view
	{
	public:
		buffer_view(::framework::gpu::vulkan::device * device, ::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkBufferView get_vk_buffer_view() const { return this->vk_buffer_view; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkBufferView vk_buffer_view;
	};
}