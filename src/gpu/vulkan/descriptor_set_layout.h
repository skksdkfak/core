#pragma once

#include "gpu/descriptor_set_layout.h"

namespace framework::gpu::vulkan
{
	class descriptor_set_layout final : public ::framework::gpu::descriptor_set_layout
	{
	public:
		descriptor_set_layout(::framework::gpu::vulkan::device * device, ::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkDescriptorSetLayout get_vk_descriptor_set_layout() const { return this->vk_descriptor_set_layout; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::framework::gpu::allocation_callbacks const allocator;
		::VkDescriptorSetLayout vk_descriptor_set_layout;
	};
}