﻿#pragma once

#ifdef _WIN32
#define NOMINMAX
#endif

#include <vulkan/vulkan.hpp>
#include "gpu/core.hpp"
#include "gpu/common/core.h"

#include "common.h"
#include "instance.h"
#include "physical_device.h"
#include "device.h"
#include "queue.h"
#include "semaphore.h"
#include "command_buffer.h"
#include "fence.h"
#include "device_memory.h"
#include "query_pool.h"
#include "buffer.h"
#include "image.h"
#include "buffer_view.h"
#include "image_view.h"
#include "shader_module.h"
#include "pipeline_cache.h"
#include "pipeline_layout.h"
#include "render_pass.h"
#include "pipeline.h"
#include "graphics_pipeline.h"
#include "compute_pipeline.h"
#include "ray_tracing_pipeline.h"
#include "descriptor_set_layout.h"
#include "sampler.h"
#include "descriptor_pool.h"
#include "descriptor_set.h"
#include "frame_buffer.h"
#include "render_pass_state.h"
#include "command_pool.h"
#include "swap_chain.h"
#include "surface.h"
#include "acceleration_structure_geometry_set.h"
#include "acceleration_structure.h"
#include "deferred_operation.h"
#include <utility>

namespace framework::gpu::vulkan
{
	::framework::gpu::result get_result(::VkResult vk_result);

	inline ::framework::gpu::result throw_result(::VkResult vk_result)
	{
		::framework::gpu::result result;

		switch (vk_result)
		{
		case ::VkResult::VK_SUCCESS:
			return ::framework::gpu::result::success;
		case ::VkResult::VK_NOT_READY:
			return ::framework::gpu::result::not_ready;
		case ::VkResult::VK_TIMEOUT:
			return ::framework::gpu::result::timeout;
		case ::VkResult::VK_EVENT_SET:
			return ::framework::gpu::result::event_set;
		case ::VkResult::VK_EVENT_RESET:
			return ::framework::gpu::result::event_reset;
		case ::VkResult::VK_INCOMPLETE:
			return ::framework::gpu::result::incomplete;
		case ::VkResult::VK_ERROR_OUT_OF_HOST_MEMORY:
			result = ::framework::gpu::result::error_out_of_host_memory;
			break;
		case ::VkResult::VK_ERROR_OUT_OF_DEVICE_MEMORY:
			result = ::framework::gpu::result::error_out_of_device_memory;
			break;
		case ::VkResult::VK_ERROR_INITIALIZATION_FAILED:
			result = ::framework::gpu::result::error_initialization_failed;
			break;
		case ::VkResult::VK_ERROR_DEVICE_LOST:
			result = ::framework::gpu::result::error_device_lost;
			break;
		case ::VkResult::VK_ERROR_MEMORY_MAP_FAILED:
			result = ::framework::gpu::result::error_memory_map_failed;
			break;
		case ::VkResult::VK_ERROR_LAYER_NOT_PRESENT:
			result = ::framework::gpu::result::error_layer_not_present;
			break;
		case ::VkResult::VK_ERROR_EXTENSION_NOT_PRESENT:
			result = ::framework::gpu::result::error_extension_not_present;
			break;
		case ::VkResult::VK_ERROR_FEATURE_NOT_PRESENT:
			result = ::framework::gpu::result::error_feature_not_present;
			break;
		case ::VkResult::VK_ERROR_INCOMPATIBLE_DRIVER:
			result = ::framework::gpu::result::error_incompatible_driver;
			break;
		case ::VkResult::VK_ERROR_TOO_MANY_OBJECTS:
			result = ::framework::gpu::result::error_too_many_objects;
			break;
		case ::VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED:
			result = ::framework::gpu::result::error_format_not_supported;
			break;
		case ::VkResult::VK_ERROR_FRAGMENTED_POOL:
			result = ::framework::gpu::result::error_fragmented_pool;
			break;
		case ::VkResult::VK_ERROR_UNKNOWN:
			result = ::framework::gpu::result::error_unknown;
			break;
		case ::VkResult::VK_ERROR_SURFACE_LOST_KHR:
			result = ::framework::gpu::result::error_surface_lost;
			break;
		case ::VkResult::VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			result = ::framework::gpu::result::error_native_window_in_use;
			break;
		case ::VkResult::VK_SUBOPTIMAL_KHR:
			result = ::framework::gpu::result::suboptimal;
			break;
		case ::VkResult::VK_ERROR_OUT_OF_DATE_KHR:
			result = ::framework::gpu::result::error_out_of_date;
			break;
		case ::VkResult::VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			result = ::framework::gpu::result::error_incompatible_display;
			break;
		case ::VkResult::VK_ERROR_VALIDATION_FAILED_EXT:
			result = ::framework::gpu::result::error_validation_failed;
			break;
		case ::VkResult::VK_ERROR_INVALID_SHADER_NV:
			result = ::framework::gpu::result::error_invalid_shader;
			break;
		case ::VkResult::VK_ERROR_OUT_OF_POOL_MEMORY_KHR:
			result = ::framework::gpu::result::error_out_of_pool_memory;
			break;
		case ::VkResult::VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR:
			result = ::framework::gpu::result::error_invalid_external_handle;
			break;
		case ::VkResult::VK_ERROR_NOT_PERMITTED_EXT:
			result = ::framework::gpu::result::error_not_permitted;
			break;
		default:
			result = ::framework::gpu::result::error_unknown;
			break;
		}

		throw ::framework::gpu::common::result_exception(result);
	}

	inline ::framework::gpu::physical_device_type get_physical_device_type(::VkPhysicalDeviceType vk_physical_device_type)
	{
		switch (vk_physical_device_type)
		{
		case ::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_OTHER:
			return ::framework::gpu::physical_device_type::other;
		case ::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
			return ::framework::gpu::physical_device_type::integrated_gpu;
		case ::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
			return ::framework::gpu::physical_device_type::discrete_gpu;
		case ::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
			return ::framework::gpu::physical_device_type::virtual_gpu;
		case ::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_CPU:
			return ::framework::gpu::physical_device_type::cpu_;
		default:
			::std::unreachable();
		}
	}

	inline ::VkMemoryPropertyFlags get_vk_memory_property_flags(::framework::gpu::memory_property_flags memory_property_flags)
	{
		::VkMemoryPropertyFlags vk_memory_property_flags = 0;

		if (!!(memory_property_flags & ::framework::gpu::memory_property_flags::device_local_bit))
			vk_memory_property_flags |= ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		if (!!(memory_property_flags & ::framework::gpu::memory_property_flags::host_visible_write_bit) ||
			!!(memory_property_flags & ::framework::gpu::memory_property_flags::host_visible_read_bit))
			vk_memory_property_flags |= ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		if (!!(memory_property_flags & ::framework::gpu::memory_property_flags::host_coherent_bit))
			vk_memory_property_flags |= ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		if (!!(memory_property_flags & ::framework::gpu::memory_property_flags::host_cached_bit))
			vk_memory_property_flags |= ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
		if (!!(memory_property_flags & ::framework::gpu::memory_property_flags::lazily_allocated_bit))
			vk_memory_property_flags |= ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;

		return vk_memory_property_flags;
	}

	inline ::VkSemaphoreWaitFlags get_vk_semaphore_wait_flags(::framework::gpu::semaphore_wait_flags semaphore_wait_flags)
	{
		::VkSemaphoreWaitFlags vk_semaphore_wait_flags = 0;
		if (!!(semaphore_wait_flags & ::framework::gpu::semaphore_wait_flags::any_bit))
			vk_semaphore_wait_flags |= ::VkSemaphoreWaitFlagBits::VK_SEMAPHORE_WAIT_ANY_BIT;

		return vk_semaphore_wait_flags;
	}

	inline ::VkStencilOp get_vk_stencil_op(::framework::gpu::stencil_op stencil_op)
	{
		switch (stencil_op)
		{
		case ::framework::gpu::stencil_op::keep:
			return ::VkStencilOp::VK_STENCIL_OP_KEEP;
		case ::framework::gpu::stencil_op::zero:
			return ::VkStencilOp::VK_STENCIL_OP_ZERO;
		case ::framework::gpu::stencil_op::replace:
			return ::VkStencilOp::VK_STENCIL_OP_REPLACE;
		case ::framework::gpu::stencil_op::increment_and_clamp:
			return ::VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_CLAMP;
		case ::framework::gpu::stencil_op::decrement_and_clamp:
			return ::VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_CLAMP;
		case ::framework::gpu::stencil_op::invert:
			return ::VkStencilOp::VK_STENCIL_OP_INVERT;
		case ::framework::gpu::stencil_op::increment_and_wrap:
			return ::VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_WRAP;
		case ::framework::gpu::stencil_op::decrement_and_wrap:
			return ::VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_WRAP;
		default:
			::std::unreachable();
		}
	};

	inline ::VkVertexInputRate get_vk_vertex_input_rate(::framework::gpu::vertex_input_rate vertex_input_rate)
	{
		switch (vertex_input_rate)
		{
		case ::framework::gpu::vertex_input_rate::vertex:
			return ::VkVertexInputRate::VK_VERTEX_INPUT_RATE_VERTEX;
		case ::framework::gpu::vertex_input_rate::instance:
			return ::VkVertexInputRate::VK_VERTEX_INPUT_RATE_INSTANCE;
		default:
			::std::unreachable();
		}
	};

	inline ::VkPolygonMode get_vk_polygon_mode(::framework::gpu::polygon_mode polygon_mode)
	{
		switch (polygon_mode)
		{
		case ::framework::gpu::polygon_mode::fill:
			return ::VkPolygonMode::VK_POLYGON_MODE_FILL;
		case ::framework::gpu::polygon_mode::line:
			return ::VkPolygonMode::VK_POLYGON_MODE_LINE;
		case ::framework::gpu::polygon_mode::point:
			return ::VkPolygonMode::VK_POLYGON_MODE_POINT;
		default:
			::std::unreachable();
		}
	};

	inline ::VkCullModeFlags get_vk_cull_mode_flags(::framework::gpu::cull_mode_flags cull_mode_flags)
	{
		::VkCullModeFlags vk_cull_mode_flags = ::VkCullModeFlagBits::VK_CULL_MODE_NONE;
		if (!!(cull_mode_flags & ::framework::gpu::cull_mode_flags::front_bit))
			vk_cull_mode_flags |= ::VkCullModeFlagBits::VK_CULL_MODE_FRONT_BIT;
		if (!!(cull_mode_flags & ::framework::gpu::cull_mode_flags::back_bit))
			vk_cull_mode_flags |= ::VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT;
		if (!!(cull_mode_flags & ::framework::gpu::cull_mode_flags::front_and_back))
			vk_cull_mode_flags |= ::VkCullModeFlagBits::VK_CULL_MODE_FRONT_AND_BACK;

		return vk_cull_mode_flags;
	};

	inline ::VkFrontFace get_vk_front_face(::framework::gpu::front_face front_face)
	{
		switch (front_face)
		{
		case ::framework::gpu::front_face::counter_clockwise:
			return ::VkFrontFace::VK_FRONT_FACE_COUNTER_CLOCKWISE;
		case ::framework::gpu::front_face::clockwise:
			return ::VkFrontFace::VK_FRONT_FACE_CLOCKWISE;
		default:
			::std::unreachable();
		}
	};

	inline ::VkPrimitiveTopology get_vk_primitive_topology(::framework::gpu::primitive_topology primitive_topology)
	{
		switch (primitive_topology)
		{
		case ::framework::gpu::primitive_topology::point_list:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		case ::framework::gpu::primitive_topology::line_list:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
		case ::framework::gpu::primitive_topology::line_strip:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
		case ::framework::gpu::primitive_topology::triangle_list:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		case ::framework::gpu::primitive_topology::triangle_strip:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
		case ::framework::gpu::primitive_topology::triangle_fan:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
		case ::framework::gpu::primitive_topology::line_list_with_adjacency:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
		case ::framework::gpu::primitive_topology::line_strip_with_adjacency:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
		case ::framework::gpu::primitive_topology::triangle_list_with_adjacency:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
		case ::framework::gpu::primitive_topology::triangle_strip_with_adjacency:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
		case ::framework::gpu::primitive_topology::patch_list:
			return ::VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
		default:
			::std::unreachable();
		}

	};

	inline ::VkColorComponentFlags get_vk_color_write_mask(::framework::gpu::color_component_flags color_component_flags)
	{
		::VkColorComponentFlags vk_color_write_mask = 0;
		if (!!(color_component_flags & ::framework::gpu::color_component_flags::r_bit))
			vk_color_write_mask |= ::VkColorComponentFlagBits::VK_COLOR_COMPONENT_R_BIT;
		if (!!(color_component_flags & ::framework::gpu::color_component_flags::g_bit))
			vk_color_write_mask |= ::VkColorComponentFlagBits::VK_COLOR_COMPONENT_G_BIT;
		if (!!(color_component_flags & ::framework::gpu::color_component_flags::b_bit))
			vk_color_write_mask |= ::VkColorComponentFlagBits::VK_COLOR_COMPONENT_B_BIT;
		if (!!(color_component_flags & ::framework::gpu::color_component_flags::a_bit))
			vk_color_write_mask |= ::VkColorComponentFlagBits::VK_COLOR_COMPONENT_A_BIT;

		return vk_color_write_mask;
	}

	inline ::VkComponentSwizzle get_vk_component_swizzle(::framework::gpu::component_swizzle component_swizzle)
	{
		switch (component_swizzle)
		{
		case ::framework::gpu::component_swizzle::identity:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_IDENTITY;
		case ::framework::gpu::component_swizzle::zero:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_ZERO;
		case ::framework::gpu::component_swizzle::one:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_ONE;
		case ::framework::gpu::component_swizzle::r:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_R;
		case ::framework::gpu::component_swizzle::g:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_G;
		case ::framework::gpu::component_swizzle::b:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_B;
		case ::framework::gpu::component_swizzle::a:
			return ::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_A;
		default:
			::std::unreachable();
		}
	};

	inline ::VkSampleCountFlagBits get_vk_sample_count_flag_bits(::framework::gpu::sample_count_flags sample_count_flags)
	{
		switch (sample_count_flags)
		{
		case ::framework::gpu::sample_count_flags::sample_count_1_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_2_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_2_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_4_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_4_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_8_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_8_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_16_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_16_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_32_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_32_BIT;
		case ::framework::gpu::sample_count_flags::sample_count_64_bit:
			return ::VkSampleCountFlagBits::VK_SAMPLE_COUNT_64_BIT;
		default:
			::std::unreachable();
		}
	};

	inline ::VkCompareOp get_vk_compare_op(::framework::gpu::compare_op compare_op)
	{
		switch (compare_op)
		{
		case ::framework::gpu::compare_op::never:
			return ::VkCompareOp::VK_COMPARE_OP_NEVER;
		case ::framework::gpu::compare_op::less:
			return ::VkCompareOp::VK_COMPARE_OP_LESS;
		case ::framework::gpu::compare_op::equal:
			return ::VkCompareOp::VK_COMPARE_OP_EQUAL;
		case ::framework::gpu::compare_op::less_or_equal:
			return ::VkCompareOp::VK_COMPARE_OP_LESS_OR_EQUAL;
		case ::framework::gpu::compare_op::greater:
			return ::VkCompareOp::VK_COMPARE_OP_GREATER;
		case ::framework::gpu::compare_op::not_equal:
			return ::VkCompareOp::VK_COMPARE_OP_NOT_EQUAL;
		case ::framework::gpu::compare_op::greater_or_equal:
			return ::VkCompareOp::VK_COMPARE_OP_GREATER_OR_EQUAL;
		case ::framework::gpu::compare_op::always:
			return ::VkCompareOp::VK_COMPARE_OP_ALWAYS;
		default:
			::std::unreachable();
		}
	};

	inline ::VkFilter get_vk_filter(::framework::gpu::filter filter)
	{
		switch (filter)
		{
		case ::framework::gpu::filter::nearest:
			return ::VkFilter::VK_FILTER_NEAREST;
		case ::framework::gpu::filter::linear:
			return ::VkFilter::VK_FILTER_LINEAR;
		case ::framework::gpu::filter::cubic_img:
			return ::VkFilter::VK_FILTER_CUBIC_IMG;
		default:
			::std::unreachable();
		}
	};

	inline ::VkSamplerAddressMode get_vk_sampler_address_mode(::framework::gpu::sampler_address_mode sampler_address_mode)
	{
		switch (sampler_address_mode)
		{
		case ::framework::gpu::sampler_address_mode::repeat:
			return ::VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT;
		case ::framework::gpu::sampler_address_mode::mirrored_repeat:
			return ::VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
		case ::framework::gpu::sampler_address_mode::clamp_to_edge:
			return ::VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		case ::framework::gpu::sampler_address_mode::clamp_to_border:
			return ::VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		case ::framework::gpu::sampler_address_mode::mirror_clamp_to_edge:
			return ::VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
		default:
			::std::unreachable();
		}
	};

	inline ::VkLogicOp get_vk_logic_op(::framework::gpu::logic_op logic_op)
	{
		switch (logic_op)
		{
		case ::framework::gpu::logic_op::clear:
			return ::VkLogicOp::VK_LOGIC_OP_CLEAR;
		case ::framework::gpu::logic_op::and_:
			return ::VkLogicOp::VK_LOGIC_OP_AND;
		case ::framework::gpu::logic_op::and_reverse:
			return ::VkLogicOp::VK_LOGIC_OP_AND_REVERSE;
		case ::framework::gpu::logic_op::copy:
			return ::VkLogicOp::VK_LOGIC_OP_COPY;
		case ::framework::gpu::logic_op::and_inverted:
			return ::VkLogicOp::VK_LOGIC_OP_AND_INVERTED;
		case ::framework::gpu::logic_op::no_op:
			return ::VkLogicOp::VK_LOGIC_OP_NO_OP;
		case ::framework::gpu::logic_op::xor_:
			return ::VkLogicOp::VK_LOGIC_OP_XOR;
		case ::framework::gpu::logic_op::or_:
			return ::VkLogicOp::VK_LOGIC_OP_OR;
		case ::framework::gpu::logic_op::nor:
			return ::VkLogicOp::VK_LOGIC_OP_NOR;
		case ::framework::gpu::logic_op::equivalent:
			return ::VkLogicOp::VK_LOGIC_OP_EQUIVALENT;
		case ::framework::gpu::logic_op::invert:
			return ::VkLogicOp::VK_LOGIC_OP_INVERT;
		case ::framework::gpu::logic_op::or_reverse:
			return ::VkLogicOp::VK_LOGIC_OP_OR_REVERSE;
		case ::framework::gpu::logic_op::copy_inverted:
			return ::VkLogicOp::VK_LOGIC_OP_COPY_INVERTED;
		case ::framework::gpu::logic_op::or_inverted:
			return ::VkLogicOp::VK_LOGIC_OP_OR_INVERTED;
		case ::framework::gpu::logic_op::nand:
			return ::VkLogicOp::VK_LOGIC_OP_NAND;
		case ::framework::gpu::logic_op::set:
			return ::VkLogicOp::VK_LOGIC_OP_SET;
		default:
			::std::unreachable();
		}
	};

	inline ::VkBlendFactor get_vk_blend_factor(::framework::gpu::blend_factor blend_factor)
	{
		switch (blend_factor)
		{
		case ::framework::gpu::blend_factor::zero:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ZERO;
		case ::framework::gpu::blend_factor::one:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE;
		case ::framework::gpu::blend_factor::src_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_SRC_COLOR;
		case ::framework::gpu::blend_factor::one_minus_src_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
		case ::framework::gpu::blend_factor::dst_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_DST_COLOR;
		case ::framework::gpu::blend_factor::one_minus_dst_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
		case ::framework::gpu::blend_factor::src_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA;
		case ::framework::gpu::blend_factor::one_minus_src_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		case ::framework::gpu::blend_factor::dst_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_DST_ALPHA;
		case ::framework::gpu::blend_factor::one_minus_dst_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
		case ::framework::gpu::blend_factor::constant_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_COLOR;
		case ::framework::gpu::blend_factor::one_minus_constant_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
		case ::framework::gpu::blend_factor::constant_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_ALPHA;
		case ::framework::gpu::blend_factor::one_minus_constant_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
		case ::framework::gpu::blend_factor::src_alpha_saturate:
			return ::VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
		case ::framework::gpu::blend_factor::src1_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_SRC1_COLOR;
		case ::framework::gpu::blend_factor::one_minus_src1_color:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
		case ::framework::gpu::blend_factor::src1_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_SRC1_ALPHA;
		case ::framework::gpu::blend_factor::one_minus_src1_alpha:
			return ::VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
		default:
			::std::unreachable();
		}
	};

	inline ::VkBlendOp get_vk_blend_op(::framework::gpu::blend_op blend_op)
	{
		switch (blend_op)
		{
		case ::framework::gpu::blend_op::add:
			return ::VkBlendOp::VK_BLEND_OP_ADD;
		case ::framework::gpu::blend_op::subtract:
			return ::VkBlendOp::VK_BLEND_OP_SUBTRACT;
		case ::framework::gpu::blend_op::reverse_subtract:
			return ::VkBlendOp::VK_BLEND_OP_REVERSE_SUBTRACT;
		case ::framework::gpu::blend_op::min:
			return ::VkBlendOp::VK_BLEND_OP_MIN;
		case ::framework::gpu::blend_op::max:
			return ::VkBlendOp::VK_BLEND_OP_MAX;
		case ::framework::gpu::blend_op::zero:
			return ::VkBlendOp::VK_BLEND_OP_ZERO_EXT;
		case ::framework::gpu::blend_op::src:
			return ::VkBlendOp::VK_BLEND_OP_SRC_EXT;
		case ::framework::gpu::blend_op::dst:
			return ::VkBlendOp::VK_BLEND_OP_DST_EXT;
		case ::framework::gpu::blend_op::src_over:
			return ::VkBlendOp::VK_BLEND_OP_SRC_OVER_EXT;
		case ::framework::gpu::blend_op::dst_over:
			return ::VkBlendOp::VK_BLEND_OP_DST_OVER_EXT;
		case ::framework::gpu::blend_op::src_in:
			return ::VkBlendOp::VK_BLEND_OP_SRC_IN_EXT;
		case ::framework::gpu::blend_op::dst_in:
			return ::VkBlendOp::VK_BLEND_OP_DST_IN_EXT;
		case ::framework::gpu::blend_op::src_out:
			return ::VkBlendOp::VK_BLEND_OP_SRC_OUT_EXT;
		case ::framework::gpu::blend_op::dst_out:
			return ::VkBlendOp::VK_BLEND_OP_DST_OUT_EXT;
		case ::framework::gpu::blend_op::src_atop:
			return ::VkBlendOp::VK_BLEND_OP_SRC_ATOP_EXT;
		case ::framework::gpu::blend_op::dst_atop:
			return ::VkBlendOp::VK_BLEND_OP_DST_ATOP_EXT;
		case ::framework::gpu::blend_op::xor_:
			return ::VkBlendOp::VK_BLEND_OP_XOR_EXT;
		case ::framework::gpu::blend_op::multiply:
			return ::VkBlendOp::VK_BLEND_OP_MULTIPLY_EXT;
		case ::framework::gpu::blend_op::screen:
			return ::VkBlendOp::VK_BLEND_OP_SCREEN_EXT;
		case ::framework::gpu::blend_op::overlay:
			return ::VkBlendOp::VK_BLEND_OP_OVERLAY_EXT;
		case ::framework::gpu::blend_op::darken:
			return ::VkBlendOp::VK_BLEND_OP_DARKEN_EXT;
		case ::framework::gpu::blend_op::lighten:
			return ::VkBlendOp::VK_BLEND_OP_LIGHTEN_EXT;
		case ::framework::gpu::blend_op::colordodge:
			return ::VkBlendOp::VK_BLEND_OP_COLORDODGE_EXT;
		case ::framework::gpu::blend_op::colorburn:
			return ::VkBlendOp::VK_BLEND_OP_COLORBURN_EXT;
		case ::framework::gpu::blend_op::hardlight:
			return ::VkBlendOp::VK_BLEND_OP_HARDLIGHT_EXT;
		case ::framework::gpu::blend_op::softlight:
			return ::VkBlendOp::VK_BLEND_OP_SOFTLIGHT_EXT;
		case ::framework::gpu::blend_op::difference:
			return ::VkBlendOp::VK_BLEND_OP_DIFFERENCE_EXT;
		case ::framework::gpu::blend_op::exclusion:
			return ::VkBlendOp::VK_BLEND_OP_EXCLUSION_EXT;
		case ::framework::gpu::blend_op::invert:
			return ::VkBlendOp::VK_BLEND_OP_INVERT_EXT;
		case ::framework::gpu::blend_op::invert_rgb:
			return ::VkBlendOp::VK_BLEND_OP_INVERT_RGB_EXT;
		case ::framework::gpu::blend_op::lineardodge:
			return ::VkBlendOp::VK_BLEND_OP_LINEARDODGE_EXT;
		case ::framework::gpu::blend_op::linearburn:
			return ::VkBlendOp::VK_BLEND_OP_LINEARBURN_EXT;
		case ::framework::gpu::blend_op::vividlight:
			return ::VkBlendOp::VK_BLEND_OP_VIVIDLIGHT_EXT;
		case ::framework::gpu::blend_op::linearlight:
			return ::VkBlendOp::VK_BLEND_OP_LINEARLIGHT_EXT;
		case ::framework::gpu::blend_op::pinlight:
			return ::VkBlendOp::VK_BLEND_OP_PINLIGHT_EXT;
		case ::framework::gpu::blend_op::hardmix:
			return ::VkBlendOp::VK_BLEND_OP_HARDMIX_EXT;
		case ::framework::gpu::blend_op::hsl_hue:
			return ::VkBlendOp::VK_BLEND_OP_HSL_HUE_EXT;
		case ::framework::gpu::blend_op::hsl_saturation:
			return ::VkBlendOp::VK_BLEND_OP_HSL_SATURATION_EXT;
		case ::framework::gpu::blend_op::hsl_color:
			return ::VkBlendOp::VK_BLEND_OP_HSL_COLOR_EXT;
		case ::framework::gpu::blend_op::hsl_luminosity:
			return ::VkBlendOp::VK_BLEND_OP_HSL_LUMINOSITY_EXT;
		case ::framework::gpu::blend_op::plus:
			return ::VkBlendOp::VK_BLEND_OP_PLUS_EXT;
		case ::framework::gpu::blend_op::plus_clamped:
			return ::VkBlendOp::VK_BLEND_OP_PLUS_CLAMPED_EXT;
		case ::framework::gpu::blend_op::plus_clamped_alpha:
			return ::VkBlendOp::VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT;
		case ::framework::gpu::blend_op::plus_darker:
			return ::VkBlendOp::VK_BLEND_OP_PLUS_DARKER_EXT;
		case ::framework::gpu::blend_op::minus:
			return ::VkBlendOp::VK_BLEND_OP_MINUS_EXT;
		case ::framework::gpu::blend_op::minus_clamped:
			return ::VkBlendOp::VK_BLEND_OP_MINUS_CLAMPED_EXT;
		case ::framework::gpu::blend_op::contrast:
			return ::VkBlendOp::VK_BLEND_OP_CONTRAST_EXT;
		case ::framework::gpu::blend_op::invert_ovg:
			return ::VkBlendOp::VK_BLEND_OP_INVERT_OVG_EXT;
		case ::framework::gpu::blend_op::red:
			return ::VkBlendOp::VK_BLEND_OP_RED_EXT;
		case ::framework::gpu::blend_op::green:
			return ::VkBlendOp::VK_BLEND_OP_GREEN_EXT;
		case ::framework::gpu::blend_op::blue:
			return ::VkBlendOp::VK_BLEND_OP_BLUE_EXT;
		default:
			::std::unreachable();
		}
	};

	inline ::VkImageViewType get_vk_image_view_type(::framework::gpu::image_view_type image_view_type)
	{
		switch (image_view_type)
		{
		case ::framework::gpu::image_view_type::one_dimensional:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_1D;
		case ::framework::gpu::image_view_type::two_dimensional:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_2D;
		case ::framework::gpu::image_view_type::three_dimensional:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_3D;
		case ::framework::gpu::image_view_type::cube:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE;
		case ::framework::gpu::image_view_type::one_dimensional_array:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_1D_ARRAY;
		case ::framework::gpu::image_view_type::two_dimensional_array:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_2D_ARRAY;
		case ::framework::gpu::image_view_type::cube_array:
			return VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
		default:
			::std::unreachable();
		}
	};

	inline ::VkSubmitFlagsKHR get_vk_submit_flags(::framework::gpu::submit_flags submit_flags)
	{
		::VkSubmitFlagsKHR vk_submit_flags = 0;

		if (!!(submit_flags & ::framework::gpu::submit_flags::protected_bit))
			vk_submit_flags |= ::VkSubmitFlagBitsKHR::VK_SUBMIT_PROTECTED_BIT_KHR;

		return vk_submit_flags;
	}

	inline ::VkClearValue get_vk_clear_value(::framework::gpu::clear_value const & clear_value)
	{
		return *reinterpret_cast<::VkClearValue const *>(&clear_value);
	}

	inline ::VkClearColorValue get_clear_color_value(::framework::gpu::clear_color_value const & clear_color_value)
	{
		return *reinterpret_cast<::VkClearColorValue const *>(&clear_color_value);
	}

	inline ::VkPipelineBindPoint get_vk_pipeline_bind_point(::framework::gpu::pipeline_bind_point pipeline_bind_point)
	{
		switch (pipeline_bind_point)
		{
		case ::framework::gpu::pipeline_bind_point::graphics:
			return ::VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS;
		case ::framework::gpu::pipeline_bind_point::compute:
			return ::VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_COMPUTE;
		case ::framework::gpu::pipeline_bind_point::ray_tracing:
			return ::VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR;
		default:
			::std::unreachable();
		}
	}

	inline ::VkImageAspectFlags get_vk_image_aspect_flags(::framework::gpu::image_aspect_flags image_aspect_flags)
	{
		::VkImageAspectFlags vk_image_aspect_flags = 0;

		if (!!(image_aspect_flags & ::framework::gpu::image_aspect_flags::color_bit))
			vk_image_aspect_flags |= ::VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT;
		if (!!(image_aspect_flags & ::framework::gpu::image_aspect_flags::depth_bit))
			vk_image_aspect_flags |= ::VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT;
		if (!!(image_aspect_flags & ::framework::gpu::image_aspect_flags::stencil_bit))
			vk_image_aspect_flags |= ::VkImageAspectFlagBits::VK_IMAGE_ASPECT_STENCIL_BIT;
		if (!!(image_aspect_flags & ::framework::gpu::image_aspect_flags::metadata_bit))
			vk_image_aspect_flags |= ::VkImageAspectFlagBits::VK_IMAGE_ASPECT_METADATA_BIT;

		return vk_image_aspect_flags;
	}

	inline ::VkImageSubresourceRange get_vk_image_subresource_range(::framework::gpu::image_subresource_range const & image_subresource_range)
	{
		return
		{
			::framework::gpu::vulkan::get_vk_image_aspect_flags(image_subresource_range.aspect_mask),
			image_subresource_range.base_mip_level,
			image_subresource_range.level_count,
			image_subresource_range.base_array_layer,
			image_subresource_range.layer_count
		};
	}

	inline ::framework::gpu::memory_property_flags get_memory_property_flags(::VkMemoryPropertyFlags vk_memory_property_flags)
	{
		::framework::gpu::memory_property_flags memory_property_flags = ::framework::gpu::memory_property_flags::none;

		if (vk_memory_property_flags & ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
			memory_property_flags |= ::framework::gpu::memory_property_flags::device_local_bit;
		if (vk_memory_property_flags & ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
			memory_property_flags |= ::framework::gpu::memory_property_flags::host_visible_write_bit |
			::framework::gpu::memory_property_flags::host_visible_read_bit;
		if (vk_memory_property_flags & ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
			memory_property_flags |= ::framework::gpu::memory_property_flags::host_coherent_bit;
		if (vk_memory_property_flags & ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
			memory_property_flags |= ::framework::gpu::memory_property_flags::host_cached_bit;
		if (vk_memory_property_flags & ::VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
			memory_property_flags |= ::framework::gpu::memory_property_flags::lazily_allocated_bit;

		return memory_property_flags;
	}

	inline ::framework::gpu::memory_heap_flags get_memory_heap_flags(::VkMemoryHeapFlags vk_memory_heap_flags)
	{
		::framework::gpu::memory_heap_flags memory_heap_flags = ::framework::gpu::memory_heap_flags::none;

		if (vk_memory_heap_flags & ::VkMemoryHeapFlagBits::VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
			memory_heap_flags |= ::framework::gpu::memory_heap_flags::device_local_bit;
		if (vk_memory_heap_flags & ::VkMemoryHeapFlagBits::VK_MEMORY_HEAP_MULTI_INSTANCE_BIT)
			memory_heap_flags |= ::framework::gpu::memory_heap_flags::multi_instance_bit;

		return memory_heap_flags;
	}

	inline ::framework::gpu::component_type get_component_type(::VkComponentTypeKHR const vk_component_type)
	{
		switch (vk_component_type)
		{
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_FLOAT16_KHR:
			return ::framework::gpu::component_type::float16;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_FLOAT32_KHR:
			return ::framework::gpu::component_type::float32;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_FLOAT64_KHR:
			return ::framework::gpu::component_type::float64;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_SINT8_KHR:
			return ::framework::gpu::component_type::sint8;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_SINT16_KHR:
			return ::framework::gpu::component_type::sint16;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_SINT32_KHR:
			return ::framework::gpu::component_type::sint32;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_SINT64_KHR:
			return ::framework::gpu::component_type::sint64;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_UINT8_KHR:
			return ::framework::gpu::component_type::uint8;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_UINT16_KHR:
			return ::framework::gpu::component_type::uint16;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_UINT32_KHR:
			return ::framework::gpu::component_type::uint16;
		case  ::VkComponentTypeKHR::VK_COMPONENT_TYPE_UINT64_KHR:
			return ::framework::gpu::component_type::uint16;
		default:
			::std::unreachable();
		}
	}

	inline ::framework::gpu::scope get_scope(::VkScopeKHR const vk_scope)
	{
		switch (vk_scope)
		{
		case  ::VkScopeKHR::VK_SCOPE_DEVICE_KHR:
			return ::framework::gpu::scope::device;
		case  ::VkScopeKHR::VK_SCOPE_WORKGROUP_KHR:
			return ::framework::gpu::scope::workgroup;
		case  ::VkScopeKHR::VK_SCOPE_SUBGROUP_KHR:
			return ::framework::gpu::scope::subgroup;
		case  ::VkScopeKHR::VK_SCOPE_QUEUE_FAMILY_KHR:
			return ::framework::gpu::scope::queue_family;
		default:
			::std::unreachable();
		}
	}

	::VkFormat get_vk_format(::framework::gpu::format format);

	inline ::framework::gpu::format get_format(::VkFormat vk_format)
	{
		switch (vk_format)
		{
		case ::VkFormat::VK_FORMAT_UNDEFINED:
			return ::framework::gpu::format::undefined;
			//case ::VkFormat::VK_FORMAT_R4G4_UNORM_PACK8:

			//case ::VkFormat::VK_FORMAT_R4G4B4A4_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_B4G4R4A4_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_R5G6B5_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_B5G6R5_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_R5G5B5A1_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_B5G5R5A1_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_A1R5G5B5_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_R8_UNORM:

			//case ::VkFormat::VK_FORMAT_R8_SNORM:

			//case ::VkFormat::VK_FORMAT_R8_USCALED:

			//case ::VkFormat::VK_FORMAT_R8_SSCALED:

			//case ::VkFormat::VK_FORMAT_R8_UINT:

			//case ::VkFormat::VK_FORMAT_R8_SINT:

			//case ::VkFormat::VK_FORMAT_R8_SRGB:

			//case ::VkFormat::VK_FORMAT_R8G8_UNORM:

			//case ::VkFormat::VK_FORMAT_R8G8_SNORM:

			//case ::VkFormat::VK_FORMAT_R8G8_USCALED:

			//case ::VkFormat::VK_FORMAT_R8G8_SSCALED:

			//case ::VkFormat::VK_FORMAT_R8G8_UINT:

			//case ::VkFormat::VK_FORMAT_R8G8_SINT:

			//case ::VkFormat::VK_FORMAT_R8G8_SRGB:

			//case ::VkFormat::VK_FORMAT_R8G8B8_UNORM:

			//case ::VkFormat::VK_FORMAT_R8G8B8_SNORM:

			//case ::VkFormat::VK_FORMAT_R8G8B8_USCALED:

			//case ::VkFormat::VK_FORMAT_R8G8B8_SSCALED:

			//case ::VkFormat::VK_FORMAT_R8G8B8_UINT:

			//case ::VkFormat::VK_FORMAT_R8G8B8_SINT:

			//case ::VkFormat::VK_FORMAT_R8G8B8_SRGB:

			//case ::VkFormat::VK_FORMAT_B8G8R8_UNORM:

			//case ::VkFormat::VK_FORMAT_B8G8R8_SNORM:

			//case ::VkFormat::VK_FORMAT_B8G8R8_USCALED:

			//case ::VkFormat::VK_FORMAT_B8G8R8_SSCALED:

			//case ::VkFormat::VK_FORMAT_B8G8R8_UINT:

			//case ::VkFormat::VK_FORMAT_B8G8R8_SINT:

			//case ::VkFormat::VK_FORMAT_B8G8R8_SRGB:

		case ::VkFormat::VK_FORMAT_R8G8B8A8_UNORM:
			return ::framework::gpu::format::r8g8b8a8_unorm;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_SNORM:
			return ::framework::gpu::format::r8g8b8a8_snorm;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_USCALED:
			return ::framework::gpu::format::r8g8b8a8_uscaled;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_SSCALED:
			return ::framework::gpu::format::r8g8b8a8_sscaled;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_UINT:
			return ::framework::gpu::format::r8g8b8a8_uint;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_SINT:
			return ::framework::gpu::format::r8g8b8a8_sint;
		case ::VkFormat::VK_FORMAT_R8G8B8A8_SRGB:
			return ::framework::gpu::format::r8g8b8a8_srgb;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_UNORM:
			return ::framework::gpu::format::b8g8r8a8_unorm;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_SNORM:
			return ::framework::gpu::format::b8g8r8a8_snorm;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_USCALED:
			return ::framework::gpu::format::b8g8r8a8_uscaled;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_SSCALED:
			return ::framework::gpu::format::b8g8r8a8_sscaled;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_UINT:
			return ::framework::gpu::format::b8g8r8a8_uint;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_SINT:
			return ::framework::gpu::format::b8g8r8a8_sint;
		case ::VkFormat::VK_FORMAT_B8G8R8A8_SRGB:
			return ::framework::gpu::format::b8g8r8a8_srgb;
			//case ::VkFormat::VK_FORMAT_A8B8G8R8_UNORM_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_SNORM_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_USCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_SSCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_UINT_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_SINT_PACK32:

			//case ::VkFormat::VK_FORMAT_A8B8G8R8_SRGB_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_UNORM_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_SNORM_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_USCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_SSCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_UINT_PACK32:

			//case ::VkFormat::VK_FORMAT_A2R10G10B10_SINT_PACK32:

		case ::VkFormat::VK_FORMAT_A2B10G10R10_UNORM_PACK32:
			return ::framework::gpu::format::a2b10g10r10_unorm_pack32;
			//case ::VkFormat::VK_FORMAT_A2B10G10R10_SNORM_PACK32:

			//case ::VkFormat::VK_FORMAT_A2B10G10R10_USCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A2B10G10R10_SSCALED_PACK32:

			//case ::VkFormat::VK_FORMAT_A2B10G10R10_UINT_PACK32:

			//case ::VkFormat::VK_FORMAT_A2B10G10R10_SINT_PACK32:

			//case ::VkFormat::VK_FORMAT_R16_UNORM:

			//case ::VkFormat::VK_FORMAT_R16_SNORM:

			//case ::VkFormat::VK_FORMAT_R16_USCALED:

			//case ::VkFormat::VK_FORMAT_R16_SSCALED:

			//case ::VkFormat::VK_FORMAT_R16_UINT:

			//case ::VkFormat::VK_FORMAT_R16_SINT:

			//case ::VkFormat::VK_FORMAT_R16_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R16G16_UNORM:

			//case ::VkFormat::VK_FORMAT_R16G16_SNORM:

			//case ::VkFormat::VK_FORMAT_R16G16_USCALED:

			//case ::VkFormat::VK_FORMAT_R16G16_SSCALED:

			//case ::VkFormat::VK_FORMAT_R16G16_UINT:

			//case ::VkFormat::VK_FORMAT_R16G16_SINT:

			//case ::VkFormat::VK_FORMAT_R16G16_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R16G16B16_UNORM:

			//case ::VkFormat::VK_FORMAT_R16G16B16_SNORM:

			//case ::VkFormat::VK_FORMAT_R16G16B16_USCALED:

			//case ::VkFormat::VK_FORMAT_R16G16B16_SSCALED:

			//case ::VkFormat::VK_FORMAT_R16G16B16_UINT:

			//case ::VkFormat::VK_FORMAT_R16G16B16_SINT:

			//case ::VkFormat::VK_FORMAT_R16G16B16_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_UNORM:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_SNORM:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_USCALED:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_SSCALED:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_UINT:

			//case ::VkFormat::VK_FORMAT_R16G16B16A16_SINT:

		case ::VkFormat::VK_FORMAT_R16G16B16A16_SFLOAT:
			return ::framework::gpu::format::r16g16b16a16_sfloat;
		case ::VkFormat::VK_FORMAT_R32_UINT:
			return ::framework::gpu::format::r32_uint;
		case ::VkFormat::VK_FORMAT_R32_SINT:
			return ::framework::gpu::format::r32_sint;
		case ::VkFormat::VK_FORMAT_R32_SFLOAT:
			return ::framework::gpu::format::r32_sfloat;
		case ::VkFormat::VK_FORMAT_R32G32_UINT:
			return ::framework::gpu::format::r32g32_uint;
		case ::VkFormat::VK_FORMAT_R32G32_SINT:
			return ::framework::gpu::format::r32g32_sint;
		case ::VkFormat::VK_FORMAT_R32G32_SFLOAT:
			return ::framework::gpu::format::r32g32_sfloat;
		case ::VkFormat::VK_FORMAT_R32G32B32_UINT:
			return ::framework::gpu::format::r32g32b32_uint;
		case ::VkFormat::VK_FORMAT_R32G32B32_SINT:
			return ::framework::gpu::format::r32g32b32_sint;
		case ::VkFormat::VK_FORMAT_R32G32B32_SFLOAT:
			return ::framework::gpu::format::r32g32b32_sfloat;
		case ::VkFormat::VK_FORMAT_R32G32B32A32_UINT:
			return ::framework::gpu::format::r32g32b32a32_uint;
		case ::VkFormat::VK_FORMAT_R32G32B32A32_SINT:
			return ::framework::gpu::format::r32g32b32a32_sint;
		case ::VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT:
			return ::framework::gpu::format::r32g32b32a32_sfloat;
			//case ::VkFormat::VK_FORMAT_R64_UINT:

			//case ::VkFormat::VK_FORMAT_R64_SINT:

			//case ::VkFormat::VK_FORMAT_R64_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R64G64_UINT:

			//case ::VkFormat::VK_FORMAT_R64G64_SINT:

			//case ::VkFormat::VK_FORMAT_R64G64_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R64G64B64_UINT:

			//case ::VkFormat::VK_FORMAT_R64G64B64_SINT:

			//case ::VkFormat::VK_FORMAT_R64G64B64_SFLOAT:

			//case ::VkFormat::VK_FORMAT_R64G64B64A64_UINT:

			//case ::VkFormat::VK_FORMAT_R64G64B64A64_SINT:

			//case ::VkFormat::VK_FORMAT_R64G64B64A64_SFLOAT:

			//case ::VkFormat::VK_FORMAT_B10G11R11_UFLOAT_PACK32:

			//case ::VkFormat::VK_FORMAT_E5B9G9R9_UFLOAT_PACK32:

			//case ::VkFormat::VK_FORMAT_D16_UNORM:

			//case ::VkFormat::VK_FORMAT_X8_D24_UNORM_PACK32:

		case ::VkFormat::VK_FORMAT_D32_SFLOAT:
			return ::framework::gpu::format::d32_sfloat;
			//case ::VkFormat::VK_FORMAT_S8_UINT:

			//case ::VkFormat::VK_FORMAT_D16_UNORM_S8_UINT:

			//case ::VkFormat::VK_FORMAT_D24_UNORM_S8_UINT:

		case ::VkFormat::VK_FORMAT_D32_SFLOAT_S8_UINT:
			return ::framework::gpu::format::d32_sfloat_s8_uint;
			//case ::VkFormat::VK_FORMAT_BC1_RGB_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC1_RGB_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC1_RGBA_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC1_RGBA_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC2_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC2_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC3_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC3_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC4_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC4_SNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC5_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC5_SNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC6H_UFLOAT_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC6H_SFLOAT_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC7_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_BC7_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_EAC_R11_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_EAC_R11_SNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_EAC_R11G11_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_EAC_R11G11_SNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_4x4_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_4x4_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_5x4_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_5x4_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_5x5_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_5x5_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_6x5_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_6x5_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_6x6_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_6x6_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x5_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x5_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x6_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x6_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x8_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_8x8_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x5_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x5_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x6_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x6_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x8_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x8_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x10_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_10x10_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_12x10_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_12x10_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_12x12_UNORM_BLOCK:

			//case ::VkFormat::VK_FORMAT_ASTC_12x12_SRGB_BLOCK:

			//case ::VkFormat::VK_FORMAT_G8B8G8R8_422_UNORM:

			//case ::VkFormat::VK_FORMAT_B8G8R8G8_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM:

			//case ::VkFormat::VK_FORMAT_G8_B8R8_2PLANE_420_UNORM:

			//case ::VkFormat::VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G8_B8R8_2PLANE_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM:

			//case ::VkFormat::VK_FORMAT_R10X6_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_R10X6G10X6_UNORM_2PACK16:

			//case ::VkFormat::VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_R12X4_UNORM_PACK16:

			//case ::VkFormat::VK_FORMAT_R12X4G12X4_UNORM_2PACK16:

			//case ::VkFormat::VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16:

			//case ::VkFormat::VK_FORMAT_G16B16G16R16_422_UNORM:

			//case ::VkFormat::VK_FORMAT_B16G16R16G16_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM:

			//case ::VkFormat::VK_FORMAT_G16_B16R16_2PLANE_420_UNORM:

			//case ::VkFormat::VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G16_B16R16_2PLANE_422_UNORM:

			//case ::VkFormat::VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM:

			//case ::VkFormat::VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG:

			//case ::VkFormat::VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT:

			//case ::VkFormat::VK_FORMAT_A4R4G4B4_UNORM_PACK16_EXT:

			//case ::VkFormat::VK_FORMAT_A4B4G4R4_UNORM_PACK16_EXT:

		default:
			return ::framework::gpu::format::undefined;
		}
	}

	inline ::VkColorSpaceKHR get_vk_color_space(::framework::gpu::color_space color_space)
	{
		switch (color_space)
		{
		case ::framework::gpu::color_space::srgb_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
		case ::framework::gpu::color_space::display_p3_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT;
		case ::framework::gpu::color_space::extended_srgb_linear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT;
		case ::framework::gpu::color_space::dci_p3_linear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_DCI_P3_LINEAR_EXT;
		case ::framework::gpu::color_space::dci_p3_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT;
		case ::framework::gpu::color_space::bt709_linear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_BT709_LINEAR_EXT;
		case ::framework::gpu::color_space::bt709_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_BT709_NONLINEAR_EXT;
		case ::framework::gpu::color_space::bt2020_linear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_BT2020_LINEAR_EXT;
		case ::framework::gpu::color_space::hdr10_st2084:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_HDR10_ST2084_EXT;
		case ::framework::gpu::color_space::dolbyvision:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_DOLBYVISION_EXT;
		case ::framework::gpu::color_space::hdr10_hlg:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_HDR10_HLG_EXT;
		case ::framework::gpu::color_space::adobergb_linear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT;
		case ::framework::gpu::color_space::adobergb_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT;
		case ::framework::gpu::color_space::pass_through:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_PASS_THROUGH_EXT;
		case ::framework::gpu::color_space::extended_srgb_nonlinear:
			return ::VkColorSpaceKHR::VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT;
		default:
			::std::unreachable();
		}
	}

	inline ::framework::gpu::color_space get_color_space(::VkColorSpaceKHR vk_color_space)
	{
		switch (vk_color_space)
		{
		case::VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR:
			return ::framework::gpu::color_space::srgb_nonlinear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT:
			return ::framework::gpu::color_space::display_p3_nonlinear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT:
			return ::framework::gpu::color_space::extended_srgb_linear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_DCI_P3_LINEAR_EXT:
			return ::framework::gpu::color_space::dci_p3_linear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT:
			return ::framework::gpu::color_space::dci_p3_nonlinear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_BT709_LINEAR_EXT:
			return ::framework::gpu::color_space::bt709_linear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_BT709_NONLINEAR_EXT:
			return ::framework::gpu::color_space::bt709_nonlinear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_BT2020_LINEAR_EXT:
			return ::framework::gpu::color_space::bt2020_linear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_HDR10_ST2084_EXT:
			return ::framework::gpu::color_space::hdr10_st2084;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_DOLBYVISION_EXT:
			return ::framework::gpu::color_space::dolbyvision;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_HDR10_HLG_EXT:
			return ::framework::gpu::color_space::hdr10_hlg;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT:
			return ::framework::gpu::color_space::adobergb_linear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT:
			return ::framework::gpu::color_space::adobergb_nonlinear;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_PASS_THROUGH_EXT:
			return ::framework::gpu::color_space::pass_through;
		case::VkColorSpaceKHR::VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT:
			return ::framework::gpu::color_space::extended_srgb_nonlinear;
		default:
			::std::unreachable();
		}
	}

	inline ::framework::gpu::shader_stage_flags get_shader_stage_flags(::VkShaderStageFlags vk_shader_stage_flags);

	inline ::VkShaderStageFlags get_vk_shader_stage_flags(::framework::gpu::shader_stage_flags shader_stage_flags);

	inline ::std::uint32_t get_vk_queue_family_index(::framework::gpu::queue_family_ownership const & queue_family_ownership)
	{
		if (!!(queue_family_ownership.flags & ::framework::gpu::queue_family_ownership_flags::queue_family_ignored))
			return VK_QUEUE_FAMILY_IGNORED;
		else if (!!(queue_family_ownership.flags & ::framework::gpu::queue_family_ownership_flags::queue_family_external))
			return VK_QUEUE_FAMILY_EXTERNAL;
		else if (!!(queue_family_ownership.flags & ::framework::gpu::queue_family_ownership_flags::queue_family_foreign))
			return VK_QUEUE_FAMILY_FOREIGN_EXT;
		else
			return queue_family_ownership.queue_family_index;
	}

	inline ::VkShaderStageFlagBits get_vk_shader_stage_flag_bits(::framework::gpu::shader_stage_flags shader_stage_flags)
	{
		switch (shader_stage_flags)
		{
		case ::framework::gpu::shader_stage_flags::vertex_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT;
		case ::framework::gpu::shader_stage_flags::tessellation_control_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
		case ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
		case ::framework::gpu::shader_stage_flags::geometry_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT;
		case ::framework::gpu::shader_stage_flags::fragment_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT;
		case ::framework::gpu::shader_stage_flags::compute_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT;
		case ::framework::gpu::shader_stage_flags::all_graphics:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL_GRAPHICS;
		case ::framework::gpu::shader_stage_flags::all:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL;
		case ::framework::gpu::shader_stage_flags::raygen_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_RAYGEN_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::any_hit_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::closest_hit_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::miss_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_MISS_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::intersection_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::callable_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_CALLABLE_BIT_KHR;
		case ::framework::gpu::shader_stage_flags::task_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_TASK_BIT_NV;
		case ::framework::gpu::shader_stage_flags::mesh_bit:
			return ::VkShaderStageFlagBits::VK_SHADER_STAGE_MESH_BIT_NV;
		default:
			::std::unreachable();
		}
	}

	inline ::VkImageUsageFlags get_vk_image_usage_flags(::framework::gpu::image_usage_flags image_usage_flags)
	{
		::VkImageUsageFlags vk_image_usage_flags = 0;

		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::transfer_src_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		if (!!(image_usage_flags & (::framework::gpu::image_usage_flags::transfer_dst_bit |
			::framework::gpu::image_usage_flags::allow_clear_bit)))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::sampled_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::storage_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_STORAGE_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::color_attachment_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::transient_attachment_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT;
		if (!!(image_usage_flags & ::framework::gpu::image_usage_flags::input_attachment_bit))
			vk_image_usage_flags |= ::VkImageUsageFlagBits::VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;

		return vk_image_usage_flags;
	}

	inline ::framework::gpu::image_usage_flags get_image_usage_flags(::VkImageUsageFlags vk_image_usage_flags)
	{
		::framework::gpu::image_usage_flags image_usage_flags = ::framework::gpu::image_usage_flags::none;

		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::transfer_src_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::transfer_dst_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::sampled_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_STORAGE_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::storage_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::color_attachment_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::transient_attachment_bit;
		if (vk_image_usage_flags & ::VkImageUsageFlagBits::VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT)
			image_usage_flags |= ::framework::gpu::image_usage_flags::input_attachment_bit;

		return image_usage_flags;
	}

	inline ::VkImageLayout get_vk_image_layout(::framework::gpu::image_layout_flags image_layout_flags)
	{
		if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::undefined_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::preinitialized_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_PREINITIALIZED;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::general_bit |
			::framework::gpu::image_layout_flags::clear_color_image_general_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_general_bit)))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_GENERAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::color_attachment_optimal_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::depth_stencil_read_only_optimal_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit |
			::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit)))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_GENERAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::transfer_src_optimal_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::transfer_dst_optimal_bit |
			::framework::gpu::image_layout_flags::clear_color_image_transfer_dst_optimal_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_transfer_dst_optimal_bit)))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::present_src_bit))
			return ::VkImageLayout::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		else
			::std::unreachable();
	}

	inline ::VkDependencyFlags get_vk_dependency_flags(::framework::gpu::dependency_flags dependency_flags)
	{
		::VkDependencyFlags vk_dependency_flags = 0;

		if (!!(dependency_flags & ::framework::gpu::dependency_flags::by_region_bit))
			vk_dependency_flags |= ::VkDependencyFlagBits::VK_DEPENDENCY_BY_REGION_BIT;
		if (!!(dependency_flags & ::framework::gpu::dependency_flags::view_local_bit))
			vk_dependency_flags |= ::VkDependencyFlagBits::VK_DEPENDENCY_VIEW_LOCAL_BIT;
		if (!!(dependency_flags & ::framework::gpu::dependency_flags::device_group_bit))
			vk_dependency_flags |= ::VkDependencyFlagBits::VK_DEPENDENCY_DEVICE_GROUP_BIT;

		return vk_dependency_flags;
	}

	inline ::VkDependencyFlags get_vk_descriptor_binding_flags(::framework::gpu::descriptor_binding_flags descriptor_binding_flags)
	{
		::VkDescriptorBindingFlagsEXT vk_descriptor_binding_flags = 0;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::update_after_bind_bit))
			vk_descriptor_binding_flags |= ::VkDescriptorBindingFlagBitsEXT::VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::update_unused_while_pending_bit))
			vk_descriptor_binding_flags |= ::VkDescriptorBindingFlagBitsEXT::VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::partially_bound_bit))
			vk_descriptor_binding_flags |= ::VkDescriptorBindingFlagBitsEXT::VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::variable_descriptor_count_bit))
			vk_descriptor_binding_flags |= ::VkDescriptorBindingFlagBitsEXT::VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT;
		return vk_descriptor_binding_flags;
	}

	inline constexpr ::VkAccessFlags get_vk_access_flags(::framework::gpu::access_flags access_flags)
	{
		::VkAccessFlags vk_access_flags = 0;

		if (!!(access_flags & ::framework::gpu::access_flags::indirect_command_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::index_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_INDEX_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::vertex_attribute_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::uniform_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_UNIFORM_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::input_attachment_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_SHADER_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::color_attachment_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::color_attachment_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::depth_stencil_attachment_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::depth_stencil_attachment_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::transfer_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_TRANSFER_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::transfer_write_bit) || !!(access_flags & ::framework::gpu::access_flags::fill_buffer_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::host_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_HOST_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::host_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_HOST_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::memory_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_MEMORY_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::memory_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_MEMORY_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::command_preprocess_read_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV;
		if (!!(access_flags & ::framework::gpu::access_flags::command_preprocess_write_bit))
			vk_access_flags |= ::VkAccessFlagBits::VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV;

		return vk_access_flags;
	}

	inline constexpr ::VkAccessFlags2 get_vk_access_flags2(::framework::gpu::access_flags access_flags)
	{
		::VkAccessFlags2 vk_access_flags = ::VK_ACCESS_2_NONE;

		if (!!(access_flags & ::framework::gpu::access_flags::indirect_command_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_INDIRECT_COMMAND_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::index_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_INDEX_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::vertex_attribute_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_VERTEX_ATTRIBUTE_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::uniform_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_UNIFORM_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::input_attachment_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_INPUT_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_SHADER_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_SHADER_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::color_attachment_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_COLOR_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::color_attachment_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::depth_stencil_attachment_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::depth_stencil_attachment_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::transfer_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_TRANSFER_READ_BIT;
		if (!!(access_flags & (::framework::gpu::access_flags::transfer_write_bit |
			::framework::gpu::access_flags::fill_buffer_bit |
			::framework::gpu::access_flags::clear_color_image_bit |
			::framework::gpu::access_flags::clear_depth_stencil_image_bit)))
			vk_access_flags |= ::VK_ACCESS_2_TRANSFER_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::host_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_HOST_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::host_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_HOST_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::memory_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_MEMORY_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::memory_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_MEMORY_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_sampled_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_SHADER_SAMPLED_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_storage_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_SHADER_STORAGE_READ_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_storage_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_SHADER_STORAGE_WRITE_BIT;
		if (!!(access_flags & ::framework::gpu::access_flags::transform_feedback_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_TRANSFORM_FEEDBACK_WRITE_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::transform_feedback_counter_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::transform_feedback_counter_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::conditional_rendering_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_CONDITIONAL_RENDERING_READ_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::command_preprocess_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_COMMAND_PREPROCESS_READ_BIT_NV;
		if (!!(access_flags & ::framework::gpu::access_flags::command_preprocess_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_COMMAND_PREPROCESS_WRITE_BIT_NV;
		if (!!(access_flags & ::framework::gpu::access_flags::fragment_shading_rate_attachment_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR;
		if (!!(access_flags & ::framework::gpu::access_flags::acceleration_structure_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;
		if (!!(access_flags & ::framework::gpu::access_flags::acceleration_structure_write_bit))
			vk_access_flags |= ::VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
		if (!!(access_flags & ::framework::gpu::access_flags::fragment_density_map_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_FRAGMENT_DENSITY_MAP_READ_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::color_attachment_read_noncoherent_bit))
			vk_access_flags |= ::VK_ACCESS_2_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT;
		if (!!(access_flags & ::framework::gpu::access_flags::invocation_mask_read_bit))
			vk_access_flags |= ::VK_ACCESS_2_INVOCATION_MASK_READ_BIT_HUAWEI;

		return vk_access_flags;
	}

	inline ::VkAttachmentLoadOp get_vk_attachment_load_op(::framework::gpu::attachment_load_op attachment_load_op)
	{
		::VkAttachmentLoadOp vk_attachment_load_op;

		switch (attachment_load_op)
		{
		case ::framework::gpu::attachment_load_op::load:
			vk_attachment_load_op = ::VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_LOAD;
			break;
		case ::framework::gpu::attachment_load_op::clear:
			vk_attachment_load_op = ::VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR;
			break;
		case ::framework::gpu::attachment_load_op::dont_care:
			vk_attachment_load_op = ::VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			break;
		}

		return vk_attachment_load_op;
	};

	inline ::VkAttachmentStoreOp get_vk_attachment_store_op(::framework::gpu::attachment_store_op attachment_store_op)
	{
		::VkAttachmentStoreOp vk_attachment_store_op;

		switch (attachment_store_op)
		{
		case ::framework::gpu::attachment_store_op::store:
			vk_attachment_store_op = ::VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE;
			break;
		case ::framework::gpu::attachment_store_op::dont_care:
			vk_attachment_store_op = ::VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE;
			break;
		case ::framework::gpu::attachment_store_op::none:
			vk_attachment_store_op = ::VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_NONE;
			break;
		}

		return vk_attachment_store_op;
	};

	inline ::VkPipelineStageFlags get_vk_pipeline_stage_flags(::framework::gpu::pipeline_stage_flags pipeline_stage_flags)
	{
		::VkPipelineStageFlags vk_pipeline_stage_flags = 0;

		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::draw_indirect_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_input_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::tessellation_control_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::tessellation_evaluation_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::geometry_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::compute_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::transfer_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::host_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_HOST_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_graphics_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_commands_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::transform_feedback_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::conditional_rendering_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_shading_rate_attachment_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::task_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::mesh_shader_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_density_process_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::command_preprocess_bit))
			vk_pipeline_stage_flags |= ::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV;

		return vk_pipeline_stage_flags;
	}

	inline ::VkPipelineStageFlags2KHR get_vk_pipeline_stage_flags2(::framework::gpu::pipeline_stage_flags pipeline_stage_flags)
	{
		::VkPipelineStageFlags2KHR vk_pipeline_stage_flags = ::VK_PIPELINE_STAGE_2_NONE_KHR;

		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::draw_indirect_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_DRAW_INDIRECT_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_input_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_VERTEX_INPUT_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_VERTEX_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::tessellation_control_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TESSELLATION_CONTROL_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::tessellation_evaluation_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TESSELLATION_EVALUATION_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::geometry_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_GEOMETRY_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::compute_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_transfer_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_ALL_TRANSFER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::transfer_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TRANSFER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::host_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_HOST_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_graphics_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_commands_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::copy_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_COPY_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::resolve_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_RESOLVE_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::blit_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_BLIT_BIT_KHR;
		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::clear_bit |
			::framework::gpu::pipeline_stage_flags::fill_buffer_bit |
			::framework::gpu::pipeline_stage_flags::clear_color_image_bit |
			::framework::gpu::pipeline_stage_flags::clear_depth_stencil_image_bit)))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_CLEAR_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::index_input_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_INDEX_INPUT_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_attribute_input_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_VERTEX_ATTRIBUTE_INPUT_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::pre_rasterization_shaders_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_PRE_RASTERIZATION_SHADERS_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::transform_feedback_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TRANSFORM_FEEDBACK_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::conditional_rendering_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_CONDITIONAL_RENDERING_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::command_preprocess_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_COMMAND_PREPROCESS_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_shading_rate_attachment_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_SHADING_RATE_IMAGE_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::fragment_density_process_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_FRAGMENT_DENSITY_PROCESS_BIT_EXT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::task_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_TASK_SHADER_BIT_NV;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::mesh_shader_bit))
			vk_pipeline_stage_flags |= ::VK_PIPELINE_STAGE_2_MESH_SHADER_BIT_NV;

		return vk_pipeline_stage_flags;
	}

	inline ::VkPipelineCreateFlags get_vk_pipeline_create_flags(::framework::gpu::pipeline_create_flags pipeline_create_flags)
	{
		::VkPipelineCreateFlags vk_pipeline_create_flags = 0;

		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::disable_optimization_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::allow_derivatives_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::derivative_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_DERIVATIVE_BIT;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::view_index_from_device_index_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::dispatch_base))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_DISPATCH_BASE;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_no_null_any_hit_shaders_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_no_null_closest_hit_shaders_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_no_null_miss_shaders_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_no_null_intersection_shaders_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_skip_triangles_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_skip_aabbs_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::ray_tracing_shader_group_handle_capture_replay_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::defer_compile_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::capture_statistics_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::capture_internal_representations_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::indirect_bindable_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::library_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_LIBRARY_BIT_KHR;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::fail_on_pipeline_compile_required_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT_EXT;
		if (!!(pipeline_create_flags & ::framework::gpu::pipeline_create_flags::early_return_on_failure_bit))
			vk_pipeline_create_flags |= ::VkPipelineCreateFlagBits::VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT;

		return vk_pipeline_create_flags;
	}

	inline ::VkPipelineDynamicStateCreateFlags get_vk_pipeline_dynamic_state_create_flags(::framework::gpu::pipeline_dynamic_state_create_flags pipeline_dynamic_state_create_flags)
	{
		::VkPipelineDynamicStateCreateFlags vk_pipeline_dynamic_state_create_flags = 0;

		return vk_pipeline_dynamic_state_create_flags;
	}

	inline ::VkQueryResultFlags get_vk_query_result_flags(::framework::gpu::query_result_flags flags)
	{
		::VkQueryResultFlags vk_query_result_flags = 0;

		if (!!(flags & ::framework::gpu::query_result_flags::query_result_64_bit))
			vk_query_result_flags |= ::VkQueryResultFlagBits::VK_QUERY_RESULT_64_BIT;
		if (!!(flags & ::framework::gpu::query_result_flags::wait_bit))
			vk_query_result_flags |= ::VkQueryResultFlagBits::VK_QUERY_RESULT_WAIT_BIT;
		if (!!(flags & ::framework::gpu::query_result_flags::with_availability_bit))
			vk_query_result_flags |= ::VkQueryResultFlagBits::VK_QUERY_RESULT_WITH_AVAILABILITY_BIT;
		if (!!(flags & ::framework::gpu::query_result_flags::partial_bit))
			vk_query_result_flags |= ::VkQueryResultFlagBits::VK_QUERY_RESULT_PARTIAL_BIT;

		return vk_query_result_flags;
	}

	inline ::VkDynamicState get_vk_dynamic_state(::framework::gpu::dynamic_state dynamic_state)
	{
		::VkDynamicState vk_dynamic_state;

		switch (dynamic_state)
		{
		case ::framework::gpu::dynamic_state::viewport:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_VIEWPORT;
			break;
		case ::framework::gpu::dynamic_state::scissor:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_SCISSOR;
			break;
		case ::framework::gpu::dynamic_state::line_width:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_LINE_WIDTH;
			break;
		case ::framework::gpu::dynamic_state::depth_bias:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_DEPTH_BIAS;
			break;
		case ::framework::gpu::dynamic_state::blend_constants:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_BLEND_CONSTANTS;
			break;
		case ::framework::gpu::dynamic_state::depth_bounds:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_DEPTH_BOUNDS;
			break;
		case ::framework::gpu::dynamic_state::stencil_compare_mask:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK;
			break;
		case ::framework::gpu::dynamic_state::stencil_write_mask:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_STENCIL_WRITE_MASK;
			break;
		case ::framework::gpu::dynamic_state::stencil_reference:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_STENCIL_REFERENCE;
			break;
		case ::framework::gpu::dynamic_state::ray_tracing_pipeline_stack_size:
			vk_dynamic_state = ::VkDynamicState::VK_DYNAMIC_STATE_RAY_TRACING_PIPELINE_STACK_SIZE_KHR;
			break;
		}

		return vk_dynamic_state;
	}

	inline ::VkPipelineShaderStageCreateFlags get_vk_pipeline_shader_stage_create_flags(::framework::gpu::pipeline_shader_stage_create_flags pipeline_shader_stage_create_flags)
	{
		::VkPipelineShaderStageCreateFlags vk_pipeline_shader_stage_create_flags = 0;

		if (!!(pipeline_shader_stage_create_flags & ::framework::gpu::pipeline_shader_stage_create_flags::allow_varying_subgroup_size_bit))
			vk_pipeline_shader_stage_create_flags |= ::VkPipelineShaderStageCreateFlagBits::VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT;
		if (!!(pipeline_shader_stage_create_flags & ::framework::gpu::pipeline_shader_stage_create_flags::require_full_subgroups_bit))
			vk_pipeline_shader_stage_create_flags |= ::VkPipelineShaderStageCreateFlagBits::VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT;

		return vk_pipeline_shader_stage_create_flags;
	}

	inline ::VkAccelerationStructureBuildTypeKHR get_vk_acceleration_structure_build_type(::framework::gpu::acceleration_structure_build_type acceleration_structure_build_type)
	{
		switch (acceleration_structure_build_type)
		{
		case ::framework::gpu::acceleration_structure_build_type::host:
			return ::VkAccelerationStructureBuildTypeKHR::VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_KHR;
		case ::framework::gpu::acceleration_structure_build_type::device:
			return ::VkAccelerationStructureBuildTypeKHR::VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR;
		case ::framework::gpu::acceleration_structure_build_type::host_or_device:
			return ::VkAccelerationStructureBuildTypeKHR::VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_OR_DEVICE_KHR;
		}
	}

	inline ::VkSurfaceTransformFlagsKHR get_vk_surface_transform_flags(::framework::gpu::surface_transform_flags surface_transform_flags)
	{
		::VkSurfaceTransformFlagsKHR vk_surface_transform_flags = 0;

		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::identity_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::rotate_90_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::rotate_180_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::rotate_270_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::horizontal_mirror_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_90_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_180_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_270_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR;
		if (!!(surface_transform_flags & ::framework::gpu::surface_transform_flags::inherit_bit))
			vk_surface_transform_flags |= ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR;

		return vk_surface_transform_flags;
	}

	inline ::VkDeviceOrHostAddressKHR get_vk_device_or_host_address(::framework::gpu::device_or_host_address const & device_or_host_address)
	{
		return *reinterpret_cast<::VkDeviceOrHostAddressKHR const *>(&device_or_host_address);
	}

	inline ::VkDeviceOrHostAddressConstKHR get_vk_device_or_host_address_const(::framework::gpu::device_or_host_address_const const & device_or_host_address_const)
	{
		return *reinterpret_cast<::VkDeviceOrHostAddressConstKHR const *>(&device_or_host_address_const);
	}

	inline ::VkIndexType get_vk_index_type(::framework::gpu::index_type index_type)
	{
		switch (index_type)
		{
		case ::framework::gpu::index_type::uint8:
			return ::VkIndexType::VK_INDEX_TYPE_UINT16;
		case ::framework::gpu::index_type::uint16:
			return ::VkIndexType::VK_INDEX_TYPE_UINT16;
		case ::framework::gpu::index_type::uint32:
			return ::VkIndexType::VK_INDEX_TYPE_UINT32;
		case ::framework::gpu::index_type::none:
			return ::VkIndexType::VK_INDEX_TYPE_NONE_KHR;
		}
	}

	inline ::VkSemaphoreType get_vk_semaphore_type(::framework::gpu::semaphore_type semaphore_type)
	{
		switch (semaphore_type)
		{
		case ::framework::gpu::semaphore_type::binary:
			return ::VkSemaphoreType::VK_SEMAPHORE_TYPE_BINARY;
		case ::framework::gpu::semaphore_type::timeline:
			return ::VkSemaphoreType::VK_SEMAPHORE_TYPE_TIMELINE;
		}
	}

	inline ::VkAccelerationStructureTypeKHR get_vk_acceleration_structure_type(::framework::gpu::acceleration_structure_type acceleration_structure_type)
	{
		switch (acceleration_structure_type)
		{
		case framework::gpu::acceleration_structure_type::top_level:
			return ::VkAccelerationStructureTypeKHR::VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
		case framework::gpu::acceleration_structure_type::bottom_level:
			return ::VkAccelerationStructureTypeKHR::VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
		}
	}

	inline ::VkBuildAccelerationStructureFlagsKHR get_vk_build_acceleration_structure_flags(::framework::gpu::build_acceleration_structure_flags build_acceleration_structure_flags)
	{
		::VkBuildAccelerationStructureFlagsKHR vk_build_acceleration_structure_flags = 0;

		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::allow_update_bit))
			vk_build_acceleration_structure_flags |= ::VkBuildAccelerationStructureFlagBitsKHR::VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::allow_compaction_bit))
			vk_build_acceleration_structure_flags |= ::VkBuildAccelerationStructureFlagBitsKHR::VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit))
			vk_build_acceleration_structure_flags |= ::VkBuildAccelerationStructureFlagBitsKHR::VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::prefer_fast_build_bit))
			vk_build_acceleration_structure_flags |= ::VkBuildAccelerationStructureFlagBitsKHR::VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::low_memory_bit))
			vk_build_acceleration_structure_flags |= ::VkBuildAccelerationStructureFlagBitsKHR::VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_KHR;

		return vk_build_acceleration_structure_flags;
	}

	inline ::VkMemoryAllocateFlags get_vk_memory_allocate_flags(::framework::gpu::memory_allocate_flags memory_allocate_flags)
	{
		::VkMemoryAllocateFlags vk_memory_allocate_flags = 0;

		if (!!(memory_allocate_flags & framework::gpu::memory_allocate_flags::device_mask_bit))
			vk_memory_allocate_flags |= ::VkMemoryAllocateFlagBits::VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT;
		if (!!(memory_allocate_flags & framework::gpu::memory_allocate_flags::device_address_bit))
			vk_memory_allocate_flags |= ::VkMemoryAllocateFlagBits::VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;
		if (!!(memory_allocate_flags & framework::gpu::memory_allocate_flags::device_address_capture_replay_bit))
			vk_memory_allocate_flags |= ::VkMemoryAllocateFlagBits::VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT;

		return vk_memory_allocate_flags;
	}

	inline ::VkAccelerationStructureCreateFlagsKHR get_vk_acceleration_structure_create_flags(::framework::gpu::acceleration_structure_create_flags acceleration_structure_create_flags)
	{
		::VkAccelerationStructureCreateFlagsKHR vk_acceleration_structure_create_flags = 0;

		if (!!(acceleration_structure_create_flags & framework::gpu::acceleration_structure_create_flags::device_address_capture_replay_bit))
			vk_acceleration_structure_create_flags |= ::VkAccelerationStructureCreateFlagBitsKHR::VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;

		return vk_acceleration_structure_create_flags;
	}

	inline ::VkGeometryTypeKHR get_vk_geometry_type(::framework::gpu::geometry_type geometry_type)
	{
		switch (geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
			return ::VkGeometryTypeKHR::VK_GEOMETRY_TYPE_TRIANGLES_KHR;
		case ::framework::gpu::geometry_type::aabbs:
			return ::VkGeometryTypeKHR::VK_GEOMETRY_TYPE_AABBS_KHR;
		case ::framework::gpu::geometry_type::instances:
			return ::VkGeometryTypeKHR::VK_GEOMETRY_TYPE_INSTANCES_KHR;
		default:
			::std::unreachable();
		}
	}

	inline ::VkBuildAccelerationStructureModeKHR get_vk_build_acceleration_structure_mode(::framework::gpu::build_acceleration_structure_mode build_acceleration_structure_mode)
	{
		switch (build_acceleration_structure_mode)
		{
		case framework::gpu::build_acceleration_structure_mode::build:
			return ::VkBuildAccelerationStructureModeKHR::VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
		case framework::gpu::build_acceleration_structure_mode::update:
			return ::VkBuildAccelerationStructureModeKHR::VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
		default:
			::std::unreachable();
		}
	}

	inline ::VkGeometryFlagsKHR get_vk_geometry_flags(::framework::gpu::geometry_flags geometry_flags)
	{
		::VkGeometryFlagsKHR vk_geometry_flags = 0;

		if (!!(geometry_flags & framework::gpu::geometry_flags::opaque_bit))
			vk_geometry_flags |= ::VkGeometryFlagBitsKHR::VK_GEOMETRY_OPAQUE_BIT_KHR;
		if (!!(geometry_flags & framework::gpu::geometry_flags::no_duplicate_any_hit_invocation_bit))
			vk_geometry_flags |= ::VkGeometryFlagBitsKHR::VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR;

		return vk_geometry_flags;
	}

	inline ::VkRayTracingShaderGroupTypeKHR get_vk_ray_tracing_shader_group_type(::framework::gpu::ray_tracing_shader_group_type ray_tracing_shader_group_type)
	{
		switch (ray_tracing_shader_group_type)
		{
		case ::framework::gpu::ray_tracing_shader_group_type::general:
			return ::VkRayTracingShaderGroupTypeKHR::VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
		case ::framework::gpu::ray_tracing_shader_group_type::triangles_hit_group:
			return ::VkRayTracingShaderGroupTypeKHR::VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
		case ::framework::gpu::ray_tracing_shader_group_type::procedural_hit_group:
			return ::VkRayTracingShaderGroupTypeKHR::VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR;
		default:
			::std::unreachable();
		}
	}

	inline ::VkBufferUsageFlags get_vk_buffer_usage_flags(::framework::gpu::buffer_usage_flags buffer_usage_flags)
	{
		::VkBufferUsageFlags vk_buffer_usage_flags = 0;

		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::transfer_src_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::transfer_dst_bit) ||
			!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::allow_clear_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::uniform_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::storage_buffer_bit) ||
			!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::sampled_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::index_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::vertex_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::indirect_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::shader_device_address_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::transform_feedback_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::transform_feedback_counter_buffer_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::conditional_rendering_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::acceleration_structure_build_input_read_only_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR;
		if (!!(buffer_usage_flags & ::framework::gpu::buffer_usage_flags::shader_binding_table_bit))
			vk_buffer_usage_flags |= ::VkBufferUsageFlagBits::VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR;

		return vk_buffer_usage_flags;
	}

	inline ::VkQueryControlFlags get_vk_query_control_flags(::framework::gpu::query_control_flags query_control_flags)
	{
		::VkQueryControlFlags vk_query_control_flags = 0;

		if (!!(query_control_flags & ::framework::gpu::query_control_flags::precise_bit))
			vk_query_control_flags |= ::VkQueryControlFlagBits::VK_QUERY_CONTROL_PRECISE_BIT;

		return vk_query_control_flags;
	}

	inline ::VkSubpassContents get_vk_subpass_contents(::framework::gpu::subpass_contents subpass_contents)
	{
		switch (subpass_contents)
		{
		case ::framework::gpu::subpass_contents::inline_:
			return ::VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE;
		case ::framework::gpu::subpass_contents::secondary_command_buffers:
			return ::VkSubpassContents::VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
		default:
			::std::unreachable();
		}
	}

	inline ::VkDescriptorType get_vk_descriptor_type(::framework::gpu::descriptor_type descriptor_type)
	{
		switch (descriptor_type)
		{
		case ::framework::gpu::descriptor_type::sampler:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_SAMPLER;
		case ::framework::gpu::descriptor_type::combined_image_sampler:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		case ::framework::gpu::descriptor_type::sampled_image:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		case ::framework::gpu::descriptor_type::storage_image:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		case ::framework::gpu::descriptor_type::uniform_texel_buffer:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		case ::framework::gpu::descriptor_type::storage_texel_buffer:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
		case ::framework::gpu::descriptor_type::uniform_buffer:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		case ::framework::gpu::descriptor_type::sampled_buffer:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		case ::framework::gpu::descriptor_type::storage_buffer:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
		case ::framework::gpu::descriptor_type::input_attachment:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		case ::framework::gpu::descriptor_type::inline_uniform_block:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT;
		case ::framework::gpu::descriptor_type::acceleration_structure:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
		default:
			return ::VkDescriptorType::VK_DESCRIPTOR_TYPE_MAX_ENUM;
		}
	}

	inline ::VkQueryPipelineStatisticFlags get_vk_query_pipeline_statistic_flags(::framework::gpu::query_pipeline_statistic_flags query_pipeline_statistic_flags)
	{
		::VkQueryPipelineStatisticFlags vk_query_pipeline_statistic_flags = 0;

		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::input_assembly_vertices_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::input_assembly_primitives_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::vertex_shader_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::geometry_shader_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::geometry_shader_primitives_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::clipping_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::clipping_primitives_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::fragment_shader_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::tessellation_control_shader_patches_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::tessellation_evaluation_shader_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT;
		if (!!(query_pipeline_statistic_flags & ::framework::gpu::query_pipeline_statistic_flags::compute_shader_invocations_bit))
			vk_query_pipeline_statistic_flags |= ::VkQueryPipelineStatisticFlagBits::VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT;

		return vk_query_pipeline_statistic_flags;
	}

	inline ::VkStridedDeviceAddressRegionKHR get_vk_strided_device_address_region(::framework::gpu::strided_device_address_region const & strided_device_address_region)
	{
		::VkStridedDeviceAddressRegionKHR vk_strided_device_address_region;
		vk_strided_device_address_region.deviceAddress = strided_device_address_region.device_address;
		vk_strided_device_address_region.stride = strided_device_address_region.stride;
		vk_strided_device_address_region.size = strided_device_address_region.size;

		return vk_strided_device_address_region;
	}

	inline ::framework::gpu::surface_transform_flags get_surface_transform_flags(::VkSurfaceTransformFlagsKHR vk_surface_transform_flags)
	{
		::framework::gpu::surface_transform_flags surface_transform_flags = ::framework::gpu::surface_transform_flags::none;

		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::identity_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::rotate_90_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::rotate_180_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::rotate_270_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::horizontal_mirror_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_90_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_180_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::horizontal_mirror_rotate_270_bit;
		if (vk_surface_transform_flags & ::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR)
			surface_transform_flags |= ::framework::gpu::surface_transform_flags::inherit_bit;

		return surface_transform_flags;
	}

	inline ::framework::gpu::composite_alpha_flags get_composite_alpha_flags(::VkCompositeAlphaFlagsKHR vk_composite_alpha_flags)
	{
		::framework::gpu::composite_alpha_flags composite_alpha_flags = ::framework::gpu::composite_alpha_flags::none;

		if (vk_composite_alpha_flags & ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
			composite_alpha_flags |= ::framework::gpu::composite_alpha_flags::opaque_bit;
		if (vk_composite_alpha_flags & ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR)
			composite_alpha_flags |= ::framework::gpu::composite_alpha_flags::pre_multiplied_bit;
		if (vk_composite_alpha_flags & ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR)
			composite_alpha_flags |= ::framework::gpu::composite_alpha_flags::post_multiplied_bit;
		if (vk_composite_alpha_flags & ::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR)
			composite_alpha_flags |= ::framework::gpu::composite_alpha_flags::inherit_bit;

		return composite_alpha_flags;
	}

	inline void * default_allocation(void * user_data, ::std::size_t size, ::std::size_t alignment, ::framework::gpu::system_allocation_scope allocation_scope)
	{
		return ::std::malloc(size);
	}

	inline void * default_reallocation(void * user_data, void * original, ::std::size_t size, ::std::size_t alignment, ::framework::gpu::system_allocation_scope allocation_scope)
	{
		return ::std::realloc(original, size);
	}

	inline void default_free(void * user_data, void * memory)
	{
		::std::free(memory);
	}
}

#include "gpu/vulkan/core.inl"