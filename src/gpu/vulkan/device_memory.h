#pragma once

#include "gpu/device_memory.h"

namespace framework::gpu::vulkan
{
	class device_memory final : public ::framework::gpu::device_memory
	{
	public:
		device_memory(::framework::gpu::vulkan::device * device, ::framework::gpu::memory_allocate_info const & allocate_info, ::VkDeviceMemory vk_device_memory);

		::VkDeviceMemory get_vk_device_memory() const { return vk_device_memory; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkDeviceMemory vk_device_memory;
	};
}