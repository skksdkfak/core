#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::render_pass::render_pass(::framework::gpu::vulkan::device * device, ::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	::VkRenderPassCreateFlags vk_render_pass_create_flags = 0;

	::framework::gpu::common::unique_ptr<::VkAttachmentDescription[]> vk_attachment_descriptions;
	if (create_info->attachment_count)
	{
		vk_attachment_descriptions = ::framework::gpu::common::make_unique<::VkAttachmentDescription[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->attachment_count);

		for (::std::uint32_t i = 0; i < create_info->attachment_count; i++)
		{
			::VkAttachmentDescriptionFlags vk_attachment_description_flags = 0;
			if (!!(create_info->attachments[i].flags & ::framework::gpu::attachment_description_flags::may_alias_bit))
				vk_attachment_description_flags |= ::VkAttachmentDescriptionFlagBits::VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT;

			vk_attachment_descriptions[i].flags = vk_attachment_description_flags;
			vk_attachment_descriptions[i].format = ::framework::gpu::vulkan::get_vk_format(create_info->attachments[i].format);
			vk_attachment_descriptions[i].samples = ::framework::gpu::vulkan::get_vk_sample_count_flag_bits(create_info->attachments[i].samples);
			vk_attachment_descriptions[i].loadOp = ::framework::gpu::vulkan::get_vk_attachment_load_op(create_info->attachments[i].load_op);
			vk_attachment_descriptions[i].storeOp = ::framework::gpu::vulkan::get_vk_attachment_store_op(create_info->attachments[i].store_op);
			vk_attachment_descriptions[i].stencilLoadOp = ::framework::gpu::vulkan::get_vk_attachment_load_op(create_info->attachments[i].stencil_load_op);
			vk_attachment_descriptions[i].stencilStoreOp = ::framework::gpu::vulkan::get_vk_attachment_store_op(create_info->attachments[i].stencil_store_op);
			vk_attachment_descriptions[i].initialLayout = ::framework::gpu::vulkan::get_vk_image_layout(create_info->attachments[i].initial_layout);
			vk_attachment_descriptions[i].finalLayout = ::framework::gpu::vulkan::get_vk_image_layout(create_info->attachments[i].final_layout);
		}
	}

	::framework::gpu::common::unique_ptr<::VkSubpassDescription[]> vk_subpass_descriptions;
	::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]> vk_input_attachments;
	::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]> vk_color_attachments;
	::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAttachmentReference>[]> vk_depth_stencil_attachments;
	::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]> vk_resolve_attachments;
	::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::std::uint32_t[]>[]> vk_preserve_attachments;
	if (create_info->subpass_count)
	{
		vk_subpass_descriptions = ::framework::gpu::common::make_unique<::VkSubpassDescription[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);
		vk_input_attachments = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);
		vk_color_attachments = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);
		vk_depth_stencil_attachments = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAttachmentReference>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);
		vk_resolve_attachments = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAttachmentReference[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);
		vk_preserve_attachments = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::std::uint32_t[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpass_count);

		auto const & get_vk_attachment_reference = [](::framework::gpu::attachment_reference const & attachment_reference, ::VkAttachmentReference & vk_attachment_reference) -> void
		{
			vk_attachment_reference.attachment = attachment_reference.attachment;
			vk_attachment_reference.layout = ::framework::gpu::vulkan::get_vk_image_layout(attachment_reference.layout);
		};

		for (::std::uint32_t i = 0; i < create_info->subpass_count; i++)
		{
			::VkSubpassDescriptionFlags vk_subpass_description_flags = 0;
			if (!!(create_info->subpasses[i].flags & ::framework::gpu::subpass_description_flags::per_view_attributes_bit))
				vk_subpass_description_flags |= VkSubpassDescriptionFlagBits::VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX;
			if (!!(create_info->subpasses[i].flags & ::framework::gpu::subpass_description_flags::per_view_position_x_only_bit))
				vk_subpass_description_flags |= VkSubpassDescriptionFlagBits::VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX;

			if (create_info->subpasses[i].input_attachment_count)
			{
				vk_input_attachments[i] = ::framework::gpu::common::make_unique<::VkAttachmentReference[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpasses[i].input_attachment_count);
				for (::std::uint32_t j = 0; j < create_info->subpasses[i].input_attachment_count; j++)
				{
					get_vk_attachment_reference(create_info->subpasses[i].input_attachments[j], vk_input_attachments[i][j]);
				}
			}

			if (create_info->subpasses[i].color_attachment_count)
			{
				vk_color_attachments[i] = ::framework::gpu::common::make_unique<::VkAttachmentReference[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpasses[i].color_attachment_count);
				for (::std::uint32_t j = 0; j < create_info->subpasses[i].color_attachment_count; j++)
				{
					get_vk_attachment_reference(create_info->subpasses[i].color_attachments[j], vk_color_attachments[i][j]);
				}
			}

			if (create_info->subpasses[i].depth_stencil_attachment)
			{
				vk_depth_stencil_attachments[i] = ::framework::gpu::common::make_unique<::VkAttachmentReference>(this->allocator, ::framework::gpu::system_allocation_scope::command);
				get_vk_attachment_reference(*create_info->subpasses[i].depth_stencil_attachment, *vk_depth_stencil_attachments[i].get());
			}

			if (create_info->subpasses[i].resolve_attachments)
			{
				vk_resolve_attachments[i] = ::framework::gpu::common::make_unique<::VkAttachmentReference[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpasses[i].color_attachment_count);
				for (::std::uint32_t j = 0; j < create_info->subpasses[i].color_attachment_count; j++)
				{
					get_vk_attachment_reference(create_info->subpasses[i].resolve_attachments[j], vk_resolve_attachments[i][j]);
				}
			}

			if (create_info->subpasses[i].preserve_attachment_count)
			{
				vk_preserve_attachments[i] = ::framework::gpu::common::make_unique<::std::uint32_t[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->subpasses[i].preserve_attachment_count);
				::std::memcpy(vk_preserve_attachments[i].get(), create_info->subpasses[i].preserve_attachments, sizeof(::std::uint32_t) * create_info->subpasses[i].preserve_attachment_count);
			}

			vk_subpass_descriptions[i].flags = vk_subpass_description_flags;
			vk_subpass_descriptions[i].pipelineBindPoint = ::VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS;
			vk_subpass_descriptions[i].inputAttachmentCount = create_info->subpasses[i].input_attachment_count;
			vk_subpass_descriptions[i].pInputAttachments = vk_input_attachments[i].get();
			vk_subpass_descriptions[i].colorAttachmentCount = create_info->subpasses[i].color_attachment_count;
			vk_subpass_descriptions[i].pColorAttachments = vk_color_attachments[i].get();
			vk_subpass_descriptions[i].pResolveAttachments = vk_resolve_attachments[i].get();
			vk_subpass_descriptions[i].pDepthStencilAttachment = vk_depth_stencil_attachments[i].get();
			vk_subpass_descriptions[i].preserveAttachmentCount = create_info->subpasses[i].preserve_attachment_count;
			vk_subpass_descriptions[i].pPreserveAttachments = vk_preserve_attachments[i].get();
		}
	}

	::framework::gpu::common::unique_ptr<::VkSubpassDependency[]> vk_subpass_dependencies;
	if (create_info->dependency_count)
	{
		vk_subpass_dependencies = ::framework::gpu::common::make_unique<::VkSubpassDependency[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->dependency_count);

		for (::std::uint32_t dependency = 0; dependency < create_info->dependency_count; ++dependency)
		{
			vk_subpass_dependencies[dependency].srcSubpass = create_info->dependencies[dependency].src_subpass;
			vk_subpass_dependencies[dependency].dstSubpass = create_info->dependencies[dependency].dst_subpass;
			vk_subpass_dependencies[dependency].srcStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags(create_info->dependencies[dependency].src_stage_mask);
			vk_subpass_dependencies[dependency].dstStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags(create_info->dependencies[dependency].dst_stage_mask);
			vk_subpass_dependencies[dependency].srcAccessMask = ::framework::gpu::vulkan::get_vk_access_flags(create_info->dependencies[dependency].src_access_mask);
			vk_subpass_dependencies[dependency].dstAccessMask = ::framework::gpu::vulkan::get_vk_access_flags(create_info->dependencies[dependency].dst_access_mask);
			vk_subpass_dependencies[dependency].dependencyFlags = ::framework::gpu::vulkan::get_vk_dependency_flags(create_info->dependencies[dependency].dependency_flags);
		}
	}

	::VkRenderPassCreateInfo vk_render_pass_create_info;
	vk_render_pass_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	vk_render_pass_create_info.pNext = nullptr;
	vk_render_pass_create_info.flags = vk_render_pass_create_flags;
	vk_render_pass_create_info.attachmentCount = create_info->attachment_count;
	vk_render_pass_create_info.pAttachments = vk_attachment_descriptions.get();
	vk_render_pass_create_info.subpassCount = create_info->subpass_count;
	vk_render_pass_create_info.pSubpasses = vk_subpass_descriptions.get();
	vk_render_pass_create_info.dependencyCount = create_info->dependency_count;
	vk_render_pass_create_info.pDependencies = vk_subpass_dependencies.get();
	result = ::framework::gpu::vulkan::throw_result(::vkCreateRenderPass(device->get_vk_device(), &vk_render_pass_create_info, nullptr, &this->vk_render_pass));
}