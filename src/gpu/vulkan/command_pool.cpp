#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::command_pool::command_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
    allocator(allocator),
    allocated_command_buffers({this->allocator, ::framework::gpu::system_allocation_scope::object })
{
    ::VkResult vk_result;

    ::VkCommandPoolCreateFlags vk_command_pool_create_flags = 0;
    if (!!(create_info->flags & ::framework::gpu::command_pool_create_flags::transient_bit))
        vk_command_pool_create_flags |= ::VkCommandPoolCreateFlagBits::VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    if (!!(create_info->flags & ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit))
        vk_command_pool_create_flags |= ::VkCommandPoolCreateFlagBits::VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    ::VkCommandPoolCreateInfo vk_command_pool_create_info;
    vk_command_pool_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    vk_command_pool_create_info.pNext = nullptr;
    vk_command_pool_create_info.flags = vk_command_pool_create_flags;
    vk_command_pool_create_info.queueFamilyIndex = create_info->queue_family_index;

    vk_result = ::vkCreateCommandPool(device->get_vk_device(), &vk_command_pool_create_info, nullptr, &this->vk_command_pool);

    result = ::framework::gpu::vulkan::throw_result(vk_result);
}

::framework::gpu::vulkan::command_pool::~command_pool()
{
    for (auto command_buffer : this->allocated_command_buffers)
    {
        DELETE(command_buffer, this->allocator);
    }
}

void ::framework::gpu::vulkan::command_pool::allocate_command_buffer(::framework::gpu::vulkan::command_buffer * command_buffer)
{
    this->allocated_command_buffers.insert(command_buffer);
}

void ::framework::gpu::vulkan::command_pool::free_command_buffer(::framework::gpu::vulkan::command_buffer * command_buffer)
{
    this->allocated_command_buffers.erase(command_buffer);
    DELETE(command_buffer, this->allocator);
}