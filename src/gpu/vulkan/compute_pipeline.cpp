#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::compute_pipeline::compute_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::compute_pipeline_create_info const * create_info, VkPipeline vk_pipeline) noexcept :
	::framework::gpu::vulkan::pipeline(device, vk_pipeline)
{
}