﻿inline ::framework::gpu::shader_stage_flags (::framework::gpu::vulkan::get_shader_stage_flags)(::VkShaderStageFlags vk_shader_stage_flags)
{
	::framework::gpu::shader_stage_flags shader_stage_flags = ::framework::gpu::shader_stage_flags::none;

	if ((vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL) == ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL)
		shader_stage_flags |= ::framework::gpu::shader_stage_flags::all;
	else if ((vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL_GRAPHICS) == ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL_GRAPHICS)
		shader_stage_flags |= ::framework::gpu::shader_stage_flags::all_graphics;
	else
	{
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::vertex_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::tessellation_control_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::geometry_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::fragment_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::compute_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_RAYGEN_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::raygen_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_ANY_HIT_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::any_hit_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::closest_hit_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_MISS_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::miss_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_INTERSECTION_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::intersection_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_CALLABLE_BIT_KHR))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::callable_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_TASK_BIT_EXT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::task_bit;
		if (!!(vk_shader_stage_flags & ::VkShaderStageFlagBits::VK_SHADER_STAGE_MESH_BIT_EXT))
			shader_stage_flags |= ::framework::gpu::shader_stage_flags::mesh_bit;
	}

	return shader_stage_flags;
}

inline ::VkShaderStageFlags (::framework::gpu::vulkan::get_vk_shader_stage_flags)(::framework::gpu::shader_stage_flags shader_stage_flags)
{
	::VkShaderStageFlags vk_shader_stage_flags = 0;

	if ((shader_stage_flags & ::framework::gpu::shader_stage_flags::all) == ::framework::gpu::shader_stage_flags::all)
		vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL;
	else if ((shader_stage_flags & ::framework::gpu::shader_stage_flags::all_graphics) == ::framework::gpu::shader_stage_flags::all_graphics)
		vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_ALL_GRAPHICS;
	else
	{
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::vertex_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::tessellation_control_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::geometry_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::fragment_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::compute_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::raygen_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_RAYGEN_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::any_hit_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::closest_hit_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::miss_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_MISS_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::intersection_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::callable_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_CALLABLE_BIT_KHR;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::task_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_TASK_BIT_EXT;
		if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::mesh_bit))
			vk_shader_stage_flags |= ::VkShaderStageFlagBits::VK_SHADER_STAGE_MESH_BIT_EXT;
	}

	return vk_shader_stage_flags;
}