#define NOMINMAX
#include "gpu/vulkan/core.hpp"
#include <limits>

::framework::gpu::vulkan::descriptor_pool::descriptor_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	descriptor_set_free_list_head(0)
{
	::VkResult vk_result;

	::VkDescriptorPoolCreateFlags vk_descriptor_pool_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::descriptor_pool_create_flags::free_descriptor_set_bit))
		vk_descriptor_pool_create_flags |= ::VkDescriptorPoolCreateFlagBits::VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	if (!!(create_info->flags & ::framework::gpu::descriptor_pool_create_flags::update_after_bind_bit))
		vk_descriptor_pool_create_flags |= ::VkDescriptorPoolCreateFlagBits::VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT_EXT;
	if (!!(create_info->flags & ::framework::gpu::descriptor_pool_create_flags::host_only_bit))
		vk_descriptor_pool_create_flags |= ::VkDescriptorPoolCreateFlagBits::VK_DESCRIPTOR_POOL_CREATE_HOST_ONLY_BIT_VALVE;

	::std::uint32_t pool_size_count = 0;
	::framework::gpu::common::unique_ptr<::VkDescriptorPoolSize[]> vk_descriptor_pool_sizes;
	if (create_info->pool_size_count)
	{
		vk_descriptor_pool_sizes = ::framework::gpu::common::make_unique<::VkDescriptorPoolSize[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->pool_size_count);
		for (::std::uint32_t i = 0; i < create_info->pool_size_count; i++)
		{
			::VkDescriptorType vk_descriptor_type = ::framework::gpu::vulkan::get_vk_descriptor_type(create_info->pool_sizes[i].type);
			if (vk_descriptor_type == ::VkDescriptorType::VK_DESCRIPTOR_TYPE_MAX_ENUM)
			{
				continue;
			}
			vk_descriptor_pool_sizes[pool_size_count].type = vk_descriptor_type;
			vk_descriptor_pool_sizes[pool_size_count].descriptorCount = create_info->pool_sizes[i].descriptor_count;
			pool_size_count++;
		}
	}

	::VkDescriptorPoolCreateInfo vk_descriptor_pool_create_info;
	vk_descriptor_pool_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	vk_descriptor_pool_create_info.pNext = nullptr;
	vk_descriptor_pool_create_info.flags = vk_descriptor_pool_create_flags;
	vk_descriptor_pool_create_info.maxSets = create_info->max_sets;
	vk_descriptor_pool_create_info.poolSizeCount = pool_size_count;
	vk_descriptor_pool_create_info.pPoolSizes = vk_descriptor_pool_sizes.get();

	vk_result = ::vkCreateDescriptorPool(device->get_vk_device(), &vk_descriptor_pool_create_info, nullptr, &vk_descriptor_pool);

	this->descriptor_sets = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::descriptor_set[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->max_sets);

	for (::std::uint32_t i = 0; i < create_info->max_sets - 1; i++)
		this->descriptor_sets[i].id = i + 1;
	this->descriptor_sets[create_info->max_sets - 1].id = ::std::numeric_limits<::std::uint32_t>::max();

	result = ::framework::gpu::vulkan::throw_result(vk_result);
}

void ::framework::gpu::vulkan::descriptor_pool::destroy(::framework::gpu::vulkan::device * device)
{
	::vkDestroyDescriptorPool(device->get_vk_device(), this->vk_descriptor_pool, nullptr);
}

void ::framework::gpu::vulkan::descriptor_pool::allocate_descriptor_sets(::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set ** descriptor_sets)
{
	for (::std::uint32_t i = 0; i < descriptor_set_count; i++)
	{
		[[likely]] if (this->descriptor_set_free_list_head != ::std::numeric_limits<::std::uint32_t>::max())
		{
			::std::uint32_t const next_free_index = this->descriptor_sets[this->descriptor_set_free_list_head].id;
			this->descriptor_sets[this->descriptor_set_free_list_head].id = i;
			descriptor_sets[i] = &this->descriptor_sets[this->descriptor_set_free_list_head];
			this->descriptor_set_free_list_head = next_free_index;
		}
		else
		{
			for (; i < descriptor_set_count; i++)
			{
				descriptor_sets[i] = nullptr;
			}

			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_pool_memory);
		}
	}
}

void ::framework::gpu::vulkan::descriptor_pool::free_descriptor_sets(::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets) noexcept
{
	for (::std::uint32_t i = 0; i < descriptor_set_count; i++)
	{
		if (descriptor_sets[i])
		{
			::std::uint32_t const descriptor_set_id = static_cast<::framework::gpu::vulkan::descriptor_set *>(descriptor_sets[i])->id;
			this->descriptor_sets[descriptor_set_id].id = this->descriptor_set_free_list_head;
			this->descriptor_set_free_list_head = descriptor_set_id;
		}
	}
}