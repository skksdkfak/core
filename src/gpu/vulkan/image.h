#pragma once

#include "gpu/image.h"

namespace framework::gpu::vulkan
{
	class swap_chain;

	class image final : public ::framework::gpu::image
	{
	public:
		image(::framework::gpu::vulkan::device * device, ::framework::gpu::image_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		image(::framework::gpu::vulkan::device * device, ::framework::gpu::image_create_info const * create_info, ::VkImage vk_image, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkImage get_vk_image() const { return this->vk_image; }

	private:
		friend class ::framework::gpu::vulkan::device;
		friend class ::framework::gpu::vulkan::swap_chain;

		::VkImage vk_image;
	};
}