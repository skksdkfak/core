#pragma once

#include "gpu/descriptor_set.h"

namespace framework::gpu::vulkan
{
	class descriptor_set final : public ::framework::gpu::descriptor_set
	{
	public:
		::VkDescriptorSet get_vk_descriptor_set() const { return this->vk_descriptor_set; }

	private:
		friend class ::framework::gpu::vulkan::device;
		friend class ::framework::gpu::vulkan::descriptor_pool;

		::VkDescriptorSet vk_descriptor_set;
		::std::uint32_t id;
	};
}