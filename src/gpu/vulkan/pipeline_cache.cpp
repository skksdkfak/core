#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::pipeline_cache::pipeline_cache(::framework::gpu::vulkan::device * device, ::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkPipelineCacheCreateFlags vk_pipeline_cache_create_flags = 0;
	
	::VkPipelineCacheCreateInfo vk_pipeline_cache_create_info;
	vk_pipeline_cache_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	vk_pipeline_cache_create_info.pNext = nullptr;
	vk_pipeline_cache_create_info.flags = vk_pipeline_cache_create_flags;
	vk_pipeline_cache_create_info.initialDataSize = create_info->initial_data_size;
	vk_pipeline_cache_create_info.pInitialData = create_info->initial_data;

	result = ::framework::gpu::vulkan::throw_result(::vkCreatePipelineCache(device->get_vk_device(), &vk_pipeline_cache_create_info, nullptr, &this->vk_pipeline_cache));
}