#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::semaphore::semaphore(::framework::gpu::vulkan::device * device, ::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	void * next = nullptr;

	::VkSemaphoreTypeCreateInfo vk_semaphore_type_create_info;
	vk_semaphore_type_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
	vk_semaphore_type_create_info.pNext = next;
	vk_semaphore_type_create_info.semaphoreType = ::framework::gpu::vulkan::get_vk_semaphore_type(create_info->semaphore_type);
	vk_semaphore_type_create_info.initialValue = create_info->initial_value;

	next = &vk_semaphore_type_create_info;

	::VkSemaphoreCreateFlags vk_semaphore_create_flags = 0;

	::VkSemaphoreCreateInfo vk_semaphore_create_info;
    vk_semaphore_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    vk_semaphore_create_info.pNext = next;
    vk_semaphore_create_info.flags = vk_semaphore_create_flags;
    result = ::framework::gpu::vulkan::throw_result(::vkCreateSemaphore(device->get_vk_device(), &vk_semaphore_create_info, nullptr, &this->vk_semaphore));
}