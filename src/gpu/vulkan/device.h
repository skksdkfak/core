#pragma once

#include "gpu/device.h"

namespace framework::gpu::vulkan
{
	class device final : public ::framework::gpu::device
	{
	public:
		device(::framework::gpu::vulkan::physical_device * physical_device, ::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~device();

		void get_queue(::std::uint32_t queue_family_index, ::std::uint32_t queue_index, ::framework::gpu::queue ** queue) override;

        ::framework::gpu::result wait_idle() override;

		::framework::gpu::result allocate_memory(::framework::gpu::memory_allocate_info const * allocate_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device_memory ** device_memory) override;

		void free_memory(::framework::gpu::device_memory * device_memory, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result map_memory(::framework::gpu::device_memory * memory, ::framework::gpu::device_size offset, ::framework::gpu::device_size size, ::framework::gpu::memory_map_flags flags, void ** data) override;

		void unmap_memory(::framework::gpu::device_memory * memory) override;

		::framework::gpu::result flush_mapped_memory_ranges(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges) override;

		::framework::gpu::result invalidate_mapped_memory_ranges(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges) override;

		::framework::gpu::result bind_buffer_memory(::std::uint32_t bind_info_count, ::framework::gpu::bind_buffer_memory_info const * bind_infos) override;

		::framework::gpu::result bind_image_memory(::std::uint32_t bind_info_count, ::framework::gpu::bind_image_memory_info const * bind_infos) override;

		void get_buffer_memory_requirements(::framework::gpu::buffer_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements) override;

		void get_image_memory_requirements(::framework::gpu::image_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements) override;

		::framework::gpu::result create_fence(::framework::gpu::fence_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::fence ** fence) override;

		void destroy_fence(::framework::gpu::fence * fence, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result reset_fences(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences) override;

		::framework::gpu::result wait_for_fences(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences, bool wait_all, ::std::uint64_t timeout) override;

		::framework::gpu::result create_semaphore(::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::semaphore ** semaphore) override;

		void destroy_semaphore(::framework::gpu::semaphore * semaphore, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result get_semaphore_counter_value(::framework::gpu::semaphore * semaphore, ::std::uint64_t * value) override;

		::framework::gpu::result wait_semaphores(::framework::gpu::semaphore_wait_info const * wait_info, ::std::uint64_t timeout) override;

		::framework::gpu::result signal_semaphore(::framework::gpu::semaphore_signal_info const * signal_info) override;

		::framework::gpu::result create_query_pool(::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::query_pool ** query_pool) override;

		void destroy_query_pool(::framework::gpu::query_pool * query_pool, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result get_query_pool_results(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::std::size_t data_size, void * data, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags) override;

		::framework::gpu::result create_buffer(::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer ** buffer) override;

		void destroy_buffer(::framework::gpu::buffer * buffer, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_buffer_view(::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer_view ** buffer_view) override;

		void destroy_buffer_view(::framework::gpu::buffer_view * buffer_view, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_image(::framework::gpu::image_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image ** image) override;

		::framework::gpu::result create_image(::framework::gpu::image_create_info const * create_info, ::VkImage vk_image, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::vulkan::image ** image);

		void destroy_image(::framework::gpu::image * image, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_image_view(::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image_view ** image_view) override;

		void destroy_image_view(::framework::gpu::image_view * image_view, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_shader_module(::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::shader_module ** shader_module) override;

		void destroy_shader_module(::framework::gpu::shader_module * shader_module, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_graphics_pipelines(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::graphics_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines) override;

		::framework::gpu::result create_compute_pipelines(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::compute_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines) override;

		void destroy_pipeline(::framework::gpu::pipeline * pipeline, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_pipeline_cache(::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_cache ** pipeline_cache) override;

		void destroy_pipeline_cache(::framework::gpu::pipeline_cache * pipeline_cache, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_pipeline_layout(::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_layout ** pipeline_layout) override;

		void destroy_pipeline_layout(::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_sampler(::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::sampler ** sampler) override;

		void destroy_sampler(::framework::gpu::sampler * sampler, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_set_layout ** descriptor_set_layout) override;

		void destroy_descriptor_set_layout(::framework::gpu::descriptor_set_layout * descriptor_set_layout, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_descriptor_pool(::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_pool ** descriptor_pool) override;

		void destroy_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result allocate_descriptor_sets(::framework::gpu::descriptor_set_allocate_info const * allocate_info, ::framework::gpu::descriptor_set ** descriptor_sets) override;

		::framework::gpu::result free_descriptor_sets(::framework::gpu::descriptor_pool * descriptor_pool, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets) override;

		void update_descriptor_sets(::std::uint32_t descriptor_write_count, ::framework::gpu::write_descriptor_set const * descriptor_writes, ::std::uint32_t descriptor_copy_count, ::framework::gpu::copy_descriptor_set const * descriptor_copies) override;

		::framework::gpu::result create_frame_buffer(::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::frame_buffer ** frame_buffer) override;

		void destroy_frame_buffer(::framework::gpu::frame_buffer * frame_buffer, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_render_pass(::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass ** render_pass) override;

		void destroy_render_pass(::framework::gpu::render_pass * render_pass, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_render_pass_state(::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass_state ** render_pass_state) override;

		void destroy_render_pass_state(::framework::gpu::render_pass_state * render_pass_state, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_command_pool(::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::command_pool ** command_pool) override;

		void destroy_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result reset_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_reset_flags flags) override;

		void trim_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_trim_flags flags) override;

		::framework::gpu::result allocate_command_buffers(::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::command_buffer ** command_buffers) override;

		void free_command_buffers(::framework::gpu::command_pool * command_pool, ::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers) override;

		::framework::gpu::result create_swap_chain(::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::swap_chain ** swap_chain) override;

		void destroy_swap_chain(::framework::gpu::swap_chain * swap_chain, ::framework::gpu::allocation_callbacks const * allocator) override;

		void get_swap_chain_images(::framework::gpu::swap_chain * swap_chain, ::std::uint32_t * swap_chain_image_count, ::framework::gpu::image ** swap_chain_images) override;

		::framework::gpu::result acquire_next_image(::framework::gpu::acquire_next_image_info const * acquire_info, ::std::uint32_t * image_index) override;

		::framework::gpu::result create_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure_geometry_set ** acceleration_structure_geometry_set) override;

		void destroy_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set * acceleration_structure_geometry_set, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result create_acceleration_structure(::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure ** acceleration_structure) override;

		void destroy_acceleration_structure(::framework::gpu::acceleration_structure * acceleration_structure, ::framework::gpu::allocation_callbacks const * allocator) override;

		::framework::gpu::result build_acceleration_structures(::framework::gpu::deferred_operation * deferred_operation, ::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos) override;

		::framework::gpu::result copy_acceleration_structure(::framework::gpu::copy_acceleration_structure_info const * info) override;

		::framework::gpu::result copy_acceleration_structure_to_memory(::framework::gpu::copy_acceleration_structure_to_memory_info const * info) override;

		::framework::gpu::result copy_memory_to_acceleration_structure(::framework::gpu::copy_memory_to_acceleration_structure_info const * info) override;

		::framework::gpu::result get_ray_tracing_shader_group_handles(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data) override;

		::framework::gpu::result write_acceleration_structures_properties(::std::uint32_t acceleration_structure_count, ::framework::gpu::acceleration_structure const * acceleration_structures, ::framework::gpu::query_type query_type, ::std::size_t data_size, void * data, ::std::size_t stride) override;

		::framework::gpu::result create_ray_tracing_pipelines(::framework::gpu::deferred_operation * deferred_operation, ::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::ray_tracing_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines) override;

		::framework::gpu::device_address get_acceleration_structure_device_address(::framework::gpu::acceleration_structure_device_address_info const * info) override;

		::framework::gpu::result get_ray_tracing_capture_replay_shader_group_handles(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data) override;

		::framework::gpu::result get_device_acceleration_structure_compatibility(::framework::gpu::acceleration_structure_version const * version) override;

		void get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type build_type, ::framework::gpu::acceleration_structure_build_geometry_info const * build_info, ::framework::gpu::acceleration_structure_build_sizes_info * size_info) override;

		::framework::gpu::device_or_host_descriptor_handle get_descriptor_handle(::framework::gpu::descriptor_handle_info const * info) override;

		::framework::gpu::device_address get_buffer_device_address(::framework::gpu::buffer_device_address_info const * info) override;

		::std::uint64_t get_buffer_opaque_capture_address(::framework::gpu::buffer_device_address_info const * info) override;

		::std::uint64_t get_device_memory_opaque_capture_address(::framework::gpu::device_memory_opaque_capture_address_info const * info) override;

		::framework::gpu::result set_object_name(::framework::gpu::debug_utils_object_name_info const * name_info) override;

		::framework::gpu::vulkan::physical_device * get_physical_device() const { return this->physical_device; }

		::framework::gpu::allocation_callbacks const & get_allocation_callbacks() { return this->allocator; }

		auto const & get_dynamic_library() const { return this->dynamic_library; }

		::VkDevice get_vk_device() const { return this->vk_device; }

	private:
		friend class ::framework::gpu::vulkan::physical_device;

		::framework::gpu::vulkan::physical_device * physical_device;
		::framework::gpu::allocation_callbacks const allocator;
		::VkDevice vk_device;
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::queue>[]>[]> queues;
		struct
		{
			::PFN_vkCmdPipelineBarrier2 vkCmdPipelineBarrier2;
			::PFN_vkQueueSubmit2 vkQueueSubmit2;
			::PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
			::PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
			::PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
			::PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
			::PFN_vkBuildAccelerationStructuresKHR vkBuildAccelerationStructuresKHR;
			::PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
			::PFN_vkCmdTraceRaysKHR vkCmdTraceRaysKHR;
			::PFN_vkCmdSetRayTracingPipelineStackSizeKHR vkCmdSetRayTracingPipelineStackSizeKHR;
			::PFN_vkGetRayTracingShaderGroupHandlesKHR vkGetRayTracingShaderGroupHandlesKHR;
			::PFN_vkCreateRayTracingPipelinesKHR vkCreateRayTracingPipelinesKHR;
			::PFN_vkGetRayTracingCaptureReplayShaderGroupHandlesKHR vkGetRayTracingCaptureReplayShaderGroupHandlesKHR;
			::PFN_vkCmdWriteTimestamp2KHR vkCmdWriteTimestamp2;
			::PFN_vkSetDebugUtilsObjectNameEXT vkSetDebugUtilsObjectNameEXT;
		} dynamic_library;
	};
}