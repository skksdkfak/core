#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::image::image(::framework::gpu::vulkan::device * device, ::framework::gpu::image_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkImageCreateFlags vk_image_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::sparse_binding_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SPARSE_BINDING_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::sparse_residency_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::sparse_aliased_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SPARSE_ALIASED_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::mutable_format_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::cube_compatible_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::alias_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_ALIAS_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::split_instance_bind_regions_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::two_dimensional_array_compatible_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::block_texel_view_compatible_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::extended_usage_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_EXTENDED_USAGE_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::protected_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_PROTECTED_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::disjoint_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_DISJOINT_BIT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::corner_sampled_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::sample_locations_compatible_depth_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT;
	if (!!(create_info->flags & ::framework::gpu::image_create_flags::subsampled_bit))
		vk_image_create_flags |= ::VkImageCreateFlagBits::VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT;

	::VkImageType vk_image_type;
	switch (create_info->image_type)
	{
	case ::framework::gpu::image_type::one_dimensional:
		vk_image_type = ::VkImageType::VK_IMAGE_TYPE_1D;
		break;
	case ::framework::gpu::image_type::two_dimensional:
		vk_image_type = ::VkImageType::VK_IMAGE_TYPE_2D;
		break;
	case ::framework::gpu::image_type::three_dimensional:
		vk_image_type = ::VkImageType::VK_IMAGE_TYPE_3D;
		break;
	}

	::VkImageTiling vk_image_tiling;
	switch (create_info->tiling)
	{
	case ::framework::gpu::image_tiling::optimal:
		vk_image_tiling = ::VkImageTiling::VK_IMAGE_TILING_OPTIMAL;
		break;
	case ::framework::gpu::image_tiling::linear:
		vk_image_tiling = ::VkImageTiling::VK_IMAGE_TILING_LINEAR;
		break;
	}

	::VkSharingMode vk_sharing_mode;
	switch (create_info->sharing_mode)
	{
	case ::framework::gpu::sharing_mode::exclusive:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
		break;
	case ::framework::gpu::sharing_mode::concurrent:
		vk_sharing_mode = ::VkSharingMode::VK_SHARING_MODE_CONCURRENT;
		break;
	}

	::VkImageCreateInfo vk_image_create_info;
	vk_image_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	vk_image_create_info.pNext = nullptr;
	vk_image_create_info.flags = vk_image_create_flags;
	vk_image_create_info.imageType = vk_image_type;
	vk_image_create_info.format = ::framework::gpu::vulkan::get_vk_format(create_info->format);
	vk_image_create_info.extent.width = create_info->extent.width;
	vk_image_create_info.extent.height = create_info->extent.height;
	vk_image_create_info.extent.depth = create_info->extent.depth;
	vk_image_create_info.mipLevels = create_info->mip_levels;
	vk_image_create_info.arrayLayers = create_info->array_layers;
	vk_image_create_info.samples = ::framework::gpu::vulkan::get_vk_sample_count_flag_bits(create_info->samples);
	vk_image_create_info.tiling = vk_image_tiling;
	vk_image_create_info.usage = ::framework::gpu::vulkan::get_vk_image_usage_flags(create_info->usage);
	vk_image_create_info.sharingMode = vk_sharing_mode;
	vk_image_create_info.queueFamilyIndexCount = create_info->queue_family_index_count;
	vk_image_create_info.pQueueFamilyIndices = create_info->queue_family_indices;
	vk_image_create_info.initialLayout = ::framework::gpu::vulkan::get_vk_image_layout(create_info->initial_layout);

	result = ::framework::gpu::vulkan::throw_result(::vkCreateImage(device->get_vk_device(), &vk_image_create_info, nullptr, &this->vk_image));
}

::framework::gpu::vulkan::image::image(::framework::gpu::vulkan::device * device, ::framework::gpu::image_create_info const * create_info, ::VkImage vk_image, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	vk_image(vk_image)
{
	result = ::framework::gpu::result::success;
}