#include "gpu/vulkan/core.hpp"
#ifdef _WIN32
#include "platform/win32/connection.hpp"
#include "platform/win32/window.hpp"
#else
#ifdef __ANDROID__
#else
#if defined(_DIRECT2DISPLAY)
#else
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
#else
#include "platform/linux/xcb/connection.h"
#include "platform/linux/xcb/window.h"
#endif
#endif
#endif
#include <X11/Xutil.h>
#endif

::framework::gpu::vulkan::surface::surface(::framework::gpu::vulkan::instance * instance, ::framework::gpu::surface_create_info const * create_info)
{
#ifdef _WIN32
	::VkWin32SurfaceCreateInfoKHR vk_win32_surface_create_info;
	vk_win32_surface_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	vk_win32_surface_create_info.pNext = nullptr;
	vk_win32_surface_create_info.flags = 0;
	vk_win32_surface_create_info.hinstance = ::framework::platform::win32::connection::get_hmodule();
	vk_win32_surface_create_info.hwnd = static_cast<::framework::platform::win32::window const *>(create_info->window)->get_hwnd();
	::vkCreateWin32SurfaceKHR(static_cast<::framework::gpu::vulkan::instance const *>(instance)->get_vk_instance(), &vk_win32_surface_create_info, nullptr, &this->vk_surface);
#else
#ifdef __ANDROID__
#else
#if defined(_DIRECT2DISPLAY)
#else
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
#else
	::VkXcbSurfaceCreateInfoKHR vk_xcb_surface_create_info;
	vk_xcb_surface_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	vk_xcb_surface_create_info.pNext = nullptr;
	vk_xcb_surface_create_info.flags = 0;
	vk_xcb_surface_create_info.connection = static_cast<::framework::platform::linux_::xcb::window const *>(create_info->window)->get_connection()->get_xcb_connection();
	vk_xcb_surface_create_info.window = static_cast<::framework::platform::linux_::xcb::window const *>(create_info->window)->get_xcb_window();
	::vkCreateXcbSurfaceKHR(static_cast<::framework::gpu::vulkan::instance const *>(instance)->get_vk_instance(), &vk_xcb_surface_create_info, nullptr, &this->vk_surface);
#endif
#endif
#endif
#endif
}