#pragma once

#include "gpu/image_view.h"

namespace framework::gpu::vulkan
{
	class image_view final : public ::framework::gpu::image_view
	{
	public:
		image_view(::framework::gpu::vulkan::device * device, ::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkImageView get_vk_image_view() const { return this->vk_image_view; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkImageView vk_image_view;
	};
}