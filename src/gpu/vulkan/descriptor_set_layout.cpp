#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::descriptor_set_layout::descriptor_set_layout(::framework::gpu::vulkan::device * device, ::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	if (!!(create_info->flags & ::framework::gpu::descriptor_set_layout_create_flags::local_bit))
	{
		result = ::framework::gpu::result::success;

		this->vk_descriptor_set_layout = VK_NULL_HANDLE;

		return;
	}

	::VkDescriptorSetLayoutCreateFlags vk_descriptor_set_layout_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::descriptor_set_layout_create_flags::push_descriptor_bit))
		vk_descriptor_set_layout_create_flags |= VkDescriptorSetLayoutCreateFlagBits::VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;
	if (!!(create_info->flags & ::framework::gpu::descriptor_set_layout_create_flags::update_after_bind_pool_bit))
		vk_descriptor_set_layout_create_flags |= VkDescriptorSetLayoutCreateFlagBits::VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
	if (!!(create_info->flags & ::framework::gpu::descriptor_set_layout_create_flags::host_only_bit))
		vk_descriptor_set_layout_create_flags |= VkDescriptorSetLayoutCreateFlagBits::VK_DESCRIPTOR_SET_LAYOUT_CREATE_HOST_ONLY_POOL_BIT_VALVE;

	::framework::gpu::common::unique_ptr<::VkDescriptorSetLayoutBinding[]> vk_descriptor_set_layout_bindings;
	::framework::gpu::common::unique_ptr<::VkDescriptorBindingFlagsEXT[]> vk_descriptor_binding_flags;
	::framework::gpu::common::unique_ptr<::VkSampler[]> vk_immutable_samplers;

	::std::uint32_t binding_count = 0;
	if (create_info->binding_count)
	{
		vk_descriptor_set_layout_bindings = ::framework::gpu::common::make_unique<::VkDescriptorSetLayoutBinding[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->binding_count);
		vk_descriptor_binding_flags = ::framework::gpu::common::make_unique<::VkDescriptorBindingFlagsEXT[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->binding_count);

		for (::std::uint32_t i = 0; i < create_info->binding_count; i++)
		{
			::VkDescriptorType vk_descriptor_type = ::framework::gpu::vulkan::get_vk_descriptor_type(create_info->bindings[i].descriptor_type);
			if (vk_descriptor_type == ::VkDescriptorType::VK_DESCRIPTOR_TYPE_MAX_ENUM)
			{
				continue;
			}
			vk_descriptor_binding_flags[binding_count] = ::framework::gpu::vulkan::get_vk_descriptor_binding_flags(create_info->bindings[i].flags);

			if (create_info->bindings[i].immutable_samplers)
			{
				vk_immutable_samplers = ::framework::gpu::common::make_unique<::VkSampler[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->bindings[i].descriptor_count);

				for (::std::uint32_t j = 0; j < create_info->bindings[i].descriptor_count; j++)
				{
					vk_immutable_samplers[j] = static_cast<::framework::gpu::vulkan::sampler *>(create_info->bindings[i].immutable_samplers[j])->get_vk_sampler();
				}
			}

			vk_descriptor_set_layout_bindings[binding_count].binding = create_info->bindings[i].binding;
			vk_descriptor_set_layout_bindings[binding_count].descriptorType = vk_descriptor_type;
			vk_descriptor_set_layout_bindings[binding_count].descriptorCount = create_info->bindings[i].descriptor_count;
			vk_descriptor_set_layout_bindings[binding_count].stageFlags = ::framework::gpu::vulkan::get_vk_shader_stage_flags(create_info->bindings[i].stage_flags);
			vk_descriptor_set_layout_bindings[binding_count].pImmutableSamplers = vk_immutable_samplers.get();
			binding_count++;
		}
	}

	::VkDescriptorSetLayoutBindingFlagsCreateInfoEXT vk_descriptor_set_layout_binding_flags_create_info;
	vk_descriptor_set_layout_binding_flags_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT;
	vk_descriptor_set_layout_binding_flags_create_info.pNext = nullptr;
	vk_descriptor_set_layout_binding_flags_create_info.bindingCount = binding_count;
	vk_descriptor_set_layout_binding_flags_create_info.pBindingFlags = vk_descriptor_binding_flags.get();

	::VkDescriptorSetLayoutCreateInfo vk_descriptor_set_layout_create_info;
	vk_descriptor_set_layout_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	vk_descriptor_set_layout_create_info.pNext = &vk_descriptor_set_layout_binding_flags_create_info;
	vk_descriptor_set_layout_create_info.flags = vk_descriptor_set_layout_create_flags;
	vk_descriptor_set_layout_create_info.bindingCount = binding_count;
	vk_descriptor_set_layout_create_info.pBindings = vk_descriptor_set_layout_bindings.get();

	::VkResult vk_result = ::vkCreateDescriptorSetLayout(device->get_vk_device(), &vk_descriptor_set_layout_create_info, nullptr, &this->vk_descriptor_set_layout);

	result = ::framework::gpu::vulkan::throw_result(vk_result);
}