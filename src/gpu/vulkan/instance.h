#pragma once

#include "gpu/instance.h"

namespace framework::gpu::vulkan
{
	class physical_device;

	class instance final : public ::framework::gpu::instance
	{
	public:
		static ::framework::gpu::result create_instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance);

		static void destroy_instance(::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator);

		instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~instance();

		void enumerate_physical_devices(::std::uint32_t * physical_device_count, ::framework::gpu::physical_device ** physical_devices) override;

		void create_surface(::framework::gpu::surface_create_info const * create_info, ::framework::gpu::surface ** surface) override;

		void destroy_surface(::framework::gpu::surface * surface) override;

		::framework::gpu::allocation_callbacks const & get_allocation_callbacks() const { return this->allocator; }

		::VkInstance get_vk_instance() const { return this->vk_instance; }

		auto const & get_dynamic_library() const { return this->dynamic_library; }

	private:
		::framework::gpu::allocation_callbacks const allocator;
		::VkInstance vk_instance;
		::std::uint32_t physical_device_count;
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::physical_device>[]> physical_devices;
		struct
		{
			PFN_vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR;
		} dynamic_library;
	};
}