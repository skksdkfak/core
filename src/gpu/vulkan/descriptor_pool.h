#pragma once

#include "gpu/descriptor_pool.h"

namespace framework::gpu::vulkan
{
	class descriptor_pool final : public ::framework::gpu::descriptor_pool
	{
	public:
		descriptor_pool(::framework::gpu::vulkan::device * device, ::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkDescriptorPool get_vk_descriptor_pool() const { return this->vk_descriptor_pool; }

	private:
		friend class ::framework::gpu::vulkan::device;

		void allocate_descriptor_sets(::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set ** descriptor_sets);

		void free_descriptor_sets(::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets) noexcept;

		void destroy(::framework::gpu::vulkan::device * device);

		::framework::gpu::allocation_callbacks const allocator;
		::VkDescriptorPool vk_descriptor_pool;
		::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::descriptor_set[]> descriptor_sets;
		::std::uint32_t descriptor_set_free_list_head;
	};
}