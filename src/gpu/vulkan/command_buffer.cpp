#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::command_buffer::command_buffer(::framework::gpu::vulkan::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::VkCommandBuffer vk_command_buffer) :
	device(device),
	command_pool(static_cast<::framework::gpu::vulkan::command_pool *>(allocate_info->command_pool)),
	vk_command_buffer(vk_command_buffer)
{
}

::framework::gpu::result(::framework::gpu::vulkan::command_buffer::begin_command_buffer)(::framework::gpu::command_buffer_begin_info const * begin_info)
{
	::VkResult vk_result;

	::VkCommandBufferUsageFlags flags = 0;
	if (!!(begin_info->flags & ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit))
		flags |= ::VkCommandBufferUsageFlagBits::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	if (!!(begin_info->flags & ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit))
		flags |= ::VkCommandBufferUsageFlagBits::VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
	if (!!(begin_info->flags & ::framework::gpu::command_buffer_usage_flags::simultaneous_use_bit))
		flags |= ::VkCommandBufferUsageFlagBits::VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	::VkCommandBufferInheritanceInfo vk_command_buffer_inheritance_info;
	if (begin_info->inheritance_info)
	{
		::framework::gpu::vulkan::render_pass_state::render_pass_state_info const & render_pass_state_info = static_cast<::framework::gpu::vulkan::render_pass_state *>(begin_info->inheritance_info->render_pass_state)->get_render_pass_state_info();

		vk_command_buffer_inheritance_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		vk_command_buffer_inheritance_info.pNext = nullptr;
		vk_command_buffer_inheritance_info.renderPass = render_pass_state_info.render_pass ? render_pass_state_info.render_pass->get_vk_render_pass() : VK_NULL_HANDLE;
		vk_command_buffer_inheritance_info.subpass = begin_info->inheritance_info->subpass;
		vk_command_buffer_inheritance_info.framebuffer = render_pass_state_info.frame_buffer ? render_pass_state_info.frame_buffer->get_vk_framebuffer() : VK_NULL_HANDLE;
		vk_command_buffer_inheritance_info.occlusionQueryEnable = begin_info->inheritance_info->occlusion_query_enable;
		vk_command_buffer_inheritance_info.queryFlags = ::framework::gpu::vulkan::get_vk_query_control_flags(begin_info->inheritance_info->query_flags);
		vk_command_buffer_inheritance_info.pipelineStatistics = ::framework::gpu::vulkan::get_vk_query_pipeline_statistic_flags(begin_info->inheritance_info->pipeline_statistics);
	}

	::VkCommandBufferBeginInfo vk_command_buffer_begin_info;
	vk_command_buffer_begin_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	vk_command_buffer_begin_info.pNext = nullptr;
	vk_command_buffer_begin_info.flags = flags;
	vk_command_buffer_begin_info.pInheritanceInfo = begin_info->inheritance_info ? &vk_command_buffer_inheritance_info : nullptr;

	vk_result = ::vkBeginCommandBuffer(this->vk_command_buffer, &vk_command_buffer_begin_info);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::command_buffer::end_command_buffer)()
{
	::VkResult vk_result;

	vk_result = ::vkEndCommandBuffer(this->vk_command_buffer);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

void ::framework::gpu::vulkan::command_buffer::push_constants(::framework::gpu::pipeline_layout * layout, ::framework::gpu::shader_stage_flags stage_flags, ::std::uint32_t binding, ::std::uint32_t offset, ::std::uint32_t size, void const * values)
{
	::vkCmdPushConstants(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::pipeline_layout *>(layout)->get_vk_pipeline_layout(), get_vk_shader_stage_flags(stage_flags), static_cast<::framework::gpu::vulkan::pipeline_layout *>(layout)->get_push_constants_global_offset(binding) + offset, size, values);
}

void ::framework::gpu::vulkan::command_buffer::begin_render_pass(::framework::gpu::render_pass_begin_info const * render_pass_begin, ::framework::gpu::subpass_contents contents)
{
	::framework::gpu::vulkan::render_pass_state::render_pass_state_info const & render_pass_state_info = static_cast<::framework::gpu::vulkan::render_pass_state *>(render_pass_begin->render_pass_state)->get_render_pass_state_info();

	::VkRenderPassBeginInfo vk_render_pass_begin_info;
	vk_render_pass_begin_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	vk_render_pass_begin_info.pNext = nullptr;
	vk_render_pass_begin_info.renderPass = render_pass_state_info.render_pass ? render_pass_state_info.render_pass->get_vk_render_pass() : VK_NULL_HANDLE;
	vk_render_pass_begin_info.framebuffer = render_pass_state_info.frame_buffer ? render_pass_state_info.frame_buffer->get_vk_framebuffer() : VK_NULL_HANDLE;
	vk_render_pass_begin_info.renderArea = render_pass_state_info.render_area;
	vk_render_pass_begin_info.clearValueCount = render_pass_state_info.clear_value_count;
	vk_render_pass_begin_info.pClearValues = render_pass_state_info.clear_values.get();

	::vkCmdBeginRenderPass(this->vk_command_buffer, &vk_render_pass_begin_info, ::framework::gpu::vulkan::get_vk_subpass_contents(contents));
}

void ::framework::gpu::vulkan::command_buffer::next_subpass(::framework::gpu::subpass_contents contents)
{
	::vkCmdNextSubpass(this->vk_command_buffer, ::framework::gpu::vulkan::get_vk_subpass_contents(contents));
}

void ::framework::gpu::vulkan::command_buffer::end_render_pass()
{
	::vkCmdEndRenderPass(this->vk_command_buffer);
}

void ::framework::gpu::vulkan::command_buffer::execute_commands(::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkCommandBuffer[]> vk_command_buffers = ::framework::gpu::common::make_unique<::VkCommandBuffer[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, command_buffer_count);
		for (::std::uint32_t i = 0; i < command_buffer_count; i++)
		{
			vk_command_buffers[i] = static_cast<::framework::gpu::vulkan::command_buffer const *>(command_buffers[i])->get_vk_command_buffer();
		}

		::vkCmdExecuteCommands(this->vk_command_buffer, command_buffer_count, vk_command_buffers.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::bind_pipeline(::framework::gpu::pipeline_bind_point pipeline_bind_point, ::framework::gpu::pipeline const * pipeline)
{
	::vkCmdBindPipeline(this->vk_command_buffer, ::framework::gpu::vulkan::get_vk_pipeline_bind_point(pipeline_bind_point), static_cast<::framework::gpu::vulkan::pipeline const *>(pipeline)->get_vk_pipeline());
}

void ::framework::gpu::vulkan::command_buffer::set_viewport(::std::uint32_t first_viewport, ::std::uint32_t viewport_count, ::framework::gpu::viewport const * viewports)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkViewport[]> vk_viewports = ::framework::gpu::common::make_unique<::VkViewport[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, viewport_count);
		for (::std::uint32_t i = 0; i < viewport_count; i++)
		{
			vk_viewports[i].x = viewports[i].x;
			vk_viewports[i].y = viewports[i].y;
			vk_viewports[i].width = viewports[i].width;
			vk_viewports[i].height = viewports[i].height;
			vk_viewports[i].minDepth = viewports[i].min_depth;
			vk_viewports[i].maxDepth = viewports[i].max_depth;
		}

		::vkCmdSetViewport(this->vk_command_buffer, first_viewport, viewport_count, vk_viewports.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::set_scissor(::std::uint32_t first_scissor, ::std::uint32_t scissor_count, ::framework::gpu::rect_2d const * scissors)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkRect2D[]> vk_rect_2d = ::framework::gpu::common::make_unique<::VkRect2D[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, scissor_count);
		for (::std::uint32_t i = 0; i < scissor_count; i++)
		{
			vk_rect_2d[i].offset.x = scissors[i].offset.x;
			vk_rect_2d[i].offset.y = scissors[i].offset.y;
			vk_rect_2d[i].extent.width = scissors[i].extent.width;
			vk_rect_2d[i].extent.height = scissors[i].extent.height;
		}

		::vkCmdSetScissor(this->vk_command_buffer, first_scissor, scissor_count, vk_rect_2d.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::bind_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool)
{
}

void ::framework::gpu::vulkan::command_buffer::bind_descriptor_sets(::framework::gpu::pipeline_bind_point pipeline_bind_point, ::framework::gpu::pipeline_layout * layout, ::std::uint32_t first_set, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset_count, ::std::uint32_t const * dynamic_offsets)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkDescriptorSet[]> vk_descriptor_sets = ::framework::gpu::common::make_unique<::VkDescriptorSet[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, descriptor_set_count);
		for (::std::uint32_t i = 0; i < descriptor_set_count; i++)
		{
			vk_descriptor_sets[i] = static_cast<::framework::gpu::vulkan::descriptor_set *>(descriptor_sets[i])->get_vk_descriptor_set();
		}

		::vkCmdBindDescriptorSets(this->vk_command_buffer, ::framework::gpu::vulkan::get_vk_pipeline_bind_point(pipeline_bind_point), static_cast<::framework::gpu::vulkan::pipeline_layout *>(layout)->get_vk_pipeline_layout(), first_set, descriptor_set_count, vk_descriptor_sets.get(), dynamic_offset_count, dynamic_offsets);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::bind_index_buffer(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::framework::gpu::index_type index_type)
{
	::vkCmdBindIndexBuffer(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer const *>(buffer)->get_vk_buffer(), offset, ::framework::gpu::vulkan::get_vk_index_type(index_type));
}

void ::framework::gpu::vulkan::command_buffer::bind_vertex_buffers(::std::uint32_t first_binding, ::std::uint32_t binding_count, ::framework::gpu::buffer * const * buffers, ::framework::gpu::device_size const * offsets)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkBuffer[]> vk_buffers = ::framework::gpu::common::make_unique<::VkBuffer[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, binding_count);
		for (::std::uint32_t i = 0; i < binding_count; i++)
		{
			vk_buffers[i] = static_cast<::framework::gpu::vulkan::buffer const *>(buffers[i])->get_vk_buffer();
		}

		::vkCmdBindVertexBuffers(this->vk_command_buffer, first_binding, binding_count, vk_buffers.get(), offsets);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::draw(::std::uint32_t vertex_count, ::std::uint32_t instance_count, ::std::uint32_t first_vertex, ::std::uint32_t first_instance)
{
	::vkCmdDraw(this->vk_command_buffer, vertex_count, instance_count, first_vertex, first_instance);
}

void ::framework::gpu::vulkan::command_buffer::draw_indexed(::std::uint32_t index_count, ::std::uint32_t instance_count, ::std::uint32_t first_index, ::std::int32_t vertex_offset, ::std::uint32_t first_instance)
{
	::vkCmdDrawIndexed(this->vk_command_buffer, index_count, instance_count, first_index, vertex_offset, first_instance);
}

void ::framework::gpu::vulkan::command_buffer::draw_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride)
{
	::vkCmdDrawIndirect(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer const *>(buffer)->get_vk_buffer(), offset, draw_count, stride);
}

void ::framework::gpu::vulkan::command_buffer::draw_indexed_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride)
{
	::vkCmdDrawIndexedIndirect(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer const *>(buffer)->get_vk_buffer(), offset, draw_count, stride);
}

void ::framework::gpu::vulkan::command_buffer::dispatch(::std::uint32_t group_count_x, ::std::uint32_t group_count_y, ::std::uint32_t group_count_z)
{
	::vkCmdDispatch(this->vk_command_buffer, group_count_x, group_count_y, group_count_z);
}

void ::framework::gpu::vulkan::command_buffer::dispatch_indirect(::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset)
{
	::vkCmdDispatchIndirect(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer const *>(buffer)->get_vk_buffer(), static_cast<::VkDeviceSize>(offset));
}

void ::framework::gpu::vulkan::command_buffer::copy_buffer(::framework::gpu::buffer * src_buffer, ::framework::gpu::buffer * dst_buffer, ::std::uint32_t region_count, ::framework::gpu::buffer_copy const * regions)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkBufferCopy[]> vk_buffer_copies = ::framework::gpu::common::make_unique<::VkBufferCopy[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, region_count);
		for (::std::uint32_t i = 0; i < region_count; i++)
		{
			vk_buffer_copies[i].srcOffset = regions[i].src_offset;
			vk_buffer_copies[i].dstOffset = regions[i].dst_offset;
			vk_buffer_copies[i].size = regions[i].size;
		}

		::vkCmdCopyBuffer(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer const *>(src_buffer)->get_vk_buffer(), static_cast<::framework::gpu::vulkan::buffer const *>(dst_buffer)->get_vk_buffer(), region_count, vk_buffer_copies.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::copy_image(::framework::gpu::image * src_image, ::framework::gpu::image_layout_flags src_image_layout, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, ::framework::gpu::image_copy const * regions)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkImageCopy2[]> vk_image_copy_regions = ::framework::gpu::common::make_unique<::VkImageCopy2[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, region_count);
		for (::std::uint32_t i = 0; i < region_count; i++)
		{
			vk_image_copy_regions[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_COPY_2;
			vk_image_copy_regions[i].pNext = nullptr;
			vk_image_copy_regions[i].srcSubresource.aspectMask = ::framework::gpu::vulkan::get_vk_image_aspect_flags(regions[i].src_subresource.aspect_mask);
			vk_image_copy_regions[i].srcSubresource.mipLevel = regions[i].src_subresource.mip_level;
			vk_image_copy_regions[i].srcSubresource.baseArrayLayer = regions[i].src_subresource.base_array_layer;
			vk_image_copy_regions[i].srcSubresource.layerCount = regions[i].src_subresource.layer_count;
			vk_image_copy_regions[i].srcOffset.x = regions[i].src_offset.x;
			vk_image_copy_regions[i].srcOffset.y = regions[i].src_offset.y;
			vk_image_copy_regions[i].srcOffset.z = regions[i].src_offset.z;
			vk_image_copy_regions[i].dstSubresource.aspectMask = ::framework::gpu::vulkan::get_vk_image_aspect_flags(regions[i].dst_subresource.aspect_mask);
			vk_image_copy_regions[i].dstSubresource.mipLevel = regions[i].dst_subresource.mip_level;
			vk_image_copy_regions[i].dstSubresource.baseArrayLayer = regions[i].dst_subresource.base_array_layer;
			vk_image_copy_regions[i].dstSubresource.layerCount = regions[i].dst_subresource.layer_count;
			vk_image_copy_regions[i].dstOffset.x = regions[i].dst_offset.x;
			vk_image_copy_regions[i].dstOffset.y = regions[i].dst_offset.y;
			vk_image_copy_regions[i].dstOffset.z = regions[i].dst_offset.z;
			vk_image_copy_regions[i].extent.width = regions[i].extent.width;
			vk_image_copy_regions[i].extent.height = regions[i].extent.height;
			vk_image_copy_regions[i].extent.depth = regions[i].extent.depth;
		}

		::VkCopyImageInfo2 vk_copy_image_info_2;
		vk_copy_image_info_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2;
		vk_copy_image_info_2.pNext = nullptr;
		vk_copy_image_info_2.srcImage = static_cast<::framework::gpu::vulkan::image *>(src_image)->get_vk_image();
		vk_copy_image_info_2.srcImageLayout = ::framework::gpu::vulkan::get_vk_image_layout(src_image_layout);
		vk_copy_image_info_2.dstImage = static_cast<::framework::gpu::vulkan::image *>(dst_image)->get_vk_image();
		vk_copy_image_info_2.dstImageLayout = ::framework::gpu::vulkan::get_vk_image_layout(dst_image_layout);
		vk_copy_image_info_2.regionCount = region_count;
		vk_copy_image_info_2.pRegions = vk_image_copy_regions.get();
		::vkCmdCopyImage2(this->vk_command_buffer, &vk_copy_image_info_2);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::copy_buffer_to_image(::framework::gpu::buffer * src_buffer, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, ::framework::gpu::buffer_image_copy const * regions)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkBufferImageCopy2[]> vk_buffer_image_copy_2 = ::framework::gpu::common::make_unique<::VkBufferImageCopy2[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, region_count);
		for (::std::uint32_t i = 0; i < region_count; i++)
		{
			vk_buffer_image_copy_2[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2;
			vk_buffer_image_copy_2[i].pNext = nullptr;
			vk_buffer_image_copy_2[i].bufferOffset = regions[i].buffer_offset;
			vk_buffer_image_copy_2[i].bufferRowLength = regions[i].buffer_row_length;
			vk_buffer_image_copy_2[i].bufferImageHeight = regions[i].buffer_image_height;
			vk_buffer_image_copy_2[i].imageSubresource.aspectMask = ::framework::gpu::vulkan::get_vk_image_aspect_flags(regions[i].image_subresource.aspect_mask);
			vk_buffer_image_copy_2[i].imageSubresource.mipLevel = regions[i].image_subresource.mip_level;
			vk_buffer_image_copy_2[i].imageSubresource.baseArrayLayer = regions[i].image_subresource.base_array_layer;
			vk_buffer_image_copy_2[i].imageSubresource.layerCount = regions[i].image_subresource.layer_count;
			vk_buffer_image_copy_2[i].imageOffset.x = regions[i].image_offset.x;
			vk_buffer_image_copy_2[i].imageOffset.y = regions[i].image_offset.y;
			vk_buffer_image_copy_2[i].imageOffset.z = regions[i].image_offset.z;
			vk_buffer_image_copy_2[i].imageExtent.width = regions[i].image_extent.width;
			vk_buffer_image_copy_2[i].imageExtent.height = regions[i].image_extent.height;
			vk_buffer_image_copy_2[i].imageExtent.depth = regions[i].image_extent.depth;
		}

		::VkCopyBufferToImageInfo2 vk_copy_buffer_to_image_info_2;
		vk_copy_buffer_to_image_info_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2;
		vk_copy_buffer_to_image_info_2.pNext = nullptr;
		vk_copy_buffer_to_image_info_2.srcBuffer = static_cast<::framework::gpu::vulkan::buffer *>(src_buffer)->get_vk_buffer();
		vk_copy_buffer_to_image_info_2.dstImage = static_cast<::framework::gpu::vulkan::image *>(dst_image)->get_vk_image();
		vk_copy_buffer_to_image_info_2.dstImageLayout = ::framework::gpu::vulkan::get_vk_image_layout(dst_image_layout);
		vk_copy_buffer_to_image_info_2.regionCount = region_count;
		vk_copy_buffer_to_image_info_2.pRegions = vk_buffer_image_copy_2.get();
		::vkCmdCopyBufferToImage2(this->vk_command_buffer, &vk_copy_buffer_to_image_info_2);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::copy_image_to_buffer(::framework::gpu::copy_image_to_buffer_info const * copy_image_to_buffer_info)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkBufferImageCopy2[]> vk_buffer_image_copy = ::framework::gpu::common::make_unique<::VkBufferImageCopy2[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, copy_image_to_buffer_info->region_count);
		for (::std::uint32_t i = 0; i < copy_image_to_buffer_info->region_count; i++)
		{
			vk_buffer_image_copy[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2;
			vk_buffer_image_copy[i].pNext = nullptr;
			vk_buffer_image_copy[i].bufferOffset = copy_image_to_buffer_info->regions[i].buffer_offset;
			vk_buffer_image_copy[i].bufferRowLength = copy_image_to_buffer_info->regions[i].buffer_row_length;
			vk_buffer_image_copy[i].bufferImageHeight = copy_image_to_buffer_info->regions[i].buffer_image_height;
			vk_buffer_image_copy[i].imageSubresource.aspectMask = ::framework::gpu::vulkan::get_vk_image_aspect_flags(copy_image_to_buffer_info->regions[i].image_subresource.aspect_mask);
			vk_buffer_image_copy[i].imageSubresource.mipLevel = copy_image_to_buffer_info->regions[i].image_subresource.mip_level;
			vk_buffer_image_copy[i].imageSubresource.baseArrayLayer = copy_image_to_buffer_info->regions[i].image_subresource.base_array_layer;
			vk_buffer_image_copy[i].imageSubresource.layerCount = copy_image_to_buffer_info->regions[i].image_subresource.layer_count;
			vk_buffer_image_copy[i].imageOffset.x = copy_image_to_buffer_info->regions[i].image_offset.x;
			vk_buffer_image_copy[i].imageOffset.y = copy_image_to_buffer_info->regions[i].image_offset.y;
			vk_buffer_image_copy[i].imageOffset.z = copy_image_to_buffer_info->regions[i].image_offset.z;
			vk_buffer_image_copy[i].imageExtent.width = copy_image_to_buffer_info->regions[i].image_extent.width;
			vk_buffer_image_copy[i].imageExtent.height = copy_image_to_buffer_info->regions[i].image_extent.height;
			vk_buffer_image_copy[i].imageExtent.depth = copy_image_to_buffer_info->regions[i].image_extent.depth;
		}

		::VkCopyImageToBufferInfo2 vk_copy_image_to_buffer_info_2;
		vk_copy_image_to_buffer_info_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2;
		vk_copy_image_to_buffer_info_2.pNext = nullptr;
		vk_copy_image_to_buffer_info_2.srcImage = static_cast<::framework::gpu::vulkan::image *>(copy_image_to_buffer_info->src_image)->get_vk_image();
		vk_copy_image_to_buffer_info_2.srcImageLayout = ::framework::gpu::vulkan::get_vk_image_layout(copy_image_to_buffer_info->src_image_layout);
		vk_copy_image_to_buffer_info_2.dstBuffer = static_cast<::framework::gpu::vulkan::buffer *>(copy_image_to_buffer_info->dst_buffer)->get_vk_buffer();
		vk_copy_image_to_buffer_info_2.regionCount = copy_image_to_buffer_info->region_count;
		vk_copy_image_to_buffer_info_2.pRegions = vk_buffer_image_copy.get();
		::vkCmdCopyImageToBuffer2(this->vk_command_buffer, &vk_copy_image_to_buffer_info_2);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::update_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size data_size, void const * data)
{
	::vkCmdUpdateBuffer(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer *>(dst_buffer)->get_vk_buffer(), dst_offset, data_size, data);
}

void ::framework::gpu::vulkan::command_buffer::fill_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size size, ::std::uint32_t data, ::framework::gpu::device_size dst_view_offset, ::framework::gpu::device_descriptor_handle view_device_handle_in_current_pool, ::framework::gpu::host_descriptor_handle view_host_handle)
{
	::vkCmdFillBuffer(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::buffer *>(dst_buffer)->get_vk_buffer(), dst_offset, size, data);
}

void ::framework::gpu::vulkan::command_buffer::clear_color_image(::framework::gpu::image * image, ::framework::gpu::image_layout_flags image_layout, ::framework::gpu::clear_color_value const * color, ::std::uint32_t range_count, ::framework::gpu::image_subresource_range const * ranges, ::framework::gpu::host_descriptor_handle image_view_descriptor_handle)
{
	try
	{
		::VkClearColorValue vk_clear_value = ::framework::gpu::vulkan::get_clear_color_value(*color);
		::framework::gpu::common::unique_ptr<::VkImageSubresourceRange[]> vk_image_subresource_ranges = ::framework::gpu::common::make_unique<::VkImageSubresourceRange[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, range_count);
		for (::std::uint32_t i = 0; i < range_count; i++)
		{
			vk_image_subresource_ranges[i] = ::framework::gpu::vulkan::get_vk_image_subresource_range(ranges[i]);
		}

		::vkCmdClearColorImage(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::image *>(image)->get_vk_image(), ::framework::gpu::vulkan::get_vk_image_layout(image_layout), &vk_clear_value, range_count, vk_image_subresource_ranges.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::pipeline_barrier(::framework::gpu::dependency_info const * dependency_info)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkMemoryBarrier2KHR[]> vk_memory_barriers = ::framework::gpu::common::make_unique<::VkMemoryBarrier2KHR[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, dependency_info->memory_barrier_count);
		::framework::gpu::common::unique_ptr<::VkBufferMemoryBarrier2KHR[]> vk_buffer_memory_barriers = ::framework::gpu::common::make_unique<::VkBufferMemoryBarrier2KHR[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, dependency_info->buffer_memory_barrier_count);
		::framework::gpu::common::unique_ptr<::VkImageMemoryBarrier2KHR[]> vk_image_memory_barriers = ::framework::gpu::common::make_unique<::VkImageMemoryBarrier2KHR[]>(this->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, dependency_info->image_memory_barrier_count);

		for (::std::uint32_t memory_barrier = 0; memory_barrier < dependency_info->memory_barrier_count; ++memory_barrier)
		{
			vk_memory_barriers[memory_barrier].sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR;
			vk_memory_barriers[memory_barrier].pNext = nullptr;
			vk_memory_barriers[memory_barrier].srcStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->memory_barriers[memory_barrier].src_stage_mask);
			vk_memory_barriers[memory_barrier].srcAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->memory_barriers[memory_barrier].src_access_mask);
			vk_memory_barriers[memory_barrier].dstStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->memory_barriers[memory_barrier].dst_stage_mask);
			vk_memory_barriers[memory_barrier].dstAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->memory_barriers[memory_barrier].dst_access_mask);
		}

		for (::std::uint32_t buffer_memory_barrier = 0; buffer_memory_barrier < dependency_info->buffer_memory_barrier_count; ++buffer_memory_barrier)
		{
			vk_buffer_memory_barriers[buffer_memory_barrier].sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR;
			vk_buffer_memory_barriers[buffer_memory_barrier].pNext = nullptr;
			vk_buffer_memory_barriers[buffer_memory_barrier].srcStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->buffer_memory_barriers[buffer_memory_barrier].src_stage_mask);
			vk_buffer_memory_barriers[buffer_memory_barrier].srcAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->buffer_memory_barriers[buffer_memory_barrier].src_access_mask);
			vk_buffer_memory_barriers[buffer_memory_barrier].dstStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->buffer_memory_barriers[buffer_memory_barrier].dst_stage_mask);
			vk_buffer_memory_barriers[buffer_memory_barrier].dstAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->buffer_memory_barriers[buffer_memory_barrier].dst_access_mask);
			vk_buffer_memory_barriers[buffer_memory_barrier].srcQueueFamilyIndex = ::framework::gpu::vulkan::get_vk_queue_family_index(dependency_info->buffer_memory_barriers[buffer_memory_barrier].src_queue_family_ownership);
			vk_buffer_memory_barriers[buffer_memory_barrier].dstQueueFamilyIndex = ::framework::gpu::vulkan::get_vk_queue_family_index(dependency_info->buffer_memory_barriers[buffer_memory_barrier].dst_queue_family_ownership);
			vk_buffer_memory_barriers[buffer_memory_barrier].buffer = static_cast<::framework::gpu::vulkan::buffer const *>(dependency_info->buffer_memory_barriers[buffer_memory_barrier].buffer)->get_vk_buffer();
			vk_buffer_memory_barriers[buffer_memory_barrier].offset = dependency_info->buffer_memory_barriers[buffer_memory_barrier].offset;
			vk_buffer_memory_barriers[buffer_memory_barrier].size = dependency_info->buffer_memory_barriers[buffer_memory_barrier].size;
		}

		for (::std::uint32_t image_memory_barrier = 0; image_memory_barrier < dependency_info->image_memory_barrier_count; ++image_memory_barrier)
		{
			vk_image_memory_barriers[image_memory_barrier].sType = ::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
			vk_image_memory_barriers[image_memory_barrier].pNext = nullptr;
			vk_image_memory_barriers[image_memory_barrier].srcStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->image_memory_barriers[image_memory_barrier].src_stage_mask);
			vk_image_memory_barriers[image_memory_barrier].srcAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->image_memory_barriers[image_memory_barrier].src_access_mask);
			vk_image_memory_barriers[image_memory_barrier].dstStageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(dependency_info->image_memory_barriers[image_memory_barrier].dst_stage_mask);
			vk_image_memory_barriers[image_memory_barrier].dstAccessMask = ::framework::gpu::vulkan::get_vk_access_flags2(dependency_info->image_memory_barriers[image_memory_barrier].dst_access_mask);
			vk_image_memory_barriers[image_memory_barrier].oldLayout = ::framework::gpu::vulkan::get_vk_image_layout(dependency_info->image_memory_barriers[image_memory_barrier].old_layout);
			vk_image_memory_barriers[image_memory_barrier].newLayout = ::framework::gpu::vulkan::get_vk_image_layout(dependency_info->image_memory_barriers[image_memory_barrier].new_layout);
			vk_image_memory_barriers[image_memory_barrier].srcQueueFamilyIndex = ::framework::gpu::vulkan::get_vk_queue_family_index(dependency_info->image_memory_barriers[image_memory_barrier].src_queue_family_ownership);
			vk_image_memory_barriers[image_memory_barrier].dstQueueFamilyIndex = ::framework::gpu::vulkan::get_vk_queue_family_index(dependency_info->image_memory_barriers[image_memory_barrier].dst_queue_family_ownership);
			vk_image_memory_barriers[image_memory_barrier].image = static_cast<::framework::gpu::vulkan::image const *>(dependency_info->image_memory_barriers[image_memory_barrier].image)->get_vk_image();
			vk_image_memory_barriers[image_memory_barrier].subresourceRange.aspectMask = ::framework::gpu::vulkan::get_vk_image_aspect_flags(dependency_info->image_memory_barriers[image_memory_barrier].subresource_range.aspect_mask);
			vk_image_memory_barriers[image_memory_barrier].subresourceRange.baseMipLevel = dependency_info->image_memory_barriers[image_memory_barrier].subresource_range.base_mip_level;
			vk_image_memory_barriers[image_memory_barrier].subresourceRange.levelCount = dependency_info->image_memory_barriers[image_memory_barrier].subresource_range.level_count;
			vk_image_memory_barriers[image_memory_barrier].subresourceRange.baseArrayLayer = dependency_info->image_memory_barriers[image_memory_barrier].subresource_range.base_array_layer;
			vk_image_memory_barriers[image_memory_barrier].subresourceRange.layerCount = dependency_info->image_memory_barriers[image_memory_barrier].subresource_range.layer_count;
		}

		::VkDependencyInfoKHR vk_dependency_info;
		vk_dependency_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR;
		vk_dependency_info.pNext = nullptr;
		vk_dependency_info.dependencyFlags = ::framework::gpu::vulkan::get_vk_dependency_flags(dependency_info->dependency_flags);
		vk_dependency_info.memoryBarrierCount = dependency_info->memory_barrier_count;
		vk_dependency_info.pMemoryBarriers = vk_memory_barriers.get();
		vk_dependency_info.bufferMemoryBarrierCount = dependency_info->buffer_memory_barrier_count;
		vk_dependency_info.pBufferMemoryBarriers = vk_buffer_memory_barriers.get();
		vk_dependency_info.imageMemoryBarrierCount = dependency_info->image_memory_barrier_count;
		vk_dependency_info.pImageMemoryBarriers = vk_image_memory_barriers.get();
		this->device->get_dynamic_library().vkCmdPipelineBarrier2(this->vk_command_buffer, &vk_dependency_info);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::begin_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query, ::framework::gpu::query_control_flags flags)
{
	::vkCmdBeginQuery(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), query, ::framework::gpu::vulkan::get_vk_query_control_flags(flags));
}

void ::framework::gpu::vulkan::command_buffer::end_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query)
{
	::vkCmdEndQuery(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), query);
}

void ::framework::gpu::vulkan::command_buffer::reset_query_pool(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count)
{
	::vkCmdResetQueryPool(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), first_query, query_count);
}

void ::framework::gpu::vulkan::command_buffer::write_timestamp(::framework::gpu::pipeline_stage_flags pipeline_stage, ::framework::gpu::query_pool * query_pool, ::std::uint32_t query)
{
	this->device->get_dynamic_library().vkCmdWriteTimestamp2(this->vk_command_buffer, ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(pipeline_stage), static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), query);
}

void ::framework::gpu::vulkan::command_buffer::copy_query_pool_results(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags)
{
	::vkCmdCopyQueryPoolResults(this->vk_command_buffer, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), first_query, query_count, static_cast<::framework::gpu::vulkan::buffer const *>(dst_buffer)->get_vk_buffer(), dst_offset, stride, ::framework::gpu::vulkan::get_vk_query_result_flags(flags));
}

void ::framework::gpu::vulkan::command_buffer::build_acceleration_structures(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos)
{
	try
	{
		auto const & allocator = this->get_allocation_callbacks();

		::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildGeometryInfoKHR[]> vk_acceleration_structure_build_geometry_infos = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildGeometryInfoKHR[]>(allocator, ::framework::gpu::system_allocation_scope::command, info_count);
		::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR * []> vk_acceleration_structure_build_range_info_pointers = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR * []>(allocator, ::framework::gpu::system_allocation_scope::command, info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR[]>[]> vk_acceleration_structure_build_range_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR[]>[]>(allocator, ::framework::gpu::system_allocation_scope::command, info_count);

		for (::std::uint32_t i = 0; i < info_count; i++)
		{
			vk_acceleration_structure_build_geometry_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
			vk_acceleration_structure_build_geometry_infos[i].pNext = nullptr;
			vk_acceleration_structure_build_geometry_infos[i].type = ::framework::gpu::vulkan::get_vk_acceleration_structure_type(infos[i].type);
			vk_acceleration_structure_build_geometry_infos[i].flags = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_flags(infos[i].flags);
			vk_acceleration_structure_build_geometry_infos[i].mode = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_mode(infos[i].mode);
			vk_acceleration_structure_build_geometry_infos[i].srcAccelerationStructure = infos[i].src_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(infos[i].src_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
			vk_acceleration_structure_build_geometry_infos[i].dstAccelerationStructure = infos[i].dst_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(infos[i].dst_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
			vk_acceleration_structure_build_geometry_infos[i].geometryCount = infos[i].geometry_count;
			static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(infos[i].geometries)->init_geometries(infos[i].first_geometry, vk_acceleration_structure_build_geometry_infos[i].pGeometries, vk_acceleration_structure_build_geometry_infos[i].ppGeometries);
			vk_acceleration_structure_build_geometry_infos[i].scratchData = ::framework::gpu::vulkan::get_vk_device_or_host_address(infos[i].scratch_data);

			vk_acceleration_structure_build_range_infos[i] = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR[]>(allocator, ::framework::gpu::system_allocation_scope::command, infos[i].geometry_count);
			static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(infos[i].geometries)->init_build_range_infos(infos[i].first_geometry, infos[i].geometry_count, vk_acceleration_structure_build_range_infos[i].get());

			vk_acceleration_structure_build_range_info_pointers[i] = vk_acceleration_structure_build_range_infos[i].get();
		}

		this->device->get_dynamic_library().vkCmdBuildAccelerationStructuresKHR(this->vk_command_buffer, info_count, vk_acceleration_structure_build_geometry_infos.get(), vk_acceleration_structure_build_range_info_pointers.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

void ::framework::gpu::vulkan::command_buffer::build_acceleration_structures_indirect(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos, ::framework::gpu::device_address * indirect_device_addresses, ::std::uint32_t const * indirect_strides, ::std::uint32_t const * const * max_primitive_counts)
{
}

void ::framework::gpu::vulkan::command_buffer::copy_acceleration_structure(::framework::gpu::copy_acceleration_structure_info const * info)
{
}

void ::framework::gpu::vulkan::command_buffer::copy_acceleration_structure_to_memory(::framework::gpu::copy_acceleration_structure_to_memory_info const * info)
{
}

void ::framework::gpu::vulkan::command_buffer::copy_memory_to_acceleration_structure(::framework::gpu::copy_memory_to_acceleration_structure_info const * info)
{
}

void ::framework::gpu::vulkan::command_buffer::trace_rays(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::std::uint32_t width, ::std::uint32_t height, ::std::uint32_t depth)
{
	::VkStridedDeviceAddressRegionKHR vk_raygen_shader_binding_table = ::framework::gpu::vulkan::get_vk_strided_device_address_region(raygen_shader_binding_table);
	::VkStridedDeviceAddressRegionKHR vk_miss_shader_binding_table = ::framework::gpu::vulkan::get_vk_strided_device_address_region(miss_shader_binding_table);
	::VkStridedDeviceAddressRegionKHR vk_hit_shader_binding_table = ::framework::gpu::vulkan::get_vk_strided_device_address_region(hit_shader_binding_table);
	::VkStridedDeviceAddressRegionKHR vk_callable_shader_binding_table = ::framework::gpu::vulkan::get_vk_strided_device_address_region(callable_shader_binding_table);

	this->device->get_dynamic_library().vkCmdTraceRaysKHR(this->vk_command_buffer, &vk_raygen_shader_binding_table, &vk_miss_shader_binding_table, &vk_hit_shader_binding_table, &vk_callable_shader_binding_table, width, height, depth);
}

void ::framework::gpu::vulkan::command_buffer::trace_rays_indirect(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset)
{
}

void framework::gpu::vulkan::command_buffer::set_ray_tracing_pipeline_stack_size(::std::uint32_t pipeline_stack_size)
{
	this->device->get_dynamic_library().vkCmdSetRayTracingPipelineStackSizeKHR(this->vk_command_buffer, pipeline_stack_size);
}

::framework::gpu::allocation_callbacks const & ::framework::gpu::vulkan::command_buffer::get_allocation_callbacks()
{
	return this->command_pool->get_allocation_callbacks();
}
