#pragma once

#include "gpu/buffer.h"

namespace framework::gpu::vulkan
{
	class buffer final : public ::framework::gpu::buffer
	{
	public:
		buffer(::framework::gpu::vulkan::device * device, ::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkBuffer get_vk_buffer() const { return this->vk_buffer; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkBuffer vk_buffer;
	};
}