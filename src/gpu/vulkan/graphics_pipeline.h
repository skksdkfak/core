#pragma once

#include "pipeline.h"

namespace framework::gpu::vulkan
{
	class graphics_pipeline final : public ::framework::gpu::vulkan::pipeline
	{
	public:
		graphics_pipeline(::framework::gpu::vulkan::device * device, ::framework::gpu::graphics_pipeline_create_info const * create_info, ::VkPipeline vk_pipeline);

	private:
		friend class ::framework::gpu::vulkan::device;
	};
}