#include "gpu/vulkan/core.hpp"
#include <cstring>

::framework::gpu::vulkan::acceleration_structure_geometry_set::acceleration_structure_geometry_set(::framework::gpu::vulkan::device * device, ::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	layout(create_info->layout),
	geometry_count(create_info->geometry_count)
{
	::framework::gpu::common::unique_ptr<::VkAccelerationStructureGeometryKHR[]> geometries_array = ::framework::gpu::common::make_unique<::VkAccelerationStructureGeometryKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR[]> build_range_infos_array = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::VkAccelerationStructureGeometryKHR * []> geometries_array_of_pointers = ::framework::gpu::common::make_unique<::VkAccelerationStructureGeometryKHR * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR * []> build_range_infos_array_of_pointers = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);

	if (create_info->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		this->geometries_array = geometries_array.get();
		this->build_range_infos_array = build_range_infos_array.get();
	}
	else
	{
		this->geometries_array_of_pointers = geometries_array_of_pointers.get();
		this->build_range_infos_array_of_pointers = build_range_infos_array_of_pointers.get();
	}

	geometries_array.release();
	build_range_infos_array.release();
	geometries_array_of_pointers.release();
	build_range_infos_array_of_pointers.release();

	result = ::framework::gpu::result::success;
}

::framework::gpu::vulkan::acceleration_structure_geometry_set::~acceleration_structure_geometry_set()
{
	if (this->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		DELETE_ARRAY(this->geometries_array, this->geometry_count, this->allocator);
		DELETE_ARRAY(this->build_range_infos_array, this->geometry_count, this->allocator);
	}
	else
	{
		DELETE_ARRAY(this->geometries_array_of_pointers, this->geometry_count, this->allocator);
		DELETE_ARRAY(this->build_range_infos_array_of_pointers, this->geometry_count, this->allocator);
	}
}

void ::framework::gpu::vulkan::acceleration_structure_geometry_set::write_data(::framework::gpu::write_acceleration_structure_geometry const & write_info)
{
	if (write_info.layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		for (::std::uint32_t src_geometry = 0, dst_geometry = write_info.first_geometry; src_geometry < write_info.geometry_count; src_geometry++, dst_geometry++)
		{
			auto & geometry = this->geometries_array[dst_geometry];
			auto & build_range_info = this->build_range_infos_array[dst_geometry];

			geometry.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
			geometry.pNext = nullptr;
			geometry.geometryType = ::framework::gpu::vulkan::get_vk_geometry_type(write_info.geometry_type);
			switch (write_info.geometry_type)
			{
			case ::framework::gpu::geometry_type::triangles:
			{
				geometry.geometry.triangles.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
				geometry.geometry.triangles.pNext = nullptr;
				geometry.geometry.triangles.vertexFormat = ::framework::gpu::vulkan::get_vk_format(write_info.data.geometries[src_geometry].geometry.triangles.vertex_format);
				geometry.geometry.triangles.vertexData = ::framework::gpu::vulkan::get_vk_device_or_host_address_const(write_info.data.geometries[src_geometry].geometry.triangles.vertex_data);
				geometry.geometry.triangles.vertexStride = write_info.data.geometries[src_geometry].geometry.triangles.vertex_stride;
				geometry.geometry.triangles.indexType = ::framework::gpu::vulkan::get_vk_index_type(write_info.data.geometries[src_geometry].geometry.triangles.index_type);
				geometry.geometry.triangles.indexData = ::framework::gpu::vulkan::get_vk_device_or_host_address_const(write_info.data.geometries[src_geometry].geometry.triangles.index_data);
				geometry.geometry.triangles.transformData = ::framework::gpu::vulkan::get_vk_device_or_host_address_const(write_info.data.geometries[src_geometry].geometry.triangles.transform_data);

				build_range_info.primitiveCount = write_info.data.geometries[src_geometry].primitive_count;
				build_range_info.primitiveOffset = write_info.data.geometries[src_geometry].primitive_offset;
				build_range_info.firstVertex = write_info.data.geometries[src_geometry].geometry.triangles.first_vertex;
				build_range_info.transformOffset = write_info.data.geometries[src_geometry].geometry.triangles.transform_offset;
			}
			break;
			case ::framework::gpu::geometry_type::aabbs:
			{
				geometry.geometry.aabbs.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR;
				geometry.geometry.aabbs.pNext = nullptr;
				geometry.geometry.aabbs.data = ::framework::gpu::vulkan::get_vk_device_or_host_address_const(write_info.data.geometries[src_geometry].geometry.aabbs.data);
				geometry.geometry.aabbs.stride = write_info.data.geometries[src_geometry].geometry.aabbs.stride;

				build_range_info.primitiveCount = write_info.data.geometries[src_geometry].primitive_count;
				build_range_info.primitiveOffset = write_info.data.geometries[src_geometry].primitive_offset;
				build_range_info.firstVertex = 0;
				build_range_info.transformOffset = 0;
			}
			break;
			case ::framework::gpu::geometry_type::instances:
			{
				geometry.geometry.instances.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
				geometry.geometry.instances.pNext = nullptr;
				geometry.geometry.instances.arrayOfPointers = static_cast<::VkBool32>(write_info.data.geometries[src_geometry].geometry.instances.array_of_pointers);
				geometry.geometry.instances.data = ::framework::gpu::vulkan::get_vk_device_or_host_address_const(write_info.data.geometries[src_geometry].geometry.instances.data);

				build_range_info.primitiveCount = write_info.data.geometries[src_geometry].primitive_count;
				build_range_info.primitiveOffset = write_info.data.geometries[src_geometry].primitive_offset;
				build_range_info.firstVertex = 0;
				build_range_info.transformOffset = 0;
			}
			break;
			}
			geometry.flags = ::framework::gpu::vulkan::get_vk_geometry_flags(write_info.data.geometries[src_geometry].flags);
		}
	}
	else
	{
		for (::std::uint32_t geometry = 0, dst_geometry = write_info.first_geometry; geometry < write_info.geometry_count; geometry++, dst_geometry++)
		{
			::framework::gpu::vulkan::acceleration_structure_geometry_set * src_set = static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(write_info.data.geometry_references[geometry].src_set);
			::std::uint32_t src_geometry = write_info.data.geometry_references[geometry].src_array_element;

			this->geometries_array_of_pointers[dst_geometry] = &src_set->geometries_array[src_geometry];
			this->build_range_infos_array_of_pointers[dst_geometry] = &src_set->build_range_infos_array[src_geometry];
		}
	}
}

void ::framework::gpu::vulkan::acceleration_structure_geometry_set::init_geometries(::std::uint32_t first_geometry, ::VkAccelerationStructureGeometryKHR const *& geometries_array, ::VkAccelerationStructureGeometryKHR const * const *& geometries_array_of_pointers) const
{
	if (this->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		geometries_array = this->geometries_array + first_geometry;
		geometries_array_of_pointers = nullptr;
	}
	else
	{
		geometries_array = nullptr;
		geometries_array_of_pointers = this->geometries_array_of_pointers + first_geometry;
	}
}

void ::framework::gpu::vulkan::acceleration_structure_geometry_set::init_build_range_infos(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::VkAccelerationStructureBuildRangeInfoKHR * build_range_infos) const
{
	if (this->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		::std::memcpy(build_range_infos, this->build_range_infos_array + first_geometry, sizeof(::VkAccelerationStructureBuildRangeInfoKHR) * geometry_count);
	}
	else
	{
		for (::std::uint32_t geometry = 0, src_geometry = first_geometry; geometry < geometry_count; geometry++, src_geometry++)
		{
			build_range_infos[geometry] = *this->build_range_infos_array_of_pointers[src_geometry];
		}
	}
}

void framework::gpu::vulkan::acceleration_structure_geometry_set::init_max_primitive_counts(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::std::uint32_t * max_primitive_counts) const
{
	if (this->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		for (::std::uint32_t geometry = 0, src_geometry = first_geometry; geometry < geometry_count; geometry++, src_geometry++)
		{
			max_primitive_counts[geometry] = this->build_range_infos_array[src_geometry].primitiveCount;
		}
	}
	else
	{
		for (::std::uint32_t geometry = 0, src_geometry = first_geometry; geometry < geometry_count; geometry++, src_geometry++)
		{
			max_primitive_counts[geometry] = this->build_range_infos_array_of_pointers[src_geometry]->primitiveCount;
		}
	}
}
