#pragma once

#include "gpu/fence.h"

namespace framework::gpu::vulkan
{
	class fence final : public ::framework::gpu::fence
	{
	public:
		fence(::framework::gpu::vulkan::device * device, ::framework::gpu::fence_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::VkFence get_vk_fence() const { return this->vk_fence; }

	private:
		friend class ::framework::gpu::vulkan::device;

		::VkFence vk_fence;
	};
}