#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::queue::queue(::framework::gpu::vulkan::device * device, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index, float queue_priority) :
	device(device)
{
	::vkGetDeviceQueue(device->get_vk_device(), queue_family_index, queue_index, &this->vk_queue);
}

::framework::gpu::result(::framework::gpu::vulkan::queue::submit)(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkSubmitInfo2KHR[]> vk_submit_infos = ::framework::gpu::common::make_unique<::VkSubmitInfo2KHR[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submit_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSemaphoreSubmitInfoKHR[]>[]> vk_wait_semaphore_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSemaphoreSubmitInfoKHR[]>[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submit_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkCommandBufferSubmitInfoKHR[]>[]> vk_command_buffer_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkCommandBufferSubmitInfoKHR[]>[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submit_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSemaphoreSubmitInfoKHR[]>[]> vk_signal_semaphore_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSemaphoreSubmitInfoKHR[]>[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submit_count);

		for (::std::uint32_t i = 0; i < submit_count; i++)
		{
			vk_wait_semaphore_infos[i] = ::framework::gpu::common::make_unique<::VkSemaphoreSubmitInfoKHR[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submits[i].wait_semaphore_info_count);
			vk_command_buffer_infos[i] = ::framework::gpu::common::make_unique<::VkCommandBufferSubmitInfoKHR[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submits[i].command_buffer_info_count);
			vk_signal_semaphore_infos[i] = ::framework::gpu::common::make_unique<::VkSemaphoreSubmitInfoKHR[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, submits[i].signal_semaphore_info_count);

			for (::std::uint32_t j = 0; j < submits[i].wait_semaphore_info_count; j++)
			{
				vk_wait_semaphore_infos[i][j].sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR;
				vk_wait_semaphore_infos[i][j].pNext = nullptr;
				vk_wait_semaphore_infos[i][j].semaphore = static_cast<::framework::gpu::vulkan::semaphore const *>(submits[i].wait_semaphore_infos[j].semaphore)->get_vk_semaphore();
				vk_wait_semaphore_infos[i][j].value = submits[i].wait_semaphore_infos[j].value;
				vk_wait_semaphore_infos[i][j].stageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(submits[i].wait_semaphore_infos[j].stage_mask);
				vk_wait_semaphore_infos[i][j].deviceIndex = submits[i].wait_semaphore_infos[j].device_index;
			}

			for (::std::uint32_t j = 0; j < submits[i].command_buffer_info_count; j++)
			{
				vk_command_buffer_infos[i][j].sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR;
				vk_command_buffer_infos[i][j].pNext = nullptr;
				vk_command_buffer_infos[i][j].commandBuffer = static_cast<::framework::gpu::vulkan::command_buffer const *>(submits[i].command_buffer_infos[j].command_buffer)->get_vk_command_buffer();
				vk_command_buffer_infos[i][j].deviceMask = submits[i].command_buffer_infos[j].device_mask;
			}

			for (::std::uint32_t j = 0; j < submits[i].signal_semaphore_info_count; j++)
			{
				vk_signal_semaphore_infos[i][j].sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR;
				vk_signal_semaphore_infos[i][j].pNext = nullptr;
				vk_signal_semaphore_infos[i][j].semaphore = static_cast<::framework::gpu::vulkan::semaphore const *>(submits[i].signal_semaphore_infos[j].semaphore)->get_vk_semaphore();
				vk_signal_semaphore_infos[i][j].value = submits[i].signal_semaphore_infos[j].value;
				vk_signal_semaphore_infos[i][j].stageMask = ::framework::gpu::vulkan::get_vk_pipeline_stage_flags2(submits[i].signal_semaphore_infos[j].stage_mask);
				vk_signal_semaphore_infos[i][j].deviceIndex = submits[i].signal_semaphore_infos[j].device_index;
			}

			vk_submit_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR;
			vk_submit_infos[i].pNext = nullptr;
			vk_submit_infos[i].flags = ::framework::gpu::vulkan::get_vk_submit_flags(submits[i].flags);
			vk_submit_infos[i].waitSemaphoreInfoCount = submits[i].wait_semaphore_info_count;
			vk_submit_infos[i].pWaitSemaphoreInfos = vk_wait_semaphore_infos[i].get();
			vk_submit_infos[i].commandBufferInfoCount = submits[i].command_buffer_info_count;
			vk_submit_infos[i].pCommandBufferInfos = vk_command_buffer_infos[i].get();
			vk_submit_infos[i].signalSemaphoreInfoCount = submits[i].signal_semaphore_info_count;
			vk_submit_infos[i].pSignalSemaphoreInfos = vk_signal_semaphore_infos[i].get();
		}

		::VkResult vk_result = this->device->get_dynamic_library().vkQueueSubmit2(this->vk_queue, submit_count, vk_submit_infos.get(), fence ? static_cast<::framework::gpu::vulkan::fence const *>(fence)->get_vk_fence() : VK_NULL_HANDLE);

		result = ::framework::gpu::vulkan::throw_result(vk_result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::queue::present)(::framework::gpu::present_info const * present_info)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkSwapchainKHR[]> vk_swapchains = ::framework::gpu::common::make_unique<::VkSwapchainKHR[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, present_info->swap_chain_count);
		::framework::gpu::common::unique_ptr<::VkSemaphore[]> vk_wait_semaphores;
		::framework::gpu::common::unique_ptr<::VkResult[]> vk_results = present_info->results ? ::framework::gpu::common::make_unique<::VkResult[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, present_info->swap_chain_count) : nullptr;

		for (::std::uint32_t i = 0; i < present_info->swap_chain_count; i++)
		{
			vk_swapchains[i] = static_cast<::framework::gpu::vulkan::swap_chain const *>(present_info->swap_chains[i])->get_vk_swapchain();
		}

		if (present_info->wait_semaphore_count)
		{
			vk_wait_semaphores = ::framework::gpu::common::make_unique<::VkSemaphore[]>(this->device->get_allocation_callbacks(), ::framework::gpu::system_allocation_scope::command, present_info->wait_semaphore_count);
			for (::std::uint32_t i = 0; i < present_info->wait_semaphore_count; i++)
			{
				vk_wait_semaphores[i] = static_cast<::framework::gpu::vulkan::semaphore const *>(present_info->wait_semaphores[i])->get_vk_semaphore();
			}
		}

		::VkPresentInfoKHR vk_present_info;
		vk_present_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		vk_present_info.pNext = nullptr;
		vk_present_info.swapchainCount = present_info->swap_chain_count;
		vk_present_info.pSwapchains = vk_swapchains.get();
		vk_present_info.pImageIndices = present_info->image_indices;
		vk_present_info.pWaitSemaphores = vk_wait_semaphores.get();
		vk_present_info.waitSemaphoreCount = present_info->wait_semaphore_count;
		vk_present_info.pResults = vk_results.get();

		::VkResult vk_result = ::vkQueuePresentKHR(this->vk_queue, &vk_present_info);

		if (present_info->results)
		{
			for (::std::uint32_t i = 0; i < present_info->swap_chain_count; i++)
			{
				present_info->results[i] = ::framework::gpu::vulkan::get_result(vk_results[i]);
			}
		}

		result = ::framework::gpu::vulkan::throw_result(vk_result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::queue::wait_idle)()
{
	return ::framework::gpu::vulkan::get_result(::vkQueueWaitIdle(this->vk_queue));
}

::framework::gpu::result(::framework::gpu::vulkan::queue::get_timestamp_period)(float * timestamp_period)
{
	*timestamp_period = 1.0f;

	return ::framework::gpu::result::success;
}