#include "gpu/vulkan/core.hpp"
#include <cstring>

::framework::gpu::vulkan::device::device(::framework::gpu::vulkan::physical_device * physical_device, ::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	physical_device(physical_device),
	allocator(allocator),
	vk_device(VK_NULL_HANDLE)
{
	::VkDeviceCreateFlags vk_device_create_flags = 0;

	::std::vector<char const *> vk_device_layers
	{
		"VK_LAYER_NV_optimus",
		"VK_LAYER_LUNARG_api_dump",
		"VK_LAYER_LUNARG_screenshot",
		"VK_LAYER_RENDERDOC_Capture",
		"VK_LAYER_VALVE_steam_overlay",
		"VK_LAYER_LUNARG_standard_validation",
	};

	::std::vector<char const *> vk_device_extensions
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
		VK_KHR_PIPELINE_LIBRARY_EXTENSION_NAME,
		VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME,
		VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
		VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME,
		VK_KHR_RAY_QUERY_EXTENSION_NAME,
		VK_KHR_SYNCHRONIZATION_2_EXTENSION_NAME,
		VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME,
		VK_KHR_SHADER_FLOAT16_INT8_EXTENSION_NAME,
		VK_EXT_SHADER_ATOMIC_FLOAT_EXTENSION_NAME
	};

	for (::std::uint32_t i = 0; i < create_info->enabled_extension_count; i++)
	{
		if (::std::strcmp(::framework::gpu::cooperative_matrix_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			vk_device_extensions.push_back(VK_KHR_COOPERATIVE_MATRIX_EXTENSION_NAME);
		}
	}

	::framework::gpu::common::unique_ptr<::VkDeviceQueueCreateInfo[]> vk_device_queue_create_infos;
	if (create_info->queue_create_info_count)
	{
		vk_device_queue_create_infos = ::framework::gpu::common::make_unique<::VkDeviceQueueCreateInfo[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->queue_create_info_count);

		for (::std::uint32_t i = 0; i < create_info->queue_create_info_count; i++)
		{
			::VkDeviceQueueCreateFlags vk_device_queue_create_flags = 0;
			if (!!(create_info->queue_create_infos[i].flags & ::framework::gpu::device_queue_create_flags::protected_bit))
				vk_device_queue_create_flags |= ::VkDeviceQueueCreateFlagBits::VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT;

			vk_device_queue_create_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			vk_device_queue_create_infos[i].pNext = nullptr;
			vk_device_queue_create_infos[i].flags = vk_device_queue_create_flags;
			vk_device_queue_create_infos[i].queueFamilyIndex = create_info->queue_create_infos[i].queue_family_index;
			vk_device_queue_create_infos[i].queueCount = create_info->queue_create_infos[i].queue_count;
			vk_device_queue_create_infos[i].pQueuePriorities = create_info->queue_create_infos[i].queue_priorities;
		}
	}

	::VkPhysicalDeviceRayQueryFeaturesKHR vk_physical_device_ray_query_features;
	::VkPhysicalDeviceRayTracingPipelineFeaturesKHR vk_physical_device_ray_tracing_pipeline_features;
	::VkPhysicalDeviceAccelerationStructureFeaturesKHR vk_physical_device_acceleration_structure_features;
	::VkPhysicalDeviceCooperativeMatrixFeaturesKHR vk_physical_device_cooperative_matrix_features;
	::VkPhysicalDeviceShaderAtomicFloatFeaturesEXT vk_physical_device_shader_atomic_float_features;
	::VkPhysicalDeviceVulkan11Features vk_physical_device_vulkan_1_1_features;
	::VkPhysicalDeviceVulkan12Features vk_physical_device_vulkan_1_2_features;
	::VkPhysicalDeviceVulkan13Features vk_physical_device_vulkan_1_3_features;
	::VkPhysicalDeviceSynchronization2FeaturesKHR vk_physical_device_synchronization2_features;
	::VkPhysicalDeviceTimelineSemaphoreFeatures vk_physical_device_timeline_semaphore_features;
	::VkPhysicalDeviceFeatures2 vk_physical_device_features2;
	void * next = nullptr;

	vk_physical_device_synchronization2_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR;
	vk_physical_device_synchronization2_features.pNext = next;
	vk_physical_device_synchronization2_features.synchronization2 = VK_TRUE;
	next = &vk_physical_device_synchronization2_features;

	vk_physical_device_vulkan_1_1_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
	vk_physical_device_vulkan_1_1_features.pNext = next;
	vk_physical_device_vulkan_1_1_features.storageBuffer16BitAccess = VK_TRUE;
	vk_physical_device_vulkan_1_1_features.uniformAndStorageBuffer16BitAccess = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.storagePushConstant16 = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.storageInputOutput16 = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.multiview = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.multiviewGeometryShader = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.multiviewTessellationShader = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.variablePointersStorageBuffer = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.variablePointers = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.protectedMemory = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.samplerYcbcrConversion = VK_FALSE;
	vk_physical_device_vulkan_1_1_features.shaderDrawParameters = VK_FALSE;
	next = &vk_physical_device_vulkan_1_1_features;

	vk_physical_device_vulkan_1_2_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
	vk_physical_device_vulkan_1_2_features.pNext = nullptr;
	
	vk_physical_device_vulkan_1_3_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
	vk_physical_device_vulkan_1_3_features.pNext = &vk_physical_device_vulkan_1_2_features;

	vk_physical_device_features2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
	vk_physical_device_features2.pNext = &vk_physical_device_vulkan_1_3_features;
	::vkGetPhysicalDeviceFeatures2(static_cast<::framework::gpu::vulkan::physical_device *>(physical_device)->get_vk_physical_device(), &vk_physical_device_features2);

	vk_physical_device_vulkan_1_2_features.pNext = next;
	next = &vk_physical_device_vulkan_1_2_features;
	
	vk_physical_device_vulkan_1_3_features.pNext = next;
	next = &vk_physical_device_vulkan_1_3_features;

	if (create_info->enabled_features)
	{
		vk_physical_device_vulkan_1_2_features.bufferDeviceAddress = create_info->enabled_features->buffer_device_address;
		vk_physical_device_vulkan_1_2_features.bufferDeviceAddressCaptureReplay = create_info->enabled_features->buffer_device_address_capture_replay;
		vk_physical_device_vulkan_1_2_features.bufferDeviceAddressMultiDevice = create_info->enabled_features->buffer_device_address_multi_device;
		
		vk_physical_device_vulkan_1_3_features.subgroupSizeControl = create_info->enabled_features->subgroup_size_control;

		vk_physical_device_ray_query_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
		vk_physical_device_ray_query_features.pNext = next;
		vk_physical_device_ray_query_features.rayQuery = create_info->enabled_features->ray_query;

		vk_physical_device_ray_tracing_pipeline_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
		vk_physical_device_ray_tracing_pipeline_features.pNext = &vk_physical_device_ray_query_features;
		vk_physical_device_ray_tracing_pipeline_features.rayTracingPipeline = create_info->enabled_features->ray_tracing_pipeline;
		vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineShaderGroupHandleCaptureReplay = create_info->enabled_features->ray_tracing_pipeline_shader_group_handle_capture_replay;
		vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineShaderGroupHandleCaptureReplayMixed = create_info->enabled_features->ray_tracing_pipeline_shader_group_handle_capture_replay_mixed;
		vk_physical_device_ray_tracing_pipeline_features.rayTracingPipelineTraceRaysIndirect = create_info->enabled_features->ray_tracing_pipeline_trace_rays_indirect;
		vk_physical_device_ray_tracing_pipeline_features.rayTraversalPrimitiveCulling = create_info->enabled_features->ray_traversal_primitive_culling;

		vk_physical_device_acceleration_structure_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
		vk_physical_device_acceleration_structure_features.pNext = &vk_physical_device_ray_tracing_pipeline_features;
		vk_physical_device_acceleration_structure_features.accelerationStructure = create_info->enabled_features->acceleration_structure;
		vk_physical_device_acceleration_structure_features.accelerationStructureCaptureReplay = create_info->enabled_features->acceleration_structure_capture_replay;
		vk_physical_device_acceleration_structure_features.accelerationStructureIndirectBuild = create_info->enabled_features->acceleration_structure_indirect_build;
		vk_physical_device_acceleration_structure_features.accelerationStructureHostCommands = create_info->enabled_features->acceleration_structure_host_commands;
		vk_physical_device_acceleration_structure_features.descriptorBindingAccelerationStructureUpdateAfterBind = create_info->enabled_features->descriptor_binding_acceleration_structure_update_after_bind;

		vk_physical_device_cooperative_matrix_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_KHR;
		vk_physical_device_cooperative_matrix_features.pNext = &vk_physical_device_acceleration_structure_features;
		vk_physical_device_cooperative_matrix_features.cooperativeMatrix = create_info->enabled_features->cooperative_matrix;
		vk_physical_device_cooperative_matrix_features.cooperativeMatrixRobustBufferAccess = create_info->enabled_features->cooperative_matrix_robust_buffer_access;

		vk_physical_device_shader_atomic_float_features.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
		vk_physical_device_shader_atomic_float_features.pNext = &vk_physical_device_cooperative_matrix_features;
		vk_physical_device_shader_atomic_float_features.shaderBufferFloat32Atomics = create_info->enabled_features->shader_buffer_float32_atomics;
		vk_physical_device_shader_atomic_float_features.shaderBufferFloat32AtomicAdd = create_info->enabled_features->shader_buffer_float32_atomic_add;
		vk_physical_device_shader_atomic_float_features.shaderBufferFloat64Atomics = create_info->enabled_features->shader_buffer_float64_atomics;
		vk_physical_device_shader_atomic_float_features.shaderBufferFloat64AtomicAdd = create_info->enabled_features->shader_buffer_float32_atomic_add;
		vk_physical_device_shader_atomic_float_features.shaderSharedFloat32Atomics = create_info->enabled_features->shader_shared_float32_atomics;
		vk_physical_device_shader_atomic_float_features.shaderSharedFloat32AtomicAdd = create_info->enabled_features->shader_shared_float32_atomic_add;
		vk_physical_device_shader_atomic_float_features.shaderSharedFloat64Atomics = create_info->enabled_features->shader_shared_float64_atomics;
		vk_physical_device_shader_atomic_float_features.shaderSharedFloat64AtomicAdd = create_info->enabled_features->shader_shared_float64_atomic_add;
		vk_physical_device_shader_atomic_float_features.shaderImageFloat32Atomics = create_info->enabled_features->shader_image_float32_atomics;
		vk_physical_device_shader_atomic_float_features.shaderImageFloat32AtomicAdd = create_info->enabled_features->shader_image_float32_atomic_add;
		vk_physical_device_shader_atomic_float_features.sparseImageFloat32Atomics = create_info->enabled_features->sparse_image_float32_atomics;
		vk_physical_device_shader_atomic_float_features.sparseImageFloat32AtomicAdd = create_info->enabled_features->sparse_image_float32_atomic_add;

		vk_physical_device_features2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
		vk_physical_device_features2.pNext = &vk_physical_device_shader_atomic_float_features;
		//vk_physical_device_features2.features.robustBufferAccess = VK_TRUE;
		//vk_physical_device_features2.features.fullDrawIndexUint32 = VK_TRUE;
		//vk_physical_device_features2.features.imageCubeArray = VK_TRUE;
		//vk_physical_device_features2.features.independentBlend = VK_TRUE;
		vk_physical_device_features2.features.geometryShader = create_info->enabled_features->geometry_shader;
		//vk_physical_device_features2.features.tessellationShader = VK_TRUE;
		//vk_physical_device_features2.features.sampleRateShading = VK_TRUE;
		//vk_physical_device_features2.features.dualSrcBlend = VK_TRUE;
		//vk_physical_device_features2.features.logicOp = VK_TRUE;
		//vk_physical_device_features2.features.multiDrawIndirect = VK_TRUE;
		//vk_physical_device_features2.features.drawIndirectFirstInstance = VK_TRUE;
		//vk_physical_device_features2.features.depthClamp = VK_TRUE;
		//vk_physical_device_features2.features.depthBiasClamp = VK_TRUE;
		//vk_physical_device_features2.features.fillModeNonSolid = VK_TRUE;
		//vk_physical_device_features2.features.depthBounds = VK_TRUE;
		//vk_physical_device_features2.features.wideLines = VK_TRUE;
		//vk_physical_device_features2.features.largePoints = VK_TRUE;
		//vk_physical_device_features2.features.alphaToOne = VK_TRUE;
		//vk_physical_device_features2.features.multiViewport = VK_TRUE;
		//vk_physical_device_features2.features.samplerAnisotropy = VK_TRUE;
		vk_physical_device_features2.features.textureCompressionETC2 = create_info->enabled_features->texture_compression_etc_2;
		vk_physical_device_features2.features.textureCompressionASTC_LDR = create_info->enabled_features->texture_compression_astc_ldr;
		//vk_physical_device_features2.features.textureCompressionBC = VK_TRUE;
		//vk_physical_device_features2.features.occlusionQueryPrecise = VK_TRUE;
		//vk_physical_device_features2.features.pipelineStatisticsQuery = VK_TRUE;
		//vk_physical_device_features2.features.vertexPipelineStoresAndAtomics = VK_TRUE;
		//vk_physical_device_features2.features.fragmentStoresAndAtomics = VK_TRUE;
		//vk_physical_device_features2.features.shaderTessellationAndGeometryPointSize = VK_TRUE;
		//vk_physical_device_features2.features.shaderImageGatherExtended = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageImageExtendedFormats = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageImageMultisample = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageImageReadWithoutFormat = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageImageWriteWithoutFormat = VK_TRUE;
		//vk_physical_device_features2.features.shaderUniformBufferArrayDynamicIndexing = VK_TRUE;
		//vk_physical_device_features2.features.shaderSampledImageArrayDynamicIndexing = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageBufferArrayDynamicIndexing = VK_TRUE;
		//vk_physical_device_features2.features.shaderStorageImageArrayDynamicIndexing = VK_TRUE;
		//vk_physical_device_features2.features.shaderClipDistance = VK_TRUE;
		//vk_physical_device_features2.features.shaderCullDistance = VK_TRUE;
		//vk_physical_device_features2.features.shaderFloat64 = VK_TRUE;
		//vk_physical_device_features2.features.shaderInt64 = VK_TRUE;
		//vk_physical_device_features2.features.shaderInt16 = VK_TRUE;
		//vk_physical_device_features2.features.shaderResourceResidency = VK_TRUE;
		//vk_physical_device_features2.features.shaderResourceMinLod = VK_TRUE;
		//vk_physical_device_features2.features.sparseBinding = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidencyBuffer = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidencyImage2D = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidencyImage3D = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidency2Samples = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidency4Samples = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidency8Samples = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidency16Samples = VK_TRUE;
		//vk_physical_device_features2.features.sparseResidencyAliased = VK_TRUE;
		//vk_physical_device_features2.features.variableMultisampleRate = VK_TRUE;
		//vk_physical_device_features2.features.inheritedQueries = VK_TRUE;

		::vkGetPhysicalDeviceFeatures2(static_cast<::framework::gpu::vulkan::physical_device *>(physical_device)->get_vk_physical_device(), &vk_physical_device_features2);

		next = &vk_physical_device_features2;
	}

	::VkDeviceCreateInfo vk_device_create_info;
	vk_device_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	vk_device_create_info.pNext = next;
	vk_device_create_info.flags = vk_device_create_flags;
	vk_device_create_info.queueCreateInfoCount = create_info->queue_create_info_count;
	vk_device_create_info.pQueueCreateInfos = vk_device_queue_create_infos.get();
	vk_device_create_info.enabledLayerCount = static_cast<::std::uint32_t>(vk_device_layers.size());
	vk_device_create_info.ppEnabledLayerNames = vk_device_layers.data();
	vk_device_create_info.enabledExtensionCount = static_cast<::std::uint32_t>(vk_device_extensions.size());
	vk_device_create_info.ppEnabledExtensionNames = vk_device_extensions.data();
	vk_device_create_info.pEnabledFeatures = nullptr;

	::VkResult vk_result;

	vk_result = ::vkCreateDevice(static_cast<::framework::gpu::vulkan::physical_device *>(physical_device)->get_vk_physical_device(), &vk_device_create_info, nullptr, &this->vk_device);

	::vk::UniqueDevice vk_unique_device(::vk::Device(this->vk_device), ::vk::UniqueDevice::ObjectDestroy(nullptr));

	result = ::framework::gpu::vulkan::throw_result(vk_result);

	::std::uint32_t queue_family_count = 0;

	for (::std::uint32_t i = 0; i < create_info->queue_create_info_count; i++)
	{
		if (create_info->queue_create_infos[i].queue_family_index + 1 > queue_family_count)
		{
			queue_family_count = create_info->queue_create_infos[i].queue_family_index + 1;
		}
	}

	if (queue_family_count)
	{
		this->queues = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::queue>[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, queue_family_count);

		for (::std::uint32_t i = 0; i < create_info->queue_create_info_count; i++)
		{
			auto const & queue_create_info = create_info->queue_create_infos[i];

			if (this->queues[queue_create_info.queue_family_index])
			{
				continue;
			}

			this->queues[queue_create_info.queue_family_index] = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::framework::gpu::vulkan::queue>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, queue_create_info.queue_count);

			for (::std::uint32_t j = 0; j < queue_create_info.queue_count; j++)
			{
				this->queues[queue_create_info.queue_family_index][j] = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::queue>(this->allocator, ::framework::gpu::system_allocation_scope::object, this, queue_create_info.queue_family_index, j, queue_create_info.queue_priorities[j]);
			}
		}
	}

	this->dynamic_library.vkCmdPipelineBarrier2 = reinterpret_cast<PFN_vkCmdPipelineBarrier2>(::vkGetDeviceProcAddr(this->vk_device, "vkCmdPipelineBarrier2"));
	this->dynamic_library.vkQueueSubmit2 = reinterpret_cast<PFN_vkQueueSubmit2>(::vkGetDeviceProcAddr(this->vk_device, "vkQueueSubmit2"));
	this->dynamic_library.vkCreateAccelerationStructureKHR = reinterpret_cast<PFN_vkCreateAccelerationStructureKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkCreateAccelerationStructureKHR"));
	this->dynamic_library.vkDestroyAccelerationStructureKHR = reinterpret_cast<PFN_vkDestroyAccelerationStructureKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkDestroyAccelerationStructureKHR"));
	this->dynamic_library.vkGetAccelerationStructureBuildSizesKHR = reinterpret_cast<PFN_vkGetAccelerationStructureBuildSizesKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkGetAccelerationStructureBuildSizesKHR"));
	this->dynamic_library.vkCmdBuildAccelerationStructuresKHR = reinterpret_cast<PFN_vkCmdBuildAccelerationStructuresKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkCmdBuildAccelerationStructuresKHR"));
	this->dynamic_library.vkBuildAccelerationStructuresKHR = reinterpret_cast<PFN_vkBuildAccelerationStructuresKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkBuildAccelerationStructuresKHR"));
	this->dynamic_library.vkGetAccelerationStructureDeviceAddressKHR = reinterpret_cast<PFN_vkGetAccelerationStructureDeviceAddressKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkGetAccelerationStructureDeviceAddressKHR"));
	this->dynamic_library.vkCmdTraceRaysKHR = reinterpret_cast<PFN_vkCmdTraceRaysKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkCmdTraceRaysKHR"));
	this->dynamic_library.vkCmdSetRayTracingPipelineStackSizeKHR = reinterpret_cast<PFN_vkCmdSetRayTracingPipelineStackSizeKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkCmdSetRayTracingPipelineStackSizeKHR"));
	this->dynamic_library.vkGetRayTracingShaderGroupHandlesKHR = reinterpret_cast<PFN_vkGetRayTracingShaderGroupHandlesKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkGetRayTracingShaderGroupHandlesKHR"));
	this->dynamic_library.vkCreateRayTracingPipelinesKHR = reinterpret_cast<PFN_vkCreateRayTracingPipelinesKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkCreateRayTracingPipelinesKHR"));
	this->dynamic_library.vkGetRayTracingCaptureReplayShaderGroupHandlesKHR = reinterpret_cast<PFN_vkGetRayTracingCaptureReplayShaderGroupHandlesKHR>(::vkGetDeviceProcAddr(this->vk_device, "vkGetRayTracingCaptureReplayShaderGroupHandlesKHR"));
	this->dynamic_library.vkCmdWriteTimestamp2 = reinterpret_cast<PFN_vkCmdWriteTimestamp2>(::vkGetDeviceProcAddr(this->vk_device, "vkCmdWriteTimestamp2"));
	this->dynamic_library.vkSetDebugUtilsObjectNameEXT = reinterpret_cast<PFN_vkSetDebugUtilsObjectNameEXT>(::vkGetDeviceProcAddr(this->vk_device, "vkSetDebugUtilsObjectNameEXT"));

	vk_unique_device.release();
}

::framework::gpu::vulkan::device::~device()
{
	::vkDestroyDevice(this->vk_device, nullptr);
}

void ::framework::gpu::vulkan::device::get_queue(::std::uint32_t queue_family_index, ::std::uint32_t queue_index, ::framework::gpu::queue ** queue)
{
	*queue = this->queues[queue_family_index][queue_index].get();
}

::framework::gpu::result(::framework::gpu::vulkan::device::wait_idle)()
{
	return ::framework::gpu::vulkan::get_result(::vkDeviceWaitIdle(this->vk_device));
}

::framework::gpu::result(::framework::gpu::vulkan::device::allocate_memory)(::framework::gpu::memory_allocate_info const * allocate_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device_memory ** device_memory)
{
	::framework::gpu::result result;

	try
	{
		::VkMemoryAllocateFlagsInfo vk_memory_allocate_flags_info;
		vk_memory_allocate_flags_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
		vk_memory_allocate_flags_info.pNext = nullptr;
		vk_memory_allocate_flags_info.flags = ::framework::gpu::vulkan::get_vk_memory_allocate_flags(allocate_info->flags);
		vk_memory_allocate_flags_info.deviceMask = allocate_info->device_mask;

		::VkMemoryDedicatedAllocateInfo vk_memory_dedicated_allocate_info;
		vk_memory_dedicated_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO;
		vk_memory_dedicated_allocate_info.pNext = &vk_memory_allocate_flags_info;
		vk_memory_dedicated_allocate_info.image = allocate_info->memory_dedicated_allocate_info.image ? static_cast<::framework::gpu::vulkan::image const *>(allocate_info->memory_dedicated_allocate_info.image)->get_vk_image() : VK_NULL_HANDLE;
		vk_memory_dedicated_allocate_info.buffer = allocate_info->memory_dedicated_allocate_info.buffer ? static_cast<::framework::gpu::vulkan::buffer const *>(allocate_info->memory_dedicated_allocate_info.buffer)->get_vk_buffer() : VK_NULL_HANDLE;

		::VkMemoryOpaqueCaptureAddressAllocateInfo vk_memory_opaque_capture_address_allocate_info;
		vk_memory_opaque_capture_address_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO;
		vk_memory_opaque_capture_address_allocate_info.pNext = &vk_memory_dedicated_allocate_info;
		vk_memory_opaque_capture_address_allocate_info.opaqueCaptureAddress = allocate_info->opaque_capture_address;

		::VkMemoryAllocateInfo vk_memory_allocate_info;
		vk_memory_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		vk_memory_allocate_info.pNext = &vk_memory_opaque_capture_address_allocate_info;
		vk_memory_allocate_info.allocationSize = allocate_info->allocation_size;
		vk_memory_allocate_info.memoryTypeIndex = allocate_info->memory_type_index;

		::VkDeviceMemory vk_device_memory;
		::VkResult vk_result;

		vk_result = ::vkAllocateMemory(this->vk_device, &vk_memory_allocate_info, nullptr, &vk_device_memory);

		::vk::UniqueDeviceMemory vk_unique_device_memory(::vk::DeviceMemory(vk_device_memory), ::vk::UniqueDeviceMemory::ObjectFree(this->vk_device, nullptr));

		result = ::framework::gpu::vulkan::get_result(vk_result);

		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

		*device_memory = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::device_memory>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, *allocate_info, vk_device_memory).release();

		vk_unique_device_memory.release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::free_memory(::framework::gpu::device_memory * device_memory, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkFreeMemory(this->vk_device, static_cast<::framework::gpu::vulkan::device_memory *>(device_memory)->get_vk_device_memory(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::device_memory *>(device_memory), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::map_memory)(::framework::gpu::device_memory * memory, ::framework::gpu::device_size offset, ::framework::gpu::device_size size, ::framework::gpu::memory_map_flags flags, void ** data)
{
	::VkResult vk_result;

	::VkMemoryMapFlags vkMemoryMapFlags = 0;

	vk_result = ::vkMapMemory(this->vk_device, static_cast<::framework::gpu::vulkan::device_memory *>(memory)->get_vk_device_memory(), offset, size, vkMemoryMapFlags, data);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

void ::framework::gpu::vulkan::device::unmap_memory(::framework::gpu::device_memory * memory)
{
	::vkUnmapMemory(this->vk_device, static_cast<::framework::gpu::vulkan::device_memory *>(memory)->get_vk_device_memory());
}

::framework::gpu::result(::framework::gpu::vulkan::device::flush_mapped_memory_ranges)(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkMappedMemoryRange[]> vk_mappedmemory_ranges = ::framework::gpu::common::make_unique<::VkMappedMemoryRange[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, memory_range_count);
		for (::std::uint32_t i = 0; i < memory_range_count; i++)
		{
			vk_mappedmemory_ranges[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
			vk_mappedmemory_ranges[i].pNext = nullptr;
			vk_mappedmemory_ranges[i].memory = static_cast<::framework::gpu::vulkan::device_memory *>(memory_ranges[i].memory)->get_vk_device_memory();
			vk_mappedmemory_ranges[i].offset = memory_ranges[i].offset;
			vk_mappedmemory_ranges[i].size = memory_ranges[i].size;
		}

		::VkResult vk_result = ::vkFlushMappedMemoryRanges(this->vk_device, memory_range_count, vk_mappedmemory_ranges.get());

		result = ::framework::gpu::vulkan::throw_result(vk_result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::invalidate_mapped_memory_ranges)(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkMappedMemoryRange[]> vk_mappedmemory_ranges = ::framework::gpu::common::make_unique<::VkMappedMemoryRange[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, memory_range_count);
		for (::std::uint32_t i = 0; i < memory_range_count; i++)
		{
			vk_mappedmemory_ranges[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
			vk_mappedmemory_ranges[i].pNext = nullptr;
			vk_mappedmemory_ranges[i].memory = static_cast<::framework::gpu::vulkan::device_memory *>(memory_ranges[i].memory)->get_vk_device_memory();
			vk_mappedmemory_ranges[i].offset = memory_ranges[i].offset;
			vk_mappedmemory_ranges[i].size = memory_ranges[i].size;
		}

		::VkResult vk_result = ::vkInvalidateMappedMemoryRanges(this->vk_device, memory_range_count, vk_mappedmemory_ranges.get());

		result = ::framework::gpu::vulkan::throw_result(vk_result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::bind_buffer_memory)(::std::uint32_t bind_info_count, ::framework::gpu::bind_buffer_memory_info const * bind_infos)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkBindBufferMemoryInfo[]> vk_bind_buffer_memory_infos = ::framework::gpu::common::make_unique<::VkBindBufferMemoryInfo[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, bind_info_count);

		for (::std::uint32_t i = 0; i < bind_info_count; i++)
		{
			vk_bind_buffer_memory_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO;
			vk_bind_buffer_memory_infos[i].pNext = nullptr;
			vk_bind_buffer_memory_infos[i].buffer = static_cast<::framework::gpu::vulkan::buffer *>(bind_infos[i].buffer)->get_vk_buffer();
			vk_bind_buffer_memory_infos[i].memory = static_cast<::framework::gpu::vulkan::device_memory *>(bind_infos[i].memory)->get_vk_device_memory();
			vk_bind_buffer_memory_infos[i].memoryOffset = static_cast<VkDeviceSize>(bind_infos[i].memory_offset);
		}

		result = ::framework::gpu::vulkan::throw_result(::vkBindBufferMemory2(this->vk_device, bind_info_count, vk_bind_buffer_memory_infos.get()));
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::bind_image_memory)(::std::uint32_t bind_info_count, ::framework::gpu::bind_image_memory_info const * bind_infos)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkBindImageMemoryInfo[]> vk_bind_image_memory_infos = ::framework::gpu::common::make_unique<::VkBindImageMemoryInfo[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, bind_info_count);

		for (::std::uint32_t i = 0; i < bind_info_count; i++)
		{
			vk_bind_image_memory_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
			vk_bind_image_memory_infos[i].pNext = nullptr;
			vk_bind_image_memory_infos[i].image = static_cast<::framework::gpu::vulkan::image *>(bind_infos[i].image)->get_vk_image();
			vk_bind_image_memory_infos[i].memory = static_cast<::framework::gpu::vulkan::device_memory *>(bind_infos[i].memory)->get_vk_device_memory();
			vk_bind_image_memory_infos[i].memoryOffset = static_cast<VkDeviceSize>(bind_infos[i].memory_offset);
		}

		result = ::framework::gpu::vulkan::throw_result(::vkBindImageMemory2(this->vk_device, bind_info_count, vk_bind_image_memory_infos.get()));
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::get_buffer_memory_requirements(::framework::gpu::buffer_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements)
{
	::VkMemoryDedicatedRequirements vk_memory_dedicated_requirements;
	vk_memory_dedicated_requirements.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS;
	vk_memory_dedicated_requirements.pNext = nullptr;
	vk_memory_dedicated_requirements.prefersDedicatedAllocation;
	vk_memory_dedicated_requirements.requiresDedicatedAllocation;

	::VkMemoryRequirements2 vk_memory_requirements_2;
	vk_memory_requirements_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
	vk_memory_requirements_2.pNext = &vk_memory_dedicated_requirements;
	vk_memory_requirements_2.memoryRequirements;

	::VkBufferMemoryRequirementsInfo2 vk_buffer_memory_requirements_info_2;
	vk_buffer_memory_requirements_info_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2;
	vk_buffer_memory_requirements_info_2.pNext = nullptr;
	vk_buffer_memory_requirements_info_2.buffer = static_cast<::framework::gpu::vulkan::buffer const *>(info->buffer)->get_vk_buffer();

	::vkGetBufferMemoryRequirements2(this->vk_device, &vk_buffer_memory_requirements_info_2, &vk_memory_requirements_2);

	memory_requirements->size = vk_memory_requirements_2.memoryRequirements.size;
	memory_requirements->alignment = vk_memory_requirements_2.memoryRequirements.alignment;
	memory_requirements->memory_type_bits = vk_memory_requirements_2.memoryRequirements.memoryTypeBits;
	memory_requirements->prefers_dedicated_allocation = vk_memory_dedicated_requirements.prefersDedicatedAllocation;
	memory_requirements->requires_dedicated_allocation = vk_memory_dedicated_requirements.requiresDedicatedAllocation;
}

void ::framework::gpu::vulkan::device::get_image_memory_requirements(::framework::gpu::image_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements)
{
	::VkMemoryDedicatedRequirements vk_memory_dedicated_requirements;
	vk_memory_dedicated_requirements.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS;
	vk_memory_dedicated_requirements.pNext = nullptr;
	vk_memory_dedicated_requirements.prefersDedicatedAllocation;
	vk_memory_dedicated_requirements.requiresDedicatedAllocation;

	::VkMemoryRequirements2 vk_memory_requirements_2;
	vk_memory_requirements_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
	vk_memory_requirements_2.pNext = &vk_memory_dedicated_requirements;
	vk_memory_requirements_2.memoryRequirements;

	::VkImageMemoryRequirementsInfo2 vk_image_memory_requirements_info_2;
	vk_image_memory_requirements_info_2.sType = ::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
	vk_image_memory_requirements_info_2.pNext = nullptr;
	vk_image_memory_requirements_info_2.image = static_cast<::framework::gpu::vulkan::image const *>(info->image)->get_vk_image();

	::vkGetImageMemoryRequirements2(this->vk_device, &vk_image_memory_requirements_info_2, &vk_memory_requirements_2);

	memory_requirements->size = vk_memory_requirements_2.memoryRequirements.size;
	memory_requirements->alignment = vk_memory_requirements_2.memoryRequirements.alignment;
	memory_requirements->memory_type_bits = vk_memory_requirements_2.memoryRequirements.memoryTypeBits;
	memory_requirements->prefers_dedicated_allocation = vk_memory_dedicated_requirements.prefersDedicatedAllocation;
	memory_requirements->requires_dedicated_allocation = vk_memory_dedicated_requirements.requiresDedicatedAllocation;
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_fence)(::framework::gpu::fence_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::fence ** fence)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*fence = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::fence>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_fence(::framework::gpu::fence * fence, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyFence(this->vk_device, static_cast<::framework::gpu::vulkan::fence *>(fence)->vk_fence, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::fence *>(fence), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::reset_fences)(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkFence[]> vk_fences = ::framework::gpu::common::make_unique<::VkFence[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, fence_count);

		for (::std::uint32_t i = 0; i < fence_count; i++)
		{
			vk_fences[i] = static_cast<::framework::gpu::vulkan::fence const *>(fences[i])->get_vk_fence();
		}

		result = ::framework::gpu::vulkan::throw_result(::vkResetFences(this->vk_device, fence_count, vk_fences.get()));
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::wait_for_fences)(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences, bool wait_all, ::std::uint64_t timeout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkFence[]> vk_fences = ::framework::gpu::common::make_unique<::VkFence[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, fence_count);

		for (::std::uint32_t i = 0; i < fence_count; i++)
		{
			vk_fences[i] = static_cast<::framework::gpu::vulkan::fence const *>(fences[i])->get_vk_fence();
		}

		result = ::framework::gpu::vulkan::throw_result(::vkWaitForFences(this->vk_device, fence_count, vk_fences.get(), static_cast<::VkBool32>(wait_all), timeout));
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_semaphore)(::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::semaphore ** semaphore)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*semaphore = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::semaphore>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_semaphore(::framework::gpu::semaphore * semaphore, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroySemaphore(this->vk_device, static_cast<::framework::gpu::vulkan::semaphore *>(semaphore)->vk_semaphore, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::semaphore *>(semaphore), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::get_semaphore_counter_value)(::framework::gpu::semaphore * semaphore, ::std::uint64_t * value)
{
	::VkResult vk_result = ::vkGetSemaphoreCounterValue(this->vk_device, static_cast<::framework::gpu::vulkan::semaphore *>(semaphore)->get_vk_semaphore(), value);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::device::wait_semaphores)(::framework::gpu::semaphore_wait_info const * wait_info, ::std::uint64_t timeout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkSemaphore[]> vk_semaphores = ::framework::gpu::common::make_unique<::VkSemaphore[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, wait_info->semaphore_count);

		for (::std::uint32_t i = 0; i < wait_info->semaphore_count; i++)
		{
			vk_semaphores[i] = static_cast<::framework::gpu::vulkan::semaphore *>(wait_info->semaphores[i])->get_vk_semaphore();
		}

		::VkSemaphoreWaitInfo vk_semaphore_wait_info;
		vk_semaphore_wait_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
		vk_semaphore_wait_info.pNext = nullptr;
		vk_semaphore_wait_info.flags = ::framework::gpu::vulkan::get_vk_semaphore_wait_flags(wait_info->flags);
		vk_semaphore_wait_info.semaphoreCount = wait_info->semaphore_count;
		vk_semaphore_wait_info.pSemaphores = vk_semaphores.get();
		vk_semaphore_wait_info.pValues = wait_info->values;

		::VkResult vk_result = ::vkWaitSemaphores(this->vk_device, &vk_semaphore_wait_info, timeout);

		result = ::framework::gpu::vulkan::throw_result(vk_result);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::signal_semaphore)(::framework::gpu::semaphore_signal_info const * signal_info)
{
	::VkSemaphoreSignalInfo vk_semaphore_signal_info;
	vk_semaphore_signal_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
	vk_semaphore_signal_info.pNext = nullptr;
	vk_semaphore_signal_info.semaphore = static_cast<::framework::gpu::vulkan::semaphore *>(signal_info->semaphore)->get_vk_semaphore();
	vk_semaphore_signal_info.value = signal_info->value;

	::VkResult vk_result = ::vkSignalSemaphore(this->vk_device, &vk_semaphore_signal_info);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_query_pool)(::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::query_pool ** query_pool)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*query_pool = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::query_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_query_pool(::framework::gpu::query_pool * query_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyQueryPool(this->vk_device, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::query_pool *>(query_pool), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::get_query_pool_results)(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::std::size_t data_size, void * data, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags)
{
	::VkResult vk_result = ::vkGetQueryPoolResults(this->vk_device, static_cast<::framework::gpu::vulkan::query_pool *>(query_pool)->get_vk_query_pool(), first_query, query_count, data_size, data, stride, ::framework::gpu::vulkan::get_vk_query_result_flags(flags));

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_buffer)(::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer ** buffer)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*buffer = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::buffer>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_buffer(::framework::gpu::buffer * buffer, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyBuffer(this->vk_device, static_cast<::framework::gpu::vulkan::buffer const *>(buffer)->vk_buffer, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::buffer *>(buffer), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_buffer_view)(::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer_view ** buffer_view)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*buffer_view = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::buffer_view>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_buffer_view(::framework::gpu::buffer_view * buffer_view, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyBufferView(this->vk_device, static_cast<::framework::gpu::vulkan::buffer_view *>(buffer_view)->get_vk_buffer_view(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::buffer_view *>(buffer_view), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_image)(::framework::gpu::image_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image ** image)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::image>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_image)(::framework::gpu::image_create_info const * create_info, ::VkImage vk_image, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::vulkan::image ** image)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::image>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, vk_image, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_image(::framework::gpu::image * image, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyImage(this->vk_device, static_cast<::framework::gpu::vulkan::image *>(image)->get_vk_image(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::image *>(image), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_image_view)(::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image_view ** image_view)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image_view = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::image_view>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_image_view(::framework::gpu::image_view * image_view, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyImageView(this->vk_device, static_cast<::framework::gpu::vulkan::image_view *>(image_view)->get_vk_image_view(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::image_view *>(image_view), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_shader_module)(::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::shader_module ** shader_module)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*shader_module = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::shader_module>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_shader_module(::framework::gpu::shader_module * shader_module, ::framework::gpu::allocation_callbacks const * allocator)
{
	if (!shader_module)
	{
		return;
	}

	::vkDestroyShaderModule(this->vk_device, static_cast<::framework::gpu::vulkan::shader_module const *>(shader_module)->vk_shader_module, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::shader_module *>(shader_module), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_graphics_pipelines)(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::graphics_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result;
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
	::std::uint32_t pipeline_index = 0;

	try
	{
		::framework::gpu::common::unique_ptr<::VkPipeline[]> vk_pipelines = ::framework::gpu::common::make_unique<::VkPipeline[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::VkGraphicsPipelineCreateInfo[]> vk_graphics_pipeline_create_infos = ::framework::gpu::common::make_unique<::VkGraphicsPipelineCreateInfo[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineShaderStageCreateInfo[]>[]> vk_pipeline_shader_stage_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineShaderStageCreateInfo[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineVertexInputStateCreateInfo>[]> vk_pipeline_vertex_input_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineVertexInputStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkVertexInputBindingDescription[]>[]> vk_vertex_input_binding_descriptions = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkVertexInputBindingDescription[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkVertexInputAttributeDescription[]>[]> vk_vertex_input_attribute_descriptions = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkVertexInputAttributeDescription[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineInputAssemblyStateCreateInfo>[]> vk_pipeline_input_assembly_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineInputAssemblyStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineTessellationStateCreateInfo>[]> vk_pipeline_tessellation_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineTessellationStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineViewportStateCreateInfo>[]> vk_pipeline_viewport_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineViewportStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkViewport[]>[]> vk_viewports = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkViewport[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkRect2D[]>[]> vk_scissors = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkRect2D[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineRasterizationStateCreateInfo>[]> vk_pipeline_rasterization_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineRasterizationStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineDepthStencilStateCreateInfo>[]> vk_pipeline_depth_stencil_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineDepthStencilStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineDynamicStateCreateInfo>[]> vk_pipeline_dynamic_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineDynamicStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineMultisampleStateCreateInfo>[]> vk_pipeline_multisample_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineMultisampleStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkDynamicState[]>[]> vk_dynamic_states = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkDynamicState[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineColorBlendStateCreateInfo>[]> vk_pipeline_color_blend_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineColorBlendStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSampleMask>[]> vk_sample_masks = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSampleMask>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineColorBlendAttachmentState[]>[]> vk_pipeline_color_blend_attachment_states = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineColorBlendAttachmentState[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]> vk_specialization_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]> vk_specialization_map_entries = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);

		for (::std::uint32_t create_info_index = 0; create_info_index < create_info_count; create_info_index++)
		{
			auto & create_info = create_infos[create_info_index];

			if (create_info.stage_count)
			{
				vk_pipeline_shader_stage_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineShaderStageCreateInfo[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.stage_count);
				for (::std::uint32_t i = 0; i < create_info.stage_count; i++)
				{
					if (create_info.stages[i].specialization_info)
					{
						vk_specialization_map_entries[create_info_index] = ::framework::gpu::common::make_unique<::VkSpecializationMapEntry[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.stages[i].specialization_info->map_entry_count);
						for (::std::uint32_t map_entry_index = 0; map_entry_index < create_info.stages[i].specialization_info->map_entry_count; map_entry_index++)
						{
							vk_specialization_map_entries[create_info_index][map_entry_index].constantID = create_info.stages[i].specialization_info->map_entries[map_entry_index].constant_id;
							vk_specialization_map_entries[create_info_index][map_entry_index].offset = create_info.stages[i].specialization_info->map_entries[map_entry_index].offset;
							vk_specialization_map_entries[create_info_index][map_entry_index].size = create_info.stages[i].specialization_info->map_entries[map_entry_index].size;
						}

						vk_specialization_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkSpecializationInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
						vk_specialization_infos[create_info_index]->mapEntryCount = create_info.stages[i].specialization_info->map_entry_count;
						vk_specialization_infos[create_info_index]->pMapEntries = vk_specialization_map_entries[create_info_index].get();
						vk_specialization_infos[create_info_index]->dataSize = create_info.stages[i].specialization_info->data_size;
						vk_specialization_infos[create_info_index]->pData = create_info.stages[i].specialization_info->data;
					}

					vk_pipeline_shader_stage_create_infos[create_info_index][i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pNext = nullptr;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].flags = ::framework::gpu::vulkan::get_vk_pipeline_shader_stage_create_flags(create_info.stages[i].flags);
					vk_pipeline_shader_stage_create_infos[create_info_index][i].stage = ::framework::gpu::vulkan::get_vk_shader_stage_flag_bits(create_info.stages[i].stage);
					vk_pipeline_shader_stage_create_infos[create_info_index][i].module = static_cast<::framework::gpu::vulkan::shader_module const *>(create_info.stages[i].module)->get_vk_shader_module();
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pName = create_info.stages[i].name;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pSpecializationInfo = vk_specialization_infos[create_info_index].get();
				}
			}

			if (create_info.vertex_input_state)
			{
				if (create_info.vertex_input_state->vertex_binding_description_count)
				{
					vk_vertex_input_binding_descriptions[create_info_index] = ::framework::gpu::common::make_unique<::VkVertexInputBindingDescription[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.vertex_input_state->vertex_binding_description_count);
					for (::std::uint32_t i = 0; i < create_info.vertex_input_state->vertex_binding_description_count; i++)
					{
						vk_vertex_input_binding_descriptions[create_info_index][i].binding = create_info.vertex_input_state->vertex_binding_descriptions[i].binding;
						vk_vertex_input_binding_descriptions[create_info_index][i].stride = create_info.vertex_input_state->vertex_binding_descriptions[i].stride;
						vk_vertex_input_binding_descriptions[create_info_index][i].inputRate = ::framework::gpu::vulkan::get_vk_vertex_input_rate(create_info.vertex_input_state->vertex_binding_descriptions[i].input_rate);
					}
				}

				if (create_info.vertex_input_state->vertex_attribute_description_count)
				{
					vk_vertex_input_attribute_descriptions[create_info_index] = ::framework::gpu::common::make_unique<::VkVertexInputAttributeDescription[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.vertex_input_state->vertex_attribute_description_count);
					for (::std::uint32_t i = 0; i < create_info.vertex_input_state->vertex_attribute_description_count; i++)
					{
						vk_vertex_input_attribute_descriptions[create_info_index][i].location = create_info.vertex_input_state->vertex_attribute_descriptions[i].location;
						vk_vertex_input_attribute_descriptions[create_info_index][i].binding = create_info.vertex_input_state->vertex_attribute_descriptions[i].binding;
						vk_vertex_input_attribute_descriptions[create_info_index][i].format = ::framework::gpu::vulkan::get_vk_format(create_info.vertex_input_state->vertex_attribute_descriptions[i].format);
						vk_vertex_input_attribute_descriptions[create_info_index][i].offset = create_info.vertex_input_state->vertex_attribute_descriptions[i].offset;
					}
				}

				vk_pipeline_vertex_input_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineVertexInputStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->vertexBindingDescriptionCount = create_info.vertex_input_state->vertex_binding_description_count;
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->pVertexBindingDescriptions = vk_vertex_input_binding_descriptions[create_info_index].get();
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->vertexAttributeDescriptionCount = create_info.vertex_input_state->vertex_attribute_description_count;
				vk_pipeline_vertex_input_state_create_infos[create_info_index]->pVertexAttributeDescriptions = vk_vertex_input_attribute_descriptions[create_info_index].get();
			}

			if (create_info.input_assembly_state)
			{
				vk_pipeline_input_assembly_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineInputAssemblyStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_input_assembly_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
				vk_pipeline_input_assembly_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_input_assembly_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_input_assembly_state_create_infos[create_info_index]->topology = ::framework::gpu::vulkan::get_vk_primitive_topology(create_info.input_assembly_state->topology);
				vk_pipeline_input_assembly_state_create_infos[create_info_index]->primitiveRestartEnable = static_cast<::VkBool32>(create_info.input_assembly_state->primitive_restart_enable);
			}

			if (create_info.tessellation_state)
			{
				vk_pipeline_tessellation_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineTessellationStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_tessellation_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
				vk_pipeline_tessellation_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_tessellation_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_tessellation_state_create_infos[create_info_index]->patchControlPoints = create_info.tessellation_state->patch_control_points;
			}

			if (create_info.viewport_state)
			{
				if (create_info.viewport_state->viewport_count)
				{
					vk_viewports[create_info_index] = ::framework::gpu::common::make_unique<::VkViewport[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.viewport_state->viewport_count);
					for (::std::uint32_t i = 0; i < create_info.viewport_state->viewport_count; i++)
					{
						vk_viewports[create_info_index][i].width = create_info.viewport_state->viewports[i].width;
						vk_viewports[create_info_index][i].height = create_info.viewport_state->viewports[i].height;
						vk_viewports[create_info_index][i].x = create_info.viewport_state->viewports[i].x;
						vk_viewports[create_info_index][i].y = create_info.viewport_state->viewports[i].y;
						vk_viewports[create_info_index][i].minDepth = create_info.viewport_state->viewports[i].min_depth;
						vk_viewports[create_info_index][i].maxDepth = create_info.viewport_state->viewports[i].max_depth;
					}
				}

				if (create_info.viewport_state->scissor_count)
				{
					vk_scissors[create_info_index] = ::framework::gpu::common::make_unique<::VkRect2D[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.viewport_state->scissor_count);
					for (::std::uint32_t i = 0; i < create_info.viewport_state->scissor_count; i++)
					{
						vk_scissors[create_info_index][i].extent.width = create_info.viewport_state->scissors[i].extent.width;
						vk_scissors[create_info_index][i].extent.height = create_info.viewport_state->scissors[i].extent.height;
						vk_scissors[create_info_index][i].offset.x = create_info.viewport_state->scissors[i].offset.x;
						vk_scissors[create_info_index][i].offset.y = create_info.viewport_state->scissors[i].offset.y;
					}
				}

				vk_pipeline_viewport_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineViewportStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_viewport_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
				vk_pipeline_viewport_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_viewport_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_viewport_state_create_infos[create_info_index]->viewportCount = create_info.viewport_state->viewport_count;
				vk_pipeline_viewport_state_create_infos[create_info_index]->pViewports = vk_viewports[create_info_index].get();
				vk_pipeline_viewport_state_create_infos[create_info_index]->scissorCount = create_info.viewport_state->scissor_count;
				vk_pipeline_viewport_state_create_infos[create_info_index]->pScissors = vk_scissors[create_info_index].get();
			}

			if (create_info.rasterization_state)
			{
				vk_pipeline_rasterization_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineRasterizationStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->depthClampEnable = static_cast<::VkBool32>(create_info.rasterization_state->depth_clamp_enable);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->rasterizerDiscardEnable = static_cast<::VkBool32>(create_info.rasterization_state->rasterizer_discard_enable);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->polygonMode = ::framework::gpu::vulkan::get_vk_polygon_mode(create_info.rasterization_state->polygon_mode);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->cullMode = ::framework::gpu::vulkan::get_vk_cull_mode_flags(create_info.rasterization_state->cull_mode);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->frontFace = ::framework::gpu::vulkan::get_vk_front_face(create_info.rasterization_state->front_face);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->depthBiasEnable = static_cast<::VkBool32>(create_info.rasterization_state->rasterizer_discard_enable);
				vk_pipeline_rasterization_state_create_infos[create_info_index]->depthBiasConstantFactor = create_info.rasterization_state->depth_bias_constant_factor;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->depthBiasClamp = create_info.rasterization_state->depth_bias_clamp;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->depthBiasSlopeFactor = create_info.rasterization_state->depth_bias_slope_factor;
				vk_pipeline_rasterization_state_create_infos[create_info_index]->lineWidth = create_info.rasterization_state->line_width;
			}

			if (create_info.multisample_state)
			{
				if (create_info.multisample_state->sample_mask)
				{
					vk_sample_masks[create_info_index] = ::framework::gpu::common::make_unique<::VkSampleMask>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
					*vk_sample_masks[create_info_index] = *create_info.multisample_state->sample_mask;
				}

				vk_pipeline_multisample_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineMultisampleStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_multisample_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
				vk_pipeline_multisample_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_multisample_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_multisample_state_create_infos[create_info_index]->rasterizationSamples = ::framework::gpu::vulkan::get_vk_sample_count_flag_bits(create_info.multisample_state->rasterization_samples);
				vk_pipeline_multisample_state_create_infos[create_info_index]->sampleShadingEnable = static_cast<::VkBool32>(create_info.multisample_state->sample_shading_enable);
				vk_pipeline_multisample_state_create_infos[create_info_index]->minSampleShading = create_info.multisample_state->min_sample_shading;
				vk_pipeline_multisample_state_create_infos[create_info_index]->pSampleMask = vk_sample_masks[create_info_index].get();
				vk_pipeline_multisample_state_create_infos[create_info_index]->alphaToCoverageEnable = static_cast<::VkBool32>(create_info.multisample_state->alpha_to_coverage_enable);
				vk_pipeline_multisample_state_create_infos[create_info_index]->alphaToOneEnable = static_cast<::VkBool32>(create_info.multisample_state->alpha_to_one_enable);
			}

			if (create_info.depth_stencil_state)
			{
				vk_pipeline_depth_stencil_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineDepthStencilStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->depthTestEnable = static_cast<::VkBool32>(create_info.depth_stencil_state->depth_test_enable);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->depthWriteEnable = static_cast<::VkBool32>(create_info.depth_stencil_state->depth_write_enable);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->depthCompareOp = ::framework::gpu::vulkan::get_vk_compare_op(create_info.depth_stencil_state->depth_compare_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->depthBoundsTestEnable = static_cast<::VkBool32>(create_info.depth_stencil_state->depth_bounds_test_enable);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->stencilTestEnable = static_cast<::VkBool32>(create_info.depth_stencil_state->stencil_test_enable);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.failOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->front.fail_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.passOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->front.pass_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.depthFailOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->front.depth_fail_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.compareOp = ::framework::gpu::vulkan::get_vk_compare_op(create_info.depth_stencil_state->front.compare_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.compareMask = create_info.depth_stencil_state->front.compare_mask;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.writeMask = create_info.depth_stencil_state->front.write_mask;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->front.reference = create_info.depth_stencil_state->front.reference;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.failOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->back.fail_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.passOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->back.pass_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.depthFailOp = ::framework::gpu::vulkan::get_vk_stencil_op(create_info.depth_stencil_state->back.depth_fail_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.compareOp = ::framework::gpu::vulkan::get_vk_compare_op(create_info.depth_stencil_state->back.compare_op);
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.compareMask = create_info.depth_stencil_state->back.compare_mask;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.writeMask = create_info.depth_stencil_state->back.write_mask;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->back.reference = create_info.depth_stencil_state->back.reference;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->minDepthBounds = create_info.depth_stencil_state->min_depth_bounds;
				vk_pipeline_depth_stencil_state_create_infos[create_info_index]->maxDepthBounds = create_info.depth_stencil_state->max_depth_bounds;
			}

			if (create_info.color_blend_state)
			{
				if (create_info.color_blend_state->attachment_count)
				{
					vk_pipeline_color_blend_attachment_states[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineColorBlendAttachmentState[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.color_blend_state->attachment_count);
					for (::std::uint32_t i = 0; i < create_info.color_blend_state->attachment_count; i++)
					{
						vk_pipeline_color_blend_attachment_states[create_info_index][i].blendEnable = static_cast<::VkBool32>(create_info.color_blend_state->attachments->blend_enable);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].srcColorBlendFactor = ::framework::gpu::vulkan::get_vk_blend_factor(create_info.color_blend_state->attachments->src_color_blend_factor);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].dstColorBlendFactor = ::framework::gpu::vulkan::get_vk_blend_factor(create_info.color_blend_state->attachments->dst_color_blend_factor);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].colorBlendOp = ::framework::gpu::vulkan::get_vk_blend_op(create_info.color_blend_state->attachments->color_blend_op);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].srcAlphaBlendFactor = ::framework::gpu::vulkan::get_vk_blend_factor(create_info.color_blend_state->attachments->src_alpha_blend_factor);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].dstAlphaBlendFactor = ::framework::gpu::vulkan::get_vk_blend_factor(create_info.color_blend_state->attachments->dst_alpha_blend_factor);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].alphaBlendOp = ::framework::gpu::vulkan::get_vk_blend_op(create_info.color_blend_state->attachments->alpha_blend_op);
						vk_pipeline_color_blend_attachment_states[create_info_index][i].colorWriteMask = ::framework::gpu::vulkan::get_vk_color_write_mask(create_info.color_blend_state->attachments->color_write_mask);
					}
				}

				vk_pipeline_color_blend_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineColorBlendStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_color_blend_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
				vk_pipeline_color_blend_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_color_blend_state_create_infos[create_info_index]->flags = 0;
				vk_pipeline_color_blend_state_create_infos[create_info_index]->logicOpEnable = static_cast<::VkBool32>(create_info.color_blend_state->logic_op_enable);
				vk_pipeline_color_blend_state_create_infos[create_info_index]->logicOp = ::framework::gpu::vulkan::get_vk_logic_op(create_info.color_blend_state->logic_op);
				vk_pipeline_color_blend_state_create_infos[create_info_index]->attachmentCount = create_info.color_blend_state->attachment_count;
				vk_pipeline_color_blend_state_create_infos[create_info_index]->pAttachments = vk_pipeline_color_blend_attachment_states[create_info_index].get();
				::std::memcpy(vk_pipeline_color_blend_state_create_infos[create_info_index]->blendConstants, create_info.color_blend_state->blend_constants, sizeof(vk_pipeline_color_blend_state_create_infos[create_info_index]->blendConstants));
			}

			if (create_info.dynamic_state)
			{
				if (create_info.dynamic_state->dynamic_state_count)
				{
					vk_dynamic_states[create_info_index] = ::framework::gpu::common::make_unique<::VkDynamicState[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.dynamic_state->dynamic_state_count);
					for (::std::uint32_t i = 0; i < create_info.dynamic_state->dynamic_state_count; i++)
					{
						vk_dynamic_states[create_info_index][i] = ::framework::gpu::vulkan::get_vk_dynamic_state(create_info.dynamic_state->dynamic_states[i]);
					}
				}

				vk_pipeline_dynamic_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineDynamicStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_dynamic_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->flags = ::framework::gpu::vulkan::get_vk_pipeline_dynamic_state_create_flags(create_info.dynamic_state->flags);
				vk_pipeline_dynamic_state_create_infos[create_info_index]->dynamicStateCount = create_info.dynamic_state->dynamic_state_count;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->pDynamicStates = vk_dynamic_states[create_info_index].get();
			}

			vk_graphics_pipeline_create_infos[create_info_index].sType = ::VkStructureType::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			vk_graphics_pipeline_create_infos[create_info_index].pNext = nullptr;
			vk_graphics_pipeline_create_infos[create_info_index].flags = ::framework::gpu::vulkan::get_vk_pipeline_create_flags(create_info.flags);
			vk_graphics_pipeline_create_infos[create_info_index].stageCount = create_info.stage_count;
			vk_graphics_pipeline_create_infos[create_info_index].pStages = vk_pipeline_shader_stage_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pVertexInputState = vk_pipeline_vertex_input_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pInputAssemblyState = vk_pipeline_input_assembly_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pTessellationState = vk_pipeline_tessellation_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pViewportState = vk_pipeline_viewport_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pRasterizationState = vk_pipeline_rasterization_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pMultisampleState = vk_pipeline_multisample_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pDepthStencilState = vk_pipeline_depth_stencil_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pColorBlendState = vk_pipeline_color_blend_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].pDynamicState = vk_pipeline_dynamic_state_create_infos[create_info_index].get();
			vk_graphics_pipeline_create_infos[create_info_index].layout = static_cast<::framework::gpu::vulkan::pipeline_layout const *>(create_info.layout)->get_vk_pipeline_layout();
			vk_graphics_pipeline_create_infos[create_info_index].renderPass = static_cast<::framework::gpu::vulkan::render_pass const *>(create_info.render_pass)->get_vk_render_pass();
			vk_graphics_pipeline_create_infos[create_info_index].subpass = create_info.subpass;
			vk_graphics_pipeline_create_infos[create_info_index].basePipelineHandle = create_info.base_pipeline ? static_cast<::framework::gpu::vulkan::pipeline *>(create_info.base_pipeline)->get_vk_pipeline() : VK_NULL_HANDLE;
			vk_graphics_pipeline_create_infos[create_info_index].basePipelineIndex = create_info.base_pipeline_index;
		}

		result = ::framework::gpu::vulkan::throw_result(::vkCreateGraphicsPipelines(this->vk_device, pipeline_cache ? static_cast<::framework::gpu::vulkan::pipeline_cache *>(pipeline_cache)->get_vk_pipeline_cache() : VK_NULL_HANDLE, create_info_count, vk_graphics_pipeline_create_infos.get(), nullptr, vk_pipelines.get()));

		for (; pipeline_index < create_info_count; pipeline_index++)
		{
			pipelines[pipeline_index] = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::graphics_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[pipeline_index], vk_pipelines[pipeline_index]).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = 0; i < pipeline_index; i++)
		{
			this->destroy_pipeline(pipelines[i], &selected_allocator);
		}

		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_compute_pipelines)(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::compute_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result;
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
	::std::uint32_t pipeline_index = 0;

	try
	{
		::framework::gpu::common::unique_ptr<::VkPipeline[]> vk_pipelines = ::framework::gpu::common::make_unique<::VkPipeline[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::VkComputePipelineCreateInfo[]> vk_compute_pipeline_create_infos = ::framework::gpu::common::make_unique<::VkComputePipelineCreateInfo[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::VkPipelineShaderStageRequiredSubgroupSizeCreateInfo[]> vk_pipeline_shader_stage_required_subgroup_size_create_info = ::framework::gpu::common::make_unique<::VkPipelineShaderStageRequiredSubgroupSizeCreateInfo[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]> vk_specialization_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]> vk_specialization_map_entries = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);

		for (::std::uint32_t i = 0; i < create_info_count; i++)
		{
			if (create_infos[i].stage.specialization_info)
			{
				vk_specialization_infos[i] = ::framework::gpu::common::make_unique<::VkSpecializationInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_specialization_map_entries[i] = ::framework::gpu::common::make_unique<::VkSpecializationMapEntry[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_infos[i].stage.specialization_info->map_entry_count);

				for (::std::uint32_t map_entry_index = 0; map_entry_index < create_infos[i].stage.specialization_info->map_entry_count; map_entry_index++)
				{
					vk_specialization_map_entries[i][map_entry_index].constantID = create_infos[i].stage.specialization_info->map_entries[map_entry_index].constant_id;
					vk_specialization_map_entries[i][map_entry_index].offset = create_infos[i].stage.specialization_info->map_entries[map_entry_index].offset;
					vk_specialization_map_entries[i][map_entry_index].size = create_infos[i].stage.specialization_info->map_entries[map_entry_index].size;
				}

				vk_specialization_infos[i]->mapEntryCount = create_infos[i].stage.specialization_info->map_entry_count;
				vk_specialization_infos[i]->pMapEntries = vk_specialization_map_entries[i].get();
				vk_specialization_infos[i]->dataSize = create_infos[i].stage.specialization_info->data_size;
				vk_specialization_infos[i]->pData = create_infos[i].stage.specialization_info->data;
			}

			vk_pipeline_shader_stage_required_subgroup_size_create_info[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO;
			vk_pipeline_shader_stage_required_subgroup_size_create_info[i].pNext = nullptr;
			vk_pipeline_shader_stage_required_subgroup_size_create_info[i].requiredSubgroupSize = 32;

			vk_compute_pipeline_create_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
			vk_compute_pipeline_create_infos[i].pNext = nullptr;
			vk_compute_pipeline_create_infos[i].flags = ::framework::gpu::vulkan::get_vk_pipeline_create_flags(create_infos[i].flags);
			vk_compute_pipeline_create_infos[i].stage.sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			vk_compute_pipeline_create_infos[i].stage.pNext = /*&vk_pipeline_shader_stage_required_subgroup_size_create_info[i]*/nullptr;
			vk_compute_pipeline_create_infos[i].stage.flags = ::framework::gpu::vulkan::get_vk_pipeline_shader_stage_create_flags(create_infos[i].stage.flags);
			vk_compute_pipeline_create_infos[i].stage.stage = ::framework::gpu::vulkan::get_vk_shader_stage_flag_bits(create_infos[i].stage.stage);
			vk_compute_pipeline_create_infos[i].stage.module = static_cast<::framework::gpu::vulkan::shader_module const *>(create_infos[i].stage.module)->get_vk_shader_module();
			vk_compute_pipeline_create_infos[i].stage.pName = create_infos[i].stage.name;
			vk_compute_pipeline_create_infos[i].stage.pSpecializationInfo = vk_specialization_infos[i].get();
			vk_compute_pipeline_create_infos[i].layout = static_cast<::framework::gpu::vulkan::pipeline_layout const *>(create_infos[i].layout)->get_vk_pipeline_layout();
			vk_compute_pipeline_create_infos[i].basePipelineHandle = create_infos[i].base_pipeline ? static_cast<::framework::gpu::vulkan::pipeline *>(create_infos[i].base_pipeline)->get_vk_pipeline() : VK_NULL_HANDLE;
			vk_compute_pipeline_create_infos[i].basePipelineIndex = create_infos[i].base_pipeline_index;
		}

		result = ::framework::gpu::vulkan::throw_result(::vkCreateComputePipelines(this->vk_device, pipeline_cache ? static_cast<::framework::gpu::vulkan::pipeline_cache *>(pipeline_cache)->get_vk_pipeline_cache() : VK_NULL_HANDLE, create_info_count, vk_compute_pipeline_create_infos.get(), nullptr, vk_pipelines.get()));

		for (; pipeline_index < create_info_count; pipeline_index++)
		{
			pipelines[pipeline_index] = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::compute_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[pipeline_index], vk_pipelines[pipeline_index]).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = 0; i < pipeline_index; i++)
		{
			this->destroy_pipeline(pipelines[i], &selected_allocator);
		}

		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_pipeline(::framework::gpu::pipeline * pipeline, ::framework::gpu::allocation_callbacks const * allocator)
{
	if (!pipeline)
	{
		return;
	}

	::vkDestroyPipeline(this->vk_device, static_cast<::framework::gpu::vulkan::pipeline *>(pipeline)->vk_pipeline, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::pipeline *>(pipeline), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_pipeline_cache)(::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_cache ** pipeline_cache)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*pipeline_cache = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::pipeline_cache>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_pipeline_cache(::framework::gpu::pipeline_cache * pipeline_cache, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyPipelineCache(this->vk_device, static_cast<::framework::gpu::vulkan::pipeline_cache *>(pipeline_cache)->get_vk_pipeline_cache(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::pipeline_cache *>(pipeline_cache), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_pipeline_layout)(::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_layout ** pipeline_layout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*pipeline_layout = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::pipeline_layout>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_pipeline_layout(::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyPipelineLayout(this->vk_device, static_cast<::framework::gpu::vulkan::pipeline_layout *>(pipeline_layout)->vk_pipeline_layout, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::pipeline_layout *>(pipeline_layout), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_sampler)(::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::sampler ** sampler)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*sampler = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::sampler>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_sampler(::framework::gpu::sampler * sampler, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroySampler(this->vk_device, static_cast<::framework::gpu::vulkan::sampler *>(sampler)->vk_sampler, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::sampler *>(sampler), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_descriptor_set_layout)(::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_set_layout ** descriptor_set_layout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*descriptor_set_layout = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::descriptor_set_layout>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_descriptor_set_layout(::framework::gpu::descriptor_set_layout * descriptor_set_layout, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyDescriptorSetLayout(this->vk_device, static_cast<::framework::gpu::vulkan::descriptor_set_layout *>(descriptor_set_layout)->vk_descriptor_set_layout, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::descriptor_set_layout *>(descriptor_set_layout), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_descriptor_pool)(::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_pool ** descriptor_pool)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*descriptor_pool = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::descriptor_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::vulkan::descriptor_pool *>(descriptor_pool), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::allocate_descriptor_sets)(::framework::gpu::descriptor_set_allocate_info const * allocate_info, ::framework::gpu::descriptor_set ** descriptor_sets)
{
	::framework::gpu::result result;

	try
	{
		static_cast<::framework::gpu::vulkan::descriptor_pool *>(allocate_info->descriptor_pool)->allocate_descriptor_sets(allocate_info->descriptor_set_count, descriptor_sets);

		::framework::gpu::allocation_callbacks const & descriptor_pool_allocator = static_cast<::framework::gpu::vulkan::descriptor_pool const *>(allocate_info->descriptor_pool)->allocator;
		::framework::gpu::common::unique_ptr<::VkDescriptorSetLayout[]> vk_descriptor_set_layout = ::framework::gpu::common::make_unique<::VkDescriptorSetLayout[]>(descriptor_pool_allocator, ::framework::gpu::system_allocation_scope::command, allocate_info->descriptor_set_count);
		::framework::gpu::common::unique_ptr<::VkDescriptorSet[]> vk_descriptor_sets = ::framework::gpu::common::make_unique<::VkDescriptorSet[]>(descriptor_pool_allocator, ::framework::gpu::system_allocation_scope::command, allocate_info->descriptor_set_count);
		::framework::gpu::common::unique_ptr<::std::uint32_t[]> descriptor_counts;

		for (::std::uint32_t i = 0; i < allocate_info->descriptor_set_count; i++)
		{
			vk_descriptor_set_layout[i] = static_cast<::framework::gpu::vulkan::descriptor_set_layout const *>(allocate_info->set_layouts[i])->get_vk_descriptor_set_layout();
		}

		void * next = nullptr;

		::VkDescriptorSetVariableDescriptorCountAllocateInfo vk_descriptor_set_variable_descriptor_count_allocate_info;
		if (allocate_info->variable_descriptor_counts)
		{
			descriptor_counts = ::framework::gpu::common::make_unique<::std::uint32_t[]>(descriptor_pool_allocator, ::framework::gpu::system_allocation_scope::command, allocate_info->descriptor_set_count);

			for (::std::uint32_t i = 0; i < allocate_info->descriptor_set_count; i++)
			{
				descriptor_counts[i] = static_cast<::std::uint32_t>(allocate_info->variable_descriptor_counts[i]);
			}

			vk_descriptor_set_variable_descriptor_count_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
			vk_descriptor_set_variable_descriptor_count_allocate_info.pNext = next;
			vk_descriptor_set_variable_descriptor_count_allocate_info.descriptorSetCount = allocate_info->descriptor_set_count;
			vk_descriptor_set_variable_descriptor_count_allocate_info.pDescriptorCounts = descriptor_counts.get();

			next = &vk_descriptor_set_variable_descriptor_count_allocate_info;
		}

		::VkDescriptorSetAllocateInfo vk_descriptor_set_allocate_info;
		vk_descriptor_set_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		vk_descriptor_set_allocate_info.pNext = next;
		vk_descriptor_set_allocate_info.descriptorPool = static_cast<::framework::gpu::vulkan::descriptor_pool const *>(allocate_info->descriptor_pool)->get_vk_descriptor_pool();
		vk_descriptor_set_allocate_info.descriptorSetCount = allocate_info->descriptor_set_count;
		vk_descriptor_set_allocate_info.pSetLayouts = vk_descriptor_set_layout.get();

		::VkResult vk_result = ::vkAllocateDescriptorSets(this->vk_device, &vk_descriptor_set_allocate_info, vk_descriptor_sets.get());

		result = ::framework::gpu::vulkan::throw_result(vk_result);

		for (::std::uint32_t i = 0; i < allocate_info->descriptor_set_count; i++)
		{
			static_cast<::framework::gpu::vulkan::descriptor_set *>(descriptor_sets[i])->vk_descriptor_set = vk_descriptor_sets[i];
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		static_cast<::framework::gpu::vulkan::descriptor_pool *>(allocate_info->descriptor_pool)->free_descriptor_sets(allocate_info->descriptor_set_count, descriptor_sets);

		for (::std::uint32_t i = 0; i < allocate_info->descriptor_set_count; i++)
		{
			descriptor_sets[i] = nullptr;
		}

		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::free_descriptor_sets)(::framework::gpu::descriptor_pool * descriptor_pool, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkDescriptorSet[]> vk_descriptor_sets = ::framework::gpu::common::make_unique<::VkDescriptorSet[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_set_count);
		for (::std::uint32_t i = 0; i < descriptor_set_count; i++)
		{
			vk_descriptor_sets[i] = static_cast<::framework::gpu::vulkan::descriptor_set const *>(descriptor_sets[i])->get_vk_descriptor_set();
		}

		result = ::framework::gpu::vulkan::throw_result(::vkFreeDescriptorSets(this->vk_device, static_cast<::framework::gpu::vulkan::descriptor_pool const *>(descriptor_pool)->vk_descriptor_pool, descriptor_set_count, vk_descriptor_sets.get()));

		static_cast<::framework::gpu::vulkan::descriptor_pool *>(descriptor_pool)->free_descriptor_sets(descriptor_set_count, descriptor_sets);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::update_descriptor_sets(::std::uint32_t descriptor_write_count, ::framework::gpu::write_descriptor_set const * descriptor_writes, ::std::uint32_t descriptor_copy_count, ::framework::gpu::copy_descriptor_set const * descriptor_copies)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkWriteDescriptorSet[]> vk_write_descriptor_sets = ::framework::gpu::common::make_unique<::VkWriteDescriptorSet[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkWriteDescriptorSetAccelerationStructureKHR>[]> vk_write_descriptor_set_acceleration_structure = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkWriteDescriptorSetAccelerationStructureKHR>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkDescriptorImageInfo[]>[]> vk_descriptor_image_info = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkDescriptorImageInfo[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkDescriptorBufferInfo[]>[]> vk_descriptor_buffer_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkDescriptorBufferInfo[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkBufferView[]>[]> vk_buffer_views = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkBufferView[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAccelerationStructureKHR[]>[]> vk_acceleration_structures = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAccelerationStructureKHR[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_write_count);

		::std::uint32_t vk_descriptor_write_count = 0;
		for (::std::uint32_t i = 0; i < descriptor_write_count; i++)
		{
			vk_write_descriptor_sets[vk_descriptor_write_count].sType = ::VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			vk_write_descriptor_sets[vk_descriptor_write_count].pNext = nullptr;
			vk_write_descriptor_sets[vk_descriptor_write_count].dstArrayElement = descriptor_writes[i].dst_array_element;
			vk_write_descriptor_sets[vk_descriptor_write_count].dstSet = static_cast<::framework::gpu::vulkan::descriptor_set const *>(descriptor_writes[i].dst_set)->get_vk_descriptor_set();
			vk_write_descriptor_sets[vk_descriptor_write_count].descriptorCount = descriptor_writes[i].descriptor_count;
			vk_write_descriptor_sets[vk_descriptor_write_count].dstBinding = descriptor_writes[i].dst_binding;

			auto init_image_info = [&]
			{
				if (descriptor_writes[i].descriptor_count)
				{
					vk_descriptor_image_info[vk_descriptor_write_count] = ::framework::gpu::common::make_unique<::VkDescriptorImageInfo[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_writes[i].descriptor_count);
					for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
					{
						vk_descriptor_image_info[vk_descriptor_write_count][j].sampler = descriptor_writes[i].image_info[j].sampler ? static_cast<::framework::gpu::vulkan::sampler const *>(descriptor_writes[i].image_info[j].sampler)->get_vk_sampler() : VK_NULL_HANDLE;
						vk_descriptor_image_info[vk_descriptor_write_count][j].imageView = descriptor_writes[i].image_info[j].image_view ? static_cast<::framework::gpu::vulkan::image_view const *>(descriptor_writes[i].image_info[j].image_view)->get_vk_image_view() : VK_NULL_HANDLE;
						vk_descriptor_image_info[vk_descriptor_write_count][j].imageLayout = ::framework::gpu::vulkan::get_vk_image_layout(descriptor_writes[i].image_info[j].image_layout);
					}
				}

				vk_write_descriptor_sets[vk_descriptor_write_count].pImageInfo = vk_descriptor_image_info[vk_descriptor_write_count].get();
			};

			auto init_buffer_info = [&]
			{
				if (descriptor_writes[i].descriptor_count)
				{
					vk_descriptor_buffer_infos[vk_descriptor_write_count] = ::framework::gpu::common::make_unique<::VkDescriptorBufferInfo[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_writes[i].descriptor_count);
					for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
					{
						vk_descriptor_buffer_infos[vk_descriptor_write_count][j].buffer = static_cast<::framework::gpu::vulkan::buffer const *>(descriptor_writes[i].buffer_info[j].buffer)->get_vk_buffer();
						vk_descriptor_buffer_infos[vk_descriptor_write_count][j].offset = descriptor_writes[i].buffer_info[j].offset;
						vk_descriptor_buffer_infos[vk_descriptor_write_count][j].range = descriptor_writes[i].buffer_info[j].range;
					}
				}

				vk_write_descriptor_sets[vk_descriptor_write_count].pBufferInfo = vk_descriptor_buffer_infos[vk_descriptor_write_count].get();
			};

			auto init_buffer_view = [&]
			{
				if (descriptor_writes[i].descriptor_count)
				{
					vk_buffer_views[vk_descriptor_write_count] = ::framework::gpu::common::make_unique<::VkBufferView[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_writes[i].descriptor_count);
					for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
					{
						vk_buffer_views[vk_descriptor_write_count][j] = static_cast<::framework::gpu::vulkan::buffer_view const *>(descriptor_writes[i].texel_buffer_view[j])->get_vk_buffer_view();
					}
				}

				vk_write_descriptor_sets[vk_descriptor_write_count].pTexelBufferView = vk_buffer_views[vk_descriptor_write_count].get();
			};

			auto ini_acceleration_structures = [&]
			{
				if (descriptor_writes[i].descriptor_count)
				{
					vk_acceleration_structures[vk_descriptor_write_count] = ::framework::gpu::common::make_unique<::VkAccelerationStructureKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_writes[i].descriptor_count);
					for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
					{
						vk_acceleration_structures[vk_descriptor_write_count][j] = static_cast<::framework::gpu::vulkan::acceleration_structure const *>(descriptor_writes[i].acceleration_structures[j])->get_vk_acceleration_structure();
					}
				}

				vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count] = ::framework::gpu::common::make_unique<::VkWriteDescriptorSetAccelerationStructureKHR>(this->allocator, ::framework::gpu::system_allocation_scope::command);
				vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
				vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count]->pNext = vk_write_descriptor_sets[vk_descriptor_write_count].pNext;
				vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count]->accelerationStructureCount = descriptor_writes[i].descriptor_count;
				vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count]->pAccelerationStructures = vk_acceleration_structures[vk_descriptor_write_count].get();

				vk_write_descriptor_sets[vk_descriptor_write_count].pNext = vk_write_descriptor_set_acceleration_structure[vk_descriptor_write_count].get();
			};

			switch (descriptor_writes[i].descriptor_type)
			{
			case ::framework::gpu::descriptor_type::sampler:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_SAMPLER;
				init_image_info();
				break;
			case ::framework::gpu::descriptor_type::combined_image_sampler:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				init_image_info();
				break;
			case ::framework::gpu::descriptor_type::sampled_image:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = VkDescriptorType::VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
				init_image_info();
				break;
			case ::framework::gpu::descriptor_type::storage_image:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
				init_image_info();
				break;
			case ::framework::gpu::descriptor_type::uniform_texel_buffer:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
				init_buffer_view();
				break;
			case ::framework::gpu::descriptor_type::storage_texel_buffer:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
				init_buffer_view();
				break;
			case ::framework::gpu::descriptor_type::uniform_buffer:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				init_buffer_info();
				break;
			case ::framework::gpu::descriptor_type::sampled_buffer:
			case ::framework::gpu::descriptor_type::storage_buffer:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
				init_buffer_info();
				break;
			case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
				init_buffer_info();
				break;
			case ::framework::gpu::descriptor_type::sampled_buffer_dynamic:
			case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
				init_buffer_info();
				break;
			case ::framework::gpu::descriptor_type::input_attachment:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
				init_image_info();
				break;
			case ::framework::gpu::descriptor_type::acceleration_structure:
				vk_write_descriptor_sets[vk_descriptor_write_count].descriptorType = ::VkDescriptorType::VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
				ini_acceleration_structures();
				break;
			case ::framework::gpu::descriptor_type::color_attachment:
			case ::framework::gpu::descriptor_type::depth_stencil_attachment:
				continue;
			}
			vk_descriptor_write_count++;
		}

		::std::uint32_t vk_descriptor_copy_count = 0;
		::framework::gpu::common::unique_ptr<::VkCopyDescriptorSet[]> vk_copy_descriptor_sets = ::framework::gpu::common::make_unique<::VkCopyDescriptorSet[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, descriptor_copy_count);
		for (::std::uint32_t i = 0; i < descriptor_copy_count; i++)
		{
			switch (descriptor_copies[i].descriptor_type)
			{
			case ::framework::gpu::descriptor_type::color_attachment:
			case ::framework::gpu::descriptor_type::depth_stencil_attachment:
				continue;
			}

			vk_copy_descriptor_sets[vk_descriptor_copy_count].sType = ::VkStructureType::VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].pNext = nullptr;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].srcSet = static_cast<::framework::gpu::vulkan::descriptor_set *>(descriptor_copies[i].src_set)->get_vk_descriptor_set();
			vk_copy_descriptor_sets[vk_descriptor_copy_count].srcBinding = descriptor_copies[i].src_binding;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].srcArrayElement = descriptor_copies[i].src_array_element;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].dstSet = static_cast<::framework::gpu::vulkan::descriptor_set *>(descriptor_copies[i].dst_set)->get_vk_descriptor_set();
			vk_copy_descriptor_sets[vk_descriptor_copy_count].dstBinding = descriptor_copies[i].dst_binding;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].dstArrayElement = descriptor_copies[i].dst_array_element;
			vk_copy_descriptor_sets[vk_descriptor_copy_count].descriptorCount = descriptor_copies[i].descriptor_count;
			vk_descriptor_copy_count++;
		}

		::vkUpdateDescriptorSets(this->vk_device, vk_descriptor_write_count, vk_write_descriptor_sets.get(), vk_descriptor_copy_count, vk_copy_descriptor_sets.get());
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_frame_buffer)(::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::frame_buffer ** frame_buffer)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*frame_buffer = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::frame_buffer>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_frame_buffer(::framework::gpu::frame_buffer * frame_buffer, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroyFramebuffer(this->vk_device, static_cast<::framework::gpu::vulkan::frame_buffer *>(frame_buffer)->get_vk_framebuffer(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::frame_buffer *>(frame_buffer), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_render_pass)(::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass ** render_pass)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*render_pass = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::render_pass>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_render_pass(::framework::gpu::render_pass * render_pass, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	::vkDestroyRenderPass(this->vk_device, static_cast<::framework::gpu::vulkan::render_pass *>(render_pass)->vk_render_pass, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::render_pass *>(render_pass), selected_allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_render_pass_state)(::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass_state ** render_pass_state)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*render_pass_state = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::render_pass_state>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_render_pass_state(::framework::gpu::render_pass_state * render_pass_state, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	DELETE(static_cast<::framework::gpu::vulkan::render_pass_state *>(render_pass_state), selected_allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_command_pool)(::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::command_pool ** command_pool)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*command_pool = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::command_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	::vkDestroyCommandPool(this->vk_device, static_cast<::framework::gpu::vulkan::command_pool *>(command_pool)->vk_command_pool, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::command_pool *>(command_pool), selected_allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::reset_command_pool)(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_reset_flags flags)
{
	::VkCommandPoolResetFlags vk_command_pool_reset_flags = 0;
	if (!!(flags & ::framework::gpu::command_pool_reset_flags::reset_release_resources_bit))
		vk_command_pool_reset_flags |= ::VkCommandPoolResetFlagBits::VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT;

	::VkResult vk_result = ::vkResetCommandPool(this->vk_device, static_cast<::framework::gpu::vulkan::command_pool *>(command_pool)->get_vk_command_pool(), vk_command_pool_reset_flags);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

void ::framework::gpu::vulkan::device::trim_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_trim_flags flags)
{
	::VkCommandPoolTrimFlags vk_command_pool_trim_flags = 0;

	::vkTrimCommandPool(this->vk_device, static_cast<::framework::gpu::vulkan::command_pool *>(command_pool)->get_vk_command_pool(), vk_command_pool_trim_flags);
}

::framework::gpu::result(::framework::gpu::vulkan::device::allocate_command_buffers)(::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::command_buffer ** command_buffers)
{
	::framework::gpu::result result;
	::std::uint32_t command_buffer_index = 0;

	try
	{
		::framework::gpu::common::unique_ptr<::VkCommandBuffer[]> vk_command_buffers = ::framework::gpu::common::make_unique<::VkCommandBuffer[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, allocate_info->command_buffer_count);

		::VkCommandBufferLevel vk_command_buffer_level;
		switch (allocate_info->level)
		{
		case ::framework::gpu::command_buffer_level::primary:
			vk_command_buffer_level = ::VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY;
			break;
		case ::framework::gpu::command_buffer_level::secondary:
			vk_command_buffer_level = ::VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_SECONDARY;
			break;
		}

		::VkCommandBufferAllocateInfo vk_command_buffer_allocate_info;
		vk_command_buffer_allocate_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		vk_command_buffer_allocate_info.pNext = nullptr;
		vk_command_buffer_allocate_info.commandPool = static_cast<::framework::gpu::vulkan::command_pool *>(allocate_info->command_pool)->get_vk_command_pool();
		vk_command_buffer_allocate_info.level = vk_command_buffer_level;
		vk_command_buffer_allocate_info.commandBufferCount = allocate_info->command_buffer_count;

		result = ::framework::gpu::vulkan::throw_result(::vkAllocateCommandBuffers(this->vk_device, &vk_command_buffer_allocate_info, vk_command_buffers.get()));

		for (; command_buffer_index < allocate_info->command_buffer_count; command_buffer_index++)
		{
			command_buffers[command_buffer_index] = new ::framework::gpu::vulkan::command_buffer(this, allocate_info, vk_command_buffers[command_buffer_index]);
			static_cast<::framework::gpu::vulkan::command_pool *>(allocate_info->command_pool)->allocate_command_buffer(static_cast<::framework::gpu::vulkan::command_buffer *>(command_buffers[command_buffer_index]));
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = 0; i < command_buffer_index; i++)
		{
			static_cast<::framework::gpu::vulkan::command_pool *>(allocate_info->command_pool)->free_command_buffer(static_cast<::framework::gpu::vulkan::command_buffer *>(command_buffers[i]));
		}

		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::free_command_buffers(::framework::gpu::command_pool * command_pool, ::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers)
{
	try
	{
		::framework::gpu::common::unique_ptr<::VkCommandBuffer[]> vk_command_buffers = ::framework::gpu::common::make_unique<::VkCommandBuffer[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, command_buffer_count);

		for (::std::uint32_t i = 0; i < command_buffer_count; i++)
		{
			vk_command_buffers[i] = static_cast<::framework::gpu::vulkan::command_buffer const *>(command_buffers[i])->get_vk_command_buffer();
		}

		::vkFreeCommandBuffers(this->vk_device, static_cast<::framework::gpu::vulkan::command_pool const *>(command_pool)->get_vk_command_pool(), command_buffer_count, vk_command_buffers.get());

		for (::std::uint32_t i = 0; i < command_buffer_count; i++)
		{
			static_cast<::framework::gpu::vulkan::command_pool *>(command_pool)->free_command_buffer(static_cast<::framework::gpu::vulkan::command_buffer *>(command_buffers[i]));
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_swap_chain)(::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::swap_chain ** swap_chain)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*swap_chain = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::swap_chain>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_swap_chain(::framework::gpu::swap_chain * swap_chain, ::framework::gpu::allocation_callbacks const * allocator)
{
	::vkDestroySwapchainKHR(this->vk_device, static_cast<::framework::gpu::vulkan::swap_chain *>(swap_chain)->vk_swapchain, nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::swap_chain *>(swap_chain), allocator ? *allocator : this->allocator);
}

void ::framework::gpu::vulkan::device::get_swap_chain_images(::framework::gpu::swap_chain * swap_chain, ::std::uint32_t * swap_chain_image_count, ::framework::gpu::image ** swap_chain_images)
{
	static_cast<::framework::gpu::vulkan::swap_chain *>(swap_chain)->get_images(swap_chain_image_count, swap_chain_images);
}

::framework::gpu::result(::framework::gpu::vulkan::device::acquire_next_image)(::framework::gpu::acquire_next_image_info const * acquire_info, ::std::uint32_t * image_index)
{
	::VkResult vk_result;

	::VkAcquireNextImageInfoKHR vk_acquire_next_image_info;
	vk_acquire_next_image_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR;
	vk_acquire_next_image_info.pNext = nullptr;
	vk_acquire_next_image_info.swapchain = acquire_info->swap_chain ? static_cast<::framework::gpu::vulkan::swap_chain *>(acquire_info->swap_chain)->get_vk_swapchain() : VK_NULL_HANDLE;
	vk_acquire_next_image_info.timeout = acquire_info->timeout;
	vk_acquire_next_image_info.semaphore = acquire_info->semaphore ? static_cast<::framework::gpu::vulkan::semaphore *>(acquire_info->semaphore)->get_vk_semaphore() : VK_NULL_HANDLE;
	vk_acquire_next_image_info.fence = acquire_info->fence ? static_cast<::framework::gpu::vulkan::fence *>(acquire_info->fence)->get_vk_fence() : VK_NULL_HANDLE;
	vk_acquire_next_image_info.deviceMask = acquire_info->device_mask;

	vk_result = ::vkAcquireNextImage2KHR(this->vk_device, &vk_acquire_next_image_info, image_index);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result framework::gpu::vulkan::device::create_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure_geometry_set ** acceleration_structure_geometry_set)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*acceleration_structure_geometry_set = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::acceleration_structure_geometry_set>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set * acceleration_structure_geometry_set, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	DELETE(static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(acceleration_structure_geometry_set), selected_allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_acceleration_structure)(::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure ** acceleration_structure)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*acceleration_structure = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::acceleration_structure>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::vulkan::device::destroy_acceleration_structure(::framework::gpu::acceleration_structure * acceleration_structure, ::framework::gpu::allocation_callbacks const * allocator)
{
	this->get_dynamic_library().vkDestroyAccelerationStructureKHR(this->get_vk_device(), static_cast<::framework::gpu::vulkan::acceleration_structure *>(acceleration_structure)->get_vk_acceleration_structure(), nullptr);
	DELETE(static_cast<::framework::gpu::vulkan::acceleration_structure *>(acceleration_structure), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::vulkan::device::build_acceleration_structures)(::framework::gpu::deferred_operation * deferred_operation, ::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildGeometryInfoKHR[]> vk_acceleration_structure_build_geometry_infos = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildGeometryInfoKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, info_count);
		::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR * []> vk_acceleration_structure_build_range_info_pointers = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR * []>(this->allocator, ::framework::gpu::system_allocation_scope::command, info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR[]>[]> vk_acceleration_structure_build_range_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkAccelerationStructureBuildRangeInfoKHR[]>[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, info_count);

		for (::std::uint32_t i = 0; i < info_count; i++)
		{
			vk_acceleration_structure_build_geometry_infos[i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
			vk_acceleration_structure_build_geometry_infos[i].pNext = nullptr;
			vk_acceleration_structure_build_geometry_infos[i].type = ::framework::gpu::vulkan::get_vk_acceleration_structure_type(infos[i].type);
			vk_acceleration_structure_build_geometry_infos[i].flags = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_flags(infos[i].flags);
			vk_acceleration_structure_build_geometry_infos[i].mode = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_mode(infos[i].mode);
			vk_acceleration_structure_build_geometry_infos[i].srcAccelerationStructure = infos[i].src_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(infos[i].src_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
			vk_acceleration_structure_build_geometry_infos[i].dstAccelerationStructure = infos[i].dst_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(infos[i].dst_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
			vk_acceleration_structure_build_geometry_infos[i].geometryCount = static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(infos[i].geometries)->get_geometry_count();
			static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(infos[i].geometries)->init_geometries(infos[i].first_geometry, vk_acceleration_structure_build_geometry_infos[i].pGeometries, vk_acceleration_structure_build_geometry_infos[i].ppGeometries);
			vk_acceleration_structure_build_geometry_infos[i].scratchData = ::framework::gpu::vulkan::get_vk_device_or_host_address(infos[i].scratch_data);

			vk_acceleration_structure_build_range_infos[i] = ::framework::gpu::common::make_unique<::VkAccelerationStructureBuildRangeInfoKHR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, infos[i].geometry_count);
			static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(infos[i].geometries)->init_build_range_infos(infos[i].first_geometry, infos[i].geometry_count, vk_acceleration_structure_build_range_infos[i].get());

			vk_acceleration_structure_build_range_info_pointers[i] = vk_acceleration_structure_build_range_infos[i].get();
		}

		result = ::framework::gpu::vulkan::throw_result(this->dynamic_library.vkBuildAccelerationStructuresKHR(this->vk_device, deferred_operation ? static_cast<::framework::gpu::vulkan::deferred_operation *>(deferred_operation)->get_vk_deferred_operation() : VK_NULL_HANDLE, info_count, vk_acceleration_structure_build_geometry_infos.get(), vk_acceleration_structure_build_range_info_pointers.get()));
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::vulkan::device::copy_acceleration_structure)(::framework::gpu::copy_acceleration_structure_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::vulkan::device::copy_acceleration_structure_to_memory)(::framework::gpu::copy_acceleration_structure_to_memory_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::vulkan::device::copy_memory_to_acceleration_structure)(::framework::gpu::copy_memory_to_acceleration_structure_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::vulkan::device::get_ray_tracing_shader_group_handles)(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data)
{
	::VkResult vk_result;

	vk_result = this->dynamic_library.vkGetRayTracingShaderGroupHandlesKHR(this->vk_device, pipeline ? static_cast<::framework::gpu::vulkan::pipeline *>(pipeline)->get_vk_pipeline() : VK_NULL_HANDLE, first_group, group_count, data_size, data);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::device::write_acceleration_structures_properties)(::std::uint32_t acceleration_structure_count, ::framework::gpu::acceleration_structure const * acceleration_structures, ::framework::gpu::query_type query_type, ::std::size_t data_size, void * data, ::std::size_t stride)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::vulkan::device::create_ray_tracing_pipelines)(::framework::gpu::deferred_operation * deferred_operation, ::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::ray_tracing_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result;
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
	::std::uint32_t pipeline_index = 0;

	try
	{
		::framework::gpu::common::unique_ptr<::VkPipeline[]> vk_pipelines = ::framework::gpu::common::make_unique<::VkPipeline[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::VkRayTracingPipelineCreateInfoKHR[]> vk_ray_tracing_pipeline_create_infos = ::framework::gpu::common::make_unique<::VkRayTracingPipelineCreateInfoKHR[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineShaderStageCreateInfo[]>[]> vk_pipeline_shader_stage_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineShaderStageCreateInfo[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkRayTracingShaderGroupCreateInfoKHR[]>[]> vk_ray_tracing_shader_group_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkRayTracingShaderGroupCreateInfoKHR[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineLibraryCreateInfoKHR>[]> vk_pipeline_library_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineLibraryCreateInfoKHR>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkRayTracingPipelineInterfaceCreateInfoKHR>[]> vk_ray_tracing_pipeline_interface_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkRayTracingPipelineInterfaceCreateInfoKHR>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipeline[]>[]> vk_libraries = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipeline[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]> vk_specialization_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]> vk_specialization_map_entries = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkSpecializationMapEntry[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkPipelineDynamicStateCreateInfo>[]> vk_pipeline_dynamic_state_create_infos = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkPipelineDynamicStateCreateInfo>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::VkDynamicState[]>[]> vk_dynamic_states = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::VkDynamicState[]>[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info_count);

		for (::std::uint32_t create_info_index = 0; create_info_index < create_info_count; create_info_index++)
		{
			auto & create_info = create_infos[create_info_index];

			if (create_info.stage_count)
			{
				vk_pipeline_shader_stage_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineShaderStageCreateInfo[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.stage_count);
				for (::std::uint32_t i = 0; i < create_info.stage_count; i++)
				{
					if (create_info.stages[i].specialization_info)
					{
						vk_specialization_map_entries[create_info_index] = ::framework::gpu::common::make_unique<::VkSpecializationMapEntry[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.stages[i].specialization_info->map_entry_count);
						for (::std::uint32_t map_entry_index = 0; map_entry_index < create_info.stages[i].specialization_info->map_entry_count; map_entry_index++)
						{
							vk_specialization_map_entries[create_info_index][map_entry_index].constantID = create_info.stages[i].specialization_info->map_entries[map_entry_index].constant_id;
							vk_specialization_map_entries[create_info_index][map_entry_index].offset = create_info.stages[i].specialization_info->map_entries[map_entry_index].offset;
							vk_specialization_map_entries[create_info_index][map_entry_index].size = create_info.stages[i].specialization_info->map_entries[map_entry_index].size;
						}

						vk_specialization_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkSpecializationInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
						vk_specialization_infos[create_info_index]->mapEntryCount = create_info.stages[i].specialization_info->map_entry_count;
						vk_specialization_infos[create_info_index]->pMapEntries = vk_specialization_map_entries[create_info_index].get();
						vk_specialization_infos[create_info_index]->dataSize = create_info.stages[i].specialization_info->data_size;
						vk_specialization_infos[create_info_index]->pData = create_info.stages[i].specialization_info->data;
					}

					vk_pipeline_shader_stage_create_infos[create_info_index][i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pNext = nullptr;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].flags = ::framework::gpu::vulkan::get_vk_pipeline_shader_stage_create_flags(create_info.stages[i].flags);
					vk_pipeline_shader_stage_create_infos[create_info_index][i].stage = ::framework::gpu::vulkan::get_vk_shader_stage_flag_bits(create_info.stages[i].stage);
					vk_pipeline_shader_stage_create_infos[create_info_index][i].module = static_cast<::framework::gpu::vulkan::shader_module const *>(create_info.stages[i].module)->get_vk_shader_module();
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pName = create_info.stages[i].name;
					vk_pipeline_shader_stage_create_infos[create_info_index][i].pSpecializationInfo = vk_specialization_infos[create_info_index].get();
				}
			}

			if (create_info.group_count)
			{
				vk_ray_tracing_shader_group_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkRayTracingShaderGroupCreateInfoKHR[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.group_count);
				for (::std::uint32_t i = 0; i < create_info.group_count; i++)
				{
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].sType = ::VkStructureType::VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].pNext = nullptr;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].type = ::framework::gpu::vulkan::get_vk_ray_tracing_shader_group_type(create_info.groups[i].type);
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].generalShader = create_info.groups[i].general_shader;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].closestHitShader = create_info.groups[i].closest_hit_shader;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].anyHitShader = create_info.groups[i].any_hit_shader;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].intersectionShader = create_info.groups[i].intersection_shader;
					vk_ray_tracing_shader_group_create_infos[create_info_index][i].pShaderGroupCaptureReplayHandle = create_info.groups[i].shader_group_capture_replay_handle;
				}
			}

			::VkPipelineLibraryCreateInfoKHR * vk_pipeline_library_create_info = nullptr;
			if (create_info.library_info)
			{
				if (create_info.library_info->library_count)
				{
					vk_libraries[create_info_index] = ::framework::gpu::common::make_unique<::VkPipeline[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.library_info->library_count);
					for (::std::uint32_t i = 0; i < create_info.library_info->library_count; i++)
					{
						vk_libraries[create_info_index][i] = static_cast<::framework::gpu::vulkan::pipeline const *>(create_info.library_info->library_info[i])->get_vk_pipeline();
					}
				}

				vk_pipeline_library_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineLibraryCreateInfoKHR>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_library_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR;
				vk_pipeline_library_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_library_create_infos[create_info_index]->libraryCount = create_info.library_info->library_count;
				vk_pipeline_library_create_infos[create_info_index]->pLibraries = vk_libraries[create_info_index].get();
			}

			vk_ray_tracing_pipeline_interface_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkRayTracingPipelineInterfaceCreateInfoKHR>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
			vk_ray_tracing_pipeline_interface_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR;
			vk_ray_tracing_pipeline_interface_create_infos[create_info_index]->pNext = nullptr;
			vk_ray_tracing_pipeline_interface_create_infos[create_info_index]->maxPipelineRayPayloadSize = create_info.max_pipeline_ray_payload_size;
			vk_ray_tracing_pipeline_interface_create_infos[create_info_index]->maxPipelineRayHitAttributeSize = create_info.max_pipeline_ray_hit_attribute_size;

			if (create_info.dynamic_state)
			{
				if (create_info.dynamic_state->dynamic_state_count)
				{
					vk_dynamic_states[create_info_index] = ::framework::gpu::common::make_unique<::VkDynamicState[]>(selected_allocator, ::framework::gpu::system_allocation_scope::command, create_info.dynamic_state->dynamic_state_count);
					for (::std::uint32_t i = 0; i < create_info.dynamic_state->dynamic_state_count; i++)
					{
						vk_dynamic_states[create_info_index][i] = ::framework::gpu::vulkan::get_vk_dynamic_state(create_info.dynamic_state->dynamic_states[i]);
					}
				}

				vk_pipeline_dynamic_state_create_infos[create_info_index] = ::framework::gpu::common::make_unique<::VkPipelineDynamicStateCreateInfo>(selected_allocator, ::framework::gpu::system_allocation_scope::command);
				vk_pipeline_dynamic_state_create_infos[create_info_index]->sType = ::VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->pNext = nullptr;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->flags = ::framework::gpu::vulkan::get_vk_pipeline_dynamic_state_create_flags(create_info.dynamic_state->flags);
				vk_pipeline_dynamic_state_create_infos[create_info_index]->dynamicStateCount = create_info.dynamic_state->dynamic_state_count;
				vk_pipeline_dynamic_state_create_infos[create_info_index]->pDynamicStates = vk_dynamic_states[create_info_index].get();
			}

			vk_ray_tracing_pipeline_create_infos[create_info_index].sType = ::VkStructureType::VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
			vk_ray_tracing_pipeline_create_infos[create_info_index].pNext = nullptr;
			vk_ray_tracing_pipeline_create_infos[create_info_index].flags = ::framework::gpu::vulkan::get_vk_pipeline_create_flags(create_info.flags);
			vk_ray_tracing_pipeline_create_infos[create_info_index].stageCount = create_info.stage_count;
			vk_ray_tracing_pipeline_create_infos[create_info_index].pStages = vk_pipeline_shader_stage_create_infos[create_info_index].get();
			vk_ray_tracing_pipeline_create_infos[create_info_index].groupCount = create_info.group_count;
			vk_ray_tracing_pipeline_create_infos[create_info_index].pGroups = vk_ray_tracing_shader_group_create_infos[create_info_index].get();
			vk_ray_tracing_pipeline_create_infos[create_info_index].maxPipelineRayRecursionDepth = create_info.max_pipeline_ray_recursion_depth;
			vk_ray_tracing_pipeline_create_infos[create_info_index].pLibraryInfo = vk_pipeline_library_create_info;
			vk_ray_tracing_pipeline_create_infos[create_info_index].pLibraryInterface = vk_ray_tracing_pipeline_interface_create_infos[create_info_index].get();
			vk_ray_tracing_pipeline_create_infos[create_info_index].pDynamicState = vk_pipeline_dynamic_state_create_infos[create_info_index].get();
			vk_ray_tracing_pipeline_create_infos[create_info_index].layout = static_cast<::framework::gpu::vulkan::pipeline_layout const *>(create_info.layout)->get_vk_pipeline_layout();
			vk_ray_tracing_pipeline_create_infos[create_info_index].basePipelineHandle = create_info.base_pipeline ? static_cast<::framework::gpu::vulkan::pipeline *>(create_info.base_pipeline)->get_vk_pipeline() : VK_NULL_HANDLE;
			vk_ray_tracing_pipeline_create_infos[create_info_index].basePipelineIndex = create_info.base_pipeline_index;
		}

		result = ::framework::gpu::vulkan::throw_result(this->dynamic_library.vkCreateRayTracingPipelinesKHR(
			this->vk_device,
			deferred_operation ? static_cast<::framework::gpu::vulkan::deferred_operation *>(deferred_operation)->get_vk_deferred_operation() : VK_NULL_HANDLE,
			pipeline_cache ? static_cast<::framework::gpu::vulkan::pipeline_cache *>(pipeline_cache)->get_vk_pipeline_cache() : VK_NULL_HANDLE,
			create_info_count,
			vk_ray_tracing_pipeline_create_infos.get(),
			nullptr,
			vk_pipelines.get()
		));

		for (; pipeline_index < create_info_count; pipeline_index++)
		{
			pipelines[pipeline_index] = ::framework::gpu::common::make_unique<::framework::gpu::vulkan::ray_tracing_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[pipeline_index], vk_pipelines[pipeline_index]).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = 0; i < pipeline_index; i++)
		{
			this->destroy_pipeline(pipelines[i], &selected_allocator);
		}

		result = exception.result;
	}

	return result;
}

::framework::gpu::device_address(::framework::gpu::vulkan::device::get_acceleration_structure_device_address)(::framework::gpu::acceleration_structure_device_address_info const * info)
{
	::VkAccelerationStructureDeviceAddressInfoKHR vk_acceleration_structure_device_address_info;
	vk_acceleration_structure_device_address_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
	vk_acceleration_structure_device_address_info.pNext = nullptr;
	vk_acceleration_structure_device_address_info.accelerationStructure = static_cast<::framework::gpu::vulkan::acceleration_structure *>(info->acceleration_structure)->get_vk_acceleration_structure();

	return this->dynamic_library.vkGetAccelerationStructureDeviceAddressKHR(this->vk_device, &vk_acceleration_structure_device_address_info);
}

::framework::gpu::result(::framework::gpu::vulkan::device::get_ray_tracing_capture_replay_shader_group_handles)(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data)
{
	::VkResult vk_result;

	vk_result = this->dynamic_library.vkGetRayTracingCaptureReplayShaderGroupHandlesKHR(this->vk_device, static_cast<::framework::gpu::vulkan::pipeline *>(pipeline)->get_vk_pipeline(), first_group, group_count, data_size, data);

	return ::framework::gpu::vulkan::get_result(vk_result);
}

::framework::gpu::result(::framework::gpu::vulkan::device::get_device_acceleration_structure_compatibility)(::framework::gpu::acceleration_structure_version const * version)
{
	return ::framework::gpu::result();
}

void ::framework::gpu::vulkan::device::get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type build_type, ::framework::gpu::acceleration_structure_build_geometry_info const * build_info, ::framework::gpu::acceleration_structure_build_sizes_info * size_info)
{
	try
	{
		::VkAccelerationStructureBuildGeometryInfoKHR vk_acceleration_structure_build_geometry_info;
		vk_acceleration_structure_build_geometry_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
		vk_acceleration_structure_build_geometry_info.pNext = nullptr;
		vk_acceleration_structure_build_geometry_info.type = ::framework::gpu::vulkan::get_vk_acceleration_structure_type(build_info->type);
		vk_acceleration_structure_build_geometry_info.flags = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_flags(build_info->flags);
		vk_acceleration_structure_build_geometry_info.mode = ::framework::gpu::vulkan::get_vk_build_acceleration_structure_mode(build_info->mode);
		vk_acceleration_structure_build_geometry_info.srcAccelerationStructure = build_info->src_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(build_info->src_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
		vk_acceleration_structure_build_geometry_info.dstAccelerationStructure = build_info->dst_acceleration_structure ? static_cast<::framework::gpu::vulkan::acceleration_structure *>(build_info->dst_acceleration_structure)->get_vk_acceleration_structure() : VK_NULL_HANDLE;
		vk_acceleration_structure_build_geometry_info.geometryCount = static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(build_info->geometries)->get_geometry_count();
		static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(build_info->geometries)->init_geometries(build_info->first_geometry, vk_acceleration_structure_build_geometry_info.pGeometries, vk_acceleration_structure_build_geometry_info.ppGeometries);
		vk_acceleration_structure_build_geometry_info.scratchData = ::framework::gpu::vulkan::get_vk_device_or_host_address(build_info->scratch_data);

		::framework::gpu::common::unique_ptr<::std::uint32_t[]> max_primitive_counts = ::framework::gpu::common::make_unique<::std::uint32_t[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, build_info->geometry_count);
		static_cast<::framework::gpu::vulkan::acceleration_structure_geometry_set *>(build_info->geometries)->init_max_primitive_counts(build_info->first_geometry, build_info->geometry_count, max_primitive_counts.get());

		::VkAccelerationStructureBuildSizesInfoKHR vk_acceleration_structure_build_sizes_info;
		vk_acceleration_structure_build_sizes_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
		vk_acceleration_structure_build_sizes_info.pNext = nullptr;

		this->dynamic_library.vkGetAccelerationStructureBuildSizesKHR(this->vk_device, ::framework::gpu::vulkan::get_vk_acceleration_structure_build_type(build_type), &vk_acceleration_structure_build_geometry_info, max_primitive_counts.get(), &vk_acceleration_structure_build_sizes_info);

		size_info->acceleration_structure_size = vk_acceleration_structure_build_sizes_info.accelerationStructureSize;
		size_info->update_scratch_size = vk_acceleration_structure_build_sizes_info.updateScratchSize;
		size_info->build_scratch_size = vk_acceleration_structure_build_sizes_info.buildScratchSize;
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
	}
}

::framework::gpu::device_or_host_descriptor_handle(::framework::gpu::vulkan::device::get_descriptor_handle)(::framework::gpu::descriptor_handle_info const * info)
{
	return ::framework::gpu::device_or_host_descriptor_handle{};
}

::framework::gpu::device_address(::framework::gpu::vulkan::device::get_buffer_device_address)(::framework::gpu::buffer_device_address_info const * info)
{
	::framework::gpu::device_address device_address;

	::VkBufferDeviceAddressInfo vk_buffer_device_address_info;
	vk_buffer_device_address_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
	vk_buffer_device_address_info.pNext = nullptr;
	vk_buffer_device_address_info.buffer = static_cast<::framework::gpu::vulkan::buffer *>(info->buffer)->get_vk_buffer();

	device_address = ::vkGetBufferDeviceAddress(this->vk_device, &vk_buffer_device_address_info);

	return device_address;
}

::std::uint64_t(::framework::gpu::vulkan::device::get_buffer_opaque_capture_address)(::framework::gpu::buffer_device_address_info const * info)
{
	return ::std::uint64_t();
}

::std::uint64_t(::framework::gpu::vulkan::device::get_device_memory_opaque_capture_address)(::framework::gpu::device_memory_opaque_capture_address_info const * info)
{
	return ::std::uint64_t();
}

::framework::gpu::result framework::gpu::vulkan::device::set_object_name(::framework::gpu::debug_utils_object_name_info const * name_info)
{
	if (!this->dynamic_library.vkSetDebugUtilsObjectNameEXT)
	{
		return ::framework::gpu::result::success;
	}

	::VkResult vk_result;

	::VkObjectType vk_object_type;
	::std::uint64_t vk_object;

	switch (name_info->object_type)
	{
	case framework::gpu::object_type::instance:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_INSTANCE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::instance *>(name_info->object)->get_vk_instance());
		break;
	case framework::gpu::object_type::physical_device:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_PHYSICAL_DEVICE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::physical_device *>(name_info->object)->get_vk_physical_device());
		break;
	case framework::gpu::object_type::device:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_DEVICE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::device *>(name_info->object)->get_vk_device());
		break;
	case framework::gpu::object_type::queue:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_QUEUE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::queue *>(name_info->object)->get_vk_queue());
		break;
	case framework::gpu::object_type::semaphore:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_SEMAPHORE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::semaphore *>(name_info->object)->get_vk_semaphore());
		break;
	case framework::gpu::object_type::command_buffer:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_COMMAND_BUFFER;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::command_buffer *>(name_info->object)->get_vk_command_buffer());
		break;
	case framework::gpu::object_type::fence:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_FENCE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::fence *>(name_info->object)->get_vk_fence());
		break;
	case framework::gpu::object_type::device_memory:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_DEVICE_MEMORY;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::device_memory *>(name_info->object)->get_vk_device_memory());
		break;
	case framework::gpu::object_type::buffer:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_BUFFER;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::buffer *>(name_info->object)->get_vk_buffer());
		break;
	case framework::gpu::object_type::image:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_IMAGE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::image *>(name_info->object)->get_vk_image());
		break;
	case framework::gpu::object_type::query_pool:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_QUERY_POOL;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::query_pool *>(name_info->object)->get_vk_query_pool());
		break;
	case framework::gpu::object_type::buffer_view:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_BUFFER_VIEW;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::buffer_view *>(name_info->object)->get_vk_buffer_view());
		break;
	case framework::gpu::object_type::image_view:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_IMAGE_VIEW;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::image_view *>(name_info->object)->get_vk_image_view());
		break;
	case framework::gpu::object_type::shader_module:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_SHADER_MODULE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::shader_module *>(name_info->object)->get_vk_shader_module());
		break;
	case framework::gpu::object_type::pipeline_cache:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_PIPELINE_CACHE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::pipeline_cache *>(name_info->object)->get_vk_pipeline_cache());
		break;
	case framework::gpu::object_type::pipeline_layout:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_PIPELINE_LAYOUT;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::pipeline_layout *>(name_info->object)->get_vk_pipeline_layout());
		break;
	case framework::gpu::object_type::render_pass:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_RENDER_PASS;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::render_pass *>(name_info->object)->get_vk_render_pass());
		break;
	case framework::gpu::object_type::pipeline:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_PIPELINE;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::pipeline *>(name_info->object)->get_vk_pipeline());
		break;
	case framework::gpu::object_type::descriptor_set_layout:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::descriptor_set_layout *>(name_info->object)->get_vk_descriptor_set_layout());
		break;
	case framework::gpu::object_type::sampler:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_SAMPLER;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::sampler *>(name_info->object)->get_vk_sampler());
		break;
	case framework::gpu::object_type::descriptor_pool:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_DESCRIPTOR_POOL;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::descriptor_pool *>(name_info->object)->get_vk_descriptor_pool());
		break;
	case framework::gpu::object_type::descriptor_set:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_DESCRIPTOR_SET;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::descriptor_set *>(name_info->object)->get_vk_descriptor_set());
		break;
	case framework::gpu::object_type::framebuffer:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_FRAMEBUFFER;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::frame_buffer *>(name_info->object)->get_vk_framebuffer());
		break;
	case framework::gpu::object_type::command_pool:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_COMMAND_POOL;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::command_pool *>(name_info->object)->get_vk_command_pool());
		break;
	case framework::gpu::object_type::surface:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_SURFACE_KHR;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::surface *>(name_info->object)->get_vk_surface());
		break;
	case framework::gpu::object_type::swapchain:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_SWAPCHAIN_KHR;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::swap_chain *>(name_info->object)->get_vk_swapchain());
		break;
	case framework::gpu::object_type::acceleration_structure:
		vk_object_type = ::VkObjectType::VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR;
		vk_object = reinterpret_cast<::std::uint64_t>(static_cast<::framework::gpu::vulkan::acceleration_structure *>(name_info->object)->get_vk_acceleration_structure());
		break;
	default:
		return ::framework::gpu::result::success;
		break;
	}

	::VkDebugUtilsObjectNameInfoEXT vk_debug_utils_object_name_info;
	vk_debug_utils_object_name_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
	vk_debug_utils_object_name_info.pNext = nullptr;
	vk_debug_utils_object_name_info.objectType = vk_object_type;
	vk_debug_utils_object_name_info.objectHandle = vk_object;
	vk_debug_utils_object_name_info.pObjectName = name_info->object_name;

	vk_result = this->dynamic_library.vkSetDebugUtilsObjectNameEXT(this->vk_device, &vk_debug_utils_object_name_info);

	return ::framework::gpu::vulkan::get_result(vk_result);
}