#include "gpu/vulkan/core.hpp"

::framework::gpu::vulkan::image_view::image_view(::framework::gpu::vulkan::device * device, ::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::VkResult vk_result;

	::VkImageViewCreateFlags vk_image_view_create_flags = 0;
	if (!!(create_info->flags & ::framework::gpu::image_view_create_flags::fragment_density_map_dynamic_bit))
		vk_image_view_create_flags |= VkImageViewCreateFlagBits::VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT;

	::VkImageViewCreateInfo vk_image_view_create_info;
	vk_image_view_create_info.sType = ::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	vk_image_view_create_info.pNext = nullptr;
	vk_image_view_create_info.flags = vk_image_view_create_flags;
	vk_image_view_create_info.image = static_cast<::framework::gpu::vulkan::image *>(create_info->image)->get_vk_image();
	vk_image_view_create_info.viewType = ::framework::gpu::vulkan::get_vk_image_view_type(create_info->view_type);
	vk_image_view_create_info.format = ::framework::gpu::vulkan::get_vk_format(create_info->format);
	vk_image_view_create_info.components.r = ::framework::gpu::vulkan::get_vk_component_swizzle(create_info->components.r);
	vk_image_view_create_info.components.g = ::framework::gpu::vulkan::get_vk_component_swizzle(create_info->components.g);
	vk_image_view_create_info.components.b = ::framework::gpu::vulkan::get_vk_component_swizzle(create_info->components.b);
	vk_image_view_create_info.components.a = ::framework::gpu::vulkan::get_vk_component_swizzle(create_info->components.a);
	vk_image_view_create_info.subresourceRange = ::framework::gpu::vulkan::get_vk_image_subresource_range(create_info->subresource_range);

	vk_result = ::vkCreateImageView(device->get_vk_device(), &vk_image_view_create_info, nullptr, &this->vk_image_view);

	result = ::framework::gpu::vulkan::get_result(vk_result);
}