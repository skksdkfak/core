#pragma once

#include "gpu/surface.h"

namespace framework::gpu::vulkan
{
	class surface final : public ::framework::gpu::surface
	{
	public:
		surface(::framework::gpu::vulkan::instance * instance, ::framework::gpu::surface_create_info const * create_info);

		::VkSurfaceKHR get_vk_surface() const { return this->vk_surface; }

	private:
		friend class ::framework::gpu::vulkan::instance;

		::VkSurfaceKHR vk_surface;
	};
}