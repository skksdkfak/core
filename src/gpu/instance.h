#pragma once

namespace framework::gpu
{
	class instance
	{
	public:
		static ::framework::gpu::result create_instance(::framework::gpu::api_type api_type, ::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance);

		static void destroy_instance(::framework::gpu::api_type api_type, ::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator);

		virtual void enumerate_physical_devices(::std::uint32_t * physical_device_count, ::framework::gpu::physical_device ** physical_devices) = 0;

		virtual void create_surface(::framework::gpu::surface_create_info const * create_info, ::framework::gpu::surface ** surface) = 0;

		virtual void destroy_surface(::framework::gpu::surface * surface) = 0;
	};
}