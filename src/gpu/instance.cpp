#include "gpu/vulkan/core.hpp"
#if defined(_WIN32)
#include "gpu/d3d12/core.hpp"
#endif

::framework::gpu::result(::framework::gpu::instance::create_instance)(::framework::gpu::api_type api_type, ::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance)
{
	::framework::gpu::result result;

	switch (api_type)
	{
	case ::framework::gpu::api_type::vulkan:
		result = ::framework::gpu::vulkan::instance::create_instance(create_info, allocator, instance);
		break;
#if defined(_WIN32)
	case ::framework::gpu::api_type::d3d12:
		result = ::framework::gpu::d3d12::instance::create_instance(create_info, allocator, instance);
		break;
#endif
	default:
		result = ::framework::gpu::result::error_initialization_failed;
		break;
	}

	return result;
}

void ::framework::gpu::instance::destroy_instance(::framework::gpu::api_type api_type, ::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator)
{
	switch (api_type)
	{
	case ::framework::gpu::api_type::vulkan:
		::framework::gpu::vulkan::instance::destroy_instance(instance, allocator);
		break;
#if defined(_WIN32)
	case ::framework::gpu::api_type::d3d12:
		::framework::gpu::d3d12::instance::destroy_instance(instance, allocator);
		break;
#endif
	}
}