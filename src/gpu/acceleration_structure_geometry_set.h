#pragma once

namespace framework::gpu
{
	class acceleration_structure_geometry_set
	{
	public:
		virtual void write_data(::framework::gpu::write_acceleration_structure_geometry const & write_info) = 0;
	};
}