#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::buffer_view::buffer_view(::framework::gpu::d3d12::device * device, ::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::result & result) :
	buffer(static_cast<::framework::gpu::d3d12::buffer *>(create_info->buffer))
{
	::DXGI_FORMAT const dxgi_format = ::framework::gpu::d3d12::get_dxgi_format(create_info->format);
	::std::size_t const bytes_per_pixel = ::framework::gpu::d3d12::bits_per_pixel(dxgi_format) / 8;
	::framework::gpu::buffer_usage_flags const & usage = static_cast<::framework::gpu::d3d12::buffer * const>(create_info->buffer)->get_usage();

	if (!!(usage & ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit))
	{
		this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_BUFFER;
		this->d3d12_shader_resource_view_desc.Format = dxgi_format;
		this->d3d12_shader_resource_view_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		this->d3d12_shader_resource_view_desc.Buffer.FirstElement = static_cast<::UINT64>((static_cast<class ::framework::gpu::d3d12::buffer *>(create_info->buffer)->get_device_memory_offset() + create_info->offset) / bytes_per_pixel);
		this->d3d12_shader_resource_view_desc.Buffer.NumElements = static_cast<::UINT>((create_info->range == ::framework::gpu::whole_size ? static_cast<class ::framework::gpu::d3d12::buffer *>(create_info->buffer)->get_size() : create_info->range) / bytes_per_pixel);
		this->d3d12_shader_resource_view_desc.Buffer.StructureByteStride = 0;
		this->d3d12_shader_resource_view_desc.Buffer.Flags = D3D12_BUFFER_SRV_FLAGS::D3D12_BUFFER_SRV_FLAG_NONE;
	}

	if (!!(usage & ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit))
	{
		this->d3d12_unordered_access_view_desc.ViewDimension = ::D3D12_UAV_DIMENSION::D3D12_UAV_DIMENSION_BUFFER;
		this->d3d12_unordered_access_view_desc.Format = dxgi_format;
		this->d3d12_unordered_access_view_desc.Buffer.CounterOffsetInBytes = 0;
		this->d3d12_unordered_access_view_desc.Buffer.FirstElement = static_cast<::UINT64>(create_info->offset / bytes_per_pixel);
		this->d3d12_unordered_access_view_desc.Buffer.NumElements = static_cast<::UINT>((create_info->range == ::framework::gpu::whole_size ? static_cast<class ::framework::gpu::d3d12::buffer *>(create_info->buffer)->get_size() : create_info->range) / bytes_per_pixel);
		this->d3d12_unordered_access_view_desc.Buffer.StructureByteStride = 0;
		this->d3d12_unordered_access_view_desc.Buffer.Flags = ::D3D12_BUFFER_UAV_FLAGS::D3D12_BUFFER_UAV_FLAG_NONE;
	}

	result = ::framework::gpu::result::success;
}