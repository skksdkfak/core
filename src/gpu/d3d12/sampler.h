#pragma once

#include "gpu/sampler.h"

namespace framework::gpu::d3d12
{
	class sampler final : public ::framework::gpu::sampler
	{
	public:
		sampler(::framework::gpu::d3d12::device * device, ::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::result & result);

		~sampler();

		::D3D12_SAMPLER_DESC const & get_d3d12_sampler_desc() const { return this->d3d12_sampler_desc; }

		::D3D12_STATIC_BORDER_COLOR get_d3d12_static_border_color() const { return this->d3d12_static_border_color; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::D3D12_SAMPLER_DESC d3d12_sampler_desc;
		::D3D12_STATIC_BORDER_COLOR d3d12_static_border_color;
	};
}