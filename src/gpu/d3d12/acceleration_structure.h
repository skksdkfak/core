#pragma once

#include "gpu/acceleration_structure.h"

namespace framework::gpu::d3d12
{
	class acceleration_structure final : public ::framework::gpu::acceleration_structure
	{
	public:
		acceleration_structure(::framework::gpu::d3d12::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~acceleration_structure();

		::D3D12_GPU_VIRTUAL_ADDRESS get_d3d12_gpu_virtual_address() const { return this->d3d12_gpu_virtual_address; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::D3D12_GPU_VIRTUAL_ADDRESS d3d12_gpu_virtual_address;
	};
}