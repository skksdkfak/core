#pragma once

#include "gpu/swap_chain.h"

namespace framework::gpu::d3d12
{
	class swap_chain final : public ::framework::gpu::swap_chain
	{
	public:
		swap_chain(::framework::gpu::d3d12::device * device, ::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::framework::gpu::result acquire_next_image(::framework::gpu::acquire_next_image_info const * acquire_info, ::std::uint32_t * image_index) const;

		::framework::gpu::result present() const;

		void get_images(::std::uint32_t * image_count, class ::framework::gpu::image ** images) const;

		::IDXGISwapChain3 * get_dxgi_swap_chain() const { return this->dxgi_swap_chain; }

	private:
		friend class ::framework::gpu::d3d12::device;

		struct images_deleter
		{
			::framework::gpu::d3d12::swap_chain const & swap_chain;

			void operator()(::framework::gpu::d3d12::image ** images) const noexcept
			{
				if (images)
				{
					for (::std::uint32_t i = 0; i < this->swap_chain.image_count; i++)
					{
						DELETE(images[i], this->swap_chain.allocator);
					}
				}
			}
		};

		::framework::gpu::allocation_callbacks const allocator;
		::framework::gpu::d3d12::device * device;
		::std::unique_ptr<::framework::gpu::d3d12::image * [], ::framework::gpu::d3d12::swap_chain::images_deleter> images;
		::std::uint32_t image_count;
		::IDXGISwapChain3 * dxgi_swap_chain;
		::framework::platform::window * window;
		::HANDLE frame_latency_waitable_object;
		::framework::gpu::extent_2d image_extent;
	};
}