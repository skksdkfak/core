#include "gpu/d3d12/core.hpp"
#include <codecvt>
#include <locale>
#include <string>
#include <type_traits>
#include <vector>

::framework::gpu::d3d12::device::device(::framework::gpu::d3d12::physical_device * physical_device, ::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	physical_device(physical_device)
{
	result = ::framework::gpu::d3d12::throw_result(::D3D12CreateDevice(physical_device->get_dxgi_adapter(), ::D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_2, IID_PPV_ARGS(&this->d3d12_device)));

	if (create_info->queue_create_info_count)
	{
		for (::std::uint32_t i = 0; i < create_info->queue_create_info_count; i++)
		{
			auto const & queue_create_info = create_info->queue_create_infos[i];

			if (this->queues[queue_create_info.queue_family_index])
			{
				continue;
			}

			this->queue_count[queue_create_info.queue_family_index] = queue_create_info.queue_count;
			this->queues[queue_create_info.queue_family_index] = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::queue>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, queue_create_info.queue_count);

			for (::std::uint32_t j = 0; j < queue_create_info.queue_count; j++)
			{
				this->queues[queue_create_info.queue_family_index][j] = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::queue>(this->allocator, ::framework::gpu::system_allocation_scope::object, this, queue_create_info.queue_family_index, j, queue_create_info.queue_priorities[j], result);
			}
		}
	}

	{
		::D3D12_INDIRECT_ARGUMENT_DESC d3d12_indirect_argument_desc;
		d3d12_indirect_argument_desc.Type = ::D3D12_INDIRECT_ARGUMENT_TYPE::D3D12_INDIRECT_ARGUMENT_TYPE_DRAW;

		::D3D12_COMMAND_SIGNATURE_DESC d3d12_command_signature_desc;
		d3d12_command_signature_desc.ByteStride = sizeof(::D3D12_DRAW_ARGUMENTS);
		d3d12_command_signature_desc.NumArgumentDescs = 1;
		d3d12_command_signature_desc.pArgumentDescs = &d3d12_indirect_argument_desc;
		d3d12_command_signature_desc.NodeMask = 1;
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_device->CreateCommandSignature(&d3d12_command_signature_desc, nullptr, IID_PPV_ARGS(&this->draw_indirect_command_signature)));
	}

	{
		::D3D12_INDIRECT_ARGUMENT_DESC d3d12_indirect_argument_desc;
		d3d12_indirect_argument_desc.Type = ::D3D12_INDIRECT_ARGUMENT_TYPE::D3D12_INDIRECT_ARGUMENT_TYPE_DRAW_INDEXED;

		::D3D12_COMMAND_SIGNATURE_DESC d3d12_command_signature_desc;
		d3d12_command_signature_desc.ByteStride = sizeof(::D3D12_DRAW_INDEXED_ARGUMENTS);
		d3d12_command_signature_desc.NumArgumentDescs = 1;
		d3d12_command_signature_desc.pArgumentDescs = &d3d12_indirect_argument_desc;
		d3d12_command_signature_desc.NodeMask = 1;
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_device->CreateCommandSignature(&d3d12_command_signature_desc, nullptr, IID_PPV_ARGS(&this->draw_indexed_indirect_command_signature)));
	}

	{
		::D3D12_INDIRECT_ARGUMENT_DESC d3d12_indirect_argument_desc;
		d3d12_indirect_argument_desc.Type = ::D3D12_INDIRECT_ARGUMENT_TYPE::D3D12_INDIRECT_ARGUMENT_TYPE_DISPATCH;

		::D3D12_COMMAND_SIGNATURE_DESC d3d12_command_signature_desc;
		d3d12_command_signature_desc.ByteStride = sizeof(::D3D12_DISPATCH_ARGUMENTS);
		d3d12_command_signature_desc.NumArgumentDescs = 1;
		d3d12_command_signature_desc.pArgumentDescs = &d3d12_indirect_argument_desc;
		d3d12_command_signature_desc.NodeMask = 1;
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_device->CreateCommandSignature(&d3d12_command_signature_desc, nullptr, IID_PPV_ARGS(&this->dispatch_indirect_command_signature)));
	}
}

::framework::gpu::d3d12::device::~device()
{
	if (this->d3d12_device)
	{
		this->d3d12_device->Release();
	}
}

void ::framework::gpu::d3d12::device::get_queue(::std::uint32_t queue_family_index, ::std::uint32_t queue_index, ::framework::gpu::queue ** queue)
{
	*queue = this->queues[queue_family_index][queue_index].get();
}

::framework::gpu::result(::framework::gpu::d3d12::device::wait_idle)()
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	for (::std::uint32_t i = 0; i < ::framework::gpu::d3d12::queue_family_count; i++)
	{
		for (::std::uint32_t j = 0; j < this->queue_count[i]; j++)
		{
			result = this->queues[i][j]->wait_idle();

			if (result != ::framework::gpu::result::success)
			{
				return result;
			}
		}
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::allocate_memory)(::framework::gpu::memory_allocate_info const * allocate_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device_memory ** device_memory)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*device_memory = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::device_memory>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, allocate_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::free_memory(::framework::gpu::device_memory * device_memory, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::device_memory *>(device_memory), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::map_memory)(::framework::gpu::device_memory * memory, ::framework::gpu::device_size offset, ::framework::gpu::device_size size, ::framework::gpu::memory_map_flags flags, void ** data)
{
	::HRESULT hresult;

	void * mapped_memory;

	::D3D12_RANGE read_d3d12_range;
	read_d3d12_range.Begin = 0;
	read_d3d12_range.End = 0;

	hresult = static_cast<::framework::gpu::d3d12::device_memory *>(memory)->d3d12_committed_resource->Map(0, &read_d3d12_range, &mapped_memory);

	*data = static_cast<::std::byte *>(mapped_memory) + offset;

	return ::framework::gpu::d3d12::get_result(hresult);
}

void ::framework::gpu::d3d12::device::unmap_memory(::framework::gpu::device_memory * memory)
{
	::D3D12_RANGE written_d3d12_range;
	written_d3d12_range.Begin = 0;
	written_d3d12_range.End = 0;
	static_cast<::framework::gpu::d3d12::device_memory *>(memory)->d3d12_committed_resource->Unmap(0, &written_d3d12_range);
}

::framework::gpu::result(::framework::gpu::d3d12::device::flush_mapped_memory_ranges)(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		for (::std::uint32_t i = 0; i < memory_range_count; i++)
		{
			::D3D12_RANGE read_d3d12_range;
			read_d3d12_range.Begin = 0;
			read_d3d12_range.End = 0;

			::D3D12_RANGE written_d3d12_range;
			written_d3d12_range.Begin = memory_ranges[i].offset;
			written_d3d12_range.End = memory_ranges[i].offset + memory_ranges[i].size;

			::HRESULT hresult = static_cast<::framework::gpu::d3d12::device_memory *>(memory_ranges[i].memory)->d3d12_committed_resource->Map(0, &read_d3d12_range, nullptr);
			static_cast<::framework::gpu::d3d12::device_memory *>(memory_ranges[i].memory)->d3d12_committed_resource->Unmap(0, &written_d3d12_range);

			result = ::framework::gpu::d3d12::throw_result(hresult);
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::invalidate_mapped_memory_ranges)(::std::uint32_t memory_range_count, ::framework::gpu::mapped_memory_range const * memory_ranges)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		for (::std::uint32_t i = 0; i < memory_range_count; i++)
		{
			::D3D12_RANGE read_d3d12_range;
			read_d3d12_range.Begin = memory_ranges[i].offset;
			read_d3d12_range.End = memory_ranges[i].offset + memory_ranges[i].size;

			::D3D12_RANGE written_d3d12_range;
			written_d3d12_range.Begin = 0;
			written_d3d12_range.End = 0;

			::HRESULT hresult = static_cast<::framework::gpu::d3d12::device_memory *>(memory_ranges[i].memory)->d3d12_committed_resource->Map(0, &read_d3d12_range, nullptr);
			static_cast<::framework::gpu::d3d12::device_memory *>(memory_ranges[i].memory)->d3d12_committed_resource->Unmap(0, &written_d3d12_range);

			result = ::framework::gpu::d3d12::throw_result(hresult);
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::bind_buffer_memory)(::std::uint32_t bind_info_count, ::framework::gpu::bind_buffer_memory_info const * bind_infos)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	for (::std::uint32_t i = 0; i < bind_info_count; i++)
	{
		if (static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->is_dedicated)
		{
			static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->d3d12_resource = static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->d3d12_committed_resource;
			static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->device_memory_offset = bind_infos[i].memory_offset;
			static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->is_dedicated = true;
			result = ::framework::gpu::d3d12::throw_result(static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->set_name(static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->name.get()));
		}
		else
		{
			result = ::framework::gpu::d3d12::throw_result(this->d3d12_device->CreatePlacedResource(
				static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->d3d12_heap,
				bind_infos[i].memory_offset,
				&static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->d3d12_resource_desc,
				static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->initial_state,
				nullptr,
				IID_PPV_ARGS(&static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->d3d12_resource)
			));
			result = ::framework::gpu::d3d12::throw_result(static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->set_name(static_cast<::framework::gpu::d3d12::buffer *>(bind_infos[i].buffer)->name.get()));
		}
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::bind_image_memory)(::std::uint32_t bind_info_count, ::framework::gpu::bind_image_memory_info const * bind_infos)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		for (::std::uint32_t i = 0; i < bind_info_count; i++)
		{
			if (static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->is_dedicated)
			{
				static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->d3d12_resource = static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->d3d12_committed_resource;
				static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->is_dedicated = true;
			}
			else
			{
				result = ::framework::gpu::d3d12::throw_result(this->d3d12_device->CreatePlacedResource(
					static_cast<::framework::gpu::d3d12::device_memory *>(bind_infos[i].memory)->d3d12_heap,
					bind_infos[i].memory_offset,
					&static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->d3d12_resource_desc,
					static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->initial_state,
					static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->optimized_clear_value,
					IID_PPV_ARGS(&static_cast<::framework::gpu::d3d12::image *>(bind_infos[i].image)->d3d12_resource)
				));
			}
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::get_buffer_memory_requirements(::framework::gpu::buffer_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements)
{
	::D3D12_RESOURCE_ALLOCATION_INFO d3d12_resource_allocation_info = this->d3d12_device->GetResourceAllocationInfo(0, 1, &static_cast<::framework::gpu::d3d12::buffer const *>(info->buffer)->d3d12_resource_desc);
	::std::uint32_t memory_type_bits = 1 << 3 | 1 << 4 | 1 << 5;

	memory_requirements->size = d3d12_resource_allocation_info.SizeInBytes;
	memory_requirements->alignment = d3d12_resource_allocation_info.Alignment;
	memory_requirements->memory_type_bits = memory_type_bits;
	memory_requirements->prefers_dedicated_allocation = static_cast<::framework::gpu::d3d12::buffer const *>(info->buffer)->is_dedicated;
	memory_requirements->requires_dedicated_allocation = static_cast<::framework::gpu::d3d12::buffer const *>(info->buffer)->is_dedicated;
}

void ::framework::gpu::d3d12::device::get_image_memory_requirements(::framework::gpu::image_memory_requirements_info const * info, ::framework::gpu::memory_requirements * memory_requirements)
{
	::D3D12_RESOURCE_DESC const & d3d12_resource_desc = static_cast<::framework::gpu::d3d12::image const *>(info->image)->get_d3d12_resource_desc();
	::D3D12_RESOURCE_ALLOCATION_INFO d3d12_resource_allocation_info = this->d3d12_device->GetResourceAllocationInfo(0, 1, &d3d12_resource_desc);
	::std::uint32_t memory_type_bits;
	if (d3d12_resource_desc.Flags & ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET || d3d12_resource_desc.Flags & ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL)
		memory_type_bits = 1 << 9 | 1 << 10 | 1 << 11;
	else
		memory_type_bits = 1 << 6 | 1 << 7 | 1 << 8;

	memory_requirements->size = d3d12_resource_allocation_info.SizeInBytes;
	memory_requirements->alignment = d3d12_resource_allocation_info.Alignment;
	memory_requirements->memory_type_bits = memory_type_bits;
	memory_requirements->prefers_dedicated_allocation = static_cast<::framework::gpu::d3d12::image const *>(info->image)->is_dedicated;
	memory_requirements->requires_dedicated_allocation = static_cast<::framework::gpu::d3d12::image const *>(info->image)->is_dedicated;
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_fence)(::framework::gpu::fence_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::fence ** fence)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*fence = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::fence>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_fence(::framework::gpu::fence * fence, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::fence *>(fence), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::reset_fences)(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	for (::std::uint32_t i = 0; i < fence_count; i++)
	{
		assert(static_cast<::framework::gpu::d3d12::fence *>(fences[i])->get_d3d12_fence()->GetCompletedValue() == static_cast<::framework::gpu::d3d12::fence *>(fences[i])->get_fence_value());
		static_cast<::framework::gpu::d3d12::fence *>(fences[i])->reset();
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::wait_for_fences)(::std::uint32_t fence_count, ::framework::gpu::fence * const * fences, bool wait_all, ::std::uint64_t timeout)
{
	::framework::gpu::result result;

	try
	{
		::D3D12_MULTIPLE_FENCE_WAIT_FLAGS d3d12_multiple_fence_wait_flags = wait_all ? ::D3D12_MULTIPLE_FENCE_WAIT_FLAGS::D3D12_MULTIPLE_FENCE_WAIT_FLAG_ALL : ::D3D12_MULTIPLE_FENCE_WAIT_FLAGS::D3D12_MULTIPLE_FENCE_WAIT_FLAG_ANY;

		::framework::gpu::common::unique_ptr<::ID3D12Fence * []> d3d12_fences = ::framework::gpu::common::make_unique<::ID3D12Fence * []>(this->allocator, ::framework::gpu::system_allocation_scope::command, fence_count);
		::framework::gpu::common::unique_ptr<::UINT64[]> fences_values = ::framework::gpu::common::make_unique<::UINT64[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, fence_count);

		for (::std::uint32_t i = 0; i < fence_count; i++)
		{
			d3d12_fences[i] = static_cast<::framework::gpu::d3d12::fence *>(fences[i])->get_d3d12_fence();
			fences_values[i] = static_cast<::framework::gpu::d3d12::fence *>(fences[i])->get_fence_value();
		}

		::HANDLE const event_handle = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		if (event_handle == NULL)
		{
			return ::framework::gpu::d3d12::get_result(::HRESULT_FROM_WIN32(GetLastError()));
		}
		::HRESULT hresult = this->d3d12_device->SetEventOnMultipleFenceCompletion(d3d12_fences.get(), fences_values.get(), fence_count, d3d12_multiple_fence_wait_flags, event_handle);
		if (SUCCEEDED(hresult))
		{
			hresult = ::WaitForSingleObject(event_handle, static_cast<::DWORD>(timeout / 1000000ull));
		}
		::CloseHandle(event_handle);
		result = ::framework::gpu::d3d12::get_result(hresult);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_semaphore)(::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::semaphore ** semaphore)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*semaphore = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::semaphore>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_semaphore(::framework::gpu::semaphore * semaphore, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::semaphore *>(semaphore), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::get_semaphore_counter_value)(::framework::gpu::semaphore * semaphore, ::std::uint64_t * value)
{
	*value = static_cast<::framework::gpu::d3d12::semaphore *>(semaphore)->get_d3d12_fence()->GetCompletedValue();

	return ::framework::gpu::result::success;
}

::framework::gpu::result(::framework::gpu::d3d12::device::wait_semaphores)(::framework::gpu::semaphore_wait_info const * wait_info, ::std::uint64_t timeout)
{
	::framework::gpu::result result;

	try
	{
		::D3D12_MULTIPLE_FENCE_WAIT_FLAGS d3d12_multiple_fence_wait_flags = ::D3D12_MULTIPLE_FENCE_WAIT_FLAGS::D3D12_MULTIPLE_FENCE_WAIT_FLAG_ALL;
		if (!!(wait_info->flags & ::framework::gpu::semaphore_wait_flags::any_bit))
			d3d12_multiple_fence_wait_flags = ::D3D12_MULTIPLE_FENCE_WAIT_FLAGS::D3D12_MULTIPLE_FENCE_WAIT_FLAG_ANY;

		::framework::gpu::common::unique_ptr<::ID3D12Fence * []> d3d12_fences = ::framework::gpu::common::make_unique<::ID3D12Fence * []>(this->allocator, ::framework::gpu::system_allocation_scope::command, wait_info->semaphore_count);

		for (::std::uint32_t i = 0; i < wait_info->semaphore_count; i++)
		{
			d3d12_fences[i] = static_cast<::framework::gpu::d3d12::semaphore *>(wait_info->semaphores[i])->get_d3d12_fence();
		}

		::HANDLE const event_handle = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		if (event_handle == NULL)
		{
			return ::framework::gpu::d3d12::get_result(::HRESULT_FROM_WIN32(GetLastError()));
		}
		::HRESULT hresult = this->d3d12_device->SetEventOnMultipleFenceCompletion(d3d12_fences.get(), wait_info->values, wait_info->semaphore_count, d3d12_multiple_fence_wait_flags, event_handle);
		if (SUCCEEDED(hresult))
		{
			hresult = ::WaitForSingleObject(event_handle, static_cast<::DWORD>(timeout / 1000000ull));
		}
		::CloseHandle(event_handle);
		result = ::framework::gpu::d3d12::get_result(hresult);
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::signal_semaphore)(::framework::gpu::semaphore_signal_info const * signal_info)
{
	::HRESULT hresult = static_cast<::framework::gpu::d3d12::semaphore *>(signal_info->semaphore)->get_d3d12_fence()->Signal(signal_info->value);

	return ::framework::gpu::d3d12::get_result(hresult);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_query_pool)(::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::query_pool ** query_pool)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*query_pool = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::query_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_query_pool(::framework::gpu::query_pool * query_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	DELETE(static_cast<::framework::gpu::d3d12::query_pool *>(query_pool), selected_allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::get_query_pool_results)(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::std::size_t data_size, void * data, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags)
{
	return ::framework::gpu::result::error_feature_not_present;
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_buffer)(::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer ** buffer)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*buffer = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::buffer>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_buffer(::framework::gpu::buffer * buffer, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::buffer *>(buffer), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_buffer_view)(::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::buffer_view ** buffer_view)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*buffer_view = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::buffer_view>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_buffer_view(::framework::gpu::buffer_view * buffer_view, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::buffer_view *>(buffer_view), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_image)(::framework::gpu::image_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image ** image)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::image>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_image)(::framework::gpu::image_create_info const * create_info, ::ID3D12Resource * base_resource, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::d3d12::image ** image)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::image>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, base_resource, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_image(::framework::gpu::image * image, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::image *>(image), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_image_view)(const image_view_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::image_view ** image_view)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*image_view = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::image_view>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_image_view(::framework::gpu::image_view * image_view, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::image_view *>(image_view), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_shader_module)(::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::shader_module ** shader_module)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*shader_module = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::shader_module>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_shader_module(::framework::gpu::shader_module * shader_module, ::framework::gpu::allocation_callbacks const * allocator)
{
	if (!shader_module)
	{
		return;
	}

	DELETE(static_cast<::framework::gpu::d3d12::shader_module *>(shader_module), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_graphics_pipelines)(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::graphics_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result = ::framework::gpu::result::success, tmp_result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

		for (::std::uint32_t i = 0; i < create_info_count; i++)
		{
			pipelines[i] = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::graphics_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[i], selected_allocator, result).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_compute_pipelines)(::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::compute_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result = ::framework::gpu::result::success, tmp_result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

		for (::std::uint32_t i = 0; i < create_info_count; i++)
		{
			pipelines[i] = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::compute_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[i], selected_allocator, result).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_pipeline(::framework::gpu::pipeline * pipeline, ::framework::gpu::allocation_callbacks const * allocator)
{
	if (!pipeline)
	{
		return;
	}

	DELETE(static_cast<::framework::gpu::d3d12::pipeline *>(pipeline), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_pipeline_cache)(::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_cache ** pipeline_cache)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*pipeline_cache = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::pipeline_cache>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_pipeline_cache(::framework::gpu::pipeline_cache * pipeline_cache, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::pipeline_cache *>(pipeline_cache), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_pipeline_layout)(const pipeline_layout_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline_layout ** pipeline_layout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*pipeline_layout = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::pipeline_layout>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_pipeline_layout(::framework::gpu::pipeline_layout * pipeline_layout, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::pipeline_layout *>(pipeline_layout), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_sampler)(const sampler_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::sampler ** sampler)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*sampler = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::sampler>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_sampler(::framework::gpu::sampler * sampler, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::sampler *>(sampler), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_descriptor_set_layout)(::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_set_layout ** descriptor_set_layout)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*descriptor_set_layout = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::descriptor_set_layout>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_descriptor_set_layout(::framework::gpu::descriptor_set_layout * descriptor_set_layout, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::descriptor_set_layout *>(descriptor_set_layout), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_descriptor_pool)(::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::descriptor_pool ** descriptor_pool)
{
	::framework::gpu::result result;
	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*descriptor_pool = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::descriptor_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
	DELETE(static_cast<::framework::gpu::d3d12::descriptor_pool *>(descriptor_pool), selected_allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::allocate_descriptor_sets)(::framework::gpu::descriptor_set_allocate_info const * allocate_info, ::framework::gpu::descriptor_set ** descriptor_sets)
{
	::framework::gpu::result result;

	result = static_cast<::framework::gpu::d3d12::descriptor_pool *>(allocate_info->descriptor_pool)->allocate_descriptor_sets(this, allocate_info, descriptor_sets);

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::device::free_descriptor_sets)(::framework::gpu::descriptor_pool * descriptor_pool, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets)
{
	static_cast<::framework::gpu::d3d12::descriptor_pool *>(descriptor_pool)->free_descriptor_sets(this, descriptor_set_count, descriptor_sets);

	return ::framework::gpu::result::success;
}

void ::framework::gpu::d3d12::device::update_descriptor_sets(::std::uint32_t descriptor_write_count, ::framework::gpu::write_descriptor_set const * descriptor_writes, ::std::uint32_t descriptor_copy_count, ::framework::gpu::copy_descriptor_set const * descriptor_copies)
{
	::UINT const descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES]
	{
		this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
		this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER),
		this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV),
		this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
	};

	for (::std::uint32_t i = 0; i < descriptor_write_count; i++)
	{
		::framework::gpu::d3d12::descriptor_set_layout::binding const & dst_binding = static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_descriptor_set_layout()->get_binding(descriptor_writes[i].dst_binding);
		switch (descriptor_writes[i].descriptor_type)
		{
		case ::framework::gpu::descriptor_type::sampler:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] };
				this->d3d12_device->CreateSampler(&static_cast<::framework::gpu::d3d12::sampler const *>(descriptor_writes[i].image_info[j].sampler)->get_d3d12_sampler_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::combined_image_sampler:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const srv_offset_in_descriptors = dst_binding.combined_image_sampler.offset_from_start_image + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const image_non_shader_visible_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + srv_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				this->d3d12_device->CreateShaderResourceView(static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_image()->get_d3d12_resource(), &static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_d3d12_shader_resource_view_desc(), image_non_shader_visible_descriptor);

				if (descriptor_writes[i].image_info[j].sampler)
				{
					::UINT const sampler_offset_in_descriptors = dst_binding.combined_image_sampler.offset_from_start_sampler + descriptor_writes[i].dst_array_element + j;
					::D3D12_CPU_DESCRIPTOR_HANDLE const sampler_non_shader_visible_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + sampler_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] };
					this->d3d12_device->CreateSampler(&static_cast<::framework::gpu::d3d12::sampler const *>(descriptor_writes[i].image_info[j].sampler)->get_d3d12_sampler_desc(), sampler_non_shader_visible_descriptor);
				}
			}
			break;
		case ::framework::gpu::descriptor_type::storage_image:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				this->d3d12_device->CreateUnorderedAccessView(static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_image()->get_d3d12_resource(), nullptr, &static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_d3d12_unordered_access_view_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::sampled_image:
		case ::framework::gpu::descriptor_type::input_attachment:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				this->d3d12_device->CreateShaderResourceView(static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_image()->get_d3d12_resource(), &static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_d3d12_shader_resource_view_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::uniform_texel_buffer:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				this->d3d12_device->CreateShaderResourceView(static_cast<::framework::gpu::d3d12::buffer_view *>(descriptor_writes[i].texel_buffer_view[j])->get_buffer()->get_d3d12_resource(), &static_cast<::framework::gpu::d3d12::buffer_view *>(descriptor_writes[i].texel_buffer_view[j])->get_d3d12_shader_resource_view_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::storage_texel_buffer:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				this->d3d12_device->CreateUnorderedAccessView(static_cast<::framework::gpu::d3d12::buffer_view *>(descriptor_writes[i].texel_buffer_view[j])->get_buffer()->get_d3d12_resource(), nullptr, &static_cast<::framework::gpu::d3d12::buffer_view *>(descriptor_writes[i].texel_buffer_view[j])->get_d3d12_unordered_access_view_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::uniform_buffer:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };
				::D3D12_CONSTANT_BUFFER_VIEW_DESC d3d12_constant_buffer_view_desc;
				d3d12_constant_buffer_view_desc.BufferLocation = static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_d3d12_resource()->GetGPUVirtualAddress() + static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_device_memory_offset() + descriptor_writes[i].buffer_info[j].offset;
				d3d12_constant_buffer_view_desc.SizeInBytes = static_cast<::UINT>(descriptor_writes[i].buffer_info[j].range == ::framework::gpu::whole_size ? static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_size() - descriptor_writes[i].buffer_info[j].offset : descriptor_writes[i].buffer_info[j].range);
				this->d3d12_device->CreateConstantBufferView(&d3d12_constant_buffer_view_desc, dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::sampled_buffer:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };

				::D3D12_SHADER_RESOURCE_VIEW_DESC d3d12_shader_resource_view_desc;
				d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_BUFFER;
				d3d12_shader_resource_view_desc.Format = !!(descriptor_writes[i].buffer_info[j].flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit) ? ::DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS : ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
				d3d12_shader_resource_view_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
				d3d12_shader_resource_view_desc.Buffer.FirstElement = (static_cast<::framework::gpu::d3d12::buffer const *>(descriptor_writes[i].buffer_info[j].buffer)->get_device_memory_offset() + descriptor_writes[i].buffer_info[j].offset) / descriptor_writes[i].buffer_info[j].stride;
				d3d12_shader_resource_view_desc.Buffer.NumElements = static_cast<::UINT>((descriptor_writes[i].buffer_info[j].range == ::framework::gpu::whole_size ? static_cast<::framework::gpu::d3d12::buffer const *>(descriptor_writes[i].buffer_info[j].buffer)->get_size() - descriptor_writes[i].buffer_info[j].offset : descriptor_writes[i].buffer_info[j].range) / descriptor_writes[i].buffer_info[j].stride);
				d3d12_shader_resource_view_desc.Buffer.StructureByteStride = !!(descriptor_writes[i].buffer_info[j].flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit) ? 0 : static_cast<::UINT>(descriptor_writes[i].buffer_info[j].stride);
				d3d12_shader_resource_view_desc.Buffer.Flags = ::framework::gpu::d3d12::get_d3d12_buffer_srv_flags(descriptor_writes[i].buffer_info[j].flags);
				this->d3d12_device->CreateShaderResourceView(static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_d3d12_resource(), &d3d12_shader_resource_view_desc, dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::storage_buffer:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };

				::D3D12_UNORDERED_ACCESS_VIEW_DESC d3d12_unordered_access_view_desc;
				d3d12_unordered_access_view_desc.ViewDimension = ::D3D12_UAV_DIMENSION::D3D12_UAV_DIMENSION_BUFFER;
				d3d12_unordered_access_view_desc.Format = !!(descriptor_writes[i].buffer_info[j].flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit) ? ::DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS : ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
				d3d12_unordered_access_view_desc.Buffer.CounterOffsetInBytes = 0;
				d3d12_unordered_access_view_desc.Buffer.FirstElement = descriptor_writes[i].buffer_info[j].offset / descriptor_writes[i].buffer_info[j].stride;
				d3d12_unordered_access_view_desc.Buffer.NumElements = static_cast<::UINT>((descriptor_writes[i].buffer_info[j].range == ::framework::gpu::whole_size ? static_cast<::framework::gpu::d3d12::buffer const *>(descriptor_writes[i].buffer_info[j].buffer)->get_size() - descriptor_writes[i].buffer_info[j].offset : descriptor_writes[i].buffer_info[j].range) / descriptor_writes[i].buffer_info[j].stride);
				d3d12_unordered_access_view_desc.Buffer.StructureByteStride = !!(descriptor_writes[i].buffer_info[j].flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit) ? 0 : static_cast<::UINT>(descriptor_writes[i].buffer_info[j].stride);
				d3d12_unordered_access_view_desc.Buffer.Flags = ::framework::gpu::d3d12::get_d3d12_buffer_uav_flags(descriptor_writes[i].buffer_info[j].flags);
				this->d3d12_device->CreateUnorderedAccessView(static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_d3d12_resource(), nullptr, &d3d12_unordered_access_view_desc, dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
		case ::framework::gpu::descriptor_type::sampled_buffer_dynamic:
		case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->dynamic_root_parameter_addresses[dst_binding.root_parameter.offset_from_start + descriptor_writes[i].dst_array_element + j] = static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_d3d12_resource()->GetGPUVirtualAddress() + descriptor_writes[i].buffer_info[j].offset + static_cast<::framework::gpu::d3d12::buffer *>(descriptor_writes[i].buffer_info[j].buffer)->get_device_memory_offset();
			}
			break;
		case ::framework::gpu::descriptor_type::acceleration_structure:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] };

				::D3D12_SHADER_RESOURCE_VIEW_DESC d3d12_shader_resource_view_desc;
				d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_RAYTRACING_ACCELERATION_STRUCTURE;
				d3d12_shader_resource_view_desc.Format = ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
				d3d12_shader_resource_view_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
				d3d12_shader_resource_view_desc.RaytracingAccelerationStructure.Location = static_cast<::framework::gpu::d3d12::acceleration_structure const *>(descriptor_writes[i].acceleration_structures[j])->get_d3d12_gpu_virtual_address();
				this->d3d12_device->CreateShaderResourceView(nullptr, &d3d12_shader_resource_view_desc, dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::color_attachment:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV] };
				this->d3d12_device->CreateRenderTargetView(static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_image()->get_d3d12_resource(), &static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_d3d12_render_target_view_desc(), dest_descriptor);
			}
			break;
		case ::framework::gpu::descriptor_type::depth_stencil_attachment:
			for (::std::uint32_t j = 0; j < descriptor_writes[i].descriptor_count; j++)
			{
				::UINT const offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_writes[i].dst_array_element + j;
				::D3D12_CPU_DESCRIPTOR_HANDLE const dest_descriptor{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_writes[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV).ptr + offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV] };
				this->d3d12_device->CreateDepthStencilView(static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_image()->get_d3d12_resource(), &static_cast<::framework::gpu::d3d12::image_view *>(descriptor_writes[i].image_info[j].image_view)->get_d3d12_depth_stencil_view_desc(), dest_descriptor);
			}
			break;
		}
	}

	::std::vector<::D3D12_CPU_DESCRIPTOR_HANDLE> dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
	::std::vector<::D3D12_CPU_DESCRIPTOR_HANDLE> src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
	::std::vector<::UINT> descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];

	for (::std::uint32_t i = 0; i < descriptor_copy_count; i++)
	{
		auto const & dst_binding = static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_descriptor_set_layout()->get_binding(descriptor_copies[i].dst_binding);
		auto const & src_binding = static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_descriptor_set_layout()->get_binding(descriptor_copies[i].src_binding);

		if (src_binding.descriptor_type == ::framework::gpu::descriptor_type::storage_image || src_binding.descriptor_type == ::framework::gpu::descriptor_type::uniform_texel_buffer || src_binding.descriptor_type == ::framework::gpu::descriptor_type::storage_texel_buffer ||
			src_binding.descriptor_type == ::framework::gpu::descriptor_type::uniform_buffer || src_binding.descriptor_type == ::framework::gpu::descriptor_type::sampled_buffer || src_binding.descriptor_type == ::framework::gpu::descriptor_type::storage_buffer ||
			src_binding.descriptor_type == ::framework::gpu::descriptor_type::acceleration_structure)
		{
			::UINT const dst_offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_copies[i].dst_array_element;

			dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + dst_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] });
			src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + (src_binding.descriptor_range.offset_from_start + descriptor_copies[i].src_array_element) * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] });
			descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].push_back(descriptor_copies[i].descriptor_count);
		}
		else if (src_binding.descriptor_type == ::framework::gpu::descriptor_type::sampler)
		{
			::UINT const dst_offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_copies[i].dst_array_element;

			dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + dst_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] });
			src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + (src_binding.descriptor_range.offset_from_start + descriptor_copies[i].src_array_element) * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] });
			descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].push_back(descriptor_copies[i].descriptor_count);
		}
		else if (src_binding.descriptor_type == ::framework::gpu::descriptor_type::uniform_buffer_dynamic || src_binding.descriptor_type == ::framework::gpu::descriptor_type::sampled_buffer_dynamic || src_binding.descriptor_type == ::framework::gpu::descriptor_type::storage_buffer_dynamic)
		{
			::std::memcpy(&static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->dynamic_root_parameter_addresses[dst_binding.root_parameter.offset_from_start + descriptor_copies[i].dst_array_element], &static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->dynamic_root_parameter_addresses[src_binding.root_parameter.offset_from_start + descriptor_copies[i].src_array_element], sizeof(::D3D12_GPU_VIRTUAL_ADDRESS) * descriptor_copies[i].descriptor_count);
		}
		else if (src_binding.descriptor_type == ::framework::gpu::descriptor_type::combined_image_sampler)
		{
			::UINT const dst_sampler_offset_in_descriptors = dst_binding.combined_image_sampler.offset_from_start_sampler + descriptor_copies[i].dst_array_element;
			::UINT const dst_srv_offset_in_descriptors = dst_binding.combined_image_sampler.offset_from_start_image + descriptor_copies[i].dst_array_element;

			dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + dst_sampler_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] });
			src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + (src_binding.combined_image_sampler.offset_from_start_sampler + descriptor_copies[i].src_array_element) * this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER) });
			descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].push_back(descriptor_copies[i].descriptor_count);

			dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + dst_srv_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] });
			src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr + (src_binding.combined_image_sampler.offset_from_start_image + descriptor_copies[i].src_array_element) * this->d3d12_device->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV) });
			descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].push_back(descriptor_copies[i].descriptor_count);
		}
		else if (src_binding.descriptor_type == ::framework::gpu::descriptor_type::color_attachment)
		{
			::UINT const dst_offset_in_descriptors = dst_binding.descriptor_range.offset_from_start + descriptor_copies[i].dst_array_element;

			dest_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].dst_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + dst_offset_in_descriptors * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] });
			src_descriptor_range_starts[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].emplace_back(::D3D12_CPU_DESCRIPTOR_HANDLE{ static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_copies[i].src_set)->get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr + (src_binding.descriptor_range.offset_from_start + descriptor_copies[i].src_array_element) * descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] });
			descriptor_range_sizes[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].push_back(descriptor_copies[i].descriptor_count);
		}
	}

	for (::std::underlying_type_t<::D3D12_DESCRIPTOR_HEAP_TYPE> d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; d3d12_descriptor_heap_type++)
	{
		this->d3d12_device->CopyDescriptors(dest_descriptor_range_starts[d3d12_descriptor_heap_type].size(), dest_descriptor_range_starts[d3d12_descriptor_heap_type].data(), descriptor_range_sizes[d3d12_descriptor_heap_type].data(), descriptor_range_sizes[d3d12_descriptor_heap_type].size(), src_descriptor_range_starts[d3d12_descriptor_heap_type].data(), descriptor_range_sizes[d3d12_descriptor_heap_type].data(), static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type));
	}
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_frame_buffer)(const frame_buffer_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::frame_buffer ** frame_buffer)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*frame_buffer = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::frame_buffer>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_frame_buffer(::framework::gpu::frame_buffer * frame_buffer, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::frame_buffer *>(frame_buffer), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_render_pass)(const render_pass_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass ** render_pass)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*render_pass = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_render_pass(::framework::gpu::render_pass * render_pass, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::render_pass *>(render_pass), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_render_pass_state)(::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::render_pass_state ** render_pass_state)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*render_pass_state = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass_state>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_render_pass_state(::framework::gpu::render_pass_state * render_pass_state, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	DELETE(static_cast<::framework::gpu::d3d12::render_pass_state *>(render_pass_state), selected_allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_command_pool)(const command_pool_create_info * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::command_pool ** command_pool)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::command_pool> d3d12_command_pool = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::command_pool>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result);
		*command_pool = d3d12_command_pool.release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

	DELETE(static_cast<::framework::gpu::d3d12::command_pool *>(command_pool), selected_allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::reset_command_pool)(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_reset_flags flags)
{
	return static_cast<::framework::gpu::d3d12::command_pool *>(command_pool)->reset(flags);
}

void ::framework::gpu::d3d12::device::trim_command_pool(::framework::gpu::command_pool * command_pool, ::framework::gpu::command_pool_trim_flags flags)
{
	static_cast<::framework::gpu::d3d12::command_pool *>(command_pool)->trim(flags);
}

::framework::gpu::result(::framework::gpu::d3d12::device::allocate_command_buffers)(::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::command_buffer ** command_buffers)
{
	::framework::gpu::result result;

	result = static_cast<::framework::gpu::d3d12::command_pool *>(allocate_info->command_pool)->allocate_command_buffers(this, allocate_info, command_buffers);

	return result;
}

void ::framework::gpu::d3d12::device::free_command_buffers(::framework::gpu::command_pool * command_pool, ::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers)
{
	static_cast<::framework::gpu::d3d12::command_pool *>(command_pool)->free_command_buffers(this, command_buffer_count, command_buffers);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_swap_chain)(::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::swap_chain ** swap_chain)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*swap_chain = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::swap_chain>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_swap_chain(::framework::gpu::swap_chain * swap_chain, ::framework::gpu::allocation_callbacks const * allocator)
{
	if (!swap_chain)
	{
		return;
	}

	if (static_cast<::framework::gpu::d3d12::swap_chain *>(swap_chain)->dxgi_swap_chain)
		static_cast<::framework::gpu::d3d12::swap_chain *>(swap_chain)->dxgi_swap_chain->Release();
	DELETE(static_cast<::framework::gpu::d3d12::swap_chain *>(swap_chain), allocator ? *allocator : this->allocator);
}

void ::framework::gpu::d3d12::device::get_swap_chain_images(::framework::gpu::swap_chain * swap_chain, ::std::uint32_t * swap_chain_image_count, ::framework::gpu::image ** swap_chain_images)
{
	static_cast<::framework::gpu::d3d12::swap_chain *>(swap_chain)->get_images(swap_chain_image_count, swap_chain_images);
}

::framework::gpu::result(::framework::gpu::d3d12::device::acquire_next_image)(::framework::gpu::acquire_next_image_info const * acquire_info, ::std::uint32_t * image_index)
{
	return static_cast<::framework::gpu::d3d12::swap_chain *>(acquire_info->swap_chain)->acquire_next_image(acquire_info, image_index);
}

::framework::gpu::result framework::gpu::d3d12::device::create_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure_geometry_set ** acceleration_structure_geometry_set)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*acceleration_structure_geometry_set = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::acceleration_structure_geometry_set>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void framework::gpu::d3d12::device::destroy_acceleration_structure_geometry_set(::framework::gpu::acceleration_structure_geometry_set * acceleration_structure_geometry_set, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::acceleration_structure_geometry_set *>(acceleration_structure_geometry_set), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_acceleration_structure)(::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::acceleration_structure ** acceleration_structure)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;
		*acceleration_structure = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::acceleration_structure>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, create_info, selected_allocator, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::device::destroy_acceleration_structure(::framework::gpu::acceleration_structure * acceleration_structure, ::framework::gpu::allocation_callbacks const * allocator)
{
	DELETE(static_cast<::framework::gpu::d3d12::acceleration_structure *>(acceleration_structure), allocator ? *allocator : this->allocator);
}

::framework::gpu::result(::framework::gpu::d3d12::device::build_acceleration_structures)(::framework::gpu::deferred_operation * deferred_operation, ::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos)
{
	return ::framework::gpu::result::error_feature_not_present;
}

::framework::gpu::result(::framework::gpu::d3d12::device::copy_acceleration_structure)(::framework::gpu::copy_acceleration_structure_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::d3d12::device::copy_acceleration_structure_to_memory)(::framework::gpu::copy_acceleration_structure_to_memory_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::d3d12::device::copy_memory_to_acceleration_structure)(::framework::gpu::copy_memory_to_acceleration_structure_info const * info)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::d3d12::device::get_ray_tracing_shader_group_handles)(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data)
{
	for (::std::uint32_t i = 0, group_index = first_group; i < group_count; group_index++, i++)
	{
		void * shader_identifier = static_cast<::framework::gpu::d3d12::ray_tracing_pipeline *>(pipeline)->get_d3d12_state_object_properties()->GetShaderIdentifier(static_cast<::framework::gpu::d3d12::ray_tracing_pipeline *>(pipeline)->get_group_export_name(group_index).c_str());
		::std::memcpy(static_cast<::std::byte *>(data) + i * D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES, shader_identifier, D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES);
	}

	return ::framework::gpu::result::success;
}

::framework::gpu::result(::framework::gpu::d3d12::device::write_acceleration_structures_properties)(::std::uint32_t acceleration_structure_count, ::framework::gpu::acceleration_structure const * acceleration_structures, ::framework::gpu::query_type query_type, ::std::size_t data_size, void * data, ::std::size_t stride)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::d3d12::device::create_ray_tracing_pipelines)(::framework::gpu::deferred_operation * deferred_operation, ::framework::gpu::pipeline_cache * pipeline_cache, ::std::uint32_t create_info_count, ::framework::gpu::ray_tracing_pipeline_create_info const * create_infos, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::pipeline ** pipelines)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		::framework::gpu::allocation_callbacks const & selected_allocator = allocator ? *allocator : this->allocator;

		for (::std::uint32_t i = 0; i < create_info_count; i++)
		{
			pipelines[i] = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::ray_tracing_pipeline>(selected_allocator, ::framework::gpu::system_allocation_scope::object, this, &create_infos[i], selected_allocator, result).release();
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::device_address(::framework::gpu::d3d12::device::get_acceleration_structure_device_address)(::framework::gpu::acceleration_structure_device_address_info const * info)
{
	return static_cast<::framework::gpu::d3d12::acceleration_structure *>(info->acceleration_structure)->get_d3d12_gpu_virtual_address();
}

::framework::gpu::result(::framework::gpu::d3d12::device::get_ray_tracing_capture_replay_shader_group_handles)(::framework::gpu::pipeline * pipeline, ::std::uint32_t first_group, ::std::uint32_t group_count, ::std::size_t data_size, void * data)
{
	return ::framework::gpu::result();
}

::framework::gpu::result(::framework::gpu::d3d12::device::get_device_acceleration_structure_compatibility)(::framework::gpu::acceleration_structure_version const * version)
{
	return ::framework::gpu::result();
}

void ::framework::gpu::d3d12::device::get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type build_type, ::framework::gpu::acceleration_structure_build_geometry_info const * build_info, ::framework::gpu::acceleration_structure_build_sizes_info * size_info)
{
	::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO d3d12_raytracing_acceleration_structure_prebuild_info;

	::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS d3d12_build_raytracing_acceleration_structure_inputs;
	d3d12_build_raytracing_acceleration_structure_inputs.Type = ::framework::gpu::d3d12::get_d3d12_raytracing_acceleration_structure_type(build_info->type);
	d3d12_build_raytracing_acceleration_structure_inputs.Flags = ::framework::gpu::d3d12::get_d3d12_raytracing_acceleration_structure_build_flags(build_info->flags);
	d3d12_build_raytracing_acceleration_structure_inputs.NumDescs = build_info->geometry_count;
	static_cast<::framework::gpu::d3d12::acceleration_structure_geometry_set *>(build_info->geometries)->init_d3d12_build_raytracing_acceleration_structure_inputs(build_info->first_geometry, build_info->geometry_count, d3d12_build_raytracing_acceleration_structure_inputs);
	this->d3d12_device->GetRaytracingAccelerationStructurePrebuildInfo(&d3d12_build_raytracing_acceleration_structure_inputs, &d3d12_raytracing_acceleration_structure_prebuild_info);

	size_info->acceleration_structure_size = d3d12_raytracing_acceleration_structure_prebuild_info.ResultDataMaxSizeInBytes;
	size_info->update_scratch_size = d3d12_raytracing_acceleration_structure_prebuild_info.UpdateScratchDataSizeInBytes;
	size_info->build_scratch_size = d3d12_raytracing_acceleration_structure_prebuild_info.ScratchDataSizeInBytes;
}

::framework::gpu::device_or_host_descriptor_handle(::framework::gpu::d3d12::device::get_descriptor_handle)(::framework::gpu::descriptor_handle_info const * info)
{
	::framework::gpu::d3d12::descriptor_set_layout::binding const & binding_info = static_cast<::framework::gpu::d3d12::descriptor_set *>(info->set)->get_descriptor_set_layout()->get_binding(info->binding);
	::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type;
	::UINT offset_in_descriptors;
	switch (binding_info.descriptor_type)
	{
	case ::framework::gpu::descriptor_type::sampler:
		offset_in_descriptors = binding_info.descriptor_range.offset_from_start;
		d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
		break;
	case ::framework::gpu::descriptor_type::combined_image_sampler:
		if (!(info->flags & ::framework::gpu::descriptor_handle_info_flags::combined_sampler))
		{
			offset_in_descriptors = binding_info.combined_image_sampler.offset_from_start_image;
			d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		}
		else
		{
			offset_in_descriptors = binding_info.combined_image_sampler.offset_from_start_sampler;
			d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
		}
		break;
	case ::framework::gpu::descriptor_type::storage_image:
	case ::framework::gpu::descriptor_type::sampled_image:
	case ::framework::gpu::descriptor_type::input_attachment:
	case ::framework::gpu::descriptor_type::uniform_texel_buffer:
	case ::framework::gpu::descriptor_type::storage_texel_buffer:
	case ::framework::gpu::descriptor_type::uniform_buffer:
	case ::framework::gpu::descriptor_type::sampled_buffer:
	case ::framework::gpu::descriptor_type::storage_buffer:
	case ::framework::gpu::descriptor_type::acceleration_structure:
		offset_in_descriptors = binding_info.descriptor_range.offset_from_start;
		d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		break;
	case ::framework::gpu::descriptor_type::color_attachment:
		offset_in_descriptors = binding_info.descriptor_range.offset_from_start;
		d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		break;
	case ::framework::gpu::descriptor_type::depth_stencil_attachment:
		offset_in_descriptors = binding_info.descriptor_range.offset_from_start;
		d3d12_descriptor_heap_type = ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		break;
	case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
	case ::framework::gpu::descriptor_type::sampled_buffer_dynamic:
	case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
	default:
		return ::framework::gpu::device_or_host_descriptor_handle{};
	}

	typedef ::std::conditional_t<(sizeof(::D3D12_CPU_DESCRIPTOR_HANDLE::ptr) > sizeof(::D3D12_GPU_DESCRIPTOR_HANDLE::ptr)), decltype(::D3D12_CPU_DESCRIPTOR_HANDLE::ptr), decltype(::D3D12_GPU_DESCRIPTOR_HANDLE::ptr)> decriptor_handle_t;
	decriptor_handle_t descriptor_range_start = info->device_descriptor_handle ?
		static_cast<decriptor_handle_t>(static_cast<::framework::gpu::d3d12::descriptor_set *>(info->set)->get_gpu_descriptor_range_start(d3d12_descriptor_heap_type).ptr) :
		static_cast<decriptor_handle_t>(static_cast<::framework::gpu::d3d12::descriptor_set *>(info->set)->get_cpu_descriptor_range_start(d3d12_descriptor_heap_type).ptr);
	return ::framework::gpu::device_or_host_descriptor_handle{ descriptor_range_start + (offset_in_descriptors + info->array_element) * this->d3d12_device->GetDescriptorHandleIncrementSize(d3d12_descriptor_heap_type) };
}

::framework::gpu::device_address(::framework::gpu::d3d12::device::get_buffer_device_address)(::framework::gpu::buffer_device_address_info const * info)
{
	return static_cast<::framework::gpu::d3d12::buffer *>(info->buffer)->get_d3d12_resource()->GetGPUVirtualAddress();
}

::std::uint64_t(::framework::gpu::d3d12::device::get_buffer_opaque_capture_address)(::framework::gpu::buffer_device_address_info const * info)
{
	return ::std::uint64_t();
}

::std::uint64_t(::framework::gpu::d3d12::device::get_device_memory_opaque_capture_address)(::framework::gpu::device_memory_opaque_capture_address_info const * info)
{
	return ::std::uint64_t();
}

::framework::gpu::result framework::gpu::d3d12::device::set_object_name(::framework::gpu::debug_utils_object_name_info const * name_info)
{
	::ID3D12Object * d3d12_object;

	switch (name_info->object_type)
	{
	case framework::gpu::object_type::device:
		d3d12_object = static_cast<::framework::gpu::d3d12::device *>(name_info->object)->get_d3d12_device();
		break;
	case framework::gpu::object_type::queue:
		d3d12_object = static_cast<::framework::gpu::d3d12::queue *>(name_info->object)->get_d3d12_command_queue();
		break;
	case framework::gpu::object_type::semaphore:
		d3d12_object = static_cast<::framework::gpu::d3d12::semaphore *>(name_info->object)->get_d3d12_fence();
		break;
	case framework::gpu::object_type::command_buffer:
		d3d12_object = static_cast<::framework::gpu::d3d12::command_buffer *>(name_info->object)->get_d3d12_graphics_command_list();
		break;
	case framework::gpu::object_type::fence:
		d3d12_object = static_cast<::framework::gpu::d3d12::fence *>(name_info->object)->get_d3d12_fence();
		break;
	case framework::gpu::object_type::device_memory:
		d3d12_object = static_cast<::framework::gpu::d3d12::device_memory *>(name_info->object)->get_d3d12_heap();
		break;
	case framework::gpu::object_type::buffer:
		return ::framework::gpu::d3d12::get_result(static_cast<::framework::gpu::d3d12::buffer *>(name_info->object)->set_name(name_info->object_name));
	case framework::gpu::object_type::image:
		return ::framework::gpu::d3d12::get_result(static_cast<::framework::gpu::d3d12::image *>(name_info->object)->set_name(name_info->object_name));
	case framework::gpu::object_type::query_pool:
		d3d12_object = static_cast<::framework::gpu::d3d12::query_pool *>(name_info->object)->get_d3d12_query_heap();
		break;
	case framework::gpu::object_type::pipeline_cache:
		d3d12_object = static_cast<::framework::gpu::d3d12::pipeline_cache *>(name_info->object)->get_d3d12_pipeline_library();
		break;
	case framework::gpu::object_type::pipeline_layout:
		d3d12_object = static_cast<::framework::gpu::d3d12::pipeline_layout *>(name_info->object)->get_d3d12_root_signature();
		break;
		//case framework::gpu::object_type::pipeline:
		//	d3d12_object = static_cast<::framework::gpu::d3d12::pipeline *>(name_info->object)->get_d3d12_pipeline_state();
		//	break;
	case framework::gpu::object_type::command_pool:
		d3d12_object = static_cast<::framework::gpu::d3d12::command_pool *>(name_info->object)->d3d12_command_allocator;
		break;
	default:
		return ::framework::gpu::result::success;
		break;
	}

	::std::wstring_convert<::std::codecvt_utf8_utf16<wchar_t>> wstring_convert;
	::HRESULT hresult = d3d12_object->SetName(wstring_convert.from_bytes(name_info->object_name).c_str());

	return ::framework::gpu::d3d12::get_result(hresult);
}