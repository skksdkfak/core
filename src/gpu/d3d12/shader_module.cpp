#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::shader_module::shader_module(::framework::gpu::d3d12::device * device, ::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::result & result)
{
	result = ::framework::gpu::d3d12::throw_result(device->get_physical_device()->get_instance()->get_dxc_utils()->CreateBlob(create_info->code, create_info->code_size, CP_ACP, &this->dxc_blob));
}