#include "gpu/d3d12/core.hpp"
#include <platform/win32/window.hpp>

::framework::gpu::d3d12::surface::surface(::framework::gpu::d3d12::instance * instance, ::framework::gpu::surface_create_info const * create_info) :
	window(static_cast<::framework::platform::win32::window *>(create_info->window))
{
}

::framework::gpu::d3d12::surface::~surface()
{
}