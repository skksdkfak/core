#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::acceleration_structure::acceleration_structure(::framework::gpu::d3d12::device * device, ::framework::gpu::acceleration_structure_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	this->d3d12_gpu_virtual_address = static_cast<::framework::gpu::d3d12::buffer const *>(create_info->buffer)->get_d3d12_resource()->GetGPUVirtualAddress() + static_cast<::framework::gpu::d3d12::buffer const *>(create_info->buffer)->get_device_memory_offset() + create_info->offset;

	result = ::framework::gpu::result::success;
}

::framework::gpu::d3d12::acceleration_structure::~acceleration_structure()
{
}