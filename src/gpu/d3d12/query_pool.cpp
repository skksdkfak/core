#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::query_pool::query_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::D3D12_QUERY_HEAP_TYPE d3d12_query_heap_type;
	switch (create_info->query_type)
	{
	case ::framework::gpu::query_type::occlusion:
		d3d12_query_heap_type = ::D3D12_QUERY_HEAP_TYPE::D3D12_QUERY_HEAP_TYPE_OCCLUSION;
		this->d3d12_query_type = ::D3D12_QUERY_TYPE::D3D12_QUERY_TYPE_OCCLUSION;
		break;
	case ::framework::gpu::query_type::pipeline_statistics:
		d3d12_query_heap_type = ::D3D12_QUERY_HEAP_TYPE::D3D12_QUERY_HEAP_TYPE_PIPELINE_STATISTICS;
		this->d3d12_query_type = ::D3D12_QUERY_TYPE::D3D12_QUERY_TYPE_PIPELINE_STATISTICS;
		break;
	case ::framework::gpu::query_type::timestamp:
		d3d12_query_heap_type = ::D3D12_QUERY_HEAP_TYPE::D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
		this->d3d12_query_type = ::D3D12_QUERY_TYPE::D3D12_QUERY_TYPE_TIMESTAMP;
		break;
	case ::framework::gpu::query_type::transform_feedback_stream:
		d3d12_query_heap_type = ::D3D12_QUERY_HEAP_TYPE::D3D12_QUERY_HEAP_TYPE_SO_STATISTICS;
		this->d3d12_query_type = ::D3D12_QUERY_TYPE::D3D12_QUERY_TYPE_SO_STATISTICS_STREAM0;
		break;
	case ::framework::gpu::query_type::performance_query:
	case ::framework::gpu::query_type::acceleration_structure_compacted_size:
	case ::framework::gpu::query_type::acceleration_structure_serialization_size:
	default:
		throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_extension_not_present);
		break;
	}

	::D3D12_QUERY_HEAP_DESC d3d12_query_heap_desc;
	d3d12_query_heap_desc.Type = d3d12_query_heap_type;
	d3d12_query_heap_desc.Count = static_cast<::UINT>(create_info->query_count);
	d3d12_query_heap_desc.NodeMask = 0;
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateQueryHeap(&d3d12_query_heap_desc, IID_PPV_ARGS(&d3d12_query_heap)));
}