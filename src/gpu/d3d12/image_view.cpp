#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::image_view::image_view(::framework::gpu::d3d12::device * device, struct ::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::result & result) :
	image(static_cast<::framework::gpu::d3d12::image *>(create_info->image))
{
	bool const multisample = static_cast<::framework::gpu::d3d12::image *>(create_info->image)->get_samples() > 1;
	::UINT plane_slice;
	if (!!(create_info->subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit))
		plane_slice = 1;
	else
		plane_slice = 0;

	::framework::gpu::image_usage_flags usage = static_cast<::framework::gpu::d3d12::image *>(create_info->image)->get_usage();

	::DXGI_FORMAT const dxgi_format = ::framework::gpu::d3d12::get_dxgi_format(create_info->format);

	if (!!(create_info->subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::color_bit) && !!(usage & (::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::allow_clear_bit)))
	{
		this->d3d12_render_target_view_desc.Format = dxgi_format;
		switch (create_info->view_type)
		{
		case ::framework::gpu::image_view_type::two_dimensional:
			this->d3d12_render_target_view_desc.ViewDimension = ::D3D12_RTV_DIMENSION::D3D12_RTV_DIMENSION_TEXTURE2D;
			this->d3d12_render_target_view_desc.Texture2D.MipSlice = create_info->subresource_range.base_mip_level;
			this->d3d12_render_target_view_desc.Texture2D.PlaneSlice = plane_slice;
			break;
		case ::framework::gpu::image_view_type::two_dimensional_array:
			this->d3d12_render_target_view_desc.ViewDimension = ::D3D12_RTV_DIMENSION::D3D12_RTV_DIMENSION_TEXTURE2DARRAY;
			this->d3d12_render_target_view_desc.Texture2DArray.MipSlice = create_info->subresource_range.base_mip_level;
			this->d3d12_render_target_view_desc.Texture2DArray.FirstArraySlice = create_info->subresource_range.base_array_layer;
			this->d3d12_render_target_view_desc.Texture2DArray.ArraySize = create_info->subresource_range.layer_count;
			this->d3d12_render_target_view_desc.Texture2DArray.PlaneSlice = plane_slice;
			break;
		}
	}

	if (!!(create_info->subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::depth_bit) && !!(usage & ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit))
	{
		this->d3d12_depth_stencil_view_desc.Format = dxgi_format;
		switch (create_info->view_type)
		{
		case ::framework::gpu::image_view_type::one_dimensional:
			break;
		case ::framework::gpu::image_view_type::two_dimensional:
			if (multisample)
			{
				this->d3d12_depth_stencil_view_desc.ViewDimension = D3D12_DSV_DIMENSION::D3D12_DSV_DIMENSION_TEXTURE2DMS;
			}
			else
			{
				this->d3d12_depth_stencil_view_desc.ViewDimension = D3D12_DSV_DIMENSION::D3D12_DSV_DIMENSION_TEXTURE2D;
				this->d3d12_depth_stencil_view_desc.Texture2D.MipSlice = 0;
			}
			break;
		case ::framework::gpu::image_view_type::three_dimensional:
			break;
		case ::framework::gpu::image_view_type::cube:
			break;
		case ::framework::gpu::image_view_type::one_dimensional_array:
			break;
		case ::framework::gpu::image_view_type::two_dimensional_array:
			break;
		case ::framework::gpu::image_view_type::cube_array:
			break;
		}
		this->d3d12_depth_stencil_view_desc.Flags = ::framework::gpu::d3d12::get_d3d12_dsv_flags(create_info->flags);
	}

	if (!!(usage & (::framework::gpu::image_usage_flags::sampled_bit | ::framework::gpu::image_usage_flags::input_attachment_bit)))
	{
		this->d3d12_shader_resource_view_desc.Format = ::framework::gpu::d3d12::get_depth_format(dxgi_format);
		this->d3d12_shader_resource_view_desc.Shader4ComponentMapping = D3D12_ENCODE_SHADER_4_COMPONENT_MAPPING(
			::framework::gpu::d3d12::get_d3d12_shader_component_mapping(create_info->components.r, ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_0),
			::framework::gpu::d3d12::get_d3d12_shader_component_mapping(create_info->components.g, ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_1),
			::framework::gpu::d3d12::get_d3d12_shader_component_mapping(create_info->components.b, ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_2),
			::framework::gpu::d3d12::get_d3d12_shader_component_mapping(create_info->components.a, ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_3)
		);
		switch (create_info->view_type)
		{
		case ::framework::gpu::image_view_type::one_dimensional:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURE1D;
			this->d3d12_shader_resource_view_desc.Texture1D.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.Texture1D.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.Texture1D.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::two_dimensional:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURE2D;
			this->d3d12_shader_resource_view_desc.Texture2D.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.Texture2D.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.Texture2D.PlaneSlice = plane_slice;
			this->d3d12_shader_resource_view_desc.Texture2D.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::three_dimensional:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURE3D;
			this->d3d12_shader_resource_view_desc.Texture3D.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.Texture3D.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.Texture3D.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::cube:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURECUBE;
			this->d3d12_shader_resource_view_desc.TextureCube.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.TextureCube.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.TextureCube.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::one_dimensional_array:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURE1DARRAY;
			this->d3d12_shader_resource_view_desc.Texture1DArray.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.Texture1DArray.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.Texture1DArray.FirstArraySlice = create_info->subresource_range.base_array_layer;
			this->d3d12_shader_resource_view_desc.Texture1DArray.ArraySize = create_info->subresource_range.layer_count;
			this->d3d12_shader_resource_view_desc.Texture1DArray.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::two_dimensional_array:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
			this->d3d12_shader_resource_view_desc.Texture2DArray.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.Texture2DArray.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.Texture2DArray.FirstArraySlice = create_info->subresource_range.base_array_layer;
			this->d3d12_shader_resource_view_desc.Texture2DArray.ArraySize = create_info->subresource_range.layer_count;
			this->d3d12_shader_resource_view_desc.Texture2DArray.PlaneSlice = plane_slice;
			this->d3d12_shader_resource_view_desc.Texture2DArray.ResourceMinLODClamp = 0.0f;
			break;
		case ::framework::gpu::image_view_type::cube_array:
			this->d3d12_shader_resource_view_desc.ViewDimension = ::D3D12_SRV_DIMENSION::D3D12_SRV_DIMENSION_TEXTURECUBEARRAY;
			this->d3d12_shader_resource_view_desc.TextureCubeArray.MostDetailedMip = create_info->subresource_range.base_mip_level;
			this->d3d12_shader_resource_view_desc.TextureCubeArray.MipLevels = create_info->subresource_range.level_count;
			this->d3d12_shader_resource_view_desc.TextureCubeArray.First2DArrayFace = create_info->subresource_range.base_array_layer;
			this->d3d12_shader_resource_view_desc.TextureCubeArray.NumCubes = create_info->subresource_range.layer_count;
			this->d3d12_shader_resource_view_desc.TextureCubeArray.ResourceMinLODClamp = 0.0f;
			break;
		}
	}

	if (!!(usage & ::framework::gpu::image_usage_flags::storage_bit))
	{
		this->d3d12_unordered_access_view_desc.Format = dxgi_format;
		switch (create_info->view_type)
		{
		case ::framework::gpu::image_view_type::two_dimensional:
			this->d3d12_unordered_access_view_desc.ViewDimension = ::D3D12_UAV_DIMENSION::D3D12_UAV_DIMENSION_TEXTURE2D;
			this->d3d12_unordered_access_view_desc.Texture2D.MipSlice = create_info->subresource_range.base_mip_level;
			this->d3d12_unordered_access_view_desc.Texture2D.PlaneSlice = plane_slice;
			break;
		case ::framework::gpu::image_view_type::two_dimensional_array:
			this->d3d12_unordered_access_view_desc.ViewDimension = ::D3D12_UAV_DIMENSION::D3D12_UAV_DIMENSION_TEXTURE2DARRAY;
			this->d3d12_unordered_access_view_desc.Texture2DArray.MipSlice = create_info->subresource_range.base_mip_level;
			this->d3d12_unordered_access_view_desc.Texture2DArray.FirstArraySlice = create_info->subresource_range.base_array_layer;
			this->d3d12_unordered_access_view_desc.Texture2DArray.ArraySize = create_info->subresource_range.layer_count;
			this->d3d12_unordered_access_view_desc.Texture2DArray.PlaneSlice = plane_slice;
			break;
		}
	}

	result = ::framework::gpu::result::success;
}