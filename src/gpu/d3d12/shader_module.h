#pragma once

#include "gpu/shader_module.h"

namespace framework::gpu::d3d12
{
	class shader_module final : public ::framework::gpu::shader_module
	{
	public:
		shader_module(::framework::gpu::d3d12::device * device, ::framework::gpu::shader_module_create_info const * create_info, ::framework::gpu::result & result);

		::IDxcBlobEncoding * get_dxc_blob() const
		{
			return this->dxc_blob.Get();
		}

	private:
		friend class ::framework::gpu::d3d12::device;

		::Microsoft::WRL::ComPtr<::IDxcBlobEncoding> dxc_blob;
	};
}