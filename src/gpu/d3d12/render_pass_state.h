#pragma once

#include "gpu/render_pass_state.h"

namespace framework::gpu::d3d12
{
	class render_pass_state final : public ::framework::gpu::render_pass_state
	{
	public:
		struct subpass_info
		{
			::UINT num_render_targets;
			::framework::gpu::common::unique_ptr<::D3D12_RENDER_PASS_RENDER_TARGET_DESC[]> render_targets;
			::framework::gpu::common::unique_ptr<::D3D12_RENDER_PASS_DEPTH_STENCIL_DESC> depth_stencil;
		};

		render_pass_state(::framework::gpu::d3d12::device * device, ::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		void set_state(::framework::gpu::render_pass_state_info const & render_pass_state_info) override;

		::framework::gpu::d3d12::render_pass const * get_render_pass() const
		{
			return this->render_pass;
		}

		::framework::gpu::d3d12::frame_buffer const * get_frame_buffer() const
		{
			return this->frame_buffer;
		}

		::framework::gpu::d3d12::render_pass_state::subpass_info const & get_subpass_info(::std::uint32_t subpass) const
		{
			return subpass_infos[subpass];
		}

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::framework::gpu::d3d12::render_pass const * render_pass;
		::framework::gpu::d3d12::frame_buffer const * frame_buffer;
		::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::render_pass_state::subpass_info[]> subpass_infos;
		::std::uint32_t subpass_count;
	};
}