#include "gpu/d3d12/core.hpp"
#include <codecvt>
#include <cstring>

::framework::gpu::d3d12::resource::resource() :
	d3d12_resource(nullptr),
	is_dedicated(false)
{
}

::framework::gpu::d3d12::resource::~resource()
{
	if (this->d3d12_resource && !this->is_dedicated)
		this->d3d12_resource->Release();
}

::HRESULT(::framework::gpu::d3d12::resource::set_name)(char const * object_name)
{
	::HRESULT hresult;
	if (!object_name)
	{
		this->name = nullptr;
		hresult = S_OK;
	}
	else if (this->d3d12_resource)
	{
		::std::wstring_convert<::std::codecvt_utf8_utf16<wchar_t>> wstring_convert;
		hresult = this->d3d12_resource->SetName(wstring_convert.from_bytes(object_name).c_str());
		this->name = nullptr;
	}
	else
	{
		this->name = ::std::make_unique<char[]>(::std::strlen(object_name) + 1);
		::std::strcpy(this->name.get(), object_name);
		hresult = S_OK;
	}

	return hresult;
}