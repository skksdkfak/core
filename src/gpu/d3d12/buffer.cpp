#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::buffer::buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::result & result) :
	device_memory_offset(0),
	size(create_info->size),
	usage(create_info->usage)
{
	this->resource_flags = ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_NONE;
	if (!!(create_info->usage & ::framework::gpu::buffer_usage_flags::storage_texel_buffer_bit) ||
		!!(create_info->usage & ::framework::gpu::buffer_usage_flags::storage_buffer_bit) ||
		!!(create_info->usage & ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit) ||
		!!(create_info->usage & ::framework::gpu::buffer_usage_flags::allow_clear_bit))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
	if (!(create_info->usage & ::framework::gpu::buffer_usage_flags::uniform_buffer_bit) &&
		!(create_info->usage & ::framework::gpu::buffer_usage_flags::sampled_buffer_bit) &&
		!(create_info->usage & ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit) &&
		!(create_info->usage & ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;

	if (create_info->initial_queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
		this->initial_state = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COMMON;
	else
		this->initial_state = ::framework::gpu::d3d12::get_d3d12_resource_states(create_info->initial_state);

	this->is_dedicated = !!(create_info->usage & (::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::host_write_bit));

	this->d3d12_resource_desc.Alignment = 0;
	this->d3d12_resource_desc.DepthOrArraySize = 1;
	this->d3d12_resource_desc.Dimension = ::D3D12_RESOURCE_DIMENSION::D3D12_RESOURCE_DIMENSION_BUFFER;
	this->d3d12_resource_desc.Flags = resource_flags;
	this->d3d12_resource_desc.Format = ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
	this->d3d12_resource_desc.Height = 1;
	this->d3d12_resource_desc.Layout = ::D3D12_TEXTURE_LAYOUT::D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	this->d3d12_resource_desc.MipLevels = 1;
	this->d3d12_resource_desc.SampleDesc.Count = 1;
	this->d3d12_resource_desc.SampleDesc.Quality = 0;
	this->d3d12_resource_desc.Width = create_info->size;

	result = ::framework::gpu::result::success;
}