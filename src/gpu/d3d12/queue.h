#pragma once

#include "gpu/queue.h"

namespace framework::gpu::d3d12
{
	class queue final : public ::framework::gpu::queue
	{
	public:
		queue(::framework::gpu::d3d12::device * device, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index, float queue_priority, ::framework::gpu::result & result);

		~queue();

		::framework::gpu::result submit(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence) override;

		::framework::gpu::result present(::framework::gpu::present_info const * present_info) override;

		::framework::gpu::result wait_idle() override;

		::framework::gpu::result get_timestamp_period(float * timestamp_period) override;

		::ID3D12CommandQueue * const get_d3d12_command_queue() const { return this->d3d12_command_queue.Get(); }

	protected:
		friend class ::framework::gpu::d3d12::device;

		::Microsoft::WRL::ComPtr<::ID3D12CommandQueue> d3d12_command_queue;
		::Microsoft::WRL::ComPtr<::ID3D12Fence> d3d12_fence;
		::UINT64 next_fence_value;
	};
}