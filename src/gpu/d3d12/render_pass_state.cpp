#define NOMINMAX
#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::render_pass_state::render_pass_state(::framework::gpu::d3d12::device * device, struct ::framework::gpu::render_pass_state_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	render_pass(static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)),
	subpass_count(static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_count())
{
	this->subpass_infos = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass_state::subpass_info[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->subpass_count);

	for (::std::uint32_t subpass = 0; subpass < this->subpass_count; subpass++)
	{
		::framework::gpu::d3d12::render_pass::subpass_info const & subpass_info = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_info(subpass);

		this->subpass_infos[subpass].num_render_targets = subpass_info.num_render_targets;
		this->subpass_infos[subpass].render_targets = ::framework::gpu::common::make_unique<::D3D12_RENDER_PASS_RENDER_TARGET_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->subpass_infos[subpass].num_render_targets);

		if (subpass_info.depth_stencil)
		{
			this->subpass_infos[subpass].depth_stencil = ::framework::gpu::common::make_unique<::D3D12_RENDER_PASS_DEPTH_STENCIL_DESC>(this->allocator, ::framework::gpu::system_allocation_scope::object);
		}
	}

	result = ::framework::gpu::result::success;
}

void ::framework::gpu::d3d12::render_pass_state::set_state(::framework::gpu::render_pass_state_info const & render_pass_state_info)
{
	this->frame_buffer = static_cast<::framework::gpu::d3d12::frame_buffer const *>(render_pass_state_info.frame_buffer);

	::std::uint32_t const subpass_count = this->render_pass->get_subpass_count();
	::framework::gpu::attachment_view_and_descriptor const * attachments = this->frame_buffer->get_attachments();

	for (::std::uint32_t subpass = 0; subpass < subpass_count; subpass++)
	{
		auto const & subpass_info = this->render_pass->get_subpass_info(subpass);

		for (::uint32_t i = 0; i < subpass_info.num_render_targets; i++)
		{
			this->subpass_infos[subpass].render_targets[i].cpuDescriptor = ::framework::gpu::d3d12::get_d3d12_cpu_descriptor_handle(attachments[subpass_info.render_targets[i].attachment].descriptor_handle);
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Type = subpass_info.render_targets[i].beginning_access_type;
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Clear.ClearValue.Format = ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
			this->subpass_infos[subpass].render_targets[i].EndingAccess.Type = subpass_info.render_targets[i].ending_access_type;
		}

		for (::std::uint32_t i = 0; i < ::std::min(render_pass_state_info.clear_value_count, subpass_info.num_render_targets); i++)
		{
			auto const & clear_value = render_pass_state_info.clear_values[subpass_info.render_targets[i].attachment];
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Clear.ClearValue.Color[0] = clear_value.color.float32[0];
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Clear.ClearValue.Color[1] = clear_value.color.float32[1];
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Clear.ClearValue.Color[2] = clear_value.color.float32[2];
			this->subpass_infos[subpass].render_targets[i].BeginningAccess.Clear.ClearValue.Color[3] = clear_value.color.float32[3];
		}

		if (this->subpass_infos[subpass].depth_stencil)
		{
			auto const & clear_value = render_pass_state_info.clear_values[subpass_info.depth_stencil->attachment.attachment];
			this->subpass_infos[subpass].depth_stencil->cpuDescriptor = ::framework::gpu::d3d12::get_d3d12_cpu_descriptor_handle(attachments[subpass_info.depth_stencil->attachment.attachment].descriptor_handle);
			this->subpass_infos[subpass].depth_stencil->DepthBeginningAccess.Type = subpass_info.depth_stencil->depth_beginning_access_type;
			this->subpass_infos[subpass].depth_stencil->DepthBeginningAccess.Clear.ClearValue.DepthStencil.Depth = clear_value.depth_stencil.depth;
			this->subpass_infos[subpass].depth_stencil->DepthBeginningAccess.Clear.ClearValue.DepthStencil.Stencil = clear_value.depth_stencil.stencil;
			this->subpass_infos[subpass].depth_stencil->DepthBeginningAccess.Clear.ClearValue.Format = ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
			this->subpass_infos[subpass].depth_stencil->StencilBeginningAccess.Type = subpass_info.depth_stencil->stencil_beginning_access_type;
			this->subpass_infos[subpass].depth_stencil->DepthEndingAccess.Type = subpass_info.depth_stencil->depth_ending_access_type;
			this->subpass_infos[subpass].depth_stencil->StencilEndingAccess.Type = subpass_info.depth_stencil->stencil_ending_access_type;
		}
	}
}