#pragma once

#include "pipeline.h"

namespace framework::gpu::d3d12
{
	class compute_pipeline final : public ::framework::gpu::d3d12::pipeline
	{
	public:
		compute_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::compute_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::ID3D12PipelineState * get_d3d12_pipeline_state() const { return this->d3d12_pipeline_state.Get(); }

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::Microsoft::WRL::ComPtr<::ID3D12PipelineState> d3d12_pipeline_state;
	};
}