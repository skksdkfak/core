#pragma once

#include "gpu/query_pool.h"

namespace framework::gpu::d3d12
{
	class query_pool final : public ::framework::gpu::query_pool
	{
	public:
		query_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::query_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::ID3D12QueryHeap * get_d3d12_query_heap() const { return this->d3d12_query_heap.Get(); }

		::D3D12_QUERY_TYPE get_d3d12_query_type() const { return this->d3d12_query_type; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::Microsoft::WRL::ComPtr<::ID3D12QueryHeap> d3d12_query_heap;
		::D3D12_QUERY_TYPE d3d12_query_type;
	};
}