#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::semaphore::semaphore(::framework::gpu::d3d12::device * device, ::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::result & result)
{
	::HRESULT hresult;

	::UINT64 initial_value;
	if (create_info->semaphore_type == ::framework::gpu::semaphore_type::binary)
	{
		initial_value = 0;
		this->is_binary_semaphore = true;
	}
	else
	{
		initial_value = create_info->initial_value;
		this->is_binary_semaphore = false;
	}

	this->next_fence_value = initial_value + 1;

	hresult = device->get_d3d12_device()->CreateFence(initial_value, ::D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&this->d3d12_fence));

	result = ::framework::gpu::d3d12::get_result(hresult);
}

::framework::gpu::d3d12::semaphore::~semaphore()
{
	this->d3d12_fence->Release();
}