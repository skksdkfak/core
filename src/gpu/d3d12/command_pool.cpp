#define NOMINMAX
#include "gpu/d3d12/core.hpp"
#include <utility>

::framework::gpu::d3d12::command_pool::command_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	d3d12_command_list_type(::framework::gpu::d3d12::get_d3d12_command_list_type(create_info->queue_family_index)),
	reset_command_buffer(!!(create_info->flags & ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit)),
	allocated_command_buffer_count(0)
{
	if (this->reset_command_buffer)
	{
		result = ::framework::gpu::result::success;
	}
	else
	{
		::HRESULT const hresult = device->get_d3d12_device()->CreateCommandAllocator(this->d3d12_command_list_type, IID_PPV_ARGS(&this->d3d12_command_allocator));
		result = ::framework::gpu::d3d12::throw_result(hresult);
	}
}

::framework::gpu::d3d12::command_pool::~command_pool()
{
	if (this->reset_command_buffer)
	{
		for (::std::uint32_t i = 0; i < this->allocated_command_buffer_count; i++)
		{
			this->allocations[i].command_buffer->get_d3d12_command_allocator()->Release();
			DELETE(this->allocations[i].command_buffer, this->allocator);
		}

		for (::std::uint32_t i = this->allocated_command_buffer_count; i < static_cast<::std::uint32_t>(this->allocations.size()); i++)
		{
			this->allocations[i].d3d12_command_allocator->Release();
		}
		this->allocations.resize(this->allocated_command_buffer_count);
	}
	else
	{
		this->d3d12_command_allocator->Release();
	}
}

::framework::gpu::result(::framework::gpu::d3d12::command_pool::allocate_command_buffers)(::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::command_buffer ** command_buffers) noexcept
{
	::framework::gpu::result result;
	::std::uint32_t command_buffer_index = 0;

	try
	{
		for (; command_buffer_index < allocate_info->command_buffer_count; command_buffer_index++)
		{
			::ID3D12CommandAllocator * d3d12_command_allocator;
			if (this->reset_command_buffer)
			{
				if (this->allocations.size() > this->allocated_command_buffer_count)
				{
					d3d12_command_allocator = this->allocations[this->allocated_command_buffer_count].d3d12_command_allocator;
				}
				else
				{
					result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateCommandAllocator(this->d3d12_command_list_type, IID_PPV_ARGS(&d3d12_command_allocator)));
					::framework::gpu::d3d12::command_pool::allocation allocation;
					allocation.d3d12_command_allocator = d3d12_command_allocator;
					this->allocations.push_back(allocation);
				}
			}
			else
			{
				d3d12_command_allocator = this->d3d12_command_allocator;
			}

			::std::uint32_t const id = this->reset_command_buffer ? this->allocated_command_buffer_count++ : ~0ul;
			::framework::gpu::d3d12::command_buffer * command_buffer;
			switch (allocate_info->level)
			{
			case ::framework::gpu::command_buffer_level::primary:
			{
				::std::uint32_t const command_sub_buffer_count = allocate_info->subbuffer_count;
				::framework::gpu::common::unique_ptr<::Microsoft::WRL::ComPtr<::ID3D12GraphicsCommandList7>[]> d3d12_graphics_command_lists = ::framework::gpu::common::make_unique<::Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList7>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, command_sub_buffer_count);
				for (::std::uint32_t command_subbuffer = 0; command_subbuffer < command_sub_buffer_count; command_subbuffer++)
				{
					::HRESULT hresult = device->get_d3d12_device()->CreateCommandList(0, this->d3d12_command_list_type, d3d12_command_allocator, nullptr, IID_PPV_ARGS(&d3d12_graphics_command_lists[command_subbuffer]));
					if (SUCCEEDED(hresult))
					{
						hresult = d3d12_graphics_command_lists[command_subbuffer]->Close();
					}
					result = ::framework::gpu::d3d12::throw_result(hresult);
				}

				command_buffer = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::primary_command_buffer>(this->allocator, ::framework::gpu::system_allocation_scope::object, device, allocate_info, ::std::move(d3d12_graphics_command_lists), d3d12_command_allocator, id).release();
			}
			break;
			case ::framework::gpu::command_buffer_level::secondary:
			{
				::Microsoft::WRL::ComPtr<::ID3D12GraphicsCommandList7> d3d12_graphics_command_list;
				::HRESULT hresult = device->get_d3d12_device()->CreateCommandList(0, this->d3d12_command_list_type, d3d12_command_allocator, nullptr, IID_PPV_ARGS(&d3d12_graphics_command_list));
				if (SUCCEEDED(hresult))
				{
					hresult = d3d12_graphics_command_list->Close();
				}
				result = ::framework::gpu::d3d12::throw_result(hresult);

				command_buffer = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::command_buffer>(this->allocator, ::framework::gpu::system_allocation_scope::object, device, allocate_info, d3d12_graphics_command_list.Detach(), d3d12_command_allocator, id).release();
			}
			break;
			default:
				::std::unreachable();
			}

			if (this->reset_command_buffer)
			{
				this->allocations[id].command_buffer = command_buffer;
			}
			command_buffers[command_buffer_index] = command_buffer;
		}

		result = ::framework::gpu::result::success;
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = command_buffer_index; i < allocate_info->command_buffer_count; i++)
		{
			command_buffers[i] = nullptr;
		}

		this->free_command_buffers(device, command_buffer_index, command_buffers);

		for (::std::uint32_t i = 0; i < command_buffer_index; i++)
		{
			command_buffers[i] = nullptr;
		}

		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::command_pool::free_command_buffers(::framework::gpu::d3d12::device * device, ::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers) noexcept
{
	for (::std::uint32_t i = 0; i < command_buffer_count; i++)
	{
		if (command_buffers[i])
		{
			::framework::gpu::d3d12::command_buffer * const command_buffer = static_cast<::framework::gpu::d3d12::command_buffer *>(command_buffers[i]);
			if (this->reset_command_buffer)
			{
				::std::swap(this->allocations[command_buffer->id], this->allocations[--this->allocated_command_buffer_count]);
				this->allocations[command_buffer->id].command_buffer->id = command_buffer->id;
				this->allocations[this->allocated_command_buffer_count].d3d12_command_allocator = command_buffer->get_d3d12_command_allocator();
			}
			DELETE(command_buffer, this->allocator);
		}
	}
}

::framework::gpu::result(::framework::gpu::d3d12::command_pool::reset)(::framework::gpu::command_pool_reset_flags flags)
{
	::HRESULT hresult;

	if (this->reset_command_buffer)
	{
		::HRESULT tmp_hresult;
		hresult = S_OK;
		for (::std::uint32_t command_buffer_index = 0; command_buffer_index < this->allocated_command_buffer_count; command_buffer_index++)
		{
			tmp_hresult = this->allocations[command_buffer_index].command_buffer->get_d3d12_command_allocator()->Reset();
			if (hresult == S_OK && tmp_hresult != S_OK)
			{
				hresult = tmp_hresult;
			}
		}
	}
	else
	{
		hresult = this->d3d12_command_allocator->Reset();
	}

	return ::framework::gpu::d3d12::get_result(hresult);
}

void ::framework::gpu::d3d12::command_pool::trim(::framework::gpu::command_pool_trim_flags flags)
{
	if (this->reset_command_buffer)
	{
		for (::std::uint32_t i = this->allocated_command_buffer_count; i < static_cast<::std::uint32_t>(this->allocations.size()); i++)
		{
			this->allocations[i].d3d12_command_allocator->Release();
		}
		this->allocations.resize(this->allocated_command_buffer_count);
	}
}