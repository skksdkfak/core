#pragma once

#include "gpu/image_view.h"

namespace framework::gpu::d3d12
{
	class image_view final : public ::framework::gpu::image_view
	{
	public:
		image_view(::framework::gpu::d3d12::device * device, ::framework::gpu::image_view_create_info const * create_info, ::framework::gpu::result & result);

		::D3D12_SHADER_RESOURCE_VIEW_DESC const & get_d3d12_shader_resource_view_desc() const { return this->d3d12_shader_resource_view_desc; }
		::D3D12_UNORDERED_ACCESS_VIEW_DESC const & get_d3d12_unordered_access_view_desc() const { return this->d3d12_unordered_access_view_desc; }
		::D3D12_RENDER_TARGET_VIEW_DESC const & get_d3d12_render_target_view_desc() const { return this->d3d12_render_target_view_desc; }
		::D3D12_DEPTH_STENCIL_VIEW_DESC const & get_d3d12_depth_stencil_view_desc() const { return this->d3d12_depth_stencil_view_desc; }
		::framework::gpu::d3d12::image * get_image() const { return this->image; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::D3D12_SHADER_RESOURCE_VIEW_DESC d3d12_shader_resource_view_desc;
		::D3D12_UNORDERED_ACCESS_VIEW_DESC d3d12_unordered_access_view_desc;
		::D3D12_RENDER_TARGET_VIEW_DESC d3d12_render_target_view_desc;
		::D3D12_DEPTH_STENCIL_VIEW_DESC d3d12_depth_stencil_view_desc;
		::framework::gpu::d3d12::image * image;
	};
}