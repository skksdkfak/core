#include "gpu/d3d12/core.hpp"
#include <type_traits>

::framework::gpu::d3d12::descriptor_pool::descriptor_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	max_sets(create_info->max_sets),
	current_offset_in_descriptors_from_heap_start(),
	d3d12_descriptor_heap_count(0),
	allocated_descriptor_set_count(0)
{
	result = ::framework::gpu::result::success;

	this->shader_visible_descriptor_heap = !(create_info->flags & ::framework::gpu::descriptor_pool_create_flags::host_only_bit);

	::D3D12_DESCRIPTOR_HEAP_FLAGS d3d12_descriptor_heap_flags;
	if (this->shader_visible_descriptor_heap)
	{
		d3d12_descriptor_heap_flags = ::D3D12_DESCRIPTOR_HEAP_FLAGS::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	}
	else
	{
		d3d12_descriptor_heap_flags = ::D3D12_DESCRIPTOR_HEAP_FLAGS::D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	}

	::UINT descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES]{};
	for (::std::uint32_t i = 0; i < create_info->pool_size_count; i++)
	{
		switch (create_info->pool_sizes[i].type)
		{
		case ::framework::gpu::descriptor_type::combined_image_sampler:
			descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] += create_info->pool_sizes[i].descriptor_count;
		case ::framework::gpu::descriptor_type::sampler:
			descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] += create_info->pool_sizes[i].descriptor_count;
			break;
		case ::framework::gpu::descriptor_type::sampled_image:
		case ::framework::gpu::descriptor_type::storage_image:
		case ::framework::gpu::descriptor_type::uniform_texel_buffer:
		case ::framework::gpu::descriptor_type::storage_texel_buffer:
		case ::framework::gpu::descriptor_type::uniform_buffer:
		case ::framework::gpu::descriptor_type::sampled_buffer:
		case ::framework::gpu::descriptor_type::storage_buffer:
		case ::framework::gpu::descriptor_type::input_attachment:
		case ::framework::gpu::descriptor_type::acceleration_structure:
			descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] += create_info->pool_sizes[i].descriptor_count;
			break;
		case ::framework::gpu::descriptor_type::color_attachment:
			descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV] += create_info->pool_sizes[i].descriptor_count;
			break;
		case ::framework::gpu::descriptor_type::depth_stencil_attachment:
			descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV] += create_info->pool_sizes[i].descriptor_count;
			break;
		}
	}

	for (::std::underlying_type_t<::D3D12_DESCRIPTOR_HEAP_TYPE> i = 0; i < ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; i++)
	{
		if (descriptor_count[i] == 0)
		{
			this->d3d12_descriptor_heaps[i] = nullptr;
			continue;
		}

		::D3D12_DESCRIPTOR_HEAP_DESC d3d12_descriptor_heap_desc;
		d3d12_descriptor_heap_desc.Type = static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(i);
		d3d12_descriptor_heap_desc.NumDescriptors = descriptor_count[i];
		d3d12_descriptor_heap_desc.Flags = d3d12_descriptor_heap_flags;
		d3d12_descriptor_heap_desc.NodeMask = 1;
		result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateDescriptorHeap(&d3d12_descriptor_heap_desc, IID_PPV_ARGS(&this->d3d12_descriptor_heaps[i])));
	}

	if (descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV])
	{
		this->d3d12_descriptor_heap_count++;
		this->first_d3d12_descriptor_heap = 0;
	}
	else
	{
		this->first_d3d12_descriptor_heap = 1;
	}

	if (descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER])
	{
		this->d3d12_descriptor_heap_count++;
	}

	this->descriptor_sets = ::framework::gpu::common::make_unique<::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::descriptor_set>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->max_sets);
	for (::std::uint32_t i = 0; i < create_info->max_sets; i++)
	{
		this->descriptor_sets[i] = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::descriptor_set>(this->allocator, ::framework::gpu::system_allocation_scope::object);
	}
}

::framework::gpu::d3d12::descriptor_pool::~descriptor_pool()
{
	for (::std::underlying_type_t<::D3D12_DESCRIPTOR_HEAP_TYPE> i = 0; i < ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; i++)
	{
		if (this->d3d12_descriptor_heaps[i])
		{
			this->d3d12_descriptor_heaps[i]->Release();
		}
	}
}

::framework::gpu::result(::framework::gpu::d3d12::descriptor_pool::allocate_descriptor_sets)(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_set_allocate_info const * allocate_info, ::framework::gpu::descriptor_set ** descriptor_sets) noexcept
{
	::framework::gpu::result result;
	::std::uint32_t descriptor_set_index = 0;

	try
	{
		[[unlikely]] if (this->allocated_descriptor_set_count + allocate_info->descriptor_set_count > this->max_sets)
		{
			for (::std::uint32_t i = 0; i < allocate_info->descriptor_set_count; i++)
			{
				descriptor_sets[i] = nullptr;
			}

			return ::framework::gpu::result::error_out_of_pool_memory;
		}

		::UINT const descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES]
		{
			device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
			device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER),
			device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV),
			device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
		};

		for (; descriptor_set_index < allocate_info->descriptor_set_count; descriptor_set_index++)
		{
			::std::uint32_t const descriptor_set_id = this->allocated_descriptor_set_count++;
			::framework::gpu::d3d12::descriptor_set * descriptor_set = this->descriptor_sets[descriptor_set_id].get();
			descriptor_set->id = descriptor_set_id;

			for (::UINT d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; d3d12_descriptor_heap_type++)
			{
				::UINT const descriptor_count = static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(allocate_info->set_layouts[descriptor_set_index])->get_descriptor_count(static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type));
				if (descriptor_count)
				{

					::UINT const offset_from_heap_start = this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type] * descriptor_handle_increment_size[d3d12_descriptor_heap_type];
					descriptor_set->cpu_descriptor_range_start[d3d12_descriptor_heap_type].ptr = this->d3d12_descriptor_heaps[d3d12_descriptor_heap_type]->GetCPUDescriptorHandleForHeapStart().ptr + offset_from_heap_start;
					if (this->shader_visible_descriptor_heap)
					{
						descriptor_set->gpu_descriptor_range_start[d3d12_descriptor_heap_type].ptr = this->d3d12_descriptor_heaps[d3d12_descriptor_heap_type]->GetGPUDescriptorHandleForHeapStart().ptr + offset_from_heap_start;
					}
					this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type] += descriptor_count;
				}
			}

			descriptor_set->dynamic_root_parameter_addresses.reset();
			descriptor_set->dynamic_root_parameter_addresses = ::framework::gpu::common::make_unique<::D3D12_GPU_VIRTUAL_ADDRESS[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(allocate_info->set_layouts[descriptor_set_index])->get_dynamic_descriptor_count());
			descriptor_set->descriptor_set_layout = static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(allocate_info->set_layouts[descriptor_set_index]);

			descriptor_sets[descriptor_set_index] = descriptor_set;
		}

		result = ::framework::gpu::result::success;
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		for (::std::uint32_t i = descriptor_set_index; i < allocate_info->descriptor_set_count; i++)
		{
			descriptor_sets[i] = nullptr;
		}

		this->free_descriptor_sets(device, descriptor_set_index, descriptor_sets);

		for (::std::uint32_t i = 0; i < descriptor_set_index; i++)
		{
			descriptor_sets[i] = nullptr;
		}

		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::descriptor_pool::free_descriptor_sets(::framework::gpu::d3d12::device * device, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets) noexcept
{
	::UINT const descriptor_handle_increment_size[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES]
	{
		device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
		device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER),
		device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV),
		device->get_d3d12_device()->GetDescriptorHandleIncrementSize(::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
	};

	// Perform defragmentation of descriptors by moving rihgtmost descriptors into the place of freed descriptor set
	for (::std::uint32_t i = 0; i < descriptor_set_count; i++)
	{
		if (descriptor_sets[i])
		{
			::framework::gpu::d3d12::descriptor_set * const descriptor_set = static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_sets[i]);

			for (::std::underlying_type_t<::D3D12_DESCRIPTOR_HEAP_TYPE> d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; d3d12_descriptor_heap_type++)
			{
				::UINT const descriptor_count = descriptor_set->get_descriptor_set_layout()->get_descriptor_count(static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type));
				if (descriptor_count)
				{
					// Note: because CopyDescriptorsSimple does not support memmove behavior i.e. dst and src ranges should not overlap we move descriptors by small non-overlapping regions
					::SIZE_T const step_size = descriptor_count * descriptor_handle_increment_size[d3d12_descriptor_heap_type];
					::UINT const complete_range_count = (this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type] / descriptor_count) * descriptor_count;
					::D3D12_CPU_DESCRIPTOR_HANDLE src = ::D3D12_CPU_DESCRIPTOR_HANDLE{ descriptor_set->cpu_descriptor_range_start[d3d12_descriptor_heap_type].ptr + step_size };
					::D3D12_CPU_DESCRIPTOR_HANDLE dst = descriptor_set->cpu_descriptor_range_start[d3d12_descriptor_heap_type];
					for (; dst.ptr < complete_range_count * descriptor_handle_increment_size[d3d12_descriptor_heap_type]; src.ptr += step_size, dst.ptr += step_size)
					{
						device->get_d3d12_device()->CopyDescriptorsSimple(descriptor_count, dst, src, static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type));
					}

					if (complete_range_count < this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type])
					{
						device->get_d3d12_device()->CopyDescriptorsSimple(this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type] - complete_range_count, dst, src, static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type));
					}

					// Adjust range start of other descriptor sets from the pool
					for (::std::uint32_t j = descriptor_set->id + 1; j < this->allocated_descriptor_set_count; j++)
					{
						this->descriptor_sets[j]->cpu_descriptor_range_start[d3d12_descriptor_heap_type].ptr -= step_size;
					}

					this->current_offset_in_descriptors_from_heap_start[d3d12_descriptor_heap_type] -= descriptor_count;
				}
			}

			::std::swap(this->descriptor_sets[descriptor_set->id], this->descriptor_sets[--this->allocated_descriptor_set_count]);
			this->descriptor_sets[descriptor_set->id]->id = descriptor_set->id;
		}
	}
}