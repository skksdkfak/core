#pragma once

#include "gpu/descriptor_pool.h"
#include <vector>
#include <queue>

namespace framework::gpu::d3d12
{
	class descriptor_pool final : public ::framework::gpu::descriptor_pool
	{
	public:
		descriptor_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~descriptor_pool();

		::framework::gpu::result allocate_descriptor_sets(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_set_allocate_info const * allocate_info, ::framework::gpu::descriptor_set ** descriptor_sets) noexcept;

		void free_descriptor_sets(::framework::gpu::d3d12::device * device, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets) noexcept;

		::UINT get_d3d12_descriptor_heap_count() const { return static_cast<::UINT>(this->d3d12_descriptor_heap_count); }

		::ID3D12DescriptorHeap * const * get_d3d12_descriptor_heaps() const { return &this->d3d12_descriptor_heaps[this->first_d3d12_descriptor_heap]; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::std::uint32_t const max_sets;
		::ID3D12DescriptorHeap * d3d12_descriptor_heaps[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
		::UINT current_offset_in_descriptors_from_heap_start[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
		::std::uint8_t first_d3d12_descriptor_heap : 1;
		::std::uint8_t d3d12_descriptor_heap_count : 2;
		::std::uint8_t shader_visible_descriptor_heap : 1;
		::framework::gpu::common::unique_ptr<::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::descriptor_set>[]> descriptor_sets;
		::std::uint32_t allocated_descriptor_set_count;
	};
}