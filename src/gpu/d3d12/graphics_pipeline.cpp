#include "gpu/d3d12/core.hpp"
#include <format>

::framework::gpu::d3d12::graphics_pipeline::graphics_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::graphics_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	dynamic_states{ false }
{
	if (create_info->dynamic_state)
	{
		for (::std::uint32_t i = 0; i < create_info->dynamic_state->dynamic_state_count; i++)
		{
			this->dynamic_states[static_cast<::std::underlying_type_t<::framework::gpu::dynamic_state>>(create_info->dynamic_state->dynamic_states[i])] = true;
		}
	}

	if (create_info->viewport_state)
	{
		if (create_info->viewport_state->viewport_count)
		{
			this->viewports.resize(create_info->viewport_state->viewport_count);

			for (::std::uint32_t i = 0; i < create_info->viewport_state->viewport_count; i++)
			{
				this->viewports[i].TopLeftX = create_info->viewport_state->viewports[i].x;
				this->viewports[i].TopLeftY = create_info->viewport_state->viewports[i].y;
				this->viewports[i].Width = create_info->viewport_state->viewports[i].width;
				this->viewports[i].Height = create_info->viewport_state->viewports[i].height;
				this->viewports[i].MinDepth = create_info->viewport_state->viewports[i].min_depth;
				this->viewports[i].MaxDepth = create_info->viewport_state->viewports[i].max_depth;
			}
		}

		if (create_info->viewport_state->scissor_count)
		{
			this->scissors.resize(create_info->viewport_state->scissor_count);

			for (::std::uint32_t i = 0; i < create_info->viewport_state->scissor_count; i++)
			{
				this->scissors[i].left = create_info->viewport_state->scissors[i].offset.x;
				this->scissors[i].right = create_info->viewport_state->scissors[i].extent.width;
				this->scissors[i].bottom = create_info->viewport_state->scissors[i].extent.height;
				this->scissors[i].top = create_info->viewport_state->scissors[i].offset.y;
			}
		}
	}

	::Microsoft::WRL::ComPtr<::IDxcLinker> dxc_linker;

	result = ::framework::gpu::d3d12::throw_result(::DxcCreateInstance(::CLSID_DxcLinker, IID_PPV_ARGS(&dxc_linker)));

	::IDxcBlob * shader_codes[5]{};
	for (::std::uint32_t i = 0; i < create_info->stage_count; i++)
	{
		::std::wstring library_name = ::std::format(L"library_{}", i);

		result = ::framework::gpu::d3d12::throw_result(dxc_linker->RegisterLibrary(library_name.c_str(), static_cast<::framework::gpu::d3d12::shader_module const *>(create_info->stages[i].module)->get_dxc_blob()));

		::UINT pipeline_stage_index;
		::LPCWSTR shader_version = L"6_6";
		::LPCWSTR target_profile_name;
		switch (create_info->stages[i].stage)
		{
		case ::framework::gpu::shader_stage_flags::vertex_bit:
			target_profile_name = L"vs";
			pipeline_stage_index = 0;
			break;
		case ::framework::gpu::shader_stage_flags::tessellation_control_bit:
			target_profile_name = L"hs";
			pipeline_stage_index = 1;
			break;
		case ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit:
			target_profile_name = L"ds";
			pipeline_stage_index = 2;
			break;
		case ::framework::gpu::shader_stage_flags::geometry_bit:
			target_profile_name = L"gs";
			pipeline_stage_index = 3;
			break;
		case ::framework::gpu::shader_stage_flags::fragment_bit:
			target_profile_name = L"ps";
			pipeline_stage_index = 4;
			break;
		default:
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_initialization_failed);
		}

		::WCHAR target_profile[8];
		::std::swprintf(target_profile, ::std::size(target_profile), L"%ls_%ls", target_profile_name, shader_version);

		::framework::gpu::common::unique_ptr<::WCHAR[]> entry_point;
		int entry_point_len = ::MultiByteToWideChar(CP_UTF8, 0, create_info->stages[i].name, -1, NULL, 0);
		if (entry_point_len > 0)
		{
			entry_point = ::framework::gpu::common::make_unique<::WCHAR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, entry_point_len);
			::MultiByteToWideChar(CP_UTF8, 0, create_info->stages[i].name, -1, entry_point.get(), entry_point_len);
		}

		::Microsoft::WRL::ComPtr<::IDxcOperationResult> dxc_compile_result;
		::LPCWSTR library_names = library_name.c_str();
		result = ::framework::gpu::d3d12::throw_result(dxc_linker->Link(entry_point.get(), target_profile, &library_names, 1, nullptr, 0, &dxc_compile_result));

		::HRESULT hresult;
		result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetStatus(&hresult));

		if (FAILED(hresult))
		{
			if (dxc_compile_result)
			{
				::Microsoft::WRL::ComPtr<::IDxcBlobEncoding> errors_blob;
				result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetErrorBuffer(&errors_blob));

				if (errors_blob)
				{
					::OutputDebugStringA(static_cast<::LPCSTR>(errors_blob->GetBufferPointer()));
				}
			}

			result = ::framework::gpu::d3d12::throw_result(hresult);
		}

		result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetResult(&shader_codes[pipeline_stage_index]));

	}

	::D3D12_INPUT_LAYOUT_DESC d3d12_input_layout_desc;
	::std::vector<::D3D12_INPUT_ELEMENT_DESC> d3d12_input_element_descs;
	if (create_info->vertex_input_state)
	{
		::std::uint32_t binding_count = 0;
		for (::std::uint32_t i = 0; i < create_info->vertex_input_state->vertex_binding_description_count; i++)
		{
			::std::uint32_t const binding = create_info->vertex_input_state->vertex_binding_descriptions[i].binding;
			if (binding > binding_count)
			{
				binding_count = binding;
			}
		}

		binding_count++;

		struct input_slot_description
		{
			::D3D12_INPUT_CLASSIFICATION d3d12_input_classification;
			::UINT instance_data_step_rate;
		};

		::std::vector<typename input_slot_description> binding_slot_descriptions(binding_count);
		this->vertex_binding_stride.resize(binding_count);

		for (::std::uint32_t i = 0; i < create_info->vertex_input_state->vertex_binding_description_count; i++)
		{
			::framework::gpu::vertex_input_binding_description const & vertex_binding_descriptions = create_info->vertex_input_state->vertex_binding_descriptions[i];

			this->vertex_binding_stride[vertex_binding_descriptions.binding] = vertex_binding_descriptions.stride;

			::D3D12_INPUT_CLASSIFICATION d3d12_input_classification;
			::UINT instance_data_step_rate;
			switch (vertex_binding_descriptions.input_rate)
			{
			case ::framework::gpu::vertex_input_rate::vertex:
				d3d12_input_classification = ::D3D12_INPUT_CLASSIFICATION::D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
				instance_data_step_rate = 0;
				break;
			case ::framework::gpu::vertex_input_rate::instance:
				d3d12_input_classification = ::D3D12_INPUT_CLASSIFICATION::D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA;
				instance_data_step_rate = 1;
				break;
			}

			binding_slot_descriptions[vertex_binding_descriptions.binding] = input_slot_description{ d3d12_input_classification, instance_data_step_rate };
		}

		d3d12_input_element_descs.resize(create_info->vertex_input_state->vertex_attribute_description_count);

		for (::std::uint32_t i = 0; i < create_info->vertex_input_state->vertex_attribute_description_count; i++)
		{
			::framework::gpu::vertex_input_attribute_description const & vertex_attribute_description = create_info->vertex_input_state->vertex_attribute_descriptions[i];
			typename input_slot_description const & binding_slot_description = binding_slot_descriptions[vertex_attribute_description.binding];

			d3d12_input_element_descs[i].SemanticName = "LOCATION";
			d3d12_input_element_descs[i].SemanticIndex = vertex_attribute_description.location;
			d3d12_input_element_descs[i].Format = ::framework::gpu::d3d12::get_dxgi_format(vertex_attribute_description.format);
			d3d12_input_element_descs[i].InputSlot = vertex_attribute_description.binding;
			d3d12_input_element_descs[i].AlignedByteOffset = vertex_attribute_description.offset;
			d3d12_input_element_descs[i].InputSlotClass = binding_slot_description.d3d12_input_classification;
			d3d12_input_element_descs[i].InstanceDataStepRate = binding_slot_description.instance_data_step_rate;
		}

		d3d12_input_layout_desc.pInputElementDescs = d3d12_input_element_descs.data();
		d3d12_input_layout_desc.NumElements = create_info->vertex_input_state->vertex_attribute_description_count;
	}
	else
	{
		d3d12_input_layout_desc.pInputElementDescs = nullptr;
		d3d12_input_layout_desc.NumElements = 0;
	}

	if (create_info->input_assembly_state)
	{
		switch (create_info->input_assembly_state->topology)
		{
		case ::framework::gpu::primitive_topology::point_list:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_POINTLIST;
			break;
		case ::framework::gpu::primitive_topology::line_list:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_LINELIST;
			break;
		case ::framework::gpu::primitive_topology::line_strip:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_LINESTRIP;
			break;
		case ::framework::gpu::primitive_topology::triangle_list:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			break;
		case ::framework::gpu::primitive_topology::triangle_strip:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
			break;
		case ::framework::gpu::primitive_topology::triangle_fan:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
			break;
		case ::framework::gpu::primitive_topology::line_list_with_adjacency:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_LINELIST_ADJ;
			break;
		case ::framework::gpu::primitive_topology::line_strip_with_adjacency:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ;
			break;
		case ::framework::gpu::primitive_topology::triangle_list_with_adjacency:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_LINELIST_ADJ;
			break;
		case ::framework::gpu::primitive_topology::triangle_strip_with_adjacency:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ;
			break;
		case ::framework::gpu::primitive_topology::patch_list:
			this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST;
			break;
		}
	}
	else
	{
		this->d3d12_primitive_topology = ::D3D_PRIMITIVE_TOPOLOGY::D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	}

	auto get_d3d12_comparison_func = [](::framework::gpu::compare_op compare_op) -> ::D3D12_COMPARISON_FUNC
	{
		::D3D12_COMPARISON_FUNC d3d12_comparison_func;
		switch (compare_op)
		{
		case ::framework::gpu::compare_op::never:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_NEVER;
			break;
		case ::framework::gpu::compare_op::less:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_LESS;
			break;
		case ::framework::gpu::compare_op::equal:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_EQUAL;
			break;
		case ::framework::gpu::compare_op::less_or_equal:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_LESS_EQUAL;
			break;
		case ::framework::gpu::compare_op::greater:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_GREATER;
			break;
		case ::framework::gpu::compare_op::not_equal:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_NOT_EQUAL;
			break;
		case ::framework::gpu::compare_op::greater_or_equal:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_GREATER_EQUAL;
			break;
		case ::framework::gpu::compare_op::always:
			d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_ALWAYS;
			break;
		}
		return d3d12_comparison_func;
	};

	auto get_d3d12_stencil_op = [](::framework::gpu::stencil_op stencil_op) -> ::D3D12_STENCIL_OP
	{
		::D3D12_STENCIL_OP d3d12_stencil_op;
		switch (stencil_op)
		{
		case ::framework::gpu::stencil_op::keep:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP;
			break;
		case ::framework::gpu::stencil_op::zero:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_ZERO;
			break;
		case ::framework::gpu::stencil_op::replace:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_REPLACE;
			break;
		case ::framework::gpu::stencil_op::increment_and_clamp:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_INCR_SAT;
			break;
		case ::framework::gpu::stencil_op::decrement_and_clamp:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_DECR_SAT;
			break;
		case ::framework::gpu::stencil_op::invert:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_INCR;
			break;
		case ::framework::gpu::stencil_op::increment_and_wrap:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_INCR;
			break;
		case ::framework::gpu::stencil_op::decrement_and_wrap:
			d3d12_stencil_op = ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_DECR;
			break;
		}
		return d3d12_stencil_op;
	};

	::D3D12_RASTERIZER_DESC d3d12_rasterizer_desc;
	if (create_info->rasterization_state)
	{
		::D3D12_FILL_MODE d3d12_fill_mode;
		switch (create_info->rasterization_state->polygon_mode)
		{
		case ::framework::gpu::polygon_mode::fill:
			d3d12_fill_mode = ::D3D12_FILL_MODE::D3D12_FILL_MODE_SOLID;
			break;
		case ::framework::gpu::polygon_mode::line:
			d3d12_fill_mode = ::D3D12_FILL_MODE::D3D12_FILL_MODE_WIREFRAME;
			break;
		case ::framework::gpu::polygon_mode::point:
			d3d12_fill_mode = ::D3D12_FILL_MODE::D3D12_FILL_MODE_WIREFRAME;
			break;
		}

		::D3D12_CULL_MODE d3d12_cull_mode;
		switch (create_info->rasterization_state->cull_mode)
		{
		case ::framework::gpu::cull_mode_flags::none:
			d3d12_cull_mode = ::D3D12_CULL_MODE::D3D12_CULL_MODE_NONE;
			break;
		case ::framework::gpu::cull_mode_flags::front_bit:
			d3d12_cull_mode = ::D3D12_CULL_MODE::D3D12_CULL_MODE_FRONT;
			break;
		case ::framework::gpu::cull_mode_flags::back_bit:
			d3d12_cull_mode = ::D3D12_CULL_MODE::D3D12_CULL_MODE_BACK;
			break;
		case ::framework::gpu::cull_mode_flags::front_and_back:
			d3d12_cull_mode = ::D3D12_CULL_MODE::D3D12_CULL_MODE_BACK;
			break;
		}

		::BOOL front_counter_clockwise;
		switch (create_info->rasterization_state->front_face)
		{
		case ::framework::gpu::front_face::counter_clockwise:
			front_counter_clockwise = TRUE;
			break;
		case ::framework::gpu::front_face::clockwise:
			front_counter_clockwise = FALSE;
			break;
		}

		d3d12_rasterizer_desc.FillMode = d3d12_fill_mode;
		d3d12_rasterizer_desc.CullMode = d3d12_cull_mode;
		d3d12_rasterizer_desc.FrontCounterClockwise = front_counter_clockwise;
		d3d12_rasterizer_desc.DepthBias = create_info->rasterization_state->depth_bias_constant_factor;
		d3d12_rasterizer_desc.DepthBiasClamp = create_info->rasterization_state->depth_bias_clamp;
		d3d12_rasterizer_desc.SlopeScaledDepthBias = create_info->rasterization_state->depth_bias_slope_factor;
		d3d12_rasterizer_desc.DepthClipEnable = static_cast<::BOOL>(!create_info->rasterization_state->depth_clamp_enable);
		d3d12_rasterizer_desc.MultisampleEnable = FALSE;
		d3d12_rasterizer_desc.AntialiasedLineEnable = FALSE;
		d3d12_rasterizer_desc.ForcedSampleCount = 0;
		d3d12_rasterizer_desc.ConservativeRaster = ::D3D12_CONSERVATIVE_RASTERIZATION_MODE::D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	}
	else
	{
		d3d12_rasterizer_desc.FillMode = ::D3D12_FILL_MODE::D3D12_FILL_MODE_SOLID;
		d3d12_rasterizer_desc.CullMode = ::D3D12_CULL_MODE::D3D12_CULL_MODE_BACK;
		d3d12_rasterizer_desc.FrontCounterClockwise = FALSE;
		d3d12_rasterizer_desc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
		d3d12_rasterizer_desc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		d3d12_rasterizer_desc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		d3d12_rasterizer_desc.DepthClipEnable = TRUE;
		d3d12_rasterizer_desc.MultisampleEnable = FALSE;
		d3d12_rasterizer_desc.AntialiasedLineEnable = FALSE;
		d3d12_rasterizer_desc.ForcedSampleCount = 0;
		d3d12_rasterizer_desc.ConservativeRaster = ::D3D12_CONSERVATIVE_RASTERIZATION_MODE::D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	}

	::D3D12_BLEND_DESC d3d12_blend_desc;
	if (create_info->color_blend_state)
	{
		auto get_d3d12_blend = [](::framework::gpu::blend_factor blend_factor) -> ::D3D12_BLEND
		{
			::D3D12_BLEND d3d12_blend;
			switch (blend_factor)
			{
			case ::framework::gpu::blend_factor::zero:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_ZERO;
				break;
			case ::framework::gpu::blend_factor::one:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_ONE;
				break;
			case ::framework::gpu::blend_factor::src_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_SRC_COLOR;
				break;
			case ::framework::gpu::blend_factor::one_minus_src_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_SRC_COLOR;
				break;
			case ::framework::gpu::blend_factor::dst_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_DEST_COLOR;
				break;
			case ::framework::gpu::blend_factor::one_minus_dst_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_DEST_COLOR;
				break;
			case ::framework::gpu::blend_factor::src_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_SRC_ALPHA;
				break;
			case ::framework::gpu::blend_factor::one_minus_src_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_SRC_ALPHA;
				break;
			case ::framework::gpu::blend_factor::dst_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_DEST_ALPHA;
				break;
			case ::framework::gpu::blend_factor::one_minus_dst_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_DEST_ALPHA;
				break;
			case ::framework::gpu::blend_factor::constant_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_BLEND_FACTOR;
				break;
			case ::framework::gpu::blend_factor::one_minus_constant_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_BLEND_FACTOR;
				break;
			case ::framework::gpu::blend_factor::constant_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_BLEND_FACTOR;
				break;
			case ::framework::gpu::blend_factor::one_minus_constant_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_BLEND_FACTOR;
				break;
			case ::framework::gpu::blend_factor::src_alpha_saturate:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_SRC_ALPHA_SAT;
				break;
			case ::framework::gpu::blend_factor::src1_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_SRC1_COLOR;
				break;
			case ::framework::gpu::blend_factor::one_minus_src1_color:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_SRC1_COLOR;
				break;
			case ::framework::gpu::blend_factor::src1_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_SRC1_ALPHA;
				break;
			case ::framework::gpu::blend_factor::one_minus_src1_alpha:
				d3d12_blend = ::D3D12_BLEND::D3D12_BLEND_INV_SRC1_ALPHA;
				break;
			}
			return d3d12_blend;
		};

		auto get_d3d12_blend_op = [](::framework::gpu::blend_op blend_op) -> ::D3D12_BLEND_OP
		{
			::D3D12_BLEND_OP d3d12_blend_op;
			switch (blend_op)
			{
			case ::framework::gpu::blend_op::add:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD;
				break;
			case ::framework::gpu::blend_op::subtract:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_SUBTRACT;
				break;
			case ::framework::gpu::blend_op::reverse_subtract:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_REV_SUBTRACT;
				break;
			case ::framework::gpu::blend_op::min:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_MIN;
				break;
			case ::framework::gpu::blend_op::max:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_MAX;
				break;
			default:
				d3d12_blend_op = ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD;
				break;
			}
			return d3d12_blend_op;
		};

		::D3D12_LOGIC_OP d3d12_logic_op;
		switch (create_info->color_blend_state->logic_op)
		{
		case ::framework::gpu::logic_op::clear:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_CLEAR;
			break;
		case ::framework::gpu::logic_op::and_:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_AND;
			break;
		case ::framework::gpu::logic_op::and_reverse:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_AND_REVERSE;
			break;
		case ::framework::gpu::logic_op::copy:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_COPY;
			break;
		case ::framework::gpu::logic_op::and_inverted:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_AND_INVERTED;
			break;
		case ::framework::gpu::logic_op::no_op:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_NOOP;
			break;
		case ::framework::gpu::logic_op::xor_:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_XOR;
			break;
		case ::framework::gpu::logic_op::or_:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_OR;
			break;
		case ::framework::gpu::logic_op::nor:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_NOR;
			break;
		case ::framework::gpu::logic_op::equivalent:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_EQUIV;
			break;
		case ::framework::gpu::logic_op::invert:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_INVERT;
			break;
		case ::framework::gpu::logic_op::or_reverse:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_OR_REVERSE;
			break;
		case ::framework::gpu::logic_op::copy_inverted:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_COPY_INVERTED;
			break;
		case ::framework::gpu::logic_op::or_inverted:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_OR_INVERTED;
			break;
		case ::framework::gpu::logic_op::nand:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_NAND;
			break;
		case ::framework::gpu::logic_op::set:
			d3d12_logic_op = ::D3D12_LOGIC_OP::D3D12_LOGIC_OP_SET;
			break;
		}

		for (::std::uint32_t i = 0; i < create_info->color_blend_state->attachment_count; i++)
		{
			::UINT8 render_target_write_mask = 0;
			if (!!(create_info->color_blend_state->attachments->color_write_mask & ::framework::gpu::color_component_flags::r_bit))
				render_target_write_mask |= D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_RED;
			if (!!(create_info->color_blend_state->attachments->color_write_mask & ::framework::gpu::color_component_flags::g_bit))
				render_target_write_mask |= D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_GREEN;
			if (!!(create_info->color_blend_state->attachments->color_write_mask & ::framework::gpu::color_component_flags::b_bit))
				render_target_write_mask |= D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_BLUE;
			if (!!(create_info->color_blend_state->attachments->color_write_mask & ::framework::gpu::color_component_flags::a_bit))
				render_target_write_mask |= D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_ALPHA;

			d3d12_blend_desc.RenderTarget[i].BlendEnable = static_cast<::BOOL>(create_info->color_blend_state->attachments->blend_enable);
			d3d12_blend_desc.RenderTarget[i].LogicOpEnable = static_cast<::BOOL>(create_info->color_blend_state->logic_op_enable);
			d3d12_blend_desc.RenderTarget[i].SrcBlend = get_d3d12_blend(create_info->color_blend_state->attachments[i].src_color_blend_factor);
			d3d12_blend_desc.RenderTarget[i].DestBlend = get_d3d12_blend(create_info->color_blend_state->attachments[i].dst_color_blend_factor);
			d3d12_blend_desc.RenderTarget[i].BlendOp = get_d3d12_blend_op(create_info->color_blend_state->attachments[i].color_blend_op);
			d3d12_blend_desc.RenderTarget[i].SrcBlendAlpha = get_d3d12_blend(create_info->color_blend_state->attachments[i].src_alpha_blend_factor);
			d3d12_blend_desc.RenderTarget[i].DestBlendAlpha = get_d3d12_blend(create_info->color_blend_state->attachments[i].dst_alpha_blend_factor);
			d3d12_blend_desc.RenderTarget[i].BlendOpAlpha = get_d3d12_blend_op(create_info->color_blend_state->attachments[i].alpha_blend_op);
			d3d12_blend_desc.RenderTarget[i].LogicOp = d3d12_logic_op;
			d3d12_blend_desc.RenderTarget[i].RenderTargetWriteMask = render_target_write_mask;
		}

		::std::uint32_t render_target_blend_state_index = create_info->color_blend_state->attachment_count;
		while (render_target_blend_state_index < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT)
		{
			d3d12_blend_desc.RenderTarget[render_target_blend_state_index++] =
			{
				FALSE,FALSE,
				::D3D12_BLEND::D3D12_BLEND_ONE, ::D3D12_BLEND::D3D12_BLEND_ZERO, ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD,
				::D3D12_BLEND::D3D12_BLEND_ONE, ::D3D12_BLEND::D3D12_BLEND_ZERO, ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD,
				::D3D12_LOGIC_OP::D3D12_LOGIC_OP_NOOP,
				::D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_ALL,
			};
		}
	}
	else
	{
		d3d12_blend_desc.AlphaToCoverageEnable = FALSE;
		d3d12_blend_desc.IndependentBlendEnable = FALSE;
		for (::UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
		{
			d3d12_blend_desc.RenderTarget[i] =
			{
				FALSE,FALSE,
				::D3D12_BLEND::D3D12_BLEND_ONE, ::D3D12_BLEND::D3D12_BLEND_ZERO, ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD,
				::D3D12_BLEND::D3D12_BLEND_ONE, ::D3D12_BLEND::D3D12_BLEND_ZERO, ::D3D12_BLEND_OP::D3D12_BLEND_OP_ADD,
				::D3D12_LOGIC_OP::D3D12_LOGIC_OP_NOOP,
				::D3D12_COLOR_WRITE_ENABLE::D3D12_COLOR_WRITE_ENABLE_ALL,
			};
		}
	}

	::D3D12_DEPTH_STENCIL_DESC d3d12_depth_stencil_desc;
	if (create_info->depth_stencil_state)
	{
		d3d12_depth_stencil_desc.DepthEnable = create_info->depth_stencil_state->depth_test_enable;
		d3d12_depth_stencil_desc.DepthFunc = get_d3d12_comparison_func(create_info->depth_stencil_state->depth_compare_op);
		d3d12_depth_stencil_desc.StencilEnable = create_info->depth_stencil_state->stencil_test_enable;
		d3d12_depth_stencil_desc.DepthWriteMask = create_info->depth_stencil_state->depth_write_enable ? ::D3D12_DEPTH_WRITE_MASK::D3D12_DEPTH_WRITE_MASK_ALL : ::D3D12_DEPTH_WRITE_MASK::D3D12_DEPTH_WRITE_MASK_ZERO;
		d3d12_depth_stencil_desc.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
		d3d12_depth_stencil_desc.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;
		d3d12_depth_stencil_desc.FrontFace.StencilDepthFailOp = get_d3d12_stencil_op(create_info->depth_stencil_state->front.depth_fail_op);
		d3d12_depth_stencil_desc.FrontFace.StencilFailOp = get_d3d12_stencil_op(create_info->depth_stencil_state->front.fail_op);
		d3d12_depth_stencil_desc.FrontFace.StencilPassOp = get_d3d12_stencil_op(create_info->depth_stencil_state->front.pass_op);
		d3d12_depth_stencil_desc.FrontFace.StencilFunc = get_d3d12_comparison_func(create_info->depth_stencil_state->front.compare_op);
		d3d12_depth_stencil_desc.BackFace.StencilDepthFailOp = get_d3d12_stencil_op(create_info->depth_stencil_state->back.depth_fail_op);
		d3d12_depth_stencil_desc.BackFace.StencilFailOp = get_d3d12_stencil_op(create_info->depth_stencil_state->back.fail_op);
		d3d12_depth_stencil_desc.BackFace.StencilPassOp = get_d3d12_stencil_op(create_info->depth_stencil_state->back.pass_op);
		d3d12_depth_stencil_desc.BackFace.StencilFunc = get_d3d12_comparison_func(create_info->depth_stencil_state->back.compare_op);
	}
	else
	{
		d3d12_depth_stencil_desc.DepthEnable = FALSE;
		d3d12_depth_stencil_desc.DepthWriteMask = ::D3D12_DEPTH_WRITE_MASK::D3D12_DEPTH_WRITE_MASK_ALL;
		d3d12_depth_stencil_desc.DepthFunc = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_LESS;
		d3d12_depth_stencil_desc.StencilEnable = FALSE;
		d3d12_depth_stencil_desc.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
		d3d12_depth_stencil_desc.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;
		d3d12_depth_stencil_desc.FrontFace = { ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_ALWAYS };
		d3d12_depth_stencil_desc.BackFace = { ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_STENCIL_OP::D3D12_STENCIL_OP_KEEP, ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_ALWAYS };
	}

	::D3D12_PRIMITIVE_TOPOLOGY_TYPE d3d12_primitive_topology_type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE::D3D12_PRIMITIVE_TOPOLOGY_TYPE_UNDEFINED;
	if (create_info->input_assembly_state)
	{
		switch (create_info->input_assembly_state->topology)
		{
		case ::framework::gpu::primitive_topology::point_list:
			d3d12_primitive_topology_type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE::D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
			break;
		case ::framework::gpu::primitive_topology::line_list:
		case ::framework::gpu::primitive_topology::line_strip:
		case ::framework::gpu::primitive_topology::line_list_with_adjacency:
		case ::framework::gpu::primitive_topology::line_strip_with_adjacency:
			d3d12_primitive_topology_type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE::D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
			break;
		case ::framework::gpu::primitive_topology::triangle_list:
		case ::framework::gpu::primitive_topology::triangle_strip:
		case ::framework::gpu::primitive_topology::triangle_fan:
		case ::framework::gpu::primitive_topology::triangle_list_with_adjacency:
		case ::framework::gpu::primitive_topology::triangle_strip_with_adjacency:
			d3d12_primitive_topology_type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			break;
		case ::framework::gpu::primitive_topology::patch_list:
			d3d12_primitive_topology_type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE::D3D12_PRIMITIVE_TOPOLOGY_TYPE_PATCH;
			break;
		}
	}

	auto & subpass_info = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_info(create_info->subpass);

	::D3D12_GRAPHICS_PIPELINE_STATE_DESC d3d12_graphics_pipeline_state_desc;
	d3d12_graphics_pipeline_state_desc.pRootSignature = static_cast<::framework::gpu::d3d12::pipeline_layout const *>(create_info->layout)->get_d3d12_root_signature();
	d3d12_graphics_pipeline_state_desc.VS = shader_codes[0] ? ::D3D12_SHADER_BYTECODE{ shader_codes[0]->GetBufferPointer(), shader_codes[0]->GetBufferSize() } : ::D3D12_SHADER_BYTECODE();
	d3d12_graphics_pipeline_state_desc.HS = shader_codes[1] ? ::D3D12_SHADER_BYTECODE{ shader_codes[1]->GetBufferPointer(), shader_codes[1]->GetBufferSize() } : ::D3D12_SHADER_BYTECODE();
	d3d12_graphics_pipeline_state_desc.DS = shader_codes[2] ? ::D3D12_SHADER_BYTECODE{ shader_codes[2]->GetBufferPointer(), shader_codes[2]->GetBufferSize() } : ::D3D12_SHADER_BYTECODE();
	d3d12_graphics_pipeline_state_desc.GS = shader_codes[3] ? ::D3D12_SHADER_BYTECODE{ shader_codes[3]->GetBufferPointer(), shader_codes[3]->GetBufferSize() } : ::D3D12_SHADER_BYTECODE();
	d3d12_graphics_pipeline_state_desc.PS = shader_codes[4] ? ::D3D12_SHADER_BYTECODE{ shader_codes[4]->GetBufferPointer(), shader_codes[4]->GetBufferSize() } : ::D3D12_SHADER_BYTECODE();
	d3d12_graphics_pipeline_state_desc.StreamOutput = {};
	d3d12_graphics_pipeline_state_desc.BlendState = d3d12_blend_desc;
	d3d12_graphics_pipeline_state_desc.SampleMask = UINT_MAX;
	d3d12_graphics_pipeline_state_desc.RasterizerState = d3d12_rasterizer_desc;
	d3d12_graphics_pipeline_state_desc.DepthStencilState = d3d12_depth_stencil_desc;
	d3d12_graphics_pipeline_state_desc.InputLayout = d3d12_input_layout_desc;
	d3d12_graphics_pipeline_state_desc.IBStripCutValue = {};
	d3d12_graphics_pipeline_state_desc.PrimitiveTopologyType = d3d12_primitive_topology_type;
	d3d12_graphics_pipeline_state_desc.NumRenderTargets = subpass_info.num_render_targets;
	for (::std::uint32_t RTVIndex = 0; RTVIndex < subpass_info.num_render_targets; RTVIndex++)
		d3d12_graphics_pipeline_state_desc.RTVFormats[RTVIndex] = ::framework::gpu::d3d12::get_dxgi_format(static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_attachment_description(subpass_info.render_targets[RTVIndex].attachment).format);
	for (::std::uint32_t RTVIndex = subpass_info.num_render_targets; RTVIndex < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; RTVIndex++)
		d3d12_graphics_pipeline_state_desc.RTVFormats[RTVIndex] = ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
	d3d12_graphics_pipeline_state_desc.DSVFormat = subpass_info.depth_stencil ? ::framework::gpu::d3d12::get_dxgi_format(static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_attachment_description(subpass_info.depth_stencil->attachment.attachment).format) : ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
	d3d12_graphics_pipeline_state_desc.SampleDesc = { 1, 0 };
	d3d12_graphics_pipeline_state_desc.NodeMask = 0;
	d3d12_graphics_pipeline_state_desc.CachedPSO = {};
	d3d12_graphics_pipeline_state_desc.Flags = D3D12_PIPELINE_STATE_FLAGS::D3D12_PIPELINE_STATE_FLAG_NONE;
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateGraphicsPipelineState(&d3d12_graphics_pipeline_state_desc, IID_PPV_ARGS(&this->d3d12_pipeline_state)));
}