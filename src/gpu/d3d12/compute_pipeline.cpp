#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::compute_pipeline::compute_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::compute_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	::Microsoft::WRL::ComPtr<::IDxcLinker> dxc_linker;

	::LPCWSTR library_name = L"library";

	result = ::framework::gpu::d3d12::throw_result(::DxcCreateInstance(::CLSID_DxcLinker, IID_PPV_ARGS(&dxc_linker)));
	result = ::framework::gpu::d3d12::throw_result(dxc_linker->RegisterLibrary(library_name, static_cast<::framework::gpu::d3d12::shader_module const *>(create_info->stage.module)->get_dxc_blob()));

	::framework::gpu::common::unique_ptr<::WCHAR[]> entry_point;
	int entry_point_len = ::MultiByteToWideChar(CP_UTF8, 0, create_info->stage.name, -1, NULL, 0);
	if (entry_point_len > 0)
	{
		entry_point = ::framework::gpu::common::make_unique<::WCHAR[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, entry_point_len);
		::MultiByteToWideChar(CP_UTF8, 0, create_info->stage.name, -1, entry_point.get(), entry_point_len);
	}

	::Microsoft::WRL::ComPtr<::IDxcOperationResult> dxc_compile_result;
	result = ::framework::gpu::d3d12::throw_result(dxc_linker->Link(entry_point.get(), L"cs_6_6", &library_name, 1, nullptr, 0, &dxc_compile_result));

	::HRESULT hresult;
	result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetStatus(&hresult));

	if (FAILED(hresult))
	{
		if (dxc_compile_result)
		{
			::Microsoft::WRL::ComPtr<::IDxcBlobEncoding> errors_blob;
			result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetErrorBuffer(&errors_blob));

			if (errors_blob)
			{
				::OutputDebugStringA(static_cast<::LPCSTR>(errors_blob->GetBufferPointer()));
			}
		}

		result = ::framework::gpu::d3d12::throw_result(hresult);
	}

	::Microsoft::WRL::ComPtr<::IDxcBlob> result_dxc_blob;
	result = ::framework::gpu::d3d12::throw_result(dxc_compile_result->GetResult(&result_dxc_blob));

	::D3D12_COMPUTE_PIPELINE_STATE_DESC d3d12_compute_pipeline_state_desc;
	d3d12_compute_pipeline_state_desc.pRootSignature = static_cast<::framework::gpu::d3d12::pipeline_layout const *>(create_info->layout)->get_d3d12_root_signature();
	d3d12_compute_pipeline_state_desc.CS = ::D3D12_SHADER_BYTECODE{ result_dxc_blob->GetBufferPointer(), result_dxc_blob->GetBufferSize() };
	d3d12_compute_pipeline_state_desc.NodeMask = 0;
	d3d12_compute_pipeline_state_desc.CachedPSO = ::D3D12_CACHED_PIPELINE_STATE();
	d3d12_compute_pipeline_state_desc.Flags = ::D3D12_PIPELINE_STATE_FLAGS::D3D12_PIPELINE_STATE_FLAG_NONE;
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateComputePipelineState(&d3d12_compute_pipeline_state_desc, IID_PPV_ARGS(&this->d3d12_pipeline_state)));
}