#pragma once

#include "gpu/acceleration_structure_geometry_set.h"
#include <vector>

namespace framework::gpu::d3d12
{
	class acceleration_structure_geometry_set final : public ::framework::gpu::acceleration_structure_geometry_set
	{
	public:
		friend class ::framework::gpu::d3d12::device;

		acceleration_structure_geometry_set(::framework::gpu::d3d12::device * device, ::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~acceleration_structure_geometry_set();

		void write_data(::framework::gpu::write_acceleration_structure_geometry const & write_info) override;

		void init_d3d12_build_raytracing_acceleration_structure_inputs(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS & d3d12_build_raytracing_acceleration_structure_inputs) const;

	private:
		struct instances_desc
		{
			::UINT num_descs;
			::D3D12_ELEMENTS_LAYOUT descs_layout;
			::D3D12_GPU_VIRTUAL_ADDRESS instance_descs;
		};

		::framework::gpu::allocation_callbacks allocator;
		::D3D12_ELEMENTS_LAYOUT descs_layout;
		::std::uint32_t const geometry_count;
		::framework::gpu::geometry_type geometry_type;
		union
		{
			::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc * instances_desc_array;
			::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc ** instances_desc_array_of_pointers;
			::D3D12_RAYTRACING_GEOMETRY_DESC * geometry_descs_array;
			::D3D12_RAYTRACING_GEOMETRY_DESC ** geometry_descs_array_of_pointers;
		};
	};
}