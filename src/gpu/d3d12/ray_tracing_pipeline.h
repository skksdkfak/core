#pragma once

#include "pipeline.h"
#include <string>

namespace framework::gpu::d3d12
{
	class ray_tracing_pipeline final : public ::framework::gpu::d3d12::pipeline
	{
	public:
		ray_tracing_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::ray_tracing_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::ID3D12StateObject * get_d3d12_state_object() const { return this->d3d12_state_object.Get(); }

		::ID3D12StateObjectProperties * get_d3d12_state_object_properties() const { return this->d3d12_state_object_properties.Get(); }

		::std::wstring const & get_group_export_name(::std::uint32_t group_index) const { return this->group_export_names[group_index]; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::Microsoft::WRL::ComPtr<::ID3D12StateObject> d3d12_state_object;
		::Microsoft::WRL::ComPtr<::ID3D12StateObjectProperties> d3d12_state_object_properties;
		::std::uint32_t group_count;
		::framework::gpu::common::unique_ptr<::std::wstring[]> group_export_names;
	};
}