#pragma once

#include "gpu/device_memory.h"

namespace framework::gpu::d3d12
{
	class device_memory final : public ::framework::gpu::device_memory
	{
	public:
		device_memory(::framework::gpu::d3d12::device * device, ::framework::gpu::memory_allocate_info const * allocate_info, ::framework::gpu::result & result);

		~device_memory();

		::framework::gpu::device_size get_size() const { return this->size; }

		::ID3D12Heap * get_d3d12_heap() const { return this->d3d12_heap; }

	private:
		friend class ::framework::gpu::d3d12::device;

		bool is_dedicated;
		::ID3D12Heap * d3d12_heap;
		::ID3D12Resource * d3d12_committed_resource;
		::framework::gpu::device_size size;
	};
}