#pragma once

#include "gpu/pipeline_layout.h"
#include <vector>
#include <array>
#include <unordered_map>

namespace framework::gpu::d3d12
{
	class pipeline_layout final : public ::framework::gpu::pipeline_layout
	{
	public:
		pipeline_layout(::framework::gpu::d3d12::device * device, ::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~pipeline_layout();

		::std::uint32_t get_descriptor_set_layout_count() const { return descriptor_set_layout_count; }

		::UINT get_descriptor_count(::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type) const { return descriptor_count[d3d12_descriptor_heap_type]; }

		::UINT get_dynamic_descriptor_count() const { return dynamic_descriptor_count; }

		::std::uint32_t get_push_constants_global_offset(::std::uint32_t binding, ::D3D12_SHADER_VISIBILITY d3d12_shader_visibility) const { return push_constant_range_root_indices.at(binding)[d3d12_shader_visibility]; }

		::ID3D12RootSignature * get_d3d12_root_signature() const { return this->d3d12_root_signature; }

	private:
		friend class ::framework::gpu::d3d12::device;
		friend class ::framework::gpu::d3d12::command_buffer;

		struct descriptor_table_root_parameter
		{
			::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type;
			::UINT root_index;
		};

		struct dynamic_descriptor_root_parameter
		{
			::UINT root_index;
			::UINT offset_from_start;
			::UINT visibility_count;
			::UINT descriptor_count;
			::UINT dynamic_offset;
		};

		::framework::gpu::allocation_callbacks const allocator;
		::ID3D12RootSignature * d3d12_root_signature;
		::UINT descriptor_count[2];
		::UINT dynamic_descriptor_count;
		::std::vector<::UINT> descriptor_set_offset[2];
		::std::vector<::std::vector<::framework::gpu::d3d12::pipeline_layout::descriptor_table_root_parameter>> root_descriptor_tables;
		::std::vector<::std::vector<::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter>> root_constant_buffer_views;
		::std::vector<::std::vector<::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter>> root_shader_resource_views;
		::std::vector<::std::vector<::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter>> root_unordered_access_views;
		::std::unordered_map<::std::uint32_t, ::std::array<::std::uint32_t, 6>> push_constant_range_root_indices;
		::std::uint32_t descriptor_set_layout_count;
	};
}