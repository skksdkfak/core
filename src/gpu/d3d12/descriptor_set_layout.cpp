#include "gpu/d3d12/core.hpp"
#include <bit>

::framework::gpu::d3d12::descriptor_set_layout::descriptor_set_layout(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	binding_count(0),
	descriptor_count{},
	dynamic_descriptor_count(0),
	flags(create_info->flags),
	immutable_sampler_count(0),
	d3d12_descriptor_range_count{}
{
	if (create_info->binding_count > 0)
	{
		for (::std::uint32_t i = 0; i < create_info->binding_count; i++)
		{
			if (create_info->bindings[i].binding > this->binding_count)
			{
				this->binding_count = create_info->bindings[i].binding;
			}
		}

		this->binding_count++;
		this->descriptor_bindings = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::descriptor_set_layout::binding[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->binding_count);

		for (::std::uint32_t i = 0; i < create_info->binding_count; i++)
		{
			::framework::gpu::d3d12::descriptor_set_layout::binding & binding = this->descriptor_bindings[create_info->bindings[i].binding];
			binding.descriptor_count = create_info->bindings[i].descriptor_count;
			binding.base_shader_register = create_info->bindings[i].hlsl_shader_register;
			binding.register_space = create_info->bindings[i].hlsl_register_space;
			binding.descriptor_type = create_info->bindings[i].descriptor_type;
			binding.immutable_samplers = nullptr;

			binding.d3d12_shader_visibility_bits = ::framework::gpu::d3d12::get_d3d12_shader_visibility_bits(create_info->bindings[i].stage_flags);

			switch (create_info->bindings[i].descriptor_type)
			{
			case ::framework::gpu::descriptor_type::sampler:
				if (create_info->bindings[i].immutable_samplers)
				{
					binding.immutable_samplers = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::sampler * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->bindings[i].descriptor_count);
					for (::std::uint32_t j = 0; j < create_info->bindings[i].descriptor_count; j++)
					{
						binding.immutable_samplers[j] = static_cast<::framework::gpu::d3d12::sampler *>(create_info->bindings[i].immutable_samplers[j]);
					}
				}
				else
				{
					binding.descriptor_range = { this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER], ::framework::gpu::d3d12::get_d3d12_descriptor_range_flags(create_info->bindings[i].flags) };
					this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] += create_info->bindings[i].descriptor_count;

					for (::std::uint32_t bits = binding.d3d12_shader_visibility_bits, bit_index; bits;)
					{
						bit_index = ::std::countr_zero(bits);
						bits >>= (bit_index + 1);
						this->d3d12_descriptor_range_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER][bit_index]++;
					}
				}
				break;
			case ::framework::gpu::descriptor_type::combined_image_sampler:
				binding.combined_image_sampler = { this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER], this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV], ::framework::gpu::d3d12::get_d3d12_descriptor_range_flags(create_info->bindings[i].flags) };
				this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] += create_info->bindings[i].descriptor_count;

				for (::std::uint32_t bits = binding.d3d12_shader_visibility_bits, bit_index; bits;)
				{
					bit_index = ::std::countr_zero(bits);
					bits >>= (bit_index + 1);
					this->d3d12_descriptor_range_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV][bit_index]++;
				}

				if (create_info->bindings[i].immutable_samplers)
				{
					binding.immutable_samplers = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::sampler * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->bindings[i].descriptor_count);
					for (::std::uint32_t j = 0; j < create_info->bindings[i].descriptor_count; j++)
					{
						binding.immutable_samplers[j] = static_cast<::framework::gpu::d3d12::sampler *>(create_info->bindings[i].immutable_samplers[j]);
					}
				}
				else
				{
					this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER] += create_info->bindings[i].descriptor_count;

					for (::std::uint32_t bits = binding.d3d12_shader_visibility_bits, bit_index; bits;)
					{
						bit_index = ::std::countr_zero(bits);
						bits >>= (bit_index + 1);
						this->d3d12_descriptor_range_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER][bit_index]++;
					}
				}
				break;
			case ::framework::gpu::descriptor_type::sampled_image:
			case ::framework::gpu::descriptor_type::storage_image:
			case ::framework::gpu::descriptor_type::uniform_texel_buffer:
			case ::framework::gpu::descriptor_type::storage_texel_buffer:
			case ::framework::gpu::descriptor_type::uniform_buffer:
			case ::framework::gpu::descriptor_type::sampled_buffer:
			case ::framework::gpu::descriptor_type::storage_buffer:
			case ::framework::gpu::descriptor_type::input_attachment:
			case ::framework::gpu::descriptor_type::acceleration_structure:
				binding.descriptor_range = { this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV], ::framework::gpu::d3d12::get_d3d12_descriptor_range_flags(create_info->bindings[i].flags) };
				this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV] += create_info->bindings[i].descriptor_count;

				for (::std::uint32_t bits = binding.d3d12_shader_visibility_bits, bit_index; bits;)
				{
					bit_index = ::std::countr_zero(bits);
					bits >>= (bit_index + 1);
					this->d3d12_descriptor_range_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV][bit_index]++;
				}
				break;
			case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
			case ::framework::gpu::descriptor_type::sampled_buffer_dynamic:
			case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
				binding.root_parameter = { dynamic_descriptor_count, ::framework::gpu::d3d12::get_d3d12_root_descriptor_flags(create_info->bindings[i].flags) };
				this->dynamic_descriptor_count += create_info->bindings[i].descriptor_count;
				break;
			case ::framework::gpu::descriptor_type::color_attachment:
				binding.descriptor_range = { this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV], ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_NONE };
				this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_RTV] += create_info->bindings[i].descriptor_count;
				break;
			case ::framework::gpu::descriptor_type::depth_stencil_attachment:
				binding.descriptor_range = { this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV], ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_NONE };
				this->descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_DSV] += create_info->bindings[i].descriptor_count;
				break;
			}

			if (binding.immutable_samplers)
			{
				this->immutable_sampler_count += ::std::popcount(binding.d3d12_shader_visibility_bits) * binding.descriptor_count;
			}
		}
	}

	result = ::framework::gpu::result::success;
}