#include "gpu/d3d12/core.hpp"
#include <bit>

::framework::gpu::d3d12::pipeline_layout::pipeline_layout(::framework::gpu::d3d12::device * device, ::framework::gpu::pipeline_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	descriptor_count{ 0 },
	dynamic_descriptor_count(0)
{
	this->descriptor_set_layout_count = create_info->descriptor_set_layout_count;
	this->descriptor_set_offset[0].resize(create_info->descriptor_set_layout_count);
	this->descriptor_set_offset[1].resize(create_info->descriptor_set_layout_count);
	this->root_descriptor_tables.resize(create_info->descriptor_set_layout_count);
	this->root_constant_buffer_views.resize(create_info->descriptor_set_layout_count);
	this->root_shader_resource_views.resize(create_info->descriptor_set_layout_count);
	this->root_unordered_access_views.resize(create_info->descriptor_set_layout_count);

	::std::vector<::D3D12_ROOT_PARAMETER1> d3d12_root_parameters;
	::std::vector<::D3D12_DESCRIPTOR_RANGE1> d3d12_descriptor_ranges[2][8];
	::UINT set_descriptor_range_begin[2][8]{};
	::std::uint32_t immutable_sampler_count = 0;

	for (::std::uint32_t set = 0; set < create_info->descriptor_set_layout_count; set++)
	{
		for (::std::uint32_t d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < 2; d3d12_descriptor_heap_type++)
		{
			this->descriptor_set_offset[d3d12_descriptor_heap_type][set] = this->descriptor_count[d3d12_descriptor_heap_type];
			this->descriptor_count[d3d12_descriptor_heap_type] += static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(create_info->descriptor_set_layouts[set])->get_descriptor_count((::D3D12_DESCRIPTOR_HEAP_TYPE)d3d12_descriptor_heap_type);

			for (::std::uint32_t d3d12_shader_visibility = 0; d3d12_shader_visibility < 8; d3d12_shader_visibility++)
			{
				set_descriptor_range_begin[d3d12_descriptor_heap_type][d3d12_shader_visibility] += static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(create_info->descriptor_set_layouts[set])->d3d12_descriptor_range_count[d3d12_descriptor_heap_type][d3d12_shader_visibility];
			}
		}
		immutable_sampler_count += static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(create_info->descriptor_set_layouts[set])->immutable_sampler_count;
	}

	for (::std::uint32_t d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < 2; d3d12_descriptor_heap_type++)
	{
		for (::std::uint32_t d3d12_shader_visibility = 0; d3d12_shader_visibility < 8; d3d12_shader_visibility++)
		{
			d3d12_descriptor_ranges[d3d12_descriptor_heap_type][d3d12_shader_visibility].reserve(set_descriptor_range_begin[d3d12_descriptor_heap_type][d3d12_shader_visibility]);
			set_descriptor_range_begin[d3d12_descriptor_heap_type][d3d12_shader_visibility] = 0;
		}
	}

	::framework::gpu::common::unique_ptr<::D3D12_STATIC_SAMPLER_DESC[]> d3d12_static_sampler_descs;
	if (immutable_sampler_count)
	{
		d3d12_static_sampler_descs = ::framework::gpu::common::make_unique<::D3D12_STATIC_SAMPLER_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, immutable_sampler_count);
		immutable_sampler_count = 0;
	}

	for (::std::uint32_t set = 0; set < create_info->descriptor_set_layout_count; set++)
	{
		for (::std::uint32_t binding = 0; binding < static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(create_info->descriptor_set_layouts[set])->binding_count; binding++)
		{
			auto & binding_info = static_cast<::framework::gpu::d3d12::descriptor_set_layout const *>(create_info->descriptor_set_layouts[set])->get_binding(binding);

			auto const init_as_descriptor_table = [&](::D3D12_DESCRIPTOR_RANGE_TYPE d3d12_descriptor_range_type, ::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type, ::std::uint32_t base_shader_register, ::std::uint32_t register_space, ::UINT offset_from_start, ::D3D12_DESCRIPTOR_RANGE_FLAGS flags) -> void
			{
				for (::std::uint32_t bits = binding_info.d3d12_shader_visibility_bits, bit_index; bits;)
				{
					bit_index = ::std::countr_zero(bits);
					bits >>= (bit_index + 1);

					d3d12_descriptor_ranges[d3d12_descriptor_heap_type][bit_index].emplace_back(
						::D3D12_DESCRIPTOR_RANGE1
						{
							d3d12_descriptor_range_type,
							binding_info.descriptor_count,
							base_shader_register,
							register_space,
							flags,
							offset_from_start
						}
					);
				}
			};

			auto const init_as_dynamic_view = [&](::D3D12_ROOT_PARAMETER_TYPE parameterType, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags) -> void
			{
				for (::std::uint32_t bits = binding_info.d3d12_shader_visibility_bits, bit_index; bits;)
				{
					bit_index = ::std::countr_zero(bits);
					bits >>= (bit_index + 1);

					for (::std::uint32_t shader_register = binding_info.base_shader_register; shader_register < binding_info.base_shader_register + binding_info.descriptor_count; shader_register++)
					{
						::D3D12_ROOT_PARAMETER1 d3d12_root_parameter;
						d3d12_root_parameter.ParameterType = parameterType;
						d3d12_root_parameter.Descriptor.ShaderRegister = shader_register;
						d3d12_root_parameter.Descriptor.RegisterSpace = binding_info.register_space;
						d3d12_root_parameter.Descriptor.Flags = flags;
						d3d12_root_parameter.ShaderVisibility = static_cast<::D3D12_SHADER_VISIBILITY>(bit_index);
						d3d12_root_parameters.push_back(d3d12_root_parameter);
					}
				}
				this->dynamic_descriptor_count += binding_info.descriptor_count;
			};

			auto const init_as_static_sampler = [&](::std::uint32_t base_shader_register, ::std::uint32_t register_space) -> void
			{
				for (::std::uint32_t bits = binding_info.d3d12_shader_visibility_bits, bit_index; bits;)
				{
					bit_index = ::std::countr_zero(bits);
					bits >>= (bit_index + 1);

					for (::std::uint32_t j = 0; j < binding_info.descriptor_count; j++)
					{
						::D3D12_SAMPLER_DESC const & d3d12_sampler_desc = binding_info.immutable_samplers[j]->get_d3d12_sampler_desc();

						d3d12_static_sampler_descs[immutable_sampler_count].Filter = d3d12_sampler_desc.Filter;
						d3d12_static_sampler_descs[immutable_sampler_count].AddressU = d3d12_sampler_desc.AddressU;
						d3d12_static_sampler_descs[immutable_sampler_count].AddressV = d3d12_sampler_desc.AddressV;
						d3d12_static_sampler_descs[immutable_sampler_count].AddressW = d3d12_sampler_desc.AddressW;
						d3d12_static_sampler_descs[immutable_sampler_count].MipLODBias = d3d12_sampler_desc.MipLODBias;
						d3d12_static_sampler_descs[immutable_sampler_count].MaxAnisotropy = d3d12_sampler_desc.MaxAnisotropy;
						d3d12_static_sampler_descs[immutable_sampler_count].ComparisonFunc = d3d12_sampler_desc.ComparisonFunc;
						d3d12_static_sampler_descs[immutable_sampler_count].BorderColor = binding_info.immutable_samplers[j]->get_d3d12_static_border_color();
						d3d12_static_sampler_descs[immutable_sampler_count].MinLOD = d3d12_sampler_desc.MinLOD;
						d3d12_static_sampler_descs[immutable_sampler_count].MaxLOD = d3d12_sampler_desc.MaxLOD;
						d3d12_static_sampler_descs[immutable_sampler_count].ShaderRegister = binding_info.base_shader_register + binding_info.base_shader_register;
						d3d12_static_sampler_descs[immutable_sampler_count].RegisterSpace = binding_info.register_space;
						d3d12_static_sampler_descs[immutable_sampler_count].ShaderVisibility = static_cast<::D3D12_SHADER_VISIBILITY>(bit_index);
						immutable_sampler_count++;
					}
				}
			};

			switch (binding_info.descriptor_type)
			{
			case ::framework::gpu::descriptor_type::combined_image_sampler:
				if (binding_info.immutable_samplers)
					init_as_static_sampler(binding_info.base_shader_register, binding_info.register_space);
				else
					init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER, binding_info.base_shader_register, binding_info.register_space, binding_info.combined_image_sampler.offset_from_start_sampler, binding_info.combined_image_sampler.flags);
				init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, binding_info.base_shader_register, binding_info.register_space, binding_info.combined_image_sampler.offset_from_start_image, binding_info.combined_image_sampler.flags);
				break;
			case ::framework::gpu::descriptor_type::sampler:
				if (binding_info.immutable_samplers)
					init_as_static_sampler(binding_info.base_shader_register, binding_info.register_space);
				else
					init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER, binding_info.base_shader_register, binding_info.register_space, binding_info.descriptor_range.offset_from_start, binding_info.descriptor_range.flags);
				break;
			case ::framework::gpu::descriptor_type::uniform_buffer:
				init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_CBV, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, binding_info.base_shader_register, binding_info.register_space, binding_info.descriptor_range.offset_from_start, binding_info.descriptor_range.flags);
				break;
			case ::framework::gpu::descriptor_type::uniform_texel_buffer:
			case ::framework::gpu::descriptor_type::sampled_image:
			case ::framework::gpu::descriptor_type::sampled_buffer:
			case ::framework::gpu::descriptor_type::input_attachment:
			case ::framework::gpu::descriptor_type::acceleration_structure:
				init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, binding_info.base_shader_register, binding_info.register_space, binding_info.descriptor_range.offset_from_start, binding_info.descriptor_range.flags);
				break;
			case ::framework::gpu::descriptor_type::storage_texel_buffer:
			case ::framework::gpu::descriptor_type::storage_image:
			case ::framework::gpu::descriptor_type::storage_buffer:
				init_as_descriptor_table(::D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_UAV, ::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, binding_info.base_shader_register, binding_info.register_space, binding_info.descriptor_range.offset_from_start, binding_info.descriptor_range.flags);
				break;
			case ::framework::gpu::descriptor_type::uniform_buffer_dynamic:
				this->root_constant_buffer_views[set].emplace_back(::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter{ static_cast<::UINT>(d3d12_root_parameters.size()), binding_info.root_parameter.offset_from_start, static_cast<::UINT>(::std::popcount(binding_info.d3d12_shader_visibility_bits)), binding_info.descriptor_count, this->dynamic_descriptor_count });
				init_as_dynamic_view(::D3D12_ROOT_PARAMETER_TYPE::D3D12_ROOT_PARAMETER_TYPE_CBV, binding_info.root_parameter.flags);
				break;
			case ::framework::gpu::descriptor_type::sampled_buffer_dynamic:
				this->root_shader_resource_views[set].emplace_back(::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter{ static_cast<::UINT>(d3d12_root_parameters.size()), binding_info.root_parameter.offset_from_start, static_cast<::UINT>(::std::popcount(binding_info.d3d12_shader_visibility_bits)), binding_info.descriptor_count, this->dynamic_descriptor_count });
				init_as_dynamic_view(::D3D12_ROOT_PARAMETER_TYPE::D3D12_ROOT_PARAMETER_TYPE_SRV, binding_info.root_parameter.flags);
				break;
			case ::framework::gpu::descriptor_type::storage_buffer_dynamic:
				this->root_unordered_access_views[set].emplace_back(::framework::gpu::d3d12::pipeline_layout::dynamic_descriptor_root_parameter{ static_cast<::UINT>(d3d12_root_parameters.size()), binding_info.root_parameter.offset_from_start, static_cast<::UINT>(::std::popcount(binding_info.d3d12_shader_visibility_bits)), binding_info.descriptor_count, this->dynamic_descriptor_count });
				init_as_dynamic_view(::D3D12_ROOT_PARAMETER_TYPE::D3D12_ROOT_PARAMETER_TYPE_UAV, binding_info.root_parameter.flags);
				break;
			}
		}

		for (::std::uint32_t d3d12_descriptor_heap_type = 0; d3d12_descriptor_heap_type < 2; d3d12_descriptor_heap_type++)
		{
			for (::std::uint32_t visibility = 0; visibility < 8; visibility++)
			{
				auto & d3d12_descriptor_range = d3d12_descriptor_ranges[d3d12_descriptor_heap_type][visibility];
				::UINT const descriptor_range_begin = set_descriptor_range_begin[d3d12_descriptor_heap_type][visibility];
				::UINT const descriptor_range_count = static_cast<::UINT>(d3d12_descriptor_range.size()) - descriptor_range_begin;
				if (descriptor_range_count)
				{
					this->root_descriptor_tables[set].emplace_back(::framework::gpu::d3d12::pipeline_layout::descriptor_table_root_parameter{ static_cast<::D3D12_DESCRIPTOR_HEAP_TYPE>(d3d12_descriptor_heap_type), static_cast<::UINT>(d3d12_root_parameters.size()) });

					::D3D12_ROOT_PARAMETER1 d3d12_root_parameter;
					d3d12_root_parameter.ParameterType = ::D3D12_ROOT_PARAMETER_TYPE::D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
					d3d12_root_parameter.DescriptorTable.NumDescriptorRanges = descriptor_range_count;
					d3d12_root_parameter.DescriptorTable.pDescriptorRanges = &d3d12_descriptor_range[descriptor_range_begin];
					d3d12_root_parameter.ShaderVisibility = static_cast<::D3D12_SHADER_VISIBILITY>(visibility);
					d3d12_root_parameters.push_back(d3d12_root_parameter);

					set_descriptor_range_begin[d3d12_descriptor_heap_type][visibility] += descriptor_range_count;
				}
			}
		}
	}

	for (::std::uint32_t i = 0; i < create_info->push_constant_range_count; i++)
	{
		::std::uint32_t const size = create_info->push_constant_ranges[i].size;
		::UINT const num_32_bit_values = size / 4 + ((size % 4) > 0 ? 1 : 0);
		::std::uint32_t const d3d12_shader_visibility_bits = ::framework::gpu::d3d12::get_d3d12_shader_visibility_bits(create_info->push_constant_ranges[i].stage_flags);

		for (::std::uint32_t bits = d3d12_shader_visibility_bits, bit_index; bits;)
		{
			bit_index = ::std::countr_zero(bits);
			bits >>= (bit_index + 1);

			this->push_constant_range_root_indices[create_info->push_constant_ranges[i].binding][bit_index] = static_cast<::UINT>(d3d12_root_parameters.size());

			::D3D12_ROOT_PARAMETER1 d3d12_root_parameter;
			d3d12_root_parameter.ParameterType = ::D3D12_ROOT_PARAMETER_TYPE::D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
			d3d12_root_parameter.Constants.ShaderRegister = static_cast<::UINT>(create_info->push_constant_ranges[i].hlsl_shader_register);
			d3d12_root_parameter.Constants.RegisterSpace = static_cast<::UINT>(create_info->push_constant_ranges[i].hlsl_register_space);
			d3d12_root_parameter.Constants.Num32BitValues = num_32_bit_values;
			d3d12_root_parameter.ShaderVisibility = static_cast<::D3D12_SHADER_VISIBILITY>(bit_index);
			d3d12_root_parameters.push_back(d3d12_root_parameter);
		}
	}

	::D3D12_ROOT_SIGNATURE_FLAGS d3d12_root_signature_flags = ::D3D12_ROOT_SIGNATURE_FLAGS::D3D12_ROOT_SIGNATURE_FLAG_NONE;
	if (!!(create_info->flags & ::framework::gpu::pipeline_layout_create_flags::local_bit))
		d3d12_root_signature_flags |= ::D3D12_ROOT_SIGNATURE_FLAGS::D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;
	else // todo
		d3d12_root_signature_flags |= ::D3D12_ROOT_SIGNATURE_FLAGS::D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

	::D3D12_VERSIONED_ROOT_SIGNATURE_DESC d3d12_versioned_root_signature_desc;
	d3d12_versioned_root_signature_desc.Version = ::D3D_ROOT_SIGNATURE_VERSION::D3D_ROOT_SIGNATURE_VERSION_1_1;
	d3d12_versioned_root_signature_desc.Desc_1_1.NumParameters = static_cast<::UINT>(d3d12_root_parameters.size());
	d3d12_versioned_root_signature_desc.Desc_1_1.pParameters = d3d12_root_parameters.data();
	d3d12_versioned_root_signature_desc.Desc_1_1.NumStaticSamplers = immutable_sampler_count;
	d3d12_versioned_root_signature_desc.Desc_1_1.pStaticSamplers = d3d12_static_sampler_descs.get();
	d3d12_versioned_root_signature_desc.Desc_1_1.Flags = d3d12_root_signature_flags;

	::Microsoft::WRL::ComPtr<::ID3DBlob> blob_with_root_signature;
	::Microsoft::WRL::ComPtr<::ID3DBlob> error_blob;
	::HRESULT hresult = ::D3D12SerializeVersionedRootSignature(&d3d12_versioned_root_signature_desc, &blob_with_root_signature, &error_blob);
	if (FAILED(hresult))
	{
		::OutputDebugString(static_cast<char const *>(error_blob->GetBufferPointer()));

		result = ::framework::gpu::d3d12::throw_result(hresult);
	}

	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateRootSignature(0, blob_with_root_signature->GetBufferPointer(), blob_with_root_signature->GetBufferSize(), IID_PPV_ARGS(&this->d3d12_root_signature)));

}

::framework::gpu::d3d12::pipeline_layout::~pipeline_layout()
{
	if (this->d3d12_root_signature)
		this->d3d12_root_signature->Release();
}