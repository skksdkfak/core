#define NOMINMAX
#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::queue::queue(::framework::gpu::d3d12::device * device, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index, float queue_priority, ::framework::gpu::result & result) :
	next_fence_value(1)
{
	::D3D12_COMMAND_QUEUE_DESC d3d12_command_queue_desc;
	d3d12_command_queue_desc.Type = ::framework::gpu::d3d12::get_d3d12_command_list_type(queue_family_index);
	d3d12_command_queue_desc.Priority = static_cast<::INT>(queue_priority * 100);
	d3d12_command_queue_desc.Flags = ::D3D12_COMMAND_QUEUE_FLAGS::D3D12_COMMAND_QUEUE_FLAG_NONE;
	d3d12_command_queue_desc.NodeMask = 0;
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateCommandQueue(&d3d12_command_queue_desc, IID_PPV_ARGS(this->d3d12_command_queue.GetAddressOf())));
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateFence(0, ::D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(this->d3d12_fence.GetAddressOf())));
}

::framework::gpu::d3d12::queue::~queue()
{
}

::framework::gpu::result(::framework::gpu::d3d12::queue::submit)(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		for (::std::uint32_t i = 0; i < submit_count; i++)
		{
			for (::std::uint32_t j = 0; j < submits[i].wait_semaphore_info_count; j++)
			{
				::framework::gpu::d3d12::semaphore * semaphore = static_cast<::framework::gpu::d3d12::semaphore *>(submits[i].wait_semaphore_infos[j].semaphore);
				result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_queue->Wait(semaphore->get_d3d12_fence(), semaphore->get_is_binary_semaphore() ? semaphore->increment_fence_value() : submits[i].wait_semaphore_infos[j].value));
			}

			::std::vector<::ID3D12CommandList *> d3d12_command_lists;
			for (::std::uint32_t j = 0; j < submits[i].command_buffer_info_count; j++)
			{
				::framework::gpu::d3d12::primary_command_buffer const * command_buffer = static_cast<::framework::gpu::d3d12::primary_command_buffer const *>(submits[i].command_buffer_infos[j].command_buffer);
				d3d12_command_lists.insert(d3d12_command_lists.end(), command_buffer->get_command_list_batch(), command_buffer->get_command_list_batch() + command_buffer->get_command_list_batch_size());
			}

			this->d3d12_command_queue->ExecuteCommandLists(d3d12_command_lists.size(), d3d12_command_lists.data());

			for (::std::uint32_t j = 0; j < submits[i].signal_semaphore_info_count; j++)
			{
				::framework::gpu::d3d12::semaphore * semaphore = static_cast<::framework::gpu::d3d12::semaphore *>(submits[i].signal_semaphore_infos[j].semaphore);
				result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_queue->Signal(semaphore->get_d3d12_fence(), semaphore->get_is_binary_semaphore() ? semaphore->get_fence_value() : submits[i].signal_semaphore_infos[j].value));
			}
		}

		if (fence)
		{
			result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_queue->Signal(static_cast<::framework::gpu::d3d12::fence *>(fence)->get_d3d12_fence(), static_cast<::framework::gpu::d3d12::fence *>(fence)->get_fence_value()));
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::queue::present)(::framework::gpu::present_info const * present_info)
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		for (::std::uint32_t i = 0; i < present_info->wait_semaphore_count; i++)
		{
			result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_queue->Wait(static_cast<::framework::gpu::d3d12::semaphore const *>(present_info->wait_semaphores[i])->get_d3d12_fence(), static_cast<::framework::gpu::d3d12::semaphore const *>(present_info->wait_semaphores[i])->get_fence_value() - 1));
		}

		for (::std::uint32_t i = 0; i < present_info->swap_chain_count; i++)
		{
			result = static_cast<::framework::gpu::d3d12::swap_chain const *>(present_info->swap_chains[i])->present();

			if (present_info->results)
			{
				present_info->results[i] = result;
			}
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::queue::wait_idle)()
{
	::framework::gpu::result result = ::framework::gpu::result::success;

	try
	{
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_queue->Signal(this->d3d12_fence.Get(), this->next_fence_value));
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_fence->SetEventOnCompletion(this->next_fence_value, NULL));
		this->next_fence_value++;
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::queue::get_timestamp_period)(float * timestamp_period)
{
	::UINT64 frequency;
	::HRESULT hresult = this->d3d12_command_queue->GetTimestampFrequency(&frequency);

	*timestamp_period = 1e9 / static_cast<double>(frequency);

	return ::framework::gpu::d3d12::get_result(hresult);
}