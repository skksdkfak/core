#include "gpu/d3d12/core.hpp"

namespace framework::gpu::d3d12
{
	const ::framework::gpu::queue_family_properties queue_family_properties[::framework::gpu::d3d12::queue_family_count]
	{
		{
			.queue_flags = ::framework::gpu::queue_flags::graphics_bit,
			.queue_count = 1,
			.timestamp_valid_bits = 64,
			.min_image_transfer_granularity
			{
				.width = 1,
				.height = 1,
				.depth = 1
			}
		},
		{
			.queue_flags = ::framework::gpu::queue_flags::compute_bit,
			.queue_count = 1,
			.timestamp_valid_bits = 64,
			.min_image_transfer_granularity
			{
				.width = 1,
				.height = 1,
				.depth = 1
			}
		},
		{
			.queue_flags = ::framework::gpu::queue_flags::transfer_bit,
			.queue_count = 1,
			.timestamp_valid_bits = 64,
			.min_image_transfer_granularity
			{
				.width = 1,
				.height = 1,
				.depth = 1
			}
		}
	};

	::DXGI_FORMAT FindDepthStencilParentDXGIFormat(::DXGI_FORMAT InFormat)
	{
		switch (InFormat)
		{
		case ::DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_X24_TYPELESS_G8_UINT:
			return ::DXGI_FORMAT::DXGI_FORMAT_R24G8_TYPELESS;
#if DEPTH_32_BIT_CONVERSION
			// Changing Depth Buffers to 32 bit on Dingo as D24S8 is actually implemented as a 32 bit buffer in the hardware
		case ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G8X24_TYPELESS;
#endif
		case ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS;
		case ::DXGI_FORMAT::DXGI_FORMAT_D16_UNORM:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16_TYPELESS;
		};
		return InFormat;
	}

	::std::uint8_t GetPlaneCount(::DXGI_FORMAT format)
	{
		switch (FindDepthStencilParentDXGIFormat(format))
		{
		case ::DXGI_FORMAT::DXGI_FORMAT_R24G8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G8X24_TYPELESS:
			return 2;
		default:
			return 1;
		}
	}

	bool IsDepthStencilFormat(::framework::gpu::format format)
	{
		switch (format)
		{
		case ::framework::gpu::format::d16_unorm:
		case ::framework::gpu::format::x8_d24_unorm_pack32:
		case ::framework::gpu::format::d32_sfloat:
		case ::framework::gpu::format::s8_uint:
		case ::framework::gpu::format::d16_unorm_s8_uint:
		case ::framework::gpu::format::d24_unorm_s8_uint:
		case ::framework::gpu::format::d32_sfloat_s8_uint:
			return true;
		default:
			return false;
		};
	}

	::UINT calc_row_pitch(::DXGI_FORMAT dxgi_format, ::UINT width)
	{
		bool bc = false;
		bool packed = false;
		bool planar = false;
		::std::size_t bpe = 0;

		switch (dxgi_format)
		{
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_SNORM:
			bc = true;
			bpe = 8;
			break;
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_UF16:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_SF16:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM_SRGB:
			bc = true;
			bpe = 16;
			break;
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_B8G8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_G8R8_G8B8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_YUY2:
			packed = true;
			bpe = 4;
			break;
		case ::DXGI_FORMAT::DXGI_FORMAT_Y210:
		case ::DXGI_FORMAT::DXGI_FORMAT_Y216:
			packed = true;
			bpe = 8;
			break;
		case ::DXGI_FORMAT::DXGI_FORMAT_NV12:
		case ::DXGI_FORMAT::DXGI_FORMAT_420_OPAQUE:
			planar = true;
			bpe = 2;
			break;
		case ::DXGI_FORMAT::DXGI_FORMAT_P010:
		case ::DXGI_FORMAT::DXGI_FORMAT_P016:
			planar = true;
			bpe = 4;
			break;
		}

		if (bc)
		{
			::std::size_t num_blocks_wide = 0;
			if (width > 0)
			{
				num_blocks_wide = ::std::max<::std::size_t>(1, (width + 3) / 4);
			}

			return num_blocks_wide * bpe;
		}
		else if (packed)
		{
			return ((width + 1) >> 1) * bpe;
		}
		else if (dxgi_format == ::DXGI_FORMAT::DXGI_FORMAT_NV11)
		{
			return ((width + 3) >> 2) * 4;
		}
		else if (planar)
		{
			return ((width + 1) >> 1) * bpe;
		}
		else
		{
			::std::size_t bpp = ::framework::gpu::d3d12::bits_per_pixel(dxgi_format);
			return (width * bpp + 7) / 8;
		}
	}
}