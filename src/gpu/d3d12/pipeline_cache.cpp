#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::pipeline_cache::pipeline_cache(::framework::gpu::d3d12::device * device, ::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result)
{
	::HRESULT hresult;

	hresult = device->get_d3d12_device()->CreatePipelineLibrary(create_info->initial_data, create_info->initial_data_size, IID_PPV_ARGS(&this->d3d12_pipeline_library));

	result = ::framework::gpu::d3d12::get_result(hresult);
}

::framework::gpu::d3d12::pipeline_cache::~pipeline_cache()
{
	this->d3d12_pipeline_library->Release();
}