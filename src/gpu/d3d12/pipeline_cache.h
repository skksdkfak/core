#pragma once

#include "gpu/pipeline_cache.h"

namespace framework::gpu::d3d12
{
	class pipeline_cache : public ::framework::gpu::pipeline_cache
	{
	public:
		pipeline_cache(::framework::gpu::d3d12::device * device, ::framework::gpu::pipeline_cache_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~pipeline_cache();

		::ID3D12PipelineLibrary * get_d3d12_pipeline_library() const { return this->d3d12_pipeline_library; }

	protected:
		friend class ::framework::gpu::d3d12::device;

		::ID3D12PipelineLibrary * d3d12_pipeline_library;
	};
}