#include "platform/win32/window.hpp"
#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::swap_chain::swap_chain(::framework::gpu::d3d12::device * device, ::framework::gpu::swap_chain_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	device(device),
	images(nullptr, { *this })
{
	this->window = static_cast<::framework::gpu::d3d12::surface *>(create_info->surface)->get_window();
	this->image_extent = create_info->image_extent;

	if (create_info->old_swap_chain)
	{
		for (::std::uint32_t i = 0; i < static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->image_count; i++)
		{
			static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->images[i]->get_d3d12_resource()->Release();
			static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->images[i]->d3d12_resource = nullptr;
		}

		result = ::framework::gpu::d3d12::throw_result(static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->dxgi_swap_chain->ResizeBuffers(
			create_info->min_image_count,
			create_info->image_extent.width,
			create_info->image_extent.height,
			::framework::gpu::d3d12::get_dxgi_format(create_info->image_format),
			::DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH | ::DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT
		));

		this->dxgi_swap_chain = static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->dxgi_swap_chain;
		this->frame_latency_waitable_object = static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->frame_latency_waitable_object;

		static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->dxgi_swap_chain = NULL;
		static_cast<::framework::gpu::d3d12::swap_chain *>(create_info->old_swap_chain)->frame_latency_waitable_object = NULL;
	}
	else
	{
		::DXGI_USAGE dxgi_buffer_usage = 0;
		if (!!(create_info->image_usage & ::framework::gpu::image_usage_flags::sampled_bit))
			dxgi_buffer_usage |= DXGI_USAGE_SHADER_INPUT;
		if (!!(create_info->image_usage & ::framework::gpu::image_usage_flags::storage_bit))
			dxgi_buffer_usage |= DXGI_USAGE_UNORDERED_ACCESS;
		if (!!(create_info->image_usage & ::framework::gpu::image_usage_flags::color_attachment_bit))
			dxgi_buffer_usage |= DXGI_USAGE_RENDER_TARGET_OUTPUT;

		::DXGI_SWAP_CHAIN_DESC1 dxgi_swap_chain_desc1;
		dxgi_swap_chain_desc1.Width = create_info->image_extent.width;
		dxgi_swap_chain_desc1.Height = create_info->image_extent.height;
		dxgi_swap_chain_desc1.Format = ::framework::gpu::d3d12::get_dxgi_format(create_info->image_format);
		dxgi_swap_chain_desc1.Stereo = FALSE;
		dxgi_swap_chain_desc1.Scaling = ::DXGI_SCALING::DXGI_SCALING_NONE;
		dxgi_swap_chain_desc1.SampleDesc.Quality = 0;
		dxgi_swap_chain_desc1.SampleDesc.Count = 1;
		dxgi_swap_chain_desc1.BufferUsage = dxgi_buffer_usage;
		dxgi_swap_chain_desc1.BufferCount = create_info->min_image_count;
		dxgi_swap_chain_desc1.Flags = ::DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH | ::DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT;
		dxgi_swap_chain_desc1.SwapEffect = ::DXGI_SWAP_EFFECT::DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
		dxgi_swap_chain_desc1.AlphaMode = ::DXGI_ALPHA_MODE::DXGI_ALPHA_MODE_UNSPECIFIED;

		::framework::gpu::queue * present_queue;
		device->get_queue(0, 0, &present_queue);
		::Microsoft::WRL::ComPtr<::IDXGISwapChain1> swap_chain;
		result = ::framework::gpu::d3d12::throw_result(device->get_physical_device()->get_instance()->get_dxgi_factory()->CreateSwapChainForHwnd(
			static_cast<::framework::gpu::d3d12::queue *>(present_queue)->get_d3d12_command_queue(),
			static_cast<::framework::gpu::d3d12::surface *>(create_info->surface)->get_window()->get_hwnd(), &dxgi_swap_chain_desc1, nullptr, nullptr, &swap_chain
		));
		result = ::framework::gpu::d3d12::throw_result(swap_chain->QueryInterface(IID_PPV_ARGS(&this->dxgi_swap_chain)));
		result = ::framework::gpu::d3d12::throw_result(this->dxgi_swap_chain->SetMaximumFrameLatency(create_info->min_image_count));

		this->frame_latency_waitable_object = this->dxgi_swap_chain->GetFrameLatencyWaitableObject();

		::DXGI_MODE_ROTATION dxgi_mode_rotation;
		switch (create_info->pre_transform)
		{
		case ::framework::gpu::surface_transform_flags::identity_bit:
			dxgi_mode_rotation = ::DXGI_MODE_ROTATION::DXGI_MODE_ROTATION_IDENTITY;
			break;
		case ::framework::gpu::surface_transform_flags::rotate_90_bit:
			dxgi_mode_rotation = ::DXGI_MODE_ROTATION::DXGI_MODE_ROTATION_ROTATE90;
			break;
		case ::framework::gpu::surface_transform_flags::rotate_180_bit:
			dxgi_mode_rotation = ::DXGI_MODE_ROTATION::DXGI_MODE_ROTATION_ROTATE180;
			break;
		case ::framework::gpu::surface_transform_flags::rotate_270_bit:
			dxgi_mode_rotation = ::DXGI_MODE_ROTATION::DXGI_MODE_ROTATION_ROTATE270;
			break;
		default:
			dxgi_mode_rotation = ::DXGI_MODE_ROTATION::DXGI_MODE_ROTATION_UNSPECIFIED;
			break;
		}
		result = ::framework::gpu::d3d12::throw_result(this->dxgi_swap_chain->SetRotation(dxgi_mode_rotation));
	}

	this->image_count = create_info->min_image_count;
	this->images.reset(::new (::framework::gpu::common::allocate<::framework::gpu::d3d12::image * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->image_count)) ::framework::gpu::d3d12::image * [this->image_count]{});

	for (::std::uint32_t i = 0; i < this->image_count; i++)
	{
		::ID3D12Resource * display_plane;
		result = ::framework::gpu::d3d12::throw_result(this->dxgi_swap_chain->GetBuffer(i, IID_PPV_ARGS(&display_plane)));

		::framework::gpu::image_create_info image_create_info;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = create_info->image_format;
		image_create_info.extent.width = create_info->image_extent.width;
		image_create_info.extent.height = create_info->image_extent.height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = create_info->image_array_layers;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = create_info->image_usage;
		image_create_info.sharing_mode = create_info->image_sharing_mode;
		image_create_info.queue_family_index_count = create_info->queue_family_index_count;
		image_create_info.queue_family_indices = create_info->queue_family_indices;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		result = device->create_image(&image_create_info, display_plane, &allocator, &this->images[i]);
	}
}

::framework::gpu::result(::framework::gpu::d3d12::swap_chain::acquire_next_image)(::framework::gpu::acquire_next_image_info const * acquire_info, ::std::uint32_t * image_index) const
{
	::framework::gpu::result result;

	::WaitForSingleObject(this->frame_latency_waitable_object, static_cast<::DWORD>(acquire_info->timeout / 1000000ull));

	if (this->window->get_width() != this->image_extent.width || this->window->get_height() != this->image_extent.height)
	{
		*image_index = 0; // After resetting, current back buffer will be always 0'th

		result = ::framework::gpu::result::error_out_of_date;
	}
	else
	{
		*image_index = this->dxgi_swap_chain->GetCurrentBackBufferIndex();

		result = ::framework::gpu::result::success;
	}

	if (acquire_info->semaphore)
		static_cast<::framework::gpu::d3d12::semaphore *>(acquire_info->semaphore)->get_d3d12_fence()->Signal(static_cast<::framework::gpu::d3d12::semaphore *>(acquire_info->semaphore)->get_fence_value());
	if (acquire_info->fence)
		static_cast<::framework::gpu::d3d12::fence *>(acquire_info->fence)->get_d3d12_fence()->Signal(static_cast<::framework::gpu::d3d12::fence *>(acquire_info->fence)->get_fence_value());

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::swap_chain::present)() const
{
	::framework::gpu::result result;

	if (this->window->get_width() != this->image_extent.width || this->window->get_height() != this->image_extent.height)
	{
		result = ::framework::gpu::result::error_out_of_date;
	}
	else
	{
		::HRESULT hresult = this->dxgi_swap_chain->Present(0, 0);
		result = ::framework::gpu::d3d12::get_result(hresult);
	}

	return result;
}

void ::framework::gpu::d3d12::swap_chain::get_images(::std::uint32_t * image_count, ::framework::gpu::image ** images) const
{
	if (images)
	{
		::std::memcpy(images, this->images.get(), sizeof(::framework::gpu::image *) * *image_count);
	}
	else
	{
		*image_count = this->image_count;
	}
}