#pragma once

#include "gpu/buffer.h"
#include "resource.h"

namespace framework::gpu::d3d12
{
	class buffer final : public ::framework::gpu::buffer, public ::framework::gpu::d3d12::resource
	{
	public:
		buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::buffer_create_info const * create_info, ::framework::gpu::result & result);

		::framework::gpu::device_size get_device_memory_offset() const { return this->device_memory_offset; }

		::framework::gpu::device_size get_size() const { return this->size; }

		::framework::gpu::buffer_usage_flags get_usage() const { return this->usage; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::device_size device_memory_offset;
		::framework::gpu::device_size size;
		::framework::gpu::buffer_usage_flags usage;
	};
}