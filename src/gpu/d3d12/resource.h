#pragma once

namespace framework::gpu::d3d12
{
	class resource
	{
	public:
		::ID3D12Resource * get_d3d12_resource() const { return this->d3d12_resource; }

		::D3D12_RESOURCE_DESC const & get_d3d12_resource_desc() const { return this->d3d12_resource_desc; }

		::D3D12_RESOURCE_STATES get_initial_state() const { return this->initial_state; }

		::HRESULT set_name(char const * object_name);

	protected:
		resource();

		~resource();

		::ID3D12Resource * d3d12_resource;
		::D3D12_RESOURCE_FLAGS resource_flags;
		::D3D12_RESOURCE_DESC d3d12_resource_desc;
		::D3D12_RESOURCE_STATES initial_state;
		bool is_dedicated;
		::std::unique_ptr<char[]> name;
	};
}