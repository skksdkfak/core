#pragma once

#include "gpu/frame_buffer.h"
#include <vector>

namespace framework::gpu::d3d12
{
	class frame_buffer final : public ::framework::gpu::frame_buffer
	{
	public:
		frame_buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::std::uint32_t get_attachment_count() const
		{
			return this->attachment_count;
		}

		::framework::gpu::attachment_view_and_descriptor const * get_attachments() const
		{
			return this->attachments.get();
		}

		::std::vector<::D3D12_RESOURCE_BARRIER> get_subpass_barriers(::std::uint32_t subpass) const
		{
			return this->m_barriers[subpass];
		}

		::std::vector<::D3D12_RESOURCE_BARRIER> get_end_render_pass_barriers() const
		{
			return this->m_barriers.back();
		}

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::std::vector<::std::vector<::D3D12_RESOURCE_BARRIER>> m_barriers;
		::std::uint32_t attachment_count;
		::framework::gpu::common::unique_ptr<::framework::gpu::attachment_view_and_descriptor[]> attachments;
	};
}