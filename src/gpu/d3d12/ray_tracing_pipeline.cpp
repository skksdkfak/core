#include "gpu/d3d12/core.hpp"
#include <codecvt>
#include <locale>

::framework::gpu::d3d12::ray_tracing_pipeline::ray_tracing_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::ray_tracing_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	group_count(create_info->group_count)
{
	this->group_export_names = ::framework::gpu::common::make_unique<::std::wstring[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->group_count);

	::UINT subobject_count = 0;

	::framework::gpu::common::unique_ptr<::D3D12_STATE_SUBOBJECT[]> d3d12_state_subobjects = ::framework::gpu::common::make_unique<::D3D12_STATE_SUBOBJECT[]>(this->allocator, ::framework::gpu::system_allocation_scope::command,
		::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID + create_info->stage_count + create_info->local_layout_count + create_info->association_count);

	::std::uint32_t hit_group_count = 0;
	for (::std::uint32_t i = 0; i < create_info->group_count; i++)
	{
		switch (create_info->groups[i].type)
		{
		case ::framework::gpu::ray_tracing_shader_group_type::triangles_hit_group:
		case ::framework::gpu::ray_tracing_shader_group_type::procedural_hit_group:
			hit_group_count++;
			break;
		}
	}

	::framework::gpu::common::unique_ptr<::std::wstring[]> shader_export_names = ::framework::gpu::common::make_unique<::std::wstring[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->stage_count);
	for (::std::uint32_t i = 0; i < create_info->stage_count; i++)
	{
		shader_export_names[i] = ::std::to_wstring(i);
	}

	::framework::gpu::common::unique_ptr<::D3D12_HIT_GROUP_DESC[]> d3d12_hit_group_descs = ::framework::gpu::common::make_unique<::D3D12_HIT_GROUP_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, hit_group_count);
	::framework::gpu::common::unique_ptr<::D3D12_LOCAL_ROOT_SIGNATURE[]> d3d12_local_root_signatures = ::framework::gpu::common::make_unique<::D3D12_LOCAL_ROOT_SIGNATURE[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->local_layout_count);
	::framework::gpu::common::unique_ptr<::D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION[]> d3d12_subobject_to_exports_associations = ::framework::gpu::common::make_unique<::D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->association_count);

	::D3D12_GLOBAL_ROOT_SIGNATURE d3d12_global_root_signature;
	d3d12_global_root_signature.pGlobalRootSignature = static_cast<::framework::gpu::d3d12::pipeline_layout const *>(create_info->layout)->get_d3d12_root_signature();
	d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_GLOBAL_ROOT_SIGNATURE, &d3d12_global_root_signature };

	::UINT const d3d12_state_subobject_type_local_root_signature_offset = subobject_count;

	for (::std::uint32_t i = 0; i < create_info->local_layout_count; i++)
	{
		d3d12_local_root_signatures[i].pLocalRootSignature = static_cast<::framework::gpu::d3d12::pipeline_layout const *>(create_info->local_layouts[i])->get_d3d12_root_signature();
		d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE, &d3d12_local_root_signatures[i] };
	}

	::D3D12_RAYTRACING_SHADER_CONFIG d3d12_raytracing_shader_config;
	d3d12_raytracing_shader_config.MaxPayloadSizeInBytes = static_cast<::UINT>(create_info->max_pipeline_ray_payload_size);
	d3d12_raytracing_shader_config.MaxAttributeSizeInBytes = static_cast<::UINT>(create_info->max_pipeline_ray_hit_attribute_size);
	d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG, &d3d12_raytracing_shader_config };

	for (::std::uint32_t i = 0, hit_group_index = 0; i < create_info->group_count; i++)
	{
		::D3D12_HIT_GROUP_TYPE d3d12_hit_group_type;
		switch (create_info->groups[i].type)
		{
		case ::framework::gpu::ray_tracing_shader_group_type::general:
			this->group_export_names[i] = shader_export_names[create_info->groups[i].general_shader].c_str();
			continue;
		case ::framework::gpu::ray_tracing_shader_group_type::triangles_hit_group:
			d3d12_hit_group_type = ::D3D12_HIT_GROUP_TYPE::D3D12_HIT_GROUP_TYPE_TRIANGLES;
			break;
		case ::framework::gpu::ray_tracing_shader_group_type::procedural_hit_group:
			d3d12_hit_group_type = ::D3D12_HIT_GROUP_TYPE::D3D12_HIT_GROUP_TYPE_PROCEDURAL_PRIMITIVE;
			break;
		}

		this->group_export_names[i] = L"hit_group_" + ::std::to_wstring(hit_group_index);

		d3d12_hit_group_descs[hit_group_index].HitGroupExport = this->group_export_names[i].c_str();
		d3d12_hit_group_descs[hit_group_index].Type = d3d12_hit_group_type;
		d3d12_hit_group_descs[hit_group_index].AnyHitShaderImport = create_info->groups[i].any_hit_shader == ::framework::gpu::shader_unused ? nullptr : shader_export_names[create_info->groups[i].any_hit_shader].c_str();
		d3d12_hit_group_descs[hit_group_index].ClosestHitShaderImport = create_info->groups[i].closest_hit_shader == ::framework::gpu::shader_unused ? nullptr : shader_export_names[create_info->groups[i].closest_hit_shader].c_str();
		d3d12_hit_group_descs[hit_group_index].IntersectionShaderImport = create_info->groups[i].intersection_shader == ::framework::gpu::shader_unused ? nullptr : shader_export_names[create_info->groups[i].intersection_shader].c_str();
		d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP, &d3d12_hit_group_descs[hit_group_index] };
		hit_group_index++;
	}

	::D3D12_RAYTRACING_PIPELINE_CONFIG1 d3d12_raytracing_pipeline_config1;
	d3d12_raytracing_pipeline_config1.Flags = ::D3D12_RAYTRACING_PIPELINE_FLAGS::D3D12_RAYTRACING_PIPELINE_FLAG_NONE;
	d3d12_raytracing_pipeline_config1.MaxTraceRecursionDepth = static_cast<::UINT>(create_info->max_pipeline_ray_recursion_depth);
	d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG1, &d3d12_raytracing_pipeline_config1 };

	::framework::gpu::common::unique_ptr<::D3D12_DXIL_LIBRARY_DESC[]> d3d12_dxil_library_descs = ::framework::gpu::common::make_unique<::D3D12_DXIL_LIBRARY_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->stage_count);
	::framework::gpu::common::unique_ptr<::D3D12_EXPORT_DESC[]> d3d12_export_descs = ::framework::gpu::common::make_unique<::D3D12_EXPORT_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->stage_count);
	::framework::gpu::common::unique_ptr<::std::wstring[]> entry_points = ::framework::gpu::common::make_unique<::std::wstring[]>(this->allocator, ::framework::gpu::system_allocation_scope::command, create_info->stage_count);

	::std::wstring_convert<::std::codecvt_utf8_utf16<wchar_t>> wstring_convert;

	for (::std::uint32_t i = 0; i < create_info->stage_count; i++)
	{
		entry_points[i] = wstring_convert.from_bytes(create_info->stages[i].name);

		::IDxcBlob * const dxc_blob = static_cast<::framework::gpu::d3d12::shader_module const *>(create_info->stages[i].module)->get_dxc_blob();

		d3d12_export_descs[i].Name = shader_export_names[i].c_str();
		d3d12_export_descs[i].ExportToRename = entry_points[i].c_str();
		d3d12_export_descs[i].Flags = ::D3D12_EXPORT_FLAGS::D3D12_EXPORT_FLAG_NONE;

		d3d12_dxil_library_descs[i].DXILLibrary = ::D3D12_SHADER_BYTECODE{ dxc_blob->GetBufferPointer(), dxc_blob->GetBufferSize() };
		d3d12_dxil_library_descs[i].NumExports = 1;
		d3d12_dxil_library_descs[i].pExports = &d3d12_export_descs[i];
		d3d12_state_subobjects[subobject_count++] = { ::D3D12_STATE_SUBOBJECT_TYPE::D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY, &d3d12_dxil_library_descs[i] };
	}

	for (::std::uint32_t i = 0; i < create_info->association_count; i++)
	{
		d3d12_subobject_to_exports_associations[i].pSubobjectToAssociate = &d3d12_state_subobjects[d3d12_state_subobject_type_local_root_signature_offset + create_info->associations[i].layout];
		d3d12_subobject_to_exports_associations[i].NumExports = 1;
		d3d12_subobject_to_exports_associations[i].pExports = &d3d12_hit_group_descs[create_info->associations[i].group].HitGroupExport;
	}

	::D3D12_STATE_OBJECT_DESC d3d12_state_object_desc;
	d3d12_state_object_desc.Type = ::D3D12_STATE_OBJECT_TYPE::D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE;
	d3d12_state_object_desc.NumSubobjects = subobject_count;
	d3d12_state_object_desc.pSubobjects = d3d12_state_subobjects.get();
	result = ::framework::gpu::d3d12::throw_result(device->get_d3d12_device()->CreateStateObject(&d3d12_state_object_desc, IID_PPV_ARGS(&this->d3d12_state_object)));
	result = ::framework::gpu::d3d12::throw_result(this->d3d12_state_object->QueryInterface(IID_PPV_ARGS(&this->d3d12_state_object_properties)));
}