#pragma once

#include "gpu/semaphore.h"

namespace framework::gpu::d3d12
{
	class semaphore final : public ::framework::gpu::semaphore
	{
	public:
		semaphore(::framework::gpu::d3d12::device * device, ::framework::gpu::semaphore_create_info const * create_info, ::framework::gpu::result & result);

		~semaphore();

		::UINT64 const get_fence_value() const { return this->next_fence_value; }

		::UINT64 const increment_fence_value() { return this->next_fence_value++; }

		bool get_is_binary_semaphore() const { return this->is_binary_semaphore; }

		::ID3D12Fence * const get_d3d12_fence() const { return this->d3d12_fence; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::ID3D12Fence * d3d12_fence;
		::UINT64 next_fence_value;
		bool is_binary_semaphore;
	};
}