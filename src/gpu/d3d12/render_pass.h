#pragma once

#include "gpu/render_pass.h"
#include <vector>

namespace framework::gpu::d3d12
{
	class render_pass final : public ::framework::gpu::render_pass
	{
	public:
		struct attachment_memory_barrier
		{
			::D3D12_RESOURCE_STATES state_before;
			::D3D12_RESOURCE_STATES state_after;
			::std::uint32_t attachment;
		};

		struct attachment_info
		{
			::framework::gpu::format format;
		};

		struct render_target_desc
		{
			::std::uint32_t attachment;
			::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE beginning_access_type;
			::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE ending_access_type;
		};

		struct depth_stencil_desc
		{
			::framework::gpu::attachment_reference attachment;
			::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE depth_beginning_access_type;
			::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE stencil_beginning_access_type;
			::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE depth_ending_access_type;
			::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE stencil_ending_access_type;
		};

		struct subpass_info
		{
			::UINT num_render_targets;
			::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::render_pass::render_target_desc[]> render_targets;
			::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::render_pass::depth_stencil_desc> depth_stencil;
		};

		render_pass(::framework::gpu::d3d12::device * device, ::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::std::uint32_t get_attachment_count() const
		{
			return this->attachment_count;
		}

		::std::uint32_t get_subpass_count() const

		{
			return this->subpass_count;
		}

		::framework::gpu::d3d12::render_pass::subpass_info const & get_subpass_info(::std::uint32_t subpass) const
		{
			return this->subpass_infos[subpass];
		}

		::framework::gpu::d3d12::render_pass::attachment_info const & get_attachment_description(::std::uint32_t attachment) const
		{
			return this->attachment_infos[attachment];
		}

		::std::vector<::framework::gpu::d3d12::render_pass::attachment_memory_barrier> const & get_subpass_barriers(::std::uint32_t subpass) const
		{
			return this->attachment_memory_barriers[subpass];
		}

		::std::vector<::framework::gpu::d3d12::render_pass::attachment_memory_barrier> const & get_end_render_pass_barriers() const
		{
			return this->attachment_memory_barriers[this->subpass_count];
		}

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::std::uint32_t subpass_count;
		::std::uint32_t attachment_count;
		::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::render_pass::attachment_info[]> attachment_infos;
		::framework::gpu::common::unique_ptr<::std::vector<::framework::gpu::d3d12::render_pass::attachment_memory_barrier>[]> attachment_memory_barriers;
		::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::render_pass::subpass_info[]> subpass_infos;
	};
}