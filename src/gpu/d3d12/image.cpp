#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::image::image(::framework::gpu::d3d12::device * device, ::framework::gpu::image_create_info const * create_info, ::framework::gpu::result & result) :
	sample_count(::framework::gpu::d3d12::image::get_sample_count(create_info->samples)),
	usage(create_info->usage)
{
	::D3D12_RESOURCE_DIMENSION d3d12_resource_dimension;
	switch (create_info->image_type)
	{
	case ::framework::gpu::image_type::one_dimensional:
		d3d12_resource_dimension = ::D3D12_RESOURCE_DIMENSION::D3D12_RESOURCE_DIMENSION_TEXTURE1D;
		break;
	case ::framework::gpu::image_type::two_dimensional:
		d3d12_resource_dimension = ::D3D12_RESOURCE_DIMENSION::D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		break;
	case ::framework::gpu::image_type::three_dimensional:
		d3d12_resource_dimension = ::D3D12_RESOURCE_DIMENSION::D3D12_RESOURCE_DIMENSION_TEXTURE3D;
		break;
	}

	this->resource_flags = ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_NONE;
	if (!!(create_info->usage & ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit) &&
		!(create_info->usage & (::framework::gpu::image_usage_flags::sampled_bit | ::framework::gpu::image_usage_flags::input_attachment_bit)))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;
	if (!!(create_info->usage & ::framework::gpu::image_usage_flags::storage_bit))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
	if (!!(create_info->usage & (::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::allow_clear_bit)))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
	if (!!(create_info->usage & ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit))
		this->resource_flags |= ::D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	::DXGI_FORMAT dxgi_format = ::framework::gpu::d3d12::get_dxgi_format(create_info->format);

	::D3D12_TEXTURE_LAYOUT d3d12_texture_layout;
	switch (create_info->tiling)
	{
	case ::framework::gpu::image_tiling::optimal:
		d3d12_texture_layout = ::D3D12_TEXTURE_LAYOUT::D3D12_TEXTURE_LAYOUT_UNKNOWN;
		break;
	case ::framework::gpu::image_tiling::linear:
		d3d12_texture_layout = ::D3D12_TEXTURE_LAYOUT::D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		break;
	default:
		d3d12_texture_layout = ::D3D12_TEXTURE_LAYOUT::D3D12_TEXTURE_LAYOUT_UNKNOWN;
		break;
	}

	if (create_info->initial_queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
		this->initial_state = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COMMON;
	else
		this->initial_state = ::framework::gpu::d3d12::get_d3d12_resource_states(create_info->initial_layout);

	this->d3d12_resource_desc.Alignment = 0;
	this->d3d12_resource_desc.DepthOrArraySize = ::std::max<::std::uint32_t>(create_info->array_layers, create_info->extent.depth);
	this->d3d12_resource_desc.Dimension = d3d12_resource_dimension;
	this->d3d12_resource_desc.Flags = this->resource_flags;
	this->d3d12_resource_desc.Format = dxgi_format;
	this->d3d12_resource_desc.Height = create_info->extent.height;
	this->d3d12_resource_desc.Layout = d3d12_texture_layout;
	this->d3d12_resource_desc.MipLevels = create_info->mip_levels;
	this->d3d12_resource_desc.SampleDesc.Count = this->sample_count;
	this->d3d12_resource_desc.SampleDesc.Quality = 0;
	this->d3d12_resource_desc.Width = create_info->extent.width;

	if (create_info->optimized_clear_value)
	{
		this->optimized_clear_value = new ::D3D12_CLEAR_VALUE;

		this->optimized_clear_value->Format = dxgi_format;
		if (!!(create_info->usage & (::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::allow_clear_bit)))
		{
			this->optimized_clear_value->Color[0] = create_info->optimized_clear_value->color.float32[0];
			this->optimized_clear_value->Color[1] = create_info->optimized_clear_value->color.float32[1];
			this->optimized_clear_value->Color[2] = create_info->optimized_clear_value->color.float32[2];
			this->optimized_clear_value->Color[3] = create_info->optimized_clear_value->color.float32[3];
		}

		if (!!(create_info->usage & ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit))
		{
			this->optimized_clear_value->DepthStencil.Depth = create_info->optimized_clear_value->depth_stencil.depth;
			this->optimized_clear_value->DepthStencil.Stencil = create_info->optimized_clear_value->depth_stencil.stencil;
		}
	}
	else
	{
		this->optimized_clear_value = nullptr;
	}

	result = ::framework::gpu::result::success;
}

::framework::gpu::d3d12::image::image(::framework::gpu::d3d12::device * device, ::framework::gpu::image_create_info const * create_info, ::ID3D12Resource * base_resource, ::framework::gpu::result & result) :
	sample_count(::framework::gpu::d3d12::image::get_sample_count(create_info->samples)),
	usage(create_info->usage)
{
	this->d3d12_resource = base_resource;
	this->is_dedicated = true;
	this->optimized_clear_value = nullptr;
	this->d3d12_resource_desc = base_resource->GetDesc();

	result = ::framework::gpu::result::success;
}

::UINT(::framework::gpu::d3d12::image::get_sample_count)(::framework::gpu::sample_count_flags samples)
{
	switch (samples)
	{
	case ::framework::gpu::sample_count_flags::sample_count_1_bit:
		return 1;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_2_bit:
		return 2;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_4_bit:
		return 4;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_8_bit:
		return 8;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_16_bit:
		return 16;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_32_bit:
		return 32;
		break;
	case ::framework::gpu::sample_count_flags::sample_count_64_bit:
		return 64;
		break;
	default:
		return 1;
		break;
	}
}

::framework::gpu::d3d12::image::~image()
{
	delete this->optimized_clear_value;
}