#pragma once

#include "gpu/descriptor_set.h"

namespace framework::gpu::d3d12
{
	class descriptor_set final : public ::framework::gpu::descriptor_set
	{
	public:
		::framework::gpu::d3d12::descriptor_set_layout const * get_descriptor_set_layout() const { return this->descriptor_set_layout; }

		::D3D12_CPU_DESCRIPTOR_HANDLE get_cpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type) const { return this->cpu_descriptor_range_start[d3d12_descriptor_heap_type]; }

		::D3D12_GPU_DESCRIPTOR_HANDLE get_gpu_descriptor_range_start(::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type) const { return this->gpu_descriptor_range_start[d3d12_descriptor_heap_type]; }

	private:
		friend class ::framework::gpu::d3d12::device;
		friend class ::framework::gpu::d3d12::descriptor_pool;
		friend class ::framework::gpu::d3d12::command_buffer;

		::std::uint32_t id;
		::framework::gpu::d3d12::descriptor_set_layout const * descriptor_set_layout;
		::D3D12_CPU_DESCRIPTOR_HANDLE cpu_descriptor_range_start[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
		::D3D12_GPU_DESCRIPTOR_HANDLE gpu_descriptor_range_start[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
		::framework::gpu::common::unique_ptr<::D3D12_GPU_VIRTUAL_ADDRESS[]> dynamic_root_parameter_addresses;
	};
}