#pragma once

#include "gpu/surface.h"

namespace framework::platform::win32
{
	class window;
};

namespace framework::gpu::d3d12
{
	class surface final : public ::framework::gpu::surface
	{
	public:
		surface(::framework::gpu::d3d12::instance * instance, ::framework::gpu::surface_create_info const * create_info);

		~surface();

		::framework::platform::win32::window * get_window() const { return this->window; }

	private:
		friend class ::framework::gpu::d3d12::instance;

		::framework::platform::win32::window * window;
	};
}