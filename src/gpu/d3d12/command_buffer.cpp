#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::command_buffer::command_buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::ID3D12GraphicsCommandList7 * d3d12_graphics_command_list, ::ID3D12CommandAllocator * d3d12_command_allocator, ::std::uint32_t id) :
	id(id),
	device(device),
	d3d12_graphics_command_list(d3d12_graphics_command_list),
	d3d12_command_allocator(d3d12_command_allocator),
	opened_render_pass(false)
{
}

::framework::gpu::d3d12::command_buffer::~command_buffer()
{
	if (this->d3d12_graphics_command_list)
		this->d3d12_graphics_command_list->Release();
}

void ::framework::gpu::d3d12::command_buffer::commit_rasterizer_state() const
{
	if (!this->dynamic_viewports.empty())
	{
		this->d3d12_graphics_command_list->RSSetViewports(static_cast<::UINT>(this->dynamic_viewports.size()), this->dynamic_viewports.data());
	}

	if (!this->dynamic_scissors.empty())
	{
		this->d3d12_graphics_command_list->RSSetScissorRects(static_cast<::UINT>(this->dynamic_scissors.size()), this->dynamic_scissors.data());
	}
}

::framework::gpu::result(::framework::gpu::d3d12::command_buffer::begin_command_buffer)(::framework::gpu::command_buffer_begin_info const * begin_info)
{
	::framework::gpu::result result;

	try
	{
		// ~0ul indicates that the command buffer was allocated from a command pool that does not allow command allocator reset.
		if (this->id != ~0ul)
		{
			result = ::framework::gpu::d3d12::throw_result(this->d3d12_command_allocator->Reset());
		}
		result = ::framework::gpu::d3d12::throw_result(this->d3d12_graphics_command_list->Reset(this->d3d12_command_allocator, nullptr));

		if (begin_info->inheritance_info)
		{
			::framework::gpu::d3d12::render_pass_state::subpass_info const & subpass_info = static_cast<::framework::gpu::d3d12::render_pass_state const *>(begin_info->inheritance_info->render_pass_state)->get_subpass_info(begin_info->inheritance_info->subpass);

			::D3D12_RENDER_PASS_FLAGS d3d12_render_pass_flags = ::D3D12_RENDER_PASS_FLAGS::D3D12_RENDER_PASS_FLAG_NONE;
			if (!!(begin_info->flags & ::framework::gpu::command_buffer_usage_flags::render_pass_suspending_bit))
				d3d12_render_pass_flags |= ::D3D12_RENDER_PASS_FLAGS::D3D12_RENDER_PASS_FLAG_SUSPENDING_PASS;
			if (!!(begin_info->flags & ::framework::gpu::command_buffer_usage_flags::render_pass_resuming_bit))
				d3d12_render_pass_flags |= ::D3D12_RENDER_PASS_FLAGS::D3D12_RENDER_PASS_FLAG_RESUMING_PASS;

			this->d3d12_graphics_command_list->BeginRenderPass(subpass_info.num_render_targets, subpass_info.render_targets.get(), subpass_info.depth_stencil.get(), d3d12_render_pass_flags);
			this->opened_render_pass = true;
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::command_buffer::end_command_buffer)()
{
	::HRESULT hresult;

	if (this->opened_render_pass)
	{
		this->d3d12_graphics_command_list->EndRenderPass();
		this->opened_render_pass = false;
	}

	hresult = this->d3d12_graphics_command_list->Close();

	return ::framework::gpu::d3d12::get_result(hresult);
}

void ::framework::gpu::d3d12::command_buffer::push_constants(::framework::gpu::pipeline_layout * layout, ::framework::gpu::shader_stage_flags stage_flags, ::std::uint32_t binding, ::std::uint32_t offset, ::std::uint32_t size, void const * values)
{
	::std::size_t const alignment = 4;
	::std::size_t const dest_offset_in_32_bit_values = offset / alignment + ((offset % alignment) > 0 ? 1 : 0);
	::std::size_t const num_32_bit_values = size / alignment + ((size % alignment) > 0 ? 1 : 0);
	auto const & set_root_32bit_constants = [&](void (STDMETHODCALLTYPE::ID3D12GraphicsCommandList:: * fpSetGraphicsRoot32BitConstants)(::UINT, ::UINT, void const *, ::UINT), ::D3D12_SHADER_VISIBILITY d3d12_shader_visibility)
	{
		(this->d3d12_graphics_command_list->*fpSetGraphicsRoot32BitConstants)(static_cast<::framework::gpu::d3d12::pipeline_layout const *>(layout)->get_push_constants_global_offset(binding, d3d12_shader_visibility), num_32_bit_values, values, dest_offset_in_32_bit_values);
	};

	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::compute_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetComputeRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_ALL);
	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::vertex_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetGraphicsRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_VERTEX);
	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::tessellation_control_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetGraphicsRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_HULL);
	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetGraphicsRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_DOMAIN);
	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::geometry_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetGraphicsRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_GEOMETRY);
	if (!!(stage_flags & ::framework::gpu::shader_stage_flags::fragment_bit))
		set_root_32bit_constants(&::ID3D12GraphicsCommandList::SetGraphicsRoot32BitConstants, ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_PIXEL);
}

void ::framework::gpu::d3d12::command_buffer::begin_render_pass(::framework::gpu::render_pass_begin_info const * render_pass_begin, ::framework::gpu::subpass_contents contents)
{
}

void ::framework::gpu::d3d12::command_buffer::next_subpass(::framework::gpu::subpass_contents contents)
{
}

void ::framework::gpu::d3d12::command_buffer::end_render_pass()
{
}

void ::framework::gpu::d3d12::command_buffer::execute_commands(::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers)
{
}

void ::framework::gpu::d3d12::command_buffer::bind_pipeline(::framework::gpu::pipeline_bind_point pipeline_bind_point, const gpu::pipeline * pipeline)
{
	this->pipeline = pipeline;

	switch (pipeline_bind_point)
	{
	case ::framework::gpu::pipeline_bind_point::graphics:
	{
		this->d3d12_graphics_command_list->SetPipelineState(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_d3d12_pipeline_state());
		this->d3d12_graphics_command_list->IASetPrimitiveTopology(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_d3d12_primitive_topology());

		if (static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->has_dynamic_state(::framework::gpu::dynamic_state::viewport))
		{
			this->dynamic_viewports.resize(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_viewport_count());
		}
		else
		{
			this->d3d12_graphics_command_list->RSSetViewports(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_viewport_count(), static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_viewports());
			this->dynamic_viewports.resize(0);
		}

		if (static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->has_dynamic_state(::framework::gpu::dynamic_state::scissor))
		{
			this->dynamic_scissors.resize(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_scissor_rect_count());
		}
		else
		{
			this->d3d12_graphics_command_list->RSSetScissorRects(static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_scissor_rect_count(), static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(pipeline)->get_scissor_rects());
			this->dynamic_scissors.resize(0);
		}
	}
	break;
	case ::framework::gpu::pipeline_bind_point::compute:
		this->d3d12_graphics_command_list->SetPipelineState(static_cast<::framework::gpu::d3d12::compute_pipeline const *>(pipeline)->get_d3d12_pipeline_state());
		break;
	case ::framework::gpu::pipeline_bind_point::ray_tracing:
		this->d3d12_graphics_command_list->SetPipelineState1(static_cast<::framework::gpu::d3d12::ray_tracing_pipeline const *>(pipeline)->get_d3d12_state_object());
		break;
	}
}

void ::framework::gpu::d3d12::command_buffer::set_viewport(::std::uint32_t first_viewport, ::std::uint32_t viewport_count, ::framework::gpu::viewport const * viewports)
{
	for (::std::uint32_t i = 0, j = first_viewport; i < viewport_count; i++, j++)
	{
		this->dynamic_viewports[j].TopLeftX = viewports[i].x;
		this->dynamic_viewports[j].TopLeftY = viewports[i].y;
		this->dynamic_viewports[j].Width = viewports[i].width;
		this->dynamic_viewports[j].Height = viewports[i].height;
		this->dynamic_viewports[j].MinDepth = viewports[i].min_depth;
		this->dynamic_viewports[j].MaxDepth = viewports[i].max_depth;
	};
}

void ::framework::gpu::d3d12::command_buffer::set_scissor(::std::uint32_t first_scissor, ::std::uint32_t scissor_count, ::framework::gpu::rect_2d const * scissors)
{
	for (::std::uint32_t i = 0, j = first_scissor; i < scissor_count; i++, j++)
	{
		this->dynamic_scissors[j].left = scissors[i].offset.x;
		this->dynamic_scissors[j].right = scissors[i].extent.width;
		this->dynamic_scissors[j].bottom = scissors[i].extent.height;
		this->dynamic_scissors[j].top = scissors[i].offset.y;
	}
}

void ::framework::gpu::d3d12::command_buffer::bind_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool)
{
	this->d3d12_graphics_command_list->SetDescriptorHeaps(static_cast<::framework::gpu::d3d12::descriptor_pool *>(descriptor_pool)->get_d3d12_descriptor_heap_count(), static_cast<::framework::gpu::d3d12::descriptor_pool *>(descriptor_pool)->get_d3d12_descriptor_heaps());
}

void ::framework::gpu::d3d12::command_buffer::bind_descriptor_sets(::framework::gpu::pipeline_bind_point pipeline_bind_point, ::framework::gpu::pipeline_layout * layout, ::std::uint32_t first_set, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset_count, const uint32_t * dynamic_offsets)
{
	void (STDMETHODCALLTYPE::ID3D12GraphicsCommandList:: * fpSetRootDescriptorTable)(::UINT, ::D3D12_GPU_DESCRIPTOR_HANDLE)(nullptr);
	void (STDMETHODCALLTYPE::ID3D12GraphicsCommandList:: * fpSetRootShaderResourceView)(::UINT, ::D3D12_GPU_VIRTUAL_ADDRESS)(nullptr);
	void (STDMETHODCALLTYPE::ID3D12GraphicsCommandList:: * fpSetRootConstantBufferView)(::UINT, ::D3D12_GPU_VIRTUAL_ADDRESS)(nullptr);
	void (STDMETHODCALLTYPE::ID3D12GraphicsCommandList:: * fpSetRootUnorderedAccessView)(::UINT, ::D3D12_GPU_VIRTUAL_ADDRESS)(nullptr);

	switch (pipeline_bind_point)
	{
	case ::framework::gpu::pipeline_bind_point::graphics:
		this->d3d12_graphics_command_list->SetGraphicsRootSignature(static_cast<::framework::gpu::d3d12::pipeline_layout *>(layout)->get_d3d12_root_signature());
		fpSetRootDescriptorTable = &::ID3D12GraphicsCommandList::SetGraphicsRootDescriptorTable;
		fpSetRootConstantBufferView = &::ID3D12GraphicsCommandList::SetGraphicsRootConstantBufferView;
		fpSetRootShaderResourceView = &::ID3D12GraphicsCommandList::SetGraphicsRootShaderResourceView;
		fpSetRootUnorderedAccessView = &::ID3D12GraphicsCommandList::SetGraphicsRootUnorderedAccessView;
		break;
	case ::framework::gpu::pipeline_bind_point::compute:
	case ::framework::gpu::pipeline_bind_point::ray_tracing:
		this->d3d12_graphics_command_list->SetComputeRootSignature(static_cast<::framework::gpu::d3d12::pipeline_layout *>(layout)->get_d3d12_root_signature());
		fpSetRootDescriptorTable = &::ID3D12GraphicsCommandList::SetComputeRootDescriptorTable;
		fpSetRootConstantBufferView = &::ID3D12GraphicsCommandList::SetComputeRootConstantBufferView;
		fpSetRootShaderResourceView = &::ID3D12GraphicsCommandList::SetComputeRootShaderResourceView;
		fpSetRootUnorderedAccessView = &::ID3D12GraphicsCommandList::SetComputeRootUnorderedAccessView;
		break;
	}

	for (::std::uint32_t set = 0; set < static_cast<::framework::gpu::d3d12::pipeline_layout *>(layout)->get_descriptor_set_layout_count(); set++)
	{
		for (auto const & it : static_cast<::framework::gpu::d3d12::pipeline_layout *>(layout)->root_descriptor_tables[set])
		{
			(this->d3d12_graphics_command_list->*fpSetRootDescriptorTable)(it.root_index, static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_sets[set])->get_gpu_descriptor_range_start(it.d3d12_descriptor_heap_type));
		}

		for (auto const & it : static_cast<::framework::gpu::d3d12::pipeline_layout const *>(layout)->root_constant_buffer_views[set])
		{
			for (::std::uint32_t visibility = 0; visibility < it.visibility_count; visibility++)
			{
				for (::std::uint32_t descriptor = 0; descriptor < it.descriptor_count; descriptor++)
				{
					(this->d3d12_graphics_command_list->*fpSetRootConstantBufferView)(it.root_index + visibility, static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_sets[set])->dynamic_root_parameter_addresses[it.offset_from_start + descriptor * visibility] + dynamic_offsets[it.dynamic_offset + descriptor]);
				}
			}
		}

		for (auto const & it : static_cast<::framework::gpu::d3d12::pipeline_layout const *>(layout)->root_shader_resource_views[set])
		{
			for (::std::uint32_t visibility = 0; visibility < it.visibility_count; visibility++)
			{
				for (::std::uint32_t descriptor = 0; descriptor < it.descriptor_count; descriptor++)
				{
					(this->d3d12_graphics_command_list->*fpSetRootShaderResourceView)(it.root_index + visibility, static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_sets[set])->dynamic_root_parameter_addresses[it.offset_from_start + descriptor * visibility] + dynamic_offsets[it.dynamic_offset + descriptor]);
				}
			}
		}

		for (auto const & it : static_cast<::framework::gpu::d3d12::pipeline_layout const *>(layout)->root_unordered_access_views[set])
		{
			for (::std::uint32_t visibility = 0; visibility < it.visibility_count; visibility++)
			{
				for (::std::uint32_t descriptor = 0; descriptor < it.descriptor_count; descriptor++)
				{
					(this->d3d12_graphics_command_list->*fpSetRootUnorderedAccessView)(it.root_index + visibility, static_cast<::framework::gpu::d3d12::descriptor_set *>(descriptor_sets[set])->dynamic_root_parameter_addresses[it.offset_from_start + descriptor * visibility] + dynamic_offsets[it.dynamic_offset + descriptor]);
				}
			}
		}
	}
}

void ::framework::gpu::d3d12::command_buffer::bind_index_buffer(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::framework::gpu::index_type index_type)
{
	::DXGI_FORMAT dxgi_format;
	switch (index_type)
	{
	case ::framework::gpu::index_type::uint16:
		dxgi_format = ::DXGI_FORMAT::DXGI_FORMAT_R16_UINT;
		break;
	case ::framework::gpu::index_type::uint32:
		dxgi_format = ::DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
		break;
	}

	::D3D12_INDEX_BUFFER_VIEW d3d12_index_buffer_view;
	d3d12_index_buffer_view.BufferLocation = static_cast<::framework::gpu::d3d12::buffer const *>(buffer)->get_d3d12_resource()->GetGPUVirtualAddress() + offset;
	d3d12_index_buffer_view.Format = dxgi_format;
	d3d12_index_buffer_view.SizeInBytes = static_cast<::framework::gpu::d3d12::buffer const *>(buffer)->get_size();

	this->d3d12_graphics_command_list->IASetIndexBuffer(&d3d12_index_buffer_view);
}

void ::framework::gpu::d3d12::command_buffer::bind_vertex_buffers(::std::uint32_t first_binding, ::std::uint32_t binding_count, ::framework::gpu::buffer * const * buffers, ::framework::gpu::device_size const * offsets)
{
	::std::vector<::D3D12_VERTEX_BUFFER_VIEW> d3d12_vertex_buffer_view(static_cast<::std::size_t>(binding_count));
	for (::std::uint32_t i = 0, j = first_binding; i < binding_count; i++, j++)
	{
		d3d12_vertex_buffer_view[i].BufferLocation = static_cast<::framework::gpu::d3d12::buffer const *>(buffers[i])->get_d3d12_resource()->GetGPUVirtualAddress() + offsets[i];
		d3d12_vertex_buffer_view[i].SizeInBytes = static_cast<::UINT>(static_cast<::framework::gpu::d3d12::buffer const *>(buffers[i])->get_size() - offsets[i]);
		d3d12_vertex_buffer_view[i].StrideInBytes = static_cast<::framework::gpu::d3d12::graphics_pipeline const *>(this->pipeline)->get_vertex_binding_stride(j);
	}

	this->d3d12_graphics_command_list->IASetVertexBuffers(first_binding, binding_count, d3d12_vertex_buffer_view.data());
}

void ::framework::gpu::d3d12::command_buffer::draw(::std::uint32_t vertex_count, ::std::uint32_t instance_count, ::std::uint32_t first_vertex, ::std::uint32_t first_instance)
{
	this->commit_rasterizer_state();
	this->d3d12_graphics_command_list->DrawInstanced(vertex_count, instance_count, first_vertex, first_instance);
}

void ::framework::gpu::d3d12::command_buffer::draw_indexed(::std::uint32_t index_count, ::std::uint32_t instance_count, ::std::uint32_t first_index, ::std::int32_t vertex_offset, ::std::uint32_t first_instance)
{
	this->commit_rasterizer_state();
	this->d3d12_graphics_command_list->DrawIndexedInstanced(index_count, instance_count, first_index, vertex_offset, first_instance);
}

void ::framework::gpu::d3d12::command_buffer::draw_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride)
{
	this->commit_rasterizer_state();
	this->d3d12_graphics_command_list->ExecuteIndirect(this->device->get_draw_indirect_command_signature(), draw_count, static_cast<::framework::gpu::d3d12::buffer const *>(buffer)->get_d3d12_resource(), static_cast<::UINT64>(offset), nullptr, 0);
}

void ::framework::gpu::d3d12::command_buffer::draw_indexed_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride)
{
	this->commit_rasterizer_state();
	this->d3d12_graphics_command_list->ExecuteIndirect(this->device->get_draw_indexed_indirect_command_signature(), draw_count, static_cast<::framework::gpu::d3d12::buffer const *>(buffer)->get_d3d12_resource(), static_cast<::UINT64>(offset), nullptr, 0);
}

void ::framework::gpu::d3d12::command_buffer::dispatch(::std::uint32_t group_count_x, ::std::uint32_t group_count_y, ::std::uint32_t group_count_z)
{
	this->d3d12_graphics_command_list->Dispatch(group_count_x, group_count_y, group_count_z);
}

void ::framework::gpu::d3d12::command_buffer::dispatch_indirect(::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset)
{
	this->d3d12_graphics_command_list->ExecuteIndirect(this->device->get_dispatch_indirect_command_signature(), 1, static_cast<::framework::gpu::d3d12::buffer const *>(buffer)->get_d3d12_resource(), static_cast<::UINT64>(offset), nullptr, 0);
}

void ::framework::gpu::d3d12::command_buffer::copy_buffer(::framework::gpu::buffer * src_buffer, ::framework::gpu::buffer * dst_buffer, ::std::uint32_t region_count, ::framework::gpu::buffer_copy const * regions)
{
	for (::std::uint32_t i = 0; i < region_count; i++)
	{
		this->d3d12_graphics_command_list->CopyBufferRegion(static_cast<::framework::gpu::d3d12::buffer const *>(dst_buffer)->get_d3d12_resource(), regions[i].dst_offset, static_cast<::framework::gpu::d3d12::buffer const *>(src_buffer)->get_d3d12_resource(), regions[i].src_offset, regions[i].size);
	}
}

void ::framework::gpu::d3d12::command_buffer::copy_image(::framework::gpu::image * src_image, ::framework::gpu::image_layout_flags src_image_layout, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, ::framework::gpu::image_copy const * regions)
{
	::D3D12_RESOURCE_DESC const & src_image_resource_desc = static_cast<::framework::gpu::d3d12::image *>(src_image)->get_d3d12_resource()->GetDesc();
	::D3D12_RESOURCE_DESC const & dst_image_resource_desc = static_cast<::framework::gpu::d3d12::image *>(dst_image)->get_d3d12_resource()->GetDesc();

	for (::std::uint32_t i = 0; i < region_count; i++)
	{
		::D3D12_TEXTURE_COPY_LOCATION dst;
		dst.pResource = static_cast<::framework::gpu::d3d12::image *>(dst_image)->get_d3d12_resource();
		dst.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		dst.SubresourceIndex = regions[i].src_subresource.mip_level + regions[i].src_subresource.base_array_layer * dst_image_resource_desc.MipLevels + (!!(regions[i].src_subresource.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit) ? 1 : 0) * dst_image_resource_desc.MipLevels * regions[i].src_subresource.layer_count;

		::D3D12_TEXTURE_COPY_LOCATION src;
		src.pResource = static_cast<::framework::gpu::d3d12::image *>(src_image)->get_d3d12_resource();
		src.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		src.SubresourceIndex = regions[i].src_subresource.mip_level + regions[i].src_subresource.base_array_layer * src_image_resource_desc.MipLevels + (!!(regions[i].src_subresource.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit) ? 1 : 0) * src_image_resource_desc.MipLevels * regions[i].src_subresource.layer_count;

		::D3D12_BOX src_box;
		src_box.left = regions[i].src_offset.x;
		src_box.top = regions[i].src_offset.y;
		src_box.front = regions[i].src_offset.z;
		src_box.right = regions[i].src_offset.x + regions[i].extent.width;
		src_box.bottom = regions[i].src_offset.y + regions[i].extent.height;
		src_box.back = regions[i].src_offset.z + regions[i].extent.depth;

		this->d3d12_graphics_command_list->CopyTextureRegion(&dst, regions[i].dst_offset.x, regions[i].dst_offset.y, regions[i].dst_offset.z, &src, &src_box);
	}
}

void ::framework::gpu::d3d12::command_buffer::copy_buffer_to_image(::framework::gpu::buffer * src_buffer, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, ::framework::gpu::buffer_image_copy const * regions)
{
	::D3D12_RESOURCE_DESC const & dst_image_resource_desc = static_cast<::framework::gpu::d3d12::image *>(dst_image)->get_d3d12_resource()->GetDesc();

	for (::std::uint32_t i = 0; i < region_count; i++)
	{
		::D3D12_TEXTURE_COPY_LOCATION dst;
		dst.pResource = static_cast<::framework::gpu::d3d12::image *>(dst_image)->get_d3d12_resource();
		dst.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		dst.SubresourceIndex = regions[i].image_subresource.mip_level + regions[i].image_subresource.base_array_layer * dst_image_resource_desc.MipLevels + (!!(regions[i].image_subresource.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit) ? 1 : 0) * dst_image_resource_desc.MipLevels * regions[i].image_subresource.layer_count;

		::D3D12_TEXTURE_COPY_LOCATION src;
		src.pResource = static_cast<::framework::gpu::d3d12::buffer *>(src_buffer)->get_d3d12_resource();
		src.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
		src.PlacedFootprint.Offset = regions[i].buffer_offset;
		src.PlacedFootprint.Footprint.Format = dst_image_resource_desc.Format;
		src.PlacedFootprint.Footprint.Width = regions[i].image_extent.width;
		src.PlacedFootprint.Footprint.Height = regions[i].image_extent.height;
		src.PlacedFootprint.Footprint.Depth = regions[i].image_extent.depth;
		src.PlacedFootprint.Footprint.RowPitch = (::framework::gpu::d3d12::calc_row_pitch(src.PlacedFootprint.Footprint.Format, src.PlacedFootprint.Footprint.Width) + D3D12_TEXTURE_DATA_PITCH_ALIGNMENT - 1) & ~(D3D12_TEXTURE_DATA_PITCH_ALIGNMENT - 1);

		this->d3d12_graphics_command_list->CopyTextureRegion(&dst, regions[i].image_offset.x, regions[i].image_offset.y, regions[i].image_offset.z, &src, nullptr);
	}
}

void ::framework::gpu::d3d12::command_buffer::copy_image_to_buffer(::framework::gpu::copy_image_to_buffer_info const * copy_image_to_buffer_info)
{
	::D3D12_RESOURCE_DESC const & src_image_resource_desc = static_cast<::framework::gpu::d3d12::image *>(copy_image_to_buffer_info->src_image)->get_d3d12_resource()->GetDesc();

	for (::std::uint32_t i = 0; i < copy_image_to_buffer_info->region_count; i++)
	{
		::D3D12_TEXTURE_COPY_LOCATION dst;
		dst.pResource = static_cast<::framework::gpu::d3d12::buffer *>(copy_image_to_buffer_info->dst_buffer)->get_d3d12_resource();
		dst.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
		dst.PlacedFootprint.Offset = copy_image_to_buffer_info->regions[i].buffer_offset;
		dst.PlacedFootprint.Footprint.Format = src_image_resource_desc.Format;
		dst.PlacedFootprint.Footprint.Width = copy_image_to_buffer_info->regions[i].image_extent.width;
		dst.PlacedFootprint.Footprint.Height = copy_image_to_buffer_info->regions[i].image_extent.height;
		dst.PlacedFootprint.Footprint.Depth = copy_image_to_buffer_info->regions[i].image_extent.depth;
		dst.PlacedFootprint.Footprint.RowPitch = copy_image_to_buffer_info->regions[i].buffer_row_length;

		uint64_t CopySize = 0;
		D3D12_PLACED_SUBRESOURCE_FOOTPRINT PlacedFootprint;
		auto desc = static_cast<::framework::gpu::d3d12::image *>(copy_image_to_buffer_info->src_image)->get_d3d12_resource()->GetDesc();
		this->device->get_d3d12_device()->GetCopyableFootprints(&desc, 0, 1, 0,
			&PlacedFootprint, nullptr, nullptr, &CopySize);

		::D3D12_TEXTURE_COPY_LOCATION src;
		src.pResource = static_cast<::framework::gpu::d3d12::image *>(copy_image_to_buffer_info->src_image)->get_d3d12_resource();
		src.Type = ::D3D12_TEXTURE_COPY_TYPE::D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
		src.SubresourceIndex = copy_image_to_buffer_info->regions[i].image_subresource.mip_level + copy_image_to_buffer_info->regions[i].image_subresource.base_array_layer * src_image_resource_desc.MipLevels + (!!(copy_image_to_buffer_info->regions[i].image_subresource.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit) ? 1 : 0) * src_image_resource_desc.MipLevels * copy_image_to_buffer_info->regions[i].image_subresource.layer_count;

		::D3D12_BOX src_box;
		src_box.left = copy_image_to_buffer_info->regions[i].image_offset.x;
		src_box.top = copy_image_to_buffer_info->regions[i].image_offset.y;
		src_box.front = copy_image_to_buffer_info->regions[i].image_offset.z;
		src_box.right = copy_image_to_buffer_info->regions[i].image_offset.x + copy_image_to_buffer_info->regions[i].image_extent.width;
		src_box.bottom = copy_image_to_buffer_info->regions[i].image_offset.y + copy_image_to_buffer_info->regions[i].image_extent.height;
		src_box.back = copy_image_to_buffer_info->regions[i].image_offset.z + copy_image_to_buffer_info->regions[i].image_extent.depth;

		this->d3d12_graphics_command_list->CopyTextureRegion(&dst, 0, 0, 0, &src, &src_box);
	}
}

void ::framework::gpu::d3d12::command_buffer::update_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size data_size, void const * data)
{
}

void ::framework::gpu::d3d12::command_buffer::fill_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size size, ::std::uint32_t data, ::framework::gpu::device_size dst_view_offset, ::framework::gpu::device_descriptor_handle view_device_handle_in_current_pool, ::framework::gpu::host_descriptor_handle view_host_handle)
{
	::UINT const clear_color[4] = { data };
	::D3D12_RECT clear_rect;
	clear_rect.left = static_cast<::LONG>(dst_view_offset / 4);
	clear_rect.top = 0;
	clear_rect.right = static_cast<::LONG>((dst_view_offset + size) / 4);
	clear_rect.bottom = 1;
	this->d3d12_graphics_command_list->ClearUnorderedAccessViewUint(
		::framework::gpu::d3d12::get_d3d12_gpu_descriptor_handle(view_device_handle_in_current_pool),
		::framework::gpu::d3d12::get_d3d12_cpu_descriptor_handle(view_host_handle),
		static_cast<::framework::gpu::d3d12::buffer *>(dst_buffer)->get_d3d12_resource(),
		clear_color,
		1,
		&clear_rect
	);
}

void ::framework::gpu::d3d12::command_buffer::clear_color_image(::framework::gpu::image * image, ::framework::gpu::image_layout_flags image_layout, ::framework::gpu::clear_color_value const * color, ::std::uint32_t range_count, ::framework::gpu::image_subresource_range const * ranges, ::framework::gpu::host_descriptor_handle image_view_descriptor_handle)
{
	for (::std::uint32_t i = 0; i < range_count; i++)
	{
		this->d3d12_graphics_command_list->ClearRenderTargetView(::framework::gpu::d3d12::get_d3d12_cpu_descriptor_handle(image_view_descriptor_handle), reinterpret_cast<::FLOAT const *>(color), 0, nullptr);
	}
}

void ::framework::gpu::d3d12::command_buffer::pipeline_barrier(::framework::gpu::dependency_info const * dependency_info)
{
	if (!!(dependency_info->dependency_flags & ::framework::gpu::dependency_flags::legacy_bit))
	{
		::std::vector<::D3D12_RESOURCE_BARRIER> d3d12_resource_barriers;

		for (::std::uint32_t buffer_memory_barrier = 0; buffer_memory_barrier < dependency_info->buffer_memory_barrier_count; buffer_memory_barrier++)
		{
			if (dependency_info->buffer_memory_barriers[buffer_memory_barrier].src_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index ||
				dependency_info->buffer_memory_barriers[buffer_memory_barrier].dst_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
			{
				continue;
			}

			::D3D12_RESOURCE_STATES state_before = ::framework::gpu::d3d12::get_d3d12_resource_states(dependency_info->buffer_memory_barriers[buffer_memory_barrier].old_state);
			::D3D12_RESOURCE_STATES state_after = ::framework::gpu::d3d12::get_d3d12_resource_states(dependency_info->buffer_memory_barriers[buffer_memory_barrier].new_state);

			if (state_before != state_after)
			{
				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.Transition.pResource = static_cast<::framework::gpu::d3d12::buffer const *>(dependency_info->buffer_memory_barriers[buffer_memory_barrier].buffer)->get_d3d12_resource();
				d3d12_resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
				d3d12_resource_barrier.Transition.StateBefore = state_before;
				d3d12_resource_barrier.Transition.StateAfter = state_after;
				d3d12_resource_barriers.push_back(d3d12_resource_barrier);
			}
			else if (state_after & ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS)
			{
				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_UAV;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.UAV.pResource = static_cast<::framework::gpu::d3d12::buffer const *>(dependency_info->buffer_memory_barriers[buffer_memory_barrier].buffer)->get_d3d12_resource();
				d3d12_resource_barriers.push_back(d3d12_resource_barrier);
			}
		}

		for (::std::uint32_t image_memory_barrier = 0; image_memory_barrier < dependency_info->image_memory_barrier_count; ++image_memory_barrier)
		{
			if (dependency_info->image_memory_barriers[image_memory_barrier].src_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index ||
				dependency_info->image_memory_barriers[image_memory_barrier].dst_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
			{
				continue;
			}

			::D3D12_RESOURCE_STATES state_before = ::framework::gpu::d3d12::get_d3d12_resource_states(dependency_info->image_memory_barriers[image_memory_barrier].old_layout);
			::D3D12_RESOURCE_STATES state_after = ::framework::gpu::d3d12::get_d3d12_resource_states(dependency_info->image_memory_barriers[image_memory_barrier].new_layout);

			if (state_before != state_after)
			{
				auto const & subresource_range = dependency_info->image_memory_barriers[image_memory_barrier].subresource_range;
				::UINT plane_slice;
				if (!!(subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit))
					plane_slice = 1;
				else
					plane_slice = 0;

				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.Transition.pResource = static_cast<::framework::gpu::d3d12::image const *>(dependency_info->image_memory_barriers[image_memory_barrier].image)->get_d3d12_resource();
				d3d12_resource_barrier.Transition.Subresource = subresource_range.base_mip_level + subresource_range.base_array_layer * subresource_range.level_count + plane_slice * subresource_range.level_count * subresource_range.layer_count;
				d3d12_resource_barrier.Transition.StateBefore = state_before;
				d3d12_resource_barrier.Transition.StateAfter = state_after;
				d3d12_resource_barriers.push_back(d3d12_resource_barrier);
			}
			else if (state_after & ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS)
			{
				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_UAV;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.UAV.pResource = static_cast<::framework::gpu::d3d12::image const *>(dependency_info->image_memory_barriers[image_memory_barrier].image)->get_d3d12_resource();
				d3d12_resource_barriers.push_back(d3d12_resource_barrier);
			}
		}

		if (d3d12_resource_barriers.size())
		{
			this->d3d12_graphics_command_list->ResourceBarrier(static_cast<::UINT>(d3d12_resource_barriers.size()), d3d12_resource_barriers.data());
		}
	}
	else
	{
		::std::vector<::D3D12_BARRIER_GROUP> d3d12_barrier_groups;
		::std::vector<::D3D12_GLOBAL_BARRIER> d3d12_global_barriers;
		::std::vector<::D3D12_BUFFER_BARRIER> d3d12_buffer_barriers;
		::std::vector<::D3D12_TEXTURE_BARRIER> d3d12_texture_barrier;

		for (::std::uint32_t memory_barrier_index = 0; memory_barrier_index < dependency_info->memory_barrier_count; ++memory_barrier_index)
		{
			::framework::gpu::memory_barrier const & memory_barrier = dependency_info->memory_barriers[memory_barrier_index];

			d3d12_global_barriers.emplace_back(
				::D3D12_GLOBAL_BARRIER
				{
					.SyncBefore = ::framework::gpu::d3d12::get_d3d12_barrier_sync(memory_barrier.src_stage_mask),
					.SyncAfter = ::framework::gpu::d3d12::get_d3d12_barrier_sync(memory_barrier.dst_stage_mask),
					.AccessBefore = ::framework::gpu::d3d12::get_d3d12_barrier_access(memory_barrier.src_access_mask),
					.AccessAfter = ::framework::gpu::d3d12::get_d3d12_barrier_access(memory_barrier.dst_access_mask)
				}
			);
		}

		if (!d3d12_global_barriers.empty())
		{
			d3d12_barrier_groups.emplace_back(
				::D3D12_BARRIER_GROUP
				{
					.Type = ::D3D12_BARRIER_TYPE::D3D12_BARRIER_TYPE_GLOBAL,
					.NumBarriers = dependency_info->memory_barrier_count,
					.pGlobalBarriers = d3d12_global_barriers.data()
				}
			);
		}

		for (::std::uint32_t buffer_memory_barrier_index = 0; buffer_memory_barrier_index < dependency_info->buffer_memory_barrier_count; ++buffer_memory_barrier_index)
		{
			::framework::gpu::buffer_memory_barrier const & buffer_memory_barrier = dependency_info->buffer_memory_barriers[buffer_memory_barrier_index];
			if (buffer_memory_barrier.src_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index ||
				buffer_memory_barrier.dst_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
			{
				continue;
			}

			d3d12_buffer_barriers.emplace_back(
				::D3D12_BUFFER_BARRIER
				{
					.SyncBefore = ::framework::gpu::d3d12::get_d3d12_barrier_sync(buffer_memory_barrier.src_stage_mask),
					.SyncAfter = ::framework::gpu::d3d12::get_d3d12_barrier_sync(buffer_memory_barrier.dst_stage_mask),
					.AccessBefore = ::framework::gpu::d3d12::get_d3d12_barrier_access(buffer_memory_barrier.src_access_mask),
					.AccessAfter = ::framework::gpu::d3d12::get_d3d12_barrier_access(buffer_memory_barrier.dst_access_mask),
					.pResource = static_cast<::framework::gpu::d3d12::buffer const *>(buffer_memory_barrier.buffer)->get_d3d12_resource(),
					.Offset = buffer_memory_barrier.offset,
					.Size = buffer_memory_barrier.size
				}
			);
		}

		if (!d3d12_buffer_barriers.empty())
		{
			d3d12_barrier_groups.emplace_back(
				::D3D12_BARRIER_GROUP
				{
					.Type = ::D3D12_BARRIER_TYPE::D3D12_BARRIER_TYPE_BUFFER,
					.NumBarriers = dependency_info->buffer_memory_barrier_count,
					.pBufferBarriers = d3d12_buffer_barriers.data()
				}
			);
		}

		for (::std::uint32_t image_memory_barrier_index = 0; image_memory_barrier_index < dependency_info->image_memory_barrier_count; ++image_memory_barrier_index)
		{
			::framework::gpu::image_memory_barrier const & image_memory_barrier = dependency_info->image_memory_barriers[image_memory_barrier_index];
			if (image_memory_barrier.src_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index ||
				image_memory_barrier.dst_queue_family_ownership.queue_family_index == ::framework::gpu::d3d12::transfer_family_index)
			{
				continue;
			}

			::D3D12_TEXTURE_BARRIER_FLAGS d3d12_texture_barrier_flags = ::D3D12_TEXTURE_BARRIER_FLAGS::D3D12_TEXTURE_BARRIER_FLAG_NONE;
			if (!!(image_memory_barrier.old_layout & ::framework::gpu::image_layout_flags::undefined_bit))
				d3d12_texture_barrier_flags |= ::D3D12_TEXTURE_BARRIER_FLAGS::D3D12_TEXTURE_BARRIER_FLAG_DISCARD;

			::UINT first_plane = 0;
			::UINT num_planes = 0;
			if (!!(image_memory_barrier.subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::stencil_bit))
			{
				first_plane = 1;
				num_planes = 1;
			}

			if (!!(image_memory_barrier.subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::color_bit) ||
				!!(image_memory_barrier.subresource_range.aspect_mask & ::framework::gpu::image_aspect_flags::depth_bit))
			{
				first_plane = 0;
				num_planes++;
			}

			d3d12_texture_barrier.emplace_back(
				::D3D12_TEXTURE_BARRIER
				{
					.SyncBefore = ::framework::gpu::d3d12::get_d3d12_barrier_sync(image_memory_barrier.src_stage_mask),
					.SyncAfter = ::framework::gpu::d3d12::get_d3d12_barrier_sync(image_memory_barrier.dst_stage_mask),
					.AccessBefore = ::framework::gpu::d3d12::get_d3d12_barrier_access(image_memory_barrier.src_access_mask),
					.AccessAfter = ::framework::gpu::d3d12::get_d3d12_barrier_access(image_memory_barrier.dst_access_mask),
					.LayoutBefore = ::framework::gpu::d3d12::get_d3d12_barrier_layout(image_memory_barrier.old_layout, image_memory_barrier.src_queue_family_ownership),
					.LayoutAfter = ::framework::gpu::d3d12::get_d3d12_barrier_layout(image_memory_barrier.new_layout, image_memory_barrier.dst_queue_family_ownership),
					.pResource = static_cast<::framework::gpu::d3d12::image const *>(image_memory_barrier.image)->get_d3d12_resource(),
					.Subresources
				{
					.IndexOrFirstMipLevel = image_memory_barrier.subresource_range.base_mip_level,
					.NumMipLevels = image_memory_barrier.subresource_range.level_count,
					.FirstArraySlice = image_memory_barrier.subresource_range.base_array_layer,
					.NumArraySlices = image_memory_barrier.subresource_range.layer_count,
					.FirstPlane = first_plane,
					.NumPlanes = num_planes
				},
				.Flags = d3d12_texture_barrier_flags
				}
			);
		}

		if (!d3d12_texture_barrier.empty())
		{
			d3d12_barrier_groups.emplace_back(
				::D3D12_BARRIER_GROUP
				{
					.Type = ::D3D12_BARRIER_TYPE::D3D12_BARRIER_TYPE_TEXTURE,
					.NumBarriers = dependency_info->image_memory_barrier_count,
					.pTextureBarriers = d3d12_texture_barrier.data()
				}
			);
		}

		if (!d3d12_barrier_groups.empty())
		{
			this->d3d12_graphics_command_list->Barrier(static_cast<::UINT>(d3d12_barrier_groups.size()), d3d12_barrier_groups.data());
		}
	}
}

void ::framework::gpu::d3d12::command_buffer::begin_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query, ::framework::gpu::query_control_flags flags)
{
	this->d3d12_graphics_command_list->BeginQuery(static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_heap(), static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_type(), query);
}

void ::framework::gpu::d3d12::command_buffer::end_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query)
{
	this->d3d12_graphics_command_list->EndQuery(static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_heap(), static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_type(), query);
}

void ::framework::gpu::d3d12::command_buffer::reset_query_pool(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count)
{
}

void ::framework::gpu::d3d12::command_buffer::write_timestamp(::framework::gpu::pipeline_stage_flags pipeline_stage, ::framework::gpu::query_pool * query_pool, ::std::uint32_t query)
{
	this->d3d12_graphics_command_list->EndQuery(static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_heap(), static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_type(), query);
}

void ::framework::gpu::d3d12::command_buffer::copy_query_pool_results(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags)
{
	this->d3d12_graphics_command_list->ResolveQueryData(static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_heap(), static_cast<::framework::gpu::d3d12::query_pool *>(query_pool)->get_d3d12_query_type(), first_query, query_count, static_cast<::framework::gpu::d3d12::buffer *>(dst_buffer)->get_d3d12_resource(), dst_offset);
}

void ::framework::gpu::d3d12::command_buffer::build_acceleration_structures(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos)
{
	this->d3d12_graphics_command_list->ClearState(nullptr); // NOTE: workaround for nvidia driver bug when previousely set PSO leads to broken AS, TODO: remove it

	for (::std::uint32_t i = 0; i < info_count; i++)
	{
		::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC d3d12_build_raytracing_acceleration_structure_desc;
		d3d12_build_raytracing_acceleration_structure_desc.DestAccelerationStructureData = static_cast<::framework::gpu::d3d12::acceleration_structure *>(infos[i].dst_acceleration_structure)->get_d3d12_gpu_virtual_address();
		d3d12_build_raytracing_acceleration_structure_desc.Inputs.Type = ::framework::gpu::d3d12::get_d3d12_raytracing_acceleration_structure_type(infos[i].type);
		d3d12_build_raytracing_acceleration_structure_desc.Inputs.Flags = ::framework::gpu::d3d12::get_d3d12_raytracing_acceleration_structure_build_flags(infos[i].flags);
		static_cast<::framework::gpu::d3d12::acceleration_structure_geometry_set *>(infos[i].geometries)->init_d3d12_build_raytracing_acceleration_structure_inputs(infos[i].first_geometry, infos[i].geometry_count, d3d12_build_raytracing_acceleration_structure_desc.Inputs);
		d3d12_build_raytracing_acceleration_structure_desc.SourceAccelerationStructureData = infos[i].src_acceleration_structure ? static_cast<::framework::gpu::d3d12::acceleration_structure *>(infos[i].src_acceleration_structure)->get_d3d12_gpu_virtual_address() : NULL;
		d3d12_build_raytracing_acceleration_structure_desc.ScratchAccelerationStructureData = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(infos[i].scratch_data.device_address);
		this->d3d12_graphics_command_list->BuildRaytracingAccelerationStructure(&d3d12_build_raytracing_acceleration_structure_desc, 0, nullptr);
	}
}

void ::framework::gpu::d3d12::command_buffer::build_acceleration_structures_indirect(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos, ::framework::gpu::device_address * indirect_device_addresses, ::std::uint32_t const * indirect_strides, ::std::uint32_t const * const * max_primitive_counts)
{
}

void ::framework::gpu::d3d12::command_buffer::copy_acceleration_structure(::framework::gpu::copy_acceleration_structure_info const * info)
{
}

void ::framework::gpu::d3d12::command_buffer::copy_acceleration_structure_to_memory(::framework::gpu::copy_acceleration_structure_to_memory_info const * info)
{
}

void ::framework::gpu::d3d12::command_buffer::copy_memory_to_acceleration_structure(::framework::gpu::copy_memory_to_acceleration_structure_info const * info)
{
}

void ::framework::gpu::d3d12::command_buffer::trace_rays(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::std::uint32_t width, ::std::uint32_t height, ::std::uint32_t depth)
{
	::D3D12_DISPATCH_RAYS_DESC d3d12_dispatch_rays_desc;
	d3d12_dispatch_rays_desc.RayGenerationShaderRecord = ::framework::gpu::d3d12::get_d3d12_gpu_virtual_address_range(raygen_shader_binding_table);
	d3d12_dispatch_rays_desc.MissShaderTable = ::framework::gpu::d3d12::get_d3d12_gpu_virtual_address_range_and_stride(miss_shader_binding_table);
	d3d12_dispatch_rays_desc.HitGroupTable = ::framework::gpu::d3d12::get_d3d12_gpu_virtual_address_range_and_stride(hit_shader_binding_table);
	d3d12_dispatch_rays_desc.CallableShaderTable = ::framework::gpu::d3d12::get_d3d12_gpu_virtual_address_range_and_stride(callable_shader_binding_table);
	d3d12_dispatch_rays_desc.Width = static_cast<::UINT>(width);
	d3d12_dispatch_rays_desc.Height = static_cast<::UINT>(height);
	d3d12_dispatch_rays_desc.Depth = static_cast<::UINT>(depth);
	this->d3d12_graphics_command_list->DispatchRays(&d3d12_dispatch_rays_desc);
}

void ::framework::gpu::d3d12::command_buffer::trace_rays_indirect(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset)
{
}

void framework::gpu::d3d12::command_buffer::set_ray_tracing_pipeline_stack_size(::std::uint32_t pipeline_stack_size)
{
	static_cast<::framework::gpu::d3d12::ray_tracing_pipeline const *>(this->pipeline)->get_d3d12_state_object_properties()->SetPipelineStackSize(static_cast<::UINT64>(pipeline_stack_size));
}

void ::framework::gpu::d3d12::command_buffer::subpass(::framework::gpu::subpass_contents contents)
{
}
