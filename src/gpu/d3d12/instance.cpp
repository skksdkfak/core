#include "gpu/d3d12/core.hpp"
#include <cstring>

::framework::gpu::result(::framework::gpu::d3d12::instance::create_instance)(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance)
{
	::framework::gpu::result result;

	try
	{
		::framework::gpu::allocation_callbacks default_allocation
		{
			.pfn_allocation = ::framework::gpu::d3d12::default_allocation,
			.pfn_reallocation = ::framework::gpu::d3d12::default_reallocation,
			.pfn_free = ::framework::gpu::d3d12::default_free,
		};

		::framework::gpu::allocation_callbacks selected_allocator = allocator ? *allocator : default_allocation;

		*instance = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::instance>(default_allocation, ::framework::gpu::system_allocation_scope::object, create_info, default_allocation, result).release();
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}

void ::framework::gpu::d3d12::instance::destroy_instance(::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator)
{
	::framework::gpu::allocation_callbacks default_allocation
	{
		.pfn_allocation = ::framework::gpu::d3d12::default_allocation,
		.pfn_reallocation = ::framework::gpu::d3d12::default_reallocation,
		.pfn_free = ::framework::gpu::d3d12::default_free,
	};

	DELETE(static_cast<::framework::gpu::d3d12::instance *>(instance), allocator ? *allocator : default_allocation);
}

::framework::gpu::d3d12::instance::instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	bool enable_debug_layer = false;
	bool gpu_based_validation = false;
	for (::std::uint32_t i = 0; i < create_info->enabled_extension_count; i++)
	{
		if (::std::strcmp(::framework::gpu::debug_utils_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			enable_debug_layer = true;
		}
		else if (::std::strcmp(::framework::gpu::validation_features_extension_name, create_info->enabled_extension_names[i]) == 0)
		{
			gpu_based_validation = true;
		}
	}
	::Microsoft::WRL::ComPtr<::ID3D12Debug> d3d12_debug;
	::Microsoft::WRL::ComPtr<::ID3D12Debug1> d3d12_debug1;
	result = ::framework::gpu::d3d12::throw_result(::D3D12GetDebugInterface(IID_PPV_ARGS(&d3d12_debug)));
	result = ::framework::gpu::d3d12::throw_result(d3d12_debug->QueryInterface(IID_PPV_ARGS(&d3d12_debug1)));

	::UINT dxgi_factory_flags = 0;
	if (enable_debug_layer)
	{
		d3d12_debug->EnableDebugLayer();
		dxgi_factory_flags |= DXGI_CREATE_FACTORY_DEBUG;
	}

	if (gpu_based_validation)
	{
		d3d12_debug1->SetEnableGPUBasedValidation(true);

	}

	::Microsoft::WRL::ComPtr<::IDXGIFactory2> dxgi_factory_2;
	result = ::framework::gpu::d3d12::throw_result(::CreateDXGIFactory2(dxgi_factory_flags, IID_PPV_ARGS(&dxgi_factory_2)));
	dxgi_factory_2.As(&this->dxgi_factory);

	::UUID const features[] = { ::D3D12ExperimentalShaderModels, ::D3D12TiledResourceTier4 };

	result = ::framework::gpu::d3d12::throw_result(::D3D12EnableExperimentalFeatures(static_cast<::UINT>(::std::size(features)), features, nullptr, nullptr));

	::Microsoft::WRL::ComPtr<::IDXGIAdapter1> dxgi_adapter_1;
	for (::UINT adapter_index = 0; this->dxgi_factory->EnumAdapterByGpuPreference(adapter_index, ::DXGI_GPU_PREFERENCE::DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&dxgi_adapter_1)) != DXGI_ERROR_NOT_FOUND; adapter_index++)
	{
		this->physical_devices.push_back(new ::framework::gpu::d3d12::physical_device(this, this->allocator, dxgi_adapter_1.Detach()));
	}

	result = ::framework::gpu::d3d12::throw_result(::DxcCreateInstance(::CLSID_DxcUtils, IID_PPV_ARGS(&this->dxc_utils)));
	result = ::framework::gpu::d3d12::throw_result(::DxcCreateInstance(::CLSID_DxcCompiler, IID_PPV_ARGS(&this->dxc_compiler)));
}

void ::framework::gpu::d3d12::instance::enumerate_physical_devices(::std::uint32_t * physical_device_count, ::framework::gpu::physical_device ** physical_devices)
{
	if (physical_devices)
	{
		::std::memcpy(physical_devices, this->physical_devices.data(), sizeof(::framework::gpu::physical_device *) * *physical_device_count);
	}
	else
	{
		*physical_device_count = static_cast<::std::uint32_t>(this->physical_devices.size());
	}
}

void ::framework::gpu::d3d12::instance::create_surface(::framework::gpu::surface_create_info const * create_info, class ::framework::gpu::surface ** surface)
{
	*surface = new ::framework::gpu::d3d12::surface(this, create_info);
}

void ::framework::gpu::d3d12::instance::destroy_surface(class ::framework::gpu::surface * surface)
{
	delete static_cast<::framework::gpu::d3d12::surface *>(surface);
}