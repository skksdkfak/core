#pragma once

#include "gpu/instance.h"

namespace framework::gpu::d3d12
{
	class instance final : public ::framework::gpu::instance
	{
	public:
		static ::framework::gpu::result create_instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::instance ** instance);

		static void destroy_instance(::framework::gpu::instance * instance, ::framework::gpu::allocation_callbacks const * allocator);

		instance(::framework::gpu::instance_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		void enumerate_physical_devices(::std::uint32_t * physical_device_count, class ::framework::gpu::physical_device ** physical_devices) override;

		void create_surface(::framework::gpu::surface_create_info const * create_info, class ::framework::gpu::surface ** surface) override;

		void destroy_surface(class ::framework::gpu::surface * surface) override;

		::IDXGIFactory6 * get_dxgi_factory() const { return this->dxgi_factory.Get(); }

		::IDxcUtils * get_dxc_utils() const { return this->dxc_utils.Get(); }

		::IDxcCompiler * get_dxc_compiler() const { return this->dxc_compiler.Get(); }

	private:
		::framework::gpu::allocation_callbacks const allocator;
		::Microsoft::WRL::ComPtr<::IDXGIFactory6> dxgi_factory;
		::Microsoft::WRL::ComPtr<::IDxcUtils> dxc_utils;
		::Microsoft::WRL::ComPtr<::IDxcCompiler> dxc_compiler;
		::std::vector<::framework::gpu::physical_device *> physical_devices;
	};
}