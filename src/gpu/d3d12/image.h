#pragma once

#include "gpu/image.h"
#include "resource.h"

namespace framework::gpu::d3d12
{
	class swap_chain;

	class image final : public ::framework::gpu::image, public ::framework::gpu::d3d12::resource
	{
	public:
		image(::framework::gpu::d3d12::device * device, ::framework::gpu::image_create_info const * create_info, ::framework::gpu::result & result);

		image(::framework::gpu::d3d12::device * device, ::framework::gpu::image_create_info const * create_info, ::ID3D12Resource * base_d3d12_resource, ::framework::gpu::result & result);

		~image();

		::UINT get_samples() const { return this->sample_count; }

		::framework::gpu::image_usage_flags get_usage() const { return this->usage; }

		::D3D12_CLEAR_VALUE * get_optimized_clear_value() const { return this->optimized_clear_value; }

	private:
		friend class ::framework::gpu::d3d12::device;
		friend class ::framework::gpu::d3d12::swap_chain;

		static ::UINT get_sample_count(::framework::gpu::sample_count_flags samples);

		::UINT sample_count;
		::framework::gpu::image_usage_flags usage;
		::D3D12_CLEAR_VALUE * optimized_clear_value;
	};
}