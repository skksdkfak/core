#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::acceleration_structure_geometry_set::acceleration_structure_geometry_set(::framework::gpu::d3d12::device * device, ::framework::gpu::acceleration_structure_geometry_set_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	geometry_count(create_info->geometry_count),
	geometry_type(create_info->geometry_type)
{
	::framework::gpu::common::unique_ptr<::D3D12_RAYTRACING_GEOMETRY_DESC[]> geometry_descs_array = ::framework::gpu::common::make_unique<::D3D12_RAYTRACING_GEOMETRY_DESC[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc[]> instances_desc_array = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::D3D12_RAYTRACING_GEOMETRY_DESC * []> geometry_descs_array_of_pointers = ::framework::gpu::common::make_unique<::D3D12_RAYTRACING_GEOMETRY_DESC * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);
	::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc * []> instances_desc_array_of_pointers = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::acceleration_structure_geometry_set::instances_desc * []>(this->allocator, ::framework::gpu::system_allocation_scope::object, this->geometry_count);

	if (create_info->layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		this->descs_layout = ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY;

		switch (create_info->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			this->geometry_descs_array = geometry_descs_array.get();
			break;
		case ::framework::gpu::geometry_type::instances:
			this->instances_desc_array = instances_desc_array.get();
			break;
		}
	}
	else
	{
		this->descs_layout = ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY_OF_POINTERS;

		switch (create_info->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			this->geometry_descs_array_of_pointers = geometry_descs_array_of_pointers.get();
			break;
		case ::framework::gpu::geometry_type::instances:
			this->instances_desc_array_of_pointers = instances_desc_array_of_pointers.get();
			break;
		}
	}

	geometry_descs_array.release();
	instances_desc_array.release();
	geometry_descs_array_of_pointers.release();
	instances_desc_array_of_pointers.release();

	result = ::framework::gpu::result::success;
}

::framework::gpu::d3d12::acceleration_structure_geometry_set::~acceleration_structure_geometry_set()
{
	if (this->descs_layout == ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY)
	{
		switch (this->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			DELETE_ARRAY(this->geometry_descs_array, this->geometry_count, this->allocator);
			break;
		case ::framework::gpu::geometry_type::instances:
			DELETE_ARRAY(this->instances_desc_array, this->geometry_count, this->allocator);
			break;
		}
	}
	else
	{
		switch (this->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			DELETE_ARRAY(this->geometry_descs_array_of_pointers, this->geometry_count, this->allocator);
			break;
		case ::framework::gpu::geometry_type::instances:
			DELETE_ARRAY(this->instances_desc_array_of_pointers, this->geometry_count, this->allocator);
			break;
		}
	}
}

void ::framework::gpu::d3d12::acceleration_structure_geometry_set::init_d3d12_build_raytracing_acceleration_structure_inputs(::std::uint32_t first_geometry, ::std::uint32_t geometry_count, ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS & d3d12_build_raytracing_acceleration_structure_inputs) const
{
	if (this->descs_layout == ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY)
	{
		switch (this->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			d3d12_build_raytracing_acceleration_structure_inputs.NumDescs = geometry_count;
			d3d12_build_raytracing_acceleration_structure_inputs.DescsLayout = this->descs_layout;
			d3d12_build_raytracing_acceleration_structure_inputs.pGeometryDescs = this->geometry_descs_array + first_geometry;
			break;
		case ::framework::gpu::geometry_type::instances:
			auto & instances_data = this->instances_desc_array[first_geometry];
			d3d12_build_raytracing_acceleration_structure_inputs.NumDescs = instances_data.num_descs;
			d3d12_build_raytracing_acceleration_structure_inputs.DescsLayout = instances_data.descs_layout;
			d3d12_build_raytracing_acceleration_structure_inputs.InstanceDescs = instances_data.instance_descs;
			break;
		}
	}
	else
	{
		switch (this->geometry_type)
		{
		case ::framework::gpu::geometry_type::triangles:
		case ::framework::gpu::geometry_type::aabbs:
			d3d12_build_raytracing_acceleration_structure_inputs.NumDescs = geometry_count;
			d3d12_build_raytracing_acceleration_structure_inputs.DescsLayout = this->descs_layout;
			d3d12_build_raytracing_acceleration_structure_inputs.ppGeometryDescs = this->geometry_descs_array_of_pointers + first_geometry;
			break;
		case ::framework::gpu::geometry_type::instances:
			auto & instances_data = this->instances_desc_array_of_pointers[first_geometry];
			d3d12_build_raytracing_acceleration_structure_inputs.NumDescs = instances_data->num_descs;
			d3d12_build_raytracing_acceleration_structure_inputs.DescsLayout = instances_data->descs_layout;
			d3d12_build_raytracing_acceleration_structure_inputs.InstanceDescs = instances_data->instance_descs;
			break;
		}
	}
}

void ::framework::gpu::d3d12::acceleration_structure_geometry_set::write_data(::framework::gpu::write_acceleration_structure_geometry const & write_info)
{
	if (write_info.layout == ::framework::gpu::acceleration_structure_geometry_set_layout::array)
	{
		for (::std::uint32_t src_geometry = 0, dst_geometry = write_info.first_geometry; src_geometry < write_info.geometry_count; src_geometry++, dst_geometry++)
		{
			switch (write_info.geometry_type)
			{
			case ::framework::gpu::geometry_type::triangles:
			{
				auto & geometry_desc = this->geometry_descs_array[dst_geometry];

				geometry_desc.Type = ::D3D12_RAYTRACING_GEOMETRY_TYPE::D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
				geometry_desc.Flags = ::framework::gpu::d3d12::get_d3d12_raytracing_geometry_flags(write_info.data.geometries[src_geometry].flags);
				geometry_desc.Triangles.Transform3x4 = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(write_info.data.geometries[src_geometry].geometry.triangles.transform_data.device_address) + write_info.data.geometries[src_geometry].geometry.triangles.transform_offset;
				geometry_desc.Triangles.IndexFormat = ::framework::gpu::d3d12::get_dxgi_format(write_info.data.geometries[src_geometry].geometry.triangles.index_type);
				geometry_desc.Triangles.VertexFormat = ::framework::gpu::d3d12::get_dxgi_format(write_info.data.geometries[src_geometry].geometry.triangles.vertex_format);
				geometry_desc.Triangles.IndexCount = write_info.data.geometries[src_geometry].primitive_count * 3;
				geometry_desc.Triangles.VertexCount = write_info.data.geometries[src_geometry].geometry.triangles.vertex_count;
				geometry_desc.Triangles.IndexBuffer = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(write_info.data.geometries[src_geometry].geometry.triangles.index_data.device_address);
				geometry_desc.Triangles.VertexBuffer.StartAddress = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(write_info.data.geometries[src_geometry].geometry.triangles.vertex_data.device_address) + geometry_desc.Triangles.VertexBuffer.StrideInBytes * write_info.data.geometries[src_geometry].geometry.triangles.first_vertex;
				geometry_desc.Triangles.VertexBuffer.StrideInBytes = static_cast<::UINT64>(write_info.data.geometries[src_geometry].geometry.triangles.vertex_stride);

				if (geometry_desc.Triangles.IndexBuffer)
				{
					geometry_desc.Triangles.IndexBuffer += write_info.data.geometries[src_geometry].primitive_offset;
				}
				else
				{
					geometry_desc.Triangles.VertexBuffer.StartAddress += write_info.data.geometries[src_geometry].primitive_offset;
				}
			}
			break;
			case ::framework::gpu::geometry_type::aabbs:
			{
				auto & geometry_desc = this->geometry_descs_array[dst_geometry];

				geometry_desc.Type = ::D3D12_RAYTRACING_GEOMETRY_TYPE::D3D12_RAYTRACING_GEOMETRY_TYPE_PROCEDURAL_PRIMITIVE_AABBS;
				geometry_desc.Flags = ::framework::gpu::d3d12::get_d3d12_raytracing_geometry_flags(write_info.data.geometries[src_geometry].flags);
				geometry_desc.AABBs.AABBCount = write_info.data.geometries[src_geometry].primitive_count;
				geometry_desc.AABBs.AABBs.StartAddress = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(write_info.data.geometries[src_geometry].geometry.aabbs.data.device_address) + write_info.data.geometries[src_geometry].primitive_offset;
				geometry_desc.AABBs.AABBs.StrideInBytes = static_cast<::UINT64>(write_info.data.geometries[src_geometry].geometry.aabbs.stride);
			}
			break;
			case ::framework::gpu::geometry_type::instances:
			{
				auto & geometry_desc = this->instances_desc_array[dst_geometry];

				geometry_desc.num_descs = write_info.data.geometries[src_geometry].primitive_count;
				geometry_desc.descs_layout = write_info.data.geometries[src_geometry].geometry.instances.array_of_pointers ? ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY_OF_POINTERS : ::D3D12_ELEMENTS_LAYOUT::D3D12_ELEMENTS_LAYOUT_ARRAY;
				geometry_desc.instance_descs = static_cast<::D3D12_GPU_VIRTUAL_ADDRESS>(write_info.data.geometries[src_geometry].geometry.instances.data.device_address) + write_info.data.geometries[src_geometry].primitive_offset;
			}
			break;
			}
		}
	}
	else
	{
		for (::std::uint32_t geometry = 0, dst_geometry = write_info.first_geometry; geometry < write_info.geometry_count; geometry++, dst_geometry++)
		{
			::framework::gpu::d3d12::acceleration_structure_geometry_set * src_set = static_cast<::framework::gpu::d3d12::acceleration_structure_geometry_set *>(write_info.data.geometry_references[geometry].src_set);
			::std::uint32_t src_geometry = write_info.data.geometry_references[geometry].src_array_element;

			switch (write_info.geometry_type)
			{
			case ::framework::gpu::geometry_type::triangles:
			case ::framework::gpu::geometry_type::aabbs:
				this->geometry_descs_array_of_pointers[dst_geometry] = &src_set->geometry_descs_array[src_geometry];
				break;
			case ::framework::gpu::geometry_type::instances:
				this->instances_desc_array_of_pointers[dst_geometry] = &src_set->instances_desc_array[src_geometry];
				break;
			}
		}
	}
}
