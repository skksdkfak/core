#pragma once

#include "pipeline.h"
#include <vector>

namespace framework::gpu::d3d12
{
	class graphics_pipeline final : public ::framework::gpu::d3d12::pipeline
	{
	public:
		graphics_pipeline(::framework::gpu::d3d12::device * device, ::framework::gpu::graphics_pipeline_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::ID3D12PipelineState * get_d3d12_pipeline_state() const
		{
			return this->d3d12_pipeline_state.Get();
		}

		bool has_dynamic_state(::framework::gpu::dynamic_state dynamicState) const
		{
			return this->dynamic_states[static_cast<::std::underlying_type_t<::framework::gpu::dynamic_state>>(dynamicState)];
		}

		::D3D12_PRIMITIVE_TOPOLOGY get_d3d12_primitive_topology() const
		{
			return this->d3d12_primitive_topology;
		}

		::UINT get_viewport_count() const
		{
			return static_cast<::UINT>(this->viewports.size());
		}

		::D3D12_VIEWPORT const * get_viewports() const
		{
			return this->viewports.data();
		}

		::UINT get_scissor_rect_count() const
		{
			return static_cast<::UINT>(this->scissors.size());
		}

		::D3D12_RECT const * get_scissor_rects() const
		{
			return this->scissors.data();
		}

		::UINT get_vertex_binding_stride(::std::uint32_t binding) const
		{
			return this->vertex_binding_stride[binding];
		}

	private:
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::Microsoft::WRL::ComPtr<::ID3D12PipelineState> d3d12_pipeline_state;
		::D3D12_PRIMITIVE_TOPOLOGY d3d12_primitive_topology;
		::std::vector<::D3D12_VIEWPORT> viewports;
		::std::vector<::D3D12_RECT> scissors;
		::std::vector<::UINT> vertex_binding_stride;
		bool dynamic_states[9];
	};
}