#pragma once

#include "gpu/fence.h"

namespace framework::gpu::d3d12
{
	class fence final : public ::framework::gpu::fence
	{
	public:
		fence(::framework::gpu::d3d12::device * device, ::framework::gpu::fence_create_info const * create_info, ::framework::gpu::result & result);

		~fence();

		::UINT64 get_fence_value() const { return this->next_fence_value; }

		void reset() { ++this->next_fence_value; }

		::ID3D12Fence * get_d3d12_fence() const { return this->d3d12_fence; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::ID3D12Fence * d3d12_fence;
		::UINT64 next_fence_value;
	};
}