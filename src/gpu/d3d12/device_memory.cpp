#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::device_memory::device_memory(::framework::gpu::d3d12::device * device, ::framework::gpu::memory_allocate_info const * allocate_info, ::framework::gpu::result & result) :
	d3d12_heap(nullptr),
	d3d12_committed_resource(nullptr),
	size(allocate_info->allocation_size)
{
	::HRESULT hresult;

	this->is_dedicated = allocate_info->memory_dedicated_allocate_info.image || allocate_info->memory_dedicated_allocate_info.buffer;

	::D3D12_HEAP_PROPERTIES d3d12_heap_properties = device->get_d3d12_device()->GetCustomHeapProperties(0, ::framework::gpu::d3d12::memory_types[allocate_info->memory_type_index].Type);
	//HeapProperties.Type = Type;
	//HeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	//HeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	//HeapProperties.CreationNodeMask = 1;
	//HeapProperties.VisibleNodeMask = 1;

	if (allocate_info->memory_dedicated_allocate_info.image)
	{
		this->size = static_cast<::framework::gpu::d3d12::image *>(allocate_info->memory_dedicated_allocate_info.image)->get_d3d12_resource_desc().Width;
		hresult = device->get_d3d12_device()->CreateCommittedResource(
			&d3d12_heap_properties,
			::D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
			&static_cast<::framework::gpu::d3d12::image *>(allocate_info->memory_dedicated_allocate_info.image)->get_d3d12_resource_desc(),
			static_cast<::framework::gpu::d3d12::image *>(allocate_info->memory_dedicated_allocate_info.image)->get_initial_state(),
			static_cast<::framework::gpu::d3d12::image *>(allocate_info->memory_dedicated_allocate_info.image)->get_optimized_clear_value(),
			IID_PPV_ARGS(&this->d3d12_committed_resource)
		);
	}
	else if (allocate_info->memory_dedicated_allocate_info.buffer)
	{
		this->size = static_cast<::framework::gpu::d3d12::buffer *>(allocate_info->memory_dedicated_allocate_info.buffer)->get_d3d12_resource_desc().Width;
		hresult = device->get_d3d12_device()->CreateCommittedResource(
			&d3d12_heap_properties,
			::D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE,
			&static_cast<::framework::gpu::d3d12::buffer *>(allocate_info->memory_dedicated_allocate_info.buffer)->get_d3d12_resource_desc(),
			static_cast<::framework::gpu::d3d12::buffer *>(allocate_info->memory_dedicated_allocate_info.buffer)->get_initial_state(),
			nullptr,
			IID_PPV_ARGS(&this->d3d12_committed_resource)
		);
	}
	else
	{
		::D3D12_HEAP_DESC d3d12_heap_desc;
		d3d12_heap_desc.SizeInBytes = allocate_info->allocation_size;
		d3d12_heap_desc.Properties = d3d12_heap_properties;
		d3d12_heap_desc.Alignment = allocate_info->allocation_alignment;
		d3d12_heap_desc.Flags = ::framework::gpu::d3d12::memory_types[allocate_info->memory_type_index].flags;

		hresult = device->get_d3d12_device()->CreateHeap(&d3d12_heap_desc, IID_PPV_ARGS(&this->d3d12_heap));
	}

	result = ::framework::gpu::d3d12::get_result(hresult);
}

::framework::gpu::d3d12::device_memory::~device_memory()
{
	if (this->d3d12_heap)
		this->d3d12_heap->Release();
	if (this->d3d12_committed_resource)
		this->d3d12_committed_resource->Release();
}