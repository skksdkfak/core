#define NOMINMAX
#include "gpu/d3d12/core.hpp"
#include "platform/win32/window.hpp"
#include <cstring>

::framework::gpu::d3d12::physical_device::physical_device(::framework::gpu::d3d12::instance * instance, ::framework::gpu::allocation_callbacks const & allocator, ::IDXGIAdapter1 * dxgi_adapter_1) :
	allocator(allocator),
	dxgi_adapter_1(dxgi_adapter_1),
	instance(instance)
{
}

::framework::gpu::d3d12::physical_device::~physical_device()
{
	this->dxgi_adapter_1->Release();
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::create_device)(::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, class ::framework::gpu::device ** device)
{
	::framework::gpu::result result;

	*device = new ::framework::gpu::d3d12::device(this, create_info, allocator ? *allocator : this->allocator, result);

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::enumerate_device_extension_properties)(char const * layer_name, ::std::uint32_t * property_count, ::framework::gpu::extension_properties * properties)
{
	if (properties)
	{
		::std::strncpy(properties[0].extension_name, ::framework::gpu::hlsl_shader_extension_name, ::framework::gpu::max_extension_name_size);
		properties[0].spec_version = 1;
	}
	else
	{
		*property_count = 1;
	}

	return ::framework::gpu::result::success;
}

void ::framework::gpu::d3d12::physical_device::get_features(::framework::gpu::physical_device_features * features)
{
	features->depth_stencil_view_combine_all_access_types = false;
	features->clear_color_image_requires_views = true;
	features->any_image_initial_layout = true;
	features->shader_visible_host_descriptors = false;
	features->query_pool_host_commands = false;
	features->geometry_shader = true;
	features->dynamic_state_depth_bias = false;
	features->texture_compression_etc_2 = false/*todo*/;
	features->texture_compression_astc_ldr = false/*todo*/;
	features->texture_compression_bc = true;
	features->texture_compression_bc_unaligned = false/*todo: D3D12_FEATURE_DATA_D3D12_OPTIONS8::UnalignedBlockTexturesSupported*/;
	features->buffer_device_address = false/*todo*/;
	features->buffer_device_address_capture_replay = false/*todo*/;
	features->buffer_device_address_multi_device = false/*todo*/;
	features->ray_tracing_pipeline = false/*todo*/;
	features->ray_tracing_pipeline_shader_group_handle_capture_replay = false;
	features->ray_tracing_pipeline_shader_group_handle_capture_replay_mixed = false;
	features->ray_tracing_pipeline_trace_rays_indirect = false/*todo*/;
	features->ray_traversal_primitive_culling = false/*todo*/;
	features->acceleration_structure = false/*todo*/;
	features->acceleration_structure_capture_replay = false/*todo*/;
	features->acceleration_structure_indirect_build = false/*todo*/;
	features->acceleration_structure_host_commands = false/*todo*/;
	features->descriptor_binding_acceleration_structure_update_after_bind = false/*todo*/;
	features->ray_query = false/*todo*/;
	features->cooperative_matrix = false;
	features->cooperative_matrix_robust_buffer_access = false;
	features->shader_buffer_float32_atomics = false;
	features->shader_buffer_float32_atomic_add = false;
	features->shader_buffer_float64_atomics = false;
	features->shader_buffer_float64_atomic_add = false;
	features->shader_shared_float32_atomics = false;
	features->shader_shared_float32_atomic_add = false;
	features->shader_shared_float64_atomics = false;
	features->shader_shared_float64_atomic_add = false;
	features->shader_image_float32_atomics = false;
	features->shader_image_float32_atomic_add = false;
	features->sparse_image_float32_atomics = false;
	features->sparse_image_float32_atomic_add = false;
	features->shader_buffer_float16_atomics = false;
	features->shader_buffer_float16_atomic_add = false;
	features->shader_buffer_float16_atomic_min_max =false;
	features->shader_buffer_float32_atomic_min_max =false;
	features->shader_buffer_float64_atomic_min_max =false;
	features->shader_shared_float16_atomics = false;
	features->shader_shared_float16_atomic_add = false;
	features->shader_shared_float16_atomic_min_max = false;
	features->shader_shared_float32_atomic_min_max = false;
	features->shader_shared_float64_atomic_min_max = false;
	features->shader_image_float32_atomic_min_max = false;
	features->sparse_image_float32_atomic_min_max = false;
	features->subgroup_size_control = true;
}

void ::framework::gpu::d3d12::physical_device::get_properties(::framework::gpu::physical_device_properties * properties)
{
	::DXGI_ADAPTER_DESC1 dxgi_adapter_desc_1;
	this->dxgi_adapter_1->GetDesc1(&dxgi_adapter_desc_1);

	// todo
	//::D3D12_FEATURE_DATA_D3D12_OPTIONS1 d3d12_feature_data_d3d12_options1;
	//this->instance->get_dxgi_factory()->CheckFeatureSupport(::D3D12_FEATURE::D3D12_FEATURE_D3D12_OPTIONS1, &d3d12_feature_data_d3d12_options1, sizeof(d3d12_feature_data_d3d12_options1)));

	properties->device_type = ::framework::gpu::physical_device_type::other;
	::std::wcstombs(properties->device_name, dxgi_adapter_desc_1.Description, sizeof(::framework::gpu::physical_device_properties::device_name));
	properties->min_subgroup_size = 32;
	properties->max_subgroup_size = 32;
	properties->max_compute_workgroup_subgroups = 32;
	properties->required_subgroup_size_stages = ::framework::gpu::shader_stage_flags::all;
	properties->limits.max_geometry_shader_invocations = D3D12_GS_MAX_INSTANCE_COUNT;
	properties->limits.max_compute_shared_memory_size = D3D12_CS_TGSM_REGISTER_COUNT * 4;
	properties->limits.max_compute_workgroup_count[0] = D3D12_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION;
	properties->limits.max_compute_workgroup_count[1] = D3D12_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION;
	properties->limits.max_compute_workgroup_count[2] = D3D12_CS_DISPATCH_MAX_THREAD_GROUPS_PER_DIMENSION;
	properties->limits.max_compute_workgroup_invocations = D3D12_CS_THREAD_GROUP_MAX_THREADS_PER_GROUP;
	properties->limits.max_compute_workgroup_size[0] = D3D12_CS_THREAD_GROUP_MAX_X;
	properties->limits.max_compute_workgroup_size[1] = D3D12_CS_THREAD_GROUP_MAX_Y;
	properties->limits.max_compute_workgroup_size[2] = D3D12_CS_THREAD_GROUP_MAX_Z;
	properties->limits.min_texel_buffer_offset_alignment = D3D12_RAW_UAV_SRV_BYTE_ALIGNMENT;
	properties->limits.min_uniform_buffer_offset_alignment = D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT;
	properties->limits.min_storage_buffer_offset_alignment = D3D12_RAW_UAV_SRV_BYTE_ALIGNMENT;
	properties->limits.timestamp_period = 1;
	properties->limits.optimal_buffer_copy_offset_alignment = D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT;
	properties->limits.optimal_buffer_copy_row_pitch_alignment = D3D12_TEXTURE_DATA_PITCH_ALIGNMENT;
	properties->limits.viewport_y_up = true;
	properties->ray_tracing_pipeline_properties.shader_group_handle_size = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
	properties->ray_tracing_pipeline_properties.max_ray_recursion_depth = D3D12_RAYTRACING_MAX_DECLARABLE_TRACE_RECURSION_DEPTH;
	properties->ray_tracing_pipeline_properties.max_shader_group_stride = D3D12_RAYTRACING_MAX_SHADER_RECORD_STRIDE;
	properties->ray_tracing_pipeline_properties.shader_group_base_alignment = D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT;
	properties->ray_tracing_pipeline_properties.shader_group_handle_capture_replay_size = 0;
	properties->ray_tracing_pipeline_properties.max_ray_dispatch_invocation_count = D3D12_RAYTRACING_MAX_RAY_GENERATION_SHADER_THREADS;
	properties->ray_tracing_pipeline_properties.shader_group_handle_alignment = D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT;
	properties->ray_tracing_pipeline_properties.max_ray_hit_attribute_size = D3D12_RAYTRACING_MAX_ATTRIBUTE_SIZE_IN_BYTES;
	properties->acceleration_structure_properties.max_geometry_count = D3D12_RAYTRACING_MAX_GEOMETRIES_PER_BOTTOM_LEVEL_ACCELERATION_STRUCTURE;
	properties->acceleration_structure_properties.max_instance_count = D3D12_RAYTRACING_MAX_INSTANCES_PER_TOP_LEVEL_ACCELERATION_STRUCTURE;
	properties->acceleration_structure_properties.max_primitive_count = D3D12_RAYTRACING_MAX_PRIMITIVES_PER_BOTTOM_LEVEL_ACCELERATION_STRUCTURE;
	properties->acceleration_structure_properties.max_per_stage_descriptor_acceleration_structures = 0/*todo*/;
	properties->acceleration_structure_properties.max_per_stage_descriptor_update_after_bind_acceleration_structures = 0/*todo*/;
	properties->acceleration_structure_properties.max_descriptor_set_acceleration_structures = 0/*todo*/;
	properties->acceleration_structure_properties.max_descriptor_set_update_after_bind_acceleration_structures = 0/*todo*/;
	properties->acceleration_structure_properties.min_acceleration_structure_scratch_offset_alignment = 0/*todo*/;
}

void ::framework::gpu::d3d12::physical_device::get_queue_family_properties(::std::uint32_t * queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties)
{
	if (queue_family_properties)
	{
		for (::std::uint32_t i = 0; i < *queue_family_property_count; i++)
		{
			::std::memcpy(queue_family_properties, ::framework::gpu::d3d12::queue_family_properties, sizeof(::framework::gpu::queue_family_properties) * (*queue_family_property_count));
		}
	}
	else
	{
		*queue_family_property_count = 3;
	}
}

void ::framework::gpu::d3d12::physical_device::get_memory_properties(::framework::gpu::physical_device_memory_properties * physical_device_memory_properties)
{
	physical_device_memory_properties->memory_type_count = static_cast<::std::uint32_t>(::std::size(::framework::gpu::d3d12::memory_types));
	for (::std::uint32_t i = 0; i < physical_device_memory_properties->memory_type_count; i++)
	{
		physical_device_memory_properties->memory_types[i].property_flags = ::framework::gpu::d3d12::get_memory_property_flags(::framework::gpu::d3d12::memory_types[i].Type);
		physical_device_memory_properties->memory_types[i].heap_index = 0;
	}
	physical_device_memory_properties->memory_heap_count = 1;
	physical_device_memory_properties->memory_heaps[0].size = UINT64_MAX;
	physical_device_memory_properties->memory_heaps[0].flags = ::framework::gpu::memory_heap_flags::device_local_bit;
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::get_surface_support)(::std::uint32_t queue_family_index, ::framework::gpu::surface * surface, ::framework::gpu::bool32_t * supported)
{
	*supported = !!(::framework::gpu::d3d12::queue_family_properties[queue_family_index].queue_flags & ::framework::gpu::queue_flags::graphics_bit);

	return ::framework::gpu::result::success;
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::get_surface_capabilities)(::framework::gpu::surface * surface, ::framework::gpu::surface_capabilities * surface_capabilities)
{
	surface_capabilities->min_image_count = 1;
	surface_capabilities->max_image_count = DXGI_MAX_SWAP_CHAIN_BUFFERS;
	surface_capabilities->current_extent.width = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_width();
	surface_capabilities->current_extent.height = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_height();
	surface_capabilities->min_image_extent.width = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_width();
	surface_capabilities->min_image_extent.height = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_height();
	surface_capabilities->max_image_extent.width = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_width();
	surface_capabilities->max_image_extent.height = static_cast<::framework::gpu::d3d12::surface *>(surface)->get_window()->get_height();
	surface_capabilities->max_image_array_layers = 1;
	surface_capabilities->supported_transforms = ::framework::gpu::surface_transform_flags::identity_bit |
		::framework::gpu::surface_transform_flags::rotate_90_bit |
		::framework::gpu::surface_transform_flags::rotate_180_bit |
		::framework::gpu::surface_transform_flags::rotate_270_bit |
		::framework::gpu::surface_transform_flags::inherit_bit;
	surface_capabilities->current_transform = ::framework::gpu::surface_transform_flags::identity_bit;
	surface_capabilities->supported_composite_alpha = ::framework::gpu::composite_alpha_flags::opaque_bit;
	surface_capabilities->supported_usage_flags =
		::framework::gpu::image_usage_flags::transfer_src_bit |
		::framework::gpu::image_usage_flags::transfer_dst_bit |
		::framework::gpu::image_usage_flags::sampled_bit |
		::framework::gpu::image_usage_flags::storage_bit |
		::framework::gpu::image_usage_flags::color_attachment_bit |
		::framework::gpu::image_usage_flags::input_attachment_bit;

	return ::framework::gpu::result::success;
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::get_surface_formats)(::framework::gpu::surface * surface, ::std::uint32_t * surface_format_count, ::framework::gpu::surface_format * surface_formats)
{
	::framework::gpu::result tmp_result = ::framework::gpu::result::success;

	if (surface_formats)
	{
		for (::std::uint32_t i = 0; i < ::std::min(*surface_format_count, 1u); i++)
		{
			surface_formats[i].format = ::framework::gpu::format::b8g8r8a8_unorm;
			surface_formats[i].color_space = ::framework::gpu::color_space::srgb_nonlinear;
		}
	}
	else
	{
		*surface_format_count = 1u;
	}

	return tmp_result;
}

::framework::gpu::result(::framework::gpu::d3d12::physical_device::get_cooperative_matrix_properties)(::std::uint32_t * property_count, ::framework::gpu::cooperative_matrix_properties * properties)
{
	::framework::gpu::result result;

	try
	{
		if (properties)
		{
			result = ::framework::gpu::result::error_feature_not_present;
		}
		else
		{
			*property_count = 0;
			result = ::framework::gpu::result::error_feature_not_present;
		}
	}
	catch (::framework::gpu::common::result_exception & exception)
	{
		result = exception.result;
	}

	return result;
}
