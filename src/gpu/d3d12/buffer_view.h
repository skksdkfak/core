#pragma once

#include "gpu/buffer_view.h"

namespace framework::gpu::d3d12
{
	class buffer_view final : public ::framework::gpu::buffer_view
	{
	public:
		buffer_view(::framework::gpu::d3d12::device * device, ::framework::gpu::buffer_view_create_info const * create_info, ::framework::gpu::result & result);

		::D3D12_SHADER_RESOURCE_VIEW_DESC const & get_d3d12_shader_resource_view_desc() const { return this->d3d12_shader_resource_view_desc; }

		::D3D12_UNORDERED_ACCESS_VIEW_DESC const & get_d3d12_unordered_access_view_desc() const { return this->d3d12_unordered_access_view_desc; }

		::framework::gpu::d3d12::buffer * get_buffer() const { return this->buffer; }

	private:
		friend class ::framework::gpu::d3d12::device;

		::D3D12_SHADER_RESOURCE_VIEW_DESC d3d12_shader_resource_view_desc;
		::D3D12_UNORDERED_ACCESS_VIEW_DESC d3d12_unordered_access_view_desc;
		::framework::gpu::d3d12::buffer * buffer;
	};
}