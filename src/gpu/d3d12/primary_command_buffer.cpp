#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::primary_command_buffer::primary_command_buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::common::unique_ptr<::Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList7>[]> && d3d12_graphics_command_lists, ::ID3D12CommandAllocator * d3d12_command_allocator, ::std::uint32_t id) :
	::framework::gpu::d3d12::command_buffer(device, allocate_info, d3d12_graphics_command_lists[0].Get(), d3d12_command_allocator, id),
	command_subbuffer_count(allocate_info->subbuffer_count),
	command_list_index(0),
	render_pass_begin_info(),
	subpass_index(0)
{
	this->d3d12_graphics_command_lists = ::std::move(d3d12_graphics_command_lists);
}

::framework::gpu::d3d12::primary_command_buffer::~primary_command_buffer()
{
	this->d3d12_graphics_command_list = nullptr;
}

::framework::gpu::result(::framework::gpu::d3d12::primary_command_buffer::begin_command_buffer)(::framework::gpu::command_buffer_begin_info const * begin_info)
{
	::framework::gpu::result result;

	this->subpass_index = 0;
	this->command_list_index = 0;
	this->d3d12_graphics_command_list = this->d3d12_graphics_command_lists[0].Get();
	this->command_list_batch.clear();

	result = ::framework::gpu::d3d12::command_buffer::begin_command_buffer(begin_info);

	return result;
}

::framework::gpu::result(::framework::gpu::d3d12::primary_command_buffer::end_command_buffer)()
{
	::HRESULT hresult;

	hresult = this->d3d12_graphics_command_list->Close();

	this->command_list_batch.push_back(this->d3d12_graphics_command_list);

	return ::framework::gpu::d3d12::get_result(hresult);
}

void ::framework::gpu::d3d12::primary_command_buffer::begin_render_pass(::framework::gpu::render_pass_begin_info const * render_pass_begin, ::framework::gpu::subpass_contents contents)
{
	this->render_pass_begin_info.render_pass_state = render_pass_begin->render_pass_state;

	this->subpass_index = 0;
	this->subpass(contents);
}

void ::framework::gpu::d3d12::primary_command_buffer::next_subpass(::framework::gpu::subpass_contents contents)
{
	this->subpass_index++;
	this->subpass(contents);
}

void ::framework::gpu::d3d12::primary_command_buffer::end_render_pass()
{
	if (this->opened_render_pass)
	{
		this->d3d12_graphics_command_list->EndRenderPass();
		this->opened_render_pass = false;
	}

	auto const & barriers = static_cast<::framework::gpu::d3d12::render_pass_state const *>(this->render_pass_begin_info.render_pass_state)->get_frame_buffer()->get_end_render_pass_barriers();
	if (!barriers.empty())
		this->d3d12_graphics_command_list->ResourceBarrier(static_cast<::UINT>(barriers.size()), barriers.data());
}

void ::framework::gpu::d3d12::primary_command_buffer::execute_commands(::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers)
{
	for (::std::uint32_t i = 0; i < command_buffer_count; i++)
	{
		this->command_list_batch.push_back(static_cast<::framework::gpu::d3d12::command_buffer *>(command_buffers[i])->get_d3d12_graphics_command_list());
	}
}

void ::framework::gpu::d3d12::primary_command_buffer::subpass(::framework::gpu::subpass_contents contents)
{
	if (this->opened_render_pass)
	{
		this->d3d12_graphics_command_list->EndRenderPass();
		this->opened_render_pass = false;
	}

	auto const & barriers = static_cast<::framework::gpu::d3d12::render_pass_state const *>(this->render_pass_begin_info.render_pass_state)->get_frame_buffer()->get_subpass_barriers(this->subpass_index);

	if (!barriers.empty())
		this->d3d12_graphics_command_list->ResourceBarrier(static_cast<::UINT>(barriers.size()), barriers.data());

	if (contents == ::framework::gpu::subpass_contents::inline_)
	{
		auto const & subpass_info = static_cast<::framework::gpu::d3d12::render_pass_state const *>(this->render_pass_begin_info.render_pass_state)->get_subpass_info(this->subpass_index);
		this->d3d12_graphics_command_list->BeginRenderPass(subpass_info.num_render_targets, subpass_info.render_targets.get(), subpass_info.depth_stencil.get(), ::D3D12_RENDER_PASS_FLAGS::D3D12_RENDER_PASS_FLAG_NONE);
		this->opened_render_pass = true;
	}
	else if (contents == ::framework::gpu::subpass_contents::secondary_command_buffers)
	{
		this->d3d12_graphics_command_list->Close();
		this->command_list_batch.push_back(this->d3d12_graphics_command_list);

		this->command_list_index++;
		this->d3d12_graphics_command_list = this->d3d12_graphics_command_lists[this->command_list_index].Get();
		this->d3d12_graphics_command_list->Reset(this->d3d12_command_allocator, nullptr);
	}
}
