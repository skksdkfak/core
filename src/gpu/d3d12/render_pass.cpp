#include "gpu/d3d12/core.hpp"
#include <vector>

::framework::gpu::d3d12::render_pass::render_pass(::framework::gpu::d3d12::device * device, struct ::framework::gpu::render_pass_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator),
	attachment_count(create_info->attachment_count),
	subpass_count(create_info->subpass_count)
{
	this->attachment_memory_barriers = ::framework::gpu::common::make_unique<::std::vector<::framework::gpu::d3d12::render_pass::attachment_memory_barrier>[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->subpass_count + 1);
	this->attachment_infos = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass::attachment_info[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->attachment_count);
	this->subpass_infos = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass::subpass_info[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->subpass_count);

	struct attachment_preprocess_info
	{
		bool load_op_clear;
		bool used;
		bool depth_stencil;
		bool stencil;
		::std::uint32_t first_subpass;
		::std::uint32_t last_subpass;
		::D3D12_RESOURCE_STATES state;
	};
	::std::vector<struct attachment_preprocess_info> attachment_preprocess_info(create_info->attachment_count);

	for (::std::uint32_t i = 0; i < create_info->attachment_count; i++)
	{
		attachment_preprocess_info[i] =
		{
			.load_op_clear = create_info->attachments[i].load_op == ::framework::gpu::attachment_load_op::clear,
			.used = false,
			.depth_stencil = ::framework::gpu::d3d12::IsDepthStencilFormat(create_info->attachments[i].format),
			.stencil = ::framework::gpu::d3d12::has_stencil_component(create_info->attachments[i].format),
			.first_subpass = ::framework::gpu::subpass_external,
			.last_subpass = ::framework::gpu::subpass_external,
			.state = ::framework::gpu::d3d12::get_d3d12_resource_states(create_info->attachments[i].initial_layout)
		};
		this->attachment_infos[i].format = create_info->attachments[i].format;
	}

	for (::std::uint32_t subpass = 0; subpass < create_info->subpass_count; subpass++)
	{
		auto const & preprocess_attachment = [&](::framework::gpu::attachment_reference const & attachmentReference) -> void
		{
			if (attachment_preprocess_info[attachmentReference.attachment].first_subpass == ::framework::gpu::subpass_external)
				attachment_preprocess_info[attachmentReference.attachment].first_subpass = subpass;
			attachment_preprocess_info[attachmentReference.attachment].last_subpass = subpass;
			::D3D12_RESOURCE_STATES state_before = attachment_preprocess_info[attachmentReference.attachment].state;
			::D3D12_RESOURCE_STATES state_after = ::framework::gpu::d3d12::get_d3d12_resource_states(attachmentReference.layout);
			if (state_before != state_after)
			{
				this->attachment_memory_barriers[subpass].emplace_back(::framework::gpu::d3d12::render_pass::attachment_memory_barrier{ state_before, state_after, attachmentReference.attachment });
				attachment_preprocess_info[attachmentReference.attachment].state = state_after;
			}

			if (attachment_preprocess_info[attachmentReference.attachment].load_op_clear && !attachment_preprocess_info[attachmentReference.attachment].used)
			{
				attachment_preprocess_info[attachmentReference.attachment].used = true;
			}
		};

		for (::std::uint32_t j = 0; j < create_info->subpasses[subpass].input_attachment_count; j++)
		{
			preprocess_attachment(create_info->subpasses[subpass].input_attachments[j]);
		}
		for (::std::uint32_t j = 0; j < create_info->subpasses[subpass].color_attachment_count; j++)
		{
			preprocess_attachment(create_info->subpasses[subpass].color_attachments[j]);
		}

		if (create_info->subpasses[subpass].depth_stencil_attachment)
		{
			preprocess_attachment(*create_info->subpasses[subpass].depth_stencil_attachment);
		}
	}

	for (::std::uint32_t subpass = 0; subpass < create_info->subpass_count; subpass++)
	{
		if (create_info->subpasses[subpass].color_attachment_count)
		{
			this->subpass_infos[subpass].render_targets = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass::render_target_desc[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->subpasses[subpass].color_attachment_count);

			for (::std::uint32_t j = 0; j < create_info->subpasses[subpass].color_attachment_count; j++)
			{
				::std::uint32_t const attachment = create_info->subpasses[subpass].color_attachments[j].attachment;

				this->subpass_infos[subpass].render_targets[j].attachment = attachment;
				this->subpass_infos[subpass].render_targets[j].beginning_access_type = subpass == attachment_preprocess_info[attachment].first_subpass ?
					::framework::gpu::d3d12::get_d3d12_render_pass_beginning_access_type(create_info->attachments[attachment].load_op) :
					::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_PRESERVE;
				this->subpass_infos[subpass].render_targets[j].ending_access_type = subpass == attachment_preprocess_info[attachment].last_subpass ?
					::framework::gpu::d3d12::get_d3d12_render_pass_ending_access_type(create_info->attachments[attachment].store_op) :
					::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_PRESERVE;
			}
		}

		if (create_info->subpasses[subpass].depth_stencil_attachment)
		{
			this->subpass_infos[subpass].depth_stencil = ::framework::gpu::common::make_unique<::framework::gpu::d3d12::render_pass::depth_stencil_desc>(this->allocator, ::framework::gpu::system_allocation_scope::object);

			::std::uint32_t const attachment = create_info->subpasses[subpass].depth_stencil_attachment->attachment;

			this->subpass_infos[subpass].depth_stencil->attachment = *create_info->subpasses[subpass].depth_stencil_attachment;
			this->subpass_infos[subpass].depth_stencil->depth_beginning_access_type = subpass == attachment_preprocess_info[attachment].first_subpass ?
				::framework::gpu::d3d12::get_d3d12_render_pass_beginning_access_type(create_info->attachments[attachment].load_op) :
				::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_PRESERVE;
			this->subpass_infos[subpass].depth_stencil->depth_ending_access_type = subpass == attachment_preprocess_info[attachment].last_subpass ?
				::framework::gpu::d3d12::get_d3d12_render_pass_ending_access_type(create_info->attachments[attachment].store_op) :
				::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_PRESERVE;

			if (attachment_preprocess_info[attachment].stencil)
			{
				this->subpass_infos[subpass].depth_stencil->stencil_beginning_access_type = subpass == attachment_preprocess_info[attachment].first_subpass ?
					::framework::gpu::d3d12::get_d3d12_render_pass_beginning_access_type(create_info->attachments[attachment].stencil_load_op) :
					::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_PRESERVE;
				this->subpass_infos[subpass].depth_stencil->stencil_ending_access_type = subpass == attachment_preprocess_info[attachment].last_subpass ?
					::framework::gpu::d3d12::get_d3d12_render_pass_ending_access_type(create_info->attachments[attachment].stencil_store_op) :
					::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_PRESERVE;
			}
			else
			{
				this->subpass_infos[subpass].depth_stencil->stencil_beginning_access_type = ::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_NO_ACCESS;
				this->subpass_infos[subpass].depth_stencil->stencil_ending_access_type = ::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_NO_ACCESS;
			}
		}

		this->subpass_infos[subpass].num_render_targets = create_info->subpasses[subpass].color_attachment_count;
	}

	for (::std::uint32_t i = 0; i < create_info->attachment_count; i++)
	{
		::D3D12_RESOURCE_STATES state_before = attachment_preprocess_info[i].state;
		::D3D12_RESOURCE_STATES state_after = ::framework::gpu::d3d12::get_d3d12_resource_states(create_info->attachments[i].final_layout);
		if (state_before != state_after)
		{
			this->attachment_memory_barriers[create_info->subpass_count].emplace_back(::framework::gpu::d3d12::render_pass::attachment_memory_barrier{ state_before, state_after, i });
		}
	}

	result = ::framework::gpu::result::success;
}