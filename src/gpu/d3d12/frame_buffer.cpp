#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::frame_buffer::frame_buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::frame_buffer_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result) :
	allocator(allocator)
{
	::std::uint32_t const subpass_count = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_count();
	this->m_barriers.resize(subpass_count + 1);
	this->attachment_count = create_info->attachment_count;
	this->attachments = ::framework::gpu::common::make_unique<::framework::gpu::attachment_view_and_descriptor[]>(this->allocator, ::framework::gpu::system_allocation_scope::object, create_info->attachment_count);
	::std::memcpy(this->attachments.get(), create_info->attachments, sizeof(::framework::gpu::attachment_view_and_descriptor) * create_info->attachment_count);

	for (::std::uint32_t subpass = 0; subpass < subpass_count; ++subpass)
	{
		auto const & subpass_barriers = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_barriers(subpass);
		for (auto const & subpass_barrier : subpass_barriers)
		{
			::D3D12_RESOURCE_STATES const & state_before = subpass_barrier.state_before;
			::D3D12_RESOURCE_STATES const & state_after = subpass_barrier.state_after;

			if (state_before != state_after)
			{
				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.Transition.pResource = static_cast<::framework::gpu::d3d12::image_view const *>(create_info->attachments[subpass_barrier.attachment].view)->get_image()->get_d3d12_resource();
				d3d12_resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
				d3d12_resource_barrier.Transition.StateBefore = state_before;
				d3d12_resource_barrier.Transition.StateAfter = state_after;

				this->m_barriers[subpass].push_back(d3d12_resource_barrier);
			}
			else if (state_after & ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS)
			{
				::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
				d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_UAV;
				d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
				d3d12_resource_barrier.UAV.pResource = static_cast<::framework::gpu::d3d12::image_view const *>(create_info->attachments[subpass_barrier.attachment].view)->get_image()->get_d3d12_resource();

				this->m_barriers[subpass].push_back(d3d12_resource_barrier);
			}
		}

		auto const & subpass_info = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_subpass_info(subpass);
	}

	auto const & end_render_pass_barriers = static_cast<::framework::gpu::d3d12::render_pass const *>(create_info->render_pass)->get_end_render_pass_barriers();
	for (auto const & subpass_barrier : end_render_pass_barriers)
	{
		::D3D12_RESOURCE_STATES const & state_before = subpass_barrier.state_before;
		::D3D12_RESOURCE_STATES const & state_after = subpass_barrier.state_after;
		if (state_before != state_after)
		{
			::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
			d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
			d3d12_resource_barrier.Transition.pResource = static_cast<::framework::gpu::d3d12::image_view const *>(create_info->attachments[subpass_barrier.attachment].view)->get_image()->get_d3d12_resource();
			d3d12_resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			d3d12_resource_barrier.Transition.StateBefore = state_before;
			d3d12_resource_barrier.Transition.StateAfter = state_after;

			this->m_barriers.back().push_back(d3d12_resource_barrier);
		}
		else if (state_after & ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS)
		{
			::D3D12_RESOURCE_BARRIER d3d12_resource_barrier;
			d3d12_resource_barrier.Type = ::D3D12_RESOURCE_BARRIER_TYPE::D3D12_RESOURCE_BARRIER_TYPE_UAV;
			d3d12_resource_barrier.Flags = ::D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
			d3d12_resource_barrier.UAV.pResource = static_cast<::framework::gpu::d3d12::image_view const *>(create_info->attachments[subpass_barrier.attachment].view)->get_image()->get_d3d12_resource();

			this->m_barriers.back().push_back(d3d12_resource_barrier);
		}
	}

	result = ::framework::gpu::result::success;
}