#pragma once

#include "gpu/descriptor_set_layout.h"

namespace framework::gpu::d3d12
{
	class descriptor_set_layout final : public ::framework::gpu::descriptor_set_layout
	{
	public:
		struct descriptor_range
		{
			::UINT offset_from_start;
			::D3D12_DESCRIPTOR_RANGE_FLAGS flags;
		};

		struct combined_image_sampler
		{
			::UINT offset_from_start_sampler;
			::UINT offset_from_start_image;
			::D3D12_DESCRIPTOR_RANGE_FLAGS flags;
		};

		struct root_parameter
		{
			::UINT offset_from_start;
			::D3D12_ROOT_DESCRIPTOR_FLAGS flags;
		};

		struct binding
		{
			::UINT descriptor_count;
			::std::uint32_t d3d12_shader_visibility_bits;
			::std::uint32_t base_shader_register;
			::std::uint32_t register_space;
			::framework::gpu::descriptor_type descriptor_type;
			::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::sampler * []> immutable_samplers;
			union
			{
				::framework::gpu::d3d12::descriptor_set_layout::combined_image_sampler combined_image_sampler;
				::framework::gpu::d3d12::descriptor_set_layout::descriptor_range descriptor_range;
				::framework::gpu::d3d12::descriptor_set_layout::root_parameter root_parameter;
			};
		};

		descriptor_set_layout(::framework::gpu::d3d12::device * device, ::framework::gpu::descriptor_set_layout_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		::framework::gpu::d3d12::descriptor_set_layout::binding const & get_binding(::std::uint32_t binding) const
		{
			return this->descriptor_bindings[binding];
		}

		::UINT get_descriptor_count(::D3D12_DESCRIPTOR_HEAP_TYPE d3d12_descriptor_heap_type) const { return this->descriptor_count[d3d12_descriptor_heap_type]; }

		::std::uint32_t get_dynamic_descriptor_count() const { return this->dynamic_descriptor_count; }

		::framework::gpu::descriptor_set_layout_create_flags get_flags() const { return this->flags; }

	private:
		friend class ::framework::gpu::d3d12::pipeline_layout;
		friend class ::framework::gpu::d3d12::device;

		::framework::gpu::allocation_callbacks const allocator;
		::std::uint32_t binding_count;
		::framework::gpu::common::unique_ptr<::framework::gpu::d3d12::descriptor_set_layout::binding[]> descriptor_bindings;
		::UINT descriptor_count[::D3D12_DESCRIPTOR_HEAP_TYPE::D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES];
		::std::uint32_t dynamic_descriptor_count;
		::framework::gpu::descriptor_set_layout_create_flags flags;
		::std::uint32_t immutable_sampler_count;
		::std::uint32_t d3d12_descriptor_range_count[2][8];
	};
}