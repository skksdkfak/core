#pragma once

#include "command_buffer.h"

namespace framework::gpu::d3d12
{
	class primary_command_buffer final : public ::framework::gpu::d3d12::command_buffer
	{
	public:
		primary_command_buffer(class ::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::common::unique_ptr<::Microsoft::WRL::ComPtr<::ID3D12GraphicsCommandList7>[]> && d3d12_graphics_command_lists, ::ID3D12CommandAllocator * d3d12_command_allocator, ::std::uint32_t id);

		~primary_command_buffer();

		::framework::gpu::result begin_command_buffer(::framework::gpu::command_buffer_begin_info const * begin_info) override;

		::framework::gpu::result end_command_buffer() override;

		void begin_render_pass(::framework::gpu::render_pass_begin_info const * render_pass_begin, ::framework::gpu::subpass_contents contents) override;

		void next_subpass(::framework::gpu::subpass_contents contents) override;

		void end_render_pass() override;

		void execute_commands(::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers) override;

		void subpass(::framework::gpu::subpass_contents contents);

		::std::size_t get_command_list_batch_size() const { return this->command_list_batch.size(); }

		::ID3D12CommandList * const * get_command_list_batch() const { return this->command_list_batch.data(); }

	private:
		::framework::gpu::common::unique_ptr<::Microsoft::WRL::ComPtr<::ID3D12GraphicsCommandList7>[]> d3d12_graphics_command_lists;
		::std::vector<::ID3D12CommandList *> command_list_batch;
		::std::uint32_t command_list_index;
		::std::uint32_t command_subbuffer_count;
		::framework::gpu::render_pass_begin_info render_pass_begin_info;
		::std::uint32_t subpass_index;
	};
}