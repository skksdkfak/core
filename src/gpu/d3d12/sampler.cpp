#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::sampler::sampler(::framework::gpu::d3d12::device * device, ::framework::gpu::sampler_create_info const * create_info, ::framework::gpu::result & result)
{
	::D3D12_FILTER d3d12_filter;
	if (create_info->compare_enable)
	{
		if (create_info->anisotropy_enable)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_ANISOTROPIC;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	}
	else
	{
		if (create_info->anisotropy_enable)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_ANISOTROPIC;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_MAG_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_MAG_POINT_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::nearest && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_POINT_MAG_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_LINEAR_MAG_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::nearest && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::nearest)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_MAG_LINEAR_MIP_POINT;
		else if (create_info->min_filter == ::framework::gpu::filter::linear && create_info->mag_filter == ::framework::gpu::filter::linear && create_info->mip_map_mode == ::framework::gpu::sampler_mipmap_mode::linear)
			d3d12_filter = ::D3D12_FILTER::D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	}

	auto get_d3d12_texture_address_mode = [](::framework::gpu::sampler_address_mode sampler_address_mode) -> ::D3D12_TEXTURE_ADDRESS_MODE
	{
		switch (sampler_address_mode)
		{
		case ::framework::gpu::sampler_address_mode::repeat:
			return ::D3D12_TEXTURE_ADDRESS_MODE::D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			break;
		case ::framework::gpu::sampler_address_mode::mirrored_repeat:
			return ::D3D12_TEXTURE_ADDRESS_MODE::D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
			break;
		case ::framework::gpu::sampler_address_mode::clamp_to_edge:
			return ::D3D12_TEXTURE_ADDRESS_MODE::D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
			break;
		case ::framework::gpu::sampler_address_mode::clamp_to_border:
			return ::D3D12_TEXTURE_ADDRESS_MODE::D3D12_TEXTURE_ADDRESS_MODE_BORDER;
			break;
		case ::framework::gpu::sampler_address_mode::mirror_clamp_to_edge:
			return ::D3D12_TEXTURE_ADDRESS_MODE::D3D12_TEXTURE_ADDRESS_MODE_MIRROR_ONCE;
			break;
		}
	};

	::D3D12_COMPARISON_FUNC d3d12_comparison_func;
	switch (create_info->compare_op)
	{
	case ::framework::gpu::compare_op::never:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_NEVER;
		break;
	case ::framework::gpu::compare_op::less:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_LESS;
		break;
	case ::framework::gpu::compare_op::equal:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_EQUAL;
		break;
	case ::framework::gpu::compare_op::less_or_equal:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_LESS_EQUAL;
		break;
	case ::framework::gpu::compare_op::greater:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_GREATER;
		break;
	case ::framework::gpu::compare_op::not_equal:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_NOT_EQUAL;
		break;
	case ::framework::gpu::compare_op::greater_or_equal:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_GREATER_EQUAL;
		break;
	case ::framework::gpu::compare_op::always:
		d3d12_comparison_func = ::D3D12_COMPARISON_FUNC::D3D12_COMPARISON_FUNC_ALWAYS;
		break;
	}

	::FLOAT border_color[4];
	switch (create_info->border_color)
	{
	case ::framework::gpu::border_color::float_transparent_black:
		border_color[0] = 0.0f;
		border_color[1] = 0.0f;
		border_color[2] = 0.0f;
		border_color[3] = 0.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		break;
	case ::framework::gpu::border_color::int_transparent_black:
		border_color[0] = 0.0f;
		border_color[1] = 0.0f;
		border_color[2] = 0.0f;
		border_color[3] = 0.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
		break;
	case ::framework::gpu::border_color::float_opaque_black:
		border_color[0] = 0.0f;
		border_color[1] = 0.0f;
		border_color[2] = 0.0f;
		border_color[3] = 1.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK;
		break;
	case ::framework::gpu::border_color::int_opaque_black:
		border_color[0] = 0.0f;
		border_color[1] = 0.0f;
		border_color[2] = 0.0f;
		border_color[3] = 1.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK;
		break;
	case ::framework::gpu::border_color::float_opaque_white:
		border_color[0] = 1.0f;
		border_color[1] = 1.0f;
		border_color[2] = 1.0f;
		border_color[3] = 1.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		break;
	case ::framework::gpu::border_color::int_opaque_white:
		border_color[0] = 1.0f;
		border_color[1] = 1.0f;
		border_color[2] = 1.0f;
		border_color[3] = 1.0f;
		this->d3d12_static_border_color = ::D3D12_STATIC_BORDER_COLOR::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		break;
	}

	this->d3d12_sampler_desc.Filter = d3d12_filter;
	this->d3d12_sampler_desc.AddressU = get_d3d12_texture_address_mode(create_info->address_mode_u);
	this->d3d12_sampler_desc.AddressV = get_d3d12_texture_address_mode(create_info->address_mode_v);
	this->d3d12_sampler_desc.AddressW = get_d3d12_texture_address_mode(create_info->address_mode_w);
	this->d3d12_sampler_desc.MipLODBias = create_info->mip_lod_bias;
	this->d3d12_sampler_desc.MaxAnisotropy = static_cast<::UINT>(create_info->max_anisotropy);
	this->d3d12_sampler_desc.ComparisonFunc = d3d12_comparison_func;
	this->d3d12_sampler_desc.BorderColor[0] = border_color[0];
	this->d3d12_sampler_desc.BorderColor[1] = border_color[1];
	this->d3d12_sampler_desc.BorderColor[2] = border_color[2];
	this->d3d12_sampler_desc.BorderColor[3] = border_color[3];
	this->d3d12_sampler_desc.MinLOD = create_info->min_lod;
	this->d3d12_sampler_desc.MaxLOD = create_info->max_lod;

	result = ::framework::gpu::result::success;
}

::framework::gpu::d3d12::sampler::~sampler()
{
}