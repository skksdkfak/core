#pragma once

#include "gpu/command_pool.h"
#include <vector>
#include <queue>
#include <unordered_set>

namespace framework::gpu::d3d12
{
	class command_pool final : public ::framework::gpu::command_pool
	{
	public:
		command_pool(::framework::gpu::d3d12::device * device, ::framework::gpu::command_pool_create_info const * create_info, ::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::result & result);

		~command_pool();

		::framework::gpu::result allocate_command_buffers(::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::framework::gpu::command_buffer ** command_buffers) noexcept;

		void free_command_buffers(::framework::gpu::d3d12::device * device, ::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers) noexcept;

		::framework::gpu::result reset(::framework::gpu::command_pool_reset_flags flags);

		void trim(::framework::gpu::command_pool_trim_flags flags);

	private:
		friend class ::framework::gpu::d3d12::device;

		struct allocation
		{
			::framework::gpu::d3d12::command_buffer * command_buffer;
			::ID3D12CommandAllocator * d3d12_command_allocator;
		};

		::framework::gpu::allocation_callbacks const allocator;
		::ID3D12CommandAllocator * d3d12_command_allocator;
		::D3D12_COMMAND_LIST_TYPE d3d12_command_list_type;
		bool reset_command_buffer;
		::std::vector<struct ::framework::gpu::d3d12::command_pool::allocation> allocations;
		::std::uint32_t allocated_command_buffer_count;
	};
}