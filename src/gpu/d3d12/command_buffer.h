#pragma once

#include "gpu/command_buffer.h"

namespace framework::gpu::d3d12
{
	class command_pool;

	class command_buffer : public ::framework::gpu::command_buffer
	{
	public:
		command_buffer(::framework::gpu::d3d12::device * device, ::framework::gpu::command_buffer_allocate_info const * allocate_info, ::ID3D12GraphicsCommandList7 * d3d12_graphics_command_list, ::ID3D12CommandAllocator * d3d12_command_allocator, ::std::uint32_t id);

		~command_buffer();

		::framework::gpu::result begin_command_buffer(::framework::gpu::command_buffer_begin_info const * begin_info) override;

		::framework::gpu::result end_command_buffer() override;

		void push_constants(::framework::gpu::pipeline_layout * layout, ::framework::gpu::shader_stage_flags stage_flags, ::std::uint32_t range, ::std::uint32_t offset, ::std::uint32_t size, void const * values) override;

		void begin_render_pass(::framework::gpu::render_pass_begin_info const * render_pass_begin, ::framework::gpu::subpass_contents contents) override;

		void next_subpass(::framework::gpu::subpass_contents contents) override;

		void end_render_pass() override;

		void execute_commands(::std::uint32_t command_buffer_count, ::framework::gpu::command_buffer * const * command_buffers) override;

		void bind_pipeline(::framework::gpu::pipeline_bind_point pipeline_bind_point, const ::framework::gpu::pipeline * pipeline) override;

		void set_viewport(::std::uint32_t first_viewport, ::std::uint32_t viewport_count, const ::framework::gpu::viewport * viewports) override;

		void set_scissor(::std::uint32_t first_scissor, ::std::uint32_t scissor_count, const ::framework::gpu::rect_2d * scissors) override;

		void bind_descriptor_pool(::framework::gpu::descriptor_pool * descriptor_pool) override;

		void bind_descriptor_sets(::framework::gpu::pipeline_bind_point pipeline_bind_point, ::framework::gpu::pipeline_layout * layout, ::std::uint32_t first_set, ::std::uint32_t descriptor_set_count, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t dynamic_offset_count, ::std::uint32_t const * dynamic_offsets) override;

		void bind_index_buffer(const ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset, ::framework::gpu::index_type index_type) override;

		void bind_vertex_buffers(::std::uint32_t first_binding, ::std::uint32_t binding_count, ::framework::gpu::buffer * const * buffers, ::framework::gpu::device_size const * offsets) override;

		void draw(::std::uint32_t vertex_count, ::std::uint32_t instance_count, ::std::uint32_t first_vertex, ::std::uint32_t first_instance) override;

		void draw_indexed(::std::uint32_t index_count, ::std::uint32_t instance_count, ::std::uint32_t first_index, ::std::int32_t vertex_offset, ::std::uint32_t first_instance) override;

		void draw_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride) override;

		void draw_indexed_indirect(::framework::gpu::buffer const * buffer, ::framework::gpu::device_size offset, ::std::uint32_t draw_count, ::std::uint32_t stride) override;

		void dispatch(::std::uint32_t group_count_x, ::std::uint32_t group_count_y, ::std::uint32_t group_count_z) override;

		void dispatch_indirect(::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset) override;

		void copy_buffer(::framework::gpu::buffer * src_buffer, ::framework::gpu::buffer * dst_buffer, ::std::uint32_t region_count, const ::framework::gpu::buffer_copy * regions) override;

		void copy_image(::framework::gpu::image * src_image, ::framework::gpu::image_layout_flags src_image_layout, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, ::framework::gpu::image_copy const * regions) override;

		void copy_buffer_to_image(::framework::gpu::buffer * src_buffer, ::framework::gpu::image * dst_image, ::framework::gpu::image_layout_flags dst_image_layout, ::std::uint32_t region_count, const ::framework::gpu::buffer_image_copy * regions) override;

		void copy_image_to_buffer(::framework::gpu::copy_image_to_buffer_info const * copy_image_to_buffer_info) override;

		void update_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size data_size, void const * data) override;

		void fill_buffer(::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size size, ::std::uint32_t data, ::framework::gpu::device_size dst_view_offset, ::framework::gpu::device_descriptor_handle view_device_handle_in_current_pool, ::framework::gpu::host_descriptor_handle view_host_handle) override;

		void clear_color_image(::framework::gpu::image * image, ::framework::gpu::image_layout_flags image_layout, ::framework::gpu::clear_color_value const * color, ::std::uint32_t range_count, ::framework::gpu::image_subresource_range const * ranges, ::framework::gpu::host_descriptor_handle image_view_descriptor_handle) override;

		void pipeline_barrier(::framework::gpu::dependency_info const * dependency_info) override;

		void begin_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query, ::framework::gpu::query_control_flags flags) override;

		void end_query(::framework::gpu::query_pool * query_pool, ::std::uint32_t query) override;

		void reset_query_pool(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count) override;

		void write_timestamp(::framework::gpu::pipeline_stage_flags pipeline_stage, ::framework::gpu::query_pool * query_pool, ::std::uint32_t query) override;

		void copy_query_pool_results(::framework::gpu::query_pool * query_pool, ::std::uint32_t first_query, ::std::uint32_t query_count, ::framework::gpu::buffer * dst_buffer, ::framework::gpu::device_size dst_offset, ::framework::gpu::device_size stride, ::framework::gpu::query_result_flags flags) override;

		void build_acceleration_structures(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos) override;

		void build_acceleration_structures_indirect(::std::uint32_t info_count, ::framework::gpu::acceleration_structure_build_geometry_info const * infos, ::framework::gpu::device_address * indirect_device_addresses, ::std::uint32_t const * indirect_strides, ::std::uint32_t const * const * max_primitive_counts) override;

		void copy_acceleration_structure(::framework::gpu::copy_acceleration_structure_info const * info) override;

		void copy_acceleration_structure_to_memory(::framework::gpu::copy_acceleration_structure_to_memory_info const * info) override;

		void copy_memory_to_acceleration_structure(::framework::gpu::copy_memory_to_acceleration_structure_info const * info) override;

		void trace_rays(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::std::uint32_t width, ::std::uint32_t height, ::std::uint32_t depth) override;

		void trace_rays_indirect(::framework::gpu::strided_device_address_region const & raygen_shader_binding_table, ::framework::gpu::strided_device_address_region const & miss_shader_binding_table, ::framework::gpu::strided_device_address_region const & hit_shader_binding_table, ::framework::gpu::strided_device_address_region const & callable_shader_binding_table, ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset) override;

		void set_ray_tracing_pipeline_stack_size(::std::uint32_t pipeline_stack_size) override;

		void subpass(::framework::gpu::subpass_contents contents);

		::ID3D12GraphicsCommandList7 * get_d3d12_graphics_command_list() const { return this->d3d12_graphics_command_list; }

		::ID3D12CommandAllocator * get_d3d12_command_allocator() const { return this->d3d12_command_allocator; }

	protected:
		friend class ::framework::gpu::d3d12::command_pool;

		void commit_rasterizer_state() const;

		::std::uint32_t id;
		::framework::gpu::d3d12::device const * device;
		::ID3D12GraphicsCommandList7 * d3d12_graphics_command_list;
		::ID3D12CommandAllocator * d3d12_command_allocator;
		::std::vector<::D3D12_VIEWPORT> dynamic_viewports;
		::std::vector<::D3D12_RECT> dynamic_scissors;
		::framework::gpu::pipeline const * pipeline;
		bool opened_render_pass;
	};
}