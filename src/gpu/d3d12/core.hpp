#pragma once

#include <cstddef>
#include <type_traits>
#include <assert.h>
#include <wrl.h>
#include <comdef.h>
#include <d3d12.h>
#include <dxcapi.h>
#include <dxgi1_6.h>
#include "gpu/core.hpp"
#include "gpu/common/core.h"

#include "common.h"
#include "resource.h"
#include "instance.h"
#include "physical_device.h"
#include "device.h"
#include "queue.h"
#include "semaphore.h"
#include "primary_command_buffer.h"
#include "fence.h"
#include "device_memory.h"
#include "query_pool.h"
#include "buffer.h"
#include "image.h"
#include "buffer_view.h"
#include "image_view.h"
#include "shader_module.h"
#include "pipeline_cache.h"
#include "pipeline_layout.h"
#include "render_pass.h"
#include "graphics_pipeline.h"
#include "compute_pipeline.h"
#include "ray_tracing_pipeline.h"
#include "descriptor_set_layout.h"
#include "sampler.h"
#include "descriptor_pool.h"
#include "descriptor_set.h"
#include "frame_buffer.h"
#include "render_pass_state.h"
#include "command_pool.h"
#include "swap_chain.h"
#include "surface.h"
#include "acceleration_structure_geometry_set.h"
#include "acceleration_structure.h"

namespace framework::gpu::d3d12
{
	static_assert(sizeof(::framework::gpu::host_descriptor_handle) >= sizeof(::D3D12_CPU_DESCRIPTOR_HANDLE));
	static_assert(sizeof(::framework::gpu::device_descriptor_handle) >= sizeof(::D3D12_GPU_DESCRIPTOR_HANDLE));
	static_assert(sizeof(::framework::gpu::device_address) == sizeof(::D3D12_GPU_VIRTUAL_ADDRESS));

	enum
	{
		graphics_family_index,
		compute_family_index,
		transfer_family_index,
		queue_family_count
	};

	struct memory_type
	{
		::D3D12_HEAP_TYPE Type;
		::D3D12_HEAP_FLAGS flags;
	};

	static const ::framework::gpu::d3d12::memory_type memory_types[]
	{
		{ D3D12_HEAP_TYPE_DEFAULT, D3D12_HEAP_FLAG_ALLOW_ALL_BUFFERS_AND_TEXTURES },
		{ D3D12_HEAP_TYPE_UPLOAD, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_READBACK, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_DEFAULT, D3D12_HEAP_FLAG_ALLOW_ONLY_BUFFERS },
		{ D3D12_HEAP_TYPE_UPLOAD, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_READBACK, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_DEFAULT, D3D12_HEAP_FLAG_ALLOW_ONLY_NON_RT_DS_TEXTURES },
		{ D3D12_HEAP_TYPE_UPLOAD, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_READBACK, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_DEFAULT, D3D12_HEAP_FLAG_ALLOW_ONLY_RT_DS_TEXTURES },
		{ D3D12_HEAP_TYPE_UPLOAD, D3D12_HEAP_FLAG_NONE },
		{ D3D12_HEAP_TYPE_READBACK, D3D12_HEAP_FLAG_NONE }
	};

	extern const ::framework::gpu::queue_family_properties queue_family_properties[::framework::gpu::d3d12::queue_family_count];

	inline ::D3D12_COMMAND_LIST_TYPE get_d3d12_command_list_type(::std::uint32_t queue_family_index)
	{
		switch (queue_family_index)
		{
		case 0:
			return ::D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT;
		case 1:
			return ::D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COMPUTE;
		case 2:
			return ::D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_COPY;
		}
	}

	inline constexpr ::std::uint32_t get_d3d12_shader_visibility_bits(::framework::gpu::shader_stage_flags shader_stage_flags)
	{
		::std::uint32_t d3d12_shader_visibility_bits = 0;
		if ((shader_stage_flags & ::framework::gpu::shader_stage_flags::all_graphics) == ::framework::gpu::shader_stage_flags::all_graphics ||
			(shader_stage_flags & ::framework::gpu::shader_stage_flags::all) == ::framework::gpu::shader_stage_flags::all ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::compute_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::raygen_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::any_hit_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::closest_hit_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::miss_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::intersection_bit) ||
			!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::callable_bit))
		{
			d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_ALL;
		}
		else
		{
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::vertex_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_VERTEX;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::tessellation_control_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_HULL;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_DOMAIN;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::geometry_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_GEOMETRY;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::fragment_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_PIXEL;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::task_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_AMPLIFICATION;
			if (!!(shader_stage_flags & ::framework::gpu::shader_stage_flags::mesh_bit))
				d3d12_shader_visibility_bits |= 1 << ::D3D12_SHADER_VISIBILITY::D3D12_SHADER_VISIBILITY_MESH;
		}

		return d3d12_shader_visibility_bits;
	}

	inline ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE get_d3d12_raytracing_acceleration_structure_type(::framework::gpu::acceleration_structure_type acceleration_structure_type)
	{
		switch (acceleration_structure_type)
		{
		case ::framework::gpu::acceleration_structure_type::top_level:
			return ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
		case ::framework::gpu::acceleration_structure_type::bottom_level:
			return ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
		}
	}

	inline ::D3D12_BUFFER_SRV_FLAGS get_d3d12_buffer_srv_flags(::framework::gpu::descriptor_buffer_info_flags descriptor_buffer_info_flags)
	{
		::D3D12_BUFFER_SRV_FLAGS d3d12_buffer_srv_flags = ::D3D12_BUFFER_SRV_FLAGS::D3D12_BUFFER_SRV_FLAG_NONE;
		if (!!(descriptor_buffer_info_flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit))
			d3d12_buffer_srv_flags |= ::D3D12_BUFFER_SRV_FLAGS::D3D12_BUFFER_SRV_FLAG_RAW;
		return d3d12_buffer_srv_flags;
	}

	inline ::D3D12_BUFFER_UAV_FLAGS get_d3d12_buffer_uav_flags(::framework::gpu::descriptor_buffer_info_flags descriptor_buffer_info_flags)
	{
		::D3D12_BUFFER_UAV_FLAGS d3d12_buffer_uav_flags = ::D3D12_BUFFER_UAV_FLAGS::D3D12_BUFFER_UAV_FLAG_NONE;
		if (!!(descriptor_buffer_info_flags & ::framework::gpu::descriptor_buffer_info_flags::raw_bit))
			d3d12_buffer_uav_flags |= ::D3D12_BUFFER_UAV_FLAGS::D3D12_BUFFER_UAV_FLAG_RAW;
		return d3d12_buffer_uav_flags;
	}

	inline ::D3D12_RAYTRACING_GEOMETRY_TYPE get_d3d12_raytracing_geometry_type(::framework::gpu::geometry_type geometry_type)
	{
		switch (geometry_type)
		{
		default:
		case ::framework::gpu::geometry_type::triangles:
			return ::D3D12_RAYTRACING_GEOMETRY_TYPE::D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
		case ::framework::gpu::geometry_type::aabbs:
			return ::D3D12_RAYTRACING_GEOMETRY_TYPE::D3D12_RAYTRACING_GEOMETRY_TYPE_PROCEDURAL_PRIMITIVE_AABBS;
		}
	}

	inline ::D3D12_ROOT_DESCRIPTOR_FLAGS get_d3d12_root_descriptor_flags(::framework::gpu::descriptor_binding_flags descriptor_binding_flags)
	{
		::D3D12_ROOT_DESCRIPTOR_FLAGS flags = ::D3D12_ROOT_DESCRIPTOR_FLAGS::D3D12_ROOT_DESCRIPTOR_FLAG_NONE;

		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_volatile))
			flags |= ::D3D12_ROOT_DESCRIPTOR_FLAGS::D3D12_ROOT_DESCRIPTOR_FLAG_DATA_VOLATILE;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_static_while_set_at_execute))
			flags |= ::D3D12_ROOT_DESCRIPTOR_FLAGS::D3D12_ROOT_DESCRIPTOR_FLAG_DATA_STATIC_WHILE_SET_AT_EXECUTE;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_static))
			flags |= ::D3D12_ROOT_DESCRIPTOR_FLAGS::D3D12_ROOT_DESCRIPTOR_FLAG_DATA_STATIC;

		return flags;
	}

	inline ::D3D12_DESCRIPTOR_RANGE_FLAGS get_d3d12_descriptor_range_flags(::framework::gpu::descriptor_binding_flags descriptor_binding_flags)
	{
		::D3D12_DESCRIPTOR_RANGE_FLAGS flags = ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_NONE;

		if (!!(descriptor_binding_flags & (::framework::gpu::descriptor_binding_flags::partially_bound_bit) | ::framework::gpu::descriptor_binding_flags::variable_descriptor_count_bit))
			flags |= ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_DESCRIPTORS_VOLATILE;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_volatile))
			flags |= ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_static_while_set_at_execute))
			flags |= ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC_WHILE_SET_AT_EXECUTE;
		if (!!(descriptor_binding_flags & ::framework::gpu::descriptor_binding_flags::data_static))
			flags |= ::D3D12_DESCRIPTOR_RANGE_FLAGS::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC;

		return flags;
	}

	inline ::D3D12_RAYTRACING_GEOMETRY_FLAGS get_d3d12_raytracing_geometry_flags(::framework::gpu::geometry_flags geometry_flags)
	{
		::D3D12_RAYTRACING_GEOMETRY_FLAGS d3d12_raytracing_geometry_flags = ::D3D12_RAYTRACING_GEOMETRY_FLAGS::D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;

		if (!!(geometry_flags & ::framework::gpu::geometry_flags::opaque_bit))
			d3d12_raytracing_geometry_flags |= ::D3D12_RAYTRACING_GEOMETRY_FLAGS::D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;
		if (!!(geometry_flags & ::framework::gpu::geometry_flags::no_duplicate_any_hit_invocation_bit))
			d3d12_raytracing_geometry_flags |= ::D3D12_RAYTRACING_GEOMETRY_FLAGS::D3D12_RAYTRACING_GEOMETRY_FLAG_NO_DUPLICATE_ANYHIT_INVOCATION;

		return d3d12_raytracing_geometry_flags;
	}

	inline ::D3D12_GPU_VIRTUAL_ADDRESS_RANGE get_d3d12_gpu_virtual_address_range(::framework::gpu::strided_device_address_region const & strided_device_address_region)
	{
		::D3D12_GPU_VIRTUAL_ADDRESS_RANGE d3d12_gpu_virtual_address_range;
		d3d12_gpu_virtual_address_range.StartAddress = strided_device_address_region.device_address;
		d3d12_gpu_virtual_address_range.SizeInBytes = strided_device_address_region.size;
		return d3d12_gpu_virtual_address_range;
	}

	inline ::D3D12_GPU_VIRTUAL_ADDRESS_RANGE_AND_STRIDE get_d3d12_gpu_virtual_address_range_and_stride(::framework::gpu::strided_device_address_region const & strided_device_address_region)
	{
		::D3D12_GPU_VIRTUAL_ADDRESS_RANGE_AND_STRIDE d3d12_gpu_virtual_address_range_and_stride;
		d3d12_gpu_virtual_address_range_and_stride.StartAddress = strided_device_address_region.device_address;
		d3d12_gpu_virtual_address_range_and_stride.SizeInBytes = strided_device_address_region.size;
		d3d12_gpu_virtual_address_range_and_stride.StrideInBytes = strided_device_address_region.stride;
		return d3d12_gpu_virtual_address_range_and_stride;
	}

	inline ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS get_d3d12_raytracing_acceleration_structure_build_flags(::framework::gpu::build_acceleration_structure_flags build_acceleration_structure_flags)
	{
		::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS d3d12_raytracing_acceleration_structure_build_flags = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_NONE;

		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::allow_update_bit))
			d3d12_raytracing_acceleration_structure_build_flags |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::allow_compaction_bit))
			d3d12_raytracing_acceleration_structure_build_flags |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_COMPACTION;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit))
			d3d12_raytracing_acceleration_structure_build_flags |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::prefer_fast_build_bit))
			d3d12_raytracing_acceleration_structure_build_flags |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_BUILD;
		if (!!(build_acceleration_structure_flags & framework::gpu::build_acceleration_structure_flags::low_memory_bit))
			d3d12_raytracing_acceleration_structure_build_flags |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_MINIMIZE_MEMORY;

		return d3d12_raytracing_acceleration_structure_build_flags;
	}

	inline ::framework::gpu::result get_result(::HRESULT hresult)
	{
		::framework::gpu::result result;
		switch (hresult)
		{
		case S_OK:
			result = ::framework::gpu::result::success;
			break;
		case DXGI_ERROR_WAIT_TIMEOUT:
		case WAIT_TIMEOUT:
			result = ::framework::gpu::result::timeout;
			break;
		case DXGI_ERROR_UNSUPPORTED:
			result = ::framework::gpu::result::error_incompatible_driver;
			break;
		case E_OUTOFMEMORY:
			result = ::framework::gpu::result::error_out_of_host_memory;
			break;
		case DXGI_ERROR_REMOTE_OUTOFMEMORY:
			result = ::framework::gpu::result::error_out_of_device_memory;
			break;
		case DXGI_ERROR_DEVICE_REMOVED:
			result = ::framework::gpu::result::error_device_lost;
			break;
		default:
			result = ::framework::gpu::result::error_initialization_failed;
			break;
		}

		return result;
	}

	inline ::framework::gpu::result throw_result(::HRESULT hresult)
	{
		::framework::gpu::result result;
		switch (hresult)
		{
		case S_OK:
			return ::framework::gpu::result::success;
		case DXGI_ERROR_WAIT_TIMEOUT:
			return ::framework::gpu::result::timeout;
		case DXGI_ERROR_UNSUPPORTED:
			result = ::framework::gpu::result::error_incompatible_driver;
			break;
		case E_OUTOFMEMORY:
			result = ::framework::gpu::result::error_out_of_host_memory;
			break;
		case DXGI_ERROR_REMOTE_OUTOFMEMORY:
			result = ::framework::gpu::result::error_out_of_device_memory;
			break;
		case DXGI_ERROR_DEVICE_REMOVED:
			result = ::framework::gpu::result::error_device_lost;
			break;
		default:
			result = ::framework::gpu::result::error_initialization_failed;
			break;
		}

		throw ::framework::gpu::common::result_exception(result);
	}

	inline ::framework::gpu::memory_property_flags get_memory_property_flags(::D3D12_HEAP_TYPE d3d12_heap_type)
	{
		switch (d3d12_heap_type)
		{
		case ::D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_DEFAULT:
			return ::framework::gpu::memory_property_flags::device_local_bit;
		case ::D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_UPLOAD:
			return ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_cached_bit;
		case ::D3D12_HEAP_TYPE::D3D12_HEAP_TYPE_READBACK:
			return ::framework::gpu::memory_property_flags::host_visible_read_bit | ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_cached_bit;
		default:
			return ::framework::gpu::memory_property_flags::device_local_bit;
		}
	}

	inline constexpr ::DXGI_FORMAT get_dxgi_format(::framework::gpu::format format)
	{
		switch (format)
		{
		case ::framework::gpu::format::undefined:
			return ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		case ::framework::gpu::format::r4g4_unorm_pack8:

		case ::framework::gpu::format::r4g4b4a4_unorm_pack16:

		case ::framework::gpu::format::b4g4r4a4_unorm_pack16:

		case ::framework::gpu::format::r5g6b5_unorm_pack16:

		case ::framework::gpu::format::b5g6r5_unorm_pack16:

		case ::framework::gpu::format::r5g5b5a1_unorm_pack16:

		case ::framework::gpu::format::b5g5r5a1_unorm_pack16:

		case ::framework::gpu::format::a1r5g5b5_unorm_pack16:

		case ::framework::gpu::format::r8_unorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R8_UNORM;
		case ::framework::gpu::format::r8_snorm:

		case ::framework::gpu::format::r8_uscaled:

		case ::framework::gpu::format::r8_sscaled:

		case ::framework::gpu::format::r8_uint:

		case ::framework::gpu::format::r8_sint:

		case ::framework::gpu::format::r8_srgb:

		case ::framework::gpu::format::r8g8_unorm:

		case ::framework::gpu::format::r8g8_snorm:

		case ::framework::gpu::format::r8g8_uscaled:

		case ::framework::gpu::format::r8g8_sscaled:

		case ::framework::gpu::format::r8g8_uint:

		case ::framework::gpu::format::r8g8_sint:

		case ::framework::gpu::format::r8g8_srgb:

		case ::framework::gpu::format::r8g8b8_unorm:

		case ::framework::gpu::format::r8g8b8_snorm:

		case ::framework::gpu::format::r8g8b8_uscaled:

		case ::framework::gpu::format::r8g8b8_sscaled:

		case ::framework::gpu::format::r8g8b8_uint:

		case ::framework::gpu::format::r8g8b8_sint:

		case ::framework::gpu::format::r8g8b8_srgb:

		case ::framework::gpu::format::b8g8r8_unorm:

		case ::framework::gpu::format::b8g8r8_snorm:

		case ::framework::gpu::format::b8g8r8_uscaled:

		case ::framework::gpu::format::b8g8r8_sscaled:

		case ::framework::gpu::format::b8g8r8_uint:

		case ::framework::gpu::format::b8g8r8_sint:

		case ::framework::gpu::format::b8g8r8_srgb:

		case ::framework::gpu::format::r8g8b8a8_unorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
		case ::framework::gpu::format::r8g8b8a8_snorm:

		case ::framework::gpu::format::r8g8b8a8_uscaled:

		case ::framework::gpu::format::r8g8b8a8_sscaled:

		case ::framework::gpu::format::r8g8b8a8_uint:

		case ::framework::gpu::format::r8g8b8a8_sint:

		case ::framework::gpu::format::r8g8b8a8_srgb:
			return ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
		case ::framework::gpu::format::b8g8r8a8_unorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM;
		case ::framework::gpu::format::b8g8r8a8_snorm:

		case ::framework::gpu::format::b8g8r8a8_uscaled:

		case ::framework::gpu::format::b8g8r8a8_sscaled:

		case ::framework::gpu::format::b8g8r8a8_uint:

		case ::framework::gpu::format::b8g8r8a8_sint:

		case ::framework::gpu::format::b8g8r8a8_srgb:

		case ::framework::gpu::format::a8b8g8r8_unorm_pack32:

		case ::framework::gpu::format::a8b8g8r8_snorm_pack32:

		case ::framework::gpu::format::a8b8g8r8_uscaled_pack32:

		case ::framework::gpu::format::a8b8g8r8_sscaled_pack32:

		case ::framework::gpu::format::a8b8g8r8_uint_pack32:

		case ::framework::gpu::format::a8b8g8r8_sint_pack32:

		case ::framework::gpu::format::a8b8g8r8_srgb_pack32:

		case ::framework::gpu::format::a2r10g10b10_unorm_pack32:

		case ::framework::gpu::format::a2r10g10b10_snorm_pack32:

		case ::framework::gpu::format::a2r10g10b10_uscaled_pack32:

		case ::framework::gpu::format::a2r10g10b10_sscaled_pack32:

		case ::framework::gpu::format::a2r10g10b10_uint_pack32:

		case ::framework::gpu::format::a2r10g10b10_sint_pack32:

		case ::framework::gpu::format::a2b10g10r10_unorm_pack32:

		case ::framework::gpu::format::a2b10g10r10_snorm_pack32:

		case ::framework::gpu::format::a2b10g10r10_uscaled_pack32:

		case ::framework::gpu::format::a2b10g10r10_sscaled_pack32:

		case ::framework::gpu::format::a2b10g10r10_uint_pack32:

		case ::framework::gpu::format::a2b10g10r10_sint_pack32:

		case ::framework::gpu::format::r16_unorm:

		case ::framework::gpu::format::r16_snorm:

		case ::framework::gpu::format::r16_uscaled:

		case ::framework::gpu::format::r16_sscaled:

		case ::framework::gpu::format::r16_uint:

		case ::framework::gpu::format::r16_sint:

		case ::framework::gpu::format::r16_sfloat:

		case ::framework::gpu::format::r16g16_unorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_UNORM;
		case ::framework::gpu::format::r16g16_snorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_SNORM;
		case ::framework::gpu::format::r16g16_uscaled:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_UINT;
		case ::framework::gpu::format::r16g16_sscaled:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_SINT;
		case ::framework::gpu::format::r16g16_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_UINT;
		case ::framework::gpu::format::r16g16_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_SINT;
		case ::framework::gpu::format::r16g16_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16_FLOAT;
		case ::framework::gpu::format::r16g16b16a16_unorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UNORM;
		case ::framework::gpu::format::r16g16b16a16_snorm:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SNORM;
		case ::framework::gpu::format::r16g16b16a16_uscaled:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UINT;
		case ::framework::gpu::format::r16g16b16a16_sscaled:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SINT;
		case ::framework::gpu::format::r16g16b16a16_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UINT;
		case ::framework::gpu::format::r16g16b16a16_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SINT;
		case ::framework::gpu::format::r16g16b16a16_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT;
		case ::framework::gpu::format::r32_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
		case ::framework::gpu::format::r32_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32_SINT;
		case ::framework::gpu::format::r32_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
		case ::framework::gpu::format::r32g32_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32_UINT;
		case ::framework::gpu::format::r32g32_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32_SINT;
		case ::framework::gpu::format::r32g32_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT;
		case ::framework::gpu::format::r32g32b32_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_UINT;
		case ::framework::gpu::format::r32g32b32_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32_SINT;
		case ::framework::gpu::format::r32g32b32_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT;
		case ::framework::gpu::format::r32g32b32a32_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_UINT;
		case ::framework::gpu::format::r32g32b32a32_sint:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_SINT;
		case ::framework::gpu::format::r32g32b32a32_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
		case ::framework::gpu::format::r64_uint:

		case ::framework::gpu::format::r64_sint:

		case ::framework::gpu::format::r64_sfloat:

		case ::framework::gpu::format::r64g64_uint:

		case ::framework::gpu::format::r64g64_sint:

		case ::framework::gpu::format::r64g64_sfloat:

		case ::framework::gpu::format::r64g64b64_uint:

		case ::framework::gpu::format::r64g64b64_sint:

		case ::framework::gpu::format::r64g64b64_sfloat:

		case ::framework::gpu::format::r64g64b64a64_uint:

		case ::framework::gpu::format::r64g64b64a64_sint:

		case ::framework::gpu::format::r64g64b64a64_sfloat:

		case ::framework::gpu::format::b10g11r11_ufloat_pack32:

		case ::framework::gpu::format::e5b9g9r9_ufloat_pack32:

		case ::framework::gpu::format::d16_unorm:

		case ::framework::gpu::format::x8_d24_unorm_pack32:

		case ::framework::gpu::format::d32_sfloat:
			return ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
		case ::framework::gpu::format::s8_uint:

		case ::framework::gpu::format::d16_unorm_s8_uint:

		case ::framework::gpu::format::d24_unorm_s8_uint:

		case ::framework::gpu::format::d32_sfloat_s8_uint:
			return ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
		case ::framework::gpu::format::bc1_rgb_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM;
		case ::framework::gpu::format::bc1_rgb_srgb_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM_SRGB;
		case ::framework::gpu::format::bc1_rgba_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM;
		case ::framework::gpu::format::bc1_rgba_srgb_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM_SRGB;
		case ::framework::gpu::format::bc2_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM;
		case ::framework::gpu::format::bc2_srgb_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM_SRGB;
		case ::framework::gpu::format::bc3_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM;
		case ::framework::gpu::format::bc3_srgb_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM_SRGB;
		case ::framework::gpu::format::bc4_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC4_UNORM;
		case ::framework::gpu::format::bc4_snorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC4_SNORM;
		case ::framework::gpu::format::bc5_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC5_UNORM;
		case ::framework::gpu::format::bc5_snorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC5_SNORM;
		case ::framework::gpu::format::bc6h_ufloat_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC6H_UF16;
		case ::framework::gpu::format::bc6h_sfloat_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC6H_SF16;
		case ::framework::gpu::format::bc7_unorm_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM;
		case ::framework::gpu::format::bc7_srgb_block:
			return ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM_SRGB;
		default:
			return ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		}
	}

	inline constexpr ::DXGI_FORMAT get_dxgi_format(::framework::gpu::index_type index_type)
	{
		switch (index_type)
		{
		case ::framework::gpu::index_type::uint8:
			return ::DXGI_FORMAT::DXGI_FORMAT_R8_UINT;
		case ::framework::gpu::index_type::uint16:
			return ::DXGI_FORMAT::DXGI_FORMAT_R16_UINT;
		case ::framework::gpu::index_type::uint32:
			return ::DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
		case ::framework::gpu::index_type::none:
			return ::DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
		}
	}

	::DXGI_FORMAT FindDepthStencilParentDXGIFormat(::DXGI_FORMAT InFormat);

	::std::uint8_t GetPlaneCount(::DXGI_FORMAT format);

	bool IsDepthStencilFormat(::framework::gpu::format format);

	::UINT calc_row_pitch(::DXGI_FORMAT dxgi_format, ::UINT width);

	inline ::D3D12_CPU_DESCRIPTOR_HANDLE get_d3d12_cpu_descriptor_handle(::framework::gpu::host_descriptor_handle host_descriptor_handle)
	{
		return *reinterpret_cast<::D3D12_CPU_DESCRIPTOR_HANDLE const *>(&host_descriptor_handle);
	}

	inline ::D3D12_GPU_DESCRIPTOR_HANDLE get_d3d12_gpu_descriptor_handle(::framework::gpu::device_descriptor_handle device_descriptor_handle)
	{
		return *reinterpret_cast<::D3D12_GPU_DESCRIPTOR_HANDLE const *>(&device_descriptor_handle);
	}

	inline constexpr ::D3D12_BARRIER_SYNC get_d3d12_barrier_sync(::framework::gpu::pipeline_stage_flags pipeline_stage_flags)
	{
		::D3D12_BARRIER_SYNC d3d12_barrier_sync = ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_NONE;

		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::top_of_pipe_bit |
			::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit |
			::framework::gpu::pipeline_stage_flags::all_commands_bit)))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_ALL;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::draw_indirect_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_EXECUTE_INDIRECT;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::vertex_input_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_INPUT_ASSEMBLER;
		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::vertex_shader_bit |
			::framework::gpu::pipeline_stage_flags::tessellation_control_shader_bit |
			::framework::gpu::pipeline_stage_flags::tessellation_evaluation_shader_bit |
			::framework::gpu::pipeline_stage_flags::geometry_shader_bit |
			::framework::gpu::pipeline_stage_flags::transform_feedback_bit)))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_VERTEX_SHADING;
		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::fragment_shader_bit |
			::framework::gpu::pipeline_stage_flags::fragment_shading_rate_attachment_bit |
			::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit)))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_PIXEL_SHADING;
		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit |
			::framework::gpu::pipeline_stage_flags::clear_depth_stencil_image_bit)))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_DEPTH_STENCIL;
		if (!!(pipeline_stage_flags & (::framework::gpu::pipeline_stage_flags::color_attachment_output_bit |
			::framework::gpu::pipeline_stage_flags::clear_color_image_bit)))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_RENDER_TARGET;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::compute_shader_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_COMPUTE_SHADING;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::transfer_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_COPY;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::all_graphics_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_DRAW;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::conditional_rendering_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_PREDICATION;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_BUILD_RAYTRACING_ACCELERATION_STRUCTURE;
		if (!!(pipeline_stage_flags & ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit))
			d3d12_barrier_sync |= ::D3D12_BARRIER_SYNC::D3D12_BARRIER_SYNC_RAYTRACING;

		return d3d12_barrier_sync;
	}

	inline constexpr ::D3D12_BARRIER_ACCESS get_d3d12_barrier_access(::framework::gpu::access_flags access_flags)
	{
		::D3D12_BARRIER_ACCESS d3d12_barrier_access = ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_COMMON;

		if (!!(access_flags & ::framework::gpu::access_flags::indirect_command_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_INDIRECT_ARGUMENT;
		if (!!(access_flags & ::framework::gpu::access_flags::index_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_INDEX_BUFFER;
		if (!!(access_flags & ::framework::gpu::access_flags::vertex_attribute_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_VERTEX_BUFFER;
		if (!!(access_flags & ::framework::gpu::access_flags::uniform_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_CONSTANT_BUFFER;
		if (!!(access_flags & (::framework::gpu::access_flags::input_attachment_read_bit |
			::framework::gpu::access_flags::shader_sampled_read_bit |
			::framework::gpu::access_flags::shader_read_bit)))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_SHADER_RESOURCE;
		if (!!(access_flags & ::framework::gpu::access_flags::shader_write_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_COMMON;
		if (!!(access_flags & (::framework::gpu::access_flags::color_attachment_read_bit |
			::framework::gpu::access_flags::clear_color_image_bit |
			::framework::gpu::access_flags::color_attachment_write_bit)))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_RENDER_TARGET;
		if (!!(access_flags & ::framework::gpu::access_flags::depth_stencil_attachment_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_DEPTH_STENCIL_READ;
		if (!!(access_flags & (::framework::gpu::access_flags::depth_stencil_attachment_write_bit |
			::framework::gpu::access_flags::clear_depth_stencil_image_bit)))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_DEPTH_STENCIL_WRITE;
		if (!!(access_flags & ::framework::gpu::access_flags::transfer_read_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_COPY_SOURCE;
		if (!!(access_flags & ::framework::gpu::access_flags::transfer_write_bit |
			access_flags & ::framework::gpu::access_flags::fill_buffer_bit))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_COPY_DEST;
		if (!!(access_flags & (::framework::gpu::access_flags::shader_storage_write_bit |
			::framework::gpu::access_flags::shader_storage_read_bit |
			::framework::gpu::access_flags::shader_write_bit |
			::framework::gpu::access_flags::fill_buffer_bit)))
			d3d12_barrier_access |= ::D3D12_BARRIER_ACCESS::D3D12_BARRIER_ACCESS_UNORDERED_ACCESS;

		return d3d12_barrier_access;
	}

	inline constexpr ::D3D12_BARRIER_LAYOUT get_d3d12_barrier_layout(::framework::gpu::image_layout_flags image_layout_flags, ::framework::gpu::queue_family_ownership queue_family_ownership)
	{
		bool const queue_family_ignored = !!(queue_family_ownership.flags & ::framework::gpu::queue_family_ownership_flags::queue_family_ignored);

		if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::undefined_bit))
			return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_UNDEFINED;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::preinitialized_bit |
			::framework::gpu::image_layout_flags::general_bit)))
			if (queue_family_ignored)
				return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMMON;
			else
				switch (queue_family_ownership.queue_family_index)
				{
				case framework::gpu::d3d12::graphics_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DIRECT_QUEUE_COMMON;
				case framework::gpu::d3d12::compute_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMPUTE_QUEUE_COMMON;
				}
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::color_attachment_optimal_bit |
			::framework::gpu::image_layout_flags::clear_color_image_general_bit |
			::framework::gpu::image_layout_flags::clear_color_image_transfer_dst_optimal_bit)))
			return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_RENDER_TARGET;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_general_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_transfer_dst_optimal_bit)))
			return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DEPTH_STENCIL_WRITE;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::depth_stencil_read_only_optimal_bit))
			return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DEPTH_STENCIL_READ;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit |
			::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit)))
			if (queue_family_ignored)
				return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_SHADER_RESOURCE;
			else
				switch (queue_family_ownership.queue_family_index)
				{
				case framework::gpu::d3d12::graphics_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DIRECT_QUEUE_SHADER_RESOURCE;
				case framework::gpu::d3d12::transfer_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMPUTE_QUEUE_SHADER_RESOURCE;
				}
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit))
			if (queue_family_ignored)
				return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_UNORDERED_ACCESS;
			else
				switch (queue_family_ownership.queue_family_index)
				{
				case framework::gpu::d3d12::graphics_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DIRECT_QUEUE_UNORDERED_ACCESS;
				case framework::gpu::d3d12::compute_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMPUTE_QUEUE_UNORDERED_ACCESS;
				}
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::transfer_src_optimal_bit))
			if (queue_family_ignored)
				return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COPY_SOURCE;
			else
				switch (queue_family_ownership.queue_family_index)
				{
				case framework::gpu::d3d12::graphics_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DIRECT_QUEUE_COPY_SOURCE;
				case framework::gpu::d3d12::compute_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMPUTE_QUEUE_COPY_SOURCE;
				}
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit))
			if (queue_family_ignored)
				return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COPY_DEST;
			else
				switch (queue_family_ownership.queue_family_index)
				{
				case framework::gpu::d3d12::graphics_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_DIRECT_QUEUE_COPY_DEST;
				case framework::gpu::d3d12::compute_family_index: return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMPUTE_QUEUE_COPY_DEST;
				}
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::present_src_bit))
			return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_PRESENT;

		return ::D3D12_BARRIER_LAYOUT::D3D12_BARRIER_LAYOUT_COMMON;
	}

	inline constexpr ::D3D12_RESOURCE_STATES get_d3d12_resource_states(::framework::gpu::buffer_state_flags buffer_state_flags)
	{
		::D3D12_RESOURCE_STATES d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COMMON;

		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::uniform_buffer_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::vertex_buffer_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::index_buffer_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_INDEX_BUFFER;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::storage_buffer_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::fragment_shader_resource_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::stream_out_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_STREAM_OUT;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::indirect_argument_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_INDIRECT_ARGUMENT;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::transfer_src_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_SOURCE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::transfer_dst_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_DEST;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::raytracing_acceleration_structure_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::shading_rate_source_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_SHADING_RATE_SOURCE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::predication_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PREDICATION;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_decode_read_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_DECODE_READ;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_decode_write_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_DECODE_WRITE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_process_read_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_PROCESS_READ;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_process_write_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_PROCESS_WRITE;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_encode_read_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_ENCODE_READ;
		if (!!(buffer_state_flags & ::framework::gpu::buffer_state_flags::video_encode_write_bit))
			d3d12_resource_states |= ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_VIDEO_ENCODE_WRITE;

		return d3d12_resource_states;
	}

	inline constexpr ::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE get_d3d12_render_pass_beginning_access_type(::framework::gpu::attachment_load_op attachment_load_op)
	{
		switch (attachment_load_op)
		{
		case framework::gpu::attachment_load_op::load:
			return ::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_PRESERVE;
		case framework::gpu::attachment_load_op::clear:
			return ::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_CLEAR;
		case framework::gpu::attachment_load_op::dont_care:
			return ::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE::D3D12_RENDER_PASS_BEGINNING_ACCESS_TYPE_DISCARD;
		}
	}

	inline constexpr ::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE get_d3d12_render_pass_ending_access_type(::framework::gpu::attachment_store_op attachment_store_op)
	{
		switch (attachment_store_op)
		{
		case framework::gpu::attachment_store_op::store:
			return ::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_PRESERVE;
		case framework::gpu::attachment_store_op::dont_care:
			return ::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE::D3D12_RENDER_PASS_ENDING_ACCESS_TYPE_DISCARD;
		}
	}

	inline constexpr ::D3D12_RESOURCE_STATES get_d3d12_resource_states(::framework::gpu::image_layout_flags image_layout_flags)
	{
		::D3D12_RESOURCE_STATES d3d12_resource_states;

		if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::color_attachment_optimal_bit |
			::framework::gpu::image_layout_flags::clear_color_image_general_bit |
			::framework::gpu::image_layout_flags::clear_color_image_transfer_dst_optimal_bit)))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_RENDER_TARGET;
		else if (!!(image_layout_flags & (::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_general_bit |
			::framework::gpu::image_layout_flags::clear_depth_stencil_image_transfer_dst_optimal_bit)))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_DEPTH_WRITE;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::depth_stencil_read_only_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_DEPTH_READ;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::transfer_src_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_SOURCE;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_DEST;
		else if (!!(image_layout_flags & ::framework::gpu::image_layout_flags::present_src_bit))
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_PRESENT;
		else
			d3d12_resource_states = ::D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COMMON;

		return d3d12_resource_states;
	};

	inline constexpr ::std::size_t bits_per_pixel(::DXGI_FORMAT dxgi_format)
	{
		switch (dxgi_format)
		{
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_SINT:
			return 128;
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32B32_SINT:
			return 96;
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G32_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32G8X24_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_Y416:
		case ::DXGI_FORMAT::DXGI_FORMAT_Y210:
		case ::DXGI_FORMAT::DXGI_FORMAT_Y216:
			return 64;
		case ::DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R11G11B10_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16G16_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R32_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R24G8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_X24_TYPELESS_G8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_B8G8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_G8R8_G8B8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8X8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8X8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_AYUV:
		case ::DXGI_FORMAT::DXGI_FORMAT_Y410:
		case ::DXGI_FORMAT::DXGI_FORMAT_YUY2:
			return 32;
		case ::DXGI_FORMAT::DXGI_FORMAT_P010:
		case ::DXGI_FORMAT::DXGI_FORMAT_P016:
			return 24;
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8G8_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_FLOAT:
		case ::DXGI_FORMAT::DXGI_FORMAT_D16_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R16_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_B5G6R5_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_B5G5R5A1_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_A8P8:
		case ::DXGI_FORMAT::DXGI_FORMAT_B4G4R4A4_UNORM:
			return 16;
		case ::DXGI_FORMAT::DXGI_FORMAT_NV12:
		case ::DXGI_FORMAT::DXGI_FORMAT_420_OPAQUE:
		case ::DXGI_FORMAT::DXGI_FORMAT_NV11:
			return 12;
		case ::DXGI_FORMAT::DXGI_FORMAT_R8_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8_UINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_R8_SINT:
		case ::DXGI_FORMAT::DXGI_FORMAT_A8_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_AI44:
		case ::DXGI_FORMAT::DXGI_FORMAT_IA44:
		case ::DXGI_FORMAT::DXGI_FORMAT_P8:
			return 8;
		case ::DXGI_FORMAT::DXGI_FORMAT_R1_UNORM:
			return 1;
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC4_SNORM:
			return 4;
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC2_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM_SRGB:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC5_SNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_UF16:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC6H_SF16:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_TYPELESS:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM:
		case ::DXGI_FORMAT::DXGI_FORMAT_BC7_UNORM_SRGB:
			return 8;
		default:
			return 0;
		}
	}

	inline constexpr ::DXGI_FORMAT get_depth_format(::DXGI_FORMAT dxgi_format)
	{
		switch (dxgi_format)
		{
		case DXGI_FORMAT_R32G8X24_TYPELESS:
		case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
			return DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS;
		case DXGI_FORMAT_R32_TYPELESS:
		case DXGI_FORMAT_D32_FLOAT:
		case DXGI_FORMAT_R32_FLOAT:
			return DXGI_FORMAT_R32_FLOAT;
		case DXGI_FORMAT_R24G8_TYPELESS:
		case DXGI_FORMAT_D24_UNORM_S8_UINT:
		case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
		case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
			return DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
		case DXGI_FORMAT_R16_TYPELESS:
		case DXGI_FORMAT_D16_UNORM:
		case DXGI_FORMAT_R16_UNORM:
			return DXGI_FORMAT_R16_UNORM;
		default:
			return dxgi_format;
		}
	}

	inline constexpr::D3D12_DSV_FLAGS get_d3d12_dsv_flags(::framework::gpu::image_view_create_flags image_view_create_flags)
	{
		::D3D12_DSV_FLAGS d3d12_dsv_flags = ::D3D12_DSV_FLAGS::D3D12_DSV_FLAG_NONE;
		if (!!(image_view_create_flags & ::framework::gpu::image_view_create_flags::depth_read_ony_bit))
			d3d12_dsv_flags |= ::D3D12_DSV_FLAGS::D3D12_DSV_FLAG_READ_ONLY_DEPTH;
		if (!!(image_view_create_flags & ::framework::gpu::image_view_create_flags::depth_read_ony_bit))
			d3d12_dsv_flags |= ::D3D12_DSV_FLAGS::D3D12_DSV_FLAG_READ_ONLY_STENCIL;
		return d3d12_dsv_flags;
	}

	inline constexpr ::D3D12_SHADER_COMPONENT_MAPPING get_d3d12_shader_component_mapping(::framework::gpu::component_swizzle component_swizzle, ::D3D12_SHADER_COMPONENT_MAPPING identity_d3d12_shader_component_mapping)
	{
		switch (component_swizzle)
		{
		case framework::gpu::component_swizzle::identity:
			return identity_d3d12_shader_component_mapping;
		case framework::gpu::component_swizzle::zero:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FORCE_VALUE_0;
		case framework::gpu::component_swizzle::one:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FORCE_VALUE_1;
		case framework::gpu::component_swizzle::r:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_0;
		case framework::gpu::component_swizzle::g:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_1;
		case framework::gpu::component_swizzle::b:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_2;
		case framework::gpu::component_swizzle::a:
			return ::D3D12_SHADER_COMPONENT_MAPPING::D3D12_SHADER_COMPONENT_MAPPING_FROM_MEMORY_COMPONENT_3;
		}
	}

	inline constexpr bool has_stencil_component(::framework::gpu::format format)
	{
		switch (format)
		{
		case ::framework::gpu::format::x8_d24_unorm_pack32:
		case ::framework::gpu::format::s8_uint:
		case ::framework::gpu::format::d16_unorm_s8_uint:
		case ::framework::gpu::format::d24_unorm_s8_uint:
		case ::framework::gpu::format::d32_sfloat_s8_uint:
			return true;
		default:
			return false;
		}
	}

	inline void * default_allocation(void * user_data, ::std::size_t size, ::std::size_t alignment, ::framework::gpu::system_allocation_scope allocation_scope)
	{
		return ::std::malloc(size);
	}

	inline void * default_reallocation(void * user_data, void * original, ::std::size_t size, ::std::size_t alignment, ::framework::gpu::system_allocation_scope allocation_scope)
	{
		return ::std::realloc(original, size);
	}

	inline void default_free(void * user_data, void * memory)
	{
		::std::free(memory);
	}
}