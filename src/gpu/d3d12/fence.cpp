#include "gpu/d3d12/core.hpp"

::framework::gpu::d3d12::fence::fence(::framework::gpu::d3d12::device * device, ::framework::gpu::fence_create_info const * create_info, ::framework::gpu::result & result) :
	next_fence_value(1)
{
	::HRESULT hresult;

	hresult = device->get_d3d12_device()->CreateFence(!!(create_info->flags & ::framework::gpu::fence_create_flags::signaled_bit) ? this->next_fence_value : 0, ::D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&this->d3d12_fence));

	result = ::framework::gpu::d3d12::get_result(hresult);
}

::framework::gpu::d3d12::fence::~fence()
{
	this->d3d12_fence->Release();
}