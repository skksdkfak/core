#pragma once

#include <memory>
#include <functional>

#define DELETE(PTR, ALLOCATOR)\
[] <typename T>(T * ptr, ::framework::gpu::allocation_callbacks const & allocator)\
{\
	if (ptr)\
	{\
		ptr->~T();\
		allocator.pfn_free(allocator.user_data, (void *)ptr);\
	}\
}(PTR, ALLOCATOR)

#define DELETE_ARRAY(PTR, SIZE, ALLOCATOR)\
[] <typename T>\
(T * ptr, ::std::size_t size, ::framework::gpu::allocation_callbacks const & allocator)\
{\
	if (ptr)\
	{\
		for (::std::size_t i = 0; i < size; i++)\
			ptr[i].~T();\
		allocator.pfn_free(allocator.user_data, (void *)ptr);\
	}\
}(PTR, SIZE, ALLOCATOR)

namespace framework::gpu::common
{
	template <class T>
	struct allocator
	{
		typedef T value_type;

		allocator() = delete;

		constexpr allocator(::framework::gpu::allocation_callbacks const & allocation_callbacks, ::framework::gpu::system_allocation_scope allocation_scope) noexcept :
			allocation_callbacks(allocation_callbacks),
			allocation_scope(allocation_scope)
		{}

		template <class U>
		constexpr allocator(::framework::gpu::common::allocator<U> const & rhs) noexcept :
			allocation_callbacks(rhs.allocation_callbacks),
			allocation_scope(rhs.allocation_scope)
		{}

		[[nodiscard]]
		T * allocate(::std::size_t n)
		{
			return static_cast<T *>(this->allocation_callbacks.pfn_allocation(this->allocation_callbacks.user_data, sizeof(T) * n, 1, this->allocation_scope));
		}

		void deallocate(T * p, ::std::size_t n) noexcept
		{
			this->allocation_callbacks.pfn_free(this->allocation_callbacks.user_data, p);
		}

		::framework::gpu::allocation_callbacks const & allocation_callbacks;
		::framework::gpu::system_allocation_scope const allocation_scope;
	};

	struct result_exception : public ::std::exception
	{
		result_exception() = delete;

		explicit result_exception(::framework::gpu::result result) : result(result) {}

		::framework::gpu::result result;
	};

	struct image_deleter
	{
		::framework::gpu::allocation_callbacks const * allocator;
		::framework::gpu::device * device;

		constexpr image_deleter() noexcept = default;

		constexpr image_deleter(::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device * device) noexcept :
			allocator(allocator),
			device(device)
		{}

		constexpr void destroy(::framework::gpu::image * image) const noexcept
		{
			if (image)
			{
				this->device->destroy_image(image, this->allocator);
			}
		}
	};

	template <class T, class DELETER>
	struct object_handle
	{
		T * object;
		DELETER deleter;

		constexpr object_handle() noexcept = default;

		constexpr object_handle(T * object, DELETER const & deleter) noexcept :
			object(object),
			deleter(deleter)
		{
		}

		constexpr ~object_handle() noexcept
		{
			this->deleter.destroy(this->object);
		}

		::framework::gpu::common::object_handle<T, DELETER> & operator=(auto && right) noexcept
		{
			if (this != ::std::addressof(right))
			{
				this->object = right.object;
				this->deleter = right.deleter;
				right.object = nullptr;
			}
			return *this;
		}

		operator T * () const noexcept
		{
			return this->object;
		}

		T * get() const noexcept
		{
			return this->object;
		}
	};

	using image_handle = ::framework::gpu::common::object_handle<::framework::gpu::image, ::framework::gpu::common::image_deleter>;

	template <class T, ::std::enable_if_t<::std::is_array_v<T> && ::std::extent_v<T> == 0, int> = 0>
	void * allocate(::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::system_allocation_scope allocation_scope, ::std::size_t size)
	{
		using Elem = ::std::remove_extent_t<T>;
		void * memory = allocator.pfn_allocation(allocator.user_data, sizeof(Elem) * size, 1, allocation_scope);
		[[unlikely]] if (memory == nullptr)
		{
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_host_memory);
		}

		return memory;
	}

	template <class T>
	struct default_delete
	{
		::framework::gpu::allocation_callbacks const * allocator;

		constexpr default_delete() noexcept = default;

		default_delete(::framework::gpu::allocation_callbacks const * allocator) noexcept :
			allocator(allocator)
		{}

		void operator()(T * ptr) const noexcept
		{
			if (ptr)
			{
				ptr->~T();
				this->allocator->pfn_free(this->allocator->user_data, ptr);
			}
		}
	};

	template <class T>
	struct default_delete<T[]>
	{
		::framework::gpu::allocation_callbacks const * allocator;
		::std::size_t size;

		constexpr default_delete() noexcept = default;

		default_delete(::framework::gpu::allocation_callbacks const * allocator, ::std::size_t size) noexcept :
			allocator(allocator),
			size(size)
		{}

		template <class Ut, ::std::enable_if_t<::std::is_convertible_v<Ut(*)[], T(*)[]>, int> = 0>
		void operator()(Ut * ptr) const noexcept
		{
			if (ptr)
			{
				for (::std::size_t i = 0; i < this->size; i++)
					ptr[i].~Ut();
				this->allocator->pfn_free(this->allocator->user_data, ptr);
			}
		}
	};

	template <typename T, typename D = ::framework::gpu::common::default_delete<T>>
	using unique_ptr = ::std::unique_ptr<T, D>;

	template <class T, class... Types, ::std::enable_if_t<!::std::is_array_v<T>, int> = 0>
	::std::unique_ptr<T, ::framework::gpu::common::default_delete<T>> make_unique(::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::system_allocation_scope allocation_scope, Types&&... args)
	{
		void * memory = allocator.pfn_allocation(allocator.user_data, sizeof(T), 1, allocation_scope);
		[[unlikely]] if (memory == nullptr)
		{
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_host_memory);
		}

		return ::std::unique_ptr<T, ::framework::gpu::common::default_delete<T>>(::new (memory) T(::std::forward<Types>(args)...), &allocator);
	}

	template <class T, ::std::enable_if_t<::std::is_array_v<T> && ::std::extent_v<T> == 0, int> = 0>
	::std::unique_ptr<T, ::framework::gpu::common::default_delete<T>> make_unique(::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::system_allocation_scope allocation_scope, ::std::size_t size)
	{
		using Elem = ::std::remove_extent_t<T>;
		void * memory = allocator.pfn_allocation(allocator.user_data, sizeof(Elem) * size, 1, allocation_scope);
		[[unlikely]] if (memory == nullptr)
		{
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_host_memory);
		}

		return ::std::unique_ptr<T, ::framework::gpu::common::default_delete<T>>(::new (memory) Elem[size], { &allocator, size });
	}

	template <class T, class... Types, ::std::enable_if_t<!::std::is_array_v<T>, int> = 0>
	::std::unique_ptr<T, ::std::function<void(T *)>> make_unique(::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::system_allocation_scope allocation_scope, ::std::function<void(T *)> deleter, Types&&... args)
	{
		void * memory = allocator.pfn_allocation(allocator.user_data, sizeof(T), 1, allocation_scope);
		[[unlikely]] if (memory == nullptr)
		{
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_host_memory);
		}

		return ::std::unique_ptr<T, ::std::function<void(T *)>>(::new (memory) T(::std::forward<Types>(args)...), deleter);
	}

	template <class T, ::std::enable_if_t<::std::is_array_v<T> && ::std::extent_v<T> == 0, int> = 0>
	::std::unique_ptr<T, ::std::function<void(::std::remove_extent_t<T> *)>> make_unique(::framework::gpu::allocation_callbacks const & allocator, ::framework::gpu::system_allocation_scope allocation_scope, ::std::function<void(::std::remove_extent_t<T> *)> deleter, ::std::size_t size)
	{
		using Elem = ::std::remove_extent_t<T>;
		void * memory = allocator.pfn_allocation(allocator.user_data, sizeof(Elem) * size, 1, allocation_scope);
		[[unlikely]] if (memory == nullptr)
		{
			throw ::framework::gpu::common::result_exception(::framework::gpu::result::error_out_of_host_memory);
		}

		return ::std::unique_ptr<T, ::std::function<void(::std::remove_extent_t<T> *)>>(::new (memory) Elem[size], deleter);
	}
}