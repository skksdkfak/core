#pragma once

#include "macros.hpp"
#include <cstddef>
#include <cstdint>

namespace framework::platform
{
	class window;
}

namespace framework::gpu
{
	class instance;
	class physical_device;
	class device;
	class queue;
	class semaphore;
	class command_buffer;
	class fence;
	class device_memory;
	class query_pool;
	class buffer;
	class image;
	class buffer_view;
	class image_view;
	class shader_module;
	class pipeline_cache;
	class pipeline_layout;
	class render_pass;
	class pipeline;
	class descriptor_set_layout;
	class sampler;
	class descriptor_pool;
	class descriptor_set;
	class frame_buffer;
	class render_pass_state;
	class command_pool;
	class swap_chain;
	class surface;
	class acceleration_structure_geometry_set;
	class acceleration_structure;
	class deferred_operation;

	typedef ::std::uint32_t flags32_t;
	typedef ::std::uint64_t flags64_t;
	typedef ::std::uint32_t bool32_t;
	typedef ::std::uint64_t device_address;
	typedef void const * host_descriptor_handle;
	typedef ::std::uint64_t device_descriptor_handle;
	typedef ::std::uint64_t device_size;
	typedef ::std::uint32_t sample_mask;

	constexpr ::framework::gpu::device_size whole_size = ~0ull;
	constexpr ::std::uint32_t subpass_external = ~0ul;
	constexpr ::std::size_t max_physical_device_name_size = 256;
	constexpr ::std::size_t uuid_size = 16;
	constexpr ::std::size_t max_memory_types = 32;
	constexpr ::std::size_t max_memory_heaps = 16;
	constexpr ::std::size_t max_extension_name_size = 256;
	constexpr ::std::uint32_t shader_unused = ~0ul;

	constexpr char const layer_standard_validation_layer_name[] = "LAYER_standard_validation";

	//instance extensions
	constexpr char const surface_extension_name[] = "surface";
	constexpr char const debug_utils_extension_name[] = "debug_utils";
	constexpr char const validation_features_extension_name[] = "validation_features";
	constexpr char const win32_surface_extension_name[] = "win32_surface";
	constexpr char const xcb_surface_extension_name[] = "xcb_surface";
	constexpr char const hlsl_shader_extension_name[] = "hlsl_shader";
	constexpr char const spirv_shader_extension_name[] = "spirv_shader";

	// device extensions
	constexpr char const cooperative_matrix_extension_name[] = "cooperative_matrix";

	enum class api_type
	{
		vulkan,
		d3d12
	};

	enum class result
	{
		success = 0,
		not_ready = 1,
		timeout = 2,
		event_set = 3,
		event_reset = 4,
		incomplete = 5,
		error_out_of_host_memory = -1,
		error_out_of_device_memory = -2,
		error_initialization_failed = -3,
		error_device_lost = -4,
		error_memory_map_failed = -5,
		error_layer_not_present = -6,
		error_extension_not_present = -7,
		error_feature_not_present = -8,
		error_incompatible_driver = -9,
		error_too_many_objects = -10,
		error_format_not_supported = -11,
		error_fragmented_pool = -12,
		error_unknown = -13,
		error_surface_lost = -1000000000,
		error_native_window_in_use = -1000000001,
		suboptimal = 1000001003,
		error_out_of_date = -1000001004,
		error_incompatible_display = -1000003001,
		error_validation_failed = -1000011001,
		error_invalid_shader = -1000012000,
		error_out_of_pool_memory = -1000069000,
		error_invalid_external_handle = -1000072003,
		error_not_permitted = -1000174001
	};

	enum class system_allocation_scope
	{
		command = 0,
		object = 1,
		cache = 2,
		device = 3,
		instance = 4,
		max_enum = 0x7fffffff
	};

	enum class format
	{
		undefined = 0,
		r4g4_unorm_pack8 = 1,
		r4g4b4a4_unorm_pack16 = 2,
		b4g4r4a4_unorm_pack16 = 3,
		r5g6b5_unorm_pack16 = 4,
		b5g6r5_unorm_pack16 = 5,
		r5g5b5a1_unorm_pack16 = 6,
		b5g5r5a1_unorm_pack16 = 7,
		a1r5g5b5_unorm_pack16 = 8,
		r8_unorm = 9,
		r8_snorm = 10,
		r8_uscaled = 11,
		r8_sscaled = 12,
		r8_uint = 13,
		r8_sint = 14,
		r8_srgb = 15,
		r8g8_unorm = 16,
		r8g8_snorm = 17,
		r8g8_uscaled = 18,
		r8g8_sscaled = 19,
		r8g8_uint = 20,
		r8g8_sint = 21,
		r8g8_srgb = 22,
		r8g8b8_unorm = 23,
		r8g8b8_snorm = 24,
		r8g8b8_uscaled = 25,
		r8g8b8_sscaled = 26,
		r8g8b8_uint = 27,
		r8g8b8_sint = 28,
		r8g8b8_srgb = 29,
		b8g8r8_unorm = 30,
		b8g8r8_snorm = 31,
		b8g8r8_uscaled = 32,
		b8g8r8_sscaled = 33,
		b8g8r8_uint = 34,
		b8g8r8_sint = 35,
		b8g8r8_srgb = 36,
		r8g8b8a8_unorm = 37,
		r8g8b8a8_snorm = 38,
		r8g8b8a8_uscaled = 39,
		r8g8b8a8_sscaled = 40,
		r8g8b8a8_uint = 41,
		r8g8b8a8_sint = 42,
		r8g8b8a8_srgb = 43,
		b8g8r8a8_unorm = 44,
		b8g8r8a8_snorm = 45,
		b8g8r8a8_uscaled = 46,
		b8g8r8a8_sscaled = 47,
		b8g8r8a8_uint = 48,
		b8g8r8a8_sint = 49,
		b8g8r8a8_srgb = 50,
		a8b8g8r8_unorm_pack32 = 51,
		a8b8g8r8_snorm_pack32 = 52,
		a8b8g8r8_uscaled_pack32 = 53,
		a8b8g8r8_sscaled_pack32 = 54,
		a8b8g8r8_uint_pack32 = 55,
		a8b8g8r8_sint_pack32 = 56,
		a8b8g8r8_srgb_pack32 = 57,
		a2r10g10b10_unorm_pack32 = 58,
		a2r10g10b10_snorm_pack32 = 59,
		a2r10g10b10_uscaled_pack32 = 60,
		a2r10g10b10_sscaled_pack32 = 61,
		a2r10g10b10_uint_pack32 = 62,
		a2r10g10b10_sint_pack32 = 63,
		a2b10g10r10_unorm_pack32 = 64,
		a2b10g10r10_snorm_pack32 = 65,
		a2b10g10r10_uscaled_pack32 = 66,
		a2b10g10r10_sscaled_pack32 = 67,
		a2b10g10r10_uint_pack32 = 68,
		a2b10g10r10_sint_pack32 = 69,
		r16_unorm = 70,
		r16_snorm = 71,
		r16_uscaled = 72,
		r16_sscaled = 73,
		r16_uint = 74,
		r16_sint = 75,
		r16_sfloat = 76,
		r16g16_unorm = 77,
		r16g16_snorm = 78,
		r16g16_uscaled = 79,
		r16g16_sscaled = 80,
		r16g16_uint = 81,
		r16g16_sint = 82,
		r16g16_sfloat = 83,
		r16g16b16_unorm = 84,
		r16g16b16_snorm = 85,
		r16g16b16_uscaled = 86,
		r16g16b16_sscaled = 87,
		r16g16b16_uint = 88,
		r16g16b16_sint = 89,
		r16g16b16_sfloat = 90,
		r16g16b16a16_unorm = 91,
		r16g16b16a16_snorm = 92,
		r16g16b16a16_uscaled = 93,
		r16g16b16a16_sscaled = 94,
		r16g16b16a16_uint = 95,
		r16g16b16a16_sint = 96,
		r16g16b16a16_sfloat = 97,
		r32_uint = 98,
		r32_sint = 99,
		r32_sfloat = 100,
		r32g32_uint = 101,
		r32g32_sint = 102,
		r32g32_sfloat = 103,
		r32g32b32_uint = 104,
		r32g32b32_sint = 105,
		r32g32b32_sfloat = 106,
		r32g32b32a32_uint = 107,
		r32g32b32a32_sint = 108,
		r32g32b32a32_sfloat = 109,
		r64_uint = 110,
		r64_sint = 111,
		r64_sfloat = 112,
		r64g64_uint = 113,
		r64g64_sint = 114,
		r64g64_sfloat = 115,
		r64g64b64_uint = 116,
		r64g64b64_sint = 117,
		r64g64b64_sfloat = 118,
		r64g64b64a64_uint = 119,
		r64g64b64a64_sint = 120,
		r64g64b64a64_sfloat = 121,
		b10g11r11_ufloat_pack32 = 122,
		e5b9g9r9_ufloat_pack32 = 123,
		d16_unorm = 124,
		x8_d24_unorm_pack32 = 125,
		d32_sfloat = 126,
		s8_uint = 127,
		d16_unorm_s8_uint = 128,
		d24_unorm_s8_uint = 129,
		d32_sfloat_s8_uint = 130,
		bc1_rgb_unorm_block = 131,
		bc1_rgb_srgb_block = 132,
		bc1_rgba_unorm_block = 133,
		bc1_rgba_srgb_block = 134,
		bc2_unorm_block = 135,
		bc2_srgb_block = 136,
		bc3_unorm_block = 137,
		bc3_srgb_block = 138,
		bc4_unorm_block = 139,
		bc4_snorm_block = 140,
		bc5_unorm_block = 141,
		bc5_snorm_block = 142,
		bc6h_ufloat_block = 143,
		bc6h_sfloat_block = 144,
		bc7_unorm_block = 145,
		bc7_srgb_block = 146,
	};

	enum class image_type
	{
		one_dimensional = 0,
		two_dimensional = 1,
		three_dimensional = 2
	};

	enum class image_tiling
	{
		optimal = 0,
		linear = 1
	};

	enum class physical_device_type
	{
		other = 0,
		integrated_gpu = 1,
		discrete_gpu = 2,
		virtual_gpu = 3,
		cpu_ = 4
	};

	enum class query_type
	{
		occlusion = 0,
		pipeline_statistics = 1,
		timestamp = 2,
		transform_feedback_stream = 1000028004,
		performance_query = 1000116000,
		acceleration_structure_compacted_size = 1000165000,
		acceleration_structure_serialization_size = 1000150000,
		max_enum = 0x7fffffff
	};

	enum class sharing_mode
	{
		exclusive = 0,
		concurrent = 1
	};

	enum class image_layout_flags : ::framework::gpu::flags32_t
	{
		undefined_bit = 0x00000001,
		preinitialized_bit = 0x00000002,
		general_bit = 0x00000004,
		color_attachment_optimal_bit = 0x00000008,
		depth_stencil_attachment_optimal_bit = 0x00000010,
		depth_stencil_read_only_optimal_bit = 0x00000020,
		non_fragment_shader_read_only_optimal_bit = 0x00000040,
		fragment_shader_read_only_optimal_bit = 0x00000080,
		shader_read_write_optimal_bit = 0x00000100,
		transfer_src_optimal_bit = 0x00000800,
		transfer_dst_optimal_bit = 0x00000200,
		present_src_bit = 0x00002000,
		clear_color_image_general_bit = 0x00004000,
		clear_color_image_transfer_dst_optimal_bit = 0x00008000,
		clear_depth_stencil_image_general_bit = 0x00010000,
		clear_depth_stencil_image_transfer_dst_optimal_bit = 0x00020000
	};
	ENUM_CLASS_FLAGS(::framework::gpu::image_layout_flags)

	enum class image_view_type
	{
		one_dimensional = 0,
		two_dimensional = 1,
		three_dimensional = 2,
		cube = 3,
		one_dimensional_array = 4,
		two_dimensional_array = 5,
		cube_array = 6
	};

	enum class component_swizzle
	{
		identity = 0,
		zero = 1,
		one = 2,
		r = 3,
		g = 4,
		b = 5,
		a = 6
	};

	enum class vertex_input_rate
	{
		vertex = 0,
		instance = 1
	};

	enum class primitive_topology
	{
		point_list = 0,
		line_list = 1,
		line_strip = 2,
		triangle_list = 3,
		triangle_strip = 4,
		triangle_fan = 5,
		line_list_with_adjacency = 6,
		line_strip_with_adjacency = 7,
		triangle_list_with_adjacency = 8,
		triangle_strip_with_adjacency = 9,
		patch_list = 10
	};

	enum class polygon_mode
	{
		fill = 0,
		line = 1,
		point = 2
	};

	enum class front_face
	{
		counter_clockwise = 0,
		clockwise = 1
	};

	enum class compare_op
	{
		never = 0,
		less = 1,
		equal = 2,
		less_or_equal = 3,
		greater = 4,
		not_equal = 5,
		greater_or_equal = 6,
		always = 7
	};

	enum class stencil_op
	{
		keep = 0,
		zero = 1,
		replace = 2,
		increment_and_clamp = 3,
		decrement_and_clamp = 4,
		invert = 5,
		increment_and_wrap = 6,
		decrement_and_wrap = 7
	};

	enum class logic_op
	{
		clear = 0,
		and_ = 1,
		and_reverse = 2,
		copy = 3,
		and_inverted = 4,
		no_op = 5,
		xor_ = 6,
		or_ = 7,
		nor = 8,
		equivalent = 9,
		invert = 10,
		or_reverse = 11,
		copy_inverted = 12,
		or_inverted = 13,
		nand = 14,
		set = 15
	};

	enum class blend_factor
	{
		zero = 0,
		one = 1,
		src_color = 2,
		one_minus_src_color = 3,
		dst_color = 4,
		one_minus_dst_color = 5,
		src_alpha = 6,
		one_minus_src_alpha = 7,
		dst_alpha = 8,
		one_minus_dst_alpha = 9,
		constant_color = 10,
		one_minus_constant_color = 11,
		constant_alpha = 12,
		one_minus_constant_alpha = 13,
		src_alpha_saturate = 14,
		src1_color = 15,
		one_minus_src1_color = 16,
		src1_alpha = 17,
		one_minus_src1_alpha = 18
	};

	enum class blend_op
	{
		add = 0,
		subtract = 1,
		reverse_subtract = 2,
		min = 3,
		max = 4,
		zero = 1000148000,
		src = 1000148001,
		dst = 1000148002,
		src_over = 1000148003,
		dst_over = 1000148004,
		src_in = 1000148005,
		dst_in = 1000148006,
		src_out = 1000148007,
		dst_out = 1000148008,
		src_atop = 1000148009,
		dst_atop = 1000148010,
		xor_ = 1000148011,
		multiply = 1000148012,
		screen = 1000148013,
		overlay = 1000148014,
		darken = 1000148015,
		lighten = 1000148016,
		colordodge = 1000148017,
		colorburn = 1000148018,
		hardlight = 1000148019,
		softlight = 1000148020,
		difference = 1000148021,
		exclusion = 1000148022,
		invert = 1000148023,
		invert_rgb = 1000148024,
		lineardodge = 1000148025,
		linearburn = 1000148026,
		vividlight = 1000148027,
		linearlight = 1000148028,
		pinlight = 1000148029,
		hardmix = 1000148030,
		hsl_hue = 1000148031,
		hsl_saturation = 1000148032,
		hsl_color = 1000148033,
		hsl_luminosity = 1000148034,
		plus = 1000148035,
		plus_clamped = 1000148036,
		plus_clamped_alpha = 1000148037,
		plus_darker = 1000148038,
		minus = 1000148039,
		minus_clamped = 1000148040,
		contrast = 1000148041,
		invert_ovg = 1000148042,
		red = 1000148043,
		green = 1000148044,
		blue = 1000148045
	};

	enum class dynamic_state
	{
		viewport = 0,
		scissor = 1,
		line_width = 2,
		depth_bias = 3,
		blend_constants = 4,
		depth_bounds = 5,
		stencil_compare_mask = 6,
		stencil_write_mask = 7,
		stencil_reference = 8,
		ray_tracing_pipeline_stack_size = 9,
		max_enum = 0x7fffffff
	};

	enum class filter
	{
		nearest = 0,
		linear = 1,
		cubic_img = 2
	};

	enum class sampler_mipmap_mode
	{
		nearest = 0,
		linear = 1
	};

	enum class sampler_address_mode
	{
		repeat = 0,
		mirrored_repeat = 1,
		clamp_to_edge = 2,
		clamp_to_border = 3,
		mirror_clamp_to_edge = 4
	};

	enum class border_color
	{
		float_transparent_black = 0,
		int_transparent_black = 1,
		float_opaque_black = 2,
		int_opaque_black = 3,
		float_opaque_white = 4,
		int_opaque_white = 5
	};

	enum class descriptor_type
	{
		sampler = 0,
		combined_image_sampler = 1,
		sampled_image = 2,
		storage_image = 3,
		uniform_texel_buffer = 4,
		storage_texel_buffer = 5,
		uniform_buffer = 6,
		sampled_buffer = 7,
		storage_buffer = 8,
		uniform_buffer_dynamic = 9,
		sampled_buffer_dynamic = 10,
		storage_buffer_dynamic = 11,
		input_attachment = 12,
		inline_uniform_block = 13,
		acceleration_structure = 14,
		color_attachment = 15,
		depth_stencil_attachment = 16
	};

	enum class attachment_load_op
	{
		load = 0,
		clear = 1,
		dont_care = 2
	};

	enum class attachment_store_op
	{
		store = 0,
		dont_care = 1,
		none
	};

	enum class pipeline_bind_point
	{
		graphics = 0,
		compute = 1,
		ray_tracing = 2
	};

	enum class command_buffer_level
	{
		primary = 0,
		secondary = 1
	};

	enum class index_type
	{
		uint8 = 0,
		uint16 = 1,
		uint32 = 2,
		none = 3
	};

	enum class subpass_contents
	{
		inline_ = 0,
		secondary_command_buffers = 1
	};

	enum class image_usage_flags
	{
		none = 0,
		transfer_src_bit = 0x00000001,
		transfer_dst_bit = 0x00000002,
		sampled_bit = 0x00000004,
		storage_bit = 0x00000008,
		color_attachment_bit = 0x00000010,
		depth_stencil_attachment_bit = 0x00000020,
		transient_attachment_bit = 0x00000040,
		input_attachment_bit = 0x00000080,
		allow_clear_bit = 0x00000100
	};
	ENUM_CLASS_FLAGS(::framework::gpu::image_usage_flags)

	enum class image_create_flags
	{
		none = 0,
		sparse_binding_bit = 0x00000001,
		sparse_residency_bit = 0x00000002,
		sparse_aliased_bit = 0x00000004,
		mutable_format_bit = 0x00000008,
		cube_compatible_bit = 0x00000010,
		alias_bit = 0x00000400,
		split_instance_bind_regions_bit = 0x00000040,
		two_dimensional_array_compatible_bit = 0x00000020,
		block_texel_view_compatible_bit = 0x00000080,
		extended_usage_bit = 0x00000100,
		protected_bit = 0x00000800,
		disjoint_bit = 0x00000200,
		corner_sampled_bit = 0x00002000,
		sample_locations_compatible_depth_bit = 0x00001000,
		subsampled_bit = 0x00004000,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::image_create_flags)

	enum class sample_count_flags : ::framework::gpu::flags32_t
	{
		sample_count_1_bit = 0x00000001,
		sample_count_2_bit = 0x00000002,
		sample_count_4_bit = 0x00000004,
		sample_count_8_bit = 0x00000008,
		sample_count_16_bit = 0x00000010,
		sample_count_32_bit = 0x00000020,
		sample_count_64_bit = 0x00000040
	};
	ENUM_CLASS_FLAGS(::framework::gpu::sample_count_flags)

	enum class queue_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		graphics_bit = 0x00000001,
		compute_bit = 0x00000002,
		transfer_bit = 0x00000004,
		sparse_binding_bit = 0x00000008
	};
	ENUM_CLASS_FLAGS(::framework::gpu::queue_flags)

	enum class queue_family_ownership_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		queue_family_ignored = 0x00000001,
		queue_family_external = 0x00000002,
		queue_family_foreign = 0x00000004
	};
	ENUM_CLASS_FLAGS(::framework::gpu::queue_family_ownership_flags)

	enum class memory_property_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		device_local_bit = 0x00000001,
		host_visible_write_bit = 0x00000002,
		host_visible_read_bit = 0x00000004,
		host_coherent_bit = 0x00000008,
		host_cached_bit = 0x00000010,
		lazily_allocated_bit = 0x00000020
	};
	ENUM_CLASS_FLAGS(::framework::gpu::memory_property_flags)

	enum class memory_heap_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		device_local_bit = 0x00000001,
		multi_instance_bit = 0x00000002
	};
	ENUM_CLASS_FLAGS(::framework::gpu::memory_heap_flags)

	enum class device_create_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::device_create_flags)

	enum class device_queue_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		protected_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::device_queue_create_flags)

	enum class memory_allocate_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		device_mask_bit = 0x00000001,
		device_address_bit = 0x00000002,
		device_address_capture_replay_bit = 0x00000004,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::memory_allocate_flags)

	enum class pipeline_stage_flags : ::framework::gpu::flags64_t
	{
		none = 0,
		top_of_pipe_bit = 0x00000001,
		draw_indirect_bit = 0x00000002,
		vertex_input_bit = 0x00000004,
		vertex_shader_bit = 0x00000008,
		tessellation_control_shader_bit = 0x00000010,
		tessellation_evaluation_shader_bit = 0x00000020,
		geometry_shader_bit = 0x00000040,
		fragment_shader_bit = 0x00000080,
		early_fragment_tests_bit = 0x00000100,
		late_fragment_tests_bit = 0x00000200,
		color_attachment_output_bit = 0x00000400,
		compute_shader_bit = 0x00000800,
		all_transfer_bit = 0x00001000,
		transfer_bit = 0x00001000,
		bottom_of_pipe_bit = 0x00002000,
		host_bit = 0x00004000,
		all_graphics_bit = 0x00008000,
		all_commands_bit = 0x00010000,
		copy_bit = 0x100000000ull,
		resolve_bit = 0x200000000ull,
		blit_bit = 0x400000000ull,
		clear_bit = 0x800000000ull,
		index_input_bit = 0x1000000000ull,
		vertex_attribute_input_bit = 0x2000000000ull,
		pre_rasterization_shaders_bit = 0x4000000000ull,
		transform_feedback_bit = 0x01000000ull,
		conditional_rendering_bit = 0x00040000ull,
		command_preprocess_bit = 0x00020000ull,
		fragment_shading_rate_attachment_bit = 0x00400000ull,
		acceleration_structure_build_bit = 0x02000000ull,
		ray_tracing_shader_bit = 0x00200000ull,
		fragment_density_process_bit = 0x00800000ull,
		task_shader_bit = 0x00080000ull,
		mesh_shader_bit = 0x00100000ull,
		fill_buffer_bit = 0x8000000000ull,
		clear_color_image_bit = 0x10000000000ull,
		clear_depth_stencil_image_bit = 0x20000000000ull
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_stage_flags)

	enum class memory_map_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::memory_map_flags)

	enum class pipeline_barrier_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_barrier_flags)

	enum class image_aspect_flags : ::framework::gpu::flags32_t
	{
		color_bit = 0x00000001,
		depth_bit = 0x00000002,
		stencil_bit = 0x00000004,
		metadata_bit = 0x00000008
	};
	ENUM_CLASS_FLAGS(::framework::gpu::image_aspect_flags)

	enum class fence_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		signaled_bit = 0x00000001
	};
	ENUM_CLASS_FLAGS(::framework::gpu::fence_create_flags)

	enum class semaphore_create_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::semaphore_create_flags)

	enum class query_pipeline_statistic_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		input_assembly_vertices_bit = 0x00000001,
		input_assembly_primitives_bit = 0x00000002,
		vertex_shader_invocations_bit = 0x00000004,
		geometry_shader_invocations_bit = 0x00000008,
		geometry_shader_primitives_bit = 0x00000010,
		clipping_invocations_bit = 0x00000020,
		clipping_primitives_bit = 0x00000040,
		fragment_shader_invocations_bit = 0x00000080,
		tessellation_control_shader_patches_bit = 0x00000100,
		tessellation_evaluation_shader_invocations_bit = 0x00000200,
		compute_shader_invocations_bit = 0x00000400,
	};
	ENUM_CLASS_FLAGS(::framework::gpu::query_pipeline_statistic_flags)

	enum class query_pool_create_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::query_pool_create_flags)

	enum class query_result_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		query_result_64_bit = 0x00000001,
		wait_bit = 0x00000002,
		with_availability_bit = 0x00000004,
		partial_bit = 0x00000008,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::query_result_flags)

	enum class semaphore_type
	{
		binary = 0,
		timeline = 1
	};

	enum class buffer_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		sparse_binding_bit = 0x00000001,
		sparse_residency_bit = 0x00000002,
		sparse_aliased_bit = 0x00000004,
		protected_bit = 0x00000008,
		device_address_capture_replay_bit = 0x00000010,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::buffer_create_flags)

	enum class buffer_usage_flags : ::framework::gpu::flags32_t
	{
		transfer_src_bit = 0x00000001,
		transfer_dst_bit = 0x00000002,
		uniform_texel_buffer_bit = 0x00000004,
		storage_texel_buffer_bit = 0x00000010,
		uniform_buffer_bit = 0x00000020,
		sampled_buffer_bit = 0x00000040,
		storage_buffer_bit = 0x00000080,
		index_buffer_bit = 0x00000100,
		vertex_buffer_bit = 0x00000200,
		indirect_buffer_bit = 0x00000400,
		shader_device_address_bit = 0x00000800,
		transform_feedback_buffer_bit = 0x00001000,
		transform_feedback_counter_buffer_bit = 0x00002000,
		conditional_rendering_bit = 0x00004000,
		acceleration_structure_build_input_read_only_bit = 0x00008000,
		acceleration_structure_storage_bit = 0x00010000,
		shader_binding_table_bit = 0x00020000,
		allow_clear_bit = 0x00040000,
		host_read_bit = 0x00080000,
		host_write_bit = 0x00100000
	};
	ENUM_CLASS_FLAGS(::framework::gpu::buffer_usage_flags)

	enum class buffer_state_flags : ::framework::gpu::flags32_t
	{
		uniform_buffer_bit = 0x00000001,
		vertex_buffer_bit = 0x00000002,
		index_buffer_bit = 0x00000004,
		storage_buffer_bit = 0x00000008,
		non_fragment_shader_resource_bit = 0x00000010,
		fragment_shader_resource_bit = 0x00000020,
		stream_out_bit = 0x00000040,
		indirect_argument_bit = 0x00000080,
		transfer_src_bit = 0x00000100,
		transfer_dst_bit = 0x00000200,
		raytracing_acceleration_structure_bit = 0x00000400,
		shading_rate_source_bit = 0x00000800,
		predication_bit = 0x00001000,
		video_decode_read_bit = 0x00002000,
		video_decode_write_bit = 0x00004000,
		video_process_read_bit = 0x00008000,
		video_process_write_bit = 0x00010000,
		video_encode_read_bit = 0x00020000,
		video_encode_write_bit = 0x00040000
	};
	ENUM_CLASS_FLAGS(::framework::gpu::buffer_state_flags)

	enum class buffer_view_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::buffer_view_create_flags)

	enum class image_view_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		fragment_density_map_dynamic_bit = 0x00000001,
		depth_read_ony_bit = 0x00000002,
		stencil_read_ony_bit = 0x00000004
	};
	ENUM_CLASS_FLAGS(::framework::gpu::image_view_create_flags)
	typedef ::framework::gpu::flags32_t shader_module_create_flags;
	typedef ::framework::gpu::flags32_t pipeline_cache_create_flags;

	enum class pipeline_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		disable_optimization_bit = 0x00000001,
		allow_derivatives_bit = 0x00000002,
		derivative_bit = 0x00000004,
		view_index_from_device_index_bit = 0x00000008,
		dispatch_base = 0x00000010,
		ray_tracing_no_null_any_hit_shaders_bit = 0x00004000,
		ray_tracing_no_null_closest_hit_shaders_bit = 0x00008000,
		ray_tracing_no_null_miss_shaders_bit = 0x00010000,
		ray_tracing_no_null_intersection_shaders_bit = 0x00020000,
		ray_tracing_skip_triangles_bit = 0x00001000,
		ray_tracing_skip_aabbs_bit = 0x00002000,
		ray_tracing_shader_group_handle_capture_replay_bit = 0x00080000,
		defer_compile_bit = 0x00000020,
		capture_statistics_bit = 0x00000040,
		capture_internal_representations_bit = 0x00000080,
		indirect_bindable_bit = 0x00040000,
		library_bit = 0x00000800,
		fail_on_pipeline_compile_required_bit = 0x00000100,
		early_return_on_failure_bit = 0x00000200,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_create_flags)

	enum class pipeline_shader_stage_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		allow_varying_subgroup_size_bit = 0x00000001,
		require_full_subgroups_bit = 0x00000002,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_shader_stage_create_flags)

	enum class shader_stage_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		vertex_bit = 0x00000001,
		tessellation_control_bit = 0x00000002,
		tessellation_evaluation_bit = 0x00000004,
		geometry_bit = 0x00000008,
		fragment_bit = 0x00000010,
		compute_bit = 0x00000020,
		all_graphics = 0x0000001f,
		all = 0x7fffffff,
		raygen_bit = 0x00000100,
		any_hit_bit = 0x00000200,
		closest_hit_bit = 0x00000400,
		miss_bit = 0x00000800,
		intersection_bit = 0x00001000,
		callable_bit = 0x00002000,
		task_bit = 0x00000040,
		mesh_bit = 0x00000080,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::shader_stage_flags)
	typedef ::framework::gpu::flags32_t pipeline_vertex_input_state_create_flags;
	typedef ::framework::gpu::flags32_t pipeline_input_assembly_state_create_flags;
	typedef ::framework::gpu::flags32_t pipeline_tessellation_state_create_flags;
	typedef ::framework::gpu::flags32_t pipeline_viewport_state_create_flags;
	typedef ::framework::gpu::flags32_t pipeline_rasterization_state_create_flags;

	enum class cull_mode_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		front_bit = 0x00000001,
		back_bit = 0x00000002,
		front_and_back = 0x00000003
	};
	ENUM_CLASS_FLAGS(::framework::gpu::cull_mode_flags)

	enum class pipeline_multisample_state_create_flags : ::framework::gpu::flags32_t
	{
		none = 0
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_multisample_state_create_flags)

	enum class color_component_flags : ::framework::gpu::flags32_t
	{
		r_bit = 0x00000001,
		g_bit = 0x00000002,
		b_bit = 0x00000004,
		a_bit = 0x00000008
	};
	ENUM_CLASS_FLAGS(::framework::gpu::color_component_flags)
	typedef ::framework::gpu::flags32_t pipeline_dynamic_state_create_flags;

	enum class pipeline_layout_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		local_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::pipeline_layout_create_flags)

	enum class descriptor_set_layout_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		push_descriptor_bit = 0x00000001,
		update_after_bind_pool_bit = 0x00000002,
		host_only_bit = 0x00000004,
		local_bit = 0x00000008,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::descriptor_set_layout_create_flags)

	enum class descriptor_binding_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		update_after_bind_bit = 0x00000001,
		update_unused_while_pending_bit = 0x00000002,
		partially_bound_bit = 0x00000004,
		variable_descriptor_count_bit = 0x00000008,
		data_volatile = 0x00000010,
		data_static_while_set_at_execute = 0x00000020,
		data_static = 0x00000040,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::descriptor_binding_flags)

	enum class descriptor_pool_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		free_descriptor_set_bit = 0x00000001,
		update_after_bind_bit = 0x00000002,
		host_only_bit = 0x00000004,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::descriptor_pool_create_flags)
		
	enum class descriptor_buffer_info_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		raw_bit = 0x00000001,
	};
	ENUM_CLASS_FLAGS(::framework::gpu::descriptor_buffer_info_flags)

	typedef ::framework::gpu::flags32_t descriptor_pool_reset_flags;

	enum class frame_buffer_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		imageless_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::frame_buffer_create_flags)

	enum class render_pass_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::render_pass_create_flags)

	enum class attachment_description_flags : ::framework::gpu::flags32_t
	{
        none = 0,
		may_alias_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::attachment_description_flags)

	enum class sampler_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		subsampled_bit = 0x00000001,
		subsampled_coarse_reconstruction_bit = 0x00000002,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::sampler_create_flags)

	enum class subpass_description_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		per_view_attributes_bit = 0x00000001,
		per_view_position_x_only_bit = 0x00000002,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::subpass_description_flags)

	enum class access_flags : ::framework::gpu::flags64_t
	{
		none = 0,
		indirect_command_read_bit = 0x00000001ULL,
		index_read_bit = 0x00000002ULL,
		vertex_attribute_read_bit = 0x00000004ULL,
		uniform_read_bit = 0x00000008ULL,
		input_attachment_read_bit = 0x00000010ULL,
		shader_read_bit = 0x00000020ULL,
		shader_write_bit = 0x00000040ULL,
		color_attachment_read_bit = 0x00000080ULL,
		color_attachment_write_bit = 0x00000100ULL,
		depth_stencil_attachment_read_bit = 0x00000200ULL,
		depth_stencil_attachment_write_bit = 0x00000400ULL,
		transfer_read_bit = 0x00000800ULL,
		transfer_write_bit = 0x00001000ULL,
		host_read_bit = 0x00002000ULL,
		host_write_bit = 0x00004000ULL,
		memory_read_bit = 0x00008000ULL,
		memory_write_bit = 0x00010000ULL,
		shader_sampled_read_bit = 0x100000000ull,
		shader_storage_read_bit = 0x200000000ull,
		shader_storage_write_bit = 0x400000000ull,
		transform_feedback_write_bit = 0x02000000ull,
		transform_feedback_counter_read_bit = 0x04000000ull,
		transform_feedback_counter_write_bit = 0x08000000ull,
		conditional_rendering_read_bit = 0x00100000ull,
		command_preprocess_read_bit = 0x00020000ull,
		command_preprocess_write_bit = 0x00040000ull,
		fragment_shading_rate_attachment_read_bit = 0x00800000ull,
		acceleration_structure_read_bit = 0x00200000ull,
		acceleration_structure_write_bit = 0x00400000ull,
		fragment_density_map_read_bit = 0x01000000ull,
		color_attachment_read_noncoherent_bit = 0x00080000ull,
		invocation_mask_read_bit = 0x8000000000ull,
		fill_buffer_bit = 0x10000000000ull,
		clear_color_image_bit = 0x20000000000ull,
		clear_depth_stencil_image_bit = 0x40000000000ull
	};
	ENUM_CLASS_FLAGS(::framework::gpu::access_flags)

	enum class dependency_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		by_region_bit = 0x00000001,
		view_local_bit = 0x00000002,
		device_group_bit = 0x00000004,
		legacy_bit = 0x00000008
	};
	ENUM_CLASS_FLAGS(::framework::gpu::dependency_flags)

	enum class command_pool_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		transient_bit = 0x00000001,
		reset_command_buffer_bit = 0x00000002
	};
	ENUM_CLASS_FLAGS(::framework::gpu::command_pool_create_flags)

	enum class command_pool_reset_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		reset_release_resources_bit = 0x00000001
	};
	ENUM_CLASS_FLAGS(::framework::gpu::command_pool_reset_flags)
	typedef ::framework::gpu::flags32_t command_pool_trim_flags;

	enum class command_buffer_usage_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		one_time_submit_bit = 0x00000001,
		render_pass_continue_bit = 0x00000002,
		render_pass_suspending_bit = 0x00000004,
		render_pass_resuming_bit = 0x00000008,
		simultaneous_use_bit = 0x00000010
	};
	ENUM_CLASS_FLAGS(::framework::gpu::command_buffer_usage_flags)

	enum class query_control_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		precise_bit = 0x00000001
	};
	ENUM_CLASS_FLAGS(::framework::gpu::query_control_flags)

	enum class surface_transform_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		identity_bit = 0x00000001,
		rotate_90_bit = 0x00000002,
		rotate_180_bit = 0x00000004,
		rotate_270_bit = 0x00000008,
		horizontal_mirror_bit = 0x00000010,
		horizontal_mirror_rotate_90_bit = 0x00000020,
		horizontal_mirror_rotate_180_bit = 0x00000040,
		horizontal_mirror_rotate_270_bit = 0x00000080,
		inherit_bit = 0x00000100
	};
	ENUM_CLASS_FLAGS(::framework::gpu::surface_transform_flags)

	enum class color_space
	{
		srgb_nonlinear = 0,
		display_p3_nonlinear = 1000104001,
		extended_srgb_linear = 1000104002,
		dci_p3_linear = 1000104003,
		dci_p3_nonlinear = 1000104004,
		bt709_linear = 1000104005,
		bt709_nonlinear = 1000104006,
		bt2020_linear = 1000104007,
		hdr10_st2084 = 1000104008,
		dolbyvision = 1000104009,
		hdr10_hlg = 1000104010,
		adobergb_linear = 1000104011,
		adobergb_nonlinear = 1000104012,
		pass_through = 1000104013,
		extended_srgb_nonlinear = 1000104014,
		max_enum = 0x7fffffff
	};

	enum class present_mode
	{
		immediate = 0,
		mailbox = 1,
		fifo = 2,
		fifo_relaxed = 3,
		shared_demand_refresh = 1000111000,
		shared_continuous_refresh = 1000111001,
		max_enum = 0x7fffffff
	};

	enum class composite_alpha_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		opaque_bit = 0x00000001,
		pre_multiplied_bit = 0x00000002,
		post_multiplied_bit = 0x00000004,
		inherit_bit = 0x00000008
	};
	ENUM_CLASS_FLAGS(::framework::gpu::composite_alpha_flags)

	enum class swap_chain_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		split_instance_bind_regions_bit = 0x00000001,
		protected_bit = 0x00000002,
		mutable_format_bit = 0x00000004,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::swap_chain_create_flags)

	enum class build_acceleration_structure_mode
	{
		build = 0,
		update = 1,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_build_type
	{
		host = 0,
		device = 1,
		host_or_device = 2,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_compatibility
	{
		compatible = 0,
		incompatible = 1,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_create_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		device_address_capture_replay_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::acceleration_structure_create_flags)

	enum class ray_tracing_shader_group_type
	{
		general = 0,
		triangles_hit_group = 1,
		procedural_hit_group = 2,
		max_enum = 0x7fffffff
	};

	enum class geometry_type
	{
		triangles = 0,
		aabbs = 1,
		instances = 2,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_type
	{
		top_level = 0,
		bottom_level = 1,
		max_enum = 0x7fffffff
	};

	enum class copy_acceleration_structure_mode
	{
		clone = 0,
		compact = 1,
		serialize = 2,
		deserialize = 3,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_memory_requirements_type
	{
		object = 0,
		build_scratch = 1,
		update_scratch = 2,
		max_enum = 0x7fffffff
	};

	enum class acceleration_structure_geometry_set_layout
	{
		array = 0,
		array_of_pointers = 1,
		max_enum = 0x7fffffff
	};

	enum class geometry_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		opaque_bit = 0x00000001,
		no_duplicate_any_hit_invocation_bit = 0x00000002
	};
	ENUM_CLASS_FLAGS(::framework::gpu::geometry_flags)

	enum class geometry_instance_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		triangle_facing_cull_disable_bit = 0x00000001,
		triangle_front_counterclockwise_bit = 0x00000002,
		force_opaque_bit = 0x00000004,
		force_no_opaque_bit = 0x00000008,
	};
	ENUM_CLASS_FLAGS(::framework::gpu::geometry_instance_flags)

	enum class build_acceleration_structure_flags : ::framework::gpu::flags32_t
	{
		allow_update_bit = 0x00000001,
		allow_compaction_bit = 0x00000002,
		prefer_fast_trace_bit = 0x00000004,
		prefer_fast_build_bit = 0x00000008,
		low_memory_bit = 0x00000010,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::build_acceleration_structure_flags)

	enum class submit_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		protected_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::submit_flags)

	enum class semaphore_wait_flags : ::framework::gpu::flags32_t
	{
		any_bit = 0x00000001,
		max_enum = 0x7fffffff
	};
	ENUM_CLASS_FLAGS(::framework::gpu::semaphore_wait_flags)
		
	enum class descriptor_handle_info_flags : ::framework::gpu::flags32_t
	{
		none = 0,
		combined_sampler = 0x00000001
	};
	ENUM_CLASS_FLAGS(::framework::gpu::descriptor_handle_info_flags)

	enum class object_type
	{
		unknown = 0,
		instance = 1,
		physical_device = 2,
		device = 3,
		queue = 4,
		semaphore = 5,
		command_buffer = 6,
		fence = 7,
		device_memory = 8,
		buffer = 9,
		image = 10,
		event = 11,
		query_pool = 12,
		buffer_view = 13,
		image_view = 14,
		shader_module = 15,
		pipeline_cache = 16,
		pipeline_layout = 17,
		render_pass = 18,
		pipeline = 19,
		descriptor_set_layout = 20,
		sampler = 21,
		descriptor_pool = 22,
		descriptor_set = 23,
		framebuffer = 24,
		command_pool = 25,
		surface = 26,
		swapchain = 27,
		debug_report_callback = 28,
		display = 29,
		display_mode = 30,
		validation_cache = 33,
		sampler_ycbcr_conversion = 1000156000,
		descriptor_update_template = 1000085000,
		cu_module_nvx = 1000029000,
		cu_function_nvx = 1000029001,
		acceleration_structure = 1000150000,
		max_enum = 0x7fffffff
	};
	
	enum class component_type
	{
		float16 = 0,
		float32 = 1,
		float64 = 2,
		sint8 = 3,
		sint16 = 4,
		sint32 = 5,
		sint64 = 6,
		uint8 = 7,
		uint16 = 8,
		uint32 = 9,
		uint64 = 10
	};

	enum class scope
	{
		device = 1,
		workgroup = 2,
		subgroup = 3,
		queue_family = 5
	};

	struct application_info
	{
		char const *	application_name;
		::std::uint32_t	application_version;
		char const *	engine_name;
		::std::uint32_t	engine_version;
		::std::uint32_t	api_version;
	};

	struct instance_create_info
	{
		::framework::gpu::application_info const *	application_info;
		::std::uint32_t								enabled_layer_count;
		char const * const *						enabled_layer_names;
		::std::uint32_t								enabled_extension_count;
		char const * const *						enabled_extension_names;
	};

	typedef void * (*allocation_function)(
		void *										user_data,
		::std::size_t								size,
		::std::size_t								alignment,
		::framework::gpu::system_allocation_scope	allocation_scope
		);

	typedef void * (*reallocation_function)(
		void *										user_data,
		void *										original,
		::std::size_t								size,
		::std::size_t								alignment,
		::framework::gpu::system_allocation_scope	allocation_scope
		);

	typedef void (*free_function)(
		void *	user_data,
		void *	memory
		);

	struct allocation_callbacks
	{
		void *									user_data;
		::framework::gpu::allocation_function	pfn_allocation;
		::framework::gpu::reallocation_function	pfn_reallocation;
		::framework::gpu::free_function			pfn_free;
	};

	struct physical_device_features
	{
		::framework::gpu::bool32_t	depth_stencil_view_combine_all_access_types;
		::framework::gpu::bool32_t	clear_color_image_requires_views;
		::framework::gpu::bool32_t	any_image_initial_layout;
		::framework::gpu::bool32_t	shader_visible_host_descriptors;
		::framework::gpu::bool32_t	query_pool_host_commands;
		//bool							robustbufferaccess;
		//bool							fulldrawindexuint32;
		//bool							imagecubearray;
		//bool							independentblend;
		::framework::gpu::bool32_t	geometry_shader;
		//bool							tessellationshader;
		//bool							samplerateshading;
		//bool							dualsrcblend;
		//bool							logic_op;
		//bool							multidrawindirect;
		//bool							drawindirectfirstinstance;
		::framework::gpu::bool32_t	dynamic_state_depth_bias;
		//bool							depthclamp;
		//bool							depth_bias_clamp;
		//bool							fillmodenonsolid;
		//bool							depthbounds;
		//bool							widelines;
		//bool							largepoints;
		//bool							alphatoone;
		//bool							multiviewport;
		//bool							sampleranisotropy;
		::framework::gpu::bool32_t	texture_compression_etc_2;
		::framework::gpu::bool32_t	texture_compression_astc_ldr;
		::framework::gpu::bool32_t	texture_compression_bc;
		::framework::gpu::bool32_t	texture_compression_bc_unaligned;
		//bool							occlusionqueryprecise;
		//bool							pipelinestatisticsquery;
		//bool							vertexpipelinestoresandatomics;
		//bool							fragmentstoresandatomics;
		//bool							shadertessellationandgeometrypointsize;
		//bool							shaderimagegatherextended;
		//bool							shaderstorageimageextendedformats;
		//bool							shaderstorageimagemultisample;
		//bool							shaderstorageimagereadwithoutformat;
		//bool							shaderstorageimagewritewithoutformat;
		//bool							shaderuniformbufferarraydynamicindexing;
		//bool							shadersampledimagearraydynamicindexing;
		//bool							shaderstoragebufferarraydynamicindexing;
		//bool							shaderstorageimagearraydynamicindexing;
		//bool							shaderclipdistance;
		//bool							shaderculldistance;
		//bool							shaderfloat64;
		//bool							shaderint64;
		//bool							shaderint16;
		//bool							shaderresourceresidency;
		//bool							shaderresourceminlod;
		//bool							sparsebinding;
		//bool							sparseresidencybuffer;
		//bool							sparseresidencyimage2d;
		//bool							sparseresidencyimage3d;
		//bool							sparseresidency2samples;
		//bool							sparseresidency4samples;
		//bool							sparseresidency8samples;
		//bool							sparseresidency16samples;
		//bool							sparseresidencyaliased;
		//bool							variablemultisamplerate;
		//bool							inheritedqueries;
		::framework::gpu::bool32_t	buffer_device_address;
		::framework::gpu::bool32_t	buffer_device_address_capture_replay;
		::framework::gpu::bool32_t	buffer_device_address_multi_device;
		::framework::gpu::bool32_t	ray_tracing_pipeline;
		::framework::gpu::bool32_t	ray_tracing_pipeline_shader_group_handle_capture_replay;
		::framework::gpu::bool32_t	ray_tracing_pipeline_shader_group_handle_capture_replay_mixed;
		::framework::gpu::bool32_t	ray_tracing_pipeline_trace_rays_indirect;
		::framework::gpu::bool32_t	ray_traversal_primitive_culling;
		::framework::gpu::bool32_t	acceleration_structure;
		::framework::gpu::bool32_t	acceleration_structure_capture_replay;
		::framework::gpu::bool32_t	acceleration_structure_indirect_build;
		::framework::gpu::bool32_t	acceleration_structure_host_commands;
		::framework::gpu::bool32_t	descriptor_binding_acceleration_structure_update_after_bind;
		::framework::gpu::bool32_t	ray_query;
		::framework::gpu::bool32_t	cooperative_matrix;
		::framework::gpu::bool32_t	cooperative_matrix_robust_buffer_access;
		::framework::gpu::bool32_t	shader_buffer_float32_atomics;
		::framework::gpu::bool32_t	shader_buffer_float32_atomic_add;
		::framework::gpu::bool32_t	shader_buffer_float64_atomics;
		::framework::gpu::bool32_t	shader_buffer_float64_atomic_add;
		::framework::gpu::bool32_t	shader_shared_float32_atomics;
		::framework::gpu::bool32_t	shader_shared_float32_atomic_add;
		::framework::gpu::bool32_t	shader_shared_float64_atomics;
		::framework::gpu::bool32_t	shader_shared_float64_atomic_add;
		::framework::gpu::bool32_t	shader_image_float32_atomics;
		::framework::gpu::bool32_t	shader_image_float32_atomic_add;
		::framework::gpu::bool32_t	sparse_image_float32_atomics;
		::framework::gpu::bool32_t	sparse_image_float32_atomic_add;
		::framework::gpu::bool32_t	shader_buffer_float16_atomics;
		::framework::gpu::bool32_t	shader_buffer_float16_atomic_add;
		::framework::gpu::bool32_t	shader_buffer_float16_atomic_min_max;
		::framework::gpu::bool32_t	shader_buffer_float32_atomic_min_max;
		::framework::gpu::bool32_t	shader_buffer_float64_atomic_min_max;
		::framework::gpu::bool32_t	shader_shared_float16_atomics;
		::framework::gpu::bool32_t	shader_shared_float16_atomic_add;
		::framework::gpu::bool32_t	shader_shared_float16_atomic_min_max;
		::framework::gpu::bool32_t	shader_shared_float32_atomic_min_max;
		::framework::gpu::bool32_t	shader_shared_float64_atomic_min_max;
		::framework::gpu::bool32_t	shader_image_float32_atomic_min_max;
		::framework::gpu::bool32_t	sparse_image_float32_atomic_min_max;
		::framework::gpu::bool32_t	subgroup_size_control;
	};

	struct extent_3d
	{
		::std::uint32_t	width;
		::std::uint32_t	height;
		::std::uint32_t	depth;
	};

	struct physical_device_limits
	{
		//::std::uint32_t              maximagedimension1d;
		//::std::uint32_t              maximagedimension2d;
		//::std::uint32_t              maximagedimension3d;
		//::std::uint32_t              maximagedimensioncube;
		//::std::uint32_t              max_image_array_layers;
		//::std::uint32_t              maxtexelbufferelements;
		//::std::uint32_t              maxuniformbufferrange;
		//::std::uint32_t              maxstoragebufferrange;
		//::std::uint32_t              maxpushconstantssize;
		//::std::uint32_t              maxmemoryallocationcount;
		//::std::uint32_t              maxsamplerallocationcount;
		//::framework::gpu::device_size          bufferimagegranularity;
		//::framework::gpu::device_size          sparseaddressspacesize;
		//::std::uint32_t              maxbounddescriptorsets;
		//::std::uint32_t              maxperstagedescriptorsamplers;
		//::std::uint32_t              maxperstagedescriptoruniformbuffers;
		//::std::uint32_t              maxperstagedescriptorstoragebuffers;
		//::std::uint32_t              maxperstagedescriptorsampledimages;
		//::std::uint32_t              maxperstagedescriptorstorageimages;
		//::std::uint32_t              maxperstagedescriptorinputattachments;
		//::std::uint32_t              maxperstageresources;
		//::std::uint32_t              maxdescriptorsetsamplers;
		//::std::uint32_t              maxdescriptorsetuniformbuffers;
		//::std::uint32_t              maxdescriptorsetuniformbuffersdynamic;
		//::std::uint32_t              maxdescriptorsetstoragebuffers;
		//::std::uint32_t              maxdescriptorsetstoragebuffersdynamic;
		//::std::uint32_t              maxdescriptorsetsampledimages;
		//::std::uint32_t              maxdescriptorsetstorageimages;
		//::std::uint32_t              maxdescriptorsetinputattachments;
		//::std::uint32_t              maxvertexinputattributes;
		//::std::uint32_t              maxvertexinputbindings;
		//::std::uint32_t              maxvertexinputattributeoffset;
		//::std::uint32_t              maxvertexinputbindingstride;
		//::std::uint32_t              maxvertexoutputcomponents;
		//::std::uint32_t              maxtessellationgenerationlevel;
		//::std::uint32_t              maxtessellationpatchsize;
		//::std::uint32_t              maxtessellationcontrolpervertexinputcomponents;
		//::std::uint32_t              maxtessellationcontrolpervertexoutputcomponents;
		//::std::uint32_t              maxtessellationcontrolperpatchoutputcomponents;
		//::std::uint32_t              maxtessellationcontroltotaloutputcomponents;
		//::std::uint32_t              maxtessellationevaluationinputcomponents;
		//::std::uint32_t              maxtessellationevaluationoutputcomponents;
		::std::uint32_t					max_geometry_shader_invocations;
		//::std::uint32_t              maxgeometryinputcomponents;
		//::std::uint32_t              maxgeometryoutputcomponents;
		//::std::uint32_t              maxgeometryoutputvertices;
		//::std::uint32_t              maxgeometrytotaloutputcomponents;
		//::std::uint32_t              maxfragmentinputcomponents;
		//::std::uint32_t              maxfragmentoutputattachments;
		//::std::uint32_t              maxfragmentdualsrcattachments;
		//::std::uint32_t              maxfragmentcombinedoutputresources;
		::std::uint32_t					max_compute_shared_memory_size;
		::std::uint32_t					max_compute_workgroup_count[3];
		::std::uint32_t					max_compute_workgroup_invocations;
		::std::uint32_t					max_compute_workgroup_size[3];
		//::std::uint32_t              subpixelprecisionbits;
		//::std::uint32_t              subtexelprecisionbits;
		//::std::uint32_t              mipmapprecisionbits;
		//::std::uint32_t              maxdrawindexedindexvalue;
		//::std::uint32_t              maxdrawindirectcount;
		//float                 maxsamplerlodbias;
		//float                 maxsampleranisotropy;
		//::std::uint32_t              maxviewports;
		//::std::uint32_t              maxviewportdimensions[2];
		//float                 viewportboundsrange[2];
		//::std::uint32_t              viewportsubpixelbits;
		//::std::size_t                minmemorymapalignment;
		::framework::gpu::device_size	min_texel_buffer_offset_alignment;
		::framework::gpu::device_size	min_uniform_buffer_offset_alignment;
		::framework::gpu::device_size	min_storage_buffer_offset_alignment;
		//::std::int32_t               mintexeloffset;
		//::std::uint32_t              maxtexeloffset;
		//::std::int32_t               mintexelgatheroffset;
		//::std::uint32_t              maxtexelgatheroffset;
		//float                 mininterpolationoffset;
		//float                 maxinterpolationoffset;
		//::std::uint32_t              subpixelinterpolationoffsetbits;
		//::std::uint32_t              maxframebufferwidth;
		//::std::uint32_t              maxframebufferheight;
		//::std::uint32_t              maxframebufferlayers;
		//samplecountflags    frame_buffercolorsamplecounts;
		//samplecountflags    frame_bufferdepthsamplecounts;
		//samplecountflags    frame_bufferstencilsamplecounts;
		//samplecountflags    frame_buffernoattachmentssamplecounts;
		//::std::uint32_t              maxcolorattachments;
		//samplecountflags    sampledimagecolorsamplecounts;
		//samplecountflags    sampledimageintegersamplecounts;
		//samplecountflags    sampledimagedepthsamplecounts;
		//samplecountflags    sampledimagestencilsamplecounts;
		//samplecountflags    storageimagesamplecounts;
		//::std::uint32_t              maxsamplemaskwords;
		//::framework::gpu::bool32_t              timestampcomputeandgraphics;
		float							timestamp_period;
		//::std::uint32_t              maxclipdistances;
		//::std::uint32_t              maxculldistances;
		//::std::uint32_t              maxcombinedclipandculldistances;
		//::std::uint32_t              discretequeuepriorities;
		//float                 pointsizerange[2];
		//float                 linewidthrange[2];
		//float                 pointsizegranularity;
		//float                 linewidthgranularity;
		//::framework::gpu::bool32_t              strictlines;
		//::framework::gpu::bool32_t              standardsamplelocations;
		::framework::gpu::device_size	optimal_buffer_copy_offset_alignment;
		::framework::gpu::device_size	optimal_buffer_copy_row_pitch_alignment;
		//::framework::gpu::device_size          noncoherentatomsize;
		bool							viewport_y_up;
	};

	struct physical_device_sparse_properties
	{
		::framework::gpu::bool32_t	residencystandard2dblockshape;
		::framework::gpu::bool32_t	residencystandard2dmultisampleblockshape;
		::framework::gpu::bool32_t	residencystandard3dblockshape;
		::framework::gpu::bool32_t	residencyalignedmipsize;
		::framework::gpu::bool32_t	residencynonresidentstrict;
	};

	struct physical_device_acceleration_structure_properties
	{
		::std::uint64_t	max_geometry_count;
		::std::uint64_t	max_instance_count;
		::std::uint64_t	max_primitive_count;
		::std::uint32_t	max_per_stage_descriptor_acceleration_structures;
		::std::uint32_t	max_per_stage_descriptor_update_after_bind_acceleration_structures;
		::std::uint32_t	max_descriptor_set_acceleration_structures;
		::std::uint32_t	max_descriptor_set_update_after_bind_acceleration_structures;
		::std::uint32_t	min_acceleration_structure_scratch_offset_alignment;
	};

	struct physical_device_ray_tracing_pipeline_properties
	{
		::std::uint32_t	shader_group_handle_size;
		::std::uint32_t	max_ray_recursion_depth;
		::std::uint32_t	max_shader_group_stride;
		::std::uint32_t	shader_group_base_alignment;
		::std::uint32_t	shader_group_handle_capture_replay_size;
		::std::uint32_t	max_ray_dispatch_invocation_count;
		::std::uint32_t	shader_group_handle_alignment;
		::std::uint32_t	max_ray_hit_attribute_size;
	};

	struct physical_device_properties
	{
		::std::uint32_t														api_version;
		::std::uint32_t														driver_version;
		::std::uint32_t														vendor_id;
		::std::uint32_t														device_id;
		::framework::gpu::physical_device_type								device_type;
		char																device_name[::framework::gpu::max_physical_device_name_size];
		::std::uint8_t														pipeline_cache_uuid[::framework::gpu::uuid_size];
		::std::uint32_t														min_subgroup_size;
		::std::uint32_t														max_subgroup_size;
		::std::uint32_t														max_compute_workgroup_subgroups;
		::framework::gpu::shader_stage_flags								required_subgroup_size_stages;
		::framework::gpu::physical_device_limits							limits;
		::framework::gpu::physical_device_sparse_properties					sparse_properties;
		::framework::gpu::physical_device_ray_tracing_pipeline_properties	ray_tracing_pipeline_properties;
		::framework::gpu::physical_device_acceleration_structure_properties	acceleration_structure_properties;
	};

	struct queue_family_properties
	{
		::framework::gpu::queue_flags	queue_flags;
		::std::uint32_t					queue_count;
		::std::uint32_t					timestamp_valid_bits;
		::framework::gpu::extent_3d		min_image_transfer_granularity;
	};

	struct memory_type
	{
		::framework::gpu::memory_property_flags	property_flags;
		::std::uint32_t							heap_index;
	};

	struct memory_heap
	{
		::framework::gpu::device_size		size;
		::framework::gpu::memory_heap_flags	flags;
	};

	struct physical_device_memory_properties
	{
		::std::uint32_t					memory_type_count;
		::framework::gpu::memory_type	memory_types[::framework::gpu::max_memory_types];
		::std::uint32_t					memory_heap_count;
		::framework::gpu::memory_heap	memory_heaps[::framework::gpu::max_memory_heaps];
	};

	struct device_queue_create_info
	{
		::framework::gpu::device_queue_create_flags	flags;
		::std::uint32_t								queue_family_index;
		::std::uint32_t								queue_count;
		float const *								queue_priorities;
	};

	struct device_create_info
	{
		::framework::gpu::device_create_flags				flags;
		::std::uint32_t										device_idx;
		::std::uint32_t										queue_create_info_count;
		::framework::gpu::device_queue_create_info const *	queue_create_infos;
		::std::uint32_t										enabled_layer_count;
		char const * const *								enabled_layer_names;
		::std::uint32_t										enabled_extension_count;
		char const * const *								enabled_extension_names;
		::framework::gpu::physical_device_features const *	enabled_features;
	};

	struct extension_properties
	{
		char			extension_name[::framework::gpu::max_extension_name_size];
		::std::uint32_t	spec_version;
	};

	struct semaphore_wait_info
	{
		::framework::gpu::semaphore_wait_flags	flags;
		::std::uint32_t							semaphore_count;
		::framework::gpu::semaphore * const *	semaphores;
		::std::uint64_t const *					values;
	};

	struct semaphore_signal_info
	{
		::framework::gpu::semaphore *	semaphore;
		::std::uint64_t					value;
	};

	struct semaphore_submit_info
	{
		::framework::gpu::semaphore *			semaphore;
		::std::uint64_t							value;
		::framework::gpu::pipeline_stage_flags	stage_mask;
		::std::uint32_t							device_index;
	};

	struct command_buffer_submit_info
	{
		::framework::gpu::command_buffer *	command_buffer;
		::std::uint32_t						device_mask;
	};

	struct submit_info
	{
		::framework::gpu::submit_flags							flags;
		::std::uint32_t											wait_semaphore_info_count;
		::framework::gpu::semaphore_submit_info const *			wait_semaphore_infos;
		::std::uint32_t											command_buffer_info_count;
		::framework::gpu::command_buffer_submit_info const *	command_buffer_infos;
		::std::uint32_t											signal_semaphore_info_count;
		::framework::gpu::semaphore_submit_info const *			signal_semaphore_infos;
	};

	struct mapped_memory_range
	{
		::framework::gpu::device_memory *	memory;
		::framework::gpu::device_size		offset;
		::framework::gpu::device_size		size;
	};

	struct memory_dedicated_allocate_info
	{
		::framework::gpu::image *	image;
		::framework::gpu::buffer *	buffer;
	};

	struct memory_allocate_info
	{
		::framework::gpu::memory_allocate_flags				flags;
		::std::uint32_t										device_mask;
		::framework::gpu::device_size						allocation_size;
		::framework::gpu::device_size						allocation_alignment;
		::std::uint32_t										memory_type_index;
		::framework::gpu::memory_dedicated_allocate_info	memory_dedicated_allocate_info;
		::std::uint64_t										opaque_capture_address;
	};

	struct memory_requirements
	{
		::framework::gpu::device_size	size;
		::framework::gpu::device_size	alignment;
		::std::uint32_t					memory_type_bits;
		::framework::gpu::bool32_t		prefers_dedicated_allocation;
		::framework::gpu::bool32_t		requires_dedicated_allocation;
	};

	struct offset_3d
	{
		::std::int32_t	x;
		::std::int32_t	y;
		::std::int32_t	z;
	};

	union clear_color_value
	{
		float			float32[4];
		::std::int32_t	int32[4];
		::std::uint32_t	uint32[4];
	};

	struct clear_depth_stencil_value
	{
		float			depth;
		::std::uint32_t	stencil;
	};

	union clear_value
	{
		::framework::gpu::clear_color_value			color;
		::framework::gpu::clear_depth_stencil_value	depth_stencil;
	};

	struct fence_create_info
	{
		::framework::gpu::fence_create_flags	flags;
	};
	
	struct semaphore_create_info
	{
		::framework::gpu::semaphore_create_flags	flags;
		::framework::gpu::semaphore_type			semaphore_type;
		::std::uint64_t								initial_value;
	};

	struct query_pool_create_info
	{
		::framework::gpu::query_pool_create_flags			flags;
		::framework::gpu::query_type						query_type;
		::std::uint32_t										query_count;
		::framework::gpu::query_pipeline_statistic_flags	pipeline_statistics;
	};

	struct buffer_create_info
	{
		::framework::gpu::buffer_create_flags	flags;
		::framework::gpu::device_size			size;
		::framework::gpu::buffer_usage_flags	usage;
		::framework::gpu::sharing_mode			sharing_mode;
		::std::uint32_t							queue_family_index_count;
		::std::uint32_t const *					queue_family_indices;
		::std::uint32_t							initial_queue_family_index;
		::framework::gpu::buffer_state_flags	initial_state;
		::std::uint64_t							opaque_capture_address;
	};

	struct buffer_view_create_info
	{
		::framework::gpu::buffer_view_create_flags	flags;
		::framework::gpu::buffer *					buffer;
		::framework::gpu::format					format;
		::framework::gpu::device_size				offset;
		::framework::gpu::device_size				range;
	};

	struct image_create_info
	{
		::framework::gpu::image_create_flags	flags;
		::framework::gpu::image_type			image_type;
		::framework::gpu::format				format;
		::framework::gpu::extent_3d				extent;
		::std::uint32_t							mip_levels;
		::std::uint32_t							array_layers;
		::framework::gpu::sample_count_flags	samples;
		::framework::gpu::image_tiling			tiling;
		::framework::gpu::image_usage_flags		usage;
		::framework::gpu::sharing_mode			sharing_mode;
		::std::uint32_t							queue_family_index_count;
		::std::uint32_t const *					queue_family_indices;
		::std::uint32_t							initial_queue_family_index;
		::framework::gpu::image_layout_flags	initial_layout;
		::framework::gpu::clear_value const *	optimized_clear_value;
	};

	struct component_mapping
	{
		::framework::gpu::component_swizzle	r;
		::framework::gpu::component_swizzle	g;
		::framework::gpu::component_swizzle	b;
		::framework::gpu::component_swizzle	a;
	};

	struct image_subresource_range
	{
		::framework::gpu::image_aspect_flags	aspect_mask;
		::std::uint32_t							base_mip_level;
		::std::uint32_t							level_count;
		::std::uint32_t							base_array_layer;
		::std::uint32_t							layer_count;
	};

	struct image_view_create_info
	{
		::framework::gpu::image_view_create_flags	flags;
		::framework::gpu::image *					image;
		::framework::gpu::image_view_type			view_type;
		::framework::gpu::format					format;
		::framework::gpu::component_mapping			components;
		::framework::gpu::image_subresource_range	subresource_range;
	};

	struct shader_module_create_info
	{
		::framework::gpu::shader_module_create_flags	flags;
		::std::size_t									code_size;
		void const *									code;
	};

	struct pipeline_cache_create_info
	{
		::framework::gpu::pipeline_cache_create_flags	flags;
		::std::size_t									initial_data_size;
		void const *									initial_data;
	};

	struct specialization_map_entry
	{
		::std::uint32_t	constant_id;
		::std::uint32_t	offset;
		::std::size_t	size;
	};

	struct specialization_info
	{
		::std::uint32_t										map_entry_count;
		::framework::gpu::specialization_map_entry const *	map_entries;
		::std::size_t										data_size;
		void const *										data;
	};

	struct pipeline_shader_stage_create_info
	{
		::framework::gpu::pipeline_shader_stage_create_flags	flags;
		::framework::gpu::shader_stage_flags					stage;
		::framework::gpu::shader_module *						module;
		char const *											name;
		::framework::gpu::specialization_info const *			specialization_info;
	};

	struct vertex_input_binding_description
	{
		::std::uint32_t						binding;
		::std::uint32_t						stride;
		::framework::gpu::vertex_input_rate	input_rate;
	};

	struct vertex_input_attribute_description
	{
		::std::uint32_t				location;
		::std::uint32_t				binding;
		::framework::gpu::format	format;
		::std::uint32_t				offset;
	};

	struct pipeline_vertex_input_state_create_info
	{
		::framework::gpu::pipeline_vertex_input_state_create_flags		flags;
		::std::uint32_t													vertex_binding_description_count;
		::framework::gpu::vertex_input_binding_description const *		vertex_binding_descriptions;
		::std::uint32_t													vertex_attribute_description_count;
		::framework::gpu::vertex_input_attribute_description const *	vertex_attribute_descriptions;
	};

	struct pipeline_input_assembly_state_create_info
	{
		::framework::gpu::pipeline_input_assembly_state_create_flags	flags;
		::framework::gpu::primitive_topology							topology;
		bool															primitive_restart_enable;
	};

	struct pipeline_tessellation_state_create_info
	{
		::framework::gpu::pipeline_tessellation_state_create_flags	flags;
		::std::uint32_t												patch_control_points;
	};

	struct viewport
	{
		float	x;
		float	y;
		float	width;
		float	height;
		float	min_depth;
		float	max_depth;
	};

	struct offset_2d
	{
		::std::int32_t	x;
		::std::int32_t	y;
	};

	struct extent_2d
	{
		::std::uint32_t	width;
		::std::uint32_t	height;
	};

	struct rect_2d
	{
		::framework::gpu::offset_2d	offset;
		::framework::gpu::extent_2d	extent;
	};

	struct pipeline_viewport_state_create_info
	{
		::std::uint32_t						viewport_count;
		::framework::gpu::viewport const *	viewports;
		::std::uint32_t						scissor_count;
		::framework::gpu::rect_2d const *	scissors;
	};

	struct pipeline_rasterization_state_create_info
	{
		bool								depth_clamp_enable;
		bool								rasterizer_discard_enable;
		::framework::gpu::polygon_mode		polygon_mode;
		::framework::gpu::cull_mode_flags	cull_mode;
		::framework::gpu::front_face		front_face;
		bool								depth_bias_enable;
		float								depth_bias_constant_factor;
		float								depth_bias_clamp;
		float								depth_bias_slope_factor;
		float								line_width;
	};

	struct pipeline_multisample_state_create_info
	{
		::framework::gpu::pipeline_multisample_state_create_flags	flags;
		::framework::gpu::sample_count_flags						rasterization_samples;
		::framework::gpu::bool32_t									sample_shading_enable;
		float														min_sample_shading;
		::framework::gpu::sample_mask const *						sample_mask;
		::framework::gpu::bool32_t									alpha_to_coverage_enable;
		::framework::gpu::bool32_t									alpha_to_one_enable;
	};

	struct stencil_op_state
	{
		::framework::gpu::stencil_op	fail_op;
		::framework::gpu::stencil_op	pass_op;
		::framework::gpu::stencil_op	depth_fail_op;
		::framework::gpu::compare_op	compare_op;
		::std::uint32_t					compare_mask;
		::std::uint32_t					write_mask;
		::std::uint32_t					reference;
	};

	struct pipeline_depth_stencil_state_create_info
	{
		bool								depth_test_enable;
		bool								depth_write_enable;
		::framework::gpu::compare_op		depth_compare_op;
		bool								depth_bounds_test_enable;
		bool								stencil_test_enable;
		::framework::gpu::stencil_op_state	front;
		::framework::gpu::stencil_op_state	back;
		float								min_depth_bounds;
		float								max_depth_bounds;
	};

	struct pipeline_color_blend_attachment_state
	{
		bool									blend_enable;
		::framework::gpu::blend_factor			src_color_blend_factor;
		::framework::gpu::blend_factor			dst_color_blend_factor;
		::framework::gpu::blend_op				color_blend_op;
		::framework::gpu::blend_factor			src_alpha_blend_factor;
		::framework::gpu::blend_factor			dst_alpha_blend_factor;
		::framework::gpu::blend_op				alpha_blend_op;
		::framework::gpu::color_component_flags	color_write_mask;
	};

	struct pipeline_color_blend_state_create_info
	{
		bool															logic_op_enable;
		::framework::gpu::logic_op										logic_op;
		::std::uint32_t													attachment_count;
		::framework::gpu::pipeline_color_blend_attachment_state const *	attachments;
		float															blend_constants[4];
	};

	struct pipeline_dynamic_state_create_info
	{
		::framework::gpu::pipeline_dynamic_state_create_flags	flags;
		::std::uint32_t											dynamic_state_count;
		::framework::gpu::dynamic_state const *					dynamic_states;
	};

	struct graphics_pipeline_create_info
	{
		::framework::gpu::pipeline_create_flags								flags;
		::std::uint32_t														stage_count;
		::framework::gpu::pipeline_shader_stage_create_info const *			stages;
		::framework::gpu::pipeline_vertex_input_state_create_info const *	vertex_input_state;
		::framework::gpu::pipeline_input_assembly_state_create_info const *	input_assembly_state;
		::framework::gpu::pipeline_tessellation_state_create_info const *	tessellation_state;
		::framework::gpu::pipeline_viewport_state_create_info const *		viewport_state;
		::framework::gpu::pipeline_rasterization_state_create_info const *	rasterization_state;
		::framework::gpu::pipeline_multisample_state_create_info const *	multisample_state;
		::framework::gpu::pipeline_depth_stencil_state_create_info const *	depth_stencil_state;
		::framework::gpu::pipeline_color_blend_state_create_info const *	color_blend_state;
		::framework::gpu::pipeline_dynamic_state_create_info const *		dynamic_state;
		::framework::gpu::pipeline_layout *									layout;
		::framework::gpu::render_pass *										render_pass;
		::std::uint32_t														subpass;
		::framework::gpu::pipeline *										base_pipeline;
		::std::int32_t														base_pipeline_index;
	};

	struct compute_pipeline_create_info
	{
		::framework::gpu::pipeline_create_flags				flags;
		::framework::gpu::pipeline_shader_stage_create_info	stage;
		::framework::gpu::pipeline_layout *					layout;
		::framework::gpu::pipeline *						base_pipeline;
		::std::int32_t										base_pipeline_index;
	};

	struct push_constant_range
	{
		::framework::gpu::shader_stage_flags	stage_flags;
		::std::uint32_t							binding;
		::std::uint32_t							offset;
		::std::uint32_t							size;
		::std::uint32_t							hlsl_shader_register;
		::std::uint32_t							hlsl_register_space;
	};

	struct pipeline_layout_create_info
	{
		::framework::gpu::pipeline_layout_create_flags		flags;
		::std::uint32_t										descriptor_set_layout_count;
		::framework::gpu::descriptor_set_layout * const *	descriptor_set_layouts;
		::std::uint32_t										push_constant_range_count;
		::framework::gpu::push_constant_range const *		push_constant_ranges;
	};

	struct sampler_create_info
	{
		::framework::gpu::sampler_create_flags	flags;
		::framework::gpu::filter				mag_filter;
		::framework::gpu::filter				min_filter;
		::framework::gpu::sampler_mipmap_mode	mip_map_mode;
		::framework::gpu::sampler_address_mode	address_mode_u;
		::framework::gpu::sampler_address_mode	address_mode_v;
		::framework::gpu::sampler_address_mode	address_mode_w;
		float									mip_lod_bias;
		bool									anisotropy_enable;
		float									max_anisotropy;
		bool									compare_enable;
		::framework::gpu::compare_op			compare_op;
		float									min_lod;
		float									max_lod;
		::framework::gpu::border_color			border_color;
		bool									unnormalized_coordinates;
	};

	struct descriptor_set_layout_binding
	{
		::framework::gpu::descriptor_binding_flags	flags;
		::std::uint32_t								binding;
		::std::uint32_t								hlsl_shader_register;
		::std::uint32_t								hlsl_register_space;
		::framework::gpu::descriptor_type			descriptor_type;
		::std::uint32_t								descriptor_count;
		::framework::gpu::shader_stage_flags		stage_flags;
		::framework::gpu::sampler * const *			immutable_samplers;
	};

	struct descriptor_set_layout_create_info
	{
		::framework::gpu::descriptor_set_layout_create_flags	flags;
		::std::uint32_t											binding_count;
		::framework::gpu::descriptor_set_layout_binding const *	bindings;
	};

	struct descriptor_pool_size
	{
		::framework::gpu::descriptor_type	type;
		::std::uint32_t						descriptor_count;
	};

	struct descriptor_pool_create_info
	{
		::framework::gpu::descriptor_pool_create_flags		flags;
		::std::uint32_t										max_sets;
		::std::uint32_t										pool_size_count;
		::framework::gpu::descriptor_pool_size const *		pool_sizes;
	};

	struct descriptor_set_allocate_info
	{
		::framework::gpu::descriptor_pool *					descriptor_pool;
		::std::uint32_t										descriptor_set_count;
		::framework::gpu::descriptor_set_layout * const *	set_layouts;
		::std::uint32_t const *								variable_descriptor_counts;
	};

	struct descriptor_image_info
	{
		::framework::gpu::sampler *				sampler;
		::framework::gpu::image_view *			image_view;
		::framework::gpu::image_layout_flags	image_layout;
	};

	struct descriptor_buffer_info
	{
		::framework::gpu::descriptor_buffer_info_flags	flags;
		::framework::gpu::buffer *						buffer;
		::framework::gpu::device_size					offset;
		::framework::gpu::device_size					range;
		::framework::gpu::device_size					stride;
	};

	struct write_descriptor_set
	{
		::framework::gpu::descriptor_set *							dst_set;
		::std::uint32_t												dst_binding;
		::std::uint32_t												dst_array_element;
		::std::uint32_t												descriptor_count;
		::framework::gpu::descriptor_type							descriptor_type;
		::framework::gpu::descriptor_image_info const *				image_info;
		::framework::gpu::descriptor_buffer_info const *			buffer_info;
		::framework::gpu::buffer_view * const *						texel_buffer_view;
		::framework::gpu::acceleration_structure const * const *	acceleration_structures;
	};

	struct copy_descriptor_set
	{
		::framework::gpu::descriptor_set *	src_set;
		::std::uint32_t						src_binding;
		::std::uint32_t						src_array_element;
		::framework::gpu::descriptor_set *	dst_set;
		::std::uint32_t						dst_binding;
		::std::uint32_t						dst_array_element;
		::std::uint32_t						descriptor_count;
		::framework::gpu::descriptor_type	descriptor_type;
	};

	struct flush_descriptor_set
	{
		::framework::gpu::descriptor_set *	set;
		::std::uint32_t						binding;
		::std::uint32_t						array_element;
		::std::uint32_t						descriptor_count;
	};

	struct attachment_view_and_descriptor
	{
		::framework::gpu::image_view *				view;
		::framework::gpu::host_descriptor_handle	descriptor_handle;
	};

	struct frame_buffer_create_info
	{
		::framework::gpu::frame_buffer_create_flags					flags;
		::framework::gpu::render_pass *								render_pass;
		::std::uint32_t												attachment_count;
		::framework::gpu::attachment_view_and_descriptor const *	attachments;
		::std::uint32_t												width;
		::std::uint32_t												height;
		::std::uint32_t												layers;
	};

	struct attachment_description
	{
		::framework::gpu::attachment_description_flags	flags;
		::framework::gpu::format						format;
		::framework::gpu::sample_count_flags			samples;
		::framework::gpu::attachment_load_op			load_op;
		::framework::gpu::attachment_store_op			store_op;
		::framework::gpu::attachment_load_op			stencil_load_op;
		::framework::gpu::attachment_store_op			stencil_store_op;
		::framework::gpu::image_layout_flags			initial_layout;
		::framework::gpu::image_layout_flags			final_layout;
	};

	struct attachment_reference
	{
		::std::uint32_t							attachment;
		::framework::gpu::image_layout_flags	layout;
	};

	struct subpass_description
	{
		::framework::gpu::subpass_description_flags		flags;
		::framework::gpu::pipeline_bind_point			pipeline_bind_point;
		::std::uint32_t									input_attachment_count;
		::framework::gpu::attachment_reference const *	input_attachments;
		::std::uint32_t									color_attachment_count;
		::framework::gpu::attachment_reference const *	color_attachments;
		::framework::gpu::attachment_reference const *	resolve_attachments;
		::framework::gpu::attachment_reference const *	depth_stencil_attachment;
		::std::uint32_t									preserve_attachment_count;
		::std::uint32_t const *							preserve_attachments;
	};

	struct subpass_dependency
	{
		::std::uint32_t							src_subpass;
		::std::uint32_t							dst_subpass;
		::framework::gpu::pipeline_stage_flags	src_stage_mask;
		::framework::gpu::pipeline_stage_flags	dst_stage_mask;
		::framework::gpu::access_flags			src_access_mask;
		::framework::gpu::access_flags			dst_access_mask;
		::framework::gpu::dependency_flags		dependency_flags;
	};

	struct render_pass_create_info
	{
		::framework::gpu::render_pass_create_flags			flags;
		::std::uint32_t										attachment_count;
		::framework::gpu::attachment_description const *	attachments;
		::std::uint32_t										subpass_count;
		::framework::gpu::subpass_description const *		subpasses;
		::std::uint32_t										dependency_count;
		::framework::gpu::subpass_dependency const *		dependencies;
	};

	struct command_pool_create_info
	{
		::framework::gpu::command_pool_create_flags	flags;
		::std::uint32_t								queue_family_index;
	};

	struct command_buffer_allocate_info
	{
		::framework::gpu::command_pool *		command_pool;
		::framework::gpu::command_buffer_level	level;
		::std::uint32_t							subbuffer_count;
		::std::uint32_t							command_buffer_count;
	};

	struct command_buffer_inheritance_info
	{
		::framework::gpu::render_pass_state *				render_pass_state;
		::std::uint32_t										subpass;
		::framework::gpu::bool32_t							occlusion_query_enable;
		::framework::gpu::query_control_flags				query_flags;
		::framework::gpu::query_pipeline_statistic_flags	pipeline_statistics;
	};

	struct command_buffer_begin_info
	{
		::framework::gpu::command_buffer_usage_flags				flags;
		::framework::gpu::command_buffer_inheritance_info const *	inheritance_info;
	};

	struct buffer_copy
	{
		::framework::gpu::device_size	src_offset;
		::framework::gpu::device_size	dst_offset;
		::framework::gpu::device_size	size;
	};

	struct image_subresource_layers
	{
		::framework::gpu::image_aspect_flags	aspect_mask;
		::std::uint32_t							mip_level;
		::std::uint32_t							base_array_layer;
		::std::uint32_t							layer_count;
	};

	struct buffer_image_copy
	{
		::framework::gpu::device_size				buffer_offset;
		::std::uint32_t								buffer_row_length;
		::std::uint32_t								buffer_image_height;
		::framework::gpu::image_subresource_layers	image_subresource;
		::framework::gpu::offset_3d					image_offset;
		::framework::gpu::extent_3d					image_extent;
	};

	struct image_copy
	{
		::framework::gpu::image_subresource_layers	src_subresource;
		::framework::gpu::offset_3d					src_offset;
		::framework::gpu::image_subresource_layers	dst_subresource;
		::framework::gpu::offset_3d					dst_offset;
		::framework::gpu::extent_3d					extent;
	};

	struct queue_family_ownership
	{
		::framework::gpu::queue_family_ownership_flags	flags;
		::std::uint32_t									queue_family_index;
	};

	struct memory_barrier
	{
		::framework::gpu::pipeline_stage_flags	src_stage_mask;
		::framework::gpu::access_flags			src_access_mask;
		::framework::gpu::pipeline_stage_flags	dst_stage_mask;
		::framework::gpu::access_flags			dst_access_mask;
	};

	struct buffer_memory_barrier
	{
		::framework::gpu::access_flags				src_access_mask;
		::framework::gpu::access_flags				dst_access_mask;
		::framework::gpu::pipeline_stage_flags		src_stage_mask;
		::framework::gpu::pipeline_stage_flags		dst_stage_mask;
		::framework::gpu::buffer_state_flags		old_state;
		::framework::gpu::buffer_state_flags		new_state;
		::framework::gpu::queue_family_ownership	src_queue_family_ownership;
		::framework::gpu::queue_family_ownership	dst_queue_family_ownership;
		::framework::gpu::buffer *					buffer;
		::framework::gpu::device_size				offset;
		::framework::gpu::device_size				size;
	};

	struct image_memory_barrier
	{
		::framework::gpu::access_flags				src_access_mask;
		::framework::gpu::access_flags				dst_access_mask;
		::framework::gpu::pipeline_stage_flags		src_stage_mask;
		::framework::gpu::pipeline_stage_flags		dst_stage_mask;
		::framework::gpu::image_layout_flags		old_layout;
		::framework::gpu::image_layout_flags		new_layout;
		::framework::gpu::queue_family_ownership	src_queue_family_ownership;
		::framework::gpu::queue_family_ownership	dst_queue_family_ownership;
		::framework::gpu::image *					image;
		::framework::gpu::image_subresource_range	subresource_range;
	};

	struct copy_image_to_buffer_info
	{
		::framework::gpu::image *					src_image;
		::framework::gpu::image_layout_flags		src_image_layout;
		::framework::gpu::buffer *					dst_buffer;
		::std::uint32_t								region_count;
		::framework::gpu::buffer_image_copy const *	regions;
	};

	struct dependency_info
	{
		::framework::gpu::dependency_flags				dependency_flags;
		::std::uint32_t									memory_barrier_count;
		::framework::gpu::memory_barrier const *		memory_barriers;
		::std::uint32_t									buffer_memory_barrier_count;
		::framework::gpu::buffer_memory_barrier const *	buffer_memory_barriers;
		::std::uint32_t									image_memory_barrier_count;
		::framework::gpu::image_memory_barrier const *	image_memory_barriers;
	};

	struct render_pass_state_create_info
	{
		::framework::gpu::render_pass const *	render_pass;
		::std::uint32_t							max_clear_value_count;
	};

	struct render_pass_state_info
	{
		::framework::gpu::frame_buffer const *	frame_buffer;
		::framework::gpu::rect_2d				render_area;
		::std::uint32_t							clear_value_count;
		::framework::gpu::clear_value const *	clear_values;
	};

	struct render_pass_begin_info
	{
		::framework::gpu::render_pass_state * render_pass_state;
	};

	struct dispatch_indirect_command
	{
		::std::uint32_t	x;
		::std::uint32_t	y;
		::std::uint32_t	z;
	};

	struct draw_indexed_indirect_command
	{
		::std::uint32_t	index_count;
		::std::uint32_t	instance_count;
		::std::uint32_t	first_index;
		::std::int32_t	vertex_offset;
		::std::uint32_t	first_instance;
	};

	struct draw_indirect_command
	{
		::std::uint32_t	vertex_count;
		::std::uint32_t	instance_count;
		::std::uint32_t	first_vertex;
		::std::uint32_t	first_instance;
	};

	struct bind_buffer_memory_info
	{
		::framework::gpu::buffer *			buffer;
		::framework::gpu::device_memory *	memory;
		::framework::gpu::device_size		memory_offset;
	};

	struct bind_image_memory_info
	{
		::framework::gpu::image *			image;
		::framework::gpu::device_memory *	memory;
		::framework::gpu::device_size		memory_offset;
	};

	struct buffer_memory_requirements_info
	{
		::framework::gpu::buffer *	buffer;
	};

	struct image_memory_requirements_info
	{
		::framework::gpu::image *	image;
	};

	struct image_sparse_memory_requirements_info
	{
		::framework::gpu::image *	image;
	};

	struct pipeline_library_create_info
	{
		::std::uint32_t								library_count;
		::framework::gpu::pipeline const * const *	library_info;
	};

	union device_or_host_descriptor_handle
	{
		::framework::gpu::device_descriptor_handle	device_descriptor_handle;
		::framework::gpu::host_descriptor_handle	host_descriptor_handle;
	};

	union device_or_host_address
	{
		::framework::gpu::device_address	device_address;
		void *								host_address;
	};

	union device_or_host_address_const
	{
		::framework::gpu::device_address	device_address;
		void const *						host_address;
	};

	struct ray_tracing_shader_group_create_info
	{
		::framework::gpu::ray_tracing_shader_group_type	type;
		::std::uint32_t									general_shader;
		::std::uint32_t									closest_hit_shader;
		::std::uint32_t									any_hit_shader;
		::std::uint32_t									intersection_shader;
		void const *									shader_group_capture_replay_handle;
	};

	struct ray_tracing_local_layout_shader_association
	{
		::std::uint32_t group;
		::std::uint32_t layout;
	};

	struct ray_tracing_pipeline_create_info
	{
		::framework::gpu::pipeline_create_flags									flags;
		::std::uint32_t															stage_count;
		::framework::gpu::pipeline_shader_stage_create_info const *				stages;
		::std::uint32_t															group_count;
		::framework::gpu::ray_tracing_shader_group_create_info const *			groups;
		::std::uint32_t															max_pipeline_ray_recursion_depth;
		::std::uint32_t															max_pipeline_ray_payload_size;
		::std::uint32_t															max_pipeline_ray_hit_attribute_size;
		::framework::gpu::pipeline_library_create_info const *					library_info;
		::framework::gpu::pipeline_dynamic_state_create_info const *			dynamic_state;
		::framework::gpu::pipeline_layout *										layout;
		::std::uint32_t															local_layout_count;
		::framework::gpu::pipeline_layout * const *								local_layouts;
		::std::uint32_t															association_count;
		::framework::gpu::ray_tracing_local_layout_shader_association const *	associations;
		::framework::gpu::pipeline *											base_pipeline;
		::std::int32_t															base_pipeline_index;
	};

	struct acceleration_structure_geometry_triangles_data
	{
		::framework::gpu::format						vertex_format;
		::framework::gpu::device_or_host_address_const	vertex_data;
		::framework::gpu::device_size					vertex_stride;
		::framework::gpu::device_size					vertex_count;
		::std::uint32_t									first_vertex;
		::framework::gpu::index_type					index_type;
		::framework::gpu::device_or_host_address_const	index_data;
		::framework::gpu::device_or_host_address_const	transform_data;
		::std::uint32_t									transform_offset;
	};

	struct acceleration_structure_geometry_aabbs_data
	{
		::framework::gpu::device_or_host_address_const	data;
		::framework::gpu::device_size					stride;
	};

	struct acceleration_structure_geometry_instances_data
	{
		::framework::gpu::bool32_t						array_of_pointers;
		::framework::gpu::device_or_host_address_const	data;
	};

	union acceleration_structure_geometry_data
	{
		::framework::gpu::acceleration_structure_geometry_triangles_data	triangles;
		::framework::gpu::acceleration_structure_geometry_aabbs_data		aabbs;
		::framework::gpu::acceleration_structure_geometry_instances_data	instances;
	};

	struct acceleration_structure_geometry
	{
		::framework::gpu::geometry_flags						flags;
		::std::uint32_t											primitive_count;
		::std::uint32_t											primitive_offset;
		::framework::gpu::acceleration_structure_geometry_data	geometry;
	};

	struct acceleration_structure_geometry_reference
	{
		::framework::gpu::acceleration_structure_geometry_set * src_set;
		::std::uint32_t											src_array_element;
	};

	union acceleration_structure_geometry_set_data
	{
		::framework::gpu::acceleration_structure_geometry const *			geometries;
		::framework::gpu::acceleration_structure_geometry_reference const *	geometry_references;
	};

	struct write_acceleration_structure_geometry
	{
		::framework::gpu::acceleration_structure_geometry_set_layout	layout;
		::std::uint32_t													first_geometry;
		::std::uint32_t													geometry_count;
		::framework::gpu::geometry_type									geometry_type;
		::framework::gpu::acceleration_structure_geometry_set_data		data;
	};

	struct acceleration_structure_geometry_set_create_info
	{
		::framework::gpu::acceleration_structure_geometry_set_layout	layout;
		::std::uint32_t													geometry_count;
		::framework::gpu::geometry_type									geometry_type;
	};

	struct acceleration_structure_build_sizes_info
	{
		::framework::gpu::device_size	acceleration_structure_size;
		::framework::gpu::device_size	update_scratch_size;
		::framework::gpu::device_size	build_scratch_size;
	};

	struct acceleration_structure_build_geometry_info
	{
		::framework::gpu::acceleration_structure_type			type;
		::framework::gpu::build_acceleration_structure_flags	flags;
		::framework::gpu::build_acceleration_structure_mode		mode;
		::framework::gpu::acceleration_structure *				src_acceleration_structure;
		::framework::gpu::acceleration_structure *				dst_acceleration_structure;
		::std::uint32_t											first_geometry;
		::std::uint32_t											geometry_count;
		::framework::gpu::acceleration_structure_geometry_set *	geometries;
		::framework::gpu::device_or_host_address				scratch_data;
	};

	struct acceleration_structure_create_info
	{
		::framework::gpu::acceleration_structure_create_flags	create_flags;
		::framework::gpu::buffer *								buffer;
		::framework::gpu::device_size							offset;
		::framework::gpu::device_size							size;
		::framework::gpu::acceleration_structure_type			type;
		::framework::gpu::device_address						device_address;
	};

	struct transform_matrix
	{
		float	matrix[3][4];
	};

	struct aabb_positions
	{
		float	min_x;
		float	min_y;
		float	min_z;
		float	max_x;
		float	max_y;
		float	max_z;
	};

	struct acceleration_structure_device_address_info
	{
		::framework::gpu::acceleration_structure *	acceleration_structure;
	};

	struct acceleration_structure_version
	{
		::std::uint8_t const *	version_data;
	};

	struct strided_device_address_region
	{
		::framework::gpu::device_address	device_address;
		::framework::gpu::device_size		stride;
		::framework::gpu::device_size		size;
	};

	struct trace_rays_indirect_command
	{
		::std::uint32_t	width;
		::std::uint32_t	height;
		::std::uint32_t	depth;
	};

	struct copy_acceleration_structure_to_memory_info
	{
		::framework::gpu::acceleration_structure *			src;
		::framework::gpu::device_or_host_address			dst;
		::framework::gpu::copy_acceleration_structure_mode	mode;
	};

	struct copy_memory_to_acceleration_structure_info
	{
		::framework::gpu::device_or_host_address_const		src;
		::framework::gpu::acceleration_structure *			dst;
		::framework::gpu::copy_acceleration_structure_mode	mode;
	};

	struct copy_acceleration_structure_info
	{
		::framework::gpu::acceleration_structure *			src;
		::framework::gpu::acceleration_structure *			dst;
		::framework::gpu::copy_acceleration_structure_mode	mode;
	};

	struct bind_acceleration_structure_memory_info
	{
		::framework::gpu::acceleration_structure *	acceleration_structure;
		::framework::gpu::device_memory *			memory;
		::framework::gpu::device_size				memory_offset;
		::std::uint32_t								device_index_count;
		::std::uint32_t const *						device_indices;
	};

	struct acceleration_structure_instance
	{
		::framework::gpu::transform_matrix			transform;
		::std::uint32_t								instance_custom_index : 24;
		::std::uint32_t								mask : 8;
		::std::uint32_t								instance_shader_binding_table_record_offset : 24;
		::framework::gpu::geometry_instance_flags	flags : 8;
		::std::uint64_t								acceleration_structure_reference;
	};

	struct surface_create_info
	{
		::framework::platform::window *	window;
	};

	struct surface_capabilities
	{
		::std::uint32_t								min_image_count;
		::std::uint32_t								max_image_count;
		::framework::gpu::extent_2d					current_extent;
		::framework::gpu::extent_2d					min_image_extent;
		::framework::gpu::extent_2d					max_image_extent;
		::std::uint32_t								max_image_array_layers;
		::framework::gpu::surface_transform_flags	supported_transforms;
		::framework::gpu::surface_transform_flags	current_transform;
		::framework::gpu::composite_alpha_flags		supported_composite_alpha;
		::framework::gpu::image_usage_flags			supported_usage_flags;
	};

	struct surface_format
	{
		::framework::gpu::format		format;
		::framework::gpu::color_space	color_space;
	};

	struct swap_chain_create_info
	{
		::framework::gpu::swap_chain_create_flags	flags;
		::framework::gpu::surface *					surface;
		::std::uint32_t								min_image_count;
		::framework::gpu::format					image_format;
		::framework::gpu::color_space				image_color_space;
		::framework::gpu::extent_2d					image_extent;
		::std::uint32_t								image_array_layers;
		::framework::gpu::image_usage_flags			image_usage;
		::framework::gpu::sharing_mode				image_sharing_mode;
		::std::uint32_t								queue_family_index_count;
		::std::uint32_t const *						queue_family_indices;
		::framework::gpu::surface_transform_flags	pre_transform;
		::framework::gpu::composite_alpha_flags		composite_alpha;
		::framework::gpu::present_mode				present_mode;
		bool										clipped;
		::framework::gpu::swap_chain *				old_swap_chain;
	};

	struct acquire_next_image_info
	{
		::framework::gpu::swap_chain *	swap_chain;
		::std::uint64_t					timeout;
		::framework::gpu::semaphore *	semaphore;
		::framework::gpu::fence *		fence;
		::std::uint32_t					device_mask;
	};

	struct present_info
	{
		::std::uint32_t							wait_semaphore_count;
		::framework::gpu::semaphore * const *	wait_semaphores;
		::std::uint32_t							swap_chain_count;
		::framework::gpu::swap_chain * const *	swap_chains;
		::std::uint32_t const *					image_indices;
		::framework::gpu::result *				results;
	};

	struct descriptor_handle_info
	{
		::framework::gpu::descriptor_handle_info_flags	flags;
		bool											device_descriptor_handle;
		::framework::gpu::descriptor_set *				set;
		::std::uint32_t									binding;
		::std::uint32_t									array_element;
	};

	struct buffer_device_address_info
	{
		::framework::gpu::buffer *	buffer;
	};

	struct device_memory_opaque_capture_address_info
	{
		::framework::gpu::device_memory *	memory;
	};

	struct debug_utils_object_name_info
	{
		::framework::gpu::object_type	object_type;
		void *							object;
		char const *					object_name;
	};

	struct cooperative_matrix_properties
	{
		::std::uint32_t						m_size;
		::std::uint32_t						n_size;
		::std::uint32_t						k_size;
		::framework::gpu::component_type	a_type;
		::framework::gpu::component_type	b_type;
		::framework::gpu::component_type	c_type;
		::framework::gpu::component_type	result_type;
		::framework::gpu::bool32_t			saturating_accumulation;
		::framework::gpu::scope				scope;
	};
}