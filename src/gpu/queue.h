#pragma once

namespace framework::gpu
{
	class queue
	{
	public:
		virtual ::framework::gpu::result submit(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, ::framework::gpu::fence * fence) = 0;

		virtual ::framework::gpu::result present(::framework::gpu::present_info const * present_info) = 0;

		virtual ::framework::gpu::result wait_idle() = 0;

		virtual ::framework::gpu::result get_timestamp_period(float * timestamp_period) = 0;
	};
}