#include "gpu/utility.hpp"
#include <assert.h>
#include <iostream>
#include <stdexcept>
#include <string>

::std::uint32_t(::framework::gpu::utility::get_queue_family_index)(::std::uint32_t queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties, ::framework::gpu::queue_flags queue_flags)
{
	// Dedicated queue for compute
	// Try to find a queue family index that supports compute but not graphics
	if (!!(queue_flags & ::framework::gpu::queue_flags::compute_bit))
	{
		for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
		{
			if (!!(queue_family_properties[i].queue_flags & queue_flags) && !(queue_family_properties[i].queue_flags & ::framework::gpu::queue_flags::graphics_bit))
			{
				return i;
				break;
			}
		}
	}

	// Dedicated queue for transfer
	// Try to find a queue family index that supports transfer but not graphics and compute
	if (!!(queue_flags & ::framework::gpu::queue_flags::transfer_bit))
	{
		for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
		{
			if (!!(queue_family_properties[i].queue_flags & queue_flags) && !(queue_family_properties[i].queue_flags & ::framework::gpu::queue_flags::graphics_bit) && !(queue_family_properties[i].queue_flags & ::framework::gpu::queue_flags::compute_bit))
			{
				return i;
				break;
			}
		}
	}

	// For other queue types or if no separate compute ::framework::gpu::queue is present, return the first one to support the requested flags
	for (::std::uint32_t i = 0; i < queue_family_property_count; i++)
	{
		if (!!(queue_family_properties[i].queue_flags & queue_flags))
		{
			return i;
			break;
		}
	}

	throw ::std::runtime_error("Could not find a matching ::framework::gpu::queue family index");
}

::std::uint32_t(::framework::gpu::utility::get_memory_type_index)(::framework::gpu::physical_device_memory_properties const & physical_device_memory_properties, ::std::uint32_t type_bits, ::framework::gpu::memory_property_flags properties, bool * memory_type_found)
{
	for (::std::uint32_t i = 0; i < physical_device_memory_properties.memory_type_count; i++)
	{
		if ((type_bits & 1) == 1)
		{
			if ((physical_device_memory_properties.memory_types[i].property_flags & properties) == properties)
			{
				if (memory_type_found)
				{
					*memory_type_found = true;
				}
				return i;
			}
		}
		type_bits >>= 1;
	}

#if defined(__ANDROID__)
	//todo : Exceptions are disabled by default on Android (need to add LOCAL_CPP_FEATURES += exceptions to Android.mk), so for now just return zero
	if (memory_type_found)
	{
		*memory_type_found = false;
	}
	return 0;
#else
	if (memory_type_found)
	{
		*memory_type_found = false;
		return 0;
	}
	else
	{
		throw ::std::runtime_error("Could not find a matching memory type");
	}
#endif
}

void ::framework::gpu::utility::assert_result(::framework::gpu::result result, char const * file, int const line)
{
	if (result < ::framework::gpu::result::success)
	{
		::std::cout << "Fatal : ::framework::gpu::result is \"" << ::framework::gpu::utility::result_string(result) << "\" in " << file << " at line " << line << ::std::endl;
		assert(result == ::framework::gpu::result::success);
	}
	else if (result > ::framework::gpu::result::success)
	{
		::std::cout << "Info : ::framework::gpu::result is \"" << ::framework::gpu::utility::result_string(result) << "\" in " << file << " at line " << line << ::std::endl;
	}
}

::std::string(::framework::gpu::utility::result_string)(::framework::gpu::result result)
{
	switch (result)
	{
#define STR(r) case ::framework::gpu::result::r: return #r
		STR(not_ready);
		STR(timeout);
		STR(event_set);
		STR(event_reset);
		STR(incomplete);
		STR(error_out_of_host_memory);
		STR(error_out_of_device_memory);
		STR(error_initialization_failed);
		STR(error_device_lost);
		STR(error_memory_map_failed);
		STR(error_layer_not_present);
		STR(error_extension_not_present);
		STR(error_feature_not_present);
		STR(error_incompatible_driver);
		STR(error_too_many_objects);
		STR(error_format_not_supported);
		STR(error_validation_failed);
		STR(error_invalid_shader);
		STR(error_out_of_pool_memory);
		STR(error_invalid_external_handle);
		STR(error_not_permitted);
#undef STR
	default:
		return "UNKNOWN_ERROR";
	}
}