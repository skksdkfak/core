#pragma once

#include "gpu/core.hpp"
#include <cstdint>
#include <string>

#ifdef NDEBUG
#define assert_framework_gpu_result(f) f
#else
#define assert_framework_gpu_result(f) ::framework::gpu::utility::assert_result(f, __FILE__, __LINE__)
#endif

namespace framework::gpu::utility
{
	::std::uint32_t get_queue_family_index(::std::uint32_t queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties, ::framework::gpu::queue_flags queue_flags);

	::std::uint32_t get_memory_type_index(::framework::gpu::physical_device_memory_properties const & physical_device_memory_properties, ::std::uint32_t type_bits, ::framework::gpu::memory_property_flags properties, bool * memory_type_found = nullptr);

	void assert_result(::framework::gpu::result result, char const * file, int const line);

	constexpr inline bool is_texture_compression_bc_format(::framework::gpu::format format);
	
	constexpr inline char const * get_component_type_name(::framework::gpu::component_type component_type);

	constexpr inline char const * get_component_type_size_bits_string(::framework::gpu::component_type component_type);

	constexpr inline char const * get_component_type_size_bytes_string(::framework::gpu::component_type component_type);

	constexpr inline ::std::size_t get_component_type_size(::framework::gpu::component_type component_type);

	::std::string result_string(::framework::gpu::result result);
}

#include "gpu/utility.inl"