#pragma once

namespace framework::gpu
{
	class physical_device
	{
	public:
		virtual ::framework::gpu::result create_device(::framework::gpu::device_create_info const * create_info, ::framework::gpu::allocation_callbacks const * allocator, ::framework::gpu::device ** device) = 0;

		virtual ::framework::gpu::result enumerate_device_extension_properties(char const * layer_name, ::std::uint32_t * property_count, ::framework::gpu::extension_properties * properties) = 0;

		virtual void get_features(::framework::gpu::physical_device_features * features) = 0;

		virtual void get_properties(::framework::gpu::physical_device_properties * properties) = 0;

		virtual void get_queue_family_properties(::std::uint32_t * queue_family_property_count, ::framework::gpu::queue_family_properties * queue_family_properties) = 0;

		virtual void get_memory_properties(::framework::gpu::physical_device_memory_properties * physical_device_memory_properties) = 0;

		virtual ::framework::gpu::result get_surface_support(::std::uint32_t queue_family_index, ::framework::gpu::surface * surface, ::framework::gpu::bool32_t * supported) = 0;

		virtual ::framework::gpu::result get_surface_capabilities(::framework::gpu::surface * surface, ::framework::gpu::surface_capabilities * surface_capabilities) = 0;

		virtual ::framework::gpu::result get_surface_formats(::framework::gpu::surface * surface, ::std::uint32_t * surface_format_count, ::framework::gpu::surface_format * surface_formats) = 0;

		virtual ::framework::gpu::result get_cooperative_matrix_properties(::std::uint32_t * property_count, ::framework::gpu::cooperative_matrix_properties * properties) = 0;
	};
}