#pragma once

namespace framework::gpu
{
	class render_pass_state
	{
	public:
		virtual void set_state(::framework::gpu::render_pass_state_info const & render_pass_state_info) = 0;
	};
}