#pragma once

namespace framework
{
	namespace gpu
	{
		struct dispatch_indirect_command
		{
			static ::framework::gpu::dispatch_indirect_command create(uint x, uint y, uint z)
			{
				::framework::gpu::dispatch_indirect_command dispatch_indirect_command;
				dispatch_indirect_command.x = x;
				dispatch_indirect_command.y = y;
				dispatch_indirect_command.z = z;
				return dispatch_indirect_command;
			}

			uint32_t x;
			uint32_t y;
			uint32_t z;
		};
	}
}