#include <utility>
#include "utility.hpp"

constexpr inline bool ::framework::gpu::utility::is_texture_compression_bc_format(::framework::gpu::format format)
{
	switch (format)
	{
	case ::framework::gpu::format::bc1_rgb_unorm_block:
	case ::framework::gpu::format::bc1_rgb_srgb_block:
	case ::framework::gpu::format::bc1_rgba_unorm_block:
	case ::framework::gpu::format::bc1_rgba_srgb_block:
	case ::framework::gpu::format::bc2_unorm_block:
	case ::framework::gpu::format::bc2_srgb_block:
	case ::framework::gpu::format::bc3_unorm_block:
	case ::framework::gpu::format::bc3_srgb_block:
	case ::framework::gpu::format::bc4_unorm_block:
	case ::framework::gpu::format::bc4_snorm_block:
	case ::framework::gpu::format::bc5_unorm_block:
	case ::framework::gpu::format::bc5_snorm_block:
	case ::framework::gpu::format::bc6h_ufloat_block:
	case ::framework::gpu::format::bc6h_sfloat_block:
	case ::framework::gpu::format::bc7_unorm_block:
	case ::framework::gpu::format::bc7_srgb_block:
		return true;
	default:
		return false;
	}
}

constexpr inline char const * (::framework::gpu::utility::get_component_type_name)(::framework::gpu::component_type component_type)
{
	switch (component_type)
	{
	case ::framework::gpu::component_type::float16:
		return "float16_t";
	case ::framework::gpu::component_type::float32:
		return "float";
	case ::framework::gpu::component_type::float64:
		return "float64_t";
	case ::framework::gpu::component_type::sint8:
		return "int8_t";
	case ::framework::gpu::component_type::sint16:
		return "int16_t";
	case ::framework::gpu::component_type::sint32:
		return "int32_t";
	case ::framework::gpu::component_type::sint64:
		return "int64_t";
	case ::framework::gpu::component_type::uint8:
		return "uint8_t";
	case ::framework::gpu::component_type::uint16:
		return "uint16_t";
	case ::framework::gpu::component_type::uint32:
		return "uint32_t";
	case ::framework::gpu::component_type::uint64:
		return "uint64_t";
	default:
		::std::unreachable();
	}
}

constexpr char const * ::framework::gpu::utility::get_component_type_size_bits_string(::framework::gpu::component_type component_type)
{
	switch (component_type)
	{
	case ::framework::gpu::component_type::sint8:
	case ::framework::gpu::component_type::uint8:
		return "8";
	case ::framework::gpu::component_type::float16:
	case ::framework::gpu::component_type::sint16:
	case ::framework::gpu::component_type::uint16:
		return "16";
	case ::framework::gpu::component_type::float32:
	case ::framework::gpu::component_type::sint32:
	case ::framework::gpu::component_type::uint32:
		return "32";
	case ::framework::gpu::component_type::float64:
	case ::framework::gpu::component_type::sint64:
	case ::framework::gpu::component_type::uint64:
		return "64";
	default:
		::std::unreachable();
	}
}

constexpr char const * ::framework::gpu::utility::get_component_type_size_bytes_string(::framework::gpu::component_type component_type)
{
	switch (component_type)
	{
	case ::framework::gpu::component_type::sint8:
	case ::framework::gpu::component_type::uint8:
		return "1";
	case ::framework::gpu::component_type::float16:
	case ::framework::gpu::component_type::sint16:
	case ::framework::gpu::component_type::uint16:
		return "2";
	case ::framework::gpu::component_type::float32:
	case ::framework::gpu::component_type::sint32:
	case ::framework::gpu::component_type::uint32:
		return "4";
	case ::framework::gpu::component_type::float64:
	case ::framework::gpu::component_type::sint64:
	case ::framework::gpu::component_type::uint64:
		return "8";
	default:
		::std::unreachable();
	}
}

constexpr inline ::std::size_t(::framework::gpu::utility::get_component_type_size)(::framework::gpu::component_type component_type)
{
	switch (component_type)
	{
	case ::framework::gpu::component_type::float16:
		return 2;
	case ::framework::gpu::component_type::float32:
		return 4;
	case ::framework::gpu::component_type::float64:
		return 8;
	case ::framework::gpu::component_type::sint8:
		return 1;
	case ::framework::gpu::component_type::sint16:
		return 2;
	case ::framework::gpu::component_type::sint32:
		return 4;
	case ::framework::gpu::component_type::sint64:
		return 8;
	case ::framework::gpu::component_type::uint8:
		return 1;
	case ::framework::gpu::component_type::uint16:
		return 2;
	case ::framework::gpu::component_type::uint32:
		return 4;
	case ::framework::gpu::component_type::uint64:
		return 8;
	default:
		::std::unreachable();
	}
}
