#include "physics/color.hpp"
#include "physics/color_space.hpp"
#include "physics/spectrum.hpp"
#if defined(_WIN32)
#include "platform/win32/connection.hpp"
#elif defined(VK_USE_PLATFORM_XCB_KHR)
#include "platform/linux/xcb/connection.h"
#endif
#include "engine_core.h"
#include <chrono>

::framework::platform::connection_properties(::framework::engine_core::connection_properties);
::framework::application * ::framework::engine_core::application = nullptr;
::framework::platform::connection * ::framework::engine_core::connection = nullptr;
::framework::input::my_input_listner * ::framework::engine_core::input_listner = nullptr;

bool ::framework::application::is_done(void)
{
	return true;
}

void ::framework::engine_core::initialize()
{
	::framework::physics::spectra::init();
	::framework::physics::rgb_to_spectrum_table::init();
	::framework::physics::rgb_color_space::init();
#if defined(_WIN32)
	::framework::engine_core::connection = new ::framework::platform::win32::connection;
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	::framework::engine_core::connection = new ::framework::platform::linux_::xcb::connection;
#endif
	::framework::engine_core::input_listner = new ::framework::input::my_input_listner;

	::framework::engine_core::connection->get_properties(::framework::engine_core::connection_properties);

	::framework::engine_core::application->startup();
}

void ::framework::engine_core::terminate_application()
{
	::framework::engine_core::application->cleanup();
	delete ::framework::engine_core::application;
}

void ::framework::engine_core::run_application(::framework::application * application, char const * class_name)
{
	::framework::engine_core::application = application;	
	::framework::engine_core::initialize();

	::std::chrono::steady_clock::time_point t_start = ::std::chrono::high_resolution_clock::now();
	while (::framework::engine_core::application->is_done())
	{
		::std::chrono::steady_clock::time_point const t_end = ::std::chrono::high_resolution_clock::now();
		float const t_diff = ::std::chrono::duration<float>(t_end - t_start).count();
		t_start = ::std::chrono::high_resolution_clock::now();

		::framework::engine_core::connection->poll_for_event(::framework::engine_core::input_listner);
		::framework::engine_core::input_listner->begin_frame_update();
		::framework::engine_core::application->update(t_diff);
		::framework::engine_core::input_listner->end_frame_update();
	}

	::framework::engine_core::terminate_application();
}