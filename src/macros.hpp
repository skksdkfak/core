#pragma once

#include <type_traits>

#define ENUM_CLASS_FLAGS(enum_t) \
inline constexpr enum_t operator| (enum_t lhs, enum_t rhs) { return static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) | static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator& (enum_t lhs, enum_t rhs) { return static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) & static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator^ (enum_t lhs, enum_t rhs) { return static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) ^ static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator|= (enum_t & lhs, enum_t rhs) { return lhs = static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) | static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator&= (enum_t & lhs, enum_t rhs) { return lhs = static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) & static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator^= (enum_t & lhs, enum_t rhs) { return lhs = static_cast<enum_t>(static_cast<::std::underlying_type_t<enum_t>>(lhs) ^ static_cast<::std::underlying_type_t<enum_t>>(rhs)); } \
inline constexpr enum_t operator~ (enum_t e) { return static_cast<enum_t>(~static_cast<::std::underlying_type_t<enum_t>>(e)); } \
inline constexpr bool  operator! (enum_t e) { return !static_cast<::std::underlying_type_t<enum_t>>(e); }