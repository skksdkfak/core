inline ::framework::gpu::descriptor_set_layout * ::framework::algorithm::mma::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}