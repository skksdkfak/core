#include "algorithm/shuffle.hpp"
#include "gpu/utility.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>

namespace framework::local
{
	namespace
	{
		struct push_constants
		{
			::std::uint32_t size;
		};
	}
}

::framework::algorithm::shuffle::shuffle(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager) :
	scan(gpu_context, resource_manager, nullptr, ::framework::algorithm::scan::default_scan_executor_type_name, ::framework::algorithm::scan::default_scan_executor_path)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// input_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// output_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/shuffle.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}
}

::framework::algorithm::shuffle::executor::executor(::framework::algorithm::shuffle & shuffle, ::framework::gpu_context * gpu_context) :
	scan_executor(shuffle.scan, gpu_context)
{
	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_info.module = shuffle.shader_module;
	pipeline_shader_stage_create_info.name = "main";
	pipeline_shader_stage_create_info.specialization_info = nullptr;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	compute_pipeline_create_info.layout = shuffle.pipeline_layout;
	compute_pipeline_create_info.base_pipeline = nullptr;
	compute_pipeline_create_info.base_pipeline_index = -1;
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->pipeline));
}

void ::framework::algorithm::shuffle::executor::execute(::framework::algorithm::shuffle & shuffle, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t size)
{
	struct ::framework::local::push_constants const push_constants
	{
		.size = size
	};

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, shuffle.pipeline_layout, 0, 1, &descriptor_sets[0], 0, nullptr);
	command_buffer->push_constants(shuffle.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	//this->scan_executor.execute(shuffle.scan, command_buffer, descriptor_sets[1], size);
}