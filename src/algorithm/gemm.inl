inline ::framework::gpu::descriptor_set_layout * ::framework::algorithm::gemm::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::algorithm::gemm::device_parameters const ::framework::algorithm::gemm::executor::get_device_parameters(::framework::gpu_context * gpu_context, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t k, ::std::uint32_t split_k_slices, float alpha, float beta) const
{
	return ::framework::algorithm::gemm::device_parameters
	{
		.m = m,
		.n = n,
		.k = k,
		.k_slice = k / split_k_slices,
		.split_k_slices = split_k_slices,
		.split_k_slice_stride = m * n,
		.alpha = alpha,
		.beta = beta
	};
}

inline ::std::uint32_t(::framework::algorithm::gemm::executor::get_tile_m)() const noexcept
{
	return this->tile_m;
}

inline ::std::uint32_t(::framework::algorithm::gemm::executor::get_tile_n)() const noexcept
{
	return this->tile_n;
}

inline ::std::uint32_t(::framework::algorithm::gemm::executor::get_tile_k)() const noexcept
{
	return this->tile_k;
}