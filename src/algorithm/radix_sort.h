#pragma once

#include <gpu/core.hpp>

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	struct lsd_radix_sort_create_info
	{
		typename ::framework::resource::resource_manager * resource_manager;
		::std::uint32_t		maxSize;
		::std::uint32_t		maxSortBits;
		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
	};

	struct RadixSortUB
	{
		::std::uint32_t m_n;
		::std::uint32_t m_nAligned;
		::std::uint32_t m_nWGs;
		::std::uint32_t m_nBlocksPerWG;
	};

	class lsd_radix_sort
	{
	public:
		struct Data
		{
			::std::uint64_t m_maxSize;
			::std::uint32_t m_maxSortBits;
			::std::uint64_t size;
			::std::uint32_t m_sortBits;
			::framework::gpu::physical_device * physical_device;
			::framework::gpu::device * device;
			::framework::gpu::command_pool * command_pool;
			::framework::gpu::command_buffer * command_buffer;
			::framework::gpu::fence * d3d12_fence;
			::framework::gpu::shader_module * m_pStreamCountShader;
			::framework::gpu::shader_module * m_pPrefixScanShader;
			::framework::gpu::shader_module * m_pSortAndScatterShader;
			::framework::gpu::pipeline * m_pStreamCountPso;
			::framework::gpu::pipeline * m_pPrefixScanPso;
			::framework::gpu::pipeline * m_pSortAndScatterPso;
			::framework::gpu::pipeline_layout * m_pStreamCountPipelineLayout;
			::framework::gpu::pipeline_layout * m_pPrefixScanPipelineLayout;
			::framework::gpu::pipeline_layout * m_pSortAndScatterPipelineLayout;
			::framework::gpu::descriptor_set_layout * m_pStreamCountDescriptorSetLayout;
			::framework::gpu::descriptor_set_layout * m_pPrefixScanDescriptorSetLayout;
			::framework::gpu::descriptor_set_layout * m_pSortAndScatterDescriptorSetLayout;
			::framework::gpu::descriptor_pool * descriptor_pool;
			::framework::gpu::descriptor_pool * device_descriptor_pool;
			::framework::gpu::descriptor_set * m_pStreamCountDescriptorSets[2];
			::framework::gpu::descriptor_set * m_pPrefixScanDescriptorSet;
			::framework::gpu::descriptor_set * m_pSortAndScatterDescriptorSets[2];
			::framework::gpu::buffer * inout_keys_buffer;
			::framework::gpu::buffer * m_pWorkBuffer0;
			::framework::gpu::device_memory * m_pWorkBuffer0Memory;
			::framework::gpu::buffer * m_pWorkBuffer1;
			::framework::gpu::device_memory * m_pWorkBuffer1Memory;
			::framework::gpu::buffer * m_pWorkBuffer2;
			::framework::gpu::device_memory * m_pWorkBuffer2Memory;
			::framework::gpu::buffer * m_pWorkBuffer2Debug;
			::framework::gpu::device_memory * m_pWorkBuffer2DebugMemory;
			::framework::gpu::buffer * m_pConstBuffer;
			::framework::gpu::device_memory * m_pConstBufferMemory;
			::framework::gpu::buffer * m_pDispathIndirectCommandsBuffer;
			::framework::gpu::device_memory * m_pDispathIndirectCommandsBufferDeviceMemory;
			::std::uint32_t queue_family_index;
		};

		static void allocate(::framework::algorithm::lsd_radix_sort_create_info const & create_info, Data ** data);

		static void deallocate(Data * pData);

		static void configure(Data * data, ::framework::gpu::buffer * pInoutKeysBuffer, ::framework::gpu::buffer * pInoutValuesBuffer, ::std::uint32_t sortBits = 32);

		static void setSize(Data * data, ::std::uint64_t size);

		static void execute(Data * data, ::framework::gpu::queue * command_queue, ::framework::gpu::semaphore * wait_semaphore, ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore, ::framework::gpu::fence * fence);

	private:
		enum : ::std::uint32_t
		{
			WG_SIZE = 64,
			ELEMENTS_PER_WORK_ITEM = 256 / WG_SIZE,
			BITS_PER_PASS = 4,
			//const NUM_WGS = 20 * 6, // cypress
			//const NUM_WGS = 24 * 6, // cayman
			NUM_WGS = 1024 // nv
		};
	};
}