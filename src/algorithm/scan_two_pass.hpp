#pragma once

#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class scan_two_pass
	{
	public:
		class executor;

		scan_two_pass(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * local_scan_shader_module;
		::framework::gpu::shader_module * top_level_scan_shader_module;
		::framework::gpu::shader_module * add_offset_shader_module;
	};

	class ::framework::algorithm::scan_two_pass::executor
	{
	public:
		executor(::framework::algorithm::scan_two_pass & scan_two_pass, ::framework::gpu_context * gpu_context);

		void execute(::framework::algorithm::scan_two_pass & scan_two_pass, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t size);

	private:
		::framework::gpu::pipeline * local_scan_pipeline;
		::framework::gpu::pipeline * top_level_scan_pipeline;
		::framework::gpu::pipeline * add_offset_pipeline;
	};
}

#include "algorithm/scan_two_pass.inl"