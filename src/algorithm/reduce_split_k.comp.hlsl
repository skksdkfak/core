static const int gl_CooperativeMatrixLayoutRowMajor = 0;
static const int gl_CooperativeMatrixLayoutColumnMajor = 1;

struct device_parameters_t
{
	uint32_t m;
	uint32_t n;
	uint32_t k;
	uint32_t k_slice;
	uint32_t split_k_slices;
	uint32_t split_k_slice_stride;
	float alpha;
	float beta;
};

[[vk::constant_id(0)]] const int layout_c = 0;
[[vk::constant_id(1)]] const int layout_d = 0;

[[vk::binding(0, 0)]] RWStructuredBuffer<uint4> matrix_a_buffer : register(u0, space0);
[[vk::binding(1, 0)]] RWStructuredBuffer<uint4> matrix_b_buffer : register(u1, space0);
[[vk::binding(2, 0)]] RWStructuredBuffer<C_TYPE> matrix_c_buffer : register(u2, space0);
[[vk::binding(3, 0)]] RWStructuredBuffer<D_TYPE> matrix_d_buffer : register(u3, space0);
[[vk::binding(4, 0)]] RWStructuredBuffer<ACCUMULATOR_TYPE> matrix_d_k_slices_buffer : register(u4, space0);
[[vk::binding(5, 0)]] ConstantBuffer<device_parameters_t> device_parameters_buffer : register(b0, space0);

uint coord_to_offset(uint i, uint j, uint stride, int matrix_layout)
{
	return matrix_layout == gl_CooperativeMatrixLayoutColumnMajor ? (stride * j + i) : (stride * i + j);
}

#include "epilogue"

[numthreads(16, 16, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint32_t stride_d = layout_d == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.m : device_parameters_buffer.n;
	const uint32_t gi = dispatch_thread_id.y;
	const uint32_t gj = dispatch_thread_id.x;

	ACCUMULATOR_TYPE accumulator = ACCUMULATOR_TYPE(0.0);
	uint chunk_offset = 0;
	for (uint i = 0, chunk_offset = 0; i < device_parameters_buffer.split_k_slices; i++, chunk_offset += device_parameters_buffer.split_k_slice_stride)
	{
		accumulator += matrix_d_k_slices_buffer[chunk_offset + ::coord_to_offset(gi, gj, device_parameters_buffer.n, gl_CooperativeMatrixLayoutRowMajor)];
	}
	const D_TYPE d = ::gemm_epilogue(accumulator, gi, gj);
	matrix_d_buffer[::coord_to_offset(gi, gj, stride_d, layout_d)] = d;
}
