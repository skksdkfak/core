#include "algorithm/mma.hpp"
#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <vector>

::framework::algorithm::mma::mma(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type a_type, ::framework::gpu::component_type b_type, ::framework::gpu::component_type c_type, ::framework::gpu::component_type d_type)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[5];
		// A
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// B
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// C
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// D
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// D intermediate
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "algorithm/mma.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::preprocessor_define preprocessor_defines[8];
		char const * const type_names[] { "A_TYPE", "B_TYPE", "C_TYPE", "D_TYPE" };
		char const * const bits_names[] { "A_BITS", "B_BITS", "C_BITS", "D_BITS" };
		auto set_type_preprocessor_define = [&](::std::size_t define_offset, ::std::size_t index, ::framework::gpu::component_type component_type)
		{
			switch (component_type)
			{
			case ::framework::gpu::component_type::float16:
				preprocessor_defines[define_offset + 0] = { type_names[index], "float16_t" };
				preprocessor_defines[define_offset + 1] = { bits_names[index], "16" };
				break;
			case ::framework::gpu::component_type::float32:
				preprocessor_defines[define_offset + 0] = { type_names[index], "float" };
				preprocessor_defines[define_offset + 1] = { bits_names[index], "32" };
				break;
			default:
				assert(!"Error: Unsupported a_type.");
				break;
			}
		};
		set_type_preprocessor_define(0, 0, a_type);
		set_type_preprocessor_define(2, 1, b_type);
		set_type_preprocessor_define(4, 2, c_type);
		set_type_preprocessor_define(6, 3, d_type);

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}
}

::framework::algorithm::mma::executor::executor(::framework::algorithm::mma & mma, ::framework::gpu_context * gpu_context, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t k, ::framework::algorithm::mma::matrix_layout matrix_layout_a, ::framework::algorithm::mma::matrix_layout matrix_layout_b, ::framework::algorithm::mma::matrix_layout matrix_layout_c, ::framework::algorithm::mma::matrix_layout matrix_layout_d, float alpha, float beta) :
	matrix_layout_a(matrix_layout_a), matrix_layout_b(matrix_layout_b), matrix_layout_c(matrix_layout_c), matrix_layout_d(matrix_layout_d)
{
	assert(m % 16 == 0);
	assert(n % 16 == 0);
	assert(k % 16 == 0);

	::std::uint32_t property_count;
	assert_framework_gpu_result(gpu_context->get_physical_device()->get_cooperative_matrix_properties(&property_count, nullptr));
	::std::vector<::framework::gpu::cooperative_matrix_properties> cooperative_matrix_properties(property_count);
	assert_framework_gpu_result(gpu_context->get_physical_device()->get_cooperative_matrix_properties(&property_count, cooperative_matrix_properties.data()));

	this->tile_m = ::std::min<::std::uint32_t>(m, 128ul);
	this->tile_n = ::std::min<::std::uint32_t>(n, 128ul);
	this->tile_k = cooperative_matrix_properties[0].k_size;
	this->m = (m + this->tile_m - 1) / this->tile_m * this->tile_m;
	this->n = (n + this->tile_n - 1) / this->tile_n * this->tile_n;
	this->k = (k + this->tile_k - 1) / this->tile_k * this->tile_k;
	this->l_m = cooperative_matrix_properties[0].m_size;
	this->l_n = cooperative_matrix_properties[0].n_size;
	this->l_k = cooperative_matrix_properties[0].k_size;
	this->split_k_slices = this->k;

	::std::uint32_t const workgroup_width_in_subgroups = this->tile_m == 128 && this->tile_n == 128 ? 4 : 1;
	::std::uint32_t const workgroup_height_in_subgroups = this->tile_m == 128 && this->tile_n == 128 ? 2 : 1;
	::std::uint32_t const num_subgroups = workgroup_width_in_subgroups * workgroup_height_in_subgroups;
	::std::uint32_t const invocations_per_workgroup = 32 * num_subgroups;

	::framework::gpu::specialization_map_entry specialization_map_entries[]
	{
		{0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t)},
		{1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t)},
		{2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t)},
		{3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t)},
		{4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t)},
		{5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t)},
		{6, sizeof(::std::uint32_t) * 6, sizeof(::std::uint32_t)},
		{7, sizeof(::std::uint32_t) * 7, sizeof(::std::uint32_t)},
		{8, sizeof(::std::uint32_t) * 8, sizeof(::std::uint32_t)},
		{9, sizeof(::std::uint32_t) * 9, sizeof(::std::uint32_t)},
		{10, sizeof(::std::uint32_t) * 10, sizeof(::std::uint32_t)},
		{11, sizeof(::std::uint32_t) * 11, sizeof(::std::uint32_t)},
		{12, sizeof(::std::uint32_t) * 12, sizeof(::std::uint32_t)},
		{13, sizeof(::std::uint32_t) * 13, sizeof(::std::uint32_t)},
		{14, sizeof(::std::uint32_t) * 14, sizeof(::std::uint32_t)},
		{15, sizeof(::std::uint32_t) * 15, sizeof(::std::uint32_t)},
		{16, sizeof(::std::uint32_t) * 16, sizeof(::std::uint32_t)},
		{17, sizeof(::std::uint32_t) * 17, sizeof(::std::uint32_t)},
		{18, sizeof(::std::uint32_t) * 18, sizeof(::std::uint32_t)},
		{19, sizeof(::std::uint32_t) * 19, sizeof(::std::uint32_t)},
		{20, sizeof(::std::uint32_t) * 20, sizeof(::std::uint32_t)},
		{21, sizeof(::std::uint32_t) * 21, sizeof(::std::uint32_t)},
		{22, sizeof(::std::uint32_t) * 22, sizeof(::std::uint32_t)},
		{23, sizeof(::std::uint32_t) * 23, sizeof(::std::uint32_t)},
		{24, sizeof(::std::uint32_t) * 24, sizeof(::std::uint32_t)},
		{25, sizeof(::std::uint32_t) * 25, sizeof(::std::uint32_t)}
	};

	::std::uint32_t specialization_data[]
	{
		this->l_m, // l_m
		this->l_n, // l_n
		this->l_k, // l_k
		this->tile_m, // tile_m
		this->tile_n, // tile_n
		this->tile_k, // tile_k
		this->m * this->n, // m_n
		this->k, // K
		this->matrix_layout_a == ::framework::algorithm::mma::matrix_layout::column_major ? this->m : this->k, // stride_a
		this->matrix_layout_b == ::framework::algorithm::mma::matrix_layout::column_major ? this->k : this->n, // stride_b
		this->matrix_layout_c == ::framework::algorithm::mma::matrix_layout::column_major ? this->m : this->n, // stride_c
		this->matrix_layout_d == ::framework::algorithm::mma::matrix_layout::column_major ? this->m : this->n, // stride_d
		*(::std::uint32_t *)&alpha, // alpha
		*(::std::uint32_t *)&beta, // beta
		static_cast<::std::uint32_t>(this->matrix_layout_a), // layout_a
		static_cast<::std::uint32_t>(this->matrix_layout_b), // layout_b
		static_cast<::std::uint32_t>(this->matrix_layout_c), // layout_c
		static_cast<::std::uint32_t>(this->matrix_layout_d), // layout_d
		this->matrix_layout_a == ::framework::algorithm::mma::matrix_layout::column_major ? this->tile_m : this->tile_k, // a_tile_row_len
		this->matrix_layout_a == ::framework::algorithm::mma::matrix_layout::column_major ? this->tile_k : this->tile_m, // a_tile_row_count
		this->matrix_layout_b == ::framework::algorithm::mma::matrix_layout::column_major ? this->tile_k : this->tile_n, // b_tile_row_len
		this->matrix_layout_b == ::framework::algorithm::mma::matrix_layout::column_major ? this->tile_n : this->tile_k, // b_tile_row_count
		workgroup_width_in_subgroups, // workgroup_width_in_subgroups
		workgroup_height_in_subgroups, // workgroup_height_in_subgroups
		invocations_per_workgroup, // invocations_per_workgroup
		this->split_k_slices // split_k_slices
	};

	::framework::gpu::specialization_info specialization_info;
	specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_info.map_entries = specialization_map_entries;
	specialization_info.data_size = sizeof(specialization_data);
	specialization_info.data = specialization_data;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_info.module = mma.shader_module;
	pipeline_shader_stage_create_info.name = "main";
	pipeline_shader_stage_create_info.specialization_info = &specialization_info;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	compute_pipeline_create_info.layout = mma.pipeline_layout;
	compute_pipeline_create_info.base_pipeline = nullptr;
	compute_pipeline_create_info.base_pipeline_index = -1;
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->pipeline));
}

void ::framework::algorithm::mma::executor::execute(::framework::algorithm::mma & mma, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, mma.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(this->m / this->l_m, this->n / this->l_n, this->k / this->split_k_slices);
}