#pragma once

#include "algorithm/scan.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class shuffle
	{
	public:
		class executor;

		shuffle(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		::framework::algorithm::scan const & get_scan() const noexcept;

	private:
		::framework::algorithm::scan scan;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
	};

	class ::framework::algorithm::shuffle::executor
	{
	public:
		executor(::framework::algorithm::shuffle & shuffle, ::framework::gpu_context * gpu_context);

		void execute(::framework::algorithm::shuffle & shuffle, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * const * descriptor_sets, ::std::uint32_t size);

		::framework::algorithm::scan::executor const & get_scan_executor() const noexcept;

	private:
		::framework::algorithm::scan::executor scan_executor;
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::pipeline * gather_pipeline;
	};
}

#include "algorithm/shuffle.inl"