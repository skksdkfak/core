inline D_TYPE gemm_epilogue(ACCUMULATOR_TYPE accumulator, uint gi, uint gj)
{
	const uint32_t stride_c = layout_c == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.m : device_parameters_buffer.n;
	const ACCUMULATOR_TYPE c = device_parameters_buffer.beta == 0.0f ? ACCUMULATOR_TYPE(0.0) : ACCUMULATOR_TYPE(device_parameters_buffer.beta) * ACCUMULATOR_TYPE(matrix_c_buffer[::coord_to_offset(gi, gj, stride_c, layout_c)]);
	return D_TYPE(mad(ACCUMULATOR_TYPE(device_parameters_buffer.alpha), accumulator, c));
}