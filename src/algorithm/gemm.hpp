#pragma once

#include "gpu/core.hpp"
#include "linalg.hpp"
#include "resource/shader_compiler.hpp"
#include <cstdint>
#include <filesystem>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class gemm
	{
	public:
		static char constexpr linear_combination_gemm_epilogue_hlsl_path[] = "linear_combination_gemm_epilogue.hlsl";
		static char constexpr linear_combination_gemm_epilogue_glsl_path[] = "linear_combination_gemm_epilogue.glsl";

		class executor;
		class reduce_split_k_executor;

		struct device_parameters
		{
			::std::uint32_t m;
			::std::uint32_t n;
			::std::uint32_t k;
			::std::uint32_t k_slice;
			::std::uint32_t split_k_slices;
			::std::uint32_t split_k_slice_stride;
			float alpha;
			float beta;
		};

		gemm() = default;

		gemm(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type a_type, ::framework::gpu::component_type b_type, ::framework::gpu::component_type c_type, ::framework::gpu::component_type d_type, ::framework::gpu::component_type acc_type, ::std::uint32_t user_preprocessor_define_count, ::framework::resource::preprocessor_define const * user_preprocessor_defines, ::std::filesystem::path const & epilogue_hlsl_path, ::std::filesystem::path const & epilogue_glsl_path);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
		::framework::gpu::shader_module * reduce_split_k_shader_module;
	};

	class ::framework::algorithm::gemm::executor
	{
	public:
		executor() = default;

		executor(::framework::algorithm::gemm const & gemm, ::framework::gpu_context * gpu_context, ::std::uint32_t offset_m, ::std::uint32_t offset_n, ::std::uint32_t tile_m, ::std::uint32_t tile_n, ::std::uint32_t tile_k, bool split_k, ::framework::linalg::matrix_layout matrix_layout_a, ::framework::linalg::matrix_layout matrix_layout_b, ::framework::linalg::matrix_layout matrix_layout_c, ::framework::linalg::matrix_layout matrix_layout_d);

		void execute(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n) const;
		
		void execute_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset) const;
		
		void execute_split_k(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t split_k_slices) const;
		
		void execute_split_k_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) const;

		::framework::algorithm::gemm::device_parameters const get_device_parameters(::framework::gpu_context * gpu_context, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t k, ::std::uint32_t split_k_slices, float alpha, float beta) const;

		::std::uint32_t get_tile_m() const noexcept;

		::std::uint32_t get_tile_n() const noexcept;

		::std::uint32_t get_tile_k() const noexcept;

	private:
		::framework::gpu::pipeline * pipeline;
		::std::uint32_t tile_m;
		::std::uint32_t tile_n;
		::std::uint32_t tile_k;
	};

	class ::framework::algorithm::gemm::reduce_split_k_executor
	{
	public:
		reduce_split_k_executor() = default;

		reduce_split_k_executor(::framework::algorithm::gemm const & gemm, ::framework::gpu_context * gpu_context, ::framework::linalg::matrix_layout matrix_layout_c, ::framework::linalg::matrix_layout matrix_layout_d);

		void execute_reduce_split_k(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n) const;

		void execute_reduce_split_k_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) const;

	private:
		::framework::gpu::pipeline * reduce_split_k_pipeline;
	};
}

#include "algorithm/gemm.inl"