#include "algorithm/gemm.hpp"
#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <numeric>
#include <cassert>
#include <cstdint>
#include <vector>

::framework::algorithm::gemm::gemm(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type a_type, ::framework::gpu::component_type b_type, ::framework::gpu::component_type c_type, ::framework::gpu::component_type d_type, ::framework::gpu::component_type acc_type, ::std::uint32_t user_preprocessor_define_count, ::framework::resource::preprocessor_define const * user_preprocessor_defines, ::std::filesystem::path const & epilogue_hlsl_path, ::std::filesystem::path const & epilogue_glsl_path)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// matrix_a_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// matrix_b_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// matrix_c_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// matrix_d_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// matrix_d_k_slices_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// device_parameters_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}
	
	::framework::gpu::component_type const matrices_types[]{ a_type, b_type, c_type, d_type, acc_type };
	char const * const type_names[]{ "A_TYPE", "B_TYPE", "C_TYPE", "D_TYPE", "ACCUMULATOR_TYPE" };
	char const * const bits_names[]{ "A_BITS", "B_BITS", "C_BITS", "D_BITS", "ACC_BITS" };

	::std::vector<::framework::resource::preprocessor_define> preprocessor_defines(::std::size(matrices_types) * 2 + user_preprocessor_define_count);
	for (::std::size_t i = 0; i < ::std::size(matrices_types); i++)
	{
		preprocessor_defines[i * 2 + 0] = { type_names[i], ::framework::gpu::utility::get_component_type_name(matrices_types[i])};
		preprocessor_defines[i * 2 + 1] = { bits_names[i], ::framework::gpu::utility::get_component_type_size_bits_string(matrices_types[i]) };
	}

	if (user_preprocessor_define_count > 0)
	{
		::std::copy(user_preprocessor_defines, user_preprocessor_defines + user_preprocessor_define_count, &preprocessor_defines[::std::size(matrices_types) * 2]);
	}

	{
		::framework::resource::include_override const include_overrides[]
		{
			{ "epilogue", epilogue_glsl_path }
		};

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "algorithm/gemm.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines.data();
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::include_override const include_overrides[]
		{
			{ "epilogue", epilogue_hlsl_path }
		};

		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/reduce_split_k.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines.data();
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->reduce_split_k_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}
}

::framework::algorithm::gemm::executor::executor(::framework::algorithm::gemm const & gemm, ::framework::gpu_context * gpu_context, ::std::uint32_t offset_m, ::std::uint32_t offset_n, ::std::uint32_t tile_m, ::std::uint32_t tile_n, ::std::uint32_t tile_k, bool split_k, ::framework::linalg::matrix_layout matrix_layout_a, ::framework::linalg::matrix_layout matrix_layout_b, ::framework::linalg::matrix_layout matrix_layout_c, ::framework::linalg::matrix_layout matrix_layout_d) :
	tile_m(tile_m), tile_n(tile_n), tile_k(tile_k)
{
	assert(tile_m && (tile_m & (tile_m - 1)) == 0);
	assert(tile_n && (tile_n & (tile_n - 1)) == 0);
	assert(tile_k && (tile_k & (tile_k - 1)) == 0);

	::std::uint32_t property_count;
	assert_framework_gpu_result(gpu_context->get_physical_device()->get_cooperative_matrix_properties(&property_count, nullptr));
	::std::vector<::framework::gpu::cooperative_matrix_properties> cooperative_matrix_properties(property_count);
	assert_framework_gpu_result(gpu_context->get_physical_device()->get_cooperative_matrix_properties(&property_count, cooperative_matrix_properties.data()));

	// Each cooperative matrix multiply is lMxlNxlK
	::std::uint32_t const l_m = cooperative_matrix_properties[0].m_size;
	::std::uint32_t const l_n = cooperative_matrix_properties[0].n_size;
	::std::uint32_t const l_k = cooperative_matrix_properties[0].k_size;

	::std::uint32_t constexpr workgroup_size_in_subgroups[][2]
	{
		{1, 1}, {2, 1}, {2, 2}, {4, 2}, {4, 4}
	};

	::std::uint32_t const workgroup_in_subgroups_index = ::std::min(32u - ::std::countl_zero(::std::gcd(this->tile_m, this->tile_n)) - 5u, 4u);
	::std::uint32_t const workgroup_width_in_subgroups = workgroup_size_in_subgroups[workgroup_in_subgroups_index][0];
	::std::uint32_t const workgroup_height_in_subgroups = workgroup_size_in_subgroups[workgroup_in_subgroups_index][1];
	::std::uint32_t const num_subgroups = workgroup_width_in_subgroups * workgroup_height_in_subgroups;
	::std::uint32_t const invocations_per_workgroup = 32 * num_subgroups;

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t)},
		{1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t)},
		{2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t)},
		{3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t)},
		{4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t)},
		{5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t)},
		{6, sizeof(::std::uint32_t) * 6, sizeof(::std::uint32_t)},
		{7, sizeof(::std::uint32_t) * 7, sizeof(::std::uint32_t)},
		{8, sizeof(::std::uint32_t) * 8, sizeof(::std::uint32_t)},
		{9, sizeof(::std::uint32_t) * 9, sizeof(::std::uint32_t)},
		{10, sizeof(::std::uint32_t) * 10, sizeof(::std::uint32_t)},
		{11, sizeof(::std::uint32_t) * 11, sizeof(::std::uint32_t)},
		{12, sizeof(::std::uint32_t) * 12, sizeof(::std::uint32_t)},
		{13, sizeof(::std::uint32_t) * 13, sizeof(::std::uint32_t)},
		{14, sizeof(::std::uint32_t) * 14, sizeof(::std::uint32_t)},
		{15, sizeof(::std::uint32_t) * 15, sizeof(::std::uint32_t)}
	};

	::std::uint32_t const specialization_data[]
	{
		offset_m,
		offset_n,
		l_m, // l_m
		l_n, // l_n
		l_k, // l_k
		this->tile_m, // tile_m
		this->tile_n, // tile_n
		this->tile_k, // tile_k
		static_cast<::std::uint32_t>(matrix_layout_a), // layout_a
		static_cast<::std::uint32_t>(matrix_layout_b), // layout_b
		static_cast<::std::uint32_t>(matrix_layout_c), // layout_c
		static_cast<::std::uint32_t>(matrix_layout_d), // layout_d
		workgroup_width_in_subgroups, // workgroup_width_in_subgroups
		workgroup_height_in_subgroups, // workgroup_height_in_subgroups
		invocations_per_workgroup, // invocations_per_workgroup
		static_cast<::framework::gpu::bool32_t>(split_k) // split_k
	};

	::framework::gpu::specialization_info specialization_info;
	specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_info.map_entries = specialization_map_entries;
	specialization_info.data_size = sizeof(specialization_data);
	specialization_info.data = specialization_data;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_info.module = gemm.shader_module;
	pipeline_shader_stage_create_info.name = "main";
	pipeline_shader_stage_create_info.specialization_info = &specialization_info;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	compute_pipeline_create_info.layout = gemm.pipeline_layout;
	compute_pipeline_create_info.base_pipeline = nullptr;
	compute_pipeline_create_info.base_pipeline_index = -1;
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->pipeline));
}

void ::framework::algorithm::gemm::executor::execute(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(n / this->tile_n, m / this->tile_m, 1);
}

void ::framework::algorithm::gemm::executor::execute_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * buffer, ::framework::gpu::device_size offset) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch_indirect(buffer, offset);
}

void ::framework::algorithm::gemm::executor::execute_split_k(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t split_k_slices) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(n / this->tile_n, m / this->tile_m, split_k_slices);
}

void ::framework::algorithm::gemm::executor::execute_split_k_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}

::framework::algorithm::gemm::reduce_split_k_executor::reduce_split_k_executor(::framework::algorithm::gemm const & gemm, ::framework::gpu_context * gpu_context, ::framework::linalg::matrix_layout matrix_layout_c, ::framework::linalg::matrix_layout matrix_layout_d)
{
	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t)},
		{1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t)}
	};

	::std::uint32_t const specialization_data[]
	{
		static_cast<::std::uint32_t>(matrix_layout_c), // layout_c
		static_cast<::std::uint32_t>(matrix_layout_d), // layout_d
	};

	::framework::gpu::specialization_info specialization_info;
	specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_info.map_entries = specialization_map_entries;
	specialization_info.data_size = sizeof(specialization_data);
	specialization_info.data = specialization_data;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
	pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_info.module = gemm.reduce_split_k_shader_module;
	pipeline_shader_stage_create_info.name = "main";
	pipeline_shader_stage_create_info.specialization_info = &specialization_info;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
	compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
	compute_pipeline_create_info.layout = gemm.pipeline_layout;
	compute_pipeline_create_info.base_pipeline = nullptr;
	compute_pipeline_create_info.base_pipeline_index = -1;
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->reduce_split_k_pipeline));
}

void ::framework::algorithm::gemm::reduce_split_k_executor::execute_reduce_split_k(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t m, ::std::uint32_t n) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->reduce_split_k_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(n / 16, m / 16, 1);
}

void ::framework::algorithm::gemm::reduce_split_k_executor::execute_reduce_split_k_indirect(::framework::algorithm::gemm const & gemm, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) const
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->reduce_split_k_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, gemm.pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}