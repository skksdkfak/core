#version 450 core

#pragma use_vulkan_memory_model

#extension GL_ARB_shading_language_include : enable
#extension GL_KHR_shader_subgroup_basic : enable
#extension GL_EXT_scalar_block_layout : enable
#extension GL_KHR_memory_scope_semantics : enable
#extension GL_KHR_cooperative_matrix : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float32 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int8 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int32 : enable
#extension GL_EXT_control_flow_attributes : enable

layout(constant_id = 0) const uint offset_m = 0;
layout(constant_id = 1) const uint offset_n = 0;
layout(constant_id = 2) const uint l_m = 1;
layout(constant_id = 3) const uint l_n = 1;
layout(constant_id = 4) const uint l_k = 1;
layout(constant_id = 5) const uint tile_m = 1;
layout(constant_id = 6) const uint tile_n = 1;
layout(constant_id = 7) const uint tile_k = 1;
layout(constant_id = 8) const int layout_a = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 9) const int layout_b = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 10) const int layout_c = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 11) const int layout_d = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 12) const uint workgroup_width_in_subgroups = 1;
layout(constant_id = 13) const uint workgroup_height_in_subgroups = 1;
layout(constant_id = 14) const uint invocations_per_workgroup = 1;
layout(constant_id = 15) const bool split_k = false;

layout(std430, set = 0, binding = 0) buffer matrix_a_buffer_t
{
    uvec4 matrix_a_buffer[];
};

layout(std430, set = 0, binding = 1) buffer matrix_b_buffer_t
{
    uvec4 matrix_b_buffer[];
};

layout(std430, set = 0, binding = 2) buffer matrix_c_buffer_t
{
    C_TYPE matrix_c_buffer[];
};

layout(std430, set = 0, binding = 3) buffer matrix_d_buffer_t
{
    D_TYPE matrix_d_buffer[];
};

layout(std430, set = 0, binding = 4) buffer matrix_d_k_slices_buffer_t
{
    ACCUMULATOR_TYPE matrix_d_k_slices_buffer[];
};

layout(set = 0, binding = 5) uniform device_parameters_buffer_t
{
	uint32_t m;
	uint32_t n;
	uint32_t k;
	uint32_t k_slice;
	uint32_t split_k_slices;
	uint32_t split_k_slice_stride;
	float alpha;
	float beta;
} device_parameters_buffer;

const uint elements_per_vec4 = 16 / (A_BITS / 8); // 16 bytes, A_BITS bits per element
const uint row_pad_sh = elements_per_vec4;
const uint coop_mat_rows = tile_m / workgroup_height_in_subgroups / l_m;
const uint coop_mat_cols = tile_n / workgroup_width_in_subgroups / l_n;
const uint a_tile_row_len = layout_a == gl_CooperativeMatrixLayoutColumnMajor ? tile_m : tile_k;
const uint a_tile_row_count = layout_a == gl_CooperativeMatrixLayoutColumnMajor ? tile_k : tile_m;
const uint b_tile_row_len = layout_b == gl_CooperativeMatrixLayoutColumnMajor ? tile_k : tile_n;
const uint b_tile_row_count = layout_b == gl_CooperativeMatrixLayoutColumnMajor ? tile_n : tile_k;

// Shared memory storage. Add a skew of row_pad_sh bytes per row to avoid bank conflicts when accessing the shared memory
shared uvec4 matrix_a_shared[a_tile_row_count * (a_tile_row_len + row_pad_sh) / elements_per_vec4];
shared uvec4 matrix_b_shared[b_tile_row_count * (b_tile_row_len + row_pad_sh) / elements_per_vec4];

layout(local_size_x_id = 14, local_size_y = 1, local_size_z = 1) in;

uint coord_to_offset(uint i, uint j, uint stride, int matrix_layout)
{
    return matrix_layout == gl_CooperativeMatrixLayoutColumnMajor ? (stride * j + i) : (stride * i + j);
}

#include "epilogue"

void main()
{
    // compute position in grid
    uvec3 tile_id = gl_WorkGroupID;
    uvec2 warp_in_tile = uvec2(gl_SubgroupID % workgroup_width_in_subgroups, gl_SubgroupID / workgroup_width_in_subgroups);

    coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> result[coop_mat_rows][coop_mat_cols];

    // Initialize result to zero
    [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
    {
        [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
        {
            result[i][j] = coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>(0.0);
        }
    }

    const uint32_t stride_a = layout_a == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.m : device_parameters_buffer.k;
    const uint32_t stride_b = layout_b == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.k : device_parameters_buffer.n;
    const uint32_t stride_c = layout_c == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.m : device_parameters_buffer.n;
    const uint32_t stride_d = layout_d == gl_CooperativeMatrixLayoutColumnMajor ? device_parameters_buffer.m : device_parameters_buffer.n;
    
    uint chunk_k = tile_id.z * device_parameters_buffer.k_slice;

    // fetch A for the first iteration;
    // We want to perform as many coalesced loads as possible,
    // by placing one invocation after another to consecutively load uevc4 entries in each tile's row.
    // A single invocation is responsible for loading one uevc4 entry per iteration.
    // As we may not have enough invocations in the workgroup to load whole tile at once,
    // we repeating the process until we end up loading the whole tile.
    // Calculate the maximum number of invocations we can fit to load one row.
    const uint a_tile_invocations_per_row = a_tile_row_len / elements_per_vec4;
    const uint a_tile_rows_per_workgroup = invocations_per_workgroup / a_tile_invocations_per_row;

    uvec4 temp_a[a_tile_row_count / a_tile_rows_per_workgroup];
    const uint a_tile_k = gl_LocalInvocationID.x % a_tile_invocations_per_row;
    const uint g_a_base = coord_to_offset(offset_m + tile_m * tile_id.y, chunk_k, stride_a, layout_a);
    [[unroll]] for (uint i = 0; i < a_tile_row_count; i += a_tile_rows_per_workgroup)
    {
        const uint a_tile_i = i + gl_LocalInvocationID.x / a_tile_invocations_per_row;
        temp_a[i / a_tile_rows_per_workgroup] = matrix_a_buffer[(g_a_base + stride_a * a_tile_i) / elements_per_vec4 + a_tile_k];
    }

    // fetch B for the first iteration
    const uint b_tile_invocations_per_row = b_tile_row_len / elements_per_vec4;
    const uint b_tile_rows_per_workgroup = invocations_per_workgroup / b_tile_invocations_per_row;

    uvec4 temp_b[b_tile_row_count / b_tile_rows_per_workgroup];
    const uint b_tile_j = gl_LocalInvocationID.x % b_tile_invocations_per_row;
    // g_b_base is the anchor of this tile in global memory. It's computed from the
    // (k,j) coordinate based on whether the tile is column major. Within this tile,
    // the global->shared copy always works in terms of contiguous "rows" of memory.
    // So the addressing within a tile is not affected by layout_b.
    const uint g_b_base = coord_to_offset(chunk_k, offset_n + tile_n * tile_id.x, stride_b, layout_b);
    [[unroll]] for (uint k = 0; k < b_tile_row_count; k += b_tile_rows_per_workgroup)
    {
        const uint b_tile_k = k + gl_LocalInvocationID.x / b_tile_invocations_per_row;
        temp_b[k / b_tile_rows_per_workgroup] = matrix_b_buffer[(g_b_base + stride_b * b_tile_k) / elements_per_vec4 + b_tile_j];
    }

    const uint stride_a_sh = a_tile_row_len + row_pad_sh;
    const uint stride_b_sh = b_tile_row_len + row_pad_sh;

    // Iterate over K.
    // On each iteration, the workgroup cooperates to memcpy a row of cooperative
    // matrices from matrix A into matrix_a_shared and a column of cooperative matrices from
    // matrix B into matrix_b_shared. Then each subgroup loads the subset of those matrices
    // that it needs out of shared memory, and multiplies pairs of cooperative
    // matrices.
    for (; chunk_k < (tile_id.z + 1) * device_parameters_buffer.k_slice - tile_k; chunk_k += tile_k)
    {
        // ensure that all threads in the subgroup finished reading from SMEM during the last iteration
        barrier();

        // store A from local storage to shared memory
        [[unroll]] for (uint i = 0; i < a_tile_row_count; i += a_tile_rows_per_workgroup)
        {
            uint si = i + gl_LocalInvocationID.x / a_tile_invocations_per_row;
            matrix_a_shared[(stride_a_sh * si) / elements_per_vec4 + a_tile_k] = temp_a[i / a_tile_rows_per_workgroup];
        }

        // store B from local storage to shared memory
        [[unroll]] for (uint k = 0; k < b_tile_row_count; k += b_tile_rows_per_workgroup)
        {
            uint sk = k + gl_LocalInvocationID.x / b_tile_invocations_per_row;
            matrix_b_shared[(stride_b_sh * sk) / elements_per_vec4 + b_tile_j] = temp_b[k / b_tile_rows_per_workgroup];
        }

        // wait until all threads finished writing to shared memory before the math loop
        // Do this before fetching data for the next iteration so that the barrier does not
        // wait for the loads from global storage to be finished
        barrier();

        // we prefetch data from global memory as soon as possible to hide memory transfers
        // behind math
        // prefetch A
        const uint g_a_base = coord_to_offset(offset_m + tile_m * tile_id.y, chunk_k + tile_k, stride_a, layout_a);
        [[unroll]] for (uint i = 0; i < a_tile_row_count; i += a_tile_rows_per_workgroup)
        {
            uint a_tile_i = i + gl_LocalInvocationID.x / a_tile_invocations_per_row;
            temp_a[i / a_tile_rows_per_workgroup] = matrix_a_buffer[(g_a_base + stride_a * a_tile_i) / elements_per_vec4 + a_tile_k];
        }

        // prefetch B
        const uint g_b_base = coord_to_offset(chunk_k + tile_k, offset_n + tile_n * tile_id.x, stride_b, layout_b);
        [[unroll]] for (uint k = 0; k < b_tile_row_count; k += b_tile_rows_per_workgroup)
        {
            uint b_tile_k = k + gl_LocalInvocationID.x / b_tile_invocations_per_row;
            temp_b[k / b_tile_rows_per_workgroup] = matrix_b_buffer[(g_b_base + stride_b * b_tile_k) / elements_per_vec4 + b_tile_j];
        }

        // The actual math loop
        [[unroll]] for (uint sk = 0; sk < tile_k; sk += l_k)
        {
            // load A. A will be reused coop_mat_cols times
            coopmat<A_TYPE, gl_ScopeSubgroup, l_m, l_k, gl_MatrixUseA> mat_a[coop_mat_rows];
            [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
            {
                uint si = l_m * (coop_mat_rows * warp_in_tile.y + i);
                coopMatLoad(mat_a[i], matrix_a_shared, coord_to_offset(si, sk, stride_a_sh, layout_a) / elements_per_vec4, stride_a_sh / elements_per_vec4, layout_a);
            }

            coopmat<B_TYPE, gl_ScopeSubgroup, l_k, l_n, gl_MatrixUseB> mat_b;
            [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
            {
                uint sj = l_n * (coop_mat_cols * warp_in_tile.x + j);
                // load B
                coopMatLoad(mat_b, matrix_b_shared, coord_to_offset(sk, sj, stride_b_sh, layout_b) / elements_per_vec4, stride_b_sh / elements_per_vec4, layout_b);

                // do the matrix multiply for the current portion of the tile
                [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
                {
                    result[i][j] = coopMatMulAdd(mat_a[i], mat_b, result[i][j]);
                }
            }
        }
    }

    // Last iteration without prefetching
    {
        // ensure that all threads in the subgroup finished reading from SMEM during the last iteration
        barrier();

        // store A from local storage to shared memory
        [[unroll]] for (uint i = 0; i < a_tile_row_count; i += a_tile_rows_per_workgroup)
        {
            uint si = i + gl_LocalInvocationID.x / a_tile_invocations_per_row;
            matrix_a_shared[(stride_a_sh * si) / elements_per_vec4 + a_tile_k] = temp_a[i / a_tile_rows_per_workgroup];
        }

        // store B from local storage to shared memory
        [[unroll]] for (uint k = 0; k < b_tile_row_count; k += b_tile_rows_per_workgroup)
        {
            uint sk = k + gl_LocalInvocationID.x / b_tile_invocations_per_row;
            matrix_b_shared[(stride_b_sh * sk) / elements_per_vec4 + b_tile_j] = temp_b[k / b_tile_rows_per_workgroup];
        }

        // wait until all threads finished writing to shared memory before the math loop
        barrier();

        // The actual math loop
        [[unroll]] for (uint sk = 0; sk < tile_k; sk += l_k)
        {
            // load A. A will be reused coop_mat_cols times
            coopmat<A_TYPE, gl_ScopeSubgroup, l_m, l_k, gl_MatrixUseA> mat_a[coop_mat_rows];
            [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
            {
                uint si = l_m * (coop_mat_rows * warp_in_tile.y + i);
                coopMatLoad(mat_a[i], matrix_a_shared, coord_to_offset(si, sk, stride_a_sh, layout_a) / elements_per_vec4, stride_a_sh / elements_per_vec4, layout_a);
            }

            coopmat<B_TYPE, gl_ScopeSubgroup, l_k, l_n, gl_MatrixUseB> mat_b;
            [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
            {
                uint sj = l_n * (coop_mat_cols * warp_in_tile.x + j);
                // load B
                coopMatLoad(mat_b, matrix_b_shared, coord_to_offset(sk, sj, stride_b_sh, layout_b) / elements_per_vec4, stride_b_sh / elements_per_vec4, layout_b);

                // do the matrix multiply for the current portion of the tile
                [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
                {
                    result[i][j] = coopMatMulAdd(mat_a[i], mat_b, result[i][j]);
                }
            }
        }
    }

    if (split_k)
    {
        const uint chunk_offset = tile_id.z * device_parameters_buffer.split_k_slice_stride;
        [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
        {
            uint gi = offset_m + tile_m * tile_id.y + l_m * (coop_mat_rows * warp_in_tile.y + i);
            [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
            {
                uint gj = offset_n + tile_n * tile_id.x + l_n * (coop_mat_cols * warp_in_tile.x + j);
                coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> matrix_d_k_slices = coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>(result[i][j]);
                coopMatStore(matrix_d_k_slices, matrix_d_k_slices_buffer, chunk_offset + coord_to_offset(gi, gj, device_parameters_buffer.n, gl_CooperativeMatrixLayoutRowMajor), device_parameters_buffer.n, gl_CooperativeMatrixLayoutRowMajor);
            }
        }
    }
    else
    {
        [[unroll]] for (uint i = 0; i < coop_mat_rows; ++i)
        {
            uint gi = offset_m + tile_m * tile_id.y + l_m * (coop_mat_rows * warp_in_tile.y + i);
            coopmat<C_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> mat_c[coop_mat_cols];
            if (is_source_needed())
            {
                [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
                {
                    uint gj = offset_n + tile_n * tile_id.x + l_n * (coop_mat_cols * warp_in_tile.x + j);
                    coopMatLoad(mat_c[j], matrix_c_buffer, coord_to_offset(gi, gj, stride_c, layout_c), stride_c, layout_c);
                }
            }

            [[unroll]] for (uint j = 0; j < coop_mat_cols; ++j)
            {
                uint gj = offset_n + tile_n * tile_id.x + l_n * (coop_mat_cols * warp_in_tile.x + j);
                coopmat<D_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> mat_d = gemm_epilogue(result[i][j], mat_c[j]);
                coopMatStore(mat_d, matrix_d_buffer, coord_to_offset(gi, gj, stride_d, layout_d), stride_d, layout_d);
            }
        }
    }
}
