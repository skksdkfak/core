#version 460 core

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_ballot : enable

layout(local_size_x = 1024) in;

layout(std140, push_constant) uniform push_constants
{
	uint size;
	uint blocks_per_workgroup;
};

layout(std430, binding = 0) readonly buffer src_buffer_t
{
    uint src_buffer[];
};

layout(std430, binding = 1) writeonly buffer dst_buffer_t
{
    uint dst_buffer[];
};

layout(std430, binding = 2) buffer block_offset_buffer_t
{
	uint block_offset_buffer[];
};

shared uint lds_data[32];

uint localPrefixSum(uint data, inout uint totalSum)
{
	uint warpSum = subgroupExclusiveAdd(data);

    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        lds_data[gl_SubgroupID] = warpSum + data;
    }
	groupMemoryBarrier();
	barrier();

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		lds_data[gl_SubgroupInvocationID] = subgroupExclusiveAdd(lds_data[gl_SubgroupInvocationID]);
	}
	groupMemoryBarrier();
	barrier();

	uint prefixSum = warpSum + subgroupBroadcastFirst(lds_data[gl_SubgroupID]);
	groupMemoryBarrier();
	barrier();
	
	if (gl_LocalInvocationIndex.x == gl_WorkGroupSize.x - 1)
	{
		lds_data[0] = prefixSum + data;
	}
	groupMemoryBarrier();
	barrier();

	totalSum = subgroupBroadcastFirst(lds_data[0]);

	return prefixSum;
}

void main()
{
	uint value;
	uint block_offset = 0;
	uint totalSum = 0;
	for (uint idx = blocks_per_workgroup * gl_WorkGroupSize.x * gl_WorkGroupID.x + gl_LocalInvocationIndex.x; idx < min(blocks_per_workgroup * gl_WorkGroupSize.x * (gl_WorkGroupID.x + 1), size); idx += gl_WorkGroupSize.x)
	{
		value = src_buffer[idx];		
		dst_buffer[idx] = block_offset + localPrefixSum(value, totalSum);
		block_offset += totalSum;
	}

	if (gl_LocalInvocationIndex.x == gl_WorkGroupSize.x - 1)
    {
		block_offset_buffer[gl_WorkGroupID.x] = block_offset;
    }
}