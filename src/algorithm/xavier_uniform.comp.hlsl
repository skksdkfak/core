#include "pcg32_sample_generator.hlsl"

struct push_constants_t
{
	uint32_t offset;
	uint32_t size;
	uint32_t seed;
	float scale;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] RWStructuredBuffer<output_0_t> output_0_buffer : register(u0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<output_1_t> output_1_buffer : register(u1, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	if (dispatch_thread_id.x >= push_constants.size)
	{
		return;
	}

	const uint output_index = push_constants.offset + dispatch_thread_id.x;
	::framework::pcg32_sample_generator sample_generator = ::framework::pcg32_sample_generator::create(push_constants.seed, dispatch_thread_id.x);
	const float output_value = ::framework::sample_next_1d(sample_generator) * 2.0f * push_constants.scale - push_constants.scale;
	output_0_buffer[output_index] = output_0_t(output_value);
	output_1_buffer[output_index] = output_1_t(output_value);
}
