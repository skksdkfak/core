#version 460

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_ballot : enable

layout(local_size_x = 1024) in;

layout(std140, push_constant) uniform push_constants
{
	uint size;
	uint blocks_per_workgroup;
};

layout(std430, binding = 0) readonly buffer src_buffer_t
{
	uint src_buffer[];
};

layout(std430, binding = 1) writeonly buffer dst_buffer_t
{
	uint dst_buffer[];
};

layout(std430, binding = 2) buffer block_offset_buffer_t
{
	uint block_offset_buffer[];
};

shared uint lds_data[32];

uint localPrefixSum(uint data)
{
	uint warpSum = subgroupExclusiveAdd(data);

    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        lds_data[gl_SubgroupID] = warpSum + data;
    }
	groupMemoryBarrier();
	barrier();

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		lds_data[gl_SubgroupInvocationID] = subgroupExclusiveAdd(lds_data[gl_SubgroupInvocationID]);
	}
	groupMemoryBarrier();
	barrier();

	uint prefixSum = warpSum + subgroupBroadcastFirst(lds_data[gl_SubgroupID]);
	groupMemoryBarrier();
	barrier();
	
	return prefixSum;
}

void main()
{
	block_offset_buffer[gl_LocalInvocationIndex.x] = localPrefixSum(block_offset_buffer[gl_LocalInvocationIndex.x]);
}