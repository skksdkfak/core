#version 460

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_ballot : enable

layout(local_size_x = 1024) in;

layout(std140, push_constant) uniform push_constants
{
	uint size;
	uint blocks_per_workgroup;
};

layout(std430, binding = 0) readonly buffer src_buffer_t
{
	uint src_buffer[];
};

layout(std430, binding = 1) writeonly buffer dst_buffer_t
{
	uint dst_buffer[];
};

layout(std430, binding = 2) buffer block_offset_buffer_t
{
	uint block_offset_buffer[];
};

shared uint lds_data;

void main()
{
	if (gl_LocalInvocationIndex.x == 0)
	{
		lds_data = block_offset_buffer[gl_WorkGroupID.x];
	}

	groupMemoryBarrier();
	barrier();

	const uint block_offset = subgroupBroadcastFirst(lds_data);
	for (uint idx = blocks_per_workgroup * gl_WorkGroupSize.x * gl_WorkGroupID.x + gl_LocalInvocationIndex.x; idx < min(blocks_per_workgroup * gl_WorkGroupSize.x * (gl_WorkGroupID.x + 1), size); idx += gl_WorkGroupSize.x)
	{
		dst_buffer[idx] += block_offset;
	}
}