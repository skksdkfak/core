#include "algorithm/philox_bijective_function.hlsl"

struct push_constants_t
{
	uint32_t size;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);
[[vk::binding(0, 0)]] StructuredBuffer<uint64_t> input_buffer : register(t0, space0);
[[vk::binding(1, 0)]] RWStructuredBuffer<uint64_t> output_buffer : register(u0, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
    ::framework::philox_bijective_function philox_bijective_function;
    philox_bijective_function.init(push_constants.size);
    const uint64_t gather_key = philox_bijective_function(global_invocation_id);
    const uint64_t mapping_range = philox_bijective_function.get_mapping_range();
	output_buffer[global_invocation_id] = gather_key;
	output_buffer[global_invocation_id] = gather_key < push_constants.size;
}
