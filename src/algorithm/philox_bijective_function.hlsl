#pragma once

namespace framework
{
    uint32_t mulhilo(uint64_t a, uint32_t b, inout uint32_t hip)
    {
        uint64_t product = a * uint64_t(b);
        hip = product >> 32;
        return uint32_t(product);
    }

    template <uint64_t num_rounds = 24>
    struct philox_bijective_function
    {
        template <class sample_generator_t>
        void init(uint64_t capacity, inout sample_generator_t sample_generator)
        {
            uint64_t total_bits = this.get_cipher_bits(capacity);
            // Half bits rounded down
            this.left_side_bits = total_bits / 2;
            this.left_side_mask = (1ull << this.left_side_bits) - 1;
            // Half the bits rounded up
            this.right_side_bits = total_bits - this.left_side_bits;
            this.right_side_mask = (1ull << this.right_side_bits) - 1;
            for (int i = 0; i < num_rounds; i++)
            {
                this.key[i] = sample_generator.next();
            }
        }

        uint64_t get_mapping_range()
        {
            return 1ull << (this.left_side_bits + this.right_side_bits);
        }

        uint64_t operator()(const uint64_t val)
        {
            uint32_t state[2] = { uint32_t(val >> this.right_side_bits), uint32_t(val & this.right_side_mask) };
            for (int i = 0; i < num_rounds; i++)
            {
                uint32_t hi;
                uint32_t lo = ::framework::mulhilo(this.M0, state[0], hi);
                lo = (lo << (this.right_side_bits - this.left_side_bits)) | state[1] >> this.left_side_bits;
                state[0] = ((hi ^ this.key[i]) ^ state[1]) & this.left_side_mask;
                state[1] = lo & this.right_side_mask;
            }
            // Combine the left and right sides together to get result
            return uint64_t(state[0]) << this.right_side_bits | uint64_t(state[1]);
        }

        uint64_t get_cipher_bits(uint64_t capacity)
        {
            if (capacity == 0)
                return 0;
            uint64_t i = 0;
            capacity--;
            while (capacity != 0)
            {
                i++;
                capacity >>= 1;
            }

            return max(i, uint64_t(4));
        }

        uint64_t right_side_bits;
        uint64_t left_side_bits;
        uint64_t right_side_mask;
        uint64_t left_side_mask;
        uint32_t key[num_rounds];
        static const uint64_t M0 = 15183679468541472403ull;
    };
}