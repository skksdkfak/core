#pragma once

namespace framework
{
	namespace algorithm
	{
		namespace gemm
		{
			struct gemm_device_parameters
			{
				uint32_t m;
				uint32_t n;
				uint32_t k;
				uint32_t k_slice;
				uint32_t split_k_slices;
				uint32_t split_k_slice_stride;
				float alpha;
				float beta;
			};

			::framework::algorithm::gemm::gemm_device_parameters get_device_parameters(uint32_t m, uint32_t n, uint32_t k, uint32_t split_k_slices, float alpha, float beta)
			{
				::framework::algorithm::gemm::gemm_device_parameters gemm_device_parameters;
				gemm_device_parameters.m = m;
				gemm_device_parameters.n = n;
				gemm_device_parameters.k = k;
				gemm_device_parameters.k_slice = k / split_k_slices;
				gemm_device_parameters.split_k_slices = split_k_slices;
				gemm_device_parameters.split_k_slice_stride = m * n;
				gemm_device_parameters.alpha = alpha;
				gemm_device_parameters.beta = beta;
				return gemm_device_parameters;
			}
		}
	}
}
