#include "hybrid_radix_sort.hpp"
#include "resource/resource_manager.hpp"
#include "gpu/utility.hpp"
#include <set>
#include <array>
#include <numeric>
#include <cstring>
#include <iostream>
#include <algorithm>

void ::framework::algorithm::hybrid_radix_sort::allocate(::framework::algorithm::hybrid_radix_sort_create_info const & create_info, ::framework::algorithm::hybrid_radix_sort *& data)
{
	struct
	{
		::std::uint32_t graphics;
		::std::uint32_t compute;
		::std::uint32_t transfer;
	} queue_family_indices{}; // todo: init

	data = new ::framework::algorithm::hybrid_radix_sort;
	data->m_n = create_info.maxSize;
	data->m_k = create_info.maxSortBits;
	data->physical_device = create_info.physical_device;
	data->device = create_info.device;
	data->m_pInoutKeysBuffer = create_info.pInoutKeysBuffer;
	data->m_pInoutValuesBuffer = create_info.pInoutValuesBuffer;
	{
		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/stream_count_begin.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		data->m_pStreamCountBeginShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/stream_count.comp.glsl";
		data->m_pStreamCountShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/bucket_scan_and_merge.comp.glsl";
		data->m_pBucketScanAndMergeShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/prepare_sorting_pass_data.comp.glsl";
		data->m_pPrepareSortingPassDataShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/sort_and_scatter_begin.comp.glsl";
		data->m_pSortAndScatterBeginShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/sort_and_scatter.comp.glsl";
		data->m_pSortAndScatterShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);

		glsl_source_info.file_name = "core/src/kernels/glsl/local_sort.comp.glsl";
		data->m_pLocalSortShader = create_info.resource_manager->load_shader_module(data->device, shader_module_info);
	}

	create_info.physical_device->get_memory_properties(&data->physical_device_memory_properties);

	// StreamCount class DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[5];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// BlockAssignments
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// SrcKeys
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// BlockHistogram
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// BucketHistogram
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &data->m_pStreamCountDescriptorSetLayout);
	}

	// PrepareSortingPassData class DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// HybridRadixSortUbo
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// HybridRadixSortDispatchIndirectCommand
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &data->m_pPrepareSortingPassDataDescriptorSetLayout);
	}

	// BucketScanAndMerge class DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[7];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// BucketHistogramIn
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// BucketHistogramOut
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// SubBucketHistogram
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// BlockAssignments
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// BucketAssignments
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// HybridRadixSortDispatchIndirectCommand
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &data->m_pBucketScanAndMergeDescriptorSetLayout);
	}

	// SortAndScatter class DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[8];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// BlockAssignments
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// SrcKeys
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// BlockHistogram
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// BucketHistogram
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// DstKeys
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// src_values
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// dst_values
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[7].hlsl_register_space = 0;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &data->m_pSortAndScatterDescriptorSetLayout);
	}

	// LocalSort class DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// BucketAssignments
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// SrcKeys
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// DstKeys
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// src_values
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// dst_values
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		data->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &data->m_pLocalSortDescriptorSetLayout);
	}

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[3];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_pool_sizes[0].descriptor_count = 8;
	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_buffer;
	descriptor_pool_sizes[1].descriptor_count = 14;
	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[2].descriptor_count = 32;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.max_sets = 11;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	data->device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &data->descriptor_pool);

	class ::framework::gpu::descriptor_set_layout * descriptor_set_layouts[] =
	{
		data->m_pStreamCountDescriptorSetLayout,
		data->m_pStreamCountDescriptorSetLayout,
		data->m_pBucketScanAndMergeDescriptorSetLayout,
		data->m_pBucketScanAndMergeDescriptorSetLayout,
		data->m_pPrepareSortingPassDataDescriptorSetLayout,
		data->m_pSortAndScatterDescriptorSetLayout,
		data->m_pSortAndScatterDescriptorSetLayout,
		data->m_pLocalSortDescriptorSetLayout,
		data->m_pLocalSortDescriptorSetLayout
	};

	class ::framework::gpu::descriptor_set * descriptor_sets[::std::size(descriptor_set_layouts)];

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = data->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = ::std::size(descriptor_set_layouts);
	descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(data->device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

	data->m_pStreamCountDescriptorSets[0] = descriptor_sets[0];
	data->m_pStreamCountDescriptorSets[1] = descriptor_sets[1];
	data->m_pBucketScanAndMergeDescriptorSet[0] = descriptor_sets[2];
	data->m_pBucketScanAndMergeDescriptorSet[1] = descriptor_sets[3];
	data->m_pPrepareSortingPassDataDescriptorSet = descriptor_sets[4];
	data->m_pSortAndScatterDescriptorSets[0] = descriptor_sets[5];
	data->m_pSortAndScatterDescriptorSets[1] = descriptor_sets[6];
	data->m_pLocalSortDescriptorSets[0] = descriptor_sets[7];
	data->m_pLocalSortDescriptorSets[1] = descriptor_sets[8];

	::framework::gpu::push_constant_range push_constant_ranges;
	push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
	push_constant_ranges.binding = 0;
	push_constant_ranges.offset = 0;
	push_constant_ranges.size = sizeof(::std::uint32_t) * 2;
	push_constant_ranges.hlsl_shader_register = 1;
	push_constant_ranges.hlsl_register_space = 0;

	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.push_constant_range_count = 0;
	pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;

	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pStreamCountDescriptorSetLayout;
	data->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &data->m_pStreamCountPipelineLayout);

	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pSortAndScatterDescriptorSetLayout;
	data->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &data->m_pSortAndScatterPipelineLayout);

	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pLocalSortDescriptorSetLayout;
	data->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &data->m_pLocalSortPipelineLayout);

	pipeline_layout_create_info.push_constant_range_count = 1;
	push_constant_ranges.size = sizeof(::std::uint32_t);

	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pPrepareSortingPassDataDescriptorSetLayout;
	data->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &data->m_pPrepareSortingPassDataPipelineLayout);

	pipeline_layout_create_info.descriptor_set_layouts = &data->m_pBucketScanAndMergeDescriptorSetLayout;
	data->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &data->m_pBucketScanAndMergePipelineLayout);

	class ::framework::gpu::pipeline * pipelines[7];

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[7];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = data->m_pStreamCountBeginShader;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = data->m_pStreamCountShader;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = data->m_pBucketScanAndMergeShader;
	pipeline_shader_stage_create_infos[2].name = "main";
	pipeline_shader_stage_create_infos[2].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[3].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[3].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[3].module = data->m_pPrepareSortingPassDataShader;
	pipeline_shader_stage_create_infos[3].name = "main";
	pipeline_shader_stage_create_infos[3].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[4].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[4].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[4].module = data->m_pSortAndScatterBeginShader;
	pipeline_shader_stage_create_infos[4].name = "main";
	pipeline_shader_stage_create_infos[4].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[5].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[5].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[5].module = data->m_pSortAndScatterShader;
	pipeline_shader_stage_create_infos[5].name = "main";
	pipeline_shader_stage_create_infos[5].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[6].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[6].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[6].module = data->m_pLocalSortShader;
	pipeline_shader_stage_create_infos[6].name = "main";
	pipeline_shader_stage_create_infos[6].specialization_info = nullptr;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[7];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = data->m_pStreamCountPipelineLayout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = data->m_pStreamCountPipelineLayout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = data->m_pBucketScanAndMergePipelineLayout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	compute_pipeline_create_infos[3].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[3].stage = pipeline_shader_stage_create_infos[3];
	compute_pipeline_create_infos[3].layout = data->m_pPrepareSortingPassDataPipelineLayout;
	compute_pipeline_create_infos[3].base_pipeline = nullptr;
	compute_pipeline_create_infos[3].base_pipeline_index = -1;

	compute_pipeline_create_infos[4].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[4].stage = pipeline_shader_stage_create_infos[4];
	compute_pipeline_create_infos[4].layout = data->m_pSortAndScatterPipelineLayout;
	compute_pipeline_create_infos[4].base_pipeline = nullptr;
	compute_pipeline_create_infos[4].base_pipeline_index = -1;

	compute_pipeline_create_infos[5].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[5].stage = pipeline_shader_stage_create_infos[5];
	compute_pipeline_create_infos[5].layout = data->m_pSortAndScatterPipelineLayout;
	compute_pipeline_create_infos[5].base_pipeline = nullptr;
	compute_pipeline_create_infos[5].base_pipeline_index = -1;

	compute_pipeline_create_infos[6].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[6].stage = pipeline_shader_stage_create_infos[6];
	compute_pipeline_create_infos[6].layout = data->m_pLocalSortPipelineLayout;
	compute_pipeline_create_infos[6].base_pipeline = nullptr;
	compute_pipeline_create_infos[6].base_pipeline_index = -1;
	data->device->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines);

	data->m_pStreamCountBeginPso = pipelines[0];
	data->m_pStreamCountPso = pipelines[1];
	data->m_pBucketScanAndMergePso = pipelines[2];
	data->m_pPrepareSortingPassDataPso = pipelines[3];
	data->m_pSortAndScatterBeginPso = pipelines[4];
	data->m_pSortAndScatterPso = pipelines[5];
	data->m_pLocalSortPso = pipelines[6];

	::std::uint32_t const r = 1 << data->m_d;
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ((sizeof(HybridRadixSortUB) + 256 - 1) & ~(256 - 1)) * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pConstBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pConstBuffer;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pConstBufferMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pConstBuffer;
		bind_buffer_memory_info.memory = data->m_pConstBufferMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * (data->m_n);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer0);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pWorkBuffer0;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer0DeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pWorkBuffer0;
		bind_buffer_memory_info.memory = data->m_pWorkBuffer0DeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * (data->m_n);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pWorkBuffer1);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pWorkBuffer1;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pWorkBuffer1DeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pWorkBuffer1;
		bind_buffer_memory_info.memory = data->m_pWorkBuffer1DeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * (data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit; // todo: check if ::framework::gpu::buffer_usage_flags::transfer_src_bit really needed here and for other buffers
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBucketHistogramDataBuffer[0]);
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBucketHistogramDataBuffer[1]);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pBucketHistogramDataBuffer[0];
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBucketHistogramDeviceMemory[0]);
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBucketHistogramDeviceMemory[1]);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pBucketHistogramDataBuffer[0];
		bind_buffer_memory_info.memory = data->m_pBucketHistogramDeviceMemory[0];
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);

		bind_buffer_memory_info.buffer = data->m_pBucketHistogramDataBuffer[1];
		bind_buffer_memory_info.memory = data->m_pBucketHistogramDeviceMemory[1];
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * r * (data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pSubBucketHistogramBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pSubBucketHistogramBuffer;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pSubBucketHistogramDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pSubBucketHistogramBuffer;
		bind_buffer_memory_info.memory = data->m_pSubBucketHistogramDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * r * (data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBucketOffsetBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pBucketOffsetBuffer;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBucketOffsetDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pBucketOffsetBuffer;
		bind_buffer_memory_info.memory = data->m_pBucketOffsetDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * r * (data->m_n / KPB + data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBlockHistogramBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pBlockHistogramBuffer;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBlockHistogramDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pBlockHistogramBuffer;
		bind_buffer_memory_info.memory = data->m_pBlockHistogramDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * 3 * (data->m_n / KPB + data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBlockAssignmentsBuffer[0]);
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBlockAssignmentsBuffer[1]);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pBlockAssignmentsBuffer[0];
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBlockAssignmentsDeviceMemory[0]);
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBlockAssignmentsDeviceMemory[1]);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pBlockAssignmentsBuffer[0];
		bind_buffer_memory_info.memory = data->m_pBlockAssignmentsDeviceMemory[0];
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);

		bind_buffer_memory_info.buffer = data->m_pBlockAssignmentsBuffer[1];
		bind_buffer_memory_info.memory = data->m_pBlockAssignmentsDeviceMemory[1];
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(BucketAssignment) * ::std::min(2 * data->m_n / data->m_mbt + data->m_n / data->m_lst, r * data->m_n / data->m_lst);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBucketAssignmentsBuffer[0]);
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pBucketAssignmentsBuffer[1]);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pBucketAssignmentsBuffer[0];
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBucketAssignmentsDeviceMemory[0]);
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pBucketAssignmentsDeviceMemory[1]);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pBucketAssignmentsBuffer[0];
		bind_buffer_memory_info.memory = data->m_pBucketAssignmentsDeviceMemory[0];
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);

		bind_buffer_memory_info.buffer = data->m_pBucketAssignmentsBuffer[1];
		bind_buffer_memory_info.memory = data->m_pBucketAssignmentsDeviceMemory[1];
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::gpu::dispatch_indirect_command) * 2 * 3;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = queue_family_indices.compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		data->device->create_buffer(&buffer_create_info, nullptr, &data->m_pDispatchIndirectCommandsBuffer);

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = data->m_pDispatchIndirectCommandsBuffer;
		data->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(data->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		data->device->allocate_memory(&memory_allocate_info, nullptr, &data->m_pDispatchIndirectCommandsBufferDeviceMemory);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = data->m_pDispatchIndirectCommandsBuffer;
		bind_buffer_memory_info.memory = data->m_pDispatchIndirectCommandsBufferDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		data->device->bind_buffer_memory(1, &bind_buffer_memory_info);
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[15];
	descriptor_buffer_infos[0].buffer = data->m_pConstBuffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = (sizeof(HybridRadixSortUB) + 256 - 1) & ~(256 - 1);

	descriptor_buffer_infos[1].buffer = data->m_pInoutKeysBuffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[2].buffer = data->m_pInoutValuesBuffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[3].buffer = data->m_pWorkBuffer0;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[4].buffer = data->m_pWorkBuffer1;
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[5].buffer = data->m_pBlockAssignmentsBuffer[0];
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = sizeof(::std::uint32_t) * 3;

	descriptor_buffer_infos[6].buffer = data->m_pBlockAssignmentsBuffer[1];
	descriptor_buffer_infos[6].offset = 0;
	descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[6].stride = sizeof(::std::uint32_t) * 3;

	descriptor_buffer_infos[7].buffer = data->m_pBucketAssignmentsBuffer[0];
	descriptor_buffer_infos[7].offset = 0;
	descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[7].stride = sizeof(BucketAssignment);

	descriptor_buffer_infos[8].buffer = data->m_pBucketAssignmentsBuffer[1];
	descriptor_buffer_infos[8].offset = 0;
	descriptor_buffer_infos[8].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[8].stride = sizeof(BucketAssignment);

	descriptor_buffer_infos[9].buffer = data->m_pSubBucketHistogramBuffer;
	descriptor_buffer_infos[9].offset = 0;
	descriptor_buffer_infos[9].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[9].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[10].buffer = data->m_pBucketHistogramDataBuffer[0];
	descriptor_buffer_infos[10].offset = 0;
	descriptor_buffer_infos[10].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[10].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[11].buffer = data->m_pBucketHistogramDataBuffer[1];
	descriptor_buffer_infos[11].offset = 0;
	descriptor_buffer_infos[11].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[11].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[12].buffer = data->m_pBlockHistogramBuffer;
	descriptor_buffer_infos[12].offset = 0;
	descriptor_buffer_infos[12].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[12].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[13].buffer = data->m_pConstBuffer;
	descriptor_buffer_infos[13].offset = 0;
	descriptor_buffer_infos[13].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[13].stride = (sizeof(HybridRadixSortUB) + 256 - 1) & ~(256 - 1);

	descriptor_buffer_infos[14].buffer = data->m_pDispatchIndirectCommandsBuffer;
	descriptor_buffer_infos[14].offset = 0;
	descriptor_buffer_infos[14].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[14].stride = sizeof(::framework::gpu::dispatch_indirect_command);

	// Setup StreamCountDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[7];
		// UBO consts
		write_descriptor_sets[0].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// BlockAssignments
		write_descriptor_sets[1].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[5];
		// BlockAssignments
		write_descriptor_sets[2].dst_set = data->m_pStreamCountDescriptorSets[1];
		write_descriptor_sets[2].dst_binding = 1;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[6];
		// SrcKeys
		write_descriptor_sets[3].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[3].dst_binding = 2;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[1];
		// SrcKeys
		write_descriptor_sets[4].dst_set = data->m_pStreamCountDescriptorSets[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[3];
		// BlockHistogram
		write_descriptor_sets[5].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[5].dst_binding = 3;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[12];
		// SubBucketHistogram
		write_descriptor_sets[6].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[6].dst_binding = 4;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[9];

		::framework::gpu::copy_descriptor_set copy_descriptor_sets[3];
		copy_descriptor_sets[0].src_set = data->m_pStreamCountDescriptorSets[0];
		copy_descriptor_sets[0].src_binding = 0;
		copy_descriptor_sets[0].src_array_element = 0;
		copy_descriptor_sets[0].dst_set = data->m_pStreamCountDescriptorSets[1];
		copy_descriptor_sets[0].dst_binding = 0;
		copy_descriptor_sets[0].dst_array_element = 0;
		copy_descriptor_sets[0].descriptor_count = 1;

		copy_descriptor_sets[1].src_set = data->m_pStreamCountDescriptorSets[0];
		copy_descriptor_sets[1].src_binding = 3;
		copy_descriptor_sets[1].src_array_element = 0;
		copy_descriptor_sets[1].dst_set = data->m_pStreamCountDescriptorSets[1];
		copy_descriptor_sets[1].dst_binding = 3;
		copy_descriptor_sets[1].dst_array_element = 0;
		copy_descriptor_sets[1].descriptor_count = 1;

		copy_descriptor_sets[2].src_set = data->m_pStreamCountDescriptorSets[0];
		copy_descriptor_sets[2].src_binding = 4;
		copy_descriptor_sets[2].src_array_element = 0;
		copy_descriptor_sets[2].dst_set = data->m_pStreamCountDescriptorSets[1];
		copy_descriptor_sets[2].dst_binding = 4;
		copy_descriptor_sets[2].dst_array_element = 0;
		copy_descriptor_sets[2].descriptor_count = 1;

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		data->device->update_descriptor_sets(0, nullptr, ::std::size(copy_descriptor_sets), copy_descriptor_sets);
	}

	// Setup BucketScanAndMergeDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[11];
		// UBO consts
		write_descriptor_sets[0].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// BucketHistogramIn
		write_descriptor_sets[1].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[10];
		// BucketHistogramIn
		write_descriptor_sets[2].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		write_descriptor_sets[2].dst_binding = 1;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[11];
		// BucketHistogramOut
		write_descriptor_sets[3].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[3].dst_binding = 2;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[11];
		// BucketHistogramOut
		write_descriptor_sets[4].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[10];
		// SubBucketHistogram
		write_descriptor_sets[5].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[5].dst_binding = 3;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[9];
		// BlockAssignments
		write_descriptor_sets[6].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[6].dst_binding = 4;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[6];
		// BlockAssignments
		write_descriptor_sets[7].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		write_descriptor_sets[7].dst_binding = 4;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[5];
		// BucketAssignments
		write_descriptor_sets[8].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[8].dst_binding = 5;
		write_descriptor_sets[8].dst_array_element = 0;
		write_descriptor_sets[8].descriptor_count = 1;
		write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[8];
		// BucketAssignments
		write_descriptor_sets[9].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		write_descriptor_sets[9].dst_binding = 5;
		write_descriptor_sets[9].dst_array_element = 0;
		write_descriptor_sets[9].descriptor_count = 1;
		write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[7];
		// HybridRadixSortDispatchIndirectCommand
		write_descriptor_sets[10].dst_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		write_descriptor_sets[10].dst_binding = 6;
		write_descriptor_sets[10].dst_array_element = 0;
		write_descriptor_sets[10].descriptor_count = 1;
		write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[14];

		::framework::gpu::copy_descriptor_set copy_descriptor_sets[3];
		copy_descriptor_sets[0].src_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		copy_descriptor_sets[0].src_binding = 0;
		copy_descriptor_sets[0].src_array_element = 0;
		copy_descriptor_sets[0].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		copy_descriptor_sets[0].dst_binding = 0;
		copy_descriptor_sets[0].dst_array_element = 0;
		copy_descriptor_sets[0].descriptor_count = 1;

		copy_descriptor_sets[1].src_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		copy_descriptor_sets[1].src_binding = 3;
		copy_descriptor_sets[1].src_array_element = 0;
		copy_descriptor_sets[1].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		copy_descriptor_sets[1].dst_binding = 3;
		copy_descriptor_sets[1].dst_array_element = 0;
		copy_descriptor_sets[1].descriptor_count = 1;

		copy_descriptor_sets[2].src_set = data->m_pBucketScanAndMergeDescriptorSet[0];
		copy_descriptor_sets[2].src_binding = 6;
		copy_descriptor_sets[2].src_array_element = 0;
		copy_descriptor_sets[2].dst_set = data->m_pBucketScanAndMergeDescriptorSet[1];
		copy_descriptor_sets[2].dst_binding = 6;
		copy_descriptor_sets[2].dst_array_element = 0;
		copy_descriptor_sets[2].descriptor_count = 1;

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		data->device->update_descriptor_sets(0, nullptr, ::std::size(copy_descriptor_sets), copy_descriptor_sets);
	}

	// Setup PrepareSortingPassData descriptor set
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		// HybridRadixSortUbo
		write_descriptor_sets[0].dst_set = data->m_pPrepareSortingPassDataDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[13];
		// HybridRadixSortDispatchIndirectCommand
		write_descriptor_sets[1].dst_set = data->m_pPrepareSortingPassDataDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[14];

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}

	// Setup SortAndScatter descriptor set
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[13];
		// UBO consts
		write_descriptor_sets[0].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// BlockAssignments
		write_descriptor_sets[1].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[5];
		// BlockAssignments
		write_descriptor_sets[2].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[2].dst_binding = 1;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[6];
		// SrcKeys
		write_descriptor_sets[3].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[3].dst_binding = 2;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[1];
		// SrcKeys
		write_descriptor_sets[4].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[3];
		// BlockHistogram
		write_descriptor_sets[5].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[5].dst_binding = 3;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[12];
		// SubBucketHistogram
		write_descriptor_sets[6].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[6].dst_binding = 4;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[9];
		// DstKeys
		write_descriptor_sets[7].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[7].dst_binding = 5;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[3];
		// DstKeys
		write_descriptor_sets[8].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[8].dst_binding = 5;
		write_descriptor_sets[8].dst_array_element = 0;
		write_descriptor_sets[8].descriptor_count = 1;
		write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[1];
		// src_values
		write_descriptor_sets[9].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[9].dst_binding = 6;
		write_descriptor_sets[9].dst_array_element = 0;
		write_descriptor_sets[9].descriptor_count = 1;
		write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[2];
		// src_values
		write_descriptor_sets[10].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[10].dst_binding = 6;
		write_descriptor_sets[10].dst_array_element = 0;
		write_descriptor_sets[10].descriptor_count = 1;
		write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[4];
		// dst_values
		write_descriptor_sets[11].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[11].dst_binding = 7;
		write_descriptor_sets[11].dst_array_element = 0;
		write_descriptor_sets[11].descriptor_count = 1;
		write_descriptor_sets[11].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[11].buffer_info = &descriptor_buffer_infos[4];
		// dst_values
		write_descriptor_sets[12].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[12].dst_binding = 7;
		write_descriptor_sets[12].dst_array_element = 0;
		write_descriptor_sets[12].descriptor_count = 1;
		write_descriptor_sets[12].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[12].buffer_info = &descriptor_buffer_infos[2];

		::framework::gpu::copy_descriptor_set copy_descriptor_sets[3];
		copy_descriptor_sets[0].src_set = data->m_pSortAndScatterDescriptorSets[0];
		copy_descriptor_sets[0].src_binding = 0;
		copy_descriptor_sets[0].src_array_element = 0;
		copy_descriptor_sets[0].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		copy_descriptor_sets[0].dst_binding = 0;
		copy_descriptor_sets[0].dst_array_element = 0;
		copy_descriptor_sets[0].descriptor_count = 1;

		copy_descriptor_sets[1].src_set = data->m_pSortAndScatterDescriptorSets[0];
		copy_descriptor_sets[1].src_binding = 3;
		copy_descriptor_sets[1].src_array_element = 0;
		copy_descriptor_sets[1].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		copy_descriptor_sets[1].dst_binding = 3;
		copy_descriptor_sets[1].dst_array_element = 0;
		copy_descriptor_sets[1].descriptor_count = 1;

		copy_descriptor_sets[2].src_set = data->m_pSortAndScatterDescriptorSets[0];
		copy_descriptor_sets[2].src_binding = 4;
		copy_descriptor_sets[2].src_array_element = 0;
		copy_descriptor_sets[2].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		copy_descriptor_sets[2].dst_binding = 4;
		copy_descriptor_sets[2].dst_array_element = 0;
		copy_descriptor_sets[2].descriptor_count = 1;

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		data->device->update_descriptor_sets(0, nullptr, ::std::size(copy_descriptor_sets), copy_descriptor_sets);
	}

	// Setup LocalSortDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[11];
		// UBO consts
		write_descriptor_sets[0].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		// BucketAssignments
		write_descriptor_sets[1].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[8];
		// BucketAssignments
		write_descriptor_sets[2].dst_set = data->m_pLocalSortDescriptorSets[1];
		write_descriptor_sets[2].dst_binding = 1;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[7];
		// SrcKeys
		write_descriptor_sets[3].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[3].dst_binding = 2;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		// SrcKeys
		write_descriptor_sets[4].dst_set = data->m_pLocalSortDescriptorSets[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[1];
		// DstKeys
		write_descriptor_sets[5].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[5].dst_binding = 3;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[1];
		// DstKeys
		write_descriptor_sets[6].dst_set = data->m_pLocalSortDescriptorSets[1];
		write_descriptor_sets[6].dst_binding = 3;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[3];
		// src_values
		write_descriptor_sets[7].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[7].dst_binding = 4;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[4];
		// src_values
		write_descriptor_sets[8].dst_set = data->m_pLocalSortDescriptorSets[1];
		write_descriptor_sets[8].dst_binding = 4;
		write_descriptor_sets[8].dst_array_element = 0;
		write_descriptor_sets[8].descriptor_count = 1;
		write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[2];
		// dst_values
		write_descriptor_sets[9].dst_set = data->m_pLocalSortDescriptorSets[0];
		write_descriptor_sets[9].dst_binding = 5;
		write_descriptor_sets[9].dst_array_element = 0;
		write_descriptor_sets[9].descriptor_count = 1;
		write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[2];
		// dst_values
		write_descriptor_sets[10].dst_set = data->m_pLocalSortDescriptorSets[1];
		write_descriptor_sets[10].dst_binding = 5;
		write_descriptor_sets[10].dst_array_element = 0;
		write_descriptor_sets[10].descriptor_count = 1;
		write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[4];

		::framework::gpu::copy_descriptor_set copy_descriptor_sets[1];
		copy_descriptor_sets[0].src_set = data->m_pLocalSortDescriptorSets[0];
		copy_descriptor_sets[0].src_binding = 0;
		copy_descriptor_sets[0].src_array_element = 0;
		copy_descriptor_sets[0].dst_set = data->m_pLocalSortDescriptorSets[1];
		copy_descriptor_sets[0].dst_binding = 0;
		copy_descriptor_sets[0].dst_array_element = 0;
		copy_descriptor_sets[0].descriptor_count = 1;

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		data->device->update_descriptor_sets(0, nullptr, ::std::size(copy_descriptor_sets), copy_descriptor_sets);
	}

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		data->device->create_command_pool(&command_pool_create_info, nullptr, &data->command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = data->command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = ::std::size(data->command_buffers);
		data->device->allocate_command_buffers(&command_buffer_allocate_info, data->command_buffers);
	}

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
	data->device->create_fence(&fence_create_info, nullptr, &data->d3d12_fence);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	data->device->create_semaphore(&semaphore_create_info, nullptr, &data->m_pSemaphore);

	data->buildCommandBuffers();

	data = data;
}

void ::framework::algorithm::hybrid_radix_sort::deallocate(::framework::algorithm::hybrid_radix_sort * pData)
{
	pData->device->free_command_buffers(pData->command_pool, ::std::size(pData->command_buffers), pData->command_buffers);
	pData->device->destroy_command_pool(pData->command_pool, nullptr);
	pData->device->destroy_pipeline(pData->m_pStreamCountPso, nullptr);
	pData->device->destroy_pipeline(pData->m_pBucketScanAndMergePso, nullptr);
	pData->device->destroy_pipeline(pData->m_pPrepareSortingPassDataPso, nullptr);
	pData->device->destroy_pipeline(pData->m_pSortAndScatterPso, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pStreamCountDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pBucketScanAndMergeDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pPrepareSortingPassDataDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pSortAndScatterDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_pool(pData->descriptor_pool, nullptr);
	pData->device->destroy_buffer(pData->m_pDispatchIndirectCommandsBuffer, nullptr);
	pData->device->free_memory(pData->m_pDispatchIndirectCommandsBufferDeviceMemory, nullptr);
	pData->device->destroy_buffer(pData->m_pWorkBuffer0, nullptr);
	pData->device->free_memory(pData->m_pWorkBuffer0DeviceMemory, nullptr);
	pData->device->destroy_buffer(pData->m_pConstBuffer, nullptr);
	pData->device->free_memory(pData->m_pConstBufferMemory, nullptr);

	delete pData;
}

void ::framework::algorithm::hybrid_radix_sort::setSortBitsCount(::std::uint32_t sortBits)
{
	m_k = sortBits;
}

void ::framework::algorithm::hybrid_radix_sort::setSize(::std::uint32_t size)
{
	setSize(size, m_k - m_d);
}

void ::framework::algorithm::hybrid_radix_sort::setSize(::std::uint32_t size, ::std::uint32_t startBit)
{
	bool const currentSet = ((m_k - m_d - startBit) / m_d) % 2;

	::std::uint32_t const bucketSize = 1u << m_d;
	::std::uint32_t const nWGs = (size + KPB - 1) / KPB;
	::std::uint32_t const nBuckets = bucketSize;
	::std::uint32_t const prefixScanBlockSize = SCAN_NUM_WIS_PER_BLOCK * bucketSize;
	::std::uint32_t const nPrefixScanBlocks = (nBuckets + prefixScanBlockSize - 1) / prefixScanBlockSize;
	::std::uint32_t const nPrefixScanWGs = ::std::min(nPrefixScanBlocks, static_cast<::std::uint32_t>(SCAN_NUM_WGS));

	HybridRadixSortUB hybridRadixSortUB;
	hybridRadixSortUB.size = size;
	hybridRadixSortUB.nWGs = nWGs;
	hybridRadixSortUB.startBit = startBit;
	hybridRadixSortUB.nBuckets = nBuckets;
	hybridRadixSortUB.nScanBlocksPerWG = nPrefixScanBlocks < SCAN_NUM_WGS ? 1 : (nPrefixScanBlocks + nPrefixScanWGs - 1) / nPrefixScanWGs;

	::std::size_t const minUboAlignment = 256;
	::std::size_t const dynamicAlignment = (sizeof(HybridRadixSortUB) + minUboAlignment - 1) & ~(minUboAlignment - 1);

	void * mappedData;
	device->map_memory(m_pConstBufferMemory, 0, dynamicAlignment, ::framework::gpu::memory_map_flags::none, &mappedData);
	::std::memcpy(static_cast<char *>(mappedData), &hybridRadixSortUB, sizeof(hybridRadixSortUB));
	device->unmap_memory(m_pConstBufferMemory);

	{
		::framework::gpu::dispatch_indirect_command dispatchIndirectCommand;
		void * mappedDispatchIndirectCommands;
		device->map_memory(m_pDispatchIndirectCommandsBufferDeviceMemory, currentSet * sizeof(::framework::gpu::dispatch_indirect_command), sizeof(::framework::gpu::dispatch_indirect_command) * 2 * 3, ::framework::gpu::memory_map_flags::none, &mappedDispatchIndirectCommands);

		// Sort ::framework::gpu::dispatch_indirect_command
		dispatchIndirectCommand = { nWGs, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		// Scan ::framework::gpu::dispatch_indirect_command
		dispatchIndirectCommand = { nPrefixScanWGs, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands) + sizeof(::framework::gpu::dispatch_indirect_command), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		dispatchIndirectCommand = { 0, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands) + sizeof(::framework::gpu::dispatch_indirect_command) * 2, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		dispatchIndirectCommand = { 0, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands) + sizeof(::framework::gpu::dispatch_indirect_command) * 3, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		dispatchIndirectCommand = { 0, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands) + sizeof(::framework::gpu::dispatch_indirect_command) * 4, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		dispatchIndirectCommand = { 0, 1, 1 };
		::std::memcpy(static_cast<char *>(mappedDispatchIndirectCommands) + sizeof(::framework::gpu::dispatch_indirect_command) * 5, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));

		device->unmap_memory(m_pDispatchIndirectCommandsBufferDeviceMemory);
	}
}

void ::framework::algorithm::hybrid_radix_sort::execute(class ::framework::gpu::queue * command_queue, class ::framework::gpu::semaphore * wait_semaphore, ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, class ::framework::gpu::semaphore * signal_semaphore, class ::framework::gpu::fence * fence)
{
	for (int ib = m_k - m_d, pass = 0; ib >= 0; ib -= m_d, ++pass)
	{
		const bool is_first_pass = pass == 0;
		const bool is_last_pass = pass == m_k / m_d - 1;

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = is_first_pass ? wait_semaphore : m_pSemaphore;
		wait_semaphore_info.value = 0;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		wait_semaphore_info.device_index = 0;

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = is_first_pass ? this->command_buffers[0] : this->command_buffers[2 - pass % 2];
		command_buffer_info.device_mask = 0;

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = is_last_pass ? signal_semaphore : m_pSemaphore;
		signal_semaphore_info.value = 0;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		signal_semaphore_info.device_index = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 1;
		submit_info.wait_semaphore_infos = &wait_semaphore_info;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 1;
		submit_info.signal_semaphore_infos = &signal_semaphore_info;
		command_queue->submit(1, &submit_info, is_last_pass ? fence : nullptr);
	}
}

void ::framework::algorithm::hybrid_radix_sort::buildCommandBuffers()
{
	class ::framework::gpu::buffer * pSrcKeys = m_pInoutKeysBuffer;
	class ::framework::gpu::buffer * pSrcValues = m_pInoutValuesBuffer;
	class ::framework::gpu::buffer * pDstKeys = m_pWorkBuffer0;
	class ::framework::gpu::buffer * pDstValues = m_pInoutValuesBuffer;

	for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(::std::size(this->command_buffers)); ++i)
	{
		::std::uint32_t const cur_set = i % 2;
		::std::uint32_t const prev_set = !cur_set;
		::std::uint32_t const dynamic_offset = cur_set * ((sizeof(HybridRadixSortUB) + 256 - 1) & ~(256 - 1));

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::simultaneous_use_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		this->command_buffers[i]->begin_command_buffer(&command_buffer_begin_info);

		if (i > 0)
		{
			{
				::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
				buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::uniform_read_bit;
				buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
				buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].buffer = m_pConstBuffer;
				buffer_memory_barriers[0].offset = 0;
				buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

				buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::indirect_command_read_bit | ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::draw_indirect_bit | ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::indirect_argument_bit;
				buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].buffer = m_pDispatchIndirectCommandsBuffer;
				buffer_memory_barriers[1].offset = 0;
				buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
				dependency_info.buffer_memory_barriers = buffer_memory_barriers;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;

				this->command_buffers[i]->pipeline_barrier(&dependency_info);
			}

			this->command_buffers[i]->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pPrepareSortingPassDataPso);
			this->command_buffers[i]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pPrepareSortingPassDataPipelineLayout, 0, 1, &m_pPrepareSortingPassDataDescriptorSet, 0, nullptr);
			this->command_buffers[i]->push_constants(m_pPrepareSortingPassDataPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(cur_set), &cur_set);
			this->command_buffers[i]->dispatch(1, 1, 1);

			{
				::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
				buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::uniform_read_bit;
				buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
				buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].buffer = m_pConstBuffer;
				buffer_memory_barriers[0].offset = 0;
				buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

				buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::indirect_command_read_bit | ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::draw_indirect_bit | ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::indirect_argument_bit;
				buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].buffer = m_pDispatchIndirectCommandsBuffer;
				buffer_memory_barriers[1].offset = 0;
				buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
				dependency_info.buffer_memory_barriers = buffer_memory_barriers;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;

				this->command_buffers[i]->pipeline_barrier(&dependency_info);
			}
		}
		else
		{
			this->command_buffers[i]->fill_buffer(m_pBucketHistogramDataBuffer[0], 0, sizeof(::std::uint32_t), 0, 0, {}, {}); // todo
		}

		{// stream count
			{
				::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
				buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
				buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
				buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
				buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].buffer = m_pSubBucketHistogramBuffer;
				buffer_memory_barriers[0].offset = 0;
				buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit; /*todo: global_memory_barrier*/
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
				dependency_info.buffer_memory_barriers = buffer_memory_barriers;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;

				this->command_buffers[i]->pipeline_barrier(&dependency_info);
			}

			this->command_buffers[i]->fill_buffer(m_pSubBucketHistogramBuffer, 0, sizeof(::std::uint32_t) * 256 * (m_n / m_lst), 0, 0, {}, {}); // TODO: clear only used range

			{
				::framework::gpu::buffer_memory_barrier buffer_memory_barriers[4];
				buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
				buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
				buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[0].buffer = m_pBlockAssignmentsBuffer[cur_set];
				buffer_memory_barriers[0].offset = 0;
				buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

				buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
				buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
				buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[1].buffer = pSrcKeys;
				buffer_memory_barriers[1].offset = 0;
				buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

				buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
				buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
				buffer_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[2].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
				buffer_memory_barriers[2].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[2].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[2].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[2].buffer = m_pBlockHistogramBuffer;
				buffer_memory_barriers[2].offset = 0;
				buffer_memory_barriers[2].size = ::framework::gpu::whole_size;

				buffer_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::fill_buffer_bit;
				buffer_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
				buffer_memory_barriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
				buffer_memory_barriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
				buffer_memory_barriers[3].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
				buffer_memory_barriers[3].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
				buffer_memory_barriers[3].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[3].src_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[3].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
				buffer_memory_barriers[3].dst_queue_family_ownership.queue_family_index = queue_family_index;
				buffer_memory_barriers[3].buffer = m_pSubBucketHistogramBuffer;
				buffer_memory_barriers[3].offset = 0;
				buffer_memory_barriers[3].size = ::framework::gpu::whole_size;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit; /*todo: global_memory_barrier*/
				dependency_info.memory_barrier_count = 0;
				dependency_info.memory_barriers = nullptr;
				dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
				dependency_info.buffer_memory_barriers = buffer_memory_barriers;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;

				this->command_buffers[i]->pipeline_barrier(&dependency_info);
			}

			this->command_buffers[i]->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, i == 0 ? m_pStreamCountBeginPso : m_pStreamCountPso);
			this->command_buffers[i]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pStreamCountPipelineLayout, 0, 1, &m_pStreamCountDescriptorSets[cur_set], 1, &dynamic_offset);
			this->command_buffers[i]->dispatch_indirect(m_pDispatchIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * cur_set * 3);
		}

		{// BucketScanAndMerge
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[5];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].buffer = m_pSubBucketHistogramBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].buffer = m_pBucketHistogramDataBuffer[cur_set];
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[2].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].buffer = m_pBucketHistogramDataBuffer[prev_set];
			buffer_memory_barriers[2].offset = 0;
			buffer_memory_barriers[2].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[3].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[3].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[3].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[3].buffer = m_pDispatchIndirectCommandsBuffer;
			buffer_memory_barriers[3].offset = 0;
			buffer_memory_barriers[3].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[4].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[4].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[4].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[4].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[4].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[4].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[4].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[4].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[4].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[4].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[4].buffer = m_pDispatchIndirectCommandsBuffer;
			buffer_memory_barriers[4].offset = 0;
			buffer_memory_barriers[4].size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit; /*todo: global_memory_barrier*/
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			this->command_buffers[i]->pipeline_barrier(&dependency_info);
			this->command_buffers[i]->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pBucketScanAndMergePso);
			this->command_buffers[i]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pBucketScanAndMergePipelineLayout, 0, 1, &m_pBucketScanAndMergeDescriptorSet[cur_set], 1, &dynamic_offset);
			this->command_buffers[i]->push_constants(m_pBucketScanAndMergePipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(prev_set), &prev_set);
			this->command_buffers[i]->dispatch_indirect(m_pDispatchIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * cur_set * 3 + sizeof(::framework::gpu::dispatch_indirect_command));
		}

		{//	counting sort
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[3];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].buffer = m_pBlockHistogramBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].buffer = m_pSubBucketHistogramBuffer;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[2].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].buffer = pDstKeys;
			buffer_memory_barriers[2].offset = 0;
			buffer_memory_barriers[2].size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit; /*todo: global_memory_barrier*/
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = i > 0 ? 3 : 2;
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			this->command_buffers[i]->pipeline_barrier(&dependency_info);
			this->command_buffers[i]->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, i == 0 ? m_pSortAndScatterBeginPso : m_pSortAndScatterPso);
			this->command_buffers[i]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pSortAndScatterPipelineLayout, 0, 1, &m_pSortAndScatterDescriptorSets[cur_set], 1, &dynamic_offset);
			this->command_buffers[i]->dispatch_indirect(m_pDispatchIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * cur_set * 3);
		}

		{//	local sort
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[6];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[0].buffer = m_pBucketAssignmentsBuffer[prev_set];
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[1].buffer = pSrcKeys;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[2].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[2].buffer = pSrcValues;
			buffer_memory_barriers[2].offset = 0;
			buffer_memory_barriers[2].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[3].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[3].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[3].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[3].buffer = pDstKeys;
			buffer_memory_barriers[3].offset = 0;
			buffer_memory_barriers[3].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[4].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[4].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[4].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[4].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[4].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[4].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[4].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[4].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[4].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[4].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[4].buffer = pDstValues;
			buffer_memory_barriers[4].offset = 0;
			buffer_memory_barriers[4].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[5].src_access_mask = ::framework::gpu::access_flags::shader_read_bit | ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[5].dst_access_mask = ::framework::gpu::access_flags::indirect_command_read_bit;
			buffer_memory_barriers[5].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[5].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::draw_indirect_bit;
			buffer_memory_barriers[5].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[5].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[5].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[5].src_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[5].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[5].dst_queue_family_ownership.queue_family_index = queue_family_index;
			buffer_memory_barriers[5].buffer = m_pDispatchIndirectCommandsBuffer;
			buffer_memory_barriers[5].offset = 0;
			buffer_memory_barriers[5].size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit; /*todo: global_memory_barrier*/
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			this->command_buffers[i]->pipeline_barrier(&dependency_info);
			this->command_buffers[i]->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, m_pLocalSortPso);
			this->command_buffers[i]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, m_pLocalSortPipelineLayout, 0, 1, &m_pLocalSortDescriptorSets[cur_set], 1, &dynamic_offset);
			this->command_buffers[i]->dispatch_indirect(m_pDispatchIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command) * prev_set * 3 + sizeof(::framework::gpu::dispatch_indirect_command) * 2);
		}

		this->command_buffers[i]->end_command_buffer();

		::std::swap(pSrcKeys, pDstKeys);
		::std::swap(pSrcValues, pDstValues);
	}
}