#pragma once

#include "gpu/core.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class mma
	{
	public:
		class executor;

		enum class matrix_layout
		{
			row_major = 0,
			column_major = 1
		};

		mma(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type a_type, ::framework::gpu::component_type b_type, ::framework::gpu::component_type c_type, ::framework::gpu::component_type d_type);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
	};

	class ::framework::algorithm::mma::executor
	{
	public:
		executor(::framework::algorithm::mma & mma, ::framework::gpu_context * gpu_context, ::std::uint32_t m, ::std::uint32_t n, ::std::uint32_t k, ::framework::algorithm::mma::matrix_layout matrix_layout_a, ::framework::algorithm::mma::matrix_layout matrix_layout_b, ::framework::algorithm::mma::matrix_layout matrix_layout_c, ::framework::algorithm::mma::matrix_layout matrix_layout_d, float alpha, float beta);

		void execute(::framework::algorithm::mma & mma, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

	private:
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::pipeline * split_k_sum_pipeline;
		// MxNxK is the size of the full matrix multiply
		::std::uint32_t m;
		::std::uint32_t n;
		::std::uint32_t k;
		// Each cooperative matrix multiply is lMxlNxlK
		::std::uint32_t l_m;
		::std::uint32_t l_n;
		::std::uint32_t l_k;
		// size of workgroup tile in destination matrix
		::std::uint32_t tile_m;
		::std::uint32_t tile_n;
		::std::uint32_t tile_k;
		::std::uint32_t split_k_slices;
		::framework::algorithm::mma::matrix_layout matrix_layout_a;
		::framework::algorithm::mma::matrix_layout matrix_layout_b;
		::framework::algorithm::mma::matrix_layout matrix_layout_c;
		::framework::algorithm::mma::matrix_layout matrix_layout_d;
	};
}

#include "algorithm/mma.inl"