﻿#pragma once

#include "gpu/core.hpp"

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	struct hybrid_radix_sort_create_info
	{
		typename ::framework::resource::resource_manager * resource_manager;
		::std::uint32_t		maxSize;
		::std::uint32_t		maxSortBits;
		class ::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		class ::framework::gpu::buffer * pInoutKeysBuffer;
		class ::framework::gpu::buffer * pInoutValuesBuffer;
	};

	/* Implementation of A Memory Bandwidth-Efficient Hybrid Radix Sort on GPUs, https://arxiv.org/pdf/1611.01137.pdf */
	class hybrid_radix_sort
	{
	public:
		static void allocate(::framework::algorithm::hybrid_radix_sort_create_info const & create_info, ::framework::algorithm::hybrid_radix_sort *& data);

		static void deallocate(::framework::algorithm::hybrid_radix_sort * pData);

		void setSortBitsCount(::std::uint32_t sortBits);

		void setSize(::std::uint32_t size);

		void execute(class ::framework::gpu::queue * command_queue, class ::framework::gpu::semaphore * wait_semaphore, ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, class ::framework::gpu::semaphore * signal_semaphore, class ::framework::gpu::fence * fence);

	protected:
		struct BlockAssignment
		{
			::std::uint32_t k_offs;
			::std::uint32_t k_count;
			::std::uint32_t b_id;
		};

		struct BucketAssignment
		{
			::std::uint32_t k_offs;
			::std::uint32_t k_count;
			::std::uint32_t is_merged;
		};

		struct HybridRadixSortUB
		{
			::std::uint32_t size;
			::std::uint32_t nWGs;
			::std::uint32_t startBit;
			::std::uint32_t nBuckets;
			::std::uint32_t nScanBlocksPerWG;
			::std::uint32_t _pad[59];
		};

		enum
		{
			WG_SIZE = 256,
			KPT = 9, // number of keys per worker_thread
			KPB = WG_SIZE * KPT, // number of keys per block
			SCAN_NUM_WIS_PER_BLOCK = 40, // up to SCAN_WG_SIZE
			SCAN_NUM_WGS = 1024
		};

		void setSize(::std::uint32_t size, ::std::uint32_t startBit);
		void buildCommandBuffers();

	private:
		::std::uint64_t					m_n;
		::std::uint32_t					m_k = 32; // number of bits per key
		::std::uint32_t					m_d = 8; // number of bits per digit
		::std::uint32_t					m_lst = 256 * 18; // threshold for local sorting
		::std::uint32_t					m_mbt = m_lst/*4608*/; // threshold for merging buckets
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::framework::gpu::physical_device * physical_device;
		class ::framework::gpu::device * device;
		class ::framework::gpu::command_pool * command_pool;
		class ::framework::gpu::command_buffer * command_buffers[3]; // begin, pair for swapping usage
		class ::framework::gpu::fence * d3d12_fence;
		class ::framework::gpu::semaphore * m_pSemaphore;
		class ::framework::gpu::shader_module * m_pStreamCountBeginShader;
		class ::framework::gpu::shader_module * m_pStreamCountShader;
		class ::framework::gpu::shader_module * m_pBucketScanAndMergeShader;
		class ::framework::gpu::shader_module * m_pPrepareSortingPassDataShader;
		class ::framework::gpu::shader_module * m_pSortAndScatterBeginShader;
		class ::framework::gpu::shader_module * m_pSortAndScatterShader;
		class ::framework::gpu::shader_module * m_pLocalSortShader;
		class ::framework::gpu::pipeline * m_pStreamCountBeginPso;
		class ::framework::gpu::pipeline * m_pStreamCountPso;
		class ::framework::gpu::pipeline * m_pBucketScanAndMergePso;
		class ::framework::gpu::pipeline * m_pPrepareSortingPassDataPso;
		class ::framework::gpu::pipeline * m_pSortAndScatterBeginPso;
		class ::framework::gpu::pipeline * m_pSortAndScatterPso;
		class ::framework::gpu::pipeline * m_pLocalSortPso;
		class ::framework::gpu::pipeline_layout * m_pStreamCountPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pBucketScanAndMergePipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pPrepareSortingPassDataPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pSortAndScatterPipelineLayout;
		class ::framework::gpu::pipeline_layout * m_pLocalSortPipelineLayout;
		class ::framework::gpu::descriptor_set_layout * m_pStreamCountDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pBucketScanAndMergeDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pPrepareSortingPassDataDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pSortAndScatterDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout * m_pLocalSortDescriptorSetLayout;
		class ::framework::gpu::descriptor_pool * descriptor_pool;
		class ::framework::gpu::descriptor_set * m_pStreamCountDescriptorSets[2];
		class ::framework::gpu::descriptor_set * m_pBucketScanAndMergeDescriptorSet[2];
		class ::framework::gpu::descriptor_set * m_pPrepareSortingPassDataDescriptorSet;
		class ::framework::gpu::descriptor_set * m_pSortAndScatterDescriptorSets[2];
		class ::framework::gpu::descriptor_set * m_pLocalSortDescriptorSets[2];
		class ::framework::gpu::buffer * m_pInoutKeysBuffer;
		class ::framework::gpu::buffer * m_pInoutValuesBuffer;
		class ::framework::gpu::buffer * m_pWorkBuffer0; // keys
		class ::framework::gpu::device_memory * m_pWorkBuffer0DeviceMemory;
		class ::framework::gpu::buffer * m_pWorkBuffer1; // values
		class ::framework::gpu::device_memory * m_pWorkBuffer1DeviceMemory;
		class ::framework::gpu::buffer * m_pBucketHistogramDataBuffer[2];
		class ::framework::gpu::device_memory * m_pBucketHistogramDeviceMemory[2];
		class ::framework::gpu::buffer * m_pSubBucketHistogramBuffer;
		class ::framework::gpu::device_memory * m_pSubBucketHistogramDeviceMemory;
		class ::framework::gpu::buffer * m_pBucketOffsetBuffer;
		class ::framework::gpu::device_memory * m_pBucketOffsetDeviceMemory;
		class ::framework::gpu::buffer * m_pBlockHistogramBuffer;
		class ::framework::gpu::device_memory * m_pBlockHistogramDeviceMemory;
		class ::framework::gpu::buffer * m_pBlockAssignmentsBuffer[2];
		class ::framework::gpu::device_memory * m_pBlockAssignmentsDeviceMemory[2];
		class ::framework::gpu::buffer * m_pBucketAssignmentsBuffer[2];
		class ::framework::gpu::device_memory * m_pBucketAssignmentsDeviceMemory[2];
		class ::framework::gpu::buffer * m_pConstBuffer;
		class ::framework::gpu::device_memory * m_pConstBufferMemory;
		class ::framework::gpu::buffer * m_pDispatchIndirectCommandsBuffer;
		class ::framework::gpu::device_memory * m_pDispatchIndirectCommandsBufferDeviceMemory;
		::std::uint32_t queue_family_index;
	};
}