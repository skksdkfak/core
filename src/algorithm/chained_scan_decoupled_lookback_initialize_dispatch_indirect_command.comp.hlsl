#include "algorithm/gemm.hlsl"
#include "gpu/gpu.hlsl"

namespace framework
{
	namespace algorithm
	{
		namespace scan
		{
			struct constant_buffer
			{
				uint32_t vectorized_size;
				uint32_t thread_blocks;
			};
		}
	}
}

[[vk::binding(0, 0)]] ByteAddressBuffer size_buffer : register(t0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<::framework::algorithm::scan::constant_buffer> constant_buffer : register(u1, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	uint32_t const size = size_buffer.Load(0);
	uint32_t const max_dim = 65535;
	uint32_t const partition_size = 3072;
	uint32_t const aligned_size = (size + 3) / 4 * 4;
	uint32_t const vectorized_size = aligned_size / 4;
	uint32_t const partitions = (aligned_size + partition_size - 1) / partition_size;
	uint32_t const full_blocks = partitions / max_dim;
	uint32_t const partial_blocks = partitions - full_blocks * max_dim;

	::framework::algorithm::scan::constant_buffer constants;
	constants.vectorized_size = vectorized_size;
	constants.thread_blocks = partitions;
	constant_buffer[0] = constants;

	// full_blocks
	dispatch_indirect_command_buffer[0] = ::framework::gpu::dispatch_indirect_command::create(max_dim, full_blocks, 1);
	// partial_blocks
	dispatch_indirect_command_buffer[1] = ::framework::gpu::dispatch_indirect_command::create(partial_blocks, 1, 1);
}
