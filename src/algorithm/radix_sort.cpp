#include "radix_sort.h"
#include "resource/resource_manager.hpp"
#include "gpu/utility.hpp"
#include <iostream>
#include <algorithm>

void ::framework::algorithm::lsd_radix_sort::allocate(::framework::algorithm::lsd_radix_sort_create_info const & create_info, Data ** data)
{
	Data * out_data = new Data;
	out_data->m_maxSize = create_info.maxSize;
	out_data->m_maxSortBits = create_info.maxSortBits;
	out_data->physical_device = create_info.physical_device;
	out_data->device = create_info.device;
	//out_data->m_pStreamCountShader = create_info.resource_manager->load_shader_module(create_info.physical_device, create_info.device, "StreamCount.sdr");
	//out_data->m_pPrefixScanShader = create_info.resource_manager->load_shader_module(create_info.physical_device, create_info.device, "PrefixScan.sdr");
	//out_data->m_pSortAndScatterShader = create_info.resource_manager->load_shader_module(create_info.physical_device, create_info.device, "SortAndScatter.sdr");

	::framework::gpu::physical_device_memory_properties memoryProperties;
	create_info.physical_device->get_memory_properties(&memoryProperties);

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
	pipeline_shader_stage_create_infos[0].module = out_data->m_pStreamCountShader;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = out_data->m_pPrefixScanShader;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = out_data->m_pSortAndScatterShader;
	pipeline_shader_stage_create_infos[2].name = "main";
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;

	// StreamCount DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// srcKeys
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// histogram
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		create_info.device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &out_data->m_pStreamCountDescriptorSetLayout);
	}

	// PrefixScan DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// histogram
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		create_info.device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &out_data->m_pPrefixScanDescriptorSetLayout);
	}

	// SortAndScatter DescriptorSetLayout
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// UBO consts
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// histogram
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// srcKeys
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// srcValues
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// dstKeys
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// dstValues
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		create_info.device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &out_data->m_pSortAndScatterDescriptorSetLayout);
	}

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[3];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer;
	descriptor_pool_sizes[0].descriptor_count = 5;

	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_buffer;
	descriptor_pool_sizes[1].descriptor_count = 8;

	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[2].descriptor_count = 7;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
	descriptor_pool_create_info.max_sets = 5;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	create_info.device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &out_data->descriptor_pool);

	::framework::gpu::descriptor_set_layout * descriptor_set_layouts[] =
	{
		out_data->m_pStreamCountDescriptorSetLayout,
		out_data->m_pPrefixScanDescriptorSetLayout,
		out_data->m_pSortAndScatterDescriptorSetLayout
	};

	::framework::gpu::descriptor_set * descriptor_sets[3];

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = out_data->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
	descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	create_info.device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);

	out_data->m_pStreamCountDescriptorSets[0] = descriptor_sets[0];
	out_data->m_pPrefixScanDescriptorSet = descriptor_sets[1];
	out_data->m_pSortAndScatterDescriptorSets[0] = descriptor_sets[2];

	descriptor_set_layouts[0] = out_data->m_pStreamCountDescriptorSetLayout;
	descriptor_set_layouts[1] = out_data->m_pSortAndScatterDescriptorSetLayout;

	descriptor_set_allocate_info.descriptor_set_count = 2;
	create_info.device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);

	out_data->m_pStreamCountDescriptorSets[1] = descriptor_sets[0];
	out_data->m_pSortAndScatterDescriptorSets[1] = descriptor_sets[1];

	::framework::gpu::push_constant_range push_constant_ranges;
	push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
	push_constant_ranges.binding = 0;
	push_constant_ranges.offset = 0;
	push_constant_ranges.size = sizeof(::std::uint32_t);
	push_constant_ranges.hlsl_shader_register = 1;
	push_constant_ranges.hlsl_register_space = 0;
	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.push_constant_range_count = 1;
	pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;

	pipeline_layout_create_info.descriptor_set_layouts = &out_data->m_pStreamCountDescriptorSetLayout;
	create_info.device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &out_data->m_pStreamCountPipelineLayout);

	pipeline_layout_create_info.descriptor_set_layouts = &out_data->m_pPrefixScanDescriptorSetLayout;
	create_info.device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &out_data->m_pPrefixScanPipelineLayout);

	pipeline_layout_create_info.descriptor_set_layouts = &out_data->m_pSortAndScatterDescriptorSetLayout;
	create_info.device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &out_data->m_pSortAndScatterPipelineLayout);

	::framework::gpu::pipeline * pPipelines[3];

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[3];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = out_data->m_pStreamCountPipelineLayout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = out_data->m_pPrefixScanPipelineLayout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = out_data->m_pSortAndScatterPipelineLayout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;
	create_info.device->create_compute_pipelines(nullptr, 3, compute_pipeline_create_infos, nullptr, pPipelines);

	out_data->m_pStreamCountPso = pPipelines[0];
	out_data->m_pPrefixScanPso = pPipelines[1];
	out_data->m_pSortAndScatterPso = pPipelines[2];

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * create_info.maxSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = out_data->queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;

		{
			assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pWorkBuffer0));

			::framework::gpu::memory_requirements memory_requirements;

			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = out_data->m_pWorkBuffer0;

			create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pWorkBuffer0Memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = out_data->m_pWorkBuffer0;
			bind_buffer_memory_info.memory = out_data->m_pWorkBuffer0Memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));
		}

		{
			assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pWorkBuffer1));

			::framework::gpu::memory_requirements memory_requirements;

			::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
			buffer_memory_requirements_info.buffer = out_data->m_pWorkBuffer1;

			create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

			::framework::gpu::memory_allocate_info memory_allocate_info;
			memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
			memory_allocate_info.device_mask = 0;
			memory_allocate_info.allocation_size = memory_requirements.size;
			memory_allocate_info.allocation_alignment = memory_requirements.alignment;
			memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
			memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
			memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
			memory_allocate_info.opaque_capture_address = 0;
			assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pWorkBuffer1Memory));

			::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
			bind_buffer_memory_info.buffer = out_data->m_pWorkBuffer1;
			bind_buffer_memory_info.memory = out_data->m_pWorkBuffer1Memory;
			bind_buffer_memory_info.memory_offset = 0;
			assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));
		}
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * create_info.maxSize;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = out_data->queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pWorkBuffer2));

		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pWorkBuffer2Debug));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = out_data->m_pWorkBuffer2;

		create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pWorkBuffer2Memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = out_data->m_pWorkBuffer2;
		bind_buffer_memory_info.memory = out_data->m_pWorkBuffer2Memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));

		buffer_memory_requirements_info.buffer = out_data->m_pWorkBuffer2Debug;

		create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit);
		assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pWorkBuffer2DebugMemory));

		bind_buffer_memory_info.buffer = out_data->m_pWorkBuffer2Debug;
		bind_buffer_memory_info.memory = out_data->m_pWorkBuffer2DebugMemory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::std::size_t uboAlignment = 256;
		::std::size_t dynamicAlignment = (sizeof(RadixSortUB) / uboAlignment) * uboAlignment + ((sizeof(RadixSortUB) % uboAlignment) > 0 ? uboAlignment : 0);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = dynamicAlignment * (out_data->m_maxSortBits / BITS_PER_PASS);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = out_data->queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pConstBuffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = out_data->m_pConstBuffer;

		create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pConstBufferMemory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = out_data->m_pConstBuffer;
		bind_buffer_memory_info.memory = out_data->m_pConstBufferMemory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 3 * sizeof(::framework::gpu::dispatch_indirect_command);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = out_data->queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::indirect_argument_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->create_buffer(&buffer_create_info, nullptr, &out_data->m_pDispathIndirectCommandsBuffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = out_data->m_pDispathIndirectCommandsBuffer;

		create_info.device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(memoryProperties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(create_info.device->allocate_memory(&memory_allocate_info, nullptr, &out_data->m_pDispathIndirectCommandsBufferDeviceMemory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = out_data->m_pDispathIndirectCommandsBuffer;
		bind_buffer_memory_info.memory = out_data->m_pDispathIndirectCommandsBufferDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(create_info.device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[4];
	descriptor_buffer_infos[0].buffer = out_data->m_pConstBuffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = (sizeof(RadixSortUB) / 256) * 256 + ((sizeof(RadixSortUB) % 256) > 0 ? 256 : 0);
	descriptor_buffer_infos[0].stride = (sizeof(RadixSortUB) / 256) * 256 + ((sizeof(RadixSortUB) % 256) > 0 ? 256 : 0);

	descriptor_buffer_infos[1].buffer = out_data->m_pWorkBuffer0;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[2].buffer = out_data->m_pWorkBuffer1;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[3].buffer = out_data->m_pWorkBuffer2;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = sizeof(::std::uint32_t);

	// Setup StreamCountDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = out_data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = out_data->m_pStreamCountDescriptorSets[1];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];

		write_descriptor_sets[2].dst_set = out_data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[3];

		::framework::gpu::copy_descriptor_set copyDescriptorSets[2];
		copyDescriptorSets[0].src_set = out_data->m_pStreamCountDescriptorSets[0];
		copyDescriptorSets[0].src_binding = 0;
		copyDescriptorSets[0].src_array_element = 0;
		copyDescriptorSets[0].dst_set = out_data->m_pStreamCountDescriptorSets[1];
		copyDescriptorSets[0].dst_binding = 0;
		copyDescriptorSets[0].dst_array_element = 0;
		copyDescriptorSets[0].descriptor_count = 1;

		copyDescriptorSets[1].src_set = out_data->m_pStreamCountDescriptorSets[0];
		copyDescriptorSets[1].src_binding = 2;
		copyDescriptorSets[1].src_array_element = 0;
		copyDescriptorSets[1].dst_set = out_data->m_pStreamCountDescriptorSets[1];
		copyDescriptorSets[1].dst_binding = 2;
		copyDescriptorSets[1].dst_array_element = 0;
		copyDescriptorSets[1].descriptor_count = 1;

		create_info.device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		create_info.device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
	}

	// Setup PrefixScanDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = out_data->m_pPrefixScanDescriptorSet;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = out_data->m_pPrefixScanDescriptorSet;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[3];

		create_info.device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}

	// Setup SortAndScatterDescriptorSet
	{
		::framework::gpu::write_descriptor_set write_descriptor_sets[6];
		write_descriptor_sets[0].dst_set = out_data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = out_data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[2].dst_set = out_data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[2].dst_binding = 4;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[3].dst_set = out_data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[3].dst_binding = 5;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[4].dst_set = out_data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[4].dst_binding = 2;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[5].dst_set = out_data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[5].dst_binding = 3;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[2];

		::framework::gpu::copy_descriptor_set copyDescriptorSets[2];
		copyDescriptorSets[0].src_set = out_data->m_pSortAndScatterDescriptorSets[0];
		copyDescriptorSets[0].src_binding = 0;
		copyDescriptorSets[0].src_array_element = 0;
		copyDescriptorSets[0].dst_set = out_data->m_pSortAndScatterDescriptorSets[1];
		copyDescriptorSets[0].dst_binding = 0;
		copyDescriptorSets[0].dst_array_element = 0;
		copyDescriptorSets[0].descriptor_count = 1;
		copyDescriptorSets[1].dst_set = out_data->m_pSortAndScatterDescriptorSets[1];
		copyDescriptorSets[1].dst_binding = 1;
		copyDescriptorSets[1].dst_array_element = 0;
		copyDescriptorSets[1].src_set = out_data->m_pSortAndScatterDescriptorSets[0];
		copyDescriptorSets[1].src_binding = 1;
		copyDescriptorSets[1].src_array_element = 0;
		copyDescriptorSets[1].descriptor_count = 1;

		create_info.device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
		create_info.device->update_descriptor_sets(0, nullptr, ::std::size(copyDescriptorSets), copyDescriptorSets);
	}

	{
		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		out_data->device->create_command_pool(&command_pool_create_info, nullptr, &out_data->command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = out_data->command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		out_data->device->allocate_command_buffers(&command_buffer_allocate_info, &out_data->command_buffer);
	}

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
	out_data->device->create_fence(&fence_create_info, nullptr, &out_data->d3d12_fence);

	*data = out_data;
}

void ::framework::algorithm::lsd_radix_sort::deallocate(Data * pData)
{
	pData->device->free_command_buffers(pData->command_pool, 1, &pData->command_buffer);
	pData->device->destroy_command_pool(pData->command_pool, nullptr);
	pData->device->destroy_pipeline(pData->m_pStreamCountPso, nullptr);
	pData->device->destroy_pipeline(pData->m_pPrefixScanPso, nullptr);
	pData->device->destroy_pipeline(pData->m_pSortAndScatterPso, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pStreamCountDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pPrefixScanDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_set_layout(pData->m_pSortAndScatterDescriptorSetLayout, nullptr);
	pData->device->destroy_descriptor_pool(pData->descriptor_pool, nullptr);
	pData->device->destroy_descriptor_pool(pData->device_descriptor_pool, nullptr);
	pData->device->destroy_buffer(pData->m_pWorkBuffer0, nullptr);
	pData->device->free_memory(pData->m_pWorkBuffer0Memory, nullptr);
	pData->device->destroy_buffer(pData->m_pWorkBuffer1, nullptr);
	pData->device->free_memory(pData->m_pWorkBuffer1Memory, nullptr);
	pData->device->destroy_buffer(pData->m_pWorkBuffer2, nullptr);
	pData->device->free_memory(pData->m_pWorkBuffer2Memory, nullptr);
	pData->device->destroy_buffer(pData->m_pConstBuffer, nullptr);
	pData->device->free_memory(pData->m_pConstBufferMemory, nullptr);
	pData->device->destroy_buffer(pData->m_pDispathIndirectCommandsBuffer, nullptr);
	pData->device->free_memory(pData->m_pDispathIndirectCommandsBufferDeviceMemory, nullptr);

	delete pData;
}

void ::framework::algorithm::lsd_radix_sort::configure(Data * data, ::framework::gpu::buffer * pInoutKeysBuffer, ::framework::gpu::buffer * pInoutValuesBuffer, ::std::uint32_t sortBits)
{
	data->inout_keys_buffer = pInoutKeysBuffer;
	::framework::gpu::buffer * srcKeys = pInoutKeysBuffer;
	::framework::gpu::buffer * srcValues = pInoutValuesBuffer;
	::framework::gpu::buffer * dstKeys = data->m_pWorkBuffer0;
	::framework::gpu::buffer * dstValues = data->m_pWorkBuffer1;
	::framework::gpu::buffer * histogramBuffer = data->m_pWorkBuffer2;

	{
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = srcKeys;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
		write_descriptor_sets[0].dst_set = data->m_pStreamCountDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 1;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}
	{
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = srcKeys;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);
		descriptor_buffer_infos[1].buffer = srcValues;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);

		::framework::gpu::write_descriptor_set write_descriptor_sets[4];
		write_descriptor_sets[0].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[0].dst_binding = 2;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[1].dst_set = data->m_pSortAndScatterDescriptorSets[0];
		write_descriptor_sets[1].dst_binding = 3;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[2].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[2].dst_binding = 4;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[3].dst_set = data->m_pSortAndScatterDescriptorSets[1];
		write_descriptor_sets[3].dst_binding = 5;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[1];

		data->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	command_buffer_begin_info.inheritance_info = nullptr;
	data->command_buffer->begin_command_buffer(&command_buffer_begin_info);
	::std::uint32_t currentSet = 0, lastSet = 1;
	for (::std::uint32_t ib = 0; ib < sortBits; ib += BITS_PER_PASS)
	{
		::std::uint32_t const pushConstants[1] = { ib };

		{// stream count
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[0].buffer = histogramBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[1].buffer = srcKeys;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			data->command_buffer->pipeline_barrier(&dependency_info);
			data->command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, data->m_pStreamCountPso);
			data->command_buffer->bind_descriptor_pool(data->device_descriptor_pool);
			data->command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, data->m_pStreamCountPipelineLayout, 0, 1, &data->m_pStreamCountDescriptorSets[currentSet], 0, nullptr);
			data->command_buffer->push_constants(data->m_pStreamCountPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, 0);
		}
		{//	prefix scan group histogram
			::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
			buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barrier.buffer = histogramBuffer;
			buffer_memory_barrier.offset = 0;
			buffer_memory_barrier.size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 1;
			dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			data->command_buffer->pipeline_barrier(&dependency_info);
			data->command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, data->m_pPrefixScanPso);
			data->command_buffer->bind_descriptor_pool(data->device_descriptor_pool);
			data->command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, data->m_pPrefixScanPipelineLayout, 0, 1, &data->m_pPrefixScanDescriptorSet, 0, nullptr);
			data->command_buffer->push_constants(data->m_pPrefixScanPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, sizeof(::framework::gpu::dispatch_indirect_command));
		}
		{//	local sort and distribute
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[4];
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_write_bit | ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[0].buffer = histogramBuffer;
			buffer_memory_barriers[0].offset = 0;
			buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[1].buffer = srcValues;
			buffer_memory_barriers[1].offset = 0;
			buffer_memory_barriers[1].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[2].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[2].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[2].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[2].buffer = dstKeys;
			buffer_memory_barriers[2].offset = 0;
			buffer_memory_barriers[2].size = ::framework::gpu::whole_size;

			buffer_memory_barriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
			buffer_memory_barriers[3].old_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[3].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barriers[3].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].src_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[3].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barriers[3].dst_queue_family_ownership.queue_family_index = data->queue_family_index;
			buffer_memory_barriers[3].buffer = dstValues;
			buffer_memory_barriers[3].offset = 0;
			buffer_memory_barriers[3].size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = ::std::size(buffer_memory_barriers);
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;

			data->command_buffer->pipeline_barrier(&dependency_info);
			data->command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, data->m_pSortAndScatterPso);
			data->command_buffer->bind_descriptor_pool(data->device_descriptor_pool);
			data->command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, data->m_pSortAndScatterPipelineLayout, 0, 1, &data->m_pSortAndScatterDescriptorSets[currentSet], 0, nullptr);
			data->command_buffer->push_constants(data->m_pSortAndScatterPipelineLayout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(pushConstants), &pushConstants);
			data->command_buffer->dispatch_indirect(data->m_pDispathIndirectCommandsBuffer, 2 * sizeof(::framework::gpu::dispatch_indirect_command));
		}
		::std::swap(currentSet, lastSet);
		::std::swap(srcKeys, dstKeys);
		::std::swap(srcValues, dstValues);
	}
	data->command_buffer->end_command_buffer();
}

void ::framework::algorithm::lsd_radix_sort::setSize(Data * data, ::std::uint64_t size)
{
	int nAligned = size;
	{
		int tmp;
		if ((tmp = nAligned % 256) != 0)
			nAligned += nAligned > -1 ? (256 - tmp) : -tmp;
	}

	::std::uint32_t const nBlocks = (size + ELEMENTS_PER_WORK_ITEM * WG_SIZE - 1) / (ELEMENTS_PER_WORK_ITEM * WG_SIZE);
	::std::uint32_t const nWGs = ::std::min(nBlocks, static_cast<::std::uint32_t>(NUM_WGS));

	RadixSortUB radixSortUB;
	radixSortUB.m_n = size;
	radixSortUB.m_nAligned = nAligned;
	radixSortUB.m_nWGs = NUM_WGS;
	radixSortUB.m_nBlocksPerWG = nBlocks < NUM_WGS ? 1 : (nBlocks + radixSortUB.m_nWGs - 1) / radixSortUB.m_nWGs;

	::std::size_t const uboAlignment = 256;
	::std::size_t const dynamicAlignment = (sizeof(RadixSortUB) / uboAlignment) * uboAlignment + ((sizeof(RadixSortUB) % uboAlignment) > 0 ? uboAlignment : 0);

	void * mappedData;
	assert_framework_gpu_result(data->device->map_memory(data->m_pConstBufferMemory, 0, dynamicAlignment, ::framework::gpu::memory_map_flags::none, &mappedData));
	::std::memcpy(static_cast<char *>(mappedData), &radixSortUB, sizeof(RadixSortUB));
	data->device->unmap_memory(data->m_pConstBufferMemory);

	::framework::gpu::dispatch_indirect_command dispatchIndirectCommand;
	void * mappedData1;
	data->device->map_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory, 0, sizeof(::framework::gpu::dispatch_indirect_command) * 3, ::framework::gpu::memory_map_flags::none, &mappedData1);
	dispatchIndirectCommand = { NUM_WGS, 1, 1 };
	::std::memcpy(static_cast<char *>(mappedData1), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
	dispatchIndirectCommand = { 1, 1, 1 };
	::std::memcpy(static_cast<char *>(mappedData1) + sizeof(::framework::gpu::dispatch_indirect_command), &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
	dispatchIndirectCommand = { nWGs, 1, 1 };
	::std::memcpy(static_cast<char *>(mappedData1) + sizeof(::framework::gpu::dispatch_indirect_command) * 2, &dispatchIndirectCommand, sizeof(::framework::gpu::dispatch_indirect_command));
	data->device->unmap_memory(data->m_pDispathIndirectCommandsBufferDeviceMemory);
}

void ::framework::algorithm::lsd_radix_sort::execute(Data * data, ::framework::gpu::queue * command_queue, ::framework::gpu::semaphore * wait_semaphore, ::framework::gpu::pipeline_stage_flags wait_dst_stage_mask, ::framework::gpu::semaphore * signal_semaphore, ::framework::gpu::fence * fence)
{
	::framework::gpu::semaphore_submit_info wait_semaphore_infos;
	wait_semaphore_infos.semaphore = wait_semaphore;
	wait_semaphore_infos.value = 0;
	wait_semaphore_infos.stage_mask = wait_dst_stage_mask;
	wait_semaphore_infos.device_index = 0;

	::framework::gpu::command_buffer_submit_info command_buffer_infos;
	command_buffer_infos.command_buffer = data->command_buffer;
	command_buffer_infos.device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_infos;
	signal_semaphore_infos.semaphore = signal_semaphore;
	signal_semaphore_infos.value = 0;
	signal_semaphore_infos.stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	signal_semaphore_infos.device_index = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = wait_semaphore ? 1 : 0;
	submit_info.wait_semaphore_infos = &wait_semaphore_infos;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_infos;
	submit_info.signal_semaphore_info_count = signal_semaphore ? 1 : 0;
	submit_info.signal_semaphore_infos = &signal_semaphore_infos;
	command_queue->submit(1, &submit_info, fence);

	// Readback from gpu
	if (false)
	{
		command_queue->wait_idle();

		::framework::gpu::queue * transferQueue;
		data->device->get_queue(0, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = 0;
		::framework::gpu::command_pool * command_pool;
		data->device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.command_buffer_count = 1;
		::framework::gpu::command_buffer * copyCmd;

		data->device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);
		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		copyCmd->begin_command_buffer(&command_buffer_begin_info);

#if 0
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * (NUM_WGS * (1 << BITS_PER_PASS));
		copyCmd->copy_buffer(data->m_pWorkBuffer2, data->m_pWorkBuffer2Debug, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		data->device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_infos;
		command_buffer_infos.command_buffer = copyCmd;
		command_buffer_infos.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_infos;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

		data->device->wait_for_fences(1, &fence, true, UINT64_MAX);
		data->device->destroy_fence(fence);

		::std::vector<::std::uint32_t> dataVec(NUM_WGS * (1 << BITS_PER_PASS));

		void * pMappedData;
		data->device->map_memory(data->m_pWorkBuffer2DebugMemory, 0, buffer_copy.size, ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(dataVec.data(), pMappedData, buffer_copy.size);
		data->device->unmap_memory(data->m_pWorkBuffer2DebugMemory);

		for (::std::size_t i = 0; i < dataVec.size(); i++)
			::std::cout << /*i << "= " <<*/ dataVec[i] << (((i + 1) % 120) ? ", " : "\n");
		system("pause");
#elif 1
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(::std::uint32_t) * (1 << 15);
		copyCmd->copy_buffer(data->inout_keys_buffer, data->m_pWorkBuffer2Debug, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		data->device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_infos;
		command_buffer_infos.command_buffer = copyCmd;
		command_buffer_infos.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_infos;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

		data->device->wait_for_fences(1, &fence, true, UINT64_MAX);
		data->device->destroy_fence(fence, nullptr);

		::std::vector<::std::uint32_t> dataVec((1 << 15));

		void * pMappedData;
		data->device->map_memory(data->m_pWorkBuffer2DebugMemory, 0, buffer_copy.size, ::framework::gpu::memory_map_flags::none, &pMappedData);
		::std::memcpy(dataVec.data(), pMappedData, buffer_copy.size);
		data->device->unmap_memory(data->m_pWorkBuffer2DebugMemory);

		for (::std::size_t i = 1; i < dataVec.size(); i++)
		{
			if (dataVec[i - 1] > dataVec[i])
			{
				//__debugbreak();
			}
		}
		//system("pause");
#endif
	}
}