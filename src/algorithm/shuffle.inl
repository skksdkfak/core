inline ::framework::gpu::descriptor_set_layout * ::framework::algorithm::shuffle::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::algorithm::scan const & ::framework::algorithm::shuffle::get_scan() const noexcept
{
	return this->scan;
}

inline ::framework::algorithm::scan::executor const & ::framework::algorithm::shuffle::executor::get_scan_executor() const noexcept
{
	return this->scan_executor;
}