#include "algorithm/scan.hpp"
#include "gpu/utility.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>

namespace framework::local
{
	namespace
	{
		struct push_constants
		{
			::std::uint32_t vectorized_size;
			::std::uint32_t thread_blocks;
		};
	}
}

::framework::algorithm::scan::scan(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::descriptor_set_layout * descriptor_set_layout, char const * scan_executor_type_name, ::std::filesystem::path const & scen_executor_path)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// constant_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// index_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// thread_block_reduction_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// constant_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_descriptor_set_layout));
	}

	::framework::resource::preprocessor_define const preprocessor_defines[]
	{
		{ "current_scan_executor_t", scan_executor_type_name }
	};
	
	::framework::resource::include_override const include_overrides[]
	{
		{ "custom_include.h", scen_executor_path }
	};

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->descriptor_set_layout,
			descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->initialize_dispatch_indirect_command_descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline_layout));
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/chained_scan_decoupled_lookback_initialize_dispatch_indirect_command.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->initialize_dispatch_indirect_command_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/chained_scan_decoupled_lookback.comp.hlsl";
		hlsl_source_info.source_entry_point = "chained_scan_decoupled_lookback_initialize";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "chained_scan_decoupled_lookback_initialize";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->chained_scan_decoupled_lookback_initialize_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/chained_scan_decoupled_lookback.comp.hlsl";
		hlsl_source_info.source_entry_point = "chained_scan_decoupled_lookback_exclusive";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "chained_scan_decoupled_lookback_exclusive";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->chained_scan_decoupled_lookback_exclusive_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "algorithm/chained_scan_decoupled_lookback.comp.hlsl";
		hlsl_source_info.source_entry_point = "chained_scan_decoupled_lookback_inclusive";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = static_cast<::std::uint32_t>(::std::size(include_overrides));
		shader_stage_info.include_overrides = include_overrides;
		shader_stage_info.entry_point = "chained_scan_decoupled_lookback_inclusive";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->chained_scan_decoupled_lookback_inclusive_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}
}

::framework::algorithm::scan::executor::executor(::framework::algorithm::scan & scan, ::framework::gpu_context * gpu_context)
{
	::framework::gpu::pipeline * pipelines[4];

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[4];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = scan.chained_scan_decoupled_lookback_initialize_shader_module;
	pipeline_shader_stage_create_infos[0].name = "chained_scan_decoupled_lookback_initialize";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = scan.chained_scan_decoupled_lookback_exclusive_shader_module;
	pipeline_shader_stage_create_infos[1].name = "chained_scan_decoupled_lookback_exclusive";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = scan.chained_scan_decoupled_lookback_inclusive_shader_module;
	pipeline_shader_stage_create_infos[2].name = "chained_scan_decoupled_lookback_inclusive";
	pipeline_shader_stage_create_infos[2].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[3].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[3].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[3].module = scan.initialize_dispatch_indirect_command_shader_module;
	pipeline_shader_stage_create_infos[3].name = "main";
	pipeline_shader_stage_create_infos[3].specialization_info = nullptr;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[4];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = scan.pipeline_layout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = scan.pipeline_layout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = scan.pipeline_layout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	compute_pipeline_create_infos[3].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[3].stage = pipeline_shader_stage_create_infos[3];
	compute_pipeline_create_infos[3].layout = scan.initialize_dispatch_indirect_command_pipeline_layout;
	compute_pipeline_create_infos[3].base_pipeline = nullptr;
	compute_pipeline_create_infos[3].base_pipeline_index = -1;

	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines));
	this->chained_scan_decoupled_lookback_initialize_pipeline = pipelines[0], this->chained_scan_decoupled_lookback_exclusive_pipeline = pipelines[1], this->chained_scan_decoupled_lookback_inclusive_pipeline = pipelines[2], this->initialize_dispatch_indirect_command_pipeline = pipelines[3];
}

void ::framework::algorithm::scan::executor::initialize_dispatch_indirect_command(::framework::algorithm::scan & scan, ::framework::gpu_context * gpu_context, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(1, 1, 1);
}

void ::framework::algorithm::scan::executor::execute(::framework::algorithm::scan & scan, ::framework::gpu::command_buffer * command_buffer, ::std::span<::framework::gpu::descriptor_set * const, 2> descriptor_sets, ::std::uint32_t size)
{
	::std::uint32_t const max_dim = 65535;
	::std::uint32_t const partition_size = 3072;
	::std::uint32_t const aligned_size = (size + 3) / 4 * 4;
	::std::uint32_t const vectorized_size = aligned_size / 4;
	::std::uint32_t const partitions = (aligned_size + partition_size - 1) / partition_size;
	::std::uint32_t const full_blocks = partitions / max_dim;
	::std::uint32_t const partial_blocks = partitions - full_blocks * max_dim;

	{
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_initialize_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), 0, nullptr);
		command_buffer->dispatch(256, 1, 1);
	}

	if (full_blocks)
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		command_buffer->pipeline_barrier(&dependency_info);
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_exclusive_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), 0, nullptr);
		command_buffer->dispatch(max_dim, full_blocks, 1);

	}
	if (partial_blocks)
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		command_buffer->pipeline_barrier(&dependency_info);
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_exclusive_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), 0, nullptr);
		command_buffer->dispatch(partial_blocks, 1, 1);
	}
}

void ::framework::algorithm::scan::executor::execute_indirect(::framework::algorithm::scan & scan, ::framework::gpu::command_buffer * command_buffer, ::std::span<::framework::gpu::descriptor_set * const, 2> descriptor_sets, ::std::uint32_t dynamic_offset_count, ::std::uint32_t const * dynamic_offsets, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	{
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_initialize_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), dynamic_offset_count, dynamic_offsets);
		command_buffer->dispatch(256, 1, 1);
	}

	// full_blocks
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		command_buffer->pipeline_barrier(&dependency_info);
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_exclusive_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), 0, nullptr);
		command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);

	}
	// partial_blocks
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		command_buffer->pipeline_barrier(&dependency_info);
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->chained_scan_decoupled_lookback_exclusive_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, scan.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), ::std::data(descriptor_sets), 0, nullptr);
		command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset + sizeof(::framework::gpu::dispatch_indirect_command));
	}
}

void ::framework::algorithm::scan::executor::get_memory_requirements(::std::uint32_t size, ::std::uint32_t & index_buffer_size) const noexcept
{
	::std::uint32_t const partition_size = 3072;
	::std::uint32_t const size_alignment = 4;
	::std::uint32_t const aligned_size = (size + size_alignment - 1) / size_alignment * size_alignment;
	index_buffer_size = (aligned_size + partition_size - 1) / partition_size;
}