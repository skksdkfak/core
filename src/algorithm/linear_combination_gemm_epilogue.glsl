bool is_source_needed()
{
    return device_parameters_buffer.beta != 0.0f;
}

coopmat<D_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> gemm_epilogue(coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> accumulator, coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> source)
{
    coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> result = ACCUMULATOR_TYPE(device_parameters_buffer.alpha) * accumulator;
    if (is_source_needed())
    {
        result += ACCUMULATOR_TYPE(device_parameters_buffer.beta) * source;
    }
    return coopmat<D_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>(result);
}