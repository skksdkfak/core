
[[vk::binding(0, 1)]] RWStructuredBuffer<uint4> scan_buffer : register(u0, space1);

namespace framework
{
    struct default_scan_executor
    {
        inline void initialize()
        {
        }

        inline uint4 load(uint global_index, uint local_index)
        {
            return scan_buffer[global_index];
        }

        inline void store(uint global_index, uint local_index, uint4 data)
        {
            scan_buffer[global_index] = data;
        }
    };
}