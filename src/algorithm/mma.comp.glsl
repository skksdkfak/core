#version 450 core

#pragma use_vulkan_memory_model

#extension GL_KHR_shader_subgroup_basic : enable
#extension GL_EXT_scalar_block_layout : enable
#extension GL_KHR_memory_scope_semantics : enable
#extension GL_KHR_cooperative_matrix : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int8 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int32 : enable
#extension GL_EXT_control_flow_attributes : enable

layout(constant_id = 0) const uint l_m = 1;
layout(constant_id = 1) const uint l_n = 1;
layout(constant_id = 2) const uint l_k = 1;
layout(constant_id = 3) const uint tile_m = 1;
layout(constant_id = 4) const uint tile_n = 1;
layout(constant_id = 5) const uint tile_k = 1;
layout(constant_id = 6) const uint m_n = 1;
layout(constant_id = 7) const uint K = 1;
layout(constant_id = 8) const uint stride_a = 1;
layout(constant_id = 9) const uint stride_b = 1;
layout(constant_id = 10) const uint stride_c = 1;
layout(constant_id = 11) const uint stride_d = 1;
layout(constant_id = 12) const float alpha = 1.0f;
layout(constant_id = 13) const float beta = 1.0f;
layout(constant_id = 14) const int layout_a = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 15) const int layout_b = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 16) const int layout_c = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 17) const int layout_d = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 18) const uint a_tile_row_len = 1;
layout(constant_id = 19) const uint a_tile_row_count = 1;
layout(constant_id = 20) const uint b_tile_row_len = 1;
layout(constant_id = 21) const uint b_tile_row_count = 1;
layout(constant_id = 22) const uint workgroup_width_in_subgroups = 1;
layout(constant_id = 23) const uint workgroup_height_in_subgroups = 1;
layout(constant_id = 24) const uint invocations_per_workgroup = 1;
layout(constant_id = 25) const uint k_per_workgroup = 1;

layout(std430, set = 0, binding = 0) buffer matrix_a_t
{
    A_TYPE matrix_a[];
};

layout(std430, set = 0, binding = 1) buffer matrix_b_t
{
    B_TYPE matrix_b[];
};

layout(std430, set = 0, binding = 2) buffer matrix_c_t
{
    C_TYPE matrix_c[];
};

layout(std430, set = 0, binding = 3) buffer matrix_d_t
{
    D_TYPE matrix_d[];
};

layout(local_size_x_id = 0, local_size_y_id = 1, local_size_z = 1) in;

uint coord_to_offset(uint i, uint j, uint stride, int matrix_layout)
{
    return matrix_layout == gl_CooperativeMatrixLayoutColumnMajor ? (stride * j + i) : (stride * i + j);
}

void main()
{
    const uint i = gl_GlobalInvocationID.x;
    const uint j = gl_GlobalInvocationID.y;
    D_TYPE d = D_TYPE(0.0);
    for (uint k = 0; k < K; k++)
    {
        const uint g_a_base = coord_to_offset(i, k, stride_a, layout_a);
        const uint g_b_base = coord_to_offset(k, j, stride_b, layout_b);
        d += matrix_a[g_a_base] * matrix_b[g_b_base];
    }
    const uint g_c_base = coord_to_offset(i, j, stride_c, layout_c);
    const uint g_d_base = coord_to_offset(i, j, stride_d, layout_d);
    matrix_d[g_d_base] = C_TYPE(alpha) * d + C_TYPE(beta) * matrix_c[g_c_base];
}
