#pragma once

#include "gpu/core.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class xavier_uniform
	{
	public:
		xavier_uniform(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type output_0_type, ::framework::gpu::component_type output_1_type);

		void execute(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t offset, ::std::uint32_t size, ::std::uint32_t logical_size);

		void update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * output_0_buffer, ::framework::gpu::buffer * output_1_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
		::framework::gpu::pipeline * pipeline;
	};
}

#include "algorithm/xavier_uniform.inl"