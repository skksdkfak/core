inline ::framework::gpu::descriptor_set_layout * ::framework::algorithm::scan::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set_layout * (::framework::algorithm::scan::get_initialize_dispatch_indirect_command_descriptor_set_layout)() const noexcept
{
	return this->initialize_dispatch_indirect_command_descriptor_set_layout;
}
