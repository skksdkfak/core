#pragma once

#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include <cstdint>
#include <filesystem>
#include <span>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::algorithm
{
	class scan
	{
	public:
		class executor;

		struct constant_buffer;

		scan(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::descriptor_set_layout * descriptor_set_layout, char const * scan_executor_type_name, ::std::filesystem::path const & scen_executor_path);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		::framework::gpu::descriptor_set_layout * get_initialize_dispatch_indirect_command_descriptor_set_layout() const noexcept;

		static char constexpr default_scan_executor_type_name[] = "::framework::default_scan_executor";
		static char constexpr default_scan_executor_path[] = "default_scan_executor.hlsl";

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * initialize_dispatch_indirect_command_descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_layout * initialize_dispatch_indirect_command_pipeline_layout;
		::framework::gpu::shader_module * initialize_dispatch_indirect_command_shader_module;
		::framework::gpu::shader_module * chained_scan_decoupled_lookback_initialize_shader_module;
		::framework::gpu::shader_module * chained_scan_decoupled_lookback_exclusive_shader_module;
		::framework::gpu::shader_module * chained_scan_decoupled_lookback_inclusive_shader_module;
	};

	class ::framework::algorithm::scan::executor
	{
	public:
		executor(::framework::algorithm::scan & scan, ::framework::gpu_context * gpu_context);

		void initialize_dispatch_indirect_command(::framework::algorithm::scan & scan, ::framework::gpu_context * gpu_context, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void execute(::framework::algorithm::scan & scan, ::framework::gpu::command_buffer * command_buffer, ::std::span<::framework::gpu::descriptor_set * const, 2> descriptor_sets, ::std::uint32_t size);

		void execute_indirect(::framework::algorithm::scan & scan, ::framework::gpu::command_buffer * command_buffer, ::std::span<::framework::gpu::descriptor_set * const, 2> descriptor_sets, ::std::uint32_t dynamic_offset_count, ::std::uint32_t const * dynamic_offsets, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset);

		void get_memory_requirements(::std::uint32_t size, ::std::uint32_t & index_buffer_size) const noexcept;

	private:
		::framework::gpu::pipeline * initialize_dispatch_indirect_command_pipeline;
		::framework::gpu::pipeline * chained_scan_decoupled_lookback_initialize_pipeline;
		::framework::gpu::pipeline * chained_scan_decoupled_lookback_exclusive_pipeline;
		::framework::gpu::pipeline * chained_scan_decoupled_lookback_inclusive_pipeline;
	};

	struct ::framework::algorithm::scan::constant_buffer
	{
		::std::uint32_t vectorized_size;
		::std::uint32_t thread_blocks;
	};
}

#include "algorithm/scan.inl"