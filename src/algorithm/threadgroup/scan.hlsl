#pragma once

namespace framework
{
namespace algorithm
{
namespace threadgroup
{
    template<typename type, uint vector_size, uint workgroup_size, uint vector_per_thread>
    struct scan
    {
        static const uint min_wave_size = 32u;
        type reduction[workgroup_size / min_wave_size];

        inline uint get_wave_index(uint gtid)
        {
            return gtid / WaveGetLaneCount();
        }

        inline uint part_start(uint part_index)
        {
            return part_index * workgroup_size * vector_per_thread;
        }

        inline uint wave_part_size()
        {
            return vector_per_thread * WaveGetLaneCount();
        }

        inline uint get_wave_count()
        {
            return workgroup_size / WaveGetLaneCount();
        }

        inline uint wave_part_start(uint gtid)
        {
            return this.get_wave_index(gtid) * this.wave_part_size();
        }

        inline type get_prev_reduction(uint group_thread_id)
        {
            return group_thread_id >= WaveGetLaneCount() ? this.reduction[this.get_wave_index(group_thread_id) - 1] : 0;
        }

        inline type vector_exclusive_prefix_sum(inout vector<type, vector_size> data)
        {
            type sum = type(0);
            [unroll]
            for (uint i = 0; i < vector_size; ++i)
            {
                const type tmp = data[i];
                data[i] = sum;
                sum += tmp;
            }
            return sum;
        }

        inline void vector_inclusive_prefix_sum(inout vector<type, vector_size> data)
        {
            [unroll]
            for (uint i = 1; i < vector_size; ++i)
            {
                data[i] += data[i - 1];
            }
        }

        inline vector<type, vector_size> exclusive_add(type t, vector<type, vector_size> data)
        {
            vector<type, vector_size> result;
            result[0] = t;
            [unroll]
            for (uint i = 1; i < vector_size; ++i)
            {
                data[i] += t;
            }
            return result;
        }

        template<typename scan_executor_t>
        inline void scan_exclusive_full(uint gtid, uint part_index, inout scan_executor_t scan_executor)
        {
            const uint lane_mask = WaveGetLaneCount() - 1;
            const uint circular_shift = WaveGetLaneIndex() + lane_mask & lane_mask;
            type wave_reduction = 0;

            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                vector<type, vector_size> t = scan_executor.load(i + this.part_start(part_index), k);
                t[0] = this.vector_exclusive_prefix_sum(t);
                const type t3 = WaveReadLaneAt(t[0] + WavePrefixSum(t[0]), circular_shift);
                scan_executor.store_local(i, k, this.exclusive_add((WaveGetLaneIndex() ? t3 : 0) + wave_reduction, t));
                wave_reduction += WaveReadLaneAt(t3, 0);
            }

            if (!WaveGetLaneIndex())
            {
                this.reduction[this.get_wave_index(gtid)] = wave_reduction;
            }
        }

        template<typename scan_executor_t>
        inline void scan_exclusive_partial(uint gtid, uint part_index, inout scan_executor_t scan_executor)
        {
            const uint lane_mask = WaveGetLaneCount() - 1;
            const uint circular_shift = WaveGetLaneIndex() + lane_mask & lane_mask;
            const uint final_part_size = scan_executor.get_vectorized_size() - this.part_start(part_index);
            type wave_reduction = 0;

            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                vector<type, vector_size> t = i < final_part_size ? scan_executor.load(i + this.part_start(part_index), k) : 0;
                t[0] = this.vector_exclusive_prefix_sum(t);
                const type t3 = WaveReadLaneAt(t[0] + WavePrefixSum(t[0]), circular_shift);
                scan_executor.store_local(i, k, this.exclusive_add((WaveGetLaneIndex() ? t3 : 0) + wave_reduction, t));
                wave_reduction += WaveReadLaneAt(t3, 0);
            }

            if (!WaveGetLaneIndex())
            {
                this.reduction[this.get_wave_index(gtid)] = wave_reduction;
            }
        }

        template<typename scan_executor_t>
        inline void scan_inclusive_full(uint gtid, uint part_index, inout scan_executor_t scan_executor)
        {
            const uint lane_mask = WaveGetLaneCount() - 1;
            const uint circular_shift = WaveGetLaneIndex() + lane_mask & lane_mask;
            type wave_reduction = 0;

            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                vector<type, vector_size> t = scan_executor.load(i + this.part_start(part_index), k);
                this.vector_inclusive_prefix_sum(t);

                const type t2 = WaveReadLaneAt(t[vector_size - 1] + WavePrefixSum(t[vector_size - 1]), circular_shift);
                scan_executor.store_local(i, k, t + (WaveGetLaneIndex() ? t2 : 0) + wave_reduction);
                wave_reduction += WaveReadLaneAt(t2, 0);
            }

            if (!WaveGetLaneIndex())
            {
                this.reduction[this.get_wave_index(gtid)] = wave_reduction;
            }
        }

        template<typename scan_executor_t>
        inline void scan_inclusive_partial(uint gtid, uint part_index, inout scan_executor_t scan_executor)
        {
            const uint lane_mask = WaveGetLaneCount() - 1;
            const uint circular_shift = WaveGetLaneIndex() + lane_mask & lane_mask;
            const uint final_part_size = scan_executor.get_vectorized_size() - this.part_start(part_index);
            type wave_reduction = 0;

            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                vector<type, vector_size> t = i < final_part_size ? scan_executor.load(i + this.part_start(part_index), k) : 0;
                this.vector_inclusive_prefix_sum(t);

                const type t2 = WaveReadLaneAt(t[vector_size - 1] + WavePrefixSum(t[vector_size - 1]), circular_shift);
                scan_executor.store_local(i, k, t + (WaveGetLaneIndex() ? t2 : 0) + wave_reduction);
                wave_reduction += WaveReadLaneAt(t2, 0);
            }

            if (!WaveGetLaneIndex())
            {
                this.reduction[this.get_wave_index(gtid)] = wave_reduction;
            }
        }

        inline void local_scan_inclusive_full(uint gtid)
        {
            if (gtid < this.get_wave_count())
            {
                this.reduction[gtid] += WavePrefixSum(this.reduction[gtid]);
            }
        }

        inline void local_scan_inclusive_partial(uint gtid)
        {
            const uint scan_size = this.get_wave_count();
            if (gtid < scan_size)
                this.reduction[gtid] += WavePrefixSum(this.reduction[gtid]);
            GroupMemoryBarrierWithGroupSync();

            const uint lane_log = countbits(WaveGetLaneCount() - 1);
            uint offset = lane_log;
            uint j = WaveGetLaneCount();
            for (; j < (scan_size >> 1); j <<= lane_log)
            {
                if (gtid < (scan_size >> offset))
                {
                    this.reduction[((gtid + 1) << offset) - 1] += WavePrefixSum(this.reduction[((gtid + 1) << offset) - 1]);
                }
                GroupMemoryBarrierWithGroupSync();

                if ((gtid & ((j << lane_log) - 1)) >= j && (gtid + 1) & (j - 1))
                {
                    this.reduction[gtid] += WaveReadLaneAt(this.reduction[((gtid >> offset) << offset) - 1], 0);
                }
                offset += lane_log;
            }
            GroupMemoryBarrierWithGroupSync();

            //If scan_size is not a power of lanecount
            const uint index = gtid + j;
            if (index < scan_size)
            {
                this.reduction[index] += WaveReadLaneAt(this.reduction[((index >> offset) << offset) - 1], 0);
            }
        }

        //Pass in previous reductions, and write out
        template<typename scan_executor_t>
        inline void down_sweep_full(uint gtid, uint part_index, type prev_reduction, inout scan_executor_t scan_executor)
        {
            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                scan_executor.store(i + this.part_start(part_index), k, scan_executor.load_local(i, k) + prev_reduction);
            }
        }

        template<typename scan_executor_t>
        inline void down_sweep_partial(uint gtid, uint part_index, type prev_reduction, inout scan_executor_t scan_executor)
        {
            const uint final_part_size = scan_executor.get_vectorized_size() - this.part_start(part_index);
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread && i < final_part_size; i += WaveGetLaneCount(), ++k)
            {
                scan_executor.store(i + this.part_start(part_index), k, scan_executor.load_local(i, k) + prev_reduction);
            }
        }

        template<typename scan_executor_t>
        inline void workgroup_exclusive_prefix_sum(uint group_thread_id, inout scan_executor_t scan_executor)
        {
            const uint partition_index = 0;

            this.scan_exclusive_full(group_thread_id, partition_index, scan_executor);

            GroupMemoryBarrierWithGroupSync();

            if (WaveGetLaneCount() >= this.get_wave_count())
                this.local_scan_inclusive_full(group_thread_id);

            if (WaveGetLaneCount() < this.get_wave_count())
                this.local_scan_inclusive_partial(group_thread_id);

            GroupMemoryBarrierWithGroupSync();

            const type prev_reduction = this.get_prev_reduction(group_thread_id);

            this.down_sweep_full(group_thread_id, partition_index, prev_reduction, scan_executor);
        }

        template<typename scan_executor_t>
        inline void workgroup_inclusive_prefix_sum(uint group_thread_id, inout scan_executor_t scan_executor)
        {
            const uint partition_index = 0;

            this.scan_inclusive_full(group_thread_id, partition_index, scan_executor);

            GroupMemoryBarrierWithGroupSync();

            if (WaveGetLaneCount() >= this.get_wave_count())
                this.local_scan_inclusive_full(group_thread_id);

            if (WaveGetLaneCount() < this.get_wave_count())
                this.local_scan_inclusive_partial(group_thread_id);

            GroupMemoryBarrierWithGroupSync();

            const type prev_reduction = this.get_prev_reduction(group_thread_id);

            this.down_sweep_full(group_thread_id, partition_index, prev_reduction, scan_executor);
        }
    };
}
}
}