#pragma once

namespace framework
{
namespace algorithm
{
namespace threadgroup
{
    template<typename type, uint vector_size, uint workgroup_size, uint vector_per_thread>
    struct sum
    {
        static const uint min_wave_size = 32u;
        type reduction[workgroup_size / min_wave_size];

        inline uint get_wave_index(uint gtid)
        {
            return gtid / WaveGetLaneCount();
        }

        inline uint part_start(uint part_index)
        {
            return part_index * workgroup_size * vector_per_thread;
        }

        inline uint wave_part_size()
        {
            return vector_per_thread * WaveGetLaneCount();
        }

        inline uint wave_part_start(uint gtid)
        {
            return this.get_wave_index(gtid) * this.wave_part_size();
        }

        inline type vector_sum(inout vector<type, vector_size> data)
        {
            type sum = data[0];
            [unroll]
            for (uint i = 1; i < vector_size; ++i)
            {
                sum += data[i];
            }
            return sum;
        }

        template<typename sum_executor_t>
        inline void sum_full(uint gtid, uint part_index, inout sum_executor_t sum_executor)
        {
            type wave_reduction = 0;

            [unroll]
            for (uint i = WaveGetLaneIndex() + this.wave_part_start(gtid), k = 0; k < vector_per_thread; i += WaveGetLaneCount(), ++k)
            {
                vector<type, vector_size> t = sum_executor.load(i + this.part_start(part_index), k);
                const type thread_sum = this.vector_sum(t);
                wave_reduction += WaveActiveSum(thread_sum);
            }

            if (!WaveGetLaneIndex())
            {
                this.reduction[this.get_wave_index(gtid)] = wave_reduction;
            }
        }

        inline void local_sum_full(uint gtid)
        {
            if (gtid < workgroup_size / WaveGetLaneCount())
            {
                const type wave_reduction = WaveActiveSum(this.reduction[gtid]);
                if (gtid == 0)
                {
                    this.reduction[0] = wave_reduction;
                }
            }
        }

        inline void local_sum_partial(uint gtid)
        {
            const uint sum_size = workgroup_size / WaveGetLaneCount();
            const uint lane_log = countbits(WaveGetLaneCount() - 1);
            for (uint j = 1, offset = 0; j < sum_size; j <<= lane_log)
            {
                if (gtid < (sum_size >> offset))
                {
                    const type wave_reduction = WaveActiveSum(this.reduction[gtid << offset]);
                    offset += lane_log;
                    if (!WaveGetLaneIndex())
                    {
                        this.reduction[gtid << offset] = wave_reduction;
                    }
                }
            }
        }

        template<typename sum_executor_t>
        inline void workgroup_sum(uint group_thread_id, inout sum_executor_t sum_executor)
        {
            const uint partition_index = 0;

            this.sum_full(group_thread_id, partition_index, sum_executor);

            GroupMemoryBarrierWithGroupSync();

            if (WaveGetLaneCount() >= workgroup_size / WaveGetLaneCount())
                this.local_sum_full(group_thread_id);
            
            if (WaveGetLaneCount() < workgroup_size / WaveGetLaneCount())
                this.local_sum_partial(group_thread_id);

            GroupMemoryBarrierWithGroupSync();

            sum_executor.store(this.reduction[0]);
        }
    };
}
}
}