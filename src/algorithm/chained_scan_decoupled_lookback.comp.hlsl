#define MAX_DISPATCH_DIM    65535U
#define UINT4_PART_SIZE     768U
#define BLOCK_DIM           256U
#define UINT4_PER_THREAD    3U
#define MIN_WAVE_SIZE       4U

[[vk::binding(0, 0)]] cbuffer constant_buffer_t : register(b0, space0)
{
    uint32_t vectorized_size;
    uint32_t thread_blocks;
};

groupshared uint4 g_shared[UINT4_PART_SIZE];
groupshared uint g_reduction[BLOCK_DIM / MIN_WAVE_SIZE];

inline uint getWaveIndex(uint _gtid)
{
    return _gtid / WaveGetLaneCount();
}

inline uint PartStart(uint _partIndex)
{
    return _partIndex * UINT4_PART_SIZE;
}

inline uint WavePartSize()
{
    return UINT4_PER_THREAD * WaveGetLaneCount();
}

inline uint WavePartStart(uint _gtid)
{
    return getWaveIndex(_gtid) * WavePartSize();
}

inline uint4 SetXAddYZW(uint t, uint4 val)
{
    return uint4(t, val.yzw + t);
}

//read in and scan
template<typename scan_executor_t>
inline void ScanExclusiveFull(uint gtid, uint partIndex, inout scan_executor_t scan_executor)
{
    const uint laneMask = WaveGetLaneCount() - 1;
    const uint circularShift = WaveGetLaneIndex() + laneMask & laneMask;
    uint waveReduction = 0;
    
    [unroll]
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD;
        i += WaveGetLaneCount(), ++k)
    {
        uint4 t = scan_executor.load(i + PartStart(partIndex), k);

        uint t2 = t.x;
        t.x += t.y;
        t.y = t2;

        t2 = t.x;
        t.x += t.z;
        t.z = t2;

        t2 = t.x;
        t.x += t.w;
        t.w = t2;
        
        const uint t3 = WaveReadLaneAt(t.x + WavePrefixSum(t.x), circularShift);
        g_shared[i] = SetXAddYZW((WaveGetLaneIndex() ? t3 : 0) + waveReduction, t);
        waveReduction += WaveReadLaneAt(t3, 0);
    }
    
    if (!WaveGetLaneIndex())
        g_reduction[getWaveIndex(gtid)] = waveReduction;
}

template<typename scan_executor_t>
inline void ScanExclusivePartial(uint gtid, uint partIndex, inout scan_executor_t scan_executor)
{
    const uint laneMask = WaveGetLaneCount() - 1;
    const uint circularShift = WaveGetLaneIndex() + laneMask & laneMask;
    const uint finalPartSize = vectorized_size - PartStart(partIndex);
    uint waveReduction = 0;
    
    [unroll]
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD;
        i += WaveGetLaneCount(), ++k)
    {
        uint4 t = i < finalPartSize ? scan_executor.load(i + PartStart(partIndex), k) : 0;

        uint t2 = t.x;
        t.x += t.y;
        t.y = t2;

        t2 = t.x;
        t.x += t.z;
        t.z = t2;

        t2 = t.x;
        t.x += t.w;
        t.w = t2;
        
        const uint t3 = WaveReadLaneAt(t.x + WavePrefixSum(t.x), circularShift);
        g_shared[i] = SetXAddYZW((WaveGetLaneIndex() ? t3 : 0) + waveReduction, t);
        waveReduction += WaveReadLaneAt(t3, 0);
    }
    
    if (!WaveGetLaneIndex())
        g_reduction[getWaveIndex(gtid)] = waveReduction;
}

template<typename scan_executor_t>
inline void ScanInclusiveFull(uint gtid, uint partIndex, inout scan_executor_t scan_executor)
{
    const uint laneMask = WaveGetLaneCount() - 1;
    const uint circularShift = WaveGetLaneIndex() + laneMask & laneMask;
    uint waveReduction = 0;
    
    [unroll]
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD;
        i += WaveGetLaneCount(), ++k)
    {
        uint4 t = scan_executor.load(i + PartStart(partIndex), k);
        t.y += t.x;
        t.z += t.y;
        t.w += t.z;
        
        const uint t2 = WaveReadLaneAt(t.w + WavePrefixSum(t.w), circularShift);
        g_shared[i] = t + (WaveGetLaneIndex() ? t2 : 0) + waveReduction;
        waveReduction += WaveReadLaneAt(t2, 0);
    }
    
    if (!WaveGetLaneIndex())
        g_reduction[getWaveIndex(gtid)] = waveReduction;
}

template<typename scan_executor_t>
inline void ScanInclusivePartial(uint gtid, uint partIndex, inout scan_executor_t scan_executor)
{
    const uint laneMask = WaveGetLaneCount() - 1;
    const uint circularShift = WaveGetLaneIndex() + laneMask & laneMask;
    const uint finalPartSize = vectorized_size - PartStart(partIndex);
    uint waveReduction = 0;
    
    [unroll]
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD;
        i += WaveGetLaneCount(), ++k)
    {
        uint4 t = i < finalPartSize ? scan_executor.load(i + PartStart(partIndex), k) : 0;
        t.y += t.x;
        t.z += t.y;
        t.w += t.z;
        
        const uint t2 = WaveReadLaneAt(t.w + WavePrefixSum(t.w), circularShift);
        g_shared[i] = t + (WaveGetLaneIndex() ? t2 : 0) + waveReduction;
        waveReduction += WaveReadLaneAt(t2, 0);
    }
    
    if (!WaveGetLaneIndex())
        g_reduction[getWaveIndex(gtid)] = waveReduction;
}

//Reduce the wave reductions
inline void LocalScanInclusiveWGE16(uint gtid)
{
    if (gtid < BLOCK_DIM / WaveGetLaneCount())
        g_reduction[gtid] += WavePrefixSum(g_reduction[gtid]);
}

inline void LocalScanInclusiveWLT16(uint gtid)
{
    const uint scanSize = BLOCK_DIM / WaveGetLaneCount();
    if (gtid < scanSize)
        g_reduction[gtid] += WavePrefixSum(g_reduction[gtid]);
    GroupMemoryBarrierWithGroupSync();
        
    const uint laneLog = countbits(WaveGetLaneCount() - 1);
    uint offset = laneLog;
    uint j = WaveGetLaneCount();
    for (; j < (scanSize >> 1); j <<= laneLog)
    {
        if (gtid < (scanSize >> offset))
        {
            g_reduction[((gtid + 1) << offset) - 1] +=
                WavePrefixSum(g_reduction[((gtid + 1) << offset) - 1]);
        }
        GroupMemoryBarrierWithGroupSync();
            
        if ((gtid & ((j << laneLog) - 1)) >= j && (gtid + 1) & (j - 1))
        {
            g_reduction[gtid] +=
                WaveReadLaneAt(g_reduction[((gtid >> offset) << offset) - 1], 0);
        }
        offset += laneLog;
    }
    GroupMemoryBarrierWithGroupSync();
        
    //If scanSize is not a power of lanecount
    const uint index = gtid + j;
    if (index < scanSize)
    {
        g_reduction[index] +=
            WaveReadLaneAt(g_reduction[((index >> offset) << offset) - 1], 0);
    }
}

//Pass in previous reductions, and write out
template<typename scan_executor_t>
inline void DownSweepFull(uint gtid, uint partIndex, uint prevReduction, inout scan_executor_t scan_executor)
{
    [unroll]
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD;
        i += WaveGetLaneCount(), ++k)
    {
        scan_executor.store(i + PartStart(partIndex), k, g_shared[i] + prevReduction);
    }
}

template<typename scan_executor_t>
inline void DownSweepPartial(uint gtid, uint partIndex, uint prevReduction, inout scan_executor_t scan_executor)
{
    const uint finalPartSize = vectorized_size - PartStart(partIndex);
    for (uint i = WaveGetLaneIndex() + WavePartStart(gtid), k = 0;
        k < UINT4_PER_THREAD && i < finalPartSize;
        i += WaveGetLaneCount(), ++k)
    {
        scan_executor.store(i + PartStart(partIndex), k, g_shared[i] + prevReduction);
    }
}

#define FLAG_NOT_READY  0           //Flag indicating this partition tile's local reduction is not ready
#define FLAG_REDUCTION  1           //Flag indicating this partition tile's local reduction is ready
#define FLAG_INCLUSIVE  2           //Flag indicating this partition tile has summed all preceding tiles and added to its sum.
#define FLAG_MASK       3           //Mask used to retrieve the flag

[[vk::binding(1, 0)]] globallycoherent RWStructuredBuffer<uint> index_buffer : register(u0, space0);
[[vk::binding(2, 0)]] globallycoherent RWStructuredBuffer<uint> thread_block_reduction_buffer : register(u1, space0);

groupshared uint g_broadcast;

inline void AcquirePartitionIndex(uint gtid)
{
    if (!gtid)
        InterlockedAdd(index_buffer[0], 1, g_broadcast);
}

//use the exact thread that performed the scan on the last element
//to elide an extra barrier
inline void DeviceBroadcast(uint gtid, uint partIndex)
{
    if (gtid == BLOCK_DIM / WaveGetLaneCount() - 1)
    {
        InterlockedAdd(thread_block_reduction_buffer[partIndex], (partIndex ? FLAG_REDUCTION : FLAG_INCLUSIVE) | g_reduction[gtid] << 2);
    }
}

//Perform lookback with a single thread
inline void LookbackSingle(uint partIndex)
{
    uint prevReduction = 0;
    uint lookBackIndex = partIndex - 1;
    
    while (true)
    {
        const uint flagPayload = thread_block_reduction_buffer[lookBackIndex];

        if ((flagPayload & FLAG_MASK) > FLAG_NOT_READY)
        {
            prevReduction += flagPayload >> 2;
            if ((flagPayload & FLAG_MASK) == FLAG_INCLUSIVE)
            {
                g_broadcast = prevReduction;
                InterlockedAdd(thread_block_reduction_buffer[partIndex], 1 | (prevReduction << 2));
                break;
            }
            else
            {
                lookBackIndex--;
            }
        }
    }
}

//Perform lookback with a single warp
inline void LookbackWarp(uint partIndex)
{
    uint prevReduction = 0;
    uint k = partIndex + WaveGetLaneCount() - WaveGetLaneIndex();
    const uint waveParts = (WaveGetLaneCount() + 31) / 32;
    
    while (true)
    {
        const uint flagPayload = k > WaveGetLaneCount() ? 
            thread_block_reduction_buffer[k - WaveGetLaneCount() - 1] : FLAG_INCLUSIVE;

        if (WaveActiveAllTrue((flagPayload & FLAG_MASK) > FLAG_NOT_READY))
        {
            const uint4 inclusiveBallot = WaveActiveBallot((flagPayload & FLAG_MASK) == FLAG_INCLUSIVE);
            
            //dot(inclusiveBallot, uint4(1,1,1,1)) != 0 does not work
            //consider 0xffffffff + 1 + 0xffffffff + 1
            if (inclusiveBallot.x || inclusiveBallot.y || inclusiveBallot.z || inclusiveBallot.w)
            {
                uint inclusiveIndex = 0;
                for (uint wavePart = 0; wavePart < waveParts; ++wavePart)
                {
                    if (countbits(inclusiveBallot[wavePart]))
                    {
                        inclusiveIndex += firstbitlow(inclusiveBallot[wavePart]);
                        break;
                    }
                    else
                    {
                        inclusiveIndex += 32;
                    }
                }
                                    
                prevReduction += WaveActiveSum(WaveGetLaneIndex() <= inclusiveIndex ? (flagPayload >> 2) : 0);
                                
                if (WaveGetLaneIndex() == 0)
                {
                    g_broadcast = prevReduction;
                    InterlockedAdd(thread_block_reduction_buffer[partIndex], 1 | (prevReduction << 2));
                }
                break;
            }
            else
            {
                prevReduction += WaveActiveSum(flagPayload >> 2);
                k -= WaveGetLaneCount();
            }
        }
    }
}

#include "custom_include.h"

[numthreads(256, 1, 1)]
[shader("compute")]
void chained_scan_decoupled_lookback_initialize(uint3 id : SV_DispatchThreadID)
{
    const uint increment = 256 * 256;
    
    for (uint i = id.x; i < thread_blocks; i += increment)
        thread_block_reduction_buffer[i] = 0;
    
    if (!id.x)
        index_buffer[id.x] = 0;
}

[numthreads(BLOCK_DIM, 1, 1)]
[shader("compute")]
void chained_scan_decoupled_lookback_exclusive(uint3 gtid : SV_GroupThreadID)
{
    AcquirePartitionIndex(gtid.x);
    GroupMemoryBarrierWithGroupSync();
    const uint partitionIndex = g_broadcast;

    current_scan_executor_t scan_executor;
    scan_executor.initialize();

    if (partitionIndex < thread_blocks - 1)
        ScanExclusiveFull(gtid.x, partitionIndex, scan_executor);
    
    if (partitionIndex == thread_blocks - 1)
        ScanExclusivePartial(gtid.x, partitionIndex, scan_executor);

    GroupMemoryBarrierWithGroupSync();
    
    if (WaveGetLaneCount() >= 16)
        LocalScanInclusiveWGE16(gtid.x);
    
    if (WaveGetLaneCount() < 16)
        LocalScanInclusiveWLT16(gtid.x);
    
    DeviceBroadcast(gtid.x, partitionIndex);
    
    if (partitionIndex && !gtid.x)
        LookbackSingle(partitionIndex);
    GroupMemoryBarrierWithGroupSync();
    
    const uint prevReduction = g_broadcast + (gtid.x >= WaveGetLaneCount() ? g_reduction[getWaveIndex(gtid.x) - 1] : 0);
    
    if (partitionIndex < thread_blocks - 1)
        DownSweepFull(gtid.x, partitionIndex, prevReduction, scan_executor);
    
    if (partitionIndex == thread_blocks - 1)
        DownSweepPartial(gtid.x, partitionIndex, prevReduction, scan_executor);
}

[numthreads(BLOCK_DIM, 1, 1)]
[shader("compute")]
void chained_scan_decoupled_lookback_inclusive(uint3 gtid : SV_GroupThreadID)
{
    AcquirePartitionIndex(gtid.x);
    GroupMemoryBarrierWithGroupSync();
    const uint partitionIndex = g_broadcast;

    current_scan_executor_t scan_executor;
    scan_executor.initialize();

    if (partitionIndex < thread_blocks - 1)
        ScanInclusiveFull(gtid.x, partitionIndex, scan_executor);
    
    if (partitionIndex == thread_blocks - 1)
        ScanInclusivePartial(gtid.x, partitionIndex, scan_executor);

    GroupMemoryBarrierWithGroupSync();
    
    if (WaveGetLaneCount() >= 16)
        LocalScanInclusiveWGE16(gtid.x);
    
    if (WaveGetLaneCount() < 16)
        LocalScanInclusiveWLT16(gtid.x);
    
    DeviceBroadcast(gtid.x, partitionIndex);
    
    if (partitionIndex && !gtid.x)
        LookbackSingle(partitionIndex);
    GroupMemoryBarrierWithGroupSync();
    
    const uint prevReduction = g_broadcast + (gtid.x >= WaveGetLaneCount() ? g_reduction[getWaveIndex(gtid.x) - 1] : 0);
    
    if (partitionIndex < thread_blocks - 1)
        DownSweepFull(gtid.x, partitionIndex, prevReduction, scan_executor);
    
    if (partitionIndex == thread_blocks - 1)
        DownSweepPartial(gtid.x, partitionIndex, prevReduction, scan_executor);
}