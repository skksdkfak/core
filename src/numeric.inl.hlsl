template<typename T>
inline T(::framework::numeric::div_round_up)(T val, T divisor)
{
	return (val + divisor - 1) / divisor;
}

template<typename T>
inline T(::framework::numeric::previous_multiple)(T val, T divisor)
{
	return (val / divisor) * divisor;
}

template<typename T>
inline T(::framework::numeric::next_multiple)(T val, T divisor)
{
	return ::framework::numeric::div_round_up(val, divisor) * divisor;
}

template<typename T>
inline T(::framework::numeric::powi)(T base, T exponent)
{
	T result = 1;
	for (T i = 0; i < exponent; ++i)
	{
		result *= base;
	}

	return result;
}