#include "gpu_context.hpp"

::framework::gpu_context::gpu_context(::framework::gpu_context_create_info const & gpu_context_create_info) :
	physical_device(gpu_context_create_info.physical_device),
	device(gpu_context_create_info.device),
	queue_family_indices
	{
		gpu_context_create_info.graphics_queue_family_index,
		gpu_context_create_info.compute_queue_family_index,
		gpu_context_create_info.transfer_queue_family_index
	}
{
	this->physical_device->get_features(&this->physical_device_features);
	this->physical_device->get_properties(&this->physical_device_properties);
	this->physical_device->get_memory_properties(&this->physical_device_memory_properties);
}