#pragma once

namespace framework
{
	namespace numeric
	{
		template <typename T>
		T div_round_up(T val, T divisor);

		template <typename T>
		T previous_multiple(T val, T divisor);

		template <typename T>
		T next_multiple(T val, T divisor);

		template <typename T>
		T powi(T base, T exponent);
	}
}

#include "numeric.inl.hlsl"