#pragma once

#include "gpu/core.hpp"

namespace framework
{
	struct gpu_context_create_info
	{
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::std::uint32_t graphics_queue_family_index;
		::std::uint32_t compute_queue_family_index;
		::std::uint32_t transfer_queue_family_index;
	};

	class gpu_context
	{
	public:
		explicit gpu_context(::framework::gpu_context_create_info const & gpu_context_create_info);

		::framework::gpu::physical_device * get_physical_device() const noexcept;

		::framework::gpu::physical_device_features const & get_physical_device_features() const noexcept;

		::framework::gpu::physical_device_properties const & get_physical_device_properties() const noexcept;

		::framework::gpu::physical_device_memory_properties const & get_physical_device_memory_properties() const noexcept;

		::framework::gpu::device * get_device() const noexcept;

		auto const & get_queues() const noexcept;

		auto const & get_queue_family_indices() const noexcept;

	private:
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::physical_device_features physical_device_features;
		::framework::gpu::physical_device_properties physical_device_properties;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::framework::gpu::device * device;
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		}
		queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		}
		queues;
	};
}

#include "gpu_context.inl"