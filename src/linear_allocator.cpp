#include "gpu/utility.hpp"
#include "linear_allocator.hpp"
#include <cassert>
#include <utility>

::framework::linear_allocation_page::linear_allocation_page(::framework::linear_allocator_page_manager & linear_allocator_page_manager, ::framework::linear_allocation_page::type page_type, ::framework::gpu::device_size page_size) :
	linear_allocator_page_manager(linear_allocator_page_manager)
{
	::framework::gpu::buffer_usage_flags usage;
	::framework::gpu::memory_property_flags memory_property_flags;
	::framework::gpu::buffer_state_flags initial_state;
	switch (page_type)
	{
	case ::framework::linear_allocation_page::type::cpu_read:
		usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		memory_property_flags = ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_visible_read_bit;
		initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		break;
	case ::framework::linear_allocation_page::type::cpu_write:
		usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::host_read_bit;
		memory_property_flags = ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_visible_write_bit;
		initial_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
		break;
	default:
		::std::unreachable();
	}

	::framework::gpu::buffer_create_info buffer_create_info;
	buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
	buffer_create_info.size = page_size;
	buffer_create_info.usage = usage;
	buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	buffer_create_info.queue_family_index_count = 0;
	buffer_create_info.queue_family_indices = nullptr;
	buffer_create_info.initial_queue_family_index = linear_allocator_page_manager.get_gpu_context()->get_queue_family_indices().transfer;
	buffer_create_info.initial_state = initial_state;
	buffer_create_info.opaque_capture_address = 0;
	assert_framework_gpu_result(linear_allocator_page_manager.get_gpu_context()->get_device()->create_buffer(&buffer_create_info, nullptr, &this->buffer));

	::framework::gpu::memory_requirements memory_requirements;

	::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
	buffer_memory_requirements_info.buffer = this->buffer;
	linear_allocator_page_manager.get_gpu_context()->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
	memory_allocate_info.device_mask = 0;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(linear_allocator_page_manager.get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, memory_property_flags);
	memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->buffer : nullptr;
	memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
	memory_allocate_info.opaque_capture_address = 0;
	assert_framework_gpu_result(linear_allocator_page_manager.get_gpu_context()->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->device_memory));

	::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
	bind_buffer_memory_info.buffer = this->buffer;
	bind_buffer_memory_info.memory = this->device_memory;
	bind_buffer_memory_info.memory_offset = 0;
	assert_framework_gpu_result(linear_allocator_page_manager.get_gpu_context()->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

	assert_framework_gpu_result(linear_allocator_page_manager.get_gpu_context()->get_device()->map_memory(this->device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->mapped_buffer_data));
}

::framework::linear_allocation_page::~linear_allocation_page()
{
	this->linear_allocator_page_manager.get_gpu_context()->get_device()->destroy_buffer(this->buffer, nullptr);
	this->linear_allocator_page_manager.get_gpu_context()->get_device()->free_memory(this->device_memory, nullptr);
}

::framework::coroutine::immediate_task<::framework::linear_allocation_page *>(::framework::linear_allocator_page_manager::request_page)()
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->mutex.scoped_lock();
	::framework::linear_allocation_page * linear_allocation_page;
	if (this->available_pages.empty())
	{
		linear_allocation_page = new ::framework::linear_allocation_page(*this, this->page_type, this->page_size);
		this->page_pool.emplace_back(linear_allocation_page);
	}
	else
	{
		linear_allocation_page = this->available_pages.front();
		this->available_pages.pop();
	}
	co_return linear_allocation_page;
}

::framework::linear_allocation_page * (::framework::linear_allocator_page_manager::create_large_page)(::framework::gpu::device_size page_size)
{
	return new ::framework::linear_allocation_page(*this, this->page_type, page_size);
}

::framework::coroutine::immediate_task<void>(::framework::linear_allocator_page_manager::discard_pages)(::std::vector<::framework::linear_allocation_page *> const & retired_pages)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->mutex.scoped_lock();
	this->available_pages.push_range(retired_pages);
}

::framework::coroutine::immediate_task<struct ::framework::linear_allocator::allocation>(::framework::linear_allocator::allocate)(::framework::gpu::device_size size, ::framework::gpu::device_size alignment)
{
	if (size > this->linear_allocator_page_manager.get_page_size())
	{
		::std::unique_ptr<::framework::linear_allocation_page> const & large_page = this->large_pages.emplace_back(this->linear_allocator_page_manager.create_large_page(size));

		struct ::framework::linear_allocator::allocation allocation;
		allocation.buffer = large_page->get_buffer();
		allocation.device_memory = large_page->get_device_memory();
		allocation.offset = 0;
		allocation.size = size;
		allocation.mapped_buffer_data = large_page->get_mapped_buffer_data();

		co_return allocation;
	}
	else
	{
		::framework::gpu::device_size const alignment_mask = alignment - 1;
		this->current_offset = (this->current_offset + alignment_mask) & ~alignment_mask;

		if (this->current_offset + size > this->linear_allocator_page_manager.get_page_size())
		{
			this->retired_pages.push_back(this->current_linear_allocation_page);
			this->current_linear_allocation_page = nullptr;
		}

		if (this->current_linear_allocation_page == nullptr)
		{
			this->current_linear_allocation_page = co_await this->linear_allocator_page_manager.request_page();
			this->current_offset = 0;
		}

		struct ::framework::linear_allocator::allocation allocation;
		allocation.buffer = this->current_linear_allocation_page->get_buffer();
		allocation.device_memory = this->current_linear_allocation_page->get_device_memory();
		allocation.offset = this->current_offset;
		allocation.size = size;
		allocation.mapped_buffer_data = this->current_linear_allocation_page->get_mapped_buffer_data();

		this->current_offset += size;

		co_return allocation;
	}
}

::framework::coroutine::immediate_task<void>(::framework::linear_allocator::cleanup_used_pages)()
{
	if (this->current_linear_allocation_page)
	{
		this->retired_pages.push_back(this->current_linear_allocation_page);
		this->current_linear_allocation_page = nullptr;
		this->current_offset = 0;
	}

	if (!this->retired_pages.empty())
	{
		co_await this->linear_allocator_page_manager.discard_pages(this->retired_pages);
		this->retired_pages.clear();
	}

	this->large_pages.clear();
}
