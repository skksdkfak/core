#pragma once

#include <cmath>
#include <glm/glm.hpp>

namespace framework::physics
{
	struct rgb_sigmoid_polynomial
	{
		rgb_sigmoid_polynomial() = default;

		rgb_sigmoid_polynomial(float c0, float c1, float c2);

		float solve(float lambda) const;

		float c0, c1, c2;
	};

	class rgb_to_spectrum_table
	{
	public:
		static constexpr int res = 64;

		using coefficient_array = float[3][res][res][res][3];

		rgb_to_spectrum_table(float const * z_nodes, ::framework::physics::rgb_to_spectrum_table::coefficient_array const * coefficient);

		::framework::physics::rgb_sigmoid_polynomial operator()(::glm::vec3 const & rgb) const;

		float const * get_z_nodes() const
		{
			return this->z_nodes;
		}

		::framework::physics::rgb_to_spectrum_table::coefficient_array const * get_coefficient() const
		{
			return this->coefficient;
		}

		static void init();

		static ::framework::physics::rgb_to_spectrum_table const * srgb;

	private:
		float const * z_nodes;
		::framework::physics::rgb_to_spectrum_table::coefficient_array const * coefficient;
	};

	extern int const srgb_to_spectrum_table_res;
	extern float const srgb_to_spectrum_table_scale[64];
	extern ::framework::physics::rgb_to_spectrum_table::coefficient_array const srgb_to_spectrum_table_data;
}