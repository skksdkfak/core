#pragma once

#include "physics/color.hpp"
#include <glm/glm.hpp>
#include <cmath>
#include <cstdint>
#include <vector>

namespace framework::physics
{
	constexpr float lambda_min = 360.0f, lambda_max = 830.0f;
	constexpr float cie_y_integral = 106.856895f;
	constexpr int cie_illum_d6500_size = 214;
	constexpr int cie_sample_count = 471;
	extern const float cie_illum_d6500_lambda_value[::framework::physics::cie_illum_d6500_size];
	extern const float cie_lambda[::framework::physics::cie_sample_count];
	extern const float cie_x[::framework::physics::cie_sample_count];
	extern const float cie_y[::framework::physics::cie_sample_count];
	extern const float cie_z[::framework::physics::cie_sample_count];

	class rgb_color_space;

	class spectrum
	{
	public:
		virtual float operator()(float lambda) const = 0;
	};

	class constant_spectrum : public ::framework::physics::spectrum
	{
	public:
		constant_spectrum(float c);

		float operator()(float lambda) const override;

		float get_constant() const noexcept;

	private:
		float c;
	};

	class densely_sampled_spectrum : public ::framework::physics::spectrum
	{
	public:
		densely_sampled_spectrum(::framework::physics::spectrum const & spectrum, ::std::uint16_t lambda_min = ::framework::physics::lambda_min, ::std::uint16_t lambda_max = ::framework::physics::lambda_max);

		float operator()(float lambda) const override;

		::std::uint16_t get_lambda_min() const noexcept;

		::std::uint16_t get_lambda_max() const noexcept;

		::std::vector<float> const & get_values() const noexcept;

	private:
		::std::uint16_t lambda_min;
		::std::uint16_t lambda_max;
		::std::vector<float> values;
	};

	class piecewise_linear_spectrum : public ::framework::physics::spectrum
	{
	public:
		piecewise_linear_spectrum(float const * lambdas, float const * values, ::std::size_t size);

		piecewise_linear_spectrum(float const * samples, ::std::size_t size, bool normalize);

		void scale(float s);

		float operator()(float lambda) const override;

		::std::vector<float> const & get_lambdas() const noexcept;

		::std::vector<float> const & get_values() const noexcept;

	private:
		::std::vector<float> lambdas;
		::std::vector<float> values;
	};

	class blackbody_spectrum : public ::framework::physics::spectrum
	{
	public:
		blackbody_spectrum(float T);

		float operator()(float lambda) const override;

		float get_temperature() const noexcept;

		float get_normalization_factor() const noexcept;

	private:
		float T;
		float normalization_factor;
	};

	class rgb_albedo_spectrum : public ::framework::physics::spectrum
	{
	public:
		rgb_albedo_spectrum(::framework::physics::rgb_color_space const & rgb_color_space, ::glm::vec3 const & rgb);

		float operator()(float lambda) const override;

	private:
		::framework::physics::rgb_sigmoid_polynomial rsp;
	};

	class rgb_unbounded_spectrum : public ::framework::physics::spectrum
	{
	public:
		rgb_unbounded_spectrum();

		rgb_unbounded_spectrum(::framework::physics::rgb_color_space const & rgb_color_space, ::glm::vec3 const & rgb);

		float operator()(float lambda) const override;

	private:
		float scale;
		::framework::physics::rgb_sigmoid_polynomial rsp;
	};

	class rgb_illuminant_spectrum : public ::framework::physics::spectrum
	{
	public:
		rgb_illuminant_spectrum(::framework::physics::rgb_color_space const & rgb_color_space, ::glm::vec3 const & rgb);

		float operator()(float lambda) const override;

	private:
		float scale;
		::framework::physics::rgb_sigmoid_polynomial rsp;
		::framework::physics::densely_sampled_spectrum const * illuminant;
	};

	namespace spectra
	{
		void init();

		extern ::framework::physics::densely_sampled_spectrum const * x;
		extern ::framework::physics::densely_sampled_spectrum const * y;
		extern ::framework::physics::densely_sampled_spectrum const * z;
		extern ::framework::physics::piecewise_linear_spectrum const * cie_illum_d6500;
	}

	float blackbody(float lambda, float T);

	float inner_product(::framework::physics::spectrum const & f, ::framework::physics::spectrum const & g);
}

#include "physics/spectrum.inl"