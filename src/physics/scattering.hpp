#pragma once

#include <glm/glm.hpp>

namespace framework::physics
{
	struct trowbridge_reitz_distribution
	{
		static ::glm::vec2 roughness_to_alpha(::glm::vec2 roughness);
	};
}

#include "physics/scattering.inl"