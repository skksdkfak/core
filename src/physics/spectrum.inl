inline ::std::uint16_t(::framework::physics::densely_sampled_spectrum::get_lambda_min)() const noexcept
{
	return this->lambda_min;
}

inline ::std::uint16_t(::framework::physics::densely_sampled_spectrum::get_lambda_max)() const noexcept
{
	return this->lambda_max;
}

inline ::std::vector<float> const & ::framework::physics::densely_sampled_spectrum::get_values() const noexcept
{
	return this->values;
}

inline ::std::vector<float> const & ::framework::physics::piecewise_linear_spectrum::get_lambdas() const noexcept
{
	return this->lambdas;
}

inline ::std::vector<float> const & ::framework::physics::piecewise_linear_spectrum::get_values() const noexcept
{
	return this->values;
}

inline float ::framework::physics::constant_spectrum::get_constant() const noexcept
{
	return this->c;
}

inline float ::framework::physics::blackbody_spectrum::get_temperature() const noexcept
{
	return this->T;
}

inline float ::framework::physics::blackbody_spectrum::get_normalization_factor() const noexcept
{
	return this->normalization_factor;
}

inline float ::framework::physics::blackbody(float lambda, float T)
{
	if (T <= 0)
		return 0;
	const float c = 299792458.f;
	const float h = 6.62606957e-34f;
	const float kb = 1.3806488e-23f;
	// Return emitted radiance for blackbody at wavelength _lambda_
	float l = lambda * 1e-9f;
	float Le = (2 * h * c * c) / (::std::powf(l, 5.0f) * (::std::exp((h * c) / (l * kb * T)) - 1));
	return Le;
}

inline float ::framework::physics::inner_product(::framework::physics::spectrum const & f, ::framework::physics::spectrum const & g)
{
	float integral = 0.0f;
	for (float lambda = ::framework::physics::lambda_min; lambda <= ::framework::physics::lambda_max; ++lambda)
	{
		integral += f(lambda) * g(lambda);
	}
	return integral;
}
