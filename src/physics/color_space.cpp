#include "physics/color_space.hpp"
#include "physics/spectrum.hpp"
#include <glm/glm.hpp>
#include <cassert>

::framework::physics::rgb_color_space::rgb_color_space(::glm::vec2 const & r, ::glm::vec2 const & g, ::glm::vec2 const & b, ::framework::physics::spectrum const & illuminant, ::framework::physics::rgb_to_spectrum_table const & rgb_to_spectrum_table) :
	r(r),
	g(g),
	b(b),
	illuminant(illuminant),
	rgb_to_spectrum_table(rgb_to_spectrum_table)
{
}

::framework::physics::rgb_sigmoid_polynomial(::framework::physics::rgb_color_space::to_rgb_coeffs)(::glm::vec3 const & rgb) const
{
	assert(rgb.r >= 0 && rgb.g >= 0 && rgb.b >= 0);
	return this->rgb_to_spectrum_table(::glm::max(::glm::vec3(0.0f), rgb));
}

::framework::physics::rgb_color_space const * ::framework::physics::rgb_color_space::srgb;

void ::framework::physics::rgb_color_space::init()
{
	::framework::physics::rgb_color_space::srgb = new ::framework::physics::rgb_color_space(::glm::vec2(.64, .33), ::glm::vec2(.3, .6), ::glm::vec2(.15, .06), *::framework::physics::spectra::cie_illum_d6500, *::framework::physics::rgb_to_spectrum_table::srgb);
}
