inline ::glm::vec2 framework::physics::trowbridge_reitz_distribution::roughness_to_alpha(::glm::vec2 roughness)
{
	return ::glm::sqrt(roughness);
}
