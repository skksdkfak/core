#pragma once

#include "physics/spectrum.hpp"
#include "physics/color.hpp"
#include <glm/glm.hpp>
#include <string>

namespace framework::physics
{
	class rgb_color_space
	{
	public:
		rgb_color_space(::glm::vec2 const & r, ::glm::vec2 const & g, ::glm::vec2 const & b, ::framework::physics::spectrum const & illuminant, ::framework::physics::rgb_to_spectrum_table const & rgb_to_spectrum_table);

		::framework::physics::rgb_sigmoid_polynomial to_rgb_coeffs(::glm::vec3 const & rgb) const;

		static void init();

		::glm::vec2 r, g, b, w;
		::framework::physics::densely_sampled_spectrum illuminant;
		static ::framework::physics::rgb_color_space const * srgb;

	private:
		::framework::physics::rgb_to_spectrum_table const & rgb_to_spectrum_table;
	};
}
