#include "physics/color.hpp"
#include <algorithm>
#include <cassert>

::framework::physics::rgb_sigmoid_polynomial::rgb_sigmoid_polynomial(float c0, float c1, float c2) :
	c0(c0), c1(c1), c2(c2)
{
}

float ::framework::physics::rgb_sigmoid_polynomial::solve(float lambda) const
{
	float t = lambda;
	float x = t * (t * this->c0 + this->c1) + this->c2;
	float s = .5f + x / (2 * ::std::sqrt(1 + x * x));

	return s;
}

::framework::physics::rgb_to_spectrum_table::rgb_to_spectrum_table(float const * z_nodes, ::framework::physics::rgb_to_spectrum_table::coefficient_array const * coefficient) :
	z_nodes(z_nodes), coefficient(coefficient)
{
}

::framework::physics::rgb_sigmoid_polynomial(::framework::physics::rgb_to_spectrum_table::operator())(::glm::vec3 const & rgb) const
{
	// Handle uniform rgb values
	if (rgb[0] == rgb[1] && rgb[1] == rgb[2])
	{
		return ::framework::physics::rgb_sigmoid_polynomial(0, 0, (rgb[0] - .5f) / ::std::sqrt(rgb[0] * (1 - rgb[0])));
	}

	// Find maximum component and compute remapped component values
	int maxc = (rgb[0] > rgb[1]) ? ((rgb[0] > rgb[2]) ? 0 : 2) : ((rgb[1] > rgb[2]) ? 1 : 2);
	float z = rgb[maxc];
	float x = rgb[(maxc + 1) % 3] * (this->res - 1) / z;
	float y = rgb[(maxc + 2) % 3] * (this->res - 1) / z;

	// Compute integer indices and offsets for coefficient interpolation
	int xi = ::std::min<int>(x, this->res - 2);
	int yi = ::std::min<int>(y, this->res - 2);
	int zi = ::std::min<int>(::std::distance(this->z_nodes, ::std::lower_bound(this->z_nodes, this->z_nodes + this->res, z)), this->res - 2);
	float dx = x - xi, dy = y - yi, dz = (z - this->z_nodes[zi]) / (this->z_nodes[zi + 1] - this->z_nodes[zi]);

	// Trilinearly interpolate sigmoid polynomial coefficients c
	float c[3];
	for (int i = 0; i < 3; ++i)
	{
#		define co(dx, dy, dz) (*this->coefficient)[maxc][zi + dz][yi + dy][xi + dx][i]
		c[i] = ::std::lerp(
			::std::lerp(::std::lerp(co(0, 0, 0), co(1, 0, 0), dx),
				::std::lerp(co(0, 1, 0), co(1, 1, 0), dx), dy),
			::std::lerp(::std::lerp(co(0, 0, 1), co(1, 0, 1), dx),
				::std::lerp(co(0, 1, 1), co(1, 1, 1), dx), dy), dz);
	}

	return ::framework::physics::rgb_sigmoid_polynomial(c[0], c[1], c[2]);
}

::framework::physics::rgb_to_spectrum_table const * ::framework::physics::rgb_to_spectrum_table::srgb;

void ::framework::physics::rgb_to_spectrum_table::init()
{
	::framework::physics::rgb_to_spectrum_table::srgb = new ::framework::physics::rgb_to_spectrum_table(::framework::physics::srgb_to_spectrum_table_scale, &::framework::physics::srgb_to_spectrum_table_data);
}
