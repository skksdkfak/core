#pragma once

#include "graphics/light.hpp"
#include "graphics/scene_geometry.hpp"
#include "graphics/triangle_mesh_scene_geometry.hpp"

namespace framework::graphics
{
	struct scene_create_info
	{
	};

	class scene
	{
	public:
		virtual ::framework::graphics::triangle_mesh_scene_geometry * create_triangle_mesh_scene_geometry(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info) = 0;

		virtual ::framework::graphics::point_light * create_point_light() = 0;

		virtual ::framework::graphics::image_infinite_light * create_image_infinite_light() = 0;
	};
}