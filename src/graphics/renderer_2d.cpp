#include "renderer_2d.hpp"
#include <resource/resource_manager.hpp>
#include <gpu/utility.hpp>
#include <vector>
#include <array>
#include <cstddef>

::framework::graphics::renderer_2d::renderer_2d(::framework::graphics::renderer_2d_create_info const & create_info) :
	physical_device(create_info.physical_device),
	device(create_info.device),
	width(create_info.width),
	height(create_info.height),
	frame_buffer_count(create_info.frame_buffer_count),
	max_primitive_count(create_info.max_primitive_count),
	max_image_count(create_info.max_image_count),
	pipeline_cache(create_info.pipeline_cache),
	resource_manager(create_info.resource_manager),
	mapped_memory(nullptr),
	num_primitives(0)
{
	this->command_buffers.resize(create_info.frame_buffer_count);

	physical_device->get_memory_properties(&physical_device_memory_properties);

	::std::uint32_t queue_family_property_count;
	physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	physical_device->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	queue_family_indices =
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	this->device->get_queue(queue_family_indices.graphics, 0, &queues.graphics);
	this->device->get_queue(queue_family_indices.compute, 0, &queues.compute);
	this->device->get_queue(queue_family_indices.transfer, 0, &queues.transfer);

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = queue_family_indices.graphics;
	assert_framework_gpu_result(this->device->create_command_pool(&command_pool_create_info, nullptr, &this->command_pool));

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(this->command_buffers.size());
	assert_framework_gpu_result(this->device->allocate_command_buffers(&command_buffer_allocate_info, this->command_buffers.data()));

	::framework::gpu::buffer_create_info buffer_create_info;
	buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
	buffer_create_info.size = this->max_primitive_count * sizeof(::framework::graphics::renderer_2d::vertex_layout);
	buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::vertex_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
	buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	buffer_create_info.queue_family_index_count = 0;
	buffer_create_info.queue_family_indices = nullptr;
	buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
	buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::vertex_buffer_bit;
	buffer_create_info.opaque_capture_address = 0;
	assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->buffer));

	::framework::gpu::memory_requirements memory_requirements;

	::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
	buffer_memory_requirements_info.buffer = this->buffer;
	this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
	memory_allocate_info.device_mask = 0;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
	memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->buffer : nullptr;
	memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
	memory_allocate_info.opaque_capture_address = 0;
	assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->device_memory));

	::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
	bind_buffer_memory_info.buffer = this->buffer;
	bind_buffer_memory_info.memory = this->device_memory;
	bind_buffer_memory_info.memory_offset = 0;
	assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

	::framework::gpu::sampler_create_info sampler_create_info;
	sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
	sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
	sampler_create_info.min_filter = ::framework::gpu::filter::linear;
	sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
	sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.mip_lod_bias = 0.0f;
	sampler_create_info.anisotropy_enable = false;
	sampler_create_info.max_anisotropy = 0.0f;
	sampler_create_info.compare_enable = false;
	sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
	sampler_create_info.min_lod = 0.0f;
	sampler_create_info.max_lod = 1.0f;
	sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
	sampler_create_info.unnormalized_coordinates = false;
	assert_framework_gpu_result(this->device->create_sampler(&sampler_create_info, nullptr, &sampler));

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
	descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].hlsl_register_space = 0;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampler;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

	descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::variable_descriptor_count_bit | ::framework::gpu::descriptor_binding_flags::partially_bound_bit | ::framework::gpu::descriptor_binding_flags::update_after_bind_bit;
	descriptor_set_layout_bindings[1].binding = 1;
	descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[1].hlsl_register_space = 0;
	descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
	descriptor_set_layout_bindings[1].descriptor_count = this->max_image_count;
	descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::update_after_bind_pool_bit;
	descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	assert_framework_gpu_result(this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));

	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.descriptor_set_layouts = &this->descriptor_set_layout;
	pipeline_layout_create_info.push_constant_range_count = 0;
	pipeline_layout_create_info.push_constant_ranges = nullptr;
	assert_framework_gpu_result(this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[2];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::sampler;
	descriptor_pool_sizes[0].descriptor_count = 1;

	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::sampled_image;
	descriptor_pool_sizes[1].descriptor_count = this->max_image_count;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::update_after_bind_bit;
	descriptor_pool_create_info.max_sets = 1;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	assert_framework_gpu_result(this->device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->descriptor_pool));

	::std::uint32_t variable_descriptor_count[] = { this->max_image_count };

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &this->descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = variable_descriptor_count;
	this->device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = this->sampler;
	descriptor_image_info.image_view = nullptr;
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::undefined_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = this->descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampler;
	write_descriptor_sets[0].image_info = &descriptor_image_info;
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);

	::framework::gpu::attachment_reference color_reference;
	color_reference.attachment = 0;
	color_reference.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference depth_reference;
	depth_reference.attachment = 1;
	depth_reference.layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::subpass_description subpass_description;
	subpass_description.flags = ::framework::gpu::subpass_description_flags::none;
	subpass_description.pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
	subpass_description.input_attachment_count = 0;
	subpass_description.input_attachments = nullptr;
	subpass_description.color_attachment_count = 1;
	subpass_description.color_attachments = &color_reference;
	subpass_description.resolve_attachments = nullptr;
	subpass_description.depth_stencil_attachment = &depth_reference;
	subpass_description.preserve_attachment_count = 0;
	subpass_description.preserve_attachments = nullptr;

	::framework::gpu::attachment_description attachment_descriptions[2]
	{
		create_info.color_attachments_description,
		create_info.depth_attachment_description
	};

	::framework::gpu::render_pass_create_info render_pass_create_info;
	render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
	render_pass_create_info.attachment_count = static_cast<::std::uint32_t>(::std::size(attachment_descriptions));
	render_pass_create_info.attachments = attachment_descriptions;
	render_pass_create_info.subpass_count = 1;
	render_pass_create_info.subpasses = &subpass_description;
	render_pass_create_info.dependency_count = create_info.external_dependency_count;
	render_pass_create_info.dependencies = create_info.external_dependencies;
	assert_framework_gpu_result(this->device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass));

	::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
	render_pass_state_create_info.render_pass = this->render_pass;
	render_pass_state_create_info.max_clear_value_count = 2;
	assert_framework_gpu_result(this->device->create_render_pass_state(&render_pass_state_create_info, nullptr, &this->render_pass_state));

	this->create_frame_buffer(create_info.color_attachment, create_info.depth_attachment);
	this->create_pipeline();
}

::framework::graphics::renderer_2d::~renderer_2d()
{
	this->device->destroy_command_pool(this->command_pool, nullptr);
}

void ::framework::graphics::renderer_2d::resize(::framework::graphics::renderer_2d_resize_info const * resize_info)
{
	this->width = resize_info->width;
	this->height = resize_info->height;
	this->device->destroy_frame_buffer(this->frame_buffer, nullptr);
	this->device->destroy_pipeline(this->pipeline, nullptr);
	this->create_frame_buffer(resize_info->color_attachment, resize_info->depth_attachment);
	this->create_pipeline();
}

void ::framework::graphics::renderer_2d::bind_image(::framework::gpu::image_view * image_view, ::framework::gpu::image_layout_flags image_layout, ::std::uint32_t slot)
{
	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = nullptr;
	descriptor_image_info.image_view = image_view;
	descriptor_image_info.image_layout = image_layout;

	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = this->descriptor_set;
	write_descriptor_set.dst_binding = 1;
	write_descriptor_set.dst_array_element = slot;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
	write_descriptor_set.image_info = &descriptor_image_info;
	write_descriptor_set.buffer_info = nullptr;
	write_descriptor_set.texel_buffer_view = nullptr;
	this->device->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);
}

void ::framework::graphics::renderer_2d::begin_update()
{
	assert_framework_gpu_result(this->device->map_memory(this->device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&this->mapped_memory)));
}

::std::uint32_t(::framework::graphics::renderer_2d::add_primitive)()
{
	return this->num_primitives++;
}

void ::framework::graphics::renderer_2d::update_primitive(::std::uint32_t primitive_index, float x0, float y0, float x1, float y1, float s0, float t0, float s1, float t1, ::std::uint32_t image)
{
	this->mapped_memory[primitive_index].positions.x = x0;
	this->mapped_memory[primitive_index].positions.y = x1;
	this->mapped_memory[primitive_index].positions.z = y0;
	this->mapped_memory[primitive_index].positions.w = y1;
	this->mapped_memory[primitive_index].uvs.x = s0;
	this->mapped_memory[primitive_index].uvs.y = s1;
	this->mapped_memory[primitive_index].uvs.z = t0;
	this->mapped_memory[primitive_index].uvs.w = t1;
	this->mapped_memory[primitive_index].image_index = image;
	this->mapped_memory[primitive_index].depth = 0.1f;
}

void ::framework::graphics::renderer_2d::end_update()
{
	this->device->unmap_memory(this->device_memory);
	this->mapped_memory = nullptr;
}

void ::framework::graphics::renderer_2d::update_command_buffer(::std::uint32_t frame_buffer_index)
{
	::framework::gpu::device_size offsets = 0;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::clear_value clear_values[2];
	clear_values[0].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[1].depth_stencil = { 0.0f, 0 };

	::framework::gpu::render_pass_state_info render_pass_state_info;
	render_pass_state_info.frame_buffer = this->frame_buffer;
	render_pass_state_info.render_area.offset.x = 0;
	render_pass_state_info.render_area.offset.y = 0;
	render_pass_state_info.render_area.extent.width = this->width;
	render_pass_state_info.render_area.extent.height = this->height;
	render_pass_state_info.clear_value_count = static_cast<::std::uint32_t>(::std::size(clear_values));
	render_pass_state_info.clear_values = clear_values;
	this->render_pass_state->set_state(render_pass_state_info);

	::framework::gpu::render_pass_begin_info render_pass_begin_info;
	render_pass_begin_info.render_pass_state = this->render_pass_state;

	assert_framework_gpu_result(this->command_buffers[frame_buffer_index]->begin_command_buffer(&command_buffer_begin_info));
	this->command_buffers[frame_buffer_index]->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::inline_);
	this->command_buffers[frame_buffer_index]->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->pipeline);
	this->command_buffers[frame_buffer_index]->bind_descriptor_pool(this->descriptor_pool);
	this->command_buffers[frame_buffer_index]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
	this->command_buffers[frame_buffer_index]->bind_vertex_buffers(0, 1, &this->buffer, &offsets);
	this->command_buffers[frame_buffer_index]->draw(4, this->num_primitives, 0, 0);
	this->command_buffers[frame_buffer_index]->end_render_pass();
	assert_framework_gpu_result(this->command_buffers[frame_buffer_index]->end_command_buffer());
}

void ::framework::graphics::renderer_2d::recompile_shaders()
{
	this->device->wait_idle();

	this->device->destroy_shader_module(this->vertex_shader_module, nullptr);
	this->device->destroy_shader_module(this->fragment_shader_module, nullptr);
	this->device->destroy_pipeline(this->pipeline, nullptr);

	this->create_pipeline();
}

void ::framework::graphics::renderer_2d::create_frame_buffer(::framework::gpu::attachment_view_and_descriptor color_attachment, ::framework::gpu::attachment_view_and_descriptor depth_attachment)
{
	::framework::gpu::attachment_view_and_descriptor attachments[2];
	attachments[0] = color_attachment;
	attachments[1] = depth_attachment;

	::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
	frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
	frame_buffer_create_info.render_pass = this->render_pass;
	frame_buffer_create_info.width = this->width;
	frame_buffer_create_info.height = this->height;
	frame_buffer_create_info.layers = 1;
	frame_buffer_create_info.attachment_count = static_cast<::std::uint32_t>(::std::size(attachments));
	frame_buffer_create_info.attachments = attachments;
	assert_framework_gpu_result(this->device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffer));
}

void ::framework::graphics::renderer_2d::create_pipeline()
{
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/2d.vsh";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/2d.vert.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->vertex_shader_module = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/2d.psh";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "core/src/kernels/glsl/2d.frag.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->fragment_shader_module = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
	pipeline_shader_stage_create_infos[0].module = this->vertex_shader_module;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	pipeline_shader_stage_create_infos[1].module = this->fragment_shader_module;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	::framework::gpu::viewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(width);
	viewport.height = static_cast<float>(height);
	viewport.min_depth = 0.0f;
	viewport.max_depth = 1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = this->width;
	scissor.extent.height = this->height;

	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
	pipeline_color_blend_attachment_state.blend_enable = true;
	pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::src_alpha;
	pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
	pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
	pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[1];
	vertex_input_binding_descriptions[0].binding = 0;
	vertex_input_binding_descriptions[0].stride = sizeof(::framework::graphics::renderer_2d::vertex_layout);
	vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::instance;

	::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[4];
	// Location 0 : Position
	vertex_input_attribute_descriptions[0].location = 0;
	vertex_input_attribute_descriptions[0].binding = 0;
	vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32a32_sfloat;
	vertex_input_attribute_descriptions[0].offset = offsetof(::framework::graphics::renderer_2d::vertex_layout, positions);
	// Location 1 : UV
	vertex_input_attribute_descriptions[1].location = 1;
	vertex_input_attribute_descriptions[1].binding = 0;
	vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32a32_sfloat;
	vertex_input_attribute_descriptions[1].offset = offsetof(::framework::graphics::renderer_2d::vertex_layout, uvs);
	// Location 2 : ImageIndex
	vertex_input_attribute_descriptions[2].location = 2;
	vertex_input_attribute_descriptions[2].binding = 0;
	vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32_uint;
	vertex_input_attribute_descriptions[2].offset = offsetof(::framework::graphics::renderer_2d::vertex_layout, image_index);
	// Location 3 : depth
	vertex_input_attribute_descriptions[3].location = 3;
	vertex_input_attribute_descriptions[3].binding = 0;
	vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32_sfloat;
	vertex_input_attribute_descriptions[3].offset = offsetof(::framework::graphics::renderer_2d::vertex_layout, depth);

	::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
	vertex_input_state.vertex_binding_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_binding_descriptions));
	vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
	vertex_input_state.vertex_attribute_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_attribute_descriptions));
	vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_strip;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::pipeline_multisample_state_create_info pipeline_multisample_state_create_info;
	pipeline_multisample_state_create_info.flags = ::framework::gpu::pipeline_multisample_state_create_flags::none;
	pipeline_multisample_state_create_info.rasterization_samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	pipeline_multisample_state_create_info.sample_shading_enable = false;
	pipeline_multisample_state_create_info.min_sample_shading = 0.0f;
	pipeline_multisample_state_create_info.sample_mask = nullptr;
	pipeline_multisample_state_create_info.alpha_to_coverage_enable = false;
	pipeline_multisample_state_create_info.alpha_to_one_enable = false;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
	pipeline_depth_stencil_state_create_info.depth_test_enable = true;
	pipeline_depth_stencil_state_create_info.depth_write_enable = true;
	pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::greater;
	pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
	pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
	pipeline_depth_stencil_state_create_info.back.fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.pass_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.depth_fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.back.compare_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.write_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.reference = 0;
	pipeline_depth_stencil_state_create_info.front = pipeline_depth_stencil_state_create_info.back;
	pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
	pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = &pipeline_multisample_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = nullptr;
	graphics_pipeline_create_info.layout = this->pipeline_layout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 0;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->pipeline));
}