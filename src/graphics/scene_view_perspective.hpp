#pragma once

#include "scene_view.hpp"

namespace framework::graphics
{
	struct scene_view_perspective_create_info
	{
	};

	class scene_view_perspective : public scene_view
	{
	public:
		virtual ~scene_view_perspective() = default;

		virtual void set_fov(float vertical_fov_radians) = 0;

		virtual void set_aspect_ratio(float aspect_height_over_width) = 0;

		virtual void set_near_z(float near_z_clip) = 0;

		virtual void set_far_z(float far_z_clip) = 0;

		virtual void reverse_z(bool enable) = 0;

		virtual void inverse_y(bool enable) = 0;
	};
}