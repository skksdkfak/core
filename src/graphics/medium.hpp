#pragma once

#include "coroutine/shared_task.hpp"

namespace framework::graphics
{
	class medium
	{
	public:
		virtual ::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept = 0;
	};
}