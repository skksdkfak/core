#pragma once

#include <cstdint>

namespace framework::graphics
{
	struct geometry_instance_id
	{
		explicit geometry_instance_id(::std::uint32_t instance_id, ::std::uint32_t geometry_index) noexcept;

		operator ::std::uint32_t() const noexcept;

		::std::uint32_t index;
	};
}

#include "graphics/scene_types.inl"