#include "scene.hpp"
#include "scene_renderer.hpp"
#include "scene_geometry.hpp"
#include "triangle_mesh_scene_geometry.hpp"
#include "gpu/utility.hpp"

::framework::graphics::deferred_shading::scene::scene(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::scene_create_info * create_info)
{
	//::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	//descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::sampled_image;
	//descriptor_pool_sizes[2].descriptor_count = 256;

	//::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	//descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::device_only_bit;
	//descriptor_pool_create_info.max_sets = 1;
	//descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	//descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	//assert_framework_gpu_result(scene_renderer->get_device()->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->descriptor_pool));

	//::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]{ scene_renderer->get_scene_descriptor_set_layout() };

	//::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	//descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	//descriptor_set_allocate_info.descriptor_set_count = 1;
	//descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
	//descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	//assert_framework_gpu_result(scene_renderer->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set));


}

::framework::graphics::triangle_mesh_scene_geometry * ::framework::graphics::deferred_shading::scene::create_triangle_mesh_scene_geometry(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info)
{
	return nullptr;
	//return new ::framework::graphics::deferred_shading::triangle_mesh_scene_geometry(this, create_info);
}

::framework::graphics::point_light * (::framework::graphics::deferred_shading::scene::create_point_light)()
{
	throw ::std::exception(); // todo
	return nullptr; //todo
}

::framework::graphics::image_infinite_light * (::framework::graphics::deferred_shading::scene::create_image_infinite_light)()
{
	throw ::std::exception(); // todo
	return nullptr; //todo
}

void ::framework::graphics::deferred_shading::scene::get_view_primitives(::framework::graphics::scene_view * scene_view, ::framework::memory::allocator * allocator, ::std::uint32_t & primitive_count, ::framework::graphics::deferred_shading::scene_primitive *** primitives)
{
	::std::lock_guard lock_guard(this->queue_mutex);

	this->opaque_primitives.insert(this->opaque_primitives.end(), this->opaque_primitives_queue.begin(), this->opaque_primitives_queue.end());
	this->opaque_primitives_queue.clear();

	::std::uint32_t lprimitive_count = 1u;
	::framework::graphics::deferred_shading::scene_primitive ** lprimitives = allocator->allocate<::framework::graphics::deferred_shading::scene_primitive *>(lprimitive_count);

	for (auto & primitive : this->opaque_primitives)
	{
		lprimitives[lprimitive_count - 1] = primitive;

		lprimitives = allocator->reallocate<::framework::graphics::deferred_shading::scene_primitive *>(lprimitives, ++lprimitive_count);
	}
	lprimitives = allocator->reallocate<::framework::graphics::deferred_shading::scene_primitive *>(lprimitives, --lprimitive_count);

	primitive_count = lprimitive_count;
	*primitives = lprimitives;
}

void ::framework::graphics::deferred_shading::scene::add_opaque_primitive(::framework::graphics::deferred_shading::scene_primitive * primitive)
{
	this->opaque_primitives_queue.push_back(primitive);
}
