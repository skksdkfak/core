#include "graphics/deferred_shading/shadow_renderer.hpp"
#include "graphics/deferred_shading/scene_renderer.hpp"
#include "graphics/deferred_shading/scene_primitive.hpp"
#include "resource/resource_manager.hpp"
#include "gpu/utility.hpp"
#include "coroutine/static_thread_pool.hpp"
#include "coroutine/sync_wait.hpp"

::framework::graphics::deferred_shading::shadow_renderer::shadow_renderer(::framework::graphics::deferred_shading::shadow_renderer_create_info const & create_info) :
    scene_renderer(*create_info.scene_renderer),
    physical_device(create_info.physical_device),
    device(create_info.device),
    shadow_map_width(create_info.shadow_map_width),
    shadow_map_height(create_info.shadow_map_height),
    shadow_map_layer_count(create_info.max_shadow_map_count),
    shadow_map_format(create_info.shadow_map_format),
    frame_buffer_count(create_info.frame_buffer_count),
    pipeline_cache(create_info.pipeline_cache),
    thread_count(::std::thread::hardware_concurrency())
{
    this->frame_buffers.resize(this->frame_buffer_count);
    this->command_buffers.resize(this->frame_buffer_count);

    this->physical_device->get_memory_properties(&this->physical_device_memory_properties);

    ::std::uint32_t queue_family_property_count;
    this->physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
    assert(queue_family_property_count > 0);

    ::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
    this->physical_device->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

    queue_family_indices =
    {
        .graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
        .compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
        .transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
    };

    this->device->get_queue(queue_family_indices.graphics, 0, &queues.graphics);
    this->device->get_queue(queue_family_indices.compute, 0, &queues.compute);
    this->device->get_queue(queue_family_indices.transfer, 0, &queues.transfer);

    //this->shadowmap_vertex_shader_module = create_info.resource_manager->load_shader_module(this->physical_device, device, "shadow.vert.sdr");
    //this->shadowmap_geometry_shader_module = create_info.resource_manager->load_shader_module(this->physical_device, device, "shadow.geom.sdr");

    //this->shadowing_vertex_shader_module = create_info.resource_manager->load_shader_module(this->physical_device, device, "shadowing.vert.sdr");
    //this->shadowing_fragment_shader_module = create_info.resource_manager->load_shader_module(this->physical_device, device, "shadowing.geom.sdr");

    ::framework::gpu::command_pool_create_info command_pool_create_info;
    command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
    command_pool_create_info.queue_family_index = queue_family_indices.graphics;
    assert_framework_gpu_result(this->device->create_command_pool(&command_pool_create_info, nullptr, &this->command_pool));

    ::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
    command_buffer_allocate_info.command_pool = this->command_pool;
    command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
    command_buffer_allocate_info.subbuffer_count = 1;
    command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(this->command_buffers.size());
    assert_framework_gpu_result(this->device->allocate_command_buffers(&command_buffer_allocate_info, this->command_buffers.data()));

    ::framework::gpu::image_create_info image_create_info;
    image_create_info.flags = ::framework::gpu::image_create_flags::none;
    image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
    image_create_info.format = this->shadow_map_format;
    image_create_info.extent.width = this->shadow_map_width;
    image_create_info.extent.height = this->shadow_map_height;
    image_create_info.extent.depth = 1;
    image_create_info.mip_levels = 1;
    image_create_info.array_layers = this->shadow_map_layer_count;
    image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
    image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
    image_create_info.usage = ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit | ::framework::gpu::image_usage_flags::sampled_bit;
    image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
    image_create_info.queue_family_index_count = 0;
    image_create_info.queue_family_indices = nullptr;
    image_create_info.initial_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
    image_create_info.optimized_clear_value = nullptr;
    assert_framework_gpu_result(this->device->create_image(&image_create_info, nullptr, &this->shadow_map_image));

    ::framework::gpu::memory_requirements memory_requirements;

    ::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
    image_memory_requirements_info.image = this->shadow_map_image;

    this->device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

    ::framework::gpu::memory_allocate_info memory_allocate_info;
    memory_allocate_info.allocation_size = memory_requirements.size;
    memory_allocate_info.allocation_alignment = memory_requirements.alignment;
    memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
    memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
    memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
    memory_allocate_info.opaque_capture_address = 0;
    assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->shadow_map_device_memory));

    ::framework::gpu::bind_image_memory_info bind_image_memory_info;
    bind_image_memory_info.image = this->shadow_map_image;
    bind_image_memory_info.memory = this->shadow_map_device_memory;
    bind_image_memory_info.memory_offset = 0;
    assert_framework_gpu_result(this->device->bind_image_memory(1, &bind_image_memory_info));

    ::framework::gpu::image_view_create_info image_view_create_info;
    image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
    image_view_create_info.image = this->shadow_map_image;
    image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
    image_view_create_info.format = ::framework::gpu::format::r8_unorm;
    image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
    image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
    image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
    image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
    image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
    image_view_create_info.subresource_range.base_mip_level = 0;
    image_view_create_info.subresource_range.level_count = 1;
    image_view_create_info.subresource_range.base_array_layer = 0;
    image_view_create_info.subresource_range.layer_count = 1;
    assert_framework_gpu_result(this->device->create_image_view(&image_view_create_info, nullptr, &this->shadow_map_image_view));

    this->ubo_dynamic_alignment = (sizeof(::framework::graphics::deferred_shading::shadow_renderer::shadow_map_ubo_data) + this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1) & ~(this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1);

    ::framework::gpu::buffer_create_info buffer_create_info;
    buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
    buffer_create_info.size = this->shadow_map_layer_count * this->ubo_dynamic_alignment;
    buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::vertex_buffer_bit;
    buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
    buffer_create_info.queue_family_index_count = 0;
    buffer_create_info.queue_family_indices = nullptr;
    buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::vertex_buffer_bit;
    assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->uniform_buffer));

    //::framework::gpu::memory_requirements memory_requirements;

    ::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
    buffer_memory_requirements_info.buffer = this->uniform_buffer;

    this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

    //::framework::gpu::memory_allocate_info memory_allocate_info;
    memory_allocate_info.allocation_size = memory_requirements.size;
    memory_allocate_info.allocation_alignment = memory_requirements.alignment;
    memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
    memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->uniform_buffer : nullptr;
    memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
    memory_allocate_info.opaque_capture_address = 0;
    assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->uniform_buffer_device_memory));

    ::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
    bind_buffer_memory_info.buffer = this->uniform_buffer;
    bind_buffer_memory_info.memory = this->uniform_buffer_device_memory;
    bind_buffer_memory_info.memory_offset = 0;
    assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

    assert_framework_gpu_result(this->device->map_memory(this->uniform_buffer_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->ubo_mapped_data));

    ::framework::gpu::attachment_description attachment_description[2];
    // shadowmap attachment
    attachment_description[0].format = this->shadow_map_format;
    attachment_description[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
    attachment_description[0].load_op = ::framework::gpu::attachment_load_op::clear;
    attachment_description[0].store_op = ::framework::gpu::attachment_store_op::store;
    attachment_description[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
    attachment_description[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
    attachment_description[0].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;
    attachment_description[0].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;
    // shading output attachment
    attachment_description[1].format = this->shadow_map_format;
    attachment_description[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
    attachment_description[1].load_op = ::framework::gpu::attachment_load_op::clear;
    attachment_description[1].store_op = ::framework::gpu::attachment_store_op::store;
    attachment_description[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
    attachment_description[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
    attachment_description[1].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;
    attachment_description[1].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

    ::framework::gpu::attachment_reference depth_attachment_reference;
    depth_attachment_reference.attachment = 0;
    depth_attachment_reference.layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

    ::framework::gpu::attachment_reference input_attachment_reference;
    input_attachment_reference.attachment = 0;
    input_attachment_reference.layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

    ::framework::gpu::attachment_reference color_reference;
    color_reference.attachment = 1;
    color_reference.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

    ::framework::gpu::subpass_dependency subpass_dependencies[2];
    subpass_dependencies[0].src_subpass = ::framework::gpu::subpass_external;
    subpass_dependencies[0].dst_subpass = 0;
    subpass_dependencies[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
    subpass_dependencies[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
    subpass_dependencies[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
    subpass_dependencies[0].dst_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_write_bit;
    subpass_dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

    subpass_dependencies[1].src_subpass = 0;
    subpass_dependencies[1].dst_subpass = ::framework::gpu::subpass_external;
    subpass_dependencies[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::late_fragment_tests_bit;
    subpass_dependencies[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
    subpass_dependencies[1].src_access_mask = ::framework::gpu::access_flags::depth_stencil_attachment_write_bit;
    subpass_dependencies[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
    subpass_dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

    ::framework::gpu::subpass_description subpass_descriptions[2];
    subpass_descriptions[0].flags = ::framework::gpu::subpass_description_flags::none;
    subpass_descriptions[0].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
    subpass_descriptions[0].input_attachment_count = 0;
    subpass_descriptions[0].input_attachments = nullptr;
    subpass_descriptions[0].color_attachment_count = 0;
    subpass_descriptions[0].color_attachments = nullptr;
    subpass_descriptions[0].resolve_attachments = nullptr;
    subpass_descriptions[0].depth_stencil_attachment = &depth_attachment_reference;
    subpass_descriptions[0].preserve_attachment_count = 0;
    subpass_descriptions[0].preserve_attachments = nullptr;

    subpass_descriptions[1].flags = ::framework::gpu::subpass_description_flags::none;
    subpass_descriptions[1].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
    subpass_descriptions[1].input_attachment_count = 1;
    subpass_descriptions[1].input_attachments = &input_attachment_reference;
    subpass_descriptions[1].color_attachment_count = 1;
    subpass_descriptions[1].color_attachments = &color_reference;
    subpass_descriptions[1].resolve_attachments = nullptr;
    subpass_descriptions[1].depth_stencil_attachment = &depth_attachment_reference;
    subpass_descriptions[1].preserve_attachment_count = 0;
    subpass_descriptions[1].preserve_attachments = nullptr;

    ::framework::gpu::render_pass_create_info render_pass_create_info;
    render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
    render_pass_create_info.attachment_count = ::std::size(attachment_description);
    render_pass_create_info.attachments = attachment_description;
    render_pass_create_info.subpass_count = ::std::size(subpass_descriptions);
    render_pass_create_info.subpasses = subpass_descriptions;
    render_pass_create_info.dependency_count = ::std::size(subpass_dependencies);
    render_pass_create_info.dependencies = subpass_dependencies;
    assert_framework_gpu_result(this->device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass));

    ::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
    render_pass_state_create_info.render_pass = this->render_pass;
    render_pass_state_create_info.max_clear_value_count = 1;
    assert_framework_gpu_result(this->device->create_render_pass_state(&render_pass_state_create_info, nullptr, &this->render_pass_state));

    {
        ::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
        // ubo
        descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
        descriptor_set_layout_bindings[0].binding = 0;
        descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
        descriptor_set_layout_bindings[0].hlsl_register_space = 0;
        descriptor_set_layout_bindings[0].descriptor_count = 1;
        descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
        descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::geometry_bit;
        descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

        ::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
        descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
        descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
        descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->shadowmap_descriptor_set_layout);

        ::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
        pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
        pipeline_layout_create_info.descriptor_set_layout_count = 1;
        pipeline_layout_create_info.descriptor_set_layouts = &this->shadowmap_descriptor_set_layout;
        pipeline_layout_create_info.push_constant_range_count = 0;
        pipeline_layout_create_info.push_constant_ranges = nullptr;
        this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->shadowmap_pipeline_layout);
    }

    {
        ::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
        // shadow_map
        descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
        descriptor_set_layout_bindings[0].binding = 0;
        descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
        descriptor_set_layout_bindings[0].hlsl_register_space = 0;
        descriptor_set_layout_bindings[0].descriptor_count = 1;
        descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
        descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
        descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

        ::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
        descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
        descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
        descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
        this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->shadowing_descriptor_set_layout);

        ::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
        pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
        pipeline_layout_create_info.descriptor_set_layout_count = 1;
        pipeline_layout_create_info.descriptor_set_layouts = &this->shadowing_descriptor_set_layout;
        pipeline_layout_create_info.push_constant_range_count = 0;
        pipeline_layout_create_info.push_constant_ranges = nullptr;
        this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->shadowing_pipeline_layout);
    }

    create_frame_buffer();
    create_pipeline();
}

::framework::graphics::deferred_shading::shadow_renderer::~shadow_renderer()
{
}

void ::framework::graphics::deferred_shading::shadow_renderer::render(::std::uint32_t buffer_index, ::std::uint32_t primitive_count, ::framework::graphics::deferred_shading::scene_primitive ** primitives)
{
    return;
    if (primitive_count > 0)
    {
        ::framework::gpu::command_buffer_inheritance_info command_buffer_inheritance_info;
        //command_buffer_inheritance_info.frame_buffer = frame_buffers[buffer_index];
        command_buffer_inheritance_info.subpass = 0;
        //command_buffer_inheritance_info.render_pass = this->render_pass;

        ::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
        command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit;
        command_buffer_begin_info.inheritance_info = &command_buffer_inheritance_info;

        ::std::atomic_uint32_t ubo_element_offset = 0;

        auto primitive_draw_task = [&](::framework::coroutine::static_thread_pool & thread_pool, ::std::uint32_t primitive_begin, ::std::uint32_t primitive_end, ::std::uint32_t thread_index)->::framework::coroutine::thread_pool_task<void>
        {
            for (::std::uint32_t primitive_index = primitive_begin; primitive_index < primitive_end; primitive_index++)
            {
                ::framework::graphics::deferred_shading::shadow_renderer::shadow_map_ubo_data shadow_map_ubo_data;
                shadow_map_ubo_data.wvp = ::glm::mat4(1.0f);

                ::std::uint32_t const dynamic_offset = ubo_element_offset++ * static_cast<::std::uint32_t>(this->ubo_dynamic_alignment);
                ::std::memcpy(static_cast<char *>(this->ubo_mapped_data) + dynamic_offset, &shadow_map_ubo_data, sizeof(::framework::graphics::deferred_shading::shadow_renderer::shadow_map_ubo_data));

                auto & thread_data = this->frame_resources[buffer_index].thread_data[thread_index];
                auto & command_buffer_data = thread_data.command_buffer_data;
                auto & command_buffer = thread_data.command_buffer;
                if (!command_buffer_data.is_command_buffer_open)
                {
                    assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));
                    command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->shadowmap_pipeline);

                    command_buffer_data.is_command_buffer_open = true;
                }

                command_buffer->bind_descriptor_pool(this->descriptor_pool);
                command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->shadowmap_pipeline_layout, 0, 1, &this->shadowmap_descriptor_set, 1, &dynamic_offset);
                primitives[primitive_index]->draw(command_buffer);
            }

            co_return;
        };

        ::std::uint32_t const primitives_per_thread = (primitive_count + this->thread_count - 1) / this->thread_count;
        ::std::uint32_t thread_index = 0;
        ::std::uint32_t primitive_begin;
        ::std::uint32_t primitive_end = 0;
        do
        {
            primitive_begin = primitive_end;
            primitive_end = ::std::min(primitive_count, primitive_end + primitives_per_thread);

            primitive_draw_task(this->scene_renderer.get_thread_pool(), primitive_begin, primitive_end, thread_index++);
        } while (primitive_end < primitive_count);
    }

    ::std::vector<::framework::gpu::command_buffer *> shadowmap_command_buffer;
    {
        for (::std::uint32_t thread_index = 0; thread_index < this->thread_count; thread_index++)
        {
            auto worker_function = [&shadowmap_command_buffer](::framework::coroutine::static_thread_pool & thread_pool, ::framework::graphics::deferred_shading::shadow_renderer::thread_data thread_data)->::framework::coroutine::thread_pool_task<void>
            {
                if (thread_data.command_buffer_data.is_command_buffer_open)
                {
                    thread_data.command_buffer->end_command_buffer();
                    shadowmap_command_buffer.push_back(thread_data.command_buffer);
                }

                co_return;
            };

            worker_function(this->scene_renderer.get_thread_pool(), this->frame_resources[buffer_index].thread_data[thread_index]);
        }
    }

    ::framework::gpu::clear_value clear_values[1];
    clear_values[0].depth_stencil = { 1.0f, 0 };

    ::framework::gpu::render_pass_state_info render_pass_state_info;
    render_pass_state_info.frame_buffer = this->frame_buffers[buffer_index];
    render_pass_state_info.render_area.offset.x = 0;
    render_pass_state_info.render_area.offset.y = 0;
    render_pass_state_info.render_area.extent.width = this->shadow_map_width;
    render_pass_state_info.render_area.extent.height = this->shadow_map_height;
    render_pass_state_info.clear_value_count = ::std::size(clear_values);
    render_pass_state_info.clear_values = clear_values;
    this->render_pass_state->set_state(render_pass_state_info);

    ::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
    command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
    command_buffer_begin_info.inheritance_info = nullptr;

    ::framework::gpu::render_pass_begin_info render_pass_begin_info;
    render_pass_begin_info.render_pass_state = this->render_pass_state;

    assert_framework_gpu_result(this->command_buffers[buffer_index]->begin_command_buffer(&command_buffer_begin_info));
    this->command_buffers[buffer_index]->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::secondary_command_buffers);
    this->command_buffers[buffer_index]->execute_commands(static_cast<::std::uint32_t>(shadowmap_command_buffer.size()), shadowmap_command_buffer.data());
    this->command_buffers[buffer_index]->next_subpass(::framework::gpu::subpass_contents::inline_);
    this->command_buffers[buffer_index]->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->shadowing_pipeline);
    this->command_buffers[buffer_index]->bind_descriptor_pool(this->descriptor_pool);
    this->command_buffers[buffer_index]->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->shadowing_pipeline_layout, 0, 1, &this->shadow_descriptor_set, 0, nullptr);
    this->command_buffers[buffer_index]->draw(3, 1, 0, 0);
    this->command_buffers[buffer_index]->end_render_pass();
    assert_framework_gpu_result(this->command_buffers[buffer_index]->end_command_buffer());
}

void ::framework::graphics::deferred_shading::shadow_renderer::create_frame_buffer()
{
    ::framework::gpu::image_view * attachments[2];
    attachments[0] = this->shadow_map_image_view;

    ::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
    frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
    frame_buffer_create_info.render_pass = this->render_pass;
    frame_buffer_create_info.width = this->shadow_map_width;
    frame_buffer_create_info.height = this->shadow_map_height;
    frame_buffer_create_info.layers = this->shadow_map_layer_count;
    frame_buffer_create_info.attachment_count = ::std::size(attachments);
    //frame_buffer_create_info.attachments = attachments;

    for (::std::uint32_t i = 0; i < this->frame_buffer_count; ++i)
    {
        attachments[0] = this->shadow_map_image_view;

        assert_framework_gpu_result(this->device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffers[i]));
    }
}

void ::framework::graphics::deferred_shading::shadow_renderer::create_pipeline()
{
    ::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
    pipeline_shader_stage_create_infos[0].module = this->shadowmap_vertex_shader_module;
    pipeline_shader_stage_create_infos[0].name = "main";
    pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

    pipeline_shader_stage_create_infos[1].module = this->shadowmap_geometry_shader_module;
    pipeline_shader_stage_create_infos[1].name = "main";
    pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::geometry_bit;

    ::framework::gpu::viewport viewport;
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(this->shadow_map_width);
    viewport.height = static_cast<float>(this->shadow_map_height);
    viewport.min_depth = 0.0f;
    viewport.max_depth = 1.0f;

    ::framework::gpu::rect_2d scissor;
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = this->shadow_map_width;
    scissor.extent.height = this->shadow_map_height;

    ::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
    pipeline_viewport_state_create_info.viewport_count = 1;
    pipeline_viewport_state_create_info.scissors = &scissor;
    pipeline_viewport_state_create_info.scissor_count = 1;
    pipeline_viewport_state_create_info.viewports = &viewport;

    ::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
    pipeline_rasterization_state_create_info.depth_clamp_enable = false;
    pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
    pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
    pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::back_bit;
    pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::clockwise;
    pipeline_rasterization_state_create_info.depth_bias_enable = false;
    pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
    pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
    pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
    pipeline_rasterization_state_create_info.line_width = 1.0f;

    ::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
    pipeline_color_blend_attachment_state.blend_enable = true;
    pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::src_alpha;
    pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
    pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
    pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
    pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
    pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
    pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

    ::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
    pipeline_color_blend_state_create_info.logic_op_enable = false;
    pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
    pipeline_color_blend_state_create_info.attachment_count = 1;
    pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
    pipeline_color_blend_state_create_info.blend_constants[0] = 0;
    pipeline_color_blend_state_create_info.blend_constants[1] = 0;
    pipeline_color_blend_state_create_info.blend_constants[2] = 0;
    pipeline_color_blend_state_create_info.blend_constants[3] = 0;

    ::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
    pipeline_depth_stencil_state_create_info.depth_test_enable = true;
    pipeline_depth_stencil_state_create_info.depth_write_enable = true;
    pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::less_or_equal;
    pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
    pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
    pipeline_depth_stencil_state_create_info.front = {};
    pipeline_depth_stencil_state_create_info.back = {};
    pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
    pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
    pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

    ::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[2];
    vertex_input_binding_descriptions[0].binding = 0;
    vertex_input_binding_descriptions[0].stride = sizeof(::glm::vec4) * 2;
    vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::instance;
    vertex_input_binding_descriptions[1].binding = 1;
    vertex_input_binding_descriptions[1].stride = sizeof(::glm::vec4) * 2;
    vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::instance;

    ::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[2];
    // Location 0 : Position
    vertex_input_attribute_descriptions[0].location = 0;
    vertex_input_attribute_descriptions[0].binding = 0;
    vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32a32_sfloat;
    vertex_input_attribute_descriptions[0].offset = 0;
    // Location 1 : UV
    vertex_input_attribute_descriptions[1].location = 1;
    vertex_input_attribute_descriptions[1].binding = 1;
    vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32a32_sfloat;
    vertex_input_attribute_descriptions[1].offset = sizeof(::glm::vec4);

    ::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
    vertex_input_state.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
    vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
    vertex_input_state.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
    vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

    ::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
    pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_strip;
    pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

    ::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
    graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
    graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
    graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
    graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
    graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
    graphics_pipeline_create_info.tessellation_state = nullptr;
    graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
    graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
    graphics_pipeline_create_info.multisample_state = nullptr;
    graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
    graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
    graphics_pipeline_create_info.dynamic_state = nullptr;
    graphics_pipeline_create_info.layout = this->shadowmap_pipeline_layout;
    graphics_pipeline_create_info.render_pass = this->render_pass;
    graphics_pipeline_create_info.subpass = 0;
    graphics_pipeline_create_info.base_pipeline_index = -1;
    graphics_pipeline_create_info.base_pipeline = nullptr;
    assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->shadowmap_pipeline));
}

void ::framework::graphics::deferred_shading::shadow_renderer::create_composition_pipeline()
{
    ::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
    pipeline_shader_stage_create_infos[0].module = this->shadowing_vertex_shader_module;
    pipeline_shader_stage_create_infos[0].name = "main";
    pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

    pipeline_shader_stage_create_infos[1].module = this->shadowing_fragment_shader_module;
    pipeline_shader_stage_create_infos[1].name = "main";
    pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;

    ::framework::gpu::viewport viewport;
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(this->width);
    viewport.height = static_cast<float>(this->height);
    viewport.min_depth = 0.0f;
    viewport.max_depth = 1.0f;

    ::framework::gpu::rect_2d scissor;
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = this->width;
    scissor.extent.height = this->height;

    ::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
    pipeline_viewport_state_create_info.viewport_count = 1;
    pipeline_viewport_state_create_info.scissors = &scissor;
    pipeline_viewport_state_create_info.scissor_count = 1;
    pipeline_viewport_state_create_info.viewports = &viewport;

    ::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
    pipeline_rasterization_state_create_info.depth_clamp_enable = false;
    pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
    pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
    pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
    pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
    pipeline_rasterization_state_create_info.depth_bias_enable = false;
    pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
    pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
    pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
    pipeline_rasterization_state_create_info.line_width = 1.0f;

    ::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
    pipeline_color_blend_attachment_state.blend_enable = false;
    pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::zero;
    pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
    pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
    pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
    pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
    pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
    pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

    ::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
    pipeline_color_blend_state_create_info.logic_op_enable = false;
    pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
    pipeline_color_blend_state_create_info.attachment_count = 1;
    pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
    pipeline_color_blend_state_create_info.blend_constants[0] = 0;
    pipeline_color_blend_state_create_info.blend_constants[1] = 0;
    pipeline_color_blend_state_create_info.blend_constants[2] = 0;
    pipeline_color_blend_state_create_info.blend_constants[3] = 0;

    ::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
    pipeline_depth_stencil_state_create_info.depth_test_enable = false;
    pipeline_depth_stencil_state_create_info.depth_write_enable = false;
    pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::never;
    pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
    pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
    pipeline_depth_stencil_state_create_info.front = {};
    pipeline_depth_stencil_state_create_info.back = {};
    pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
    pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
    pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

    ::framework::gpu::pipeline_vertex_input_state_create_info pipeline_vertex_input_state_create_info;
    pipeline_vertex_input_state_create_info.vertex_binding_description_count = 0;
    pipeline_vertex_input_state_create_info.vertex_binding_descriptions = nullptr;
    pipeline_vertex_input_state_create_info.vertex_attribute_description_count = 0;
    pipeline_vertex_input_state_create_info.vertex_attribute_descriptions = nullptr;

    ::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
    pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
    pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

    ::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
    graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
    graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
    graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
    graphics_pipeline_create_info.vertex_input_state = &pipeline_vertex_input_state_create_info;
    graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
    graphics_pipeline_create_info.tessellation_state = nullptr;
    graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
    graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
    graphics_pipeline_create_info.multisample_state = nullptr;
    graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
    graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
    graphics_pipeline_create_info.dynamic_state = nullptr;
    graphics_pipeline_create_info.layout = this->shadowing_pipeline_layout;
    graphics_pipeline_create_info.render_pass = this->render_pass;
    graphics_pipeline_create_info.subpass = 1;
    graphics_pipeline_create_info.base_pipeline_index = -1;
    graphics_pipeline_create_info.base_pipeline = nullptr;
    assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->shadowing_pipeline));
}