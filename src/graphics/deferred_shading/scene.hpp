#pragma once

#include "graphics/scene.hpp"
#include "graphics/scene_geometry.hpp"
#include "graphics/scene_view.hpp"
#include "scene_primitive.hpp"
#include "gpu/core.hpp"
#include "memory/allocator.hpp"
#include <vector>
#include <list>
#include <mutex>

namespace framework::graphics::deferred_shading
{
	class scene_renderer;

	class scene : public ::framework::graphics::scene
	{
	public:
		scene(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::scene_create_info * create_info);

		::framework::graphics::triangle_mesh_scene_geometry * create_triangle_mesh_scene_geometry(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info) override;

		::framework::graphics::point_light * create_point_light() override;

		::framework::graphics::image_infinite_light * create_image_infinite_light() override;

		void get_view_primitives(::framework::graphics::scene_view * scene_view, ::framework::memory::allocator * allocator, ::std::uint32_t & primitive_count, scene_primitive *** primitives);

		void add_opaque_primitive(scene_primitive * primitive);

	private:
		::std::vector<scene_primitive *> opaque_primitives;
		::std::mutex queue_mutex;
		::std::list<scene_primitive *> opaque_primitives_queue;
		//::framework::gpu::descriptor_pool * descriptor_pool;
		//::framework::gpu::descriptor_set * descriptor_set;
	};
}