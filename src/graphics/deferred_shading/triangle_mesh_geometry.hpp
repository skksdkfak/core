#pragma once

#include "graphics/triangle_mesh_geometry.hpp"
#include "coroutine/task.hpp"
#include <cstdint>
#include <gpu/core.hpp>

namespace framework::graphics::deferred_shading
{
    class scene_renderer;

	enum vertex_attribute
	{
		POSITION,
		NORMAL,
		BINORMAL,
		TANGENT,
		UV,
		COLOR,
		BLEND_INDICES,
		BLEND_WEIGHTS,
		NUM_VERTEX_ATTRIBUTES
	};

	struct vertex_info
	{
		::framework::gpu::format format;
		::framework::gpu::buffer * buffer;
	};

	class triangle_mesh_geometry : public ::framework::graphics::triangle_mesh_geometry
	{
	public:
		struct vertex_layout
		{
			::std::uint32_t first_binding;
			::std::uint32_t binding_count;
			::framework::gpu::buffer ** buffers;
			::framework::gpu::device_size * offsets;
		};

		struct submesh
		{
			struct ::framework::graphics::deferred_shading::triangle_mesh_geometry::vertex_layout * vertex_layout;
			::std::uint32_t index_count;
			::std::uint32_t index_offset;
		};

		::std::uint32_t get_submesh_count() const { return this->submesh_count; }

		struct ::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh const & get_submesh(::std::uint32_t submesh_index) const { return submeshes[submesh_index]; }

		::framework::gpu::buffer * get_index_buffer() { return indices_buffer; }

	private:
		friend class ::framework::graphics::deferred_shading::scene_renderer;

		~triangle_mesh_geometry();

		::framework::coroutine::lazy_task<void> initialize(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::triangle_mesh_geometry_create_info * create_info);

		::framework::graphics::deferred_shading::scene_renderer * scene_renderer;
        struct ::framework::graphics::deferred_shading::triangle_mesh_geometry::vertex_layout vertex_layout;
		struct ::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh * submeshes;
		::std::uint32_t submesh_count;
		::framework::gpu::buffer * indices_buffer;
		struct ::framework::graphics::deferred_shading::vertex_info vertices[NUM_VERTEX_ATTRIBUTES];
		::framework::coroutine::lazy_task<void> initialize_task;
	};
}