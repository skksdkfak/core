#include "triangle_mesh_geometry.hpp"
#include "scene_renderer.hpp"
#include "gpu/utility.hpp"
#include "coroutine/sync_wait.hpp"
#include <glm/glm.hpp>
#include <cassert>

::framework::coroutine::lazy_task<void>(::framework::graphics::deferred_shading::triangle_mesh_geometry::initialize)(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::triangle_mesh_geometry_create_info * create_info)
{
	this->scene_renderer = scene_renderer;
	this->submesh_count = create_info->submesh_count;

	::framework::coroutine::lazy_task<::std::uint32_t> acquire_device_transfer_resources = scene_renderer->acquire_device_transfer_resources();
	::std::uint32_t const device_transfer_resources_index = co_await(acquire_device_transfer_resources);
	struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources & device_transfer_resources = scene_renderer->get_device_transfer_resources(device_transfer_resources_index);

	::framework::gpu::device_size staging_buffer_data_offset = 0;

	::std::byte * mapped_staging_buffer_data;
	assert_framework_gpu_result(scene_renderer->get_device()->map_memory(scene_renderer->get_staging_device_memory(), 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&mapped_staging_buffer_data)));

	::framework::gpu::command_buffer * command_buffer = device_transfer_resources.command_buffer;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

	auto const & init_index_buffer = [&]<typename T>(::framework::gpu::buffer * &buffer, ::framework::gpu::buffer_usage_flags usage, T * vertex_attribute, ::std::uint32_t element_count)
	{
		::framework::gpu::device_size size = sizeof(T) * element_count;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | usage;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_queue_family_indices().transfer;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->scene_renderer->get_device()->create_buffer(&buffer_create_info, nullptr, &buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = buffer;
		this->scene_renderer->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = buffer;
		bind_buffer_memory_info.memory = this->scene_renderer->get_indices_device_memory();
		bind_buffer_memory_info.memory_offset = this->scene_renderer->allocate_indices_device_memory(size, memory_requirements.alignment);
		assert(staging_buffer_data_offset + size <= this->scene_renderer->get_staging_buffer_size());
		assert(bind_buffer_memory_info.memory_offset + size <= this->scene_renderer->get_indices_buffer_size());
		assert_framework_gpu_result(this->scene_renderer->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::std::memcpy(mapped_staging_buffer_data + staging_buffer_data_offset, vertex_attribute, size);

		::framework::gpu::buffer_copy buffer_copy;
		buffer_copy.src_offset = staging_buffer_data_offset;
		buffer_copy.dst_offset = 0;
		buffer_copy.size = size;

		command_buffer->copy_buffer(scene_renderer->get_staging_buffer(), buffer, 1, &buffer_copy);

		staging_buffer_data_offset += size;
	};

	auto const & init_vertex_buffer = [&]<typename T>(::framework::gpu::buffer * &buffer, ::framework::gpu::buffer_usage_flags usage, T * vertex_attribute, ::std::uint32_t element_count)
	{
		::framework::gpu::device_size size = sizeof(T) * element_count;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | usage;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_queue_family_indices().transfer;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->scene_renderer->get_device()->create_buffer(&buffer_create_info, nullptr, &buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = buffer;
		this->scene_renderer->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = buffer;
		bind_buffer_memory_info.memory = this->scene_renderer->get_vertices_device_memory();
		bind_buffer_memory_info.memory_offset = this->scene_renderer->allocate_vertices_device_memory(size, memory_requirements.alignment);
		assert(staging_buffer_data_offset + size <= this->scene_renderer->get_staging_buffer_size());
		assert(bind_buffer_memory_info.memory_offset + size <= this->scene_renderer->get_vertices_buffer_size());
		assert_framework_gpu_result(this->scene_renderer->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::std::memcpy(mapped_staging_buffer_data + staging_buffer_data_offset, vertex_attribute, size);

		::framework::gpu::buffer_copy buffer_copy;
		buffer_copy.src_offset = staging_buffer_data_offset;
		buffer_copy.dst_offset = 0;
		buffer_copy.size = size;

		command_buffer->copy_buffer(scene_renderer->get_staging_buffer(), buffer, 1, &buffer_copy);

		staging_buffer_data_offset += size;
	};

	if (create_info->indices)
		init_index_buffer(indices_buffer, ::framework::gpu::buffer_usage_flags::index_buffer_bit, create_info->indices, create_info->index_count);
	if (create_info->positions)
		init_vertex_buffer(this->vertices[::framework::graphics::deferred_shading::vertex_attribute::POSITION].buffer, ::framework::gpu::buffer_usage_flags::vertex_buffer_bit, create_info->positions, create_info->vertex_count);
	if (create_info->normals)
		init_vertex_buffer(this->vertices[::framework::graphics::deferred_shading::vertex_attribute::NORMAL].buffer, ::framework::gpu::buffer_usage_flags::vertex_buffer_bit, create_info->normals, create_info->vertex_count);
	if (create_info->binormals)
		init_vertex_buffer(this->vertices[::framework::graphics::deferred_shading::vertex_attribute::BINORMAL].buffer, ::framework::gpu::buffer_usage_flags::vertex_buffer_bit, create_info->binormals, create_info->vertex_count);
	if (create_info->tangents)
		init_vertex_buffer(this->vertices[::framework::graphics::deferred_shading::vertex_attribute::TANGENT].buffer, ::framework::gpu::buffer_usage_flags::vertex_buffer_bit, create_info->tangents, create_info->vertex_count);
	if (create_info->uvs)
		init_vertex_buffer(this->vertices[::framework::graphics::deferred_shading::vertex_attribute::UV].buffer, ::framework::gpu::buffer_usage_flags::vertex_buffer_bit, create_info->uvs, create_info->vertex_count);

	assert_framework_gpu_result(command_buffer->end_command_buffer());

	scene_renderer->get_device()->unmap_memory(scene_renderer->get_staging_device_memory());

	auto release_device_transfer_resources = scene_renderer->release_device_transfer_resources(device_transfer_resources_index);
	co_await release_device_transfer_resources;

	::std::uint32_t binding_count = 0;
	::framework::gpu::device_size vertex_offsets[::framework::graphics::deferred_shading::vertex_attribute::NUM_VERTEX_ATTRIBUTES];
	::framework::gpu::buffer * vertex_buffers[::framework::graphics::deferred_shading::vertex_attribute::NUM_VERTEX_ATTRIBUTES];
	for (::std::uint32_t i = 0; i < ::framework::graphics::deferred_shading::vertex_attribute::NUM_VERTEX_ATTRIBUTES; i++)
	{
		if (vertex_buffers[binding_count] = this->vertices[static_cast<::framework::graphics::deferred_shading::vertex_attribute>(i)].buffer)
		{
			vertex_offsets[binding_count] = 0;
			++binding_count;
		}
	}

	this->vertex_layout.first_binding = 0;
	this->vertex_layout.binding_count = binding_count;
	this->vertex_layout.buffers = new ::framework::gpu::buffer * [binding_count];
	this->vertex_layout.offsets = new ::framework::gpu::device_size[binding_count];
	::std::memcpy(this->vertex_layout.buffers, vertex_buffers, sizeof(::framework::gpu::buffer *) * binding_count);
	::std::memcpy(this->vertex_layout.offsets, vertex_offsets, sizeof(::framework::gpu::device_size) * binding_count);

	this->submeshes = new ::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh[create_info->submesh_count];
	for (::std::uint32_t i = 0; i < create_info->submesh_count; i++)
	{
		this->submeshes[i] = { &this->vertex_layout, create_info->submeshes[i].index_count, create_info->submeshes[i].index_offset };
	}

	co_return;
}

::framework::graphics::deferred_shading::triangle_mesh_geometry::~triangle_mesh_geometry()
{
	if (this->indices_buffer)
	{
		this->scene_renderer->get_device()->destroy_buffer(this->indices_buffer, nullptr);
	}

	for (::std::uint32_t i = 0; i < ::framework::graphics::deferred_shading::vertex_attribute::NUM_VERTEX_ATTRIBUTES; i++)
	{
		if (this->vertices[i].buffer)
		{
			this->scene_renderer->get_device()->destroy_buffer(this->vertices[i].buffer, nullptr);
		}
	}
	delete[] this->submeshes;
	delete[] this->vertex_layout.buffers;
	delete[] this->vertex_layout.offsets;
}