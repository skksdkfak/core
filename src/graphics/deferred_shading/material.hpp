#pragma once

#include "graphics/material.hpp"
#include "gpu/core.hpp"

namespace framework::graphics::deferred_shading
{
	class scene_renderer;

	struct material_device_data
	{
		::std::uint32_t diffuse_texture;
	};

	class material : public ::framework::graphics::material
	{
	public:
		explicit material(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::material_create_info const & create_info, ::std::uint32_t id);

		void set_thin_surface(bool thin_surface) override;

		void set_roughness(::glm::vec2 roughness) override;

		::framework::graphics::image * get_diffuse_texture() const override;

		void set_diffuse_texture(::framework::graphics::image * image) override;

		void set_emissive_image(::framework::graphics::image * emissive_image) override;

		::framework::graphics::deferred_shading::material_device_data const & get_device_data() const { return this->device_data; }

	private:
		::std::uint32_t id;
		::framework::graphics::deferred_shading::material_device_data device_data;
	};
}