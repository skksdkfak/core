#include "graphics/deferred_shading/image.hpp"
#include "graphics/deferred_shading/material.hpp"
#include "graphics/deferred_shading/scene.hpp"
#include "graphics/deferred_shading/scene_renderer.hpp"
#include "graphics/deferred_shading/scene_view_perspective.hpp"
#include "graphics/deferred_shading/triangle_mesh_geometry.hpp"
#include "graphics/deferred_shading/triangle_mesh_scene_geometry.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/when_all_ready.hpp"
#include "gpu/utility.hpp"
#include <glm/glm.hpp>
#include <iostream>
#include <algorithm>
#include <execution>

#define NUM_OBJECTS 1024 * 8

::framework::graphics::deferred_shading::scene_renderer::scene_renderer(::framework::graphics::deferred_shading::scene_renderer_create_info const * create_info) :
	physical_device(create_info->physical_device),
	device(create_info->device),
	m_frame_buffer_count(create_info->frame_buffer_count),
	width(create_info->width),
	height(create_info->height),
	color_attachments_format(create_info->color_attachments_format),
	pipeline_cache(create_info->pipeline_cache),
	m_reverse_z_depth(create_info->reverse_z_depth),
	single_frame_allocator(create_info->single_frame_allocator),
	scene(nullptr),
	resource_manager(create_info->resource_manager),
	thread_pool(*create_info->thread_pool),
	max_texture_count(2048),
	max_material_count(NUM_OBJECTS),
	texture_counter(0),
	material_counter(0),
	indices_device_memory_offset(0),
	vertices_device_memory_offset(0)
{
	this->physical_device->get_features(&this->physical_device_features);

	this->physical_device->get_properties(&this->physical_device_properties);

	this->physical_device->get_memory_properties(&this->physical_device_memory_properties);

	::std::uint32_t queue_family_property_count;
	this->physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	this->physical_device->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	queue_family_indices =
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	this->device->get_queue(queue_family_indices.graphics, 0, &queues.graphics);
	this->device->get_queue(queue_family_indices.compute, 0, &queues.compute);
	this->device->get_queue(queue_family_indices.transfer, 0, &queues.transfer);

	//m_camera = entity_manager::create<CEntity>();
	//auto cameraTransform = m_camera.assign<CTransform>();
	//auto camera = m_camera.assign<camera>();
	//camera->setZRange(0.01f, 1024.f);
	//camera->reverseZ(false);
	//camera->inverseY(apiType == GAPIType::TYPE_VK);
	//m_camera.assign<CameraController>(::glm::vec3(0.0f, 1.0f, 0.0f));

	light_pos = ::glm::vec4();

	create_frame_buffer_attachments();
	create_render_pass();
	create_frame_buffer(create_info->color_attachments);

	{
		this->ubo_dynamic_alignment = (sizeof(path_tracer_parameters) + this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1) & ~(this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->ubo_dynamic_alignment * NUM_OBJECTS;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->m_ubo));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->m_ubo;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->m_ubo : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->ubo_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->m_ubo;
		bind_buffer_memory_info.memory = this->ubo_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->device->map_memory(this->ubo_device_memory, 0, this->ubo_dynamic_alignment * NUM_OBJECTS, ::framework::gpu::memory_map_flags::none, &this->mapped_data));
	}

	{
		this->material_ubo_dynamic_alignment = (sizeof(::framework::graphics::deferred_shading::material_device_data) + this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1) & ~(this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->material_ubo_dynamic_alignment * this->max_material_count;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->material_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->material_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->material_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->material_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->material_buffer;
		bind_buffer_memory_info.memory = this->material_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->device->map_memory(this->material_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->material_mapped_data));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(float) * 4 * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &ssbo));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = ssbo;

		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &m_pSsboDeviceMemory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = ssbo;
		bind_buffer_memory_info.memory = m_pSsboDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.flags = ::framework::gpu::buffer_view_create_flags::none;
		buffer_view_create_info.buffer = ssbo;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = sizeof(float) * 4 * 2;
		assert_framework_gpu_result(this->device->create_buffer_view(&buffer_view_create_info, nullptr, &m_pSSBOView));
	}

	::framework::gpu::buffer * pSSBOUploadBuffer;
	::framework::gpu::device_memory * pSSBOUploadDeviceMemory;
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(float) * 4 * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &pSSBOUploadBuffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = pSSBOUploadBuffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? pSSBOUploadBuffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &pSSBOUploadDeviceMemory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = pSSBOUploadBuffer;
		bind_buffer_memory_info.memory = pSSBOUploadDeviceMemory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		float color[2][4] = { { 1.0f, 0.1f, 0.7f, 1.0f }, { 0.1f, 0.89f, 0.31f, 1.0f } };

		void * mapped_data;
		this->device->map_memory(pSSBOUploadDeviceMemory, 0, sizeof(float) * 4 * 2, ::framework::gpu::memory_map_flags::none, &mapped_data);
		::std::memcpy(mapped_data, color, sizeof(float) * 4 * 2);
		this->device->unmap_memory(pSSBOUploadDeviceMemory);

		::framework::gpu::queue * transferQueue;
		this->device->get_queue(queue_family_indices.transfer, 0, &transferQueue);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		::framework::gpu::command_pool * command_pool;
		this->device->create_command_pool(&command_pool_create_info, nullptr, &command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = 1;
		::framework::gpu::command_buffer * copyCmd;
		this->device->allocate_command_buffers(&command_buffer_allocate_info, &copyCmd);

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info{};
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		copyCmd->begin_command_buffer(&command_buffer_begin_info);
		::framework::gpu::buffer_copy buffer_copy{};
		buffer_copy.size = sizeof(float) * 4 * 2;
		copyCmd->copy_buffer(pSSBOUploadBuffer, ssbo, 1, &buffer_copy);
		copyCmd->end_command_buffer();

		::framework::gpu::fence * fence;
		::framework::gpu::fence_create_info fence_create_info;
		fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
		this->device->create_fence(&fence_create_info, nullptr, &fence);

		::framework::gpu::command_buffer_submit_info command_buffer_info;
		command_buffer_info.command_buffer = copyCmd;
		command_buffer_info.device_mask = 0;

		::framework::gpu::submit_info submit_info;
		submit_info.flags = ::framework::gpu::submit_flags::none;
		submit_info.wait_semaphore_info_count = 0;
		submit_info.wait_semaphore_infos = nullptr;
		submit_info.command_buffer_info_count = 1;
		submit_info.command_buffer_infos = &command_buffer_info;
		submit_info.signal_semaphore_info_count = 0;
		submit_info.signal_semaphore_infos = nullptr;
		transferQueue->submit(1, &submit_info, fence);

		this->device->wait_for_fences(1, &fence, true, UINT64_MAX);
		this->device->wait_for_fences(1, &fence, true, UINT64_MAX);
		this->device->destroy_fence(fence, nullptr);
	}

	{
		::framework::gpu::sampler_create_info sampler_create_info;
		sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
		sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
		sampler_create_info.min_filter = ::framework::gpu::filter::linear;
		sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
		sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.mip_lod_bias = 0.0f;
		sampler_create_info.anisotropy_enable = false;
		sampler_create_info.max_anisotropy = 1.0f;
		sampler_create_info.compare_enable = false;
		sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
		sampler_create_info.min_lod = 0.0f;
		sampler_create_info.max_lod = 0.0f;
		sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
		sampler_create_info.unnormalized_coordinates = false;
		assert_framework_gpu_result(this->device->create_sampler(&sampler_create_info, nullptr, &this->sampler));
	}

	// scene offscreen rendering
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		// ubo
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::vertex_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// material
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// sampler
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampler;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = &this->sampler;
		// textures
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::variable_descriptor_count_bit | ::framework::gpu::descriptor_binding_flags::partially_bound_bit | ::framework::gpu::descriptor_binding_flags::update_after_bind_bit;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = this->max_texture_count;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::update_after_bind_pool_bit;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->scene_descriptor_set_layout);

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->scene_descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->scene_pipeline_layout);
	}

	// composition
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// position
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// normal
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// albedo
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->composition_descriptor_set_layout);

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::glm::vec4);
		push_constant_ranges.hlsl_shader_register = 1;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->composition_descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &m_pCompositionPipelineLayout);
	}

	// transparent rendering
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// ubo
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::vertex_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// depth
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// color texture
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = ::std::size(descriptor_set_layout_bindings);
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->transparent_descriptor_set_layout);

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->transparent_descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->transparent_pipeline_layout);
	}

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[6];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	descriptor_pool_sizes[0].descriptor_count = 1;

	descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_pool_sizes[1].descriptor_count = 1;

	descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::sampled_image;
	descriptor_pool_sizes[2].descriptor_count = 2048;

	descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_pool_sizes[3].descriptor_count = 1;

	descriptor_pool_sizes[4].type = ::framework::gpu::descriptor_type::storage_image;
	descriptor_pool_sizes[4].descriptor_count = 1;

	descriptor_pool_sizes[5].type = ::framework::gpu::descriptor_type::uniform_buffer;
	descriptor_pool_sizes[5].descriptor_count = 1;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::update_after_bind_bit; // todo: create separate descriptor pool only for those descriptors that uses update_after_bind_bit
	descriptor_pool_create_info.max_sets = 4;
	descriptor_pool_create_info.pool_size_count = ::std::size(descriptor_pool_sizes);
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	this->device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->descriptor_pool);

	::framework::gpu::descriptor_set * descriptor_sets[3];
	::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]{ this->scene_descriptor_set_layout, this->composition_descriptor_set_layout, this->transparent_descriptor_set_layout };
	::std::uint32_t variable_descriptor_count[3] = { this->max_texture_count, 0, 0 };

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = 3;
	descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
	descriptor_set_allocate_info.variable_descriptor_counts = variable_descriptor_count;
	device->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets);

	this->scene_descriptor_set = descriptor_sets[0];
	this->composition_descriptor_set = descriptor_sets[1];
	this->transparent_descriptor_set = descriptor_sets[2];

	{
		// https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkDescriptorBufferInfo.html
		// For VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC and VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC descriptor types,
		// offset is the base offset from which the dynamic offset is applied and !!!range is the static size used for all dynamic offsets.!!!
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
		descriptor_buffer_infos[0].buffer = this->m_ubo;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = this->ubo_dynamic_alignment;
		descriptor_buffer_infos[0].stride = this->ubo_dynamic_alignment;

		descriptor_buffer_infos[1].buffer = this->material_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = this->material_ubo_dynamic_alignment;
		descriptor_buffer_infos[1].stride = this->material_ubo_dynamic_alignment;

		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = this->scene_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = this->scene_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);

		//this->scene_vertex_shader_module = this->resource_manager->load_shader_module(this->physical_device, this->device, "gbuffer.vert.sdr");
		//this->scene_fragment_shader_module = this->resource_manager->load_shader_module(this->physical_device, this->device, "gbuffer.frag.sdr");

		this->create_scene_pipeline();
	}

	{
		::framework::gpu::descriptor_image_info descriptor_image_infos[3];
		descriptor_image_infos[0].sampler = nullptr;
		descriptor_image_infos[0].image_view = this->attachments.position.image_view;
		descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		descriptor_image_infos[1].sampler = nullptr;
		descriptor_image_infos[1].image_view = this->attachments.normal.image_view;
		descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		descriptor_image_infos[2].sampler = nullptr;
		descriptor_image_infos[2].image_view = this->attachments.albedo.image_view;
		descriptor_image_infos[2].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[0].image_info = &descriptor_image_infos[0];

		write_descriptor_sets[1].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[1].image_info = &descriptor_image_infos[1];

		write_descriptor_sets[2].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[2].image_info = &descriptor_image_infos[2];

		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);

		//m_pCompositionVertexShaderModule = this->resource_manager->load_shader_module(this->physical_device, this->device, "composition.vert.sdr");
		//m_pCompositionFragmentShaderModule = this->resource_manager->load_shader_module(this->physical_device, this->device, "composition.frag.sdr");

		this->create_composition_pipeline();
	}

	{
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[1];
		descriptor_buffer_infos[0].buffer = this->m_ubo;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = (sizeof(path_tracer_parameters) + this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1) & ~(this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1);
		descriptor_buffer_infos[0].stride = (sizeof(path_tracer_parameters) + this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1) & ~(this->physical_device_properties.limits.min_uniform_buffer_offset_alignment - 1);

		::framework::gpu::descriptor_image_info descriptor_image_infos[1];
		descriptor_image_infos[0].sampler = nullptr;
		descriptor_image_infos[0].image_view = this->attachments.position.image_view;
		descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		::framework::gpu::write_descriptor_set write_descriptor_sets[2];
		write_descriptor_sets[0].dst_set = this->transparent_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];

		write_descriptor_sets[1].dst_set = this->transparent_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[1].image_info = &descriptor_image_infos[0];
		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);

		//m_pTransparentVertexShaderModule = this->resource_manager->load_shader_module(this->physical_device, this->device, "transparent.vert.sdr");
		//m_pTransparentFragmentShaderModule = this->resource_manager->load_shader_module(this->physical_device, this->device, "transparent.frag.sdr");

		this->create_transparent_pipeline();
	}

	{
		this->device_transfer_resources_count = 8;
		this->device_transfer_resources_ring_buffer_index = 0;
		this->device_transfer_resources = ::std::make_unique<struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources[]>(this->device_transfer_resources_count);

		::std::vector<::framework::gpu::command_buffer *> device_transfer_command_buffers(this->device_transfer_resources_count);

		::framework::gpu::command_pool_create_info command_pool_create_info;
		command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
		command_pool_create_info.queue_family_index = queue_family_indices.transfer;
		this->device->create_command_pool(&command_pool_create_info, nullptr, &this->transfer_command_pool);

		::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
		command_buffer_allocate_info.command_pool = this->transfer_command_pool;
		command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
		command_buffer_allocate_info.subbuffer_count = 1;
		command_buffer_allocate_info.command_buffer_count = this->device_transfer_resources_count;
		this->device->allocate_command_buffers(&command_buffer_allocate_info, device_transfer_command_buffers.data());

		for (::std::uint32_t i = 0; i < this->device_transfer_resources_count; i++)
		{
			this->device_transfer_resources[i].command_buffer = device_transfer_command_buffers[i];

			::framework::gpu::fence_create_info fence_create_info;
			fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
			this->device->create_fence(&fence_create_info, nullptr, &this->device_transfer_resources[i].fence);
		}
	}

	{
		this->staging_buffer_size = 64 * 1024 * 1024;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->staging_buffer_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.transfer;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->staging_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->staging_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_coherent_bit | ::framework::gpu::memory_property_flags::host_visible_write_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->staging_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->staging_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->staging_buffer;
		bind_buffer_memory_info.memory = this->staging_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		this->indices_buffer_size = 256 * 1024 * 1024;

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = this->indices_buffer_size;
		memory_allocate_info.allocation_alignment = 0;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, static_cast<::std::uint32_t>(-1), ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->indices_device_memory));
	}

	{
		this->vertices_buffer_size = 1536 * 1024 * 1024;

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = this->vertices_buffer_size;
		memory_allocate_info.allocation_alignment = 0;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, static_cast<::std::uint32_t>(-1), ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->vertices_device_memory));
	}

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = queue_family_indices.graphics;
	this->device->create_command_pool(&command_pool_create_info, nullptr, &this->primary_command_pool);

	::std::vector<::framework::gpu::command_buffer *> primary_command_buffer(m_frame_buffer_count);

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->primary_command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 2;
	command_buffer_allocate_info.command_buffer_count = m_frame_buffer_count;
	this->device->allocate_command_buffers(&command_buffer_allocate_info, primary_command_buffer.data());

	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;

	thread_count = ::std::thread::hardware_concurrency();

	this->frame_resources.resize(m_frame_buffer_count);

	for (::std::uint32_t f = 0; f < m_frame_buffer_count; f++)
	{
		auto & frame_resource = this->frame_resources[f];

		assert_framework_gpu_result(this->device->create_fence(&fence_create_info, nullptr, &frame_resource.fence));

		frame_resource.primary_command_buffer = primary_command_buffer[f];

		frame_resource.thread_data.resize(thread_count);

		for (::std::uint32_t i = 0; i < thread_count; i++)
		{
			thread_data & worker_thread = frame_resource.thread_data[i];

			::framework::gpu::command_pool_create_info command_pool_create_info;
			command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
			command_pool_create_info.queue_family_index = queue_family_indices.graphics;
			assert_framework_gpu_result(this->device->create_command_pool(&command_pool_create_info, nullptr, &worker_thread.command_pool));

			::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
			command_buffer_allocate_info.command_pool = worker_thread.command_pool;
			command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::secondary;
			command_buffer_allocate_info.subbuffer_count = 1;
			command_buffer_allocate_info.command_buffer_count = 1;
			assert_framework_gpu_result(this->device->allocate_command_buffers(&command_buffer_allocate_info, &worker_thread.scene_subpass_command_buffer));
			assert_framework_gpu_result(this->device->allocate_command_buffers(&command_buffer_allocate_info, &worker_thread.transparent_subpass_command_buffer));
		}
	}

	::framework::graphics::deferred_shading::shadow_renderer_create_info shadow_renderer_create_info;
	shadow_renderer_create_info.scene_renderer = this;
	shadow_renderer_create_info.physical_device = this->physical_device;
	shadow_renderer_create_info.device = this->device;
	shadow_renderer_create_info.shadow_map_width = 1024u;
	shadow_renderer_create_info.shadow_map_height = 1024u;
	shadow_renderer_create_info.max_shadow_map_count = 16u;
	shadow_renderer_create_info.shadow_map_format = ::framework::gpu::format::d32_sfloat;
	shadow_renderer_create_info.frame_buffer_count = this->m_frame_buffer_count;
	shadow_renderer_create_info.pipeline_cache = this->pipeline_cache;
	//shadow_renderer = ::std::make_unique<::framework::graphics::deferred_shading::shadow_renderer>(shadow_renderer_create_info);
}

::framework::graphics::deferred_shading::scene_renderer::~scene_renderer()
{
	//todo
}

void ::framework::graphics::deferred_shading::scene_renderer::resize(scene_renderer_resize_info const * resize_info)
{
	this->width = resize_info->width;
	this->height = resize_info->height;

	this->destroy_frame_buffer_attachment(this->attachments.position);
	this->destroy_frame_buffer_attachment(this->attachments.normal);
	this->destroy_frame_buffer_attachment(this->attachments.albedo);
	this->destroy_frame_buffer_attachment(this->attachments.depth_stencil);

	this->create_frame_buffer_attachments();

	for (auto frame_buffer : this->frame_buffers)
	{
		this->device->destroy_frame_buffer(frame_buffer, nullptr);
	}

	this->create_frame_buffer(resize_info->color_attachments);

	// Update Composition pass DescriptorSet
	{
		::framework::gpu::descriptor_image_info descriptor_image_infos[3];
		descriptor_image_infos[0].sampler = nullptr;
		descriptor_image_infos[0].image_view = this->attachments.position.image_view;
		descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		descriptor_image_infos[1].sampler = nullptr;
		descriptor_image_infos[1].image_view = this->attachments.normal.image_view;
		descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		descriptor_image_infos[2].sampler = nullptr;
		descriptor_image_infos[2].image_view = this->attachments.albedo.image_view;
		descriptor_image_infos[2].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		::framework::gpu::write_descriptor_set write_descriptor_sets[3];
		write_descriptor_sets[0].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[0].image_info = &descriptor_image_infos[0];

		write_descriptor_sets[1].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[1].image_info = &descriptor_image_infos[1];

		write_descriptor_sets[2].dst_set = this->composition_descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[2].image_info = &descriptor_image_infos[2];

		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}

	// Update Transparent pass DescriptorSet
	{
		::framework::gpu::descriptor_image_info descriptor_image_infos[1];
		descriptor_image_infos[0].sampler = nullptr;
		descriptor_image_infos[0].image_view = this->attachments.position.image_view;
		descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		::framework::gpu::write_descriptor_set write_descriptor_sets[1];
		write_descriptor_sets[0].dst_set = this->transparent_descriptor_set;
		write_descriptor_sets[0].dst_binding = 1;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::input_attachment;
		write_descriptor_sets[0].image_info = &descriptor_image_infos[0];
		this->device->update_descriptor_sets(::std::size(write_descriptor_sets), write_descriptor_sets, 0, nullptr);
	}

	this->device->destroy_pipeline(this->scene_pipeline, nullptr);
	this->create_scene_pipeline();
	this->device->destroy_pipeline(m_pCompositionPipeline, nullptr);
	this->create_composition_pipeline();
	this->device->destroy_pipeline(this->transparent_pipeline, nullptr);
	this->create_transparent_pipeline();
}

void ::framework::graphics::deferred_shading::scene_renderer::update_command_buffer(::std::uint32_t buffer_index)
{
	/*if (!this->transfer_fence_state)
	{
		assert_framework_gpu_result(this->device->wait_for_fences(1, &this->transfer_fence, true, UINT64_MAX));
		assert_framework_gpu_result(this->device->reset_fences(1, &this->transfer_fence));

		this->transfer_fence_state = true;
	}*/

	::framework::gpu::clear_value clear_values[5];
	clear_values[0].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[1].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[2].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[3].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[4].depth_stencil = { m_reverse_z_depth ? 0.0f : 1.0f, 0 };

	::framework::gpu::render_pass_state_info render_pass_state_info;
	render_pass_state_info.frame_buffer = this->frame_buffers[buffer_index];
	render_pass_state_info.render_area.offset.x = 0;
	render_pass_state_info.render_area.offset.y = 0;
	render_pass_state_info.render_area.extent.width = this->width;
	render_pass_state_info.render_area.extent.height = this->height;
	render_pass_state_info.clear_value_count = ::std::size(clear_values);
	render_pass_state_info.clear_values = clear_values;
	this->render_pass_state->set_state(render_pass_state_info);

	::framework::gpu::command_buffer_inheritance_info command_buffer_inheritance_info;
	command_buffer_inheritance_info.render_pass_state = this->render_pass_state;
	command_buffer_inheritance_info.subpass = 0;
	command_buffer_inheritance_info.occlusion_query_enable = false;
	command_buffer_inheritance_info.query_flags = ::framework::gpu::query_control_flags::none;
	command_buffer_inheritance_info.pipeline_statistics = ::framework::gpu::query_pipeline_statistic_flags::none;

	assert_framework_gpu_result(this->device->wait_for_fences(1, &this->frame_resources[buffer_index].fence, true, UINT64_MAX));
	assert_framework_gpu_result(this->device->reset_fences(1, &this->frame_resources[buffer_index].fence));

	//auto & dispatcher = [&]() -> ::framework::coroutine::task<void>
	{
		// scene subpass commandbuffers generation
		for (::std::uint32_t i = 0; i < this->scene_views.size(); i++)
		{
			::std::uint32_t primitive_count;
			::framework::graphics::deferred_shading::scene_primitive ** primitives;
			this->scene->get_view_primitives(this->scene_views[i], single_frame_allocator, primitive_count, &primitives);

			// g-buffer rendering
			if (primitive_count > 0)
			{
				::std::uint32_t const primitives_per_thread = (primitive_count + thread_count - 1) / thread_count;
				::std::uint32_t const active_thread_count = ::std::min(thread_count, (primitive_count + primitives_per_thread - 1) / primitives_per_thread);
				::std::uint32_t const last_active_thread = active_thread_count - 1;

				//::std::uint32_t entity_index(0);

				auto draw_primitives = [&](::framework::coroutine::static_thread_pool & thread_pool, ::std::uint32_t primitive_begin, ::std::uint32_t primitive_end, ::std::uint32_t thread_index, ::framework::gpu::command_buffer_usage_flags command_buffer_usage_flags)->::framework::coroutine::thread_pool_task<void>
					{
						//co_await thread_pool;
						::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
						command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit | command_buffer_usage_flags;
						command_buffer_begin_info.inheritance_info = &command_buffer_inheritance_info;

						auto & worker_thread = this->frame_resources[buffer_index].thread_data[thread_index];
						auto & command_buffer_data = worker_thread.scene_subpass_command_buffer_data;
						auto & command_buffer = worker_thread.scene_subpass_command_buffer;
						if (!command_buffer_data.is_command_buffer_open)
						{
							assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));
							command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline);

							command_buffer_data.is_command_buffer_open = true;
						}

						for (::std::uint32_t primitive_index = primitive_begin; primitive_index < primitive_end; primitive_index++)
						{
							path_tracer_parameters path_tracer_parameters;
							path_tracer_parameters.projectionMatrix = static_cast<::framework::graphics::deferred_shading::scene_view_perspective *>(this->scene_views[i])->get_projection();
							path_tracer_parameters.modelMatrix = primitives[primitive_index]->get_local_to_world_matrix();
							path_tracer_parameters.viewMatrix = static_cast<::framework::graphics::deferred_shading::scene_view_perspective *>(this->scene_views[i])->get_view();
							path_tracer_parameters.camPos = ::glm::vec4();
							path_tracer_parameters.lightPos = light_pos;

							::std::uint32_t const dynamic_offsets[2]{ primitive_index * static_cast<::std::uint32_t>(this->ubo_dynamic_alignment), primitive_index * static_cast<::std::uint32_t>(this->material_ubo_dynamic_alignment) };

							::std::memcpy(static_cast<char *>(this->mapped_data) + dynamic_offsets[0], &path_tracer_parameters, sizeof(path_tracer_parameters));
							::std::memcpy(static_cast<char *>(this->material_mapped_data) + dynamic_offsets[1], &primitives[primitive_index]->get_material()->get_device_data(), sizeof(::framework::graphics::deferred_shading::material_device_data));

							command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
							command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline_layout, 0, 1, &this->scene_descriptor_set, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
							primitives[primitive_index]->draw(command_buffer);
						}

						co_return;
					};

				::std::vector<::framework::coroutine::thread_pool_task<void>> draw_primitives_tasks(active_thread_count);
				::std::uint32_t thread_index = 0;
				::std::uint32_t primitive_begin;
				::std::uint32_t primitive_end = 0;
				do
				{
					primitive_begin = primitive_end;
					primitive_end = ::std::min(primitive_count, primitive_end + primitives_per_thread);

					::framework::gpu::command_buffer_usage_flags command_buffer_usage_flags = ::framework::gpu::command_buffer_usage_flags::none;
					if (active_thread_count > 1)
					{
						if (thread_index > 0)
							command_buffer_usage_flags |= ::framework::gpu::command_buffer_usage_flags::render_pass_resuming_bit;
						if (thread_index < last_active_thread)
							command_buffer_usage_flags |= ::framework::gpu::command_buffer_usage_flags::render_pass_suspending_bit;
					}

					draw_primitives_tasks[thread_index] = draw_primitives(this->thread_pool, primitive_begin, primitive_end, thread_index, command_buffer_usage_flags);
					thread_index++;
				} while (primitive_end < primitive_count);

				::framework::coroutine::sync_wait(::framework::coroutine::when_all_ready(::std::move(draw_primitives_tasks)));
			}

			// shadowmap rendering
			if (primitive_count > 0)
			{
				this->shadow_renderer->render(buffer_index, primitive_count, primitives);
			}

			// shadowmap rendering
			if (primitive_count > 0 && false)
			{
				::std::uint32_t const active_thread_count = ::std::min(thread_count, primitive_count);
				::std::uint32_t const last_active_thread = active_thread_count - 1;

				//::std::uint32_t entity_index(0);

				auto primitive_draw_task = [&](::framework::coroutine::static_thread_pool & thread_pool, ::std::uint32_t primitive_begin, ::std::uint32_t primitive_end, ::std::uint32_t thread_index, ::framework::gpu::command_buffer_usage_flags command_buffer_usage_flags)->::framework::coroutine::thread_pool_task<void>
					{
						::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
						command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit | command_buffer_usage_flags;
						command_buffer_begin_info.inheritance_info = &command_buffer_inheritance_info;

						auto & thread_data = this->frame_resources[buffer_index].thread_data[thread_index];
						auto & command_buffer_data = thread_data.scene_subpass_command_buffer_data;
						auto & command_buffer = thread_data.scene_subpass_command_buffer;
						if (!command_buffer_data.is_command_buffer_open)
						{
							assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));
							command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline);

							command_buffer_data.is_command_buffer_open = true;
						}

						for (::std::uint32_t primitive_index = primitive_begin; primitive_index < primitive_end; primitive_index++)
						{
							path_tracer_parameters path_tracer_parameters;
							path_tracer_parameters.projectionMatrix = static_cast<::framework::graphics::deferred_shading::scene_view_perspective *>(this->scene_views[i])->get_projection();
							path_tracer_parameters.modelMatrix = primitives[primitive_index]->get_local_to_world_matrix();
							path_tracer_parameters.viewMatrix = static_cast<::framework::graphics::deferred_shading::scene_view_perspective *>(this->scene_views[i])->get_view();
							path_tracer_parameters.camPos = ::glm::vec4();
							path_tracer_parameters.lightPos = light_pos;

							::std::uint32_t const dynamic_offset = primitive_index * static_cast<::std::uint32_t>(this->ubo_dynamic_alignment);
							::std::memcpy(static_cast<char *>(this->mapped_data) + dynamic_offset, &path_tracer_parameters, sizeof(path_tracer_parameters));

							command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
							command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline_layout, 0, 1, &this->scene_descriptor_set, 1, &dynamic_offset);
							primitives[primitive_index]->draw(command_buffer);
						}

						co_return;
					};

				::std::vector<::framework::coroutine::thread_pool_task<void>> primitive_draw_tasks(active_thread_count);
				::std::uint32_t const primitives_per_thread = (primitive_count + thread_count - 1) / thread_count;
				::std::uint32_t thread_index = 0;
				::std::uint32_t primitive_begin;
				::std::uint32_t primitive_end = 0;
				do
				{
					primitive_begin = primitive_end;
					primitive_end = ::std::min(primitive_count, primitive_end + primitives_per_thread);

					::framework::gpu::command_buffer_usage_flags command_buffer_usage_flags = ::framework::gpu::command_buffer_usage_flags::none;
					if (active_thread_count > 1)
					{
						if (thread_index > 0)
							command_buffer_usage_flags |= ::framework::gpu::command_buffer_usage_flags::render_pass_resuming_bit;
						if (thread_index < last_active_thread)
							command_buffer_usage_flags |= ::framework::gpu::command_buffer_usage_flags::render_pass_suspending_bit;
					}

					primitive_draw_tasks[thread_index] = primitive_draw_task(this->thread_pool, primitive_begin, primitive_end, thread_index, command_buffer_usage_flags);
					thread_index++;
				} while (primitive_end < primitive_count);

				::framework::coroutine::sync_wait(::framework::coroutine::when_all_ready(::std::move(primitive_draw_tasks)));
			}
			/*auto entities = entity_manager::entities_with_components<CMeshRendererPathTracingBVH, CTransform>();
			component_handle<CMeshRendererPathTracingBVH> mesh_renderer;
			component_handle<CTransform> transform;
			for (auto entity : entities)
			{
				entity.unpack<CMeshRendererPathTracingBVH, CTransform>(mesh_renderer, transform);
				thread_pool[buffer_index].workers[threadIndex]->addJob([&, entity_index, mesh_renderer, transform, buffer_index = buffer_index, threadIndex = threadIndex]
				{
					::glm::vec3 const & camPos = m_camera.component<CTransform>()->getTranslation();

					ubo_data uboData;
					uboData.projectionMatrix = m_camera.component<camera>()->getProjectionMatrix();
					uboData.modelMatrix = transform->getWorldMatrix();
					uboData.viewMatrix = m_camera.component<camera>()->getViewMatrix();
					uboData.camPos = ::glm::vec4(camPos.x, camPos.y, camPos.z, 1.0f);
					uboData.lightPos = light_pos;

					const ::std::uint32_t dynamic_offset = entity_index * static_cast<::std::uint32_t>(this->ubo_dynamic_alignment);
					::std::memcpy(static_cast<char *>(this->mapped_data) + dynamic_offset, &uboData, sizeof(uboData));

					auto & thread_data = thread_data[buffer_index][threadIndex];

					::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
					for (::std::uint32_t i = 0; i < thread_data.scene_subpass_command_buffer_data.size(); i++)
						drawCallCountMap.insert(::std::make_pair(thread_data.scene_subpass_command_buffer_data[i].drawCallCount, i));

					auto & commandBufferIndex = drawCallCountMap.begin()->second;
					auto & command_buffer_data = thread_data.scene_subpass_command_buffer_data[commandBufferIndex];
					auto & ::framework::gpu::command_buffer = thread_data.scene_subpass_command_buffer[commandBufferIndex];
					if (!command_buffer_data.is_command_buffer_open)
					{
						CHECK_RESULT(::framework::gpu::command_buffer->begin_command_buffer(&command_buffer_begin_info));
						::framework::gpu::command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline);

						command_buffer_data.is_command_buffer_open = true;
					}

					::framework::gpu::command_buffer->bind_descriptor_pool(this->descriptor_pool);
					::framework::gpu::command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->scene_pipeline_layout, 0, 1, &this->scene_descriptor_set, 1, &dynamic_offset);
					float color[4] = { 0.2f, 0.91f, 0.12f, 1.0f };
					// CommandBuffer->push_constants(this->scene_pipeline_layout, ::framework::gpu::shader_stage_flags::fragment_bit, 0, 0, sizeof(float) * 4, color);

					mesh_renderer->draw(::framework::gpu::command_buffer);

					command_buffer_data.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
				});
				threadIndex = (threadIndex + 1) % thread_count;
				entity_index++;
			}*/

			::framework::gpu::command_buffer_inheritance_info command_buffer_inheritance_info_2;
			command_buffer_inheritance_info_2.render_pass_state = this->render_pass_state;
			command_buffer_inheritance_info_2.subpass = 2;
			command_buffer_inheritance_info_2.occlusion_query_enable = false;
			command_buffer_inheritance_info_2.query_flags = ::framework::gpu::query_control_flags::none;
			command_buffer_inheritance_info_2.pipeline_statistics = ::framework::gpu::query_pipeline_statistic_flags::none;

			::framework::gpu::command_buffer_begin_info commandBufferBeginInfo1;
			commandBufferBeginInfo1.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit | ::framework::gpu::command_buffer_usage_flags::render_pass_continue_bit;
			commandBufferBeginInfo1.inheritance_info = &command_buffer_inheritance_info_2;

			// transparent subpass commandbuffers generation
			//{
			//	auto entities = entity_manager::entities_with_components<CMeshRendererPathTracingBVH, CTransform>();
			//	component_handle<CMeshRendererPathTracingBVH> mesh_renderer;
			//	component_handle<CTransform> transform;
			//	for (auto entity : entities)
			//	{
			//		entity.unpack<CMeshRendererPathTracingBVH, CTransform>(mesh_renderer, transform);
			//		thread_pool[buffer_index].workers[threadIndex]->addJob([&, entity_index, mesh_renderer, transform, buffer_index = buffer_index, threadIndex = threadIndex]
			//		{
			//			::glm::vec3 const & camPos = m_camera.component<CTransform>()->getTranslation();

			//			ubo_data uboData;
			//			uboData.projectionMatrix = m_camera.component<camera>()->getProjectionMatrix();
			//			uboData.modelMatrix = transform->getWorldMatrix();
			//			uboData.modelMatrix = ::glm::translate(uboData.modelMatrix, ::glm::vec3(0.0f, 25.0f, 0.0f));
			//			uboData.viewMatrix = m_camera.component<camera>()->getViewMatrix();
			//			uboData.camPos = ::glm::vec4(camPos.x, camPos.y, camPos.z, 1.0f);
			//			uboData.lightPos = light_pos;

			//			const ::std::uint32_t dynamic_offset = entity_index * static_cast<::std::uint32_t>(this->ubo_dynamic_alignment);
			//			::std::memcpy(static_cast<char *>(this->mapped_data) + dynamic_offset, &uboData, sizeof(uboData));

			//			auto & thread_data = thread_data[buffer_index][threadIndex];

			//			::std::map<::std::uint32_t, ::std::uint32_t> drawCallCountMap;
			//			for (::std::uint32_t i = 0; i < thread_data.transparent_subpass_command_buffer_data.size(); i++)
			//				drawCallCountMap.insert(::std::make_pair(thread_data.transparent_subpass_command_buffer_data[i].drawCallCount, i));

			//			auto & commandBufferIndex = drawCallCountMap.begin()->second;
			//			auto & command_buffer_data = thread_data.transparent_subpass_command_buffer_data[commandBufferIndex];
			//			auto & ::framework::gpu::command_buffer = thread_data.transparent_subpass_command_buffer[commandBufferIndex];
			//			if (!command_buffer_data.is_command_buffer_open)
			//			{
			//				CHECK_RESULT(::framework::gpu::command_buffer->begin_command_buffer(&commandBufferBeginInfo1));
			//				::framework::gpu::command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->transparent_pipeline);

			//				command_buffer_data.is_command_buffer_open = true;
			//			}

			//			::framework::gpu::command_buffer->bind_descriptor_pool(this->descriptor_pool);
			//			::framework::gpu::command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->transparent_pipeline_layout, 0, 1, &this->transparent_descriptor_set, 1, &dynamic_offset);
			//			float color[4] = { 0.2f, 0.91f, 0.12f, 1.0f };
			//			// CommandBuffer->push_constants(this->scene_pipeline_layout, ::framework::gpu::shader_stage_flags::fragment_bit, 0, 0, sizeof(float) * 4, color);

			//			mesh_renderer->draw(::framework::gpu::command_buffer);

			//			command_buffer_data.drawCallCount += mesh_renderer->GetMesh()->GetSubmeshCount();
			//		});
			//		threadIndex = (threadIndex + 1) % thread_count;
			//		entity_index++;
			//	}
			//}
		}
	};

	concurrency::job::counter finalize_command_buffers_counter;
	::std::vector<::framework::gpu::command_buffer *> scene_subpass_command_buffer;
	::std::vector<::framework::gpu::command_buffer *> transparent_subpass_command_buffer;
	for (::std::uint32_t t = 0; t < thread_count; t++)
	{
		auto const & finaliseCommandBuffers = [](::framework::gpu::command_buffer * command_buffers, thread_data::command_buffer_data & command_buffer_data) -> void
			{
				if (command_buffer_data.is_command_buffer_open)
				{
					assert_framework_gpu_result(command_buffers->end_command_buffer());
				}
			};

		auto const & worker_function = [&finaliseCommandBuffers, &thread_data = this->frame_resources[buffer_index].thread_data[t], &scene_subpass_command_buffer, &transparent_subpass_command_buffer](::std::uintptr_t) -> void
			{
				finaliseCommandBuffers(thread_data.scene_subpass_command_buffer, thread_data.scene_subpass_command_buffer_data);
				finaliseCommandBuffers(thread_data.transparent_subpass_command_buffer, thread_data.transparent_subpass_command_buffer_data);
			};

		concurrency::job::declaration job_declaration;
		job_declaration.entry_point = worker_function;
		job_declaration.param = 0;
		job_declaration.priority = concurrency::job::priority::normal;
		job_declaration.counter = &finalize_command_buffers_counter;

		//thread_pool.kick_job(job_declaration);
		worker_function(0);
	}

	finalize_command_buffers_counter.wait();
	//this->frame_resources[buffer_index].thread_pool.wait();

	for (::std::uint32_t t = 0; t < thread_count; t++)
	{
		if (this->frame_resources[buffer_index].thread_data[t].scene_subpass_command_buffer_data.is_command_buffer_open)
		{
			scene_subpass_command_buffer.push_back(this->frame_resources[buffer_index].thread_data[t].scene_subpass_command_buffer);
			this->frame_resources[buffer_index].thread_data[t].scene_subpass_command_buffer_data.is_command_buffer_open = false;
		}

		if (this->frame_resources[buffer_index].thread_data[t].transparent_subpass_command_buffer_data.is_command_buffer_open)
		{
			transparent_subpass_command_buffer.push_back(this->frame_resources[buffer_index].thread_data[t].transparent_subpass_command_buffer);
			this->frame_resources[buffer_index].thread_data[t].transparent_subpass_command_buffer_data.is_command_buffer_open = false;
		}
	}

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::render_pass_begin_info render_pass_begin_info;
	render_pass_begin_info.render_pass_state = this->render_pass_state;

	assert_framework_gpu_result(this->frame_resources[buffer_index].primary_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	this->frame_resources[buffer_index].primary_command_buffer->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::secondary_command_buffers);
	if (scene_subpass_command_buffer.size())
		this->frame_resources[buffer_index].primary_command_buffer->execute_commands(static_cast<::std::uint32_t>(scene_subpass_command_buffer.size()), scene_subpass_command_buffer.data());
	this->frame_resources[buffer_index].primary_command_buffer->next_subpass(::framework::gpu::subpass_contents::inline_);
	this->frame_resources[buffer_index].primary_command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, m_pCompositionPipeline);
	this->frame_resources[buffer_index].primary_command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
	this->frame_resources[buffer_index].primary_command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, m_pCompositionPipelineLayout, 0, 1, &this->composition_descriptor_set, 0, nullptr);
	this->frame_resources[buffer_index].primary_command_buffer->draw(3, 1, 0, 0);
	this->frame_resources[buffer_index].primary_command_buffer->next_subpass(::framework::gpu::subpass_contents::secondary_command_buffers);
	if (transparent_subpass_command_buffer.size())
		this->frame_resources[buffer_index].primary_command_buffer->execute_commands(static_cast<::std::uint32_t>(transparent_subpass_command_buffer.size()), transparent_subpass_command_buffer.data());
	this->frame_resources[buffer_index].primary_command_buffer->end_render_pass();
	assert_framework_gpu_result(this->frame_resources[buffer_index].primary_command_buffer->end_command_buffer());
}

void ::framework::graphics::deferred_shading::scene_renderer::set_scene(::framework::graphics::scene * scene)
{
	this->scene = static_cast<class ::framework::graphics::deferred_shading::scene *>(scene);
}

void ::framework::graphics::deferred_shading::scene_renderer::set_scene_views(::std::uint32_t scene_view_count, ::framework::graphics::scene_view ** scene_views)
{
	this->scene_views.resize(scene_view_count);

	for (::std::uint32_t i = 0; i < scene_view_count; i++)
	{
		this->scene_views[i] = scene_views[i];
	}
}

::framework::graphics::scene * ::framework::graphics::deferred_shading::scene_renderer::create_scene(::framework::graphics::scene_create_info * create_info)
{
	return nullptr;
	//return new ::framework::graphics::deferred_shading::scene(this, create_info);
}

::framework::graphics::triangle_mesh_geometry *(::framework::graphics::deferred_shading::scene_renderer::create_triangle_mesh_geometry)(::framework::graphics::triangle_mesh_geometry_create_info const & create_info)
{
	return nullptr;
	//::framework::graphics::deferred_shading::triangle_mesh_geometry * triangle_mesh_geometry = new ::framework::graphics::deferred_shading::triangle_mesh_geometry();
	//auto triangle_mesh_geometry_initialize = triangle_mesh_geometry->initialize(this, create_info);
	//co_await triangle_mesh_geometry_initialize;

	//co_return triangle_mesh_geometry;
}

::framework::graphics::image *(::framework::graphics::deferred_shading::scene_renderer::create_image)(::framework::graphics::image_create_info const & create_info)
{
	::framework::graphics::deferred_shading::image * image = new ::framework::graphics::deferred_shading::image(this->texture_counter++, this, create_info);
	return image;
}

::framework::graphics::material * ::framework::graphics::deferred_shading::scene_renderer::create_material(::framework::graphics::material_create_info const & create_info)
{
	return new ::framework::graphics::deferred_shading::material(this, create_info, this->material_counter++);
}

::framework::graphics::medium * ::framework::graphics::deferred_shading::scene_renderer::create_medium()
{
	return nullptr;
}

::framework::graphics::scene_view_perspective * ::framework::graphics::deferred_shading::scene_renderer::create_scene_view_perspective(::framework::graphics::scene_view_perspective_create_info * create_info)
{
	return new ::framework::graphics::deferred_shading::scene_view_perspective(this, create_info);
}

inline ::framework::gpu::semaphore * (::framework::graphics::deferred_shading::scene_renderer::get_frame_semaphore)() const noexcept
{
	return nullptr;
}

inline ::std::uint64_t(::framework::graphics::deferred_shading::scene_renderer::get_last_frame_index)() const noexcept
{
	return 0;
}

template<typename AWAITER>
struct my_test_awaiter
{
	constexpr bool await_ready() noexcept { return this->awaiter.await_ready(); }
	constexpr bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
	{
		bool result = this->awaiter.await_suspend(coroutine_handle);
		if (result)
		{
			this->need_lock = true;
			this->mutex.unlock();
		}

		return result;
	}
	constexpr void await_resume() noexcept { this->awaiter.await_resume(); }

	AWAITER awaiter;
	::framework::coroutine::mutex & mutex;
	::framework::coroutine::static_thread_pool & thread_pool;
	bool & need_lock;
};

::framework::coroutine::lazy_task<::std::uint32_t>(::framework::graphics::deferred_shading::scene_renderer::acquire_device_transfer_resources)()
{
	static ::std::atomic<int> counter = 0;
	//::std::cout << "locking " << " tid: " << ::std::this_thread::get_id() << ::std::endl;
	co_await this->device_transfer_resources_mutex.lock();
	//::std::cout << "locked " << " tid: " << ::std::this_thread::get_id() << ::std::endl;
	//::std::unique_lock<::std::mutex> unique_lock(this->device_transfer_resources_mutex);

	this->device_transfer_resources_ring_buffer_index = (this->device_transfer_resources_ring_buffer_index + 1) % this->device_transfer_resources_count;
	::std::uint32_t const device_transfer_resources_ring_buffer_index = this->device_transfer_resources_ring_buffer_index;
	//::std::cout << "acquire_device_transfer_resources_index " << device_transfer_resources_ring_buffer_index << " tid: " << ::std::this_thread::get_id() << ::std::endl;

	struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources & device_transfer_resources = this->device_transfer_resources[device_transfer_resources_ring_buffer_index];

	//int old_state = counter.fetch_add(old_state);
	//assert(counter.fetch_add(1) == 0);
	//this->device_transfer_resources_mutex.unlock(this->thread_pool);
	//::std::cout << "unlock " << device_transfer_resources_ring_buffer_index << " tid: " << ::std::this_thread::get_id() << ::std::endl;
	//device_transfer_resources.condition_variable.wait(unique_lock, [&device_transfer_resources]() { return !device_transfer_resources.owned; });
	bool need_lock = false;
	my_test_awaiter<decltype(device_transfer_resources.async_auto_reset_event.operator co_await())> test{ device_transfer_resources.async_auto_reset_event.operator co_await(), this->device_transfer_resources_mutex, this->thread_pool, need_lock };
	co_await test;
	//::std::cout << "wait " << device_transfer_resources_ring_buffer_index << " tid: " << ::std::this_thread::get_id() << ::std::endl;
	if (need_lock)
		co_await this->device_transfer_resources_mutex.lock();

	this->device->wait_for_fences(1, &device_transfer_resources.fence, true, UINT64_MAX);
	this->device->reset_fences(1, &device_transfer_resources.fence);

	//device_transfer_resources.owned = true;
	this->device_transfer_resources_mutex.unlock();

	co_return device_transfer_resources_ring_buffer_index;
}

::framework::coroutine::lazy_task<void>(::framework::graphics::deferred_shading::scene_renderer::release_device_transfer_resources)(::std::uint32_t index)
{
	co_await this->device_transfer_resources_mutex.lock();
	//::std::lock_guard<::std::mutex> lock_guard(this->device_transfer_resources_mutex);

	struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources & device_transfer_resources = this->device_transfer_resources[index];

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = device_transfer_resources.command_buffer;
	command_buffer_info.device_mask = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 0;
	submit_info.wait_semaphore_infos = nullptr;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = 0;
	submit_info.signal_semaphore_infos = nullptr;
	this->queues.transfer->submit(1, &submit_info, device_transfer_resources.fence);

	this->device_transfer_resources_mutex.unlock();

	//device_transfer_resources.owned = false;
	//device_transfer_resources.condition_variable.notify_one();
	device_transfer_resources.async_auto_reset_event.set();
}

void ::framework::graphics::deferred_shading::scene_renderer::create_frame_buffer(::framework::gpu::image_view ** color_attachments)
{
	::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
	frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
	frame_buffer_create_info.render_pass = this->render_pass;
	frame_buffer_create_info.width = this->width;
	frame_buffer_create_info.height = this->height;
	frame_buffer_create_info.layers = 1;

	::framework::gpu::image_view * attachments[5];
	attachments[0] = this->attachments.position.image_view;
	attachments[1] = this->attachments.normal.image_view;
	attachments[2] = this->attachments.albedo.image_view;
	attachments[4] = this->attachments.depth_stencil.image_view;

	this->frame_buffers.resize(m_frame_buffer_count);
	for (::std::uint32_t i = 0; i < m_frame_buffer_count; ++i)
	{
		attachments[3] = color_attachments[i];

		frame_buffer_create_info.attachment_count = ::std::size(attachments);
		//frame_buffer_create_info.attachments = attachments;
		assert_framework_gpu_result(this->device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffers[i]));
	}
}

void ::framework::graphics::deferred_shading::scene_renderer::create_frame_buffer_attachments()
{
	::framework::gpu::clear_value const colorAttachmentClearValue = { .color = { { 0.0f, 0.0f, 0.0f, 0.0f } } };
	::framework::gpu::clear_value const depthStencilAttachmentClearValue = { .depth_stencil = { m_reverse_z_depth ? 0.0f : 1.0f, 0 } };

	this->create_frame_buffer_attachment(::framework::gpu::format::r16g16b16a16_sfloat, ::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::input_attachment_bit | ::framework::gpu::image_usage_flags::sampled_bit, ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit, ::framework::gpu::image_aspect_flags::color_bit, colorAttachmentClearValue, this->attachments.position);
	this->create_frame_buffer_attachment(::framework::gpu::format::r16g16b16a16_sfloat, ::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::input_attachment_bit | ::framework::gpu::image_usage_flags::sampled_bit, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit, ::framework::gpu::image_aspect_flags::color_bit, colorAttachmentClearValue, this->attachments.normal);
	this->create_frame_buffer_attachment(::framework::gpu::format::r8g8b8a8_unorm, ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::color_attachment_bit | ::framework::gpu::image_usage_flags::input_attachment_bit | ::framework::gpu::image_usage_flags::sampled_bit, ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit, ::framework::gpu::image_aspect_flags::color_bit, colorAttachmentClearValue, this->attachments.albedo);
	this->create_frame_buffer_attachment(::framework::gpu::format::d32_sfloat, ::framework::gpu::image_usage_flags::depth_stencil_attachment_bit | ::framework::gpu::image_usage_flags::sampled_bit, ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit, ::framework::gpu::image_aspect_flags::depth_bit, depthStencilAttachmentClearValue, this->attachments.depth_stencil);
}

void ::framework::graphics::deferred_shading::scene_renderer::create_render_pass()
{
	::framework::gpu::attachment_description attachment_descriptions[5];
	// Position
	attachment_descriptions[0].format = this->attachments.position.format;
	attachment_descriptions[0].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[0].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[0].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[0].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[0].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[0].initial_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[0].final_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
	// Normals
	attachment_descriptions[1].format = this->attachments.normal.format;
	attachment_descriptions[1].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[1].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[1].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[1].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[1].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[1].initial_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[1].final_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
	// Albedo
	attachment_descriptions[2].format = this->attachments.albedo.format;
	attachment_descriptions[2].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[2].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[2].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[2].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[2].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[2].initial_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[2].final_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
	// Color attachment
	attachment_descriptions[3].format = color_attachments_format;
	attachment_descriptions[3].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[3].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[3].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[3].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[3].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[3].initial_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[3].final_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;
	// Depth attachment
	attachment_descriptions[4].format = ::framework::gpu::format::d32_sfloat;
	attachment_descriptions[4].samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	attachment_descriptions[4].load_op = ::framework::gpu::attachment_load_op::clear;
	attachment_descriptions[4].store_op = ::framework::gpu::attachment_store_op::store;
	attachment_descriptions[4].stencil_load_op = ::framework::gpu::attachment_load_op::dont_care;
	attachment_descriptions[4].stencil_store_op = ::framework::gpu::attachment_store_op::dont_care;
	attachment_descriptions[4].initial_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	attachment_descriptions[4].final_layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::attachment_reference color_references_0[3];
	color_references_0[0].attachment = 0;
	color_references_0[0].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	color_references_0[1].attachment = 1;
	color_references_0[1].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	color_references_0[2].attachment = 2;
	color_references_0[2].layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference input_references_1[3];
	input_references_1[0].attachment = 0;
	input_references_1[0].layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

	input_references_1[1].attachment = 1;
	input_references_1[1].layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

	input_references_1[2].attachment = 2;
	input_references_1[2].layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

	::framework::gpu::attachment_reference color_reference_1;
	color_reference_1.attachment = 3;
	color_reference_1.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference color_attachment_reference_2;
	color_attachment_reference_2.attachment = 3;
	color_attachment_reference_2.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference input_attachment_reference_2;
	input_attachment_reference_2.attachment = 0;
	input_attachment_reference_2.layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

	::framework::gpu::attachment_reference depth_attachment_reference;
	depth_attachment_reference.attachment = 4;
	depth_attachment_reference.layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::subpass_dependency subpass_dependencies[5];
	subpass_dependencies[0].src_subpass = ::framework::gpu::subpass_external;
	subpass_dependencies[0].dst_subpass = 0;
	subpass_dependencies[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
	subpass_dependencies[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit | ::framework::gpu::pipeline_stage_flags::early_fragment_tests_bit;
	subpass_dependencies[0].src_access_mask = ::framework::gpu::access_flags::input_attachment_read_bit | ::framework::gpu::access_flags::memory_read_bit;
	subpass_dependencies[0].dst_access_mask = ::framework::gpu::access_flags::color_attachment_write_bit | ::framework::gpu::access_flags::depth_stencil_attachment_read_bit | ::framework::gpu::access_flags::depth_stencil_attachment_write_bit;
	subpass_dependencies[0].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

	subpass_dependencies[1].src_subpass = ::framework::gpu::subpass_external;
	subpass_dependencies[1].dst_subpass = 1;
	subpass_dependencies[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	subpass_dependencies[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	subpass_dependencies[1].src_access_mask = ::framework::gpu::access_flags::memory_read_bit;
	subpass_dependencies[1].dst_access_mask = ::framework::gpu::access_flags::color_attachment_write_bit;
	subpass_dependencies[1].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

	subpass_dependencies[2].src_subpass = 0;
	subpass_dependencies[2].dst_subpass = 1;
	subpass_dependencies[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	subpass_dependencies[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
	subpass_dependencies[2].src_access_mask = ::framework::gpu::access_flags::color_attachment_write_bit;
	subpass_dependencies[2].dst_access_mask = ::framework::gpu::access_flags::input_attachment_read_bit;
	subpass_dependencies[2].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

	subpass_dependencies[3].src_subpass = 1;
	subpass_dependencies[3].dst_subpass = 2;
	subpass_dependencies[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	subpass_dependencies[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	subpass_dependencies[3].src_access_mask = ::framework::gpu::access_flags::color_attachment_write_bit;
	subpass_dependencies[3].dst_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
	subpass_dependencies[3].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

	subpass_dependencies[4].src_subpass = 2;
	subpass_dependencies[4].dst_subpass = ::framework::gpu::subpass_external;
	subpass_dependencies[4].src_stage_mask = ::framework::gpu::pipeline_stage_flags::color_attachment_output_bit;
	subpass_dependencies[4].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	subpass_dependencies[4].src_access_mask = ::framework::gpu::access_flags::color_attachment_read_bit | ::framework::gpu::access_flags::color_attachment_write_bit;
	subpass_dependencies[4].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit;
	subpass_dependencies[4].dependency_flags = ::framework::gpu::dependency_flags::by_region_bit;

	::framework::gpu::subpass_description subpass_descriptions[3];
	subpass_descriptions[0].flags = ::framework::gpu::subpass_description_flags::none;
	subpass_descriptions[0].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
	subpass_descriptions[0].input_attachment_count = 0;
	subpass_descriptions[0].input_attachments = nullptr;
	subpass_descriptions[0].color_attachment_count = ::std::size(color_references_0);
	subpass_descriptions[0].color_attachments = color_references_0;
	subpass_descriptions[0].resolve_attachments = nullptr;
	subpass_descriptions[0].depth_stencil_attachment = &depth_attachment_reference;
	subpass_descriptions[0].preserve_attachment_count = 0;
	subpass_descriptions[0].preserve_attachments = nullptr;

	subpass_descriptions[1].flags = ::framework::gpu::subpass_description_flags::none;
	subpass_descriptions[1].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
	subpass_descriptions[1].input_attachment_count = ::std::size(input_references_1);
	subpass_descriptions[1].input_attachments = input_references_1;
	subpass_descriptions[1].color_attachment_count = 1;
	subpass_descriptions[1].color_attachments = &color_reference_1;
	subpass_descriptions[1].resolve_attachments = nullptr;
	subpass_descriptions[1].depth_stencil_attachment = &depth_attachment_reference;
	subpass_descriptions[1].preserve_attachment_count = 0;
	subpass_descriptions[1].preserve_attachments = nullptr;

	subpass_descriptions[2].flags = ::framework::gpu::subpass_description_flags::none;
	subpass_descriptions[2].pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
	subpass_descriptions[2].input_attachment_count = 1;
	subpass_descriptions[2].input_attachments = &input_attachment_reference_2;
	subpass_descriptions[2].color_attachment_count = 1;
	subpass_descriptions[2].color_attachments = &color_attachment_reference_2;
	subpass_descriptions[2].resolve_attachments = nullptr;
	subpass_descriptions[2].depth_stencil_attachment = &depth_attachment_reference;
	subpass_descriptions[2].preserve_attachment_count = 0;
	subpass_descriptions[2].preserve_attachments = nullptr;

	::framework::gpu::render_pass_create_info render_pass_create_info;
	render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
	render_pass_create_info.attachment_count = ::std::size(attachment_descriptions);
	render_pass_create_info.attachments = attachment_descriptions;
	render_pass_create_info.subpass_count = ::std::size(subpass_descriptions);
	render_pass_create_info.subpasses = subpass_descriptions;
	render_pass_create_info.dependency_count = ::std::size(subpass_dependencies);
	render_pass_create_info.dependencies = subpass_dependencies;
	assert_framework_gpu_result(this->device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass));

	::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
	render_pass_state_create_info.render_pass = this->render_pass;
	render_pass_state_create_info.max_clear_value_count = ::std::size(attachment_descriptions);
	assert_framework_gpu_result(this->device->create_render_pass_state(&render_pass_state_create_info, nullptr, &this->render_pass_state));
}

void ::framework::graphics::deferred_shading::scene_renderer::create_scene_pipeline()
{
	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
	pipeline_shader_stage_create_infos[0].module = this->scene_vertex_shader_module;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	pipeline_shader_stage_create_infos[1].module = this->scene_fragment_shader_module;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	::framework::gpu::viewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(this->width);
	viewport.height = static_cast<float>(this->height);
	viewport.min_depth = 0.0f;
	viewport.max_depth = 1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = this->width;
	scissor.extent.height = this->height;

	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_states[3];
	pipeline_color_blend_attachment_states[0].blend_enable = false;
	pipeline_color_blend_attachment_states[0].src_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_states[0].dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_states[0].color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_states[0].src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_states[0].dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_states[0].alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_states[0].color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

	pipeline_color_blend_attachment_states[2] = pipeline_color_blend_attachment_states[1] = pipeline_color_blend_attachment_states[0];

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = ::std::size(pipeline_color_blend_attachment_states);
	pipeline_color_blend_state_create_info.attachments = pipeline_color_blend_attachment_states;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
	pipeline_depth_stencil_state_create_info.depth_test_enable = true;
	pipeline_depth_stencil_state_create_info.depth_write_enable = true;
	pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::less_or_equal;
	pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
	pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
	pipeline_depth_stencil_state_create_info.back.fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.pass_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.depth_fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.back.compare_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.write_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.reference = 0;
	pipeline_depth_stencil_state_create_info.front = pipeline_depth_stencil_state_create_info.back;
	pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
	pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

	::framework::gpu::pipeline_multisample_state_create_info pipeline_multisample_state_create_info;
	pipeline_multisample_state_create_info.flags = ::framework::gpu::pipeline_multisample_state_create_flags::none;
	pipeline_multisample_state_create_info.rasterization_samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	pipeline_multisample_state_create_info.sample_shading_enable = false;
	pipeline_multisample_state_create_info.min_sample_shading = 0.0f;
	pipeline_multisample_state_create_info.sample_mask = nullptr;
	pipeline_multisample_state_create_info.alpha_to_coverage_enable = false;
	pipeline_multisample_state_create_info.alpha_to_one_enable = false;

	::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[5];
	vertex_input_binding_descriptions[0].binding = 0;
	vertex_input_binding_descriptions[0].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[1].binding = 1;
	vertex_input_binding_descriptions[1].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[2].binding = 2;
	vertex_input_binding_descriptions[2].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[2].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[3].binding = 3;
	vertex_input_binding_descriptions[3].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[3].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[4].binding = 4;
	vertex_input_binding_descriptions[4].stride = sizeof(float) * 2;
	vertex_input_binding_descriptions[4].input_rate = ::framework::gpu::vertex_input_rate::vertex;

	::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[5];
	// Location 0 : Position
	vertex_input_attribute_descriptions[0].location = 0;
	vertex_input_attribute_descriptions[0].binding = 0;
	vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[0].offset = 0;
	// Location 1 : Vertex normal
	vertex_input_attribute_descriptions[1].location = 1;
	vertex_input_attribute_descriptions[1].binding = 1;
	vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[1].offset = 0;
	// Location 2 : Vertex binormal
	vertex_input_attribute_descriptions[2].location = 2;
	vertex_input_attribute_descriptions[2].binding = 2;
	vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[2].offset = 0;
	// Location 3 : Vertex tangent
	vertex_input_attribute_descriptions[3].location = 3;
	vertex_input_attribute_descriptions[3].binding = 3;
	vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[3].offset = 0;
	// Location 4 : Texture coordinates
	vertex_input_attribute_descriptions[4].location = 4;
	vertex_input_attribute_descriptions[4].binding = 4;
	vertex_input_attribute_descriptions[4].format = ::framework::gpu::format::r32g32_sfloat;
	vertex_input_attribute_descriptions[4].offset = 0;

	::framework::gpu::pipeline_vertex_input_state_create_info pipeline_vertex_input_state_create_info;
	pipeline_vertex_input_state_create_info.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
	pipeline_vertex_input_state_create_info.vertex_binding_descriptions = vertex_input_binding_descriptions;
	pipeline_vertex_input_state_create_info.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
	pipeline_vertex_input_state_create_info.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = static_cast<::std::uint32_t>(::std::size(pipeline_shader_stage_create_infos));
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &pipeline_vertex_input_state_create_info;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = &pipeline_multisample_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = nullptr;
	graphics_pipeline_create_info.layout = this->scene_pipeline_layout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 0;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->scene_pipeline));
}

void ::framework::graphics::deferred_shading::scene_renderer::create_composition_pipeline()
{
	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].module = m_pCompositionVertexShaderModule;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

	pipeline_shader_stage_create_infos[1].module = m_pCompositionFragmentShaderModule;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;

	::framework::gpu::viewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(this->width);
	viewport.height = static_cast<float>(this->height);
	viewport.min_depth = 0.0f;
	viewport.max_depth = 1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = this->width;
	scissor.extent.height = this->height;

	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
	pipeline_color_blend_attachment_state.blend_enable = false;
	pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
	pipeline_depth_stencil_state_create_info.depth_test_enable = false;
	pipeline_depth_stencil_state_create_info.depth_write_enable = false;
	pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::never;
	pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
	pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
	pipeline_depth_stencil_state_create_info.front = {};
	pipeline_depth_stencil_state_create_info.back = {};
	pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
	pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

	::framework::gpu::pipeline_vertex_input_state_create_info pipeline_vertex_input_state_create_info;
	pipeline_vertex_input_state_create_info.vertex_binding_description_count = 0;
	pipeline_vertex_input_state_create_info.vertex_binding_descriptions = nullptr;
	pipeline_vertex_input_state_create_info.vertex_attribute_description_count = 0;
	pipeline_vertex_input_state_create_info.vertex_attribute_descriptions = nullptr;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &pipeline_vertex_input_state_create_info;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = nullptr;
	graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = nullptr;
	graphics_pipeline_create_info.layout = m_pCompositionPipelineLayout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 1;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &m_pCompositionPipeline));
}

void ::framework::graphics::deferred_shading::scene_renderer::create_transparent_pipeline()
{
	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].module = m_pTransparentVertexShaderModule;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;

	pipeline_shader_stage_create_infos[1].module = m_pTransparentFragmentShaderModule;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;

	::framework::gpu::viewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(this->width);
	viewport.height = static_cast<float>(this->height);
	viewport.min_depth = 0.0f;
	viewport.max_depth = 1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = this->width;
	scissor.extent.height = this->height;

	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::counter_clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_states[1];
	pipeline_color_blend_attachment_states[0].blend_enable = true;
	pipeline_color_blend_attachment_states[0].src_color_blend_factor = ::framework::gpu::blend_factor::src_alpha;
	pipeline_color_blend_attachment_states[0].dst_color_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
	pipeline_color_blend_attachment_states[0].color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_states[0].src_alpha_blend_factor = ::framework::gpu::blend_factor::one;
	pipeline_color_blend_attachment_states[0].dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_states[0].alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_states[0].color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = ::std::size(pipeline_color_blend_attachment_states);
	pipeline_color_blend_state_create_info.attachments = pipeline_color_blend_attachment_states;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
	pipeline_depth_stencil_state_create_info.depth_test_enable = true;
	pipeline_depth_stencil_state_create_info.depth_write_enable = false;
	pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::less_or_equal;
	pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
	pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
	pipeline_depth_stencil_state_create_info.back.fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.pass_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.front = pipeline_depth_stencil_state_create_info.back;
	pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
	pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

	::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[5];
	vertex_input_binding_descriptions[0].binding = 0;
	vertex_input_binding_descriptions[0].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[1].binding = 1;
	vertex_input_binding_descriptions[1].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[1].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[2].binding = 2;
	vertex_input_binding_descriptions[2].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[2].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[3].binding = 3;
	vertex_input_binding_descriptions[3].stride = sizeof(float) * 3;
	vertex_input_binding_descriptions[3].input_rate = ::framework::gpu::vertex_input_rate::vertex;
	vertex_input_binding_descriptions[4].binding = 4;
	vertex_input_binding_descriptions[4].stride = sizeof(float) * 2;
	vertex_input_binding_descriptions[4].input_rate = ::framework::gpu::vertex_input_rate::vertex;

	::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[5];
	// Location 0 : Position
	vertex_input_attribute_descriptions[0].location = 0;
	vertex_input_attribute_descriptions[0].binding = 0;
	vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[0].offset = 0;
	// Location 1 : Vertex normal
	vertex_input_attribute_descriptions[1].location = 1;
	vertex_input_attribute_descriptions[1].binding = 1;
	vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[1].offset = 0;
	// Location 2 : Vertex binormal
	vertex_input_attribute_descriptions[2].location = 2;
	vertex_input_attribute_descriptions[2].binding = 2;
	vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[2].offset = 0;
	// Location 3 : Vertex tangent
	vertex_input_attribute_descriptions[3].location = 3;
	vertex_input_attribute_descriptions[3].binding = 3;
	vertex_input_attribute_descriptions[3].format = ::framework::gpu::format::r32g32b32_sfloat;
	vertex_input_attribute_descriptions[3].offset = 0;
	// Location 4 : Texture coordinates
	vertex_input_attribute_descriptions[4].location = 4;
	vertex_input_attribute_descriptions[4].binding = 4;
	vertex_input_attribute_descriptions[4].format = ::framework::gpu::format::r32g32_sfloat;
	vertex_input_attribute_descriptions[4].offset = 0;

	::framework::gpu::pipeline_vertex_input_state_create_info pipeline_vertex_input_state_create_info;
	pipeline_vertex_input_state_create_info.vertex_binding_description_count = ::std::size(vertex_input_binding_descriptions);
	pipeline_vertex_input_state_create_info.vertex_binding_descriptions = vertex_input_binding_descriptions;
	pipeline_vertex_input_state_create_info.vertex_attribute_description_count = ::std::size(vertex_input_attribute_descriptions);
	pipeline_vertex_input_state_create_info.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_list;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &pipeline_vertex_input_state_create_info;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = nullptr;
	graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = nullptr;
	graphics_pipeline_create_info.layout = this->transparent_pipeline_layout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 2;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->transparent_pipeline));
}

void ::framework::graphics::deferred_shading::scene_renderer::create_frame_buffer_attachment(::framework::gpu::format format, ::framework::gpu::image_usage_flags usage, ::framework::gpu::image_layout_flags initial_layout, ::framework::gpu::image_aspect_flags aspect_mask, ::framework::gpu::clear_value const & optimized_clear_value, frame_buffer_attachment & frame_buffer_attachment)
{
	frame_buffer_attachment.format = format;

	::framework::gpu::image_create_info image_create_info;
	image_create_info.flags = ::framework::gpu::image_create_flags::none;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = format;
	image_create_info.extent = { this->width, this->height, 1 };
	image_create_info.mip_levels = 1;
	image_create_info.array_layers = 1;
	image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
	image_create_info.usage = usage;
	image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
	image_create_info.initial_layout = initial_layout;
	image_create_info.optimized_clear_value = &optimized_clear_value;
	assert_framework_gpu_result(this->device->create_image(&image_create_info, nullptr, &frame_buffer_attachment.image));

	::framework::gpu::memory_requirements memory_requirements;

	::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
	image_memory_requirements_info.image = frame_buffer_attachment.image;
	this->device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
	memory_allocate_info.device_mask = 0;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	memory_allocate_info.memory_dedicated_allocate_info.image = frame_buffer_attachment.image;
	memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
	memory_allocate_info.opaque_capture_address = 0;
	assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &frame_buffer_attachment.device_memory));

	::framework::gpu::bind_image_memory_info bind_image_memory_info;
	bind_image_memory_info.image = frame_buffer_attachment.image;
	bind_image_memory_info.memory = frame_buffer_attachment.device_memory;
	bind_image_memory_info.memory_offset = 0;
	assert_framework_gpu_result(this->device->bind_image_memory(1, &bind_image_memory_info));

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
	image_view_create_info.image = frame_buffer_attachment.image;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = format;
	image_view_create_info.subresource_range.aspect_mask = aspect_mask;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = 1;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
	image_view_create_info.components.r = ::framework::gpu::component_swizzle::r;
	image_view_create_info.components.g = ::framework::gpu::component_swizzle::g;
	image_view_create_info.components.b = ::framework::gpu::component_swizzle::b;
	image_view_create_info.components.a = ::framework::gpu::component_swizzle::a;
	assert_framework_gpu_result(this->device->create_image_view(&image_view_create_info, nullptr, &frame_buffer_attachment.image_view));
}

void ::framework::graphics::deferred_shading::scene_renderer::destroy_frame_buffer_attachment(frame_buffer_attachment & frame_buffer_attachment)
{
	this->device->destroy_image_view(frame_buffer_attachment.image_view, nullptr);
	this->device->destroy_image(frame_buffer_attachment.image, nullptr);
	this->device->free_memory(frame_buffer_attachment.device_memory, nullptr);
}