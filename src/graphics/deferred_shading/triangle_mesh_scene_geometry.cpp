#include "graphics/deferred_shading/scene.hpp"
#include "graphics/deferred_shading/triangle_mesh_scene_geometry.hpp"
#include "graphics/deferred_shading/triangle_mesh_geometry.hpp"
#include "graphics/deferred_shading/material.hpp"
#include <glm/gtc/matrix_transform.hpp>

::framework::graphics::deferred_shading::triangle_mesh_scene_geometry::triangle_mesh_scene_geometry(::framework::graphics::deferred_shading::scene * scene, ::framework::graphics::triangle_mesh_scene_geometry_create_info * create_info) :
	geometry(create_info->triangle_mesh_geometry)
{
	::std::uint32_t const submesh_count = static_cast<::framework::graphics::deferred_shading::triangle_mesh_geometry *>(create_info->triangle_mesh_geometry)->get_submesh_count();
	this->primitives.resize(submesh_count);
	for (::std::uint32_t i = 0; i < submesh_count; i++)
	{
		::framework::graphics::deferred_shading::triangle_submesh_scene_primitive * triangle_submesh_scene_primitive = new ::framework::graphics::deferred_shading::triangle_submesh_scene_primitive(this, create_info->material_count > i ? static_cast<::framework::graphics::deferred_shading::material *>(create_info->materials[i]) : nullptr, static_cast<::framework::graphics::deferred_shading::triangle_mesh_geometry *>(create_info->triangle_mesh_geometry)->get_submesh(i));
		this->primitives[i] = triangle_submesh_scene_primitive;
		static_cast<::framework::graphics::deferred_shading::scene *>(scene)->add_opaque_primitive(triangle_submesh_scene_primitive);
	}
}

void ::framework::graphics::deferred_shading::triangle_mesh_scene_geometry::set_transform(::glm::mat3x4 const & transform)
{
	this->local_to_world_matrix = transform;
}