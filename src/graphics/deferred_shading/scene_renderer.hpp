#pragma once

#include "graphics/scene_renderer.hpp"
#include "graphics/scene.hpp"
#include "gpu/core.hpp"
#include "memory/allocator.hpp"
#include "concurrency/thread_pool.h"
#include "coroutine/task.hpp"
#include "coroutine/thread_pool_task.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/async_manual_reset_event.hpp"
#include "coroutine/async_auto_reset_event.hpp"
#include "resource/resource_manager.hpp"
#include "shadow_renderer.hpp"
#include <memory>
#include <condition_variable>

namespace framework::graphics::deferred_shading
{
	class scene;

	struct scene_renderer_create_info
	{
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::std::uint32_t								width;
		::std::uint32_t								height;
		::framework::gpu::format					color_attachments_format;
		::std::uint32_t								frame_buffer_count;
		::framework::gpu::image_view ** color_attachments;
		::framework::gpu::pipeline_cache * pipeline_cache;
		bool										reverse_z_depth;
		::framework::memory::allocator * single_frame_allocator;
		::framework::resource::resource_manager * resource_manager;
		::framework::coroutine::static_thread_pool * thread_pool;
	};

	struct scene_renderer_resize_info
	{
		::std::uint32_t					width;
		::std::uint32_t					height;
		::framework::gpu::image_view ** color_attachments;
	};

	class scene_renderer : public ::framework::graphics::scene_renderer
	{
	public:
		// G-Buffer Framebuffer attachments
		struct frame_buffer_attachment
		{
			::framework::gpu::image * image;
			::framework::gpu::device_memory * device_memory;
			::framework::gpu::image_view * image_view;
			::framework::gpu::format format;
		};

		struct attachments
		{
			::framework::graphics::deferred_shading::scene_renderer::frame_buffer_attachment position, normal, albedo, depth_stencil;
		};

		struct device_transfer_resources
		{
			::framework::gpu::command_buffer * command_buffer;
			::framework::gpu::fence * fence;
			::framework::coroutine::async_auto_reset_event async_auto_reset_event{ true };
		};

		scene_renderer() = delete;

		explicit scene_renderer(::framework::graphics::deferred_shading::scene_renderer_create_info const * create_info);

		~scene_renderer();

		void set_scene(::framework::graphics::scene * scene) override;

		void set_scene_views(::std::uint32_t scene_view_count, ::framework::graphics::scene_view ** scene_views) override;

		::framework::graphics::scene * create_scene(::framework::graphics::scene_create_info * create_info) override;

		::framework::graphics::triangle_mesh_geometry * create_triangle_mesh_geometry(::framework::graphics::triangle_mesh_geometry_create_info const & create_info) override;

		::framework::graphics::image * create_image(::framework::graphics::image_create_info const & create_info) override;

		::framework::graphics::material * create_material(::framework::graphics::material_create_info const & create_info) override;

		::framework::graphics::medium * create_medium() override;

		::framework::graphics::scene_view_perspective * create_scene_view_perspective(::framework::graphics::scene_view_perspective_create_info * create_info) override;

		::framework::gpu::semaphore * get_frame_semaphore() const noexcept override;

		::std::uint64_t get_last_frame_index() const noexcept override;

		void resize(scene_renderer_resize_info const * resize_info);

		void update_command_buffer(::std::uint32_t buffer_index);

		::framework::gpu::command_buffer * get_command_buffer(::std::uint32_t buffer_index) { return frame_resources[buffer_index].primary_command_buffer; }

		struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources & get_device_transfer_resources(::std::uint32_t index) { return this->device_transfer_resources[index]; }

		::framework::gpu::fence * get_fence(::std::uint32_t buffer_index) { return frame_resources[buffer_index].fence; }

		struct ::framework::graphics::deferred_shading::scene_renderer::attachments const & get_attachments() const { return this->attachments; }

		::framework::coroutine::static_thread_pool & get_thread_pool() { return this->thread_pool; }

		::framework::gpu::physical_device * get_physical_device() { return this->physical_device; }

		::framework::gpu::physical_device_properties const & get_physical_device_properties() const { return this->physical_device_properties; }

		::framework::gpu::physical_device_memory_properties const & get_physical_device_memory_properties() const { return this->physical_device_memory_properties; }

		::framework::gpu::physical_device_features const & get_physical_device_features() const { return this->physical_device_features; }

		::framework::gpu::device * get_device() { return device; }

		::framework::gpu::descriptor_set_layout * get_scene_descriptor_set_layout() const { return this->scene_descriptor_set_layout; }

		::framework::gpu::descriptor_set * get_scene_descriptor_set() const { return this->scene_descriptor_set; }

		::framework::gpu::device_size get_staging_buffer_size() const { return this->staging_buffer_size; }

		::framework::gpu::buffer * get_staging_buffer() const { return this->staging_buffer; }

		::framework::gpu::device_memory * get_staging_device_memory() const { return this->staging_device_memory; }

		::framework::gpu::device_size get_indices_buffer_size() const { return this->indices_buffer_size; }

		::framework::gpu::device_memory * get_indices_device_memory() const { return this->indices_device_memory; }

		::framework::gpu::device_size allocate_indices_device_memory(::framework::gpu::device_size size, ::framework::gpu::device_size alignment) { ::framework::gpu::device_size const offset = (this->indices_device_memory_offset + alignment - 1) & ~(alignment - 1); this->indices_device_memory_offset = offset + size; return offset; }

		::framework::gpu::device_size get_indices_device_memory_usage() { return this->indices_device_memory_offset; }

		::framework::gpu::device_size get_vertices_buffer_size() const { return this->vertices_buffer_size; }

		::framework::gpu::device_memory * get_vertices_device_memory() const { return this->vertices_device_memory; }

		::framework::gpu::device_size allocate_vertices_device_memory(::framework::gpu::device_size size, ::framework::gpu::device_size alignment) { ::framework::gpu::device_size const offset = (this->vertices_device_memory_offset + alignment - 1) & ~(alignment - 1); this->vertices_device_memory_offset = offset + size; return offset; }

		::framework::gpu::device_size get_vertices_device_memory_usage() { return this->vertices_device_memory_offset; }

		::framework::coroutine::lazy_task<::std::uint32_t> acquire_device_transfer_resources();

		::framework::coroutine::lazy_task<void> release_device_transfer_resources(::std::uint32_t index);

		auto const & get_queues() { return this->queues; }

		auto const & get_queue_family_indices() { return this->queue_family_indices; }

	private:
		void create_frame_buffer(::framework::gpu::image_view ** color_attachments);

		void create_frame_buffer_attachments();

		void create_render_pass();

		void create_scene_pipeline();

		void create_composition_pipeline();

		void create_transparent_pipeline();

		void create_frame_buffer_attachment(::framework::gpu::format format, ::framework::gpu::image_usage_flags usage, ::framework::gpu::image_layout_flags initial_layout, ::framework::gpu::image_aspect_flags aspect_mask, ::framework::gpu::clear_value const & optimized_clear_value, frame_buffer_attachment & frame_buffer_attachment);

		void destroy_frame_buffer_attachment(frame_buffer_attachment & frame_buffer_attachment);

		struct thread_data
		{
			struct command_buffer_data
			{
				bool is_command_buffer_open;
			};

			::framework::gpu::command_pool * command_pool;
			::framework::gpu::command_buffer * scene_subpass_command_buffer;
			::framework::graphics::deferred_shading::scene_renderer::thread_data::command_buffer_data scene_subpass_command_buffer_data;
			::framework::gpu::command_buffer * transparent_subpass_command_buffer;
			::framework::graphics::deferred_shading::scene_renderer::thread_data::command_buffer_data transparent_subpass_command_buffer_data;
		};

		struct ::framework::graphics::deferred_shading::scene_renderer::attachments attachments;

		void * mapped_data;
		::framework::gpu::buffer * unsortedData, * unsortedValues, * unsortedDataUpload, * unsortedDataReadback;
		::framework::gpu::device_memory * unsortedDataMemory, * unsortedValuesMemory, * unsortedDataUploadMemory, * unsortedDataReadbackMemory;
		struct path_tracer_parameters
		{
			::glm::mat4 projectionMatrix;
			::glm::mat4 modelMatrix;
			::glm::mat4 viewMatrix;
			::glm::vec4 camPos;
			::glm::vec4 lightPos;
		};
		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;
		struct frame_resources
		{
			::framework::gpu::fence * fence;
			::framework::gpu::command_pool * secondary_command_pool;
			::framework::gpu::command_buffer * primary_command_buffer;
			::std::vector<::framework::graphics::deferred_shading::scene_renderer::thread_data> thread_data;
		};
		::std::vector<frame_resources> frame_resources;
		::framework::coroutine::static_thread_pool & thread_pool;
		::std::uint32_t device_transfer_resources_count;
		::std::unique_ptr<struct ::framework::graphics::deferred_shading::scene_renderer::device_transfer_resources[]> device_transfer_resources;
		::framework::coroutine::mutex device_transfer_resources_mutex;
		::std::uint32_t device_transfer_resources_ring_buffer_index;
		::std::uint32_t thread_count;
		::std::uint32_t m_frame_buffer_count;
		::std::uint32_t width, height;
		::framework::gpu::format color_attachments_format;
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::framework::gpu::physical_device_features physical_device_features;
		::framework::gpu::physical_device_properties physical_device_properties;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::framework::gpu::command_pool * primary_command_pool;
		::framework::gpu::command_pool * transfer_command_pool;
		::framework::gpu::device_size staging_buffer_size;
		::framework::gpu::device_size indices_buffer_size;
		::framework::gpu::device_size vertices_buffer_size;
		::framework::gpu::buffer * staging_buffer;
		::framework::gpu::device_memory * staging_device_memory;
		::std::vector<::framework::gpu::frame_buffer *> frame_buffers;
		::framework::gpu::render_pass * render_pass;
		::framework::gpu::render_pass_state * render_pass_state;
		::framework::gpu::buffer * ssbo;
		::framework::gpu::device_memory * m_pSsboDeviceMemory;
		::framework::gpu::buffer_view * m_pSSBOView;
		::framework::gpu::device_size indices_device_memory_offset;
		::framework::gpu::device_memory * indices_device_memory;
		::framework::gpu::device_size vertices_device_memory_offset;
		::framework::gpu::device_memory * vertices_device_memory;
		::framework::gpu::shader_module * scene_vertex_shader_module;
		::framework::gpu::shader_module * scene_fragment_shader_module;
		::framework::gpu::shader_module * m_pCompositionVertexShaderModule;
		::framework::gpu::shader_module * m_pCompositionFragmentShaderModule;
		::framework::gpu::shader_module * m_pTransparentVertexShaderModule;
		::framework::gpu::shader_module * m_pTransparentFragmentShaderModule;
		::framework::gpu::pipeline_layout * scene_pipeline_layout;
		::framework::gpu::pipeline_layout * m_pCompositionPipelineLayout;
		::framework::gpu::pipeline_layout * transparent_pipeline_layout;
		::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::gpu::pipeline * scene_pipeline;
		::framework::gpu::pipeline * m_pCompositionPipeline;
		::framework::gpu::pipeline * transparent_pipeline;
		::framework::gpu::descriptor_set_layout * scene_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * composition_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * transparent_descriptor_set_layout;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_pool * device_descriptor_pool;
		::framework::gpu::descriptor_set * scene_descriptor_set;
		::framework::gpu::descriptor_set * composition_descriptor_set;
		::framework::gpu::descriptor_set * transparent_descriptor_set;
		bool m_reverse_z_depth;
		::framework::gpu::buffer * m_ubo;
		::framework::gpu::device_memory * ubo_device_memory;
		::framework::gpu::buffer * material_buffer;
		::framework::gpu::device_memory * material_device_memory;
		::framework::gpu::device_size material_ubo_dynamic_alignment;
		void * material_mapped_data;
		::framework::gpu::sampler * sampler;
		::glm::vec4 light_pos;
		::framework::graphics::deferred_shading::scene * scene;
		::std::vector<scene_view *> scene_views;
		::framework::memory::allocator * single_frame_allocator;
		::framework::gpu::device_size ubo_dynamic_alignment;
		::framework::resource::resource_manager * resource_manager;
		::std::unique_ptr<::framework::graphics::deferred_shading::shadow_renderer> shadow_renderer;
		::std::uint32_t const max_texture_count;
		::std::uint32_t const max_material_count;
		::std::uint32_t texture_counter;
		::std::uint32_t material_counter;
	};
}