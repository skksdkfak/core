#include "graphics/deferred_shading/image.hpp"
#include "graphics/deferred_shading/material.hpp"
#include "material.hpp"

::framework::graphics::deferred_shading::material::material(::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::material_create_info const & create_info, ::std::uint32_t id) :
	id(id)
{
}

void ::framework::graphics::deferred_shading::material::set_thin_surface(bool thin_surface)
{
}

void ::framework::graphics::deferred_shading::material::set_roughness(::glm::vec2 roughness)
{
}

::framework::graphics::image * ::framework::graphics::deferred_shading::material::get_diffuse_texture() const
{
	return nullptr;
}

void ::framework::graphics::deferred_shading::material::set_diffuse_texture(::framework::graphics::image * image)
{
	this->device_data.diffuse_texture = image ? static_cast<::framework::graphics::deferred_shading::image *>(image)->get_id() : -1;
}

void ::framework::graphics::deferred_shading::material::set_emissive_image(::framework::graphics::image * emissive_image)
{
}