#pragma once

#include "coroutine/shared_task.hpp"
#include "gpu/core.hpp"
#include "graphics/image.hpp"

namespace framework::graphics::deferred_shading
{
	class scene_renderer;

	class image : public ::framework::graphics::image
	{
	public:
		explicit image(::std::uint32_t id, ::framework::graphics::deferred_shading::scene_renderer * scene_renderer, ::framework::graphics::image_create_info const & create_info);

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept override { return this->initialization_shared_task; }

		::std::uint32_t get_id() const { return this->id; }

	private:
		::framework::coroutine::shared_task<void> initialize(::framework::graphics::image_create_info const & create_info);

		::std::uint32_t const id;
		::framework::graphics::deferred_shading::scene_renderer * const scene_renderer;
		::framework::coroutine::shared_task<void> initialization_shared_task;
		::framework::gpu::image * device_image;
		::framework::gpu::device_memory * device_memory;
		::framework::gpu::image_view * image_view;
	};
}