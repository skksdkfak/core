#pragma once

#include "scene_primitive.hpp"
#include "triangle_mesh_geometry.hpp"
#include "material.hpp"
#include <cstdint>

namespace framework::graphics::deferred_shading
{
	class triangle_mesh_scene_geometry;

	class triangle_submesh_scene_primitive : public ::framework::graphics::deferred_shading::scene_primitive
	{
	public:
		triangle_submesh_scene_primitive(::framework::graphics::deferred_shading::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry, ::framework::graphics::deferred_shading::material * material, ::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh const & submesh);

		virtual void draw(::framework::gpu::command_buffer * command_buffer) override;

		virtual ::glm::mat4 const & get_local_to_world_matrix() override;

		::framework::graphics::deferred_shading::material * get_material() const override { return this->material; }

	private:
		::framework::graphics::deferred_shading::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry;
		::framework::graphics::deferred_shading::material * material;
		::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh const & submesh;
		::framework::graphics::deferred_shading::triangle_mesh_geometry * triangle_mesh_geometry;
	};
}