#include "triangle_submesh_scene_primitive.hpp"
#include "triangle_mesh_scene_geometry.hpp"
#include "graphics/material.hpp"

::framework::graphics::deferred_shading::triangle_submesh_scene_primitive::triangle_submesh_scene_primitive(::framework::graphics::deferred_shading::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry, ::framework::graphics::deferred_shading::material * material, ::framework::graphics::deferred_shading::triangle_mesh_geometry::submesh const & submesh) :
	triangle_mesh_scene_geometry(triangle_mesh_scene_geometry),
	material(material),
	submesh(submesh),
	triangle_mesh_geometry(static_cast<::framework::graphics::deferred_shading::triangle_mesh_geometry *>(triangle_mesh_scene_geometry->get_geometry()))
{
}

void ::framework::graphics::deferred_shading::triangle_submesh_scene_primitive::draw(::framework::gpu::command_buffer * command_buffer)
{
    command_buffer->bind_index_buffer(this->triangle_mesh_geometry->get_index_buffer(), 0, ::framework::gpu::index_type::uint32);
    command_buffer->bind_vertex_buffers(this->submesh.vertex_layout->first_binding, this->submesh.vertex_layout->binding_count, this->submesh.vertex_layout->buffers, this->submesh.vertex_layout->offsets);
    command_buffer->draw_indexed(this->submesh.index_count, 1, this->submesh.index_offset, 0, 0);
}

::glm::mat4 const & ::framework::graphics::deferred_shading::triangle_submesh_scene_primitive::get_local_to_world_matrix()
{
	return this->triangle_mesh_scene_geometry->get_local_to_world_matrix();
}