#pragma once

#include "gpu/core.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::deferred_shading
{
	class material;

	class scene_primitive
	{
	public:
		virtual ~scene_primitive() = default;

		virtual void draw(::framework::gpu::command_buffer * command_buffer) = 0;

		virtual ::glm::mat4 const & get_local_to_world_matrix() = 0;

		virtual ::framework::graphics::deferred_shading::material * get_material() const = 0;
	};
}