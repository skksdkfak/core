#pragma once

#include "graphics/triangle_mesh_scene_geometry.hpp"
#include "triangle_submesh_scene_primitive.hpp"
#include "scene_geometry.hpp"
#include "scene_renderer.hpp"

namespace framework::graphics::deferred_shading
{
	class triangle_mesh_scene_geometry : virtual public ::framework::graphics::triangle_mesh_scene_geometry, public ::framework::graphics::deferred_shading::scene_geometry
	{
	public:
		triangle_mesh_scene_geometry(::framework::graphics::deferred_shading::scene * scene, ::framework::graphics::triangle_mesh_scene_geometry_create_info * create_info);

		void set_transform(::glm::mat3x4 const & transform) override;

		::glm::mat3x4 const & get_local_to_world_matrix() const { return this->local_to_world_matrix; }

		::framework::graphics::geometry * get_geometry() { return this->geometry; }

	private:
		::std::vector<::framework::graphics::deferred_shading::triangle_submesh_scene_primitive *> primitives;
		::glm::mat3x4 local_to_world_matrix;
		::framework::graphics::geometry * geometry;
	};
}