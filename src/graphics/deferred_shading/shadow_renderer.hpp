#pragma once

#include <engine_core.h>
#include "gpu/core.hpp"
#include "concurrency/thread_pool.h"
#include <glm/glm.hpp>

namespace framework::resource
{
	class resource_manager;
}

namespace framework::graphics::deferred_shading
{
    class scene_renderer;
    class scene_primitive;

	struct shadow_renderer_create_info
	{
		typename ::framework::resource::resource_manager * resource_manager;
        ::framework::graphics::deferred_shading::scene_renderer * scene_renderer;
		::framework::gpu::physical_device *		physical_device;
		::framework::gpu::device *				device;
		::std::uint32_t				            shadow_map_width;
		::std::uint32_t				            shadow_map_height;
		::std::uint32_t				            max_shadow_map_count;
		::framework::gpu::format				shadow_map_format;
		::std::uint32_t				            frame_buffer_count;
		::framework::gpu::pipeline_cache *		pipeline_cache;
	};

	class shadow_renderer
	{
	public:
		shadow_renderer() = delete;

		shadow_renderer(::framework::graphics::deferred_shading::shadow_renderer_create_info const & create_info);

		~shadow_renderer();

		void render(::std::uint32_t buffer_index, ::std::uint32_t primitive_count, ::framework::graphics::deferred_shading::scene_primitive ** primitives);

	private:
		void create_frame_buffer();

		void create_pipeline();

		void create_composition_pipeline();

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;

		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;

        struct shadow_map_ubo_data
        {
            ::glm::mat4 wvp;
        };

        struct thread_data
        {
            struct command_buffer_data
            {
                bool is_command_buffer_open;
            };

            ::framework::gpu::command_pool * command_pool;
            ::framework::gpu::command_buffer * command_buffer;
            ::framework::graphics::deferred_shading::shadow_renderer::thread_data::command_buffer_data command_buffer_data;
        };

        struct frame_resources
        {
            ::framework::gpu::fence * fence;
            ::std::vector<::framework::graphics::deferred_shading::shadow_renderer::thread_data> thread_data;
        };
        ::std::vector<frame_resources> frame_resources;
		::framework::graphics::deferred_shading::scene_renderer & scene_renderer;
        ::framework::gpu::physical_device_properties physical_device_properties;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::framework::gpu::command_pool * command_pool;
		::std::vector<::framework::gpu::command_buffer *> command_buffers;
		::std::vector<::framework::gpu::frame_buffer *> frame_buffers;
        ::framework::gpu::pipeline_layout * shadowmap_pipeline_layout;
        ::framework::gpu::descriptor_set_layout * shadowmap_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * shadowing_descriptor_set_layout;
        ::framework::gpu::descriptor_set * shadowmap_descriptor_set;
        ::framework::gpu::buffer * shadowmap_ubo;
		::framework::gpu::render_pass * render_pass;
		::framework::gpu::render_pass_state * render_pass_state;
		::framework::gpu::shader_module * shadowmap_vertex_shader_module;
		::framework::gpu::shader_module * shadowmap_geometry_shader_module;
		::framework::gpu::shader_module * shadowing_vertex_shader_module;
		::framework::gpu::shader_module * shadowing_fragment_shader_module;
		::framework::gpu::pipeline_layout * shadowing_pipeline_layout;
		::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::gpu::pipeline * shadowmap_pipeline;
		::framework::gpu::pipeline * shadowing_pipeline;
		::framework::gpu::image * shadow_map_image;
		::framework::gpu::device_memory * shadow_map_device_memory;
		::framework::gpu::buffer * uniform_buffer;
		::framework::gpu::device_memory * uniform_buffer_device_memory;
		::framework::gpu::image_view * shadow_map_image_view;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_set * shadow_descriptor_set;
		::std::uint32_t shadow_map_width, shadow_map_height, shadow_map_layer_count;
		::std::uint32_t frame_buffer_count;
		::framework::gpu::format shadow_map_format;
		::std::uint32_t thread_count;
		::std::size_t ubo_dynamic_alignment;
		void * ubo_mapped_data;
		::std::uint32_t width;
		::std::uint32_t height;

		// per light data
		struct Light
		{
			class camera * pCamera;
		};

		::std::vector<Light> m_lights;
	};
}