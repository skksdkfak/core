#pragma once

#include "../scene_view_perspective.hpp"
#include "scene_renderer.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::deferred_shading
{
	class scene_view_perspective : public ::framework::graphics::scene_view_perspective
	{
	public:
		scene_view_perspective(scene_renderer * scene_renderer, scene_view_perspective_create_info * create_info);

		virtual ~scene_view_perspective() override;

		virtual void set_eye(::glm::vec3 const & eye) override;

		virtual void set_center(::glm::vec3 const & center) override;

		virtual void set_up(::glm::vec3 const & up) override;

		virtual ::glm::vec3 const & get_eye() override { return eye; }

		virtual ::glm::vec3 const & get_center() override { return center; }

		virtual ::glm::vec3 const & get_up() override { return up; }

		virtual void set_fov(float vertical_fov_radians) override;

		virtual void set_aspect_ratio(float aspect_height_over_width) override;

		virtual void set_near_z(float near_z_clip) override;

		virtual void set_far_z(float far_z_clip) override;

		virtual void reverse_z(bool enable) override;

		virtual void inverse_y(bool enable) override;

		virtual void update() override;

		::glm::mat4 const & get_view() const { return view; }

		::glm::mat4 const & get_projection() const { return projection; }

		::glm::mat4 const & get_view_projection() const { return view_projection; }

	private:
		::glm::vec3 eye;
		::glm::vec3 center;
		::glm::vec3 up;
		::glm::mat4 view;
		::glm::mat4 projection;
		::glm::mat4 view_projection;
		float m_verticalFOV;
		float m_aspectRatio;
		float m_nearClip, m_farClip;
		bool m_reverseZ;
		bool m_inverseY;
	};
}