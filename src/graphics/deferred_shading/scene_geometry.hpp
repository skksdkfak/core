#pragma once

#include "graphics/scene_geometry.hpp"

namespace framework::graphics::deferred_shading
{
	class scene;

	class scene_geometry : virtual public ::framework::graphics::scene_geometry
	{
	public:
		~scene_geometry() = default;
	};
}