#pragma once

#include <glm/glm.hpp>

namespace framework::graphics
{
	class image;

	struct material_create_info
	{
	};

	class material_node
	{
	};

	class material
	{
	public:
		virtual void set_thin_surface(bool thin_surface) = 0;

		virtual void set_roughness(::glm::vec2 roughness) = 0;

		virtual ::framework::graphics::image * get_diffuse_texture() const = 0;

		virtual void set_diffuse_texture(::framework::graphics::image * image) = 0;

		virtual void set_emissive_image(::framework::graphics::image * emissive_image) = 0;
	};
}