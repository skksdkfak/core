#pragma once

#include <cstdint>

namespace framework::graphics
{
	class geometry
	{
	public:
		virtual ::std::uint32_t get_geometry_count() const noexcept = 0;
	};
}