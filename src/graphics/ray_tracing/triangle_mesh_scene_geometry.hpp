#pragma once

#include "command_context.hpp"
#include "coroutine/shared_task.hpp"
#include "graphics/ray_tracing/scene_geometry.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "graphics/triangle_mesh_scene_geometry.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::ray_tracing
{
	class material;
	class medium;

	class triangle_mesh_scene_geometry : virtual public ::framework::graphics::triangle_mesh_scene_geometry, public ::framework::graphics::ray_tracing::scene_geometry
	{
	public:
		triangle_mesh_scene_geometry(::framework::graphics::ray_tracing::scene * scene, ::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info);

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept override;

		::framework::graphics::geometry * get_geometry() const noexcept override;

		::framework::graphics::geometry_instance_id get_geometry_instance_id(::std::uint32_t geometry_index) const noexcept override;

		::framework::graphics::material * get_material(::std::uint32_t geometry_index) const noexcept override;

		::framework::graphics::medium * get_medium(::std::uint32_t geometry_index) const noexcept override;

		void set_transform(::glm::mat3x4 const & transform) override;

		::framework::coroutine::immediate_task<void> initialize(class ::framework::command_context * transfer_command_context);

		::framework::coroutine::immediate_task<::glm::mat3x4> get_local_to_world_matrix() noexcept;

		::std::uint32_t get_instance_id() const noexcept;

		::std::uint32_t get_instance_index() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene;

		::framework::coroutine::shared_task<void> initialize(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info);

		::framework::coroutine::self_destructible_coroutine set_transform_device(::glm::mat3x4 const transform) noexcept;

		::framework::coroutine::shared_task<void> initialization_shared_task;
		::framework::graphics::ray_tracing::scene * const scene;
		::framework::coroutine::mutex device_data_mutex;
		::glm::mat3x4 local_to_world_matrix;
		::framework::graphics::ray_tracing::triangle_mesh_geometry * geometry;
		::std::vector<::framework::graphics::ray_tracing::material *> materials;
		::std::vector<::framework::graphics::ray_tracing::medium *> media;
		::std::uint32_t instance_id;
		::std::uint32_t instance_index;
	};
}

#include "graphics/ray_tracing/triangle_mesh_scene_geometry.inl"