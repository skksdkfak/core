#pragma once

#include "graphics/scene_view_perspective.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class scene_view_perspective : public ::framework::graphics::scene_view_perspective
	{
	public:
		struct device_data
		{
			::glm::mat4x4 view;
			::glm::mat4x4 projection;
			::glm::mat4x4 view_projection;
			::glm::mat4x4 view_inverse;
			::glm::mat4x4 projection_inverse;
			float jitter_x;
			float jitter_y;
			::std::uint32_t padding[2];
		};

		scene_view_perspective(::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::scene_view_perspective_create_info * create_info);

		~scene_view_perspective() override;

		void set_eye(::glm::vec3 const & eye) override;

		void set_center(::glm::vec3 const & center) override;

		void set_up(::glm::vec3 const & up) override;

		::glm::vec3 const & get_eye() override { return this->eye; }

		::glm::vec3 const & get_center() override { return this->center; }

		::glm::vec3 const & get_up() override { return this->up; }

		void set_fov(float vertical_fov_radians) override;

		void set_aspect_ratio(float aspect_height_over_width) override;

		void set_near_z(float near_z_clip) override;

		void set_far_z(float far_z_clip) override;

		void reverse_z(bool enable) override;

		void inverse_y(bool enable) override;

		void update() override;

		::glm::mat4 const & get_view() const { return this->device_data.view; }

		::glm::mat4 const & get_projection() const { return this->device_data.projection; }

		::glm::mat4 const & get_view_projection() const { return this->device_data.view_projection; }

		struct ::framework::graphics::ray_tracing::scene_view_perspective::device_data get_device_data() const { return this->device_data; }

	private:
		struct ::framework::graphics::ray_tracing::scene_view_perspective::device_data device_data;
		::glm::vec3 eye;
		::glm::vec3 center;
		::glm::vec3 up;
		float m_verticalFOV;
		float m_aspectRatio;
		float m_nearClip, m_farClip;
		bool m_reverseZ;
		bool m_inverseY;
	};
}