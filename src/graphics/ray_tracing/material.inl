inline::std::uint32_t(::framework::graphics::ray_tracing::material::get_id)() const noexcept
{
	return this->id;
}

inline bool ::framework::graphics::ray_tracing::material::is_emissive() const noexcept
{
	return this->header.data.x & (1u << ::framework::graphics::ray_tracing::material_header::material_flags::emissive_flag_offset);
}

inline bool (::framework::graphics::ray_tracing::material::add_to_update)() noexcept
{
	if ((this->device_data_state & ::framework::graphics::ray_tracing::material::device_data_state::ready_for_update) == ::framework::graphics::ray_tracing::material::device_data_state::ready_for_update)
	{
		this->device_data_state &= ~::framework::graphics::ray_tracing::material::device_data_state::perform_update;
		return true;
	}
	return false;
}