#pragma once

#include "graphics/volume_geometry.hpp"
#include "coroutine/task.hpp"
#include <gpu/core.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class volume_geometry : public ::framework::graphics::volume_geometry
	{
	public:
		::framework::gpu::acceleration_structure * get_bottom_level_acceleration_structure() const { return this->bottom_level_acceleration_structure; }

		::framework::gpu::device_address get_bottom_level_acceleration_structure_device_address() const { return this->bottom_level_acceleration_structure_device_address; }

	//private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		volume_geometry(::std::uint32_t id);

		~volume_geometry();

		::framework::coroutine::lazy_task<void> initialize(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		::std::uint32_t const id;
		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::acceleration_structure_geometry_set * bottom_acceleration_structure_geometry_set;
		::framework::gpu::device_address bottom_level_acceleration_structure_device_address;
		::framework::gpu::buffer * bottom_level_acceleration_structure_buffer;
		::framework::gpu::acceleration_structure * bottom_level_acceleration_structure;
		::framework::gpu::device_size aabbs_memory_offset;
	};
}