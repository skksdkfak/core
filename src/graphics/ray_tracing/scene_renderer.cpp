#include "engine_core.h"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "gpu/utility.hpp"
#include "graphics/ray_tracing/light.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/scene_view_perspective.hpp"
#include "graphics/ray_tracing/image.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "physics/color.hpp"
#include "physics/spectrum.hpp"
#include <glm/glm.hpp>
#include <algorithm>
#include <cassert>
#include <cstring>
#include <execution>
#include <format>
#include <iostream>
#include <random>

::framework::gpu::component_type constexpr grid_gradient_type = ::framework::gpu::component_type::float32;
::std::uint32_t const feature_count = 32u;
::framework::nn::grid::grid_type const grid_type = ::framework::nn::grid::grid_type::hash;
::framework::nn::grid::hash_type const hash_type = ::framework::nn::grid::hash_type::coherent_prime;
::framework::nn::grid::interpolation_type const interpolation_type = ::framework::nn::grid::interpolation_type::smoothstep;
::std::uint32_t const log2_hashmap_size = 19u;
::std::uint32_t const base_resolution = 1u << 7;
float const per_level_scale = 1.5f;
bool const stochastic_interpolation = true;
::std::uint32_t const features_per_level = 4u;

namespace framework::local
{
	namespace
	{
		static ::std::uint32_t const max_path_length = 4u;
		static ::std::uint32_t const max_batch_size = 1u << 20;
		static ::std::uint32_t const max_training_sample_pool_size = 1u << 22;
		static ::std::uint32_t const sample_buffer_stride = 16;

		struct generate_forward_test_samples_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t width;
			::std::uint32_t height;
			::std::uint32_t log_jacobian_buffer_stride;
		};
	}
}

::framework::graphics::ray_tracing::scene_renderer::scene_renderer(::framework::graphics::ray_tracing::scene_renderer_create_info const * create_info) :
	current_resource_index(0),
	gpu_context(create_info->gpu_context),
	frame_buffer_count(create_info->frame_buffer_count),
	width(create_info->width),
	height(create_info->height),
	output_color_format(create_info->output_color_format),
	output_color_image(create_info->output_color_image),
	output_color_image_view(create_info->output_color_image_view),
	single_frame_allocator(create_info->single_frame_allocator),
	device_descriptor_pool(create_info->device_descriptor_pool),
	scene(nullptr),
	spatial_resampling_pipeline(nullptr),
	resource_manager(create_info->resource_manager),
	thread_pool(*create_info->thread_pool),
	command_buffer_manager(*create_info->command_buffer_manager),
	profiler(create_info->profiler),
	gpu_log(create_info->gpu_log),
	attachments_descriptor_set(create_info->attachments_descriptor_set),
	staging_buffer_current_offset(0),
	triangle_mesh_geometry_counter(0),
	texture_counter(0),
	indices_device_memory_offset(0),
	vertices_device_memory_offset(0),
	bottom_level_acceleration_structure_device_memory_offset(0),
	frame_index(0),
	nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::none),
	enable_nee(false),
	enable_ncv(false),
	enable_nis(false),
	enable_nis_training(false),
	is_reset_nis(true)
{
	this->frame_resources.resize(this->frame_buffer_count);

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		for (::std::uint32_t f = 0; f < this->frame_buffer_count; f++)
		{
			auto & frame_resource = this->frame_resources[f];

			::framework::command_context * const graphics_command_context = co_await this->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();
			frame_resource.graphics_command_context = ::std::unique_ptr<::framework::command_context>(graphics_command_context);

			::framework::gpu::fence * fence;

			::framework::gpu::fence_create_info fence_create_info;
			fence_create_info.flags = ::framework::gpu::fence_create_flags::signaled_bit;
			assert_framework_gpu_result(this->gpu_context->get_device()->create_fence(&fence_create_info, nullptr, &fence));

			frame_resource.fence = ::std::make_unique<::framework::fence>(fence);
		}
	}());

	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		::framework::gpu::descriptor_pool_size descriptor_pool_sizes[7];
		descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_pool_sizes[0].descriptor_count = this->frame_buffer_count;

		descriptor_pool_sizes[1].type = ::framework::gpu::descriptor_type::acceleration_structure;
		descriptor_pool_sizes[1].descriptor_count = 1;

		descriptor_pool_sizes[2].type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_pool_sizes[2].descriptor_count = 2;

		descriptor_pool_sizes[3].type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_pool_sizes[3].descriptor_count = 2048;

		descriptor_pool_sizes[4].type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_pool_sizes[4].descriptor_count = 32;

		descriptor_pool_sizes[5].type = ::framework::gpu::descriptor_type::sampler;
		descriptor_pool_sizes[5].descriptor_count = 16;

		descriptor_pool_sizes[6].type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_pool_sizes[6].descriptor_count = 16;

		::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
		descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::host_only_bit | ::framework::gpu::descriptor_pool_create_flags::update_after_bind_bit;
		descriptor_pool_create_info.max_sets = 16;
		descriptor_pool_create_info.pool_size_count = static_cast<::std::uint32_t>(::std::size(descriptor_pool_sizes));
		descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->host_descriptor_pool));
	}
	else
	{
		this->host_descriptor_pool = this->device_descriptor_pool;
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[14];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 1;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// seed_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// sample_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// nis_samples_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// nis_spatial_features_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[4].hlsl_register_space = 1;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// nis_features_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[5].hlsl_register_space = 1;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// nis_target_buffer
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[6].hlsl_register_space = 1;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// nis_sample_pdf_buffer
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[7].hlsl_register_space = 1;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;
		// light_field_target_buffer
		descriptor_set_layout_bindings[8].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[8].binding = 8;
		descriptor_set_layout_bindings[8].hlsl_shader_register = 5;
		descriptor_set_layout_bindings[8].hlsl_register_space = 1;
		descriptor_set_layout_bindings[8].descriptor_count = 1;
		descriptor_set_layout_bindings[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[8].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[8].immutable_samplers = nullptr;
		// light_field_one_blob_encode_features_buffer
		descriptor_set_layout_bindings[9].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[9].binding = 9;
		descriptor_set_layout_bindings[9].hlsl_shader_register = 6;
		descriptor_set_layout_bindings[9].hlsl_register_space = 1;
		descriptor_set_layout_bindings[9].descriptor_count = 1;
		descriptor_set_layout_bindings[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[9].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[9].immutable_samplers = nullptr;
		// cv_shape_target_buffer
		descriptor_set_layout_bindings[10].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[10].binding = 10;
		descriptor_set_layout_bindings[10].hlsl_shader_register = 7;
		descriptor_set_layout_bindings[10].hlsl_register_space = 1;
		descriptor_set_layout_bindings[10].descriptor_count = 1;
		descriptor_set_layout_bindings[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[10].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[10].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_x_buffer
		descriptor_set_layout_bindings[11].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[11].binding = 11;
		descriptor_set_layout_bindings[11].hlsl_shader_register = 8;
		descriptor_set_layout_bindings[11].hlsl_register_space = 1;
		descriptor_set_layout_bindings[11].descriptor_count = 1;
		descriptor_set_layout_bindings[11].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[11].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[11].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_spatial_features_buffer
		descriptor_set_layout_bindings[12].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[12].binding = 12;
		descriptor_set_layout_bindings[12].hlsl_shader_register = 9;
		descriptor_set_layout_bindings[12].hlsl_register_space = 1;
		descriptor_set_layout_bindings[12].descriptor_count = 1;
		descriptor_set_layout_bindings[12].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[12].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[12].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_features_buffer
		descriptor_set_layout_bindings[13].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[13].binding = 13;
		descriptor_set_layout_bindings[13].hlsl_shader_register = 10;
		descriptor_set_layout_bindings[13].hlsl_register_space = 1;
		descriptor_set_layout_bindings[13].descriptor_count = 1;
		descriptor_set_layout_bindings[13].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[13].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[13].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->shuffle_nis_samples_descriptor_set_layout));
	}

	this->scan = ::std::make_unique<::framework::algorithm::scan>(this->gpu_context, this->resource_manager, this->shuffle_nis_samples_descriptor_set_layout, "::framework::shuffle_nis_samples_scan_executor", "graphics/ray_tracing/shuffle_nis_samples.hlsl");
	this->scan_executor = ::std::make_unique<::framework::algorithm::scan::executor>(*this->scan, this->gpu_context);

	::std::uint32_t const light_field_hidden_width = 64u;
	::std::uint32_t const light_field_hidden_layer_count = 2u;
	::std::uint32_t const light_field_spatial_feature_count = 3u;
	::std::uint32_t const light_field_spatial_feature_stride = 4u;
	::std::uint32_t const light_field_one_blob_encode_features_count = 5u;
	::std::uint32_t const light_field_one_blob_encode_features_stride = 8u;
	::std::uint32_t const light_field_output_size = 1u;
	::std::uint32_t const light_field_output_stride = 16u;
	::std::uint32_t const light_field_one_blob_encoding_bins = 4u;
	::std::uint32_t const light_field_one_blob_encoded_input_size = light_field_one_blob_encode_features_count * light_field_one_blob_encoding_bins;
	::std::uint32_t const light_field_encoded_input_size = ::std::bit_ceil(feature_count + light_field_one_blob_encoded_input_size);
	float const light_field_weight_clipping_magnitude = 1e+0f;
	::framework::linalg::matrix_layout const light_field_neural_network_input_layout = ::framework::linalg::matrix_layout::row_major;
	::framework::linalg::matrix_layout const light_field_neural_network_output_layout = ::framework::linalg::matrix_layout::column_major;
	::std::uint32_t constexpr coupling_layer_count = 2u;
	::std::uint32_t const batch_alignment = 256u;
	::std::uint32_t const inference_batch_alignment = 256u;
	::std::uint32_t const max_inference_batch_size = this->width * this->height;
	::std::uint32_t const spatial_feature_count = 3u;
	::std::uint32_t const spatial_feature_stride = 4u;
	::std::uint32_t const hidden_width = 64u;
	::std::uint32_t const hidden_layer_count = 2u;
	float constexpr light_field_network_optimizer_learning_rate = 1e-3f;
	float constexpr learning_rate = 1e-2f;
	::framework::normalizing_flows::transform_type constexpr transform_type = ::framework::normalizing_flows::transform_type::piecewise_quadratic;
	::framework::gpu::component_type constexpr parameter_type = ::framework::gpu::component_type::float16;
	::framework::gpu::component_type constexpr pdf_type = ::framework::gpu::component_type::float16;
	::framework::gpu::component_type constexpr target_type = ::framework::gpu::component_type::float16;
	::framework::gpu::component_type constexpr accumulator_type = ::framework::gpu::component_type::float32;
	::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::size_t const target_type_size = ::framework::gpu::utility::get_component_type_size(target_type);
	float loss_scale;
	switch (parameter_type)
	{
	case ::framework::gpu::component_type::float16:
		loss_scale = 128.0f;
		break;
	case ::framework::gpu::component_type::float32:
		loss_scale = 1.0f;
		break;
	default:
		assert(!"Error: Unsupported prediction_type.");
		break;
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 512 * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->bottom_level_acceleration_structure_scratch_buffer));

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->bottom_level_acceleration_structure_scratch_buffer;

		::framework::gpu::memory_requirements memory_requirements;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->bottom_level_acceleration_structure_scratch_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->bottom_level_acceleration_structure_scratch_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->bottom_level_acceleration_structure_scratch_buffer;
		bind_buffer_memory_info.memory = this->bottom_level_acceleration_structure_scratch_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->bottom_level_acceleration_structure_scratch_buffer;
		this->bottom_level_acceleration_structure_scratch_device_or_host_address.device_address = this->gpu_context->get_device()->get_buffer_device_address(&buffer_device_address_info);
	}

	{
		this->indices_buffer_size = 512 * 1024 * 1024;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->indices_buffer_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit | ::framework::gpu::buffer_usage_flags::acceleration_structure_build_input_read_only_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->indices_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->indices_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->indices_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->indices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->indices_buffer;
		bind_buffer_memory_info.memory = this->indices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->indices_buffer;
		this->indices_device_address = this->gpu_context->get_device()->get_buffer_device_address(&buffer_device_address_info);
	}

	{
		this->vertices_buffer_size = 512 * 1024 * 1024;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->vertices_buffer_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit | ::framework::gpu::buffer_usage_flags::acceleration_structure_build_input_read_only_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().transfer;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->vertices_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->vertices_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->vertices_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->vertices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->vertices_buffer;
		bind_buffer_memory_info.memory = this->vertices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->vertices_buffer;
		this->vertices_device_address = this->gpu_context->get_device()->get_buffer_device_address(&buffer_device_address_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::framework::local::max_training_sample_pool_size * ::framework::local::sample_buffer_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->sample_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->sample_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->sample_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->sample_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->sample_buffer;
		bind_buffer_memory_info.memory = this->sample_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(::framework::local::max_batch_size, max_inference_batch_size) * spatial_feature_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->nis_spatial_features_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->nis_spatial_features_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->nis_spatial_features_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->nis_spatial_features_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->nis_spatial_features_buffer;
		bind_buffer_memory_info.memory = this->nis_spatial_features_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(::framework::local::max_batch_size, max_inference_batch_size) * light_field_one_blob_encode_features_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->light_field_one_blob_encode_features_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->light_field_one_blob_encode_features_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->light_field_one_blob_encode_features_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->light_field_one_blob_encode_features_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->light_field_one_blob_encode_features_buffer;
		bind_buffer_memory_info.memory = this->light_field_one_blob_encode_features_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::framework::local::max_batch_size * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->nis_target_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->nis_target_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->nis_target_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->nis_target_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->nis_target_buffer;
		bind_buffer_memory_info.memory = this->nis_target_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::framework::local::max_batch_size * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->cv_shape_target_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->cv_shape_target_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->cv_shape_target_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->cv_shape_target_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->cv_shape_target_buffer;
		bind_buffer_memory_info.memory = this->cv_shape_target_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = light_field_output_size * ::framework::local::max_batch_size * target_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->light_field_target_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->light_field_target_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->light_field_target_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->light_field_target_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->light_field_target_buffer;
		bind_buffer_memory_info.memory = this->light_field_target_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::framework::local::max_batch_size * sizeof(::std::uint16_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->nis_sample_pdf_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->nis_sample_pdf_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->nis_sample_pdf_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->nis_sample_pdf_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->nis_sample_pdf_buffer;
		bind_buffer_memory_info.memory = this->nis_sample_pdf_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->width * this->height * 512;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->path_state_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->path_state_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->path_state_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->path_state_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->path_state_buffer;
		bind_buffer_memory_info.memory = this->path_state_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->width * this->height * ::framework::local::max_path_length * 48;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->path_vertex_state_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->path_vertex_state_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->path_vertex_state_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->path_vertex_state_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->path_vertex_state_buffer;
		bind_buffer_memory_info.memory = this->path_vertex_state_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ((sizeof(::std::uint32_t) + this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1)) * 2;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->batch_size_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->batch_size_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->batch_size_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->batch_size_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->batch_size_buffer;
		bind_buffer_memory_info.memory = this->batch_size_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->shuffle_seed_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->shuffle_seed_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->shuffle_seed_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->shuffle_seed_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->shuffle_seed_buffer;
		bind_buffer_memory_info.memory = this->shuffle_seed_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::algorithm::scan::constant_buffer);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::uniform_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->scan_constant_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->scan_constant_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->scan_constant_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->scan_constant_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->scan_constant_buffer;
		bind_buffer_memory_info.memory = this->scan_constant_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->scan_index_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->scan_index_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->scan_index_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->scan_index_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->scan_index_buffer;
		bind_buffer_memory_info.memory = this->scan_index_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::std::uint32_t block_offset_size;
		this->scan_executor->get_memory_requirements(::framework::local::max_training_sample_pool_size, block_offset_size);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = block_offset_size * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->scan_thread_block_reduction_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->scan_thread_block_reduction_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->scan_thread_block_reduction_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->scan_thread_block_reduction_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->scan_thread_block_reduction_buffer;
		bind_buffer_memory_info.memory = this->scan_thread_block_reduction_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 4 * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->visualize_hit_info_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->visualize_hit_info_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->visualize_hit_info_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->visualize_hit_info_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->visualize_hit_info_buffer;
		bind_buffer_memory_info.memory = this->visualize_hit_info_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = light_field_encoded_input_size * ::std::max(::framework::local::max_batch_size, max_inference_batch_size) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->light_field_encoded_features_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->light_field_encoded_features_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->light_field_encoded_features_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->light_field_encoded_features_buffer;
		bind_buffer_memory_info.memory = this->light_field_encoded_features_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = light_field_encoded_input_size * ::framework::local::max_batch_size * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->light_field_dloss_dnetwork_input_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->light_field_dloss_dnetwork_input_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->light_field_dloss_dnetwork_input_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->light_field_dloss_dnetwork_input_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->light_field_dloss_dnetwork_input_buffer;
		bind_buffer_memory_info.memory = this->light_field_dloss_dnetwork_input_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		this->bottom_level_acceleration_structure_device_memory_size = 1024 * 1024 * 256;

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = this->bottom_level_acceleration_structure_device_memory_size;
		memory_allocate_info.allocation_alignment = 0;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), static_cast<::std::uint32_t>(-1), ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->bottom_level_acceleration_structure_device_memory));
	}

	{	
		this->ubo_alignment = (sizeof(::framework::graphics::ray_tracing::scene_renderer::path_tracer_parameters) + this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->ubo_alignment * this->frame_buffer_count;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->ubo));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->ubo;

		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->ubo : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->ubo_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->ubo;
		bind_buffer_memory_info.memory = this->ubo_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->gpu_context->get_device()->map_memory(this->ubo_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, &this->mapped_data));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment, this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size) * 3;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::shader_binding_table_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->raygen_shader_binding_table_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->raygen_shader_binding_table_buffer;
		this->gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->raygen_shader_binding_table_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->raygen_shader_binding_table_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->raygen_shader_binding_table_buffer;
		bind_buffer_memory_info.memory = this->raygen_shader_binding_table_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->raygen_shader_binding_table_buffer;
		this->raygen_shader_binding_table_device_address = this->gpu_context->get_device()->get_buffer_device_address(&buffer_device_address_info);
	}

	this->create_screen_buffers();

	this->material_system = ::std::make_unique<::framework::graphics::ray_tracing::material_system>(this);
	this->medium_system = ::std::make_unique<::framework::graphics::ray_tracing::medium_system>(this);
	this->spectrum_system = ::std::make_unique<::framework::graphics::ray_tracing::spectrum_system>(this);
	this->accumulate = ::std::make_unique<::framework::graphics::ray_tracing::accumulate>(this);
	this->aov = ::std::make_unique<::framework::graphics::ray_tracing::aov>(this);

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[22];
		// path_tracer_parameters
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// output_color
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// sample_color
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// nis_samples_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// nis_spatial_features_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// log_jacobian_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// nis_target_buffer
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 5;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// path_state_buffer
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 6;
		descriptor_set_layout_bindings[7].hlsl_register_space = 0;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;
		// visualize_hit_info_buffer
		descriptor_set_layout_bindings[8].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[8].binding = 8;
		descriptor_set_layout_bindings[8].hlsl_shader_register = 7;
		descriptor_set_layout_bindings[8].hlsl_register_space = 0;
		descriptor_set_layout_bindings[8].descriptor_count = 1;
		descriptor_set_layout_bindings[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[8].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[8].immutable_samplers = nullptr;
		// sample_buffer
		descriptor_set_layout_bindings[9].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[9].binding = 9;
		descriptor_set_layout_bindings[9].hlsl_shader_register = 8;
		descriptor_set_layout_bindings[9].hlsl_register_space = 0;
		descriptor_set_layout_bindings[9].descriptor_count = 1;
		descriptor_set_layout_bindings[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[9].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[9].immutable_samplers = nullptr;
		// batch_size_buffer
		descriptor_set_layout_bindings[10].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[10].binding = 10;
		descriptor_set_layout_bindings[10].hlsl_shader_register = 9;
		descriptor_set_layout_bindings[10].hlsl_register_space = 0;
		descriptor_set_layout_bindings[10].descriptor_count = 1;
		descriptor_set_layout_bindings[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[10].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[10].immutable_samplers = nullptr;
		// path_vertex_state_buffer
		descriptor_set_layout_bindings[11].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[11].binding = 11;
		descriptor_set_layout_bindings[11].hlsl_shader_register = 10;
		descriptor_set_layout_bindings[11].hlsl_register_space = 0;
		descriptor_set_layout_bindings[11].descriptor_count = 1;
		descriptor_set_layout_bindings[11].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[11].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[11].immutable_samplers = nullptr;
		// nis_features_buffer
		descriptor_set_layout_bindings[12].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[12].binding = 12;
		descriptor_set_layout_bindings[12].hlsl_shader_register = 11;
		descriptor_set_layout_bindings[12].hlsl_register_space = 0;
		descriptor_set_layout_bindings[12].descriptor_count = 1;
		descriptor_set_layout_bindings[12].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[12].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[12].immutable_samplers = nullptr;
		// inference_batch_size_buffer
		descriptor_set_layout_bindings[13].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[13].binding = 13;
		descriptor_set_layout_bindings[13].hlsl_shader_register = 12;
		descriptor_set_layout_bindings[13].hlsl_register_space = 0;
		descriptor_set_layout_bindings[13].descriptor_count = 1;
		descriptor_set_layout_bindings[13].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[13].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[13].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[14].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[14].binding = 14;
		descriptor_set_layout_bindings[14].hlsl_shader_register = 13;
		descriptor_set_layout_bindings[14].hlsl_register_space = 0;
		descriptor_set_layout_bindings[14].descriptor_count = 1;
		descriptor_set_layout_bindings[14].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[14].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[14].immutable_samplers = nullptr;
		// nis_pdf_image
		descriptor_set_layout_bindings[15].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[15].binding = 15;
		descriptor_set_layout_bindings[15].hlsl_shader_register = 14;
		descriptor_set_layout_bindings[15].hlsl_register_space = 0;
		descriptor_set_layout_bindings[15].descriptor_count = 1;
		descriptor_set_layout_bindings[15].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[15].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[15].immutable_samplers = nullptr;
		// ground_truth_distribution_image
		descriptor_set_layout_bindings[16].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[16].binding = 16;
		descriptor_set_layout_bindings[16].hlsl_shader_register = 15;
		descriptor_set_layout_bindings[16].hlsl_register_space = 0;
		descriptor_set_layout_bindings[16].descriptor_count = 1;
		descriptor_set_layout_bindings[16].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[16].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[16].immutable_samplers = nullptr;
		// neural_light_field_buffer
		descriptor_set_layout_bindings[17].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[17].binding = 17;
		descriptor_set_layout_bindings[17].hlsl_shader_register = 16;
		descriptor_set_layout_bindings[17].hlsl_register_space = 0;
		descriptor_set_layout_bindings[17].descriptor_count = 1;
		descriptor_set_layout_bindings[17].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[17].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[17].immutable_samplers = nullptr;
		// light_field_one_blob_encode_features_buffer
		descriptor_set_layout_bindings[18].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[18].binding = 18;
		descriptor_set_layout_bindings[18].hlsl_shader_register = 17;
		descriptor_set_layout_bindings[18].hlsl_register_space = 0;
		descriptor_set_layout_bindings[18].descriptor_count = 1;
		descriptor_set_layout_bindings[18].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[18].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[18].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_features_buffer
		descriptor_set_layout_bindings[19].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[19].binding = 19;
		descriptor_set_layout_bindings[19].hlsl_shader_register = 18;
		descriptor_set_layout_bindings[19].hlsl_register_space = 0;
		descriptor_set_layout_bindings[19].descriptor_count = 1;
		descriptor_set_layout_bindings[19].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[19].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[19].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_log_jacobian_buffer
		descriptor_set_layout_bindings[20].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[20].binding = 20;
		descriptor_set_layout_bindings[20].hlsl_shader_register = 19;
		descriptor_set_layout_bindings[20].hlsl_register_space = 0;
		descriptor_set_layout_bindings[20].descriptor_count = 1;
		descriptor_set_layout_bindings[20].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[20].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[20].immutable_samplers = nullptr;
		// cv_shape_normalizing_flow_x_buffer
		descriptor_set_layout_bindings[21].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[21].binding = 21;
		descriptor_set_layout_bindings[21].hlsl_shader_register = 20;
		descriptor_set_layout_bindings[21].hlsl_register_space = 0;
		descriptor_set_layout_bindings[21].descriptor_count = 1;
		descriptor_set_layout_bindings[21].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[21].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[21].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->path_tracer_descriptor_set_layout);
	}

	{
		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::local_bit;
		descriptor_set_layout_create_info.binding_count = 0;
		descriptor_set_layout_create_info.bindings = nullptr;
		this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->local_descriptor_set_layout);
	}

	{
		::framework::gpu::push_constant_range push_constant_ranges[1];
		push_constant_ranges[0].stage_flags = ::framework::gpu::shader_stage_flags::closest_hit_bit;
		push_constant_ranges[0].binding = 0;
		push_constant_ranges[0].offset = 0;
		push_constant_ranges[0].size = sizeof(::std::uint32_t) * 4;
		push_constant_ranges[0].hlsl_shader_register = 0;
		push_constant_ranges[0].hlsl_register_space = 8;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::local_bit;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->local_descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = static_cast<::std::uint32_t>(::std::size(push_constant_ranges));
		pipeline_layout_create_info.push_constant_ranges = push_constant_ranges;
		this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->local_pipeline_layout);
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[11];
		// uniform_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 1;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// raytracing_acceleration_structure
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 1;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::acceleration_structure;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// world_matrices
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 1;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// inverse_transpose_world_matrices
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 1;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// geometry_instances
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[4].hlsl_register_space = 1;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::miss_bit | ::framework::gpu::shader_stage_flags::closest_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// indices
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[5].hlsl_register_space = 1;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::miss_bit | ::framework::gpu::shader_stage_flags::closest_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// vertices
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 5;
		descriptor_set_layout_bindings[6].hlsl_register_space = 1;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::miss_bit | ::framework::gpu::shader_stage_flags::closest_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// lights
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 6;
		descriptor_set_layout_bindings[7].hlsl_register_space = 1;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;
		// infinite_lights
		descriptor_set_layout_bindings[8].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[8].binding = 8;
		descriptor_set_layout_bindings[8].hlsl_shader_register = 7;
		descriptor_set_layout_bindings[8].hlsl_register_space = 1;
		descriptor_set_layout_bindings[8].descriptor_count = 1;
		descriptor_set_layout_bindings[8].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[8].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[8].immutable_samplers = nullptr;
		// emissive_triangles
		descriptor_set_layout_bindings[9].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[9].binding = 9;
		descriptor_set_layout_bindings[9].hlsl_shader_register = 8;
		descriptor_set_layout_bindings[9].hlsl_register_space = 1;
		descriptor_set_layout_bindings[9].descriptor_count = 1;
		descriptor_set_layout_bindings[9].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[9].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[9].immutable_samplers = nullptr;
		// instance_offset_buffer
		descriptor_set_layout_bindings[10].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[10].binding = 10;
		descriptor_set_layout_bindings[10].hlsl_shader_register = 9;
		descriptor_set_layout_bindings[10].hlsl_register_space = 1;
		descriptor_set_layout_bindings[10].descriptor_count = 1;
		descriptor_set_layout_bindings[10].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[10].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit | ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[10].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->scene_descriptor_set_layout));
	}

	assert_framework_gpu_result(this->gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::none, 5, ::framework::gpu::shader_stage_flags::all, this->gpu_log_descriptor_set_layout));

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->path_tracer_descriptor_set_layout,
			this->scene_descriptor_set_layout,
			this->material_system->material_system_descriptor_set_layout,
			this->medium_system->medium_system_descriptor_set_layout,
			this->spectrum_system->descriptor_set_layout,
			this->gpu_log_descriptor_set_layout,
			this->aov->get_descriptor_set_layout()
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::framework::local::generate_forward_test_samples_push_constants);
		push_constant_ranges.hlsl_shader_register = 1;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->path_tracer_pipeline_layout));
	}

	this->create_pipelines();

	this->nis_normalizing_flow = ::std::make_unique<::framework::normalizing_flows::normalizing_flow>(this->gpu_context, this->resource_manager, &this->command_buffer_manager, this->profiler, this->gpu_log, this->device_descriptor_pool, transform_type, coupling_layer_count, spatial_feature_count, spatial_feature_stride, light_field_one_blob_encode_features_count, light_field_one_blob_encode_features_stride, hidden_width, hidden_layer_count, learning_rate, ::framework::local::max_batch_size, max_inference_batch_size, batch_alignment, inference_batch_alignment, parameter_type, pdf_type, target_type, accumulator_type, this->nis_spatial_features_buffer, this->light_field_one_blob_encode_features_buffer, /*this->nis_sample_pdf_buffer*/nullptr, this->nis_target_buffer, this->batch_size_buffer);
	this->cv_shape_normalizing_flow = ::std::make_unique<::framework::normalizing_flows::normalizing_flow>(this->gpu_context, this->resource_manager, &this->command_buffer_manager, this->profiler, this->gpu_log, this->device_descriptor_pool, transform_type, coupling_layer_count, spatial_feature_count, spatial_feature_stride, light_field_one_blob_encode_features_count, light_field_one_blob_encode_features_stride, hidden_width, hidden_layer_count, learning_rate, ::framework::local::max_batch_size, max_inference_batch_size, batch_alignment, inference_batch_alignment, parameter_type, pdf_type, target_type, accumulator_type, this->nis_spatial_features_buffer, this->light_field_one_blob_encode_features_buffer, /*this->nis_sample_pdf_buffer*/nullptr, this->cv_shape_target_buffer, this->batch_size_buffer);
	this->xavier_uniform = ::std::make_unique<::framework::algorithm::xavier_uniform>(this->gpu_context, this->resource_manager, ::framework::gpu::component_type::float32, parameter_type);
	this->light_field_grid_encoding_execution_policy = ::std::make_unique<::framework::nn::grid::execution_policy>(this->gpu_context, this->resource_manager, &this->command_buffer_manager, parameter_type, grid_gradient_type, grid_type, hash_type, interpolation_type, ::std::max(::framework::local::max_batch_size, max_inference_batch_size), feature_count, log2_hashmap_size, base_resolution, per_level_scale, stochastic_interpolation, features_per_level, light_field_spatial_feature_count, light_field_spatial_feature_stride);
	this->light_field_grid_encoding = ::std::make_unique<::framework::nn::grid::encoding>(this->light_field_grid_encoding_execution_policy.get(), this->gpu_context, this->resource_manager, parameter_type, grid_gradient_type);
	this->light_field_one_blob_encoding_execution_policy = ::std::make_unique<::framework::nn::one_blob::execution_policy>(this->gpu_context, this->resource_manager, &this->command_buffer_manager, parameter_type, light_field_one_blob_encoding_bins, light_field_one_blob_encode_features_count, ::framework::linalg::matrix_layout::column_major, light_field_neural_network_input_layout, 0, light_field_one_blob_encode_features_stride, feature_count, light_field_encoded_input_size);
	this->light_field_one_blob_encoding = ::std::make_unique<::framework::nn::one_blob::encoding>(this->light_field_one_blob_encoding_execution_policy.get());
	this->light_field_network_execution_policy = ::std::make_unique<::framework::nn::fully_fused_mlp::execution_policy>(this->gpu_context, this->resource_manager, this->gpu_log, parameter_type, accumulator_type, light_field_neural_network_input_layout, light_field_neural_network_output_layout, light_field_encoded_input_size, light_field_output_size, light_field_encoded_input_size, light_field_output_stride, light_field_hidden_width, light_field_hidden_layer_count, ::framework::nn::activation::relu, ::framework::nn::activation::exponential, batch_alignment, inference_batch_alignment, ::framework::local::max_batch_size, max_inference_batch_size, true);
	this->light_field_neural_network = ::std::make_unique<::framework::nn::fully_fused_mlp::network>(*this->light_field_network_execution_policy, this->gpu_context, this->resource_manager, gpu_log, parameter_type, accumulator_type, ::framework::local::max_batch_size, max_inference_batch_size, this->light_field_encoded_features_buffer, this->light_field_dloss_dnetwork_input_buffer, this->device_descriptor_pool);
	this->light_field_encoding_optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, this->resource_manager, parameter_type, grid_gradient_type, this->light_field_grid_encoding_execution_policy->get_parameter_count(), false, false, light_field_weight_clipping_magnitude, loss_scale, light_field_network_optimizer_learning_rate, 1e-07f);
	this->light_field_network_optimizer = ::std::make_unique<::framework::nn::adam>(this->gpu_context, this->resource_manager, parameter_type, parameter_type, this->light_field_network_execution_policy->get_parameter_count(), true, false, light_field_weight_clipping_magnitude, loss_scale, light_field_network_optimizer_learning_rate, 1e-07f);
	this->light_field_loss = ::std::make_unique<::framework::nn::relative_l2>(this->gpu_context, this->resource_manager, parameter_type, pdf_type, target_type, light_field_output_stride, light_field_output_size, loss_scale, false, false);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->get_light_field_one_blob_encoding_dispatch_indirect_command_offset() + this->light_field_one_blob_encoding_execution_policy->get_dispatch_indirect_command_size();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dispatch_indirect_command_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dispatch_indirect_command_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dispatch_indirect_command_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dispatch_indirect_command_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dispatch_indirect_command_buffer;
		bind_buffer_memory_info.memory = this->dispatch_indirect_command_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	::std::uint32_t const batch_size_offset = ::framework::numeric::align_up<::std::uint32_t>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[11];
		descriptor_set_layouts[0] = this->xavier_uniform->get_descriptor_set_layout();
		descriptor_set_layouts[1] = this->xavier_uniform->get_descriptor_set_layout();
		descriptor_set_layouts[2] = this->light_field_grid_encoding_execution_policy->get_descriptor_set_layout();
		descriptor_set_layouts[3] = this->light_field_grid_encoding_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[4] = this->light_field_one_blob_encoding_execution_policy->get_descriptor_set_layout();
		descriptor_set_layouts[5] = this->light_field_one_blob_encoding_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[6] = this->light_field_network_execution_policy->get_descriptor_set_layout();
		descriptor_set_layouts[7] = this->light_field_network_execution_policy->get_initialize_dispatch_indirect_command_descriptor_set_layout();
		descriptor_set_layouts[8] = this->light_field_encoding_optimizer->get_descriptor_set_layout();
		descriptor_set_layouts[9] = this->light_field_network_optimizer->get_descriptor_set_layout();
		descriptor_set_layouts[10] = this->light_field_loss->get_descriptor_set_layout();

		::framework::gpu::descriptor_set * descriptor_sets[::std::size(descriptor_set_layouts)];

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->device_descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));

		this->light_field_grid_encoding_initialize_parameters_descriptor_set = descriptor_sets[0];
		this->light_field_network_initialize_parameters_descriptor_set = descriptor_sets[1];
		this->light_field_grid_encoding_descriptor_set = descriptor_sets[2];
		this->light_field_grid_encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[3];
		this->light_field_one_blob_encoding_descriptor_set = descriptor_sets[4];
		this->light_field_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[5];
		this->light_field_network_descriptor_set = descriptor_sets[6];
		this->light_field_network_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[7];
		this->light_field_encoding_optimizer_descriptor_set = descriptor_sets[8];
		this->light_field_network_optimizer_descriptor_set = descriptor_sets[9];
		this->light_field_loss_descriptor_set = descriptor_sets[10];

		this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->light_field_grid_encoding->get_full_precision_parameter_buffer(), this->light_field_grid_encoding->get_parameter_buffer(), this->light_field_grid_encoding_initialize_parameters_descriptor_set);
		this->xavier_uniform->update_descriptor_sets(this->gpu_context, this->light_field_neural_network->get_full_precision_weights_matrices_buffer(), this->light_field_neural_network->get_weight_matrices_buffer(), this->light_field_network_initialize_parameters_descriptor_set);
		this->light_field_grid_encoding->update_descriptor_sets(this->gpu_context->get_device(), this->batch_size_buffer, this->nis_spatial_features_buffer, this->light_field_dloss_dnetwork_input_buffer, this->light_field_encoded_features_buffer, this->light_field_grid_encoding_descriptor_set);
		this->light_field_grid_encoding_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), this->batch_size_buffer, this->dispatch_indirect_command_buffer, this->light_field_grid_encoding_initialize_dispatch_indirect_command_descriptor_set);
		this->light_field_one_blob_encoding->update_descriptor_sets(this->gpu_context->get_device(), this->batch_size_buffer, this->light_field_one_blob_encode_features_buffer, this->light_field_dloss_dnetwork_input_buffer, this->light_field_encoded_features_buffer, this->light_field_one_blob_encoding_descriptor_set);
		this->light_field_one_blob_encoding_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context->get_device(), this->batch_size_buffer, this->dispatch_indirect_command_buffer, this->light_field_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set);
		this->light_field_neural_network->update_descriptor_sets(this->gpu_context, this->light_field_encoded_features_buffer, this->light_field_dloss_dnetwork_input_buffer, this->batch_size_buffer, this->light_field_network_descriptor_set);
		this->light_field_network_execution_policy->update_initialize_dispatch_indirect_command_descriptor_set(this->gpu_context, this->batch_size_buffer, this->light_field_network_initialize_dispatch_indirect_command_descriptor_set);
		this->light_field_encoding_optimizer->update_descriptor_sets(this->gpu_context, this->light_field_grid_encoding->get_full_precision_parameter_buffer(), this->light_field_grid_encoding->get_parameter_buffer(), this->light_field_grid_encoding->get_gradient_buffer(), this->light_field_encoding_optimizer_descriptor_set);
		this->light_field_network_optimizer->update_descriptor_sets(this->gpu_context, this->light_field_neural_network->get_full_precision_weights_matrices_buffer(), this->light_field_neural_network->get_weight_matrices_buffer(), this->light_field_neural_network->get_gradient_matrices_buffer(), this->light_field_network_optimizer_descriptor_set);
		this->light_field_loss->update_descriptor_sets(this->gpu_context, this->batch_size_buffer, batch_size_offset, this->light_field_neural_network->get_output_buffer(), this->nis_sample_pdf_buffer, this->light_field_target_buffer, this->light_field_target_buffer /* unused */, this->light_field_neural_network->get_dloss_doutput_buffer(), this->light_field_loss_descriptor_set);
	}

	::std::uint32_t descriptor_set_index = 0;
	::std::size_t const descriptor_set_count = 2 + ::std::size_t(!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors);
	::std::vector<::framework::gpu::descriptor_set *> descriptor_sets(descriptor_set_count);
	::std::vector<::framework::gpu::descriptor_set_layout *> descriptor_set_layouts(descriptor_set_count);
	descriptor_set_layouts[descriptor_set_index++] = this->path_tracer_descriptor_set_layout;
	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		descriptor_set_layouts[descriptor_set_index++] = this->scene_descriptor_set_layout;
	}
	descriptor_set_layouts[descriptor_set_index++] = this->gpu_log_descriptor_set_layout;

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->device_descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(descriptor_set_layouts.size());
	descriptor_set_allocate_info.set_layouts = descriptor_set_layouts.data();
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets.data()));

	descriptor_set_index = 0;
	this->path_tracer_descriptor_set = descriptor_sets[descriptor_set_index++];
	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->scene_descriptor_set = descriptor_sets[descriptor_set_index++];
	}
	this->gpu_log_descriptor_set = descriptor_sets[descriptor_set_index++];

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[18];
	descriptor_buffer_infos[0].buffer = this->ubo;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = this->ubo_alignment;
	descriptor_buffer_infos[0].stride = this->ubo_alignment;

	descriptor_buffer_infos[1].buffer = this->nis_normalizing_flow->get_x_buffer();
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 4;
	
	descriptor_buffer_infos[2].buffer = this->nis_spatial_features_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 4;
	
	descriptor_buffer_infos[3].buffer = this->nis_normalizing_flow->get_log_jacobian_buffer();
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = 4;

	descriptor_buffer_infos[4].buffer = this->nis_target_buffer;
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = 4;

	descriptor_buffer_infos[5].buffer = this->path_state_buffer;
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = 4;

	descriptor_buffer_infos[6].buffer = this->visualize_hit_info_buffer;
	descriptor_buffer_infos[6].offset = 0;
	descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[6].stride = 4;

	descriptor_buffer_infos[7].buffer = this->sample_buffer;
	descriptor_buffer_infos[7].offset = 0;
	descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[7].stride = 2;
	// batch_size_buffer
	descriptor_buffer_infos[8].buffer = this->batch_size_buffer;
	descriptor_buffer_infos[8].offset = batch_size_offset;
	descriptor_buffer_infos[8].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[8].stride = 2;

	descriptor_buffer_infos[9].buffer = this->path_vertex_state_buffer;
	descriptor_buffer_infos[9].offset = 0;
	descriptor_buffer_infos[9].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[9].stride = 2;

	descriptor_buffer_infos[10].buffer = this->nis_normalizing_flow->get_features_buffer();
	descriptor_buffer_infos[10].offset = 0;
	descriptor_buffer_infos[10].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[10].stride = 2;
	// inference_batch_size_buffer
	descriptor_buffer_infos[11].buffer = this->batch_size_buffer;
	descriptor_buffer_infos[11].offset = 0;
	descriptor_buffer_infos[11].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[11].stride = 2;
	// dispatch_indirect_command_buffer
	descriptor_buffer_infos[12].buffer = this->dispatch_indirect_command_buffer;
	descriptor_buffer_infos[12].offset = 0;
	descriptor_buffer_infos[12].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[12].stride = 2;
	// neural_light_field_buffer
	descriptor_buffer_infos[13].buffer = this->light_field_neural_network->get_output_buffer();
	descriptor_buffer_infos[13].offset = 0;
	descriptor_buffer_infos[13].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[13].stride = 2;
	// light_field_one_blob_encode_features_buffer
	descriptor_buffer_infos[14].buffer = this->light_field_one_blob_encode_features_buffer;
	descriptor_buffer_infos[14].offset = 0;
	descriptor_buffer_infos[14].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[14].stride = 2;
	// cv_shape_normalizing_flow_features_buffer
	descriptor_buffer_infos[15].buffer = this->cv_shape_normalizing_flow->get_features_buffer();
	descriptor_buffer_infos[15].offset = 0;
	descriptor_buffer_infos[15].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[15].stride = 2;
	// cv_shape_normalizing_flow_log_jacobian_buffer
	descriptor_buffer_infos[16].buffer = this->cv_shape_normalizing_flow->get_log_jacobian_buffer();
	descriptor_buffer_infos[16].offset = 0;
	descriptor_buffer_infos[16].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[16].stride = 2;
	// cv_shape_normalizing_flow_x_buffer
	descriptor_buffer_infos[17].buffer = this->cv_shape_normalizing_flow->get_x_buffer();
	descriptor_buffer_infos[17].offset = 0;
	descriptor_buffer_infos[17].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[17].stride = 2;

	::framework::gpu::descriptor_image_info descriptor_image_infos[4];
	descriptor_image_infos[0].sampler = nullptr;
	descriptor_image_infos[0].image_view = this->color_image_view;
	descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[1].sampler = nullptr;
	descriptor_image_infos[1].image_view = this->accumulator_image_view;
	descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[2].sampler = nullptr;
	descriptor_image_infos[2].image_view = this->nis_pdf_image_view;
	descriptor_image_infos[2].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[3].sampler = nullptr;
	descriptor_image_infos[3].image_view = this->ground_truth_distribution_image_view;
	descriptor_image_infos[3].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[22];
	// path_tracer_parameters
	write_descriptor_sets[0].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// output_color
	write_descriptor_sets[1].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[1].image_info = &descriptor_image_infos[0];
	write_descriptor_sets[1].buffer_info = nullptr;
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// sample_color
	write_descriptor_sets[2].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[2].image_info = &descriptor_image_infos[1];
	write_descriptor_sets[2].buffer_info = nullptr;
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;
	// nis_samples_buffer
	write_descriptor_sets[3].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[3].image_info = nullptr;
	write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[3].texel_buffer_view = nullptr;
	write_descriptor_sets[3].acceleration_structures = nullptr;
	// nis_spatial_features_buffer
	write_descriptor_sets[4].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[4].dst_binding = 4;
	write_descriptor_sets[4].dst_array_element = 0;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[4].image_info = nullptr;
	write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[4].texel_buffer_view = nullptr;
	write_descriptor_sets[4].acceleration_structures = nullptr;
	// log_jacobian_buffer
	write_descriptor_sets[5].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[5].dst_binding = 5;
	write_descriptor_sets[5].dst_array_element = 0;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[5].image_info = nullptr;
	write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[3];
	write_descriptor_sets[5].texel_buffer_view = nullptr;
	write_descriptor_sets[5].acceleration_structures = nullptr;
	// nis_target_buffer
	write_descriptor_sets[6].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[6].dst_binding = 6;
	write_descriptor_sets[6].dst_array_element = 0;
	write_descriptor_sets[6].descriptor_count = 1;
	write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[6].image_info = nullptr;
	write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[4];
	write_descriptor_sets[6].texel_buffer_view = nullptr;
	write_descriptor_sets[6].acceleration_structures = nullptr;
	// path_state_buffer
	write_descriptor_sets[7].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[7].dst_binding = 7;
	write_descriptor_sets[7].dst_array_element = 0;
	write_descriptor_sets[7].descriptor_count = 1;
	write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[7].image_info = nullptr;
	write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[5];
	write_descriptor_sets[7].texel_buffer_view = nullptr;
	write_descriptor_sets[7].acceleration_structures = nullptr;
	// visualize_hit_info_buffer
	write_descriptor_sets[8].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[8].dst_binding = 8;
	write_descriptor_sets[8].dst_array_element = 0;
	write_descriptor_sets[8].descriptor_count = 1;
	write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[8].image_info = nullptr;
	write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[6];
	write_descriptor_sets[8].texel_buffer_view = nullptr;
	write_descriptor_sets[8].acceleration_structures = nullptr;
	// sample_buffer
	write_descriptor_sets[9].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[9].dst_binding = 9;
	write_descriptor_sets[9].dst_array_element = 0;
	write_descriptor_sets[9].descriptor_count = 1;
	write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[9].image_info = nullptr;
	write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[7];
	write_descriptor_sets[9].texel_buffer_view = nullptr;
	write_descriptor_sets[9].acceleration_structures = nullptr;
	// batch_size_buffer
	write_descriptor_sets[10].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[10].dst_binding = 10;
	write_descriptor_sets[10].dst_array_element = 0;
	write_descriptor_sets[10].descriptor_count = 1;
	write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[10].image_info = nullptr;
	write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[8];
	write_descriptor_sets[10].texel_buffer_view = nullptr;
	write_descriptor_sets[10].acceleration_structures = nullptr;
	// path_vertex_state_buffer
	write_descriptor_sets[11].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[11].dst_binding = 11;
	write_descriptor_sets[11].dst_array_element = 0;
	write_descriptor_sets[11].descriptor_count = 1;
	write_descriptor_sets[11].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[11].image_info = nullptr;
	write_descriptor_sets[11].buffer_info = &descriptor_buffer_infos[9];
	write_descriptor_sets[11].texel_buffer_view = nullptr;
	write_descriptor_sets[11].acceleration_structures = nullptr;
	// nis_features_buffer
	write_descriptor_sets[12].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[12].dst_binding = 12;
	write_descriptor_sets[12].dst_array_element = 0;
	write_descriptor_sets[12].descriptor_count = 1;
	write_descriptor_sets[12].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[12].image_info = nullptr;
	write_descriptor_sets[12].buffer_info = &descriptor_buffer_infos[10];
	write_descriptor_sets[12].texel_buffer_view = nullptr;
	write_descriptor_sets[12].acceleration_structures = nullptr;
	// inference_batch_size_buffer
	write_descriptor_sets[13].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[13].dst_binding = 13;
	write_descriptor_sets[13].dst_array_element = 0;
	write_descriptor_sets[13].descriptor_count = 1;
	write_descriptor_sets[13].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[13].image_info = nullptr;
	write_descriptor_sets[13].buffer_info = &descriptor_buffer_infos[11];
	write_descriptor_sets[13].texel_buffer_view = nullptr;
	write_descriptor_sets[13].acceleration_structures = nullptr;
	// dispatch_indirect_command_buffer
	write_descriptor_sets[14].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[14].dst_binding = 14;
	write_descriptor_sets[14].dst_array_element = 0;
	write_descriptor_sets[14].descriptor_count = 1;
	write_descriptor_sets[14].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[14].image_info = nullptr;
	write_descriptor_sets[14].buffer_info = &descriptor_buffer_infos[12];
	write_descriptor_sets[14].texel_buffer_view = nullptr;
	write_descriptor_sets[14].acceleration_structures = nullptr;
	// nis_pdf_image
	write_descriptor_sets[15].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[15].dst_binding = 15;
	write_descriptor_sets[15].dst_array_element = 0;
	write_descriptor_sets[15].descriptor_count = 1;
	write_descriptor_sets[15].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[15].image_info = &descriptor_image_infos[2];
	write_descriptor_sets[15].buffer_info = nullptr;
	write_descriptor_sets[15].texel_buffer_view = nullptr;
	write_descriptor_sets[15].acceleration_structures = nullptr;
	// ground_truth_distribution_image
	write_descriptor_sets[16].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[16].dst_binding = 16;
	write_descriptor_sets[16].dst_array_element = 0;
	write_descriptor_sets[16].descriptor_count = 1;
	write_descriptor_sets[16].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[16].image_info = &descriptor_image_infos[3];
	write_descriptor_sets[16].buffer_info = nullptr;
	write_descriptor_sets[16].texel_buffer_view = nullptr;
	write_descriptor_sets[16].acceleration_structures = nullptr;
	// neural_light_field_buffer
	write_descriptor_sets[17].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[17].dst_binding = 17;
	write_descriptor_sets[17].dst_array_element = 0;
	write_descriptor_sets[17].descriptor_count = 1;
	write_descriptor_sets[17].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[17].image_info = nullptr;
	write_descriptor_sets[17].buffer_info = &descriptor_buffer_infos[13];
	write_descriptor_sets[17].texel_buffer_view = nullptr;
	write_descriptor_sets[17].acceleration_structures = nullptr;
	// light_field_one_blob_encode_features_buffer
	write_descriptor_sets[18].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[18].dst_binding = 18;
	write_descriptor_sets[18].dst_array_element = 0;
	write_descriptor_sets[18].descriptor_count = 1;
	write_descriptor_sets[18].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[18].image_info = nullptr;
	write_descriptor_sets[18].buffer_info = &descriptor_buffer_infos[14];
	write_descriptor_sets[18].texel_buffer_view = nullptr;
	write_descriptor_sets[18].acceleration_structures = nullptr;
	// cv_shape_normalizing_flow_features_buffer
	write_descriptor_sets[19].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[19].dst_binding = 19;
	write_descriptor_sets[19].dst_array_element = 0;
	write_descriptor_sets[19].descriptor_count = 1;
	write_descriptor_sets[19].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[19].image_info = nullptr;
	write_descriptor_sets[19].buffer_info = &descriptor_buffer_infos[15];
	write_descriptor_sets[19].texel_buffer_view = nullptr;
	write_descriptor_sets[19].acceleration_structures = nullptr;
	// cv_shape_normalizing_flow_log_jacobian_buffer
	write_descriptor_sets[20].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[20].dst_binding = 20;
	write_descriptor_sets[20].dst_array_element = 0;
	write_descriptor_sets[20].descriptor_count = 1;
	write_descriptor_sets[20].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[20].image_info = nullptr;
	write_descriptor_sets[20].buffer_info = &descriptor_buffer_infos[16];
	write_descriptor_sets[20].texel_buffer_view = nullptr;
	write_descriptor_sets[20].acceleration_structures = nullptr;
	// cv_shape_normalizing_flow_x_buffer
	write_descriptor_sets[21].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[21].dst_binding = 21;
	write_descriptor_sets[21].dst_array_element = 0;
	write_descriptor_sets[21].descriptor_count = 1;
	write_descriptor_sets[21].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[21].image_info = nullptr;
	write_descriptor_sets[21].buffer_info = &descriptor_buffer_infos[17];
	write_descriptor_sets[21].texel_buffer_view = nullptr;
	write_descriptor_sets[21].acceleration_structures = nullptr;

	this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	this->gpu_log->write_descriptor_set(this->gpu_log_descriptor_set);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_semaphore(&semaphore_create_info, nullptr, &this->frame_semaphore));

	{
		::framework::gpu::descriptor_set_layout * const descriptor_set_layouts[]
		{
			this->scan->get_descriptor_set_layout(),
			this->scan->get_initialize_dispatch_indirect_command_descriptor_set_layout(),
			this->shuffle_nis_samples_descriptor_set_layout
		};
		::framework::gpu::descriptor_set * descriptor_sets[3];

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = this->device_descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets));
		this->scan_descriptor_set = descriptor_sets[0], this->scan_initialize_dispatch_indirect_command_descriptor_set = descriptor_sets[1], this->shuffle_nis_samples_descriptor_set = descriptor_sets[2];
	}

	{
		::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[20];
		// constant_buffer
		descriptor_buffer_infos[0].buffer = this->scan_constant_buffer;
		descriptor_buffer_infos[0].offset = 0;
		descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[0].stride = 2;
		// index_buffer
		descriptor_buffer_infos[1].buffer = this->scan_index_buffer;
		descriptor_buffer_infos[1].offset = 0;
		descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[1].stride = 2;
		// thread_block_reduction_buffer
		descriptor_buffer_infos[2].buffer = this->scan_thread_block_reduction_buffer;
		descriptor_buffer_infos[2].offset = 0;
		descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[2].stride = 2;
		// size_buffer
		descriptor_buffer_infos[3].buffer = this->batch_size_buffer;
		descriptor_buffer_infos[3].offset = batch_size_offset;
		descriptor_buffer_infos[3].range = sizeof(::std::uint32_t);
		descriptor_buffer_infos[3].stride = 2;
		// seed_buffer
		descriptor_buffer_infos[4].buffer = this->shuffle_seed_buffer;
		descriptor_buffer_infos[4].offset = 0;
		descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[4].stride = 2;
		// sample_buffer
		descriptor_buffer_infos[5].buffer = this->sample_buffer;
		descriptor_buffer_infos[5].offset = 0;
		descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[5].stride = 2;
		// nis_samples_buffer
		descriptor_buffer_infos[6].buffer = this->nis_normalizing_flow->get_x_buffer();
		descriptor_buffer_infos[6].offset = 0;
		descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[6].stride = 2;
		// nis_spatial_features_buffer
		descriptor_buffer_infos[7].buffer = this->nis_spatial_features_buffer;
		descriptor_buffer_infos[7].offset = 0;
		descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[7].stride = 2;
		// nis_target_buffer
		descriptor_buffer_infos[8].buffer = this->nis_target_buffer;
		descriptor_buffer_infos[8].offset = 0;
		descriptor_buffer_infos[8].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[8].stride = 2;
		// size_buffer
		descriptor_buffer_infos[9].buffer = this->batch_size_buffer;
		descriptor_buffer_infos[9].offset = batch_size_offset;
		descriptor_buffer_infos[9].range = sizeof(::std::uint32_t);
		descriptor_buffer_infos[9].stride = 2;
		// dispatch_indirect_command_buffer
		descriptor_buffer_infos[10].buffer = this->dispatch_indirect_command_buffer;
		descriptor_buffer_infos[10].offset = 0;
		descriptor_buffer_infos[10].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[10].stride = 2;
		// constant_buffer
		descriptor_buffer_infos[11].buffer = this->scan_constant_buffer;
		descriptor_buffer_infos[11].offset = 0;
		descriptor_buffer_infos[11].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[11].stride = 2;
		// nis_features_buffer
		descriptor_buffer_infos[12].buffer = this->nis_normalizing_flow->get_features_buffer();
		descriptor_buffer_infos[12].offset = 0;
		descriptor_buffer_infos[12].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[12].stride = 2;
		// nis_sample_pdf_buffer
		descriptor_buffer_infos[13].buffer = this->nis_sample_pdf_buffer;
		descriptor_buffer_infos[13].offset = 0;
		descriptor_buffer_infos[13].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[13].stride = 2;
		// light_field_target_buffer
		descriptor_buffer_infos[14].buffer = this->light_field_target_buffer;
		descriptor_buffer_infos[14].offset = 0;
		descriptor_buffer_infos[14].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[14].stride = 2;
		// light_field_one_blob_encode_features_buffer
		descriptor_buffer_infos[15].buffer = this->light_field_one_blob_encode_features_buffer;
		descriptor_buffer_infos[15].offset = 0;
		descriptor_buffer_infos[15].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[15].stride = 2;
		// cv_shape_target_buffer
		descriptor_buffer_infos[16].buffer = this->cv_shape_target_buffer;
		descriptor_buffer_infos[16].offset = 0;
		descriptor_buffer_infos[16].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[16].stride = 2;
		// cv_shape_normalizing_flow_x_buffer
		descriptor_buffer_infos[17].buffer = this->cv_shape_normalizing_flow->get_x_buffer();
		descriptor_buffer_infos[17].offset = 0;
		descriptor_buffer_infos[17].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[17].stride = 2;
		// cv_shape_normalizing_flow_spatial_features_buffer
		descriptor_buffer_infos[18].buffer = this->nis_spatial_features_buffer;
		descriptor_buffer_infos[18].offset = 0;
		descriptor_buffer_infos[18].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[18].stride = 2;
		// cv_shape_normalizing_flow_features_buffer
		descriptor_buffer_infos[19].buffer = this->cv_shape_normalizing_flow->get_features_buffer();
		descriptor_buffer_infos[19].offset = 0;
		descriptor_buffer_infos[19].range = ::framework::gpu::whole_size;
		descriptor_buffer_infos[19].stride = 2;

		::framework::gpu::write_descriptor_set write_descriptor_sets[20];
		// constant_buffer
		write_descriptor_sets[0].dst_set = this->scan_descriptor_set;
		write_descriptor_sets[0].dst_binding = 0;
		write_descriptor_sets[0].dst_array_element = 0;
		write_descriptor_sets[0].descriptor_count = 1;
		write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[0].image_info = nullptr;
		write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
		write_descriptor_sets[0].texel_buffer_view = nullptr;
		write_descriptor_sets[0].acceleration_structures = nullptr;
		// index_buffer
		write_descriptor_sets[1].dst_set = this->scan_descriptor_set;
		write_descriptor_sets[1].dst_binding = 1;
		write_descriptor_sets[1].dst_array_element = 0;
		write_descriptor_sets[1].descriptor_count = 1;
		write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[1].image_info = nullptr;
		write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
		write_descriptor_sets[1].texel_buffer_view = nullptr;
		write_descriptor_sets[1].acceleration_structures = nullptr;
		// thread_block_reduction_buffer
		write_descriptor_sets[2].dst_set = this->scan_descriptor_set;
		write_descriptor_sets[2].dst_binding = 2;
		write_descriptor_sets[2].dst_array_element = 0;
		write_descriptor_sets[2].descriptor_count = 1;
		write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[2].image_info = nullptr;
		write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
		write_descriptor_sets[2].texel_buffer_view = nullptr;
		write_descriptor_sets[2].acceleration_structures = nullptr;
		// size_buffer
		write_descriptor_sets[3].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[3].dst_binding = 0;
		write_descriptor_sets[3].dst_array_element = 0;
		write_descriptor_sets[3].descriptor_count = 1;
		write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[3].image_info = nullptr;
		write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
		write_descriptor_sets[3].texel_buffer_view = nullptr;
		write_descriptor_sets[3].acceleration_structures = nullptr;
		// seed_buffer
		write_descriptor_sets[4].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[4].dst_binding = 1;
		write_descriptor_sets[4].dst_array_element = 0;
		write_descriptor_sets[4].descriptor_count = 1;
		write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer;
		write_descriptor_sets[4].image_info = nullptr;
		write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
		write_descriptor_sets[4].texel_buffer_view = nullptr;
		write_descriptor_sets[4].acceleration_structures = nullptr;
		// sample_buffer
		write_descriptor_sets[5].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[5].dst_binding = 2;
		write_descriptor_sets[5].dst_array_element = 0;
		write_descriptor_sets[5].descriptor_count = 1;
		write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[5].image_info = nullptr;
		write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
		write_descriptor_sets[5].texel_buffer_view = nullptr;
		write_descriptor_sets[5].acceleration_structures = nullptr;
		// nis_samples_buffer
		write_descriptor_sets[6].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[6].dst_binding = 3;
		write_descriptor_sets[6].dst_array_element = 0;
		write_descriptor_sets[6].descriptor_count = 1;
		write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[6].image_info = nullptr;
		write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[6];
		write_descriptor_sets[6].texel_buffer_view = nullptr;
		write_descriptor_sets[6].acceleration_structures = nullptr;
		// nis_spatial_features_buffer
		write_descriptor_sets[7].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[7].dst_binding = 4;
		write_descriptor_sets[7].dst_array_element = 0;
		write_descriptor_sets[7].descriptor_count = 1;
		write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[7].image_info = nullptr;
		write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[7];
		write_descriptor_sets[7].texel_buffer_view = nullptr;
		write_descriptor_sets[7].acceleration_structures = nullptr;
		// nis_target_buffer
		write_descriptor_sets[8].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[8].dst_binding = 6;
		write_descriptor_sets[8].dst_array_element = 0;
		write_descriptor_sets[8].descriptor_count = 1;
		write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[8].image_info = nullptr;
		write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[8];
		write_descriptor_sets[8].texel_buffer_view = nullptr;
		write_descriptor_sets[8].acceleration_structures = nullptr;
		// size_buffer
		write_descriptor_sets[9].dst_set = this->scan_initialize_dispatch_indirect_command_descriptor_set;
		write_descriptor_sets[9].dst_binding = 0;
		write_descriptor_sets[9].dst_array_element = 0;
		write_descriptor_sets[9].descriptor_count = 1;
		write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		write_descriptor_sets[9].image_info = nullptr;
		write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[9];
		write_descriptor_sets[9].texel_buffer_view = nullptr;
		write_descriptor_sets[9].acceleration_structures = nullptr;
		// dispatch_indirect_command_buffer
		write_descriptor_sets[10].dst_set = this->scan_initialize_dispatch_indirect_command_descriptor_set;
		write_descriptor_sets[10].dst_binding = 1;
		write_descriptor_sets[10].dst_array_element = 0;
		write_descriptor_sets[10].descriptor_count = 1;
		write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[10].image_info = nullptr;
		write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[10];
		write_descriptor_sets[10].texel_buffer_view = nullptr;
		write_descriptor_sets[10].acceleration_structures = nullptr;
		// constant_buffer
		write_descriptor_sets[11].dst_set = this->scan_initialize_dispatch_indirect_command_descriptor_set;
		write_descriptor_sets[11].dst_binding = 2;
		write_descriptor_sets[11].dst_array_element = 0;
		write_descriptor_sets[11].descriptor_count = 1;
		write_descriptor_sets[11].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[11].image_info = nullptr;
		write_descriptor_sets[11].buffer_info = &descriptor_buffer_infos[11];
		write_descriptor_sets[11].texel_buffer_view = nullptr;
		write_descriptor_sets[11].acceleration_structures = nullptr;
		// nis_features_buffer
		write_descriptor_sets[12].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[12].dst_binding = 5;
		write_descriptor_sets[12].dst_array_element = 0;
		write_descriptor_sets[12].descriptor_count = 1;
		write_descriptor_sets[12].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[12].image_info = nullptr;
		write_descriptor_sets[12].buffer_info = &descriptor_buffer_infos[12];
		write_descriptor_sets[12].texel_buffer_view = nullptr;
		write_descriptor_sets[12].acceleration_structures = nullptr;
		// nis_sample_pdf_buffer
		write_descriptor_sets[13].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[13].dst_binding = 7;
		write_descriptor_sets[13].dst_array_element = 0;
		write_descriptor_sets[13].descriptor_count = 1;
		write_descriptor_sets[13].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[13].image_info = nullptr;
		write_descriptor_sets[13].buffer_info = &descriptor_buffer_infos[13];
		write_descriptor_sets[13].texel_buffer_view = nullptr;
		write_descriptor_sets[13].acceleration_structures = nullptr;
		// light_field_target_buffer
		write_descriptor_sets[14].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[14].dst_binding = 8;
		write_descriptor_sets[14].dst_array_element = 0;
		write_descriptor_sets[14].descriptor_count = 1;
		write_descriptor_sets[14].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[14].image_info = nullptr;
		write_descriptor_sets[14].buffer_info = &descriptor_buffer_infos[14];
		write_descriptor_sets[14].texel_buffer_view = nullptr;
		write_descriptor_sets[14].acceleration_structures = nullptr;
		// light_field_one_blob_encode_features_buffer
		write_descriptor_sets[15].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[15].dst_binding = 9;
		write_descriptor_sets[15].dst_array_element = 0;
		write_descriptor_sets[15].descriptor_count = 1;
		write_descriptor_sets[15].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[15].image_info = nullptr;
		write_descriptor_sets[15].buffer_info = &descriptor_buffer_infos[15];
		write_descriptor_sets[15].texel_buffer_view = nullptr;
		write_descriptor_sets[15].acceleration_structures = nullptr;
		// cv_shape_target_buffer
		write_descriptor_sets[16].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[16].dst_binding = 10;
		write_descriptor_sets[16].dst_array_element = 0;
		write_descriptor_sets[16].descriptor_count = 1;
		write_descriptor_sets[16].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[16].image_info = nullptr;
		write_descriptor_sets[16].buffer_info = &descriptor_buffer_infos[16];
		write_descriptor_sets[16].texel_buffer_view = nullptr;
		write_descriptor_sets[16].acceleration_structures = nullptr;
		// cv_shape_normalizing_flow_x_buffer
		write_descriptor_sets[17].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[17].dst_binding = 11;
		write_descriptor_sets[17].dst_array_element = 0;
		write_descriptor_sets[17].descriptor_count = 1;
		write_descriptor_sets[17].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[17].image_info = nullptr;
		write_descriptor_sets[17].buffer_info = &descriptor_buffer_infos[17];
		write_descriptor_sets[17].texel_buffer_view = nullptr;
		write_descriptor_sets[17].acceleration_structures = nullptr;
		// cv_shape_normalizing_flow_spatial_features_buffer
		write_descriptor_sets[18].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[18].dst_binding = 12;
		write_descriptor_sets[18].dst_array_element = 0;
		write_descriptor_sets[18].descriptor_count = 1;
		write_descriptor_sets[18].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[18].image_info = nullptr;
		write_descriptor_sets[18].buffer_info = &descriptor_buffer_infos[18];
		write_descriptor_sets[18].texel_buffer_view = nullptr;
		write_descriptor_sets[18].acceleration_structures = nullptr;
		// cv_shape_normalizing_flow_features_buffer
		write_descriptor_sets[19].dst_set = this->shuffle_nis_samples_descriptor_set;
		write_descriptor_sets[19].dst_binding = 13;
		write_descriptor_sets[19].dst_array_element = 0;
		write_descriptor_sets[19].descriptor_count = 1;
		write_descriptor_sets[19].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		write_descriptor_sets[19].image_info = nullptr;
		write_descriptor_sets[19].buffer_info = &descriptor_buffer_infos[19];
		write_descriptor_sets[19].texel_buffer_view = nullptr;
		write_descriptor_sets[19].acceleration_structures = nullptr;

		this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
	}
}

::framework::graphics::ray_tracing::scene_renderer::~scene_renderer()
{
	this->gpu_context->get_device()->wait_idle();
	this->gpu_context->get_device()->destroy_pipeline(this->path_tracer_pipeline, nullptr);
	this->gpu_context->get_device()->destroy_pipeline(this->spatial_resampling_pipeline, nullptr);
	this->gpu_context->get_device()->destroy_descriptor_set_layout(this->scene_descriptor_set_layout, nullptr);
	this->gpu_context->get_device()->destroy_descriptor_set_layout(this->path_tracer_descriptor_set_layout, nullptr);
	this->gpu_context->get_device()->destroy_descriptor_set_layout(this->local_descriptor_set_layout, nullptr);
	this->gpu_context->get_device()->destroy_pipeline_layout(this->path_tracer_pipeline_layout, nullptr);
	this->gpu_context->get_device()->destroy_pipeline_layout(this->local_pipeline_layout, nullptr);
	this->gpu_context->get_device()->destroy_descriptor_pool(this->device_descriptor_pool, nullptr);
	if (!this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
		this->gpu_context->get_device()->destroy_descriptor_pool(this->host_descriptor_pool, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->path_tracer_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->miss_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->closest_hit_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->volume_closest_hit_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->volume_intersection_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->spatial_resampling_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->generate_forward_test_samples_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->raygen_shader_binding_table_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->bottom_level_acceleration_structure_scratch_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->indices_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->vertices_buffer, nullptr);
	this->gpu_context->get_device()->destroy_buffer(this->ubo, nullptr);
	this->gpu_context->get_device()->free_memory(this->raygen_shader_binding_table_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->bottom_level_acceleration_structure_scratch_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->indices_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->vertices_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->bottom_level_acceleration_structure_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->ubo_device_memory, nullptr);
	this->gpu_context->get_device()->free_memory(this->accumulator_device_memory, nullptr);
	this->gpu_context->get_device()->destroy_semaphore(this->frame_semaphore, nullptr);
}

void ::framework::graphics::ray_tracing::scene_renderer::create_pipelines()
{
	{
		::framework::resource::preprocessor_define preprocessor_defines[]
		{
			{ this->aov->get_aov_preprocessor_define(::framework::graphics::ray_tracing::aov::type::geometry_instance_id), "1"},
			{ "GPU_LOG_DESCRIPTOR_SET_INDEX", "5" },
			{ "GPU_LOG_REGISTER_SPACE", "space5" }
		};

		::framework::resource::type_conformance type_conformances[]
		{
			{ "framework::standard_material", "framework::material", 0 },
			{ "framework::homogeneous_medium", "framework::medium", 0 }
		};

		::framework::resource::preprocessor_define ground_truth_visualization_preprocessor_defines[]
		{
			 // do not output AOV in ground truth distribution visualization mode
			{ "GPU_LOG_DESCRIPTOR_SET_INDEX", "5" },
			{ "GPU_LOG_REGISTER_SPACE", "space5" }
		};

		::framework::resource::slang_source_info slang_source_infos[9];
		slang_source_infos[0].file_name = "ray_tracing/miss.rmiss.slang";
		slang_source_infos[1].file_name = "ray_tracing/closesthit.rchit.slang";
		slang_source_infos[2].file_name = "ray_tracing/subsurface_scattering.rahit.slang";
		slang_source_infos[3].file_name = "ray_tracing/volume.rchit.slang";
		slang_source_infos[4].file_name = "ray_tracing/volume.rint.slang";
		slang_source_infos[5].file_name = "ray_tracing/subsurface_scattering.rmiss.slang";
		slang_source_infos[6].file_name = "ray_tracing/nis_path_integrator/trace_primary_rays.rgen.slang";
		slang_source_infos[7].file_name = "ray_tracing/nis_path_integrator/next_vertex.rgen.slang";
		slang_source_infos[8].file_name = "ray_tracing/raygen.rgen.slang";

		::framework::resource::shader_stage_info shader_stage_infos[9];

		shader_stage_infos[0].hlsl_source_info = nullptr;
		shader_stage_infos[0].glsl_source_info = nullptr;
		shader_stage_infos[0].slang_source_info = &slang_source_infos[0];
		shader_stage_infos[0].preprocessor_define_count = 0;
		shader_stage_infos[0].preprocessor_defines = nullptr;
		shader_stage_infos[0].type_conformance_count = 0;
		shader_stage_infos[0].type_conformances = nullptr;
		shader_stage_infos[0].include_override_count = 0;
		shader_stage_infos[0].include_overrides = nullptr;
		shader_stage_infos[0].entry_point = "main";
		shader_stage_infos[0].stage = ::framework::gpu::shader_stage_flags::miss_bit;

		shader_stage_infos[1].hlsl_source_info = nullptr;
		shader_stage_infos[1].glsl_source_info = nullptr;
		shader_stage_infos[1].slang_source_info = &slang_source_infos[1];
		shader_stage_infos[1].preprocessor_define_count = 0;
		shader_stage_infos[1].preprocessor_defines = nullptr;
		shader_stage_infos[1].type_conformance_count = 0;
		shader_stage_infos[1].type_conformances = nullptr;
		shader_stage_infos[1].include_override_count = 0;
		shader_stage_infos[1].include_overrides = nullptr;
		shader_stage_infos[1].entry_point = "main";
		shader_stage_infos[1].stage = ::framework::gpu::shader_stage_flags::closest_hit_bit;

		shader_stage_infos[2].hlsl_source_info = nullptr;
		shader_stage_infos[2].glsl_source_info = nullptr;
		shader_stage_infos[2].slang_source_info = &slang_source_infos[2];
		shader_stage_infos[2].preprocessor_define_count = 0;
		shader_stage_infos[2].preprocessor_defines = nullptr;
		shader_stage_infos[2].type_conformance_count = static_cast<::std::uint32_t>(::std::size(type_conformances));
		shader_stage_infos[2].type_conformances = type_conformances;
		shader_stage_infos[2].include_override_count = 0;
		shader_stage_infos[2].include_overrides = nullptr;
		shader_stage_infos[2].entry_point = "main";
		shader_stage_infos[2].stage = ::framework::gpu::shader_stage_flags::any_hit_bit;

		shader_stage_infos[3].hlsl_source_info = nullptr;
		shader_stage_infos[3].glsl_source_info = nullptr;
		shader_stage_infos[3].slang_source_info = &slang_source_infos[3];
		shader_stage_infos[3].preprocessor_define_count = 0;
		shader_stage_infos[3].preprocessor_defines = nullptr;
		shader_stage_infos[3].type_conformance_count = 0;
		shader_stage_infos[3].type_conformances = nullptr;
		shader_stage_infos[3].include_override_count = 0;
		shader_stage_infos[3].include_overrides = nullptr;
		shader_stage_infos[3].entry_point = "main";
		shader_stage_infos[3].stage = ::framework::gpu::shader_stage_flags::closest_hit_bit;

		shader_stage_infos[4].hlsl_source_info = nullptr;
		shader_stage_infos[4].glsl_source_info = nullptr;
		shader_stage_infos[4].slang_source_info = &slang_source_infos[4];
		shader_stage_infos[4].preprocessor_define_count = 0;
		shader_stage_infos[4].preprocessor_defines = nullptr;
		shader_stage_infos[4].type_conformance_count = 0;
		shader_stage_infos[4].type_conformances = nullptr;
		shader_stage_infos[4].include_override_count = 0;
		shader_stage_infos[4].include_overrides = nullptr;
		shader_stage_infos[4].entry_point = "main";
		shader_stage_infos[4].stage = ::framework::gpu::shader_stage_flags::intersection_bit;

		shader_stage_infos[5].hlsl_source_info = nullptr;
		shader_stage_infos[5].glsl_source_info = nullptr;
		shader_stage_infos[5].slang_source_info = &slang_source_infos[5];
		shader_stage_infos[5].preprocessor_define_count = 0;
		shader_stage_infos[5].preprocessor_defines = nullptr;
		shader_stage_infos[5].type_conformance_count = 0;
		shader_stage_infos[5].type_conformances = nullptr;
		shader_stage_infos[5].include_override_count = 0;
		shader_stage_infos[5].include_overrides = nullptr;
		shader_stage_infos[5].entry_point = "main";
		shader_stage_infos[5].stage = ::framework::gpu::shader_stage_flags::miss_bit;

		shader_stage_infos[6].hlsl_source_info = nullptr;
		shader_stage_infos[6].glsl_source_info = nullptr;
		shader_stage_infos[6].slang_source_info = &slang_source_infos[6];
		shader_stage_infos[6].preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_infos[6].preprocessor_defines = preprocessor_defines;
		shader_stage_infos[6].type_conformance_count = static_cast<::std::uint32_t>(::std::size(type_conformances));
		shader_stage_infos[6].type_conformances = type_conformances;
		shader_stage_infos[6].include_override_count = 0;
		shader_stage_infos[6].include_overrides = nullptr;
		shader_stage_infos[6].entry_point = "main";
		shader_stage_infos[6].stage = ::framework::gpu::shader_stage_flags::raygen_bit;

		shader_stage_infos[7].hlsl_source_info = nullptr;
		shader_stage_infos[7].glsl_source_info = nullptr;
		shader_stage_infos[7].slang_source_info = &slang_source_infos[7];
		shader_stage_infos[7].preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_infos[7].preprocessor_defines = preprocessor_defines;
		shader_stage_infos[7].type_conformance_count = static_cast<::std::uint32_t>(::std::size(type_conformances));
		shader_stage_infos[7].type_conformances = type_conformances;
		shader_stage_infos[7].include_override_count = 0;
		shader_stage_infos[7].include_overrides = nullptr;
		shader_stage_infos[7].entry_point = "main";
		shader_stage_infos[7].stage = ::framework::gpu::shader_stage_flags::raygen_bit;

		shader_stage_infos[8].hlsl_source_info = nullptr;
		shader_stage_infos[8].glsl_source_info = nullptr;
		shader_stage_infos[8].slang_source_info = &slang_source_infos[8];
		shader_stage_infos[8].preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(ground_truth_visualization_preprocessor_defines));
		shader_stage_infos[8].preprocessor_defines = ground_truth_visualization_preprocessor_defines;
		shader_stage_infos[8].type_conformance_count = static_cast<::std::uint32_t>(::std::size(type_conformances));
		shader_stage_infos[8].type_conformances = type_conformances;
		shader_stage_infos[8].include_override_count = 0;
		shader_stage_infos[8].include_overrides = nullptr;
		shader_stage_infos[8].entry_point = "main";
		shader_stage_infos[8].stage = ::framework::gpu::shader_stage_flags::raygen_bit;
	
		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;

		shader_module_info.shader_stage_infos = &shader_stage_infos[0];
		this->miss_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[1];
		this->closest_hit_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[2];
		this->subsurface_scattering_any_hit_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[3];
		this->volume_closest_hit_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[4];
		this->volume_intersection_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[5];
		this->subsurface_scattering_miss_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[6];
		this->trace_primary_rays_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[7];
		this->next_vertex_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);

		shader_module_info.shader_stage_infos = &shader_stage_infos[8];
		this->path_tracer_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	if (false)
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "ray_tracing/nis_path_integrator/spatial_resampling.comp.hlsl";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->spatial_resampling_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}
	else
	{
		this->spatial_resampling_shader_module = nullptr;
	}

	{
		::framework::resource::slang_source_info slang_source_info;
		slang_source_info.file_name = "ray_tracing/nis_path_integrator/generate_forward_test_samples.comp.slang";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = &slang_source_info;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->generate_forward_test_samples_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::slang_source_info slang_source_info;
		slang_source_info.file_name = "ray_tracing/nis_path_integrator/plot_pdf.comp.slang";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = &slang_source_info;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->plot_pdf_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "ray_tracing/nis_path_integrator/align_batch_size.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->align_batch_size_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "graphics/ray_tracing/concatenate_features.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->concatenate_features_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "graphics/ray_tracing/cv_shape_concatenate_features.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->cv_shape_concatenate_features_shader_module = this->resource_manager->load_shader_module(this->gpu_context->get_device(), shader_module_info);
	}

	::std::uint32_t const shader_index_miss = 0;
	::std::uint32_t const shader_index_closest_hit = 1;
	::std::uint32_t const subsurface_scattering_any_hit = 2;
	::std::uint32_t const shader_index_volume_closest_hit = 3;
	::std::uint32_t const shader_index_volume_intersection = 4;
	::std::uint32_t const subsurface_scattering_miss = 5;
	::std::uint32_t const trace_primary_rays_shader_index = 6;
	::std::uint32_t const next_vertex_shader_index = 7;
	::std::uint32_t const path_tracer_shader_index = 8;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[9];
	pipeline_shader_stage_create_infos[shader_index_miss].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[shader_index_miss].stage = ::framework::gpu::shader_stage_flags::miss_bit;
	pipeline_shader_stage_create_infos[shader_index_miss].module = this->miss_shader_module;
	pipeline_shader_stage_create_infos[shader_index_miss].name = "main";
	pipeline_shader_stage_create_infos[shader_index_miss].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[shader_index_closest_hit].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[shader_index_closest_hit].stage = ::framework::gpu::shader_stage_flags::closest_hit_bit;
	pipeline_shader_stage_create_infos[shader_index_closest_hit].module = this->closest_hit_shader_module;
	pipeline_shader_stage_create_infos[shader_index_closest_hit].name = "main";
	pipeline_shader_stage_create_infos[shader_index_closest_hit].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[subsurface_scattering_any_hit].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[subsurface_scattering_any_hit].stage = ::framework::gpu::shader_stage_flags::any_hit_bit;
	pipeline_shader_stage_create_infos[subsurface_scattering_any_hit].module = this->subsurface_scattering_any_hit_shader_module;
	pipeline_shader_stage_create_infos[subsurface_scattering_any_hit].name = "main";
	pipeline_shader_stage_create_infos[subsurface_scattering_any_hit].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[shader_index_volume_closest_hit].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[shader_index_volume_closest_hit].stage = ::framework::gpu::shader_stage_flags::closest_hit_bit;
	pipeline_shader_stage_create_infos[shader_index_volume_closest_hit].module = this->volume_closest_hit_shader_module;
	pipeline_shader_stage_create_infos[shader_index_volume_closest_hit].name = "main";
	pipeline_shader_stage_create_infos[shader_index_volume_closest_hit].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[shader_index_volume_intersection].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[shader_index_volume_intersection].stage = ::framework::gpu::shader_stage_flags::intersection_bit;
	pipeline_shader_stage_create_infos[shader_index_volume_intersection].module = this->volume_intersection_shader_module;
	pipeline_shader_stage_create_infos[shader_index_volume_intersection].name = "main";
	pipeline_shader_stage_create_infos[shader_index_volume_intersection].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[subsurface_scattering_miss].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[subsurface_scattering_miss].stage = ::framework::gpu::shader_stage_flags::miss_bit;
	pipeline_shader_stage_create_infos[subsurface_scattering_miss].module = this->subsurface_scattering_miss_shader_module;
	pipeline_shader_stage_create_infos[subsurface_scattering_miss].name = "main";
	pipeline_shader_stage_create_infos[subsurface_scattering_miss].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[trace_primary_rays_shader_index].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[trace_primary_rays_shader_index].stage = ::framework::gpu::shader_stage_flags::raygen_bit;
	pipeline_shader_stage_create_infos[trace_primary_rays_shader_index].module = this->trace_primary_rays_shader_module;
	pipeline_shader_stage_create_infos[trace_primary_rays_shader_index].name = "main";
	pipeline_shader_stage_create_infos[trace_primary_rays_shader_index].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[next_vertex_shader_index].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[next_vertex_shader_index].stage = ::framework::gpu::shader_stage_flags::raygen_bit;
	pipeline_shader_stage_create_infos[next_vertex_shader_index].module = this->next_vertex_shader_module;
	pipeline_shader_stage_create_infos[next_vertex_shader_index].name = "main";
	pipeline_shader_stage_create_infos[next_vertex_shader_index].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[path_tracer_shader_index].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[path_tracer_shader_index].stage = ::framework::gpu::shader_stage_flags::raygen_bit;
	pipeline_shader_stage_create_infos[path_tracer_shader_index].module = this->path_tracer_shader_module;
	pipeline_shader_stage_create_infos[path_tracer_shader_index].name = "main";
	pipeline_shader_stage_create_infos[path_tracer_shader_index].specialization_info = nullptr;

	::framework::gpu::ray_tracing_shader_group_create_info ray_tracing_shader_group_create_infos[8];
	ray_tracing_shader_group_create_infos[0].type = ::framework::gpu::ray_tracing_shader_group_type::general;
	ray_tracing_shader_group_create_infos[0].general_shader = shader_index_miss;
	ray_tracing_shader_group_create_infos[0].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[0].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[0].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[0].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[1].type = ::framework::gpu::ray_tracing_shader_group_type::general;
	ray_tracing_shader_group_create_infos[1].general_shader = subsurface_scattering_miss;
	ray_tracing_shader_group_create_infos[1].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[1].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[1].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[1].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[2].type = ::framework::gpu::ray_tracing_shader_group_type::triangles_hit_group;
	ray_tracing_shader_group_create_infos[2].general_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[2].closest_hit_shader = shader_index_closest_hit;
	ray_tracing_shader_group_create_infos[2].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[2].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[2].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[3].type = ::framework::gpu::ray_tracing_shader_group_type::triangles_hit_group;
	ray_tracing_shader_group_create_infos[3].general_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[3].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[3].any_hit_shader = subsurface_scattering_any_hit;
	ray_tracing_shader_group_create_infos[3].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[3].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[4].type = ::framework::gpu::ray_tracing_shader_group_type::procedural_hit_group;
	ray_tracing_shader_group_create_infos[4].general_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[4].closest_hit_shader = shader_index_volume_closest_hit;
	ray_tracing_shader_group_create_infos[4].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[4].intersection_shader = shader_index_volume_intersection;
	ray_tracing_shader_group_create_infos[4].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[5].type = ::framework::gpu::ray_tracing_shader_group_type::general;
	ray_tracing_shader_group_create_infos[5].general_shader = trace_primary_rays_shader_index;
	ray_tracing_shader_group_create_infos[5].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[5].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[5].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[5].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[6].type = ::framework::gpu::ray_tracing_shader_group_type::general;
	ray_tracing_shader_group_create_infos[6].general_shader = next_vertex_shader_index;
	ray_tracing_shader_group_create_infos[6].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[6].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[6].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[6].shader_group_capture_replay_handle = nullptr;

	ray_tracing_shader_group_create_infos[7].type = ::framework::gpu::ray_tracing_shader_group_type::general;
	ray_tracing_shader_group_create_infos[7].general_shader = path_tracer_shader_index;
	ray_tracing_shader_group_create_infos[7].closest_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[7].any_hit_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[7].intersection_shader = ::framework::gpu::shader_unused;
	ray_tracing_shader_group_create_infos[7].shader_group_capture_replay_handle = nullptr;

	::framework::gpu::ray_tracing_local_layout_shader_association ray_tracing_local_layout_shader_association;
	ray_tracing_local_layout_shader_association.group = 2;
	ray_tracing_local_layout_shader_association.layout = 0;

	::framework::gpu::ray_tracing_pipeline_create_info ray_tracing_pipeline_create_info;
	ray_tracing_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	ray_tracing_pipeline_create_info.stage_count = static_cast<::std::uint32_t>(::std::size(pipeline_shader_stage_create_infos));
	ray_tracing_pipeline_create_info.stages = ::std::data(pipeline_shader_stage_create_infos);
	ray_tracing_pipeline_create_info.group_count = static_cast<::std::uint32_t>(::std::size(ray_tracing_shader_group_create_infos));
	ray_tracing_pipeline_create_info.groups = ::std::data(ray_tracing_shader_group_create_infos);
	ray_tracing_pipeline_create_info.max_pipeline_ray_recursion_depth = 1;
	ray_tracing_pipeline_create_info.max_pipeline_ray_payload_size = 128;
	ray_tracing_pipeline_create_info.max_pipeline_ray_hit_attribute_size = 32;
	ray_tracing_pipeline_create_info.library_info = nullptr;
	ray_tracing_pipeline_create_info.dynamic_state = nullptr;
	ray_tracing_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
	ray_tracing_pipeline_create_info.local_layout_count = 1;
	ray_tracing_pipeline_create_info.local_layouts = &this->local_pipeline_layout;
	ray_tracing_pipeline_create_info.association_count = 1;
	ray_tracing_pipeline_create_info.associations = &ray_tracing_local_layout_shader_association;
	ray_tracing_pipeline_create_info.base_pipeline = nullptr;
	ray_tracing_pipeline_create_info.base_pipeline_index = -1;
	assert_framework_gpu_result(this->gpu_context->get_device()->create_ray_tracing_pipelines(nullptr, nullptr, 1, &ray_tracing_pipeline_create_info, nullptr, &this->path_tracer_pipeline));

	if (this->spatial_resampling_shader_module)
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->spatial_resampling_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->spatial_resampling_pipeline);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->generate_forward_test_samples_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->generate_forward_test_samples_pipeline);
	}
	
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->plot_pdf_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->plot_pdf_pipeline);
	}
	
	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->align_batch_size_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->align_batch_size_pipeline);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->concatenate_features_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->concatenate_features_pipeline));
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->cv_shape_concatenate_features_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->path_tracer_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->cv_shape_concatenate_features_pipeline));
	}

	// fill raygen shader binding table
	::std::uint32_t const group_count = 3;
	::std::uint32_t const shader_binding_table_size = group_count * this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size;
	::std::uint32_t const shader_binding_table_stride = ::std::max(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment, this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
	::std::vector<::std::byte> shader_binding_table_data(shader_binding_table_size);
	assert_framework_gpu_result(this->gpu_context->get_device()->get_ray_tracing_shader_group_handles(this->path_tracer_pipeline, 5, group_count, shader_binding_table_size, shader_binding_table_data.data()));

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		::framework::command_context * const graphics_command_context = co_await this->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));
		for (::std::uint32_t i = 0; i < group_count; i++)
		{
			co_await graphics_command_context->update_buffer(this->raygen_shader_binding_table_buffer, shader_binding_table_data.data() + i * this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size, i * shader_binding_table_stride, this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
		}

		assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
		co_await this->get_command_buffer_manager().get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await this->get_command_buffer_manager().get_graphics_queue()->release_command_context(graphics_command_context);
	}());
}

void ::framework::graphics::ray_tracing::scene_renderer::create_screen_buffers()
{
	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		image_create_info.extent.width = this->width;
		image_create_info.extent.height = this->height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::sampled_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->color_image));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->color_image;
		debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::scene_renderer::color_image";
		assert_framework_gpu_result(this->gpu_context->get_device()->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->color_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->color_image : nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->color_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->color_image;
		bind_image_memory_info.memory = this->color_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->color_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->color_image_view));
	}
	
	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r16g16b16a16_sfloat;
		image_create_info.extent.width = this->width;
		image_create_info.extent.height = this->height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::sampled_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->nis_pdf_image));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->nis_pdf_image;
		debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::scene_renderer::nis_pdf_image";
		assert_framework_gpu_result(this->gpu_context->get_device()->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->nis_pdf_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->nis_pdf_image : nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->nis_pdf_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->nis_pdf_image;
		bind_image_memory_info.memory = this->nis_pdf_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->nis_pdf_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r16g16b16a16_sfloat;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->nis_pdf_image_view));
	}
	
	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r16g16b16a16_sfloat;
		image_create_info.extent.width = this->width;
		image_create_info.extent.height = this->height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::sampled_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->ground_truth_distribution_image));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->ground_truth_distribution_image;
		debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::scene_renderer::ground_truth_distribution_image";
		assert_framework_gpu_result(this->gpu_context->get_device()->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->ground_truth_distribution_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->ground_truth_distribution_image : nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->ground_truth_distribution_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->ground_truth_distribution_image;
		bind_image_memory_info.memory = this->ground_truth_distribution_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->ground_truth_distribution_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r16g16b16a16_sfloat;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->ground_truth_distribution_image_view));
	}

	{
		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		image_create_info.extent.width = this->width;
		image_create_info.extent.height = this->height;
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = nullptr;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image(&image_create_info, nullptr, &this->accumulator_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->accumulator_image;
		this->gpu_context->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->accumulator_image : nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->accumulator_device_memory));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->accumulator_image;
		bind_image_memory_info.memory = this->accumulator_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->gpu_context->get_device()->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->accumulator_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r32g32b32a32_sfloat;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(this->gpu_context->get_device()->create_image_view(&image_view_create_info, nullptr, &this->accumulator_image_view));
	}

	if (!this->gpu_context->get_physical_device_features().any_image_initial_layout)
	{
		::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
		{
			::framework::command_context * const graphics_command_context = co_await this->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			::framework::gpu::image_subresource_range subresource_range;
			subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			subresource_range.base_mip_level = 0;
			subresource_range.level_count = 1;
			subresource_range.base_array_layer = 0;
			subresource_range.layer_count = 1;

			::framework::gpu::image_memory_barrier image_memory_barriers[4];
			image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;
			image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[0].image = this->color_image;
			image_memory_barriers[0].subresource_range = subresource_range;

			image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit;
			image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[1].image = this->accumulator_image;
			image_memory_barriers[1].subresource_range = subresource_range;

			image_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[2].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[2].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[2].src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[2].dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[2].image = this->nis_pdf_image;
			image_memory_barriers[2].subresource_range = subresource_range;

			image_memory_barriers[3].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[3].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[3].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[3].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[3].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[3].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[3].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[3].src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[3].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[3].dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
			image_memory_barriers[3].image = this->ground_truth_distribution_image;
			image_memory_barriers[3].subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
			dependency_info.image_memory_barriers = image_memory_barriers;
			graphics_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);

			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
			co_await this->get_command_buffer_manager().get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->get_command_buffer_manager().get_graphics_queue()->release_command_context(graphics_command_context);
		}());
	}
}

void ::framework::graphics::ray_tracing::scene_renderer::resize(::framework::graphics::ray_tracing::scene_renderer_resize_info const * resize_info)
{
	this->width = resize_info->width;
	this->height = resize_info->height;
	this->output_color_image = resize_info->output_color_image;
	this->output_color_image_view = resize_info->output_color_image_view;

	this->gpu_context->get_device()->destroy_image_view(this->color_image_view, nullptr);
	this->gpu_context->get_device()->destroy_image(this->color_image, nullptr);
	this->gpu_context->get_device()->free_memory(this->color_device_memory, nullptr);
	this->gpu_context->get_device()->destroy_image_view(this->accumulator_image_view, nullptr);
	this->gpu_context->get_device()->destroy_image(this->accumulator_image, nullptr);
	this->gpu_context->get_device()->free_memory(this->accumulator_device_memory, nullptr);

	this->create_screen_buffers();

	::framework::gpu::descriptor_image_info descriptor_image_infos[2];
	descriptor_image_infos[0].sampler = nullptr;
	descriptor_image_infos[0].image_view = this->color_image_view;
	descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[1].sampler = nullptr;
	descriptor_image_infos[1].image_view = this->accumulator_image_view;
	descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	// output_color
	write_descriptor_sets[0].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[0].dst_binding = 1;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[0].image_info = &descriptor_image_infos[0];
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// sample_color
	write_descriptor_sets[1].dst_set = this->path_tracer_descriptor_set;
	write_descriptor_sets[1].dst_binding = 2;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[1].image_info = &descriptor_image_infos[1];
	write_descriptor_sets[1].buffer_info = nullptr;
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	this->gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	this->accumulate->resize();
	this->aov->resize();
}

void ::framework::graphics::ray_tracing::scene_renderer::reset()
{
	this->accumulate->reset();
}

void ::framework::graphics::ray_tracing::scene_renderer::recompile_shaders()
{
	this->gpu_context->get_device()->wait_idle();

	this->gpu_context->get_device()->destroy_shader_module(this->path_tracer_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->trace_primary_rays_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->next_vertex_shader_module, nullptr);
	//this->gpu_context->get_device()->destroy_shader_module(this->miss_shader_module, nullptr);
	//this->gpu_context->get_device()->destroy_shader_module(this->closest_hit_shader_module, nullptr);
	//this->gpu_context->get_device()->destroy_shader_module(this->volume_closest_hit_shader_module, nullptr);
	//this->gpu_context->get_device()->destroy_shader_module(this->volume_intersection_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->spatial_resampling_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->generate_forward_test_samples_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->plot_pdf_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_shader_module(this->align_batch_size_shader_module, nullptr);
	this->gpu_context->get_device()->destroy_pipeline(this->path_tracer_pipeline, nullptr);
	this->gpu_context->get_device()->destroy_pipeline(this->spatial_resampling_pipeline, nullptr);
	this->gpu_context->get_device()->destroy_pipeline(this->generate_forward_test_samples_pipeline, nullptr);
	this->gpu_context->get_device()->destroy_pipeline(this->align_batch_size_pipeline, nullptr);

	this->create_pipelines();

	this->accumulate->recompile_shaders();
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene_renderer::update_command_buffer)(::std::uint32_t external_wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * external_wait_semaphore_infos)
{
	this->current_resource_index = this->frame_index % this->frame_buffer_count;

	auto & current_frame_resources = this->frame_resources[this->current_resource_index];
	::framework::gpu::fence * const frame_fence = current_frame_resources.fence->get_fence();
	assert_framework_gpu_result(this->gpu_context->get_device()->wait_for_fences(1, &frame_fence, true, UINT64_MAX));
	assert_framework_gpu_result(this->gpu_context->get_device()->reset_fences(1, &frame_fence));

	::framework::command_context * const graphics_command_context = current_frame_resources.graphics_command_context.get();
	::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

	co_await this->get_command_buffer_manager().get_graphics_queue()->reset_command_context(graphics_command_context);

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

	::framework::profiler::manual_timestamp manual_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "scene_renderer::execute");
	manual_timestamp.begin_timestamp();

	static float ray_eps = 0.0001f;

	if (::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_equals))
	{
		ray_eps += 0.01f;
	}
	else if (::framework::engine_core::input_listner->get_key_state(::framework::input::key::key_minus))
	{
		ray_eps -= 0.01f;
	}

	ray_eps = ::std::clamp(ray_eps, 0.0f, 1.0f);

	::std::uint32_t const dynamic_offsets[]
	{
		this->current_resource_index * static_cast<::std::uint32_t>(this->ubo_alignment),
		this->current_resource_index * this->scene->get_uniform_buffer_alignment(),
		this->current_resource_index * this->spectrum_system->get_uniform_buffer_alignment()
	};

	::std::random_device random_device;
	::std::mt19937 mt19937(random_device());
	::std::uniform_int_distribution<::std::uint32_t> uniform_int_distribution;

	for (::std::uint32_t i = 0; i < this->scene_views.size(); i++)
	{
		::framework::graphics::ray_tracing::scene_renderer::path_tracer_parameters path_tracer_parameters;
		path_tracer_parameters.ray_eps = ray_eps;
		path_tracer_parameters.seed = uniform_int_distribution(mt19937);
		path_tracer_parameters.sample_count = 1;
		path_tracer_parameters.enable_ncv = static_cast<::framework::gpu::bool32_t>(this->enable_ncv);
		path_tracer_parameters.enable_nis = static_cast<::framework::gpu::bool32_t>(this->enable_nis);
		path_tracer_parameters.enable_nis_debug_train = static_cast<::framework::gpu::bool32_t>(false);
		path_tracer_parameters.enable_nee = static_cast<::framework::gpu::bool32_t>(this->enable_nee);
		path_tracer_parameters.visualize_neural_light_field = static_cast<::framework::gpu::bool32_t>(this->nis_visualization_type == ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::neural_light_field);
		path_tracer_parameters.log_jacobian_buffer_stride = this->nis_normalizing_flow->get_output_stride();
		path_tracer_parameters.nis_visualization_coordinates = this->nis_visualization_coordinates;

		::std::memcpy(static_cast<char *>(this->mapped_data) + dynamic_offsets[0], &path_tracer_parameters, sizeof(path_tracer_parameters));
	}

	co_await graphics_command_context->update_buffer(this->shuffle_seed_buffer, uniform_int_distribution(mt19937));

	co_await this->spectrum_system->update();
	co_await this->material_system->update();
	co_await this->medium_system->update();
	co_await this->scene->update();

	{
		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = 1;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::framework::gpu::image_memory_barrier image_memory_barriers[1];
		image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_write_bit;
		image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::ray_tracing_shader_bit;
		image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;
		image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
		image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->gpu_context->get_queue_family_indices().graphics;
		image_memory_barriers[0].image = this->color_image;
		image_memory_barriers[0].subresource_range = subresource_range;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
		dependency_info.image_memory_barriers = image_memory_barriers;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (this->is_reset_nis)
	{
		::glm::uint const half2_fill_value = ::glm::packHalf2x16(::glm::vec2(0.0f));
		command_buffer->fill_buffer(this->light_field_encoded_features_buffer, 0, ::framework::gpu::whole_size, half2_fill_value, 0, {}, {});
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		co_await this->nis_normalizing_flow->initialize(graphics_command_context, ::framework::local::max_batch_size, this->width * this->height);
		co_await this->cv_shape_normalizing_flow->initialize(graphics_command_context, ::framework::local::max_batch_size, this->width * this->height);
		::std::uint32_t const encoding_parameter_count = this->light_field_grid_encoding_execution_policy->get_parameter_count();
		this->xavier_uniform->execute(command_buffer, this->light_field_grid_encoding_initialize_parameters_descriptor_set, 0, encoding_parameter_count, encoding_parameter_count);
		this->light_field_neural_network->initialize_xavier_uniform(command_buffer, this->xavier_uniform.get(), this->light_field_network_initialize_parameters_descriptor_set);
		this->light_field_encoding_optimizer->initialize(command_buffer, this->light_field_encoding_optimizer_descriptor_set);
		this->light_field_network_optimizer->initialize(command_buffer, this->light_field_network_optimizer_descriptor_set);
		this->is_reset_nis = false;
	}

	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		this->path_tracer_descriptor_set,
		this->scene_descriptor_set,
		this->material_system->get_descriptor_set(),
		this->medium_system->medium_system_device_descriptor_set,
		this->spectrum_system->device_descriptor_set,
		this->gpu_log_descriptor_set,
		this->aov->get_descriptor_set()
	};

	::framework::gpu::strided_device_address_region miss_shader_binding_table;
	miss_shader_binding_table.device_address = this->scene->get_miss_shader_binding_table_device_address();
	miss_shader_binding_table.stride = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment);
	miss_shader_binding_table.size = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment);

	::framework::gpu::strided_device_address_region closest_hit_shader_binding_table;
	closest_hit_shader_binding_table.device_address = this->scene->get_closest_hit_shader_binding_table_device_address();
	closest_hit_shader_binding_table.stride = this->scene->get_closest_hit_shader_group_stride();
	closest_hit_shader_binding_table.size = this->scene->get_closest_hit_shader_binding_table_size();

	::framework::gpu::strided_device_address_region callable_shader_binding_table;
	callable_shader_binding_table.device_address = 0;
	callable_shader_binding_table.stride = 0;
	callable_shader_binding_table.size = 0;

	::std::uint32_t const inference_batch_size = this->width * this->height;
	::std::uint32_t const inference_batch_size_dynamic_offset = 0;
	::std::uint32_t const batch_size_dynamic_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(sizeof(::std::uint32_t), this->gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

	{
		::framework::gpu::strided_device_address_region raygen_shader_binding_table;
		raygen_shader_binding_table.device_address = this->raygen_shader_binding_table_device_address;
		raygen_shader_binding_table.stride = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
		raygen_shader_binding_table.size = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);

		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "trace_primary_rays");

			command_buffer->fill_buffer(this->batch_size_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});

			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline);
			command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
			command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
			command_buffer->trace_rays(raygen_shader_binding_table, miss_shader_binding_table, closest_hit_shader_binding_table, callable_shader_binding_table, this->width, this->height, 1);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	{
		::framework::gpu::strided_device_address_region raygen_shader_binding_table;
		raygen_shader_binding_table.device_address = this->raygen_shader_binding_table_device_address + static_cast<::framework::gpu::device_size>(::std::max(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment, this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size));
		raygen_shader_binding_table.stride = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
		raygen_shader_binding_table.size = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);

		for (::std::uint32_t i = 0; i <= ::framework::local::max_path_length; i++)
		{
			if (i < ::framework::local::max_path_length)
			{
				struct ::framework::local::generate_forward_test_samples_push_constants push_constants
				{
					.count = true /* todo */
				};

				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->align_batch_size_pipeline);
				command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
				command_buffer->push_constants(this->path_tracer_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				command_buffer->dispatch(1, 1, 1);
				{
					::framework::gpu::memory_barrier memory_barrier;
					memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
					memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
					memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

					::framework::gpu::dependency_info dependency_info;
					dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
					dependency_info.memory_barrier_count = 1;
					dependency_info.memory_barriers = &memory_barrier;
					dependency_info.buffer_memory_barrier_count = 0;
					dependency_info.buffer_memory_barriers = nullptr;
					dependency_info.image_memory_barrier_count = 0;
					dependency_info.image_memory_barriers = nullptr;
					command_buffer->pipeline_barrier(&dependency_info);
				}

				{
					command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->concatenate_features_pipeline);
					command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
					command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
					command_buffer->push_constants(this->path_tracer_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
					command_buffer->dispatch_indirect(this->dispatch_indirect_command_buffer, sizeof(::framework::gpu::dispatch_indirect_command) * 2u);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
				}

				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "neural_light_field::inference");

					{
						::std::uint32_t const dynamic_offsets[] = { inference_batch_size_dynamic_offset, this->get_light_field_grid_encoding_dispatch_indirect_command_offset() };
						this->light_field_grid_encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->light_field_grid_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
					}
					{
						::std::uint32_t const dynamic_offsets[] = { inference_batch_size_dynamic_offset, this->get_light_field_one_blob_encoding_dispatch_indirect_command_offset() };
						this->light_field_one_blob_encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->light_field_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
					}
					this->light_field_network_execution_policy->initialize_dispatch_indirect_command(this->gpu_context, command_buffer, this->light_field_network_initialize_dispatch_indirect_command_descriptor_set, true);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
					this->light_field_grid_encoding->forward_indirect(command_buffer, this->light_field_grid_encoding_descriptor_set, inference_batch_size_dynamic_offset, this->dispatch_indirect_command_buffer, this->get_light_field_grid_encoding_dispatch_indirect_command_offset());
					this->light_field_one_blob_encoding->forward_indirect(command_buffer, this->light_field_one_blob_encoding_descriptor_set, inference_batch_size_dynamic_offset, this->dispatch_indirect_command_buffer, this->get_light_field_one_blob_encoding_dispatch_indirect_command_offset());
					this->light_field_neural_network->inference_indirect(command_buffer, this->light_field_network_descriptor_set);
				}

				if (this->enable_ncv)
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "cv_shape_normalizing_flow::forward_without_gradient");

					this->cv_shape_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, inference_batch_size_dynamic_offset, true);

					command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_log_jacobian_buffer(), 0, inference_batch_size * this->cv_shape_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
					command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_features_buffer(), 0, inference_batch_size * this->cv_shape_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
					{
						::framework::gpu::buffer_copy buffer_copy;
						buffer_copy.src_offset = 0;
						buffer_copy.dst_offset = 0;
						buffer_copy.size = inference_batch_size * 2u * sizeof(::std::uint16_t);
						command_buffer->copy_buffer(this->nis_normalizing_flow->get_x_buffer(), this->cv_shape_normalizing_flow->get_x_buffer(), 1, &buffer_copy);
					}
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
					{
						::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "cv_shape_concatenate_features");

						command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->cv_shape_concatenate_features_pipeline);
						command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
						command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
						command_buffer->dispatch_indirect(this->dispatch_indirect_command_buffer, sizeof(::framework::gpu::dispatch_indirect_command) * 2u);
					}
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}

					this->cv_shape_normalizing_flow->forward_without_gradient_indirect(command_buffer);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
				}

				if (this->enable_nis)
				{
					::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "nis_normalizing_flow::backward_without_gradient");

					this->nis_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, inference_batch_size_dynamic_offset, true);

					command_buffer->fill_buffer(this->nis_normalizing_flow->get_log_jacobian_buffer(), 0, inference_batch_size * this->nis_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
					command_buffer->fill_buffer(this->nis_normalizing_flow->get_features_buffer(), 0, inference_batch_size * this->nis_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}

					this->nis_normalizing_flow->backward_without_gradient_indirect(command_buffer);
					{
						::framework::gpu::memory_barrier memory_barrier;
						memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
						memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
						memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

						::framework::gpu::dependency_info dependency_info;
						dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
						dependency_info.memory_barrier_count = 1;
						dependency_info.memory_barriers = &memory_barrier;
						dependency_info.buffer_memory_barrier_count = 0;
						dependency_info.buffer_memory_barriers = nullptr;
						dependency_info.image_memory_barrier_count = 0;
						dependency_info.image_memory_barriers = nullptr;
						command_buffer->pipeline_barrier(&dependency_info);
					}
				}
			}

			command_buffer->fill_buffer(this->batch_size_buffer, 0, sizeof(::std::uint32_t), 0, 0, {}, {});
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			{
				::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, ::std::format("next_vertex {}", i));

				command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline);
				command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
				command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
				command_buffer->trace_rays(raygen_shader_binding_table, miss_shader_binding_table, closest_hit_shader_binding_table, callable_shader_binding_table, this->width, this->height, 1);
			}
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	if (this->enable_nis_training)
	{
		struct ::framework::local::generate_forward_test_samples_push_constants push_constants
		{
			.count = false,
		};

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "scan::initialize_dispatch_indirect_command");

			this->scan_executor->initialize_dispatch_indirect_command(*this->scan, this->gpu_context, command_buffer, this->scan_initialize_dispatch_indirect_command_descriptor_set);
		}
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->align_batch_size_pipeline);
		command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
		command_buffer->push_constants(this->path_tracer_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch(1, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "shuffle_nis_samples");

			::framework::gpu::descriptor_set * const descriptor_sets[]{ this->scan_descriptor_set, this->shuffle_nis_samples_descriptor_set };
			this->scan_executor->execute_indirect(*this->scan, command_buffer, descriptor_sets, 0, nullptr, this->dispatch_indirect_command_buffer, 0);
		}
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "neural_light_field::train");

			{
				::std::uint32_t const dynamic_offsets[] = { batch_size_dynamic_offset, this->get_light_field_grid_encoding_dispatch_indirect_command_offset() };
				this->light_field_grid_encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->light_field_grid_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
			}
			{
				::std::uint32_t const dynamic_offsets[] = { batch_size_dynamic_offset, this->get_light_field_one_blob_encoding_dispatch_indirect_command_offset() };
				this->light_field_one_blob_encoding_execution_policy->initialize_dispatch_indirect_command(command_buffer, this->light_field_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set, dynamic_offsets);
			}
			this->light_field_network_execution_policy->initialize_dispatch_indirect_command(this->gpu_context, command_buffer, this->light_field_network_initialize_dispatch_indirect_command_descriptor_set, false);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->light_field_grid_encoding->forward_indirect(command_buffer, this->light_field_grid_encoding_descriptor_set, batch_size_dynamic_offset, this->dispatch_indirect_command_buffer, this->get_light_field_grid_encoding_dispatch_indirect_command_offset());
			this->light_field_one_blob_encoding->forward_indirect(command_buffer, this->light_field_one_blob_encoding_descriptor_set, batch_size_dynamic_offset, this->dispatch_indirect_command_buffer, this->get_light_field_one_blob_encoding_dispatch_indirect_command_offset());
			this->light_field_neural_network->forward_indirect(command_buffer, this->light_field_network_descriptor_set);

			this->light_field_loss->evaluate_indirect(command_buffer, this->light_field_loss_descriptor_set, this->dispatch_indirect_command_buffer, sizeof(::framework::gpu::dispatch_indirect_command) * 3u);

			command_buffer->fill_buffer(this->light_field_dloss_dnetwork_input_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});			

			this->light_field_neural_network->backward_indirect(command_buffer, this->light_field_network_descriptor_set);
			this->light_field_grid_encoding->backward_indirect(command_buffer, this->light_field_grid_encoding_descriptor_set, batch_size_dynamic_offset, this->dispatch_indirect_command_buffer, this->get_light_field_grid_encoding_dispatch_indirect_command_offset());

			this->light_field_network_optimizer->step(command_buffer, this->light_field_network_optimizer_descriptor_set);
			this->light_field_encoding_optimizer->step(command_buffer, this->light_field_encoding_optimizer_descriptor_set);
		}

		if (this->enable_nis)
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "nis_normalizing_flow::forward");

			command_buffer->fill_buffer(this->nis_normalizing_flow->get_log_jacobian_buffer(), 0, ::framework::local::max_batch_size * this->nis_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			command_buffer->fill_buffer(this->nis_normalizing_flow->get_features_buffer(), 0, ::framework::local::max_batch_size * this->nis_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});

			this->nis_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, batch_size_dynamic_offset, false);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->nis_normalizing_flow->forward_indirect(command_buffer);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}

		if (this->enable_ncv)
		{
			::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "cv_shape_normalizing_flow::forward");

			command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_log_jacobian_buffer(), 0, ::framework::local::max_batch_size * this->cv_shape_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_features_buffer(), 0, ::framework::local::max_batch_size * this->cv_shape_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});

			this->cv_shape_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, batch_size_dynamic_offset, false);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
			this->cv_shape_normalizing_flow->forward_indirect(command_buffer);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}
	}

	if (this->nis_visualization_type == ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::ground_truth)
	{
		::framework::gpu::strided_device_address_region raygen_shader_binding_table;
		raygen_shader_binding_table.device_address = this->raygen_shader_binding_table_device_address + 2 * static_cast<::framework::gpu::device_size>(::std::max(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment, this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size));
		raygen_shader_binding_table.stride = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment);
		raygen_shader_binding_table.size = static_cast<::framework::gpu::device_size>(this->gpu_context->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment);

		::framework::profiler::scoped_timestamp const scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "trace_rays");

		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline);
		command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::ray_tracing, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
		command_buffer->trace_rays(raygen_shader_binding_table, miss_shader_binding_table, closest_hit_shader_binding_table, callable_shader_binding_table, this->width, this->height, 1);
	}
	else if (this->nis_visualization_type == ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type::nis_pdf)
	{
		{
			struct ::framework::local::generate_forward_test_samples_push_constants push_constants
			{
				.count = this->width * this->height,
				.width = this->width,
				.height = this->height
			};

			::framework::profiler::scoped_timestamp scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "generate_forward_test_samples");

			command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->generate_forward_test_samples_pipeline);
			command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
			command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
			command_buffer->push_constants(this->path_tracer_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
			command_buffer->dispatch((push_constants.count + 128 - 1) / 128, 1, 1);

			command_buffer->fill_buffer(this->batch_size_buffer, 0, sizeof(::std::uint32_t), push_constants.count, 0, {}, {});
		}

		{
			::framework::profiler::scoped_timestamp scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "forward_without_gradient");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			this->nis_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, inference_batch_size_dynamic_offset, true);

			command_buffer->fill_buffer(this->nis_normalizing_flow->get_log_jacobian_buffer(), 0, inference_batch_size * this->nis_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			command_buffer->fill_buffer(this->nis_normalizing_flow->get_features_buffer(), 0, inference_batch_size * this->nis_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			this->nis_normalizing_flow->forward_without_gradient_indirect(command_buffer);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}

		{
			::framework::profiler::scoped_timestamp scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "forward_without_gradient");
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			this->cv_shape_normalizing_flow->initialize_dispatch_indirect_command(command_buffer, inference_batch_size_dynamic_offset, true);

			command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_log_jacobian_buffer(), 0, inference_batch_size * this->cv_shape_normalizing_flow->get_output_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			command_buffer->fill_buffer(this->cv_shape_normalizing_flow->get_features_buffer(), 0, inference_batch_size * this->cv_shape_normalizing_flow->get_features_stride() * sizeof(::std::uint16_t), 0, 0, {}, {});
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}

			this->cv_shape_normalizing_flow->forward_without_gradient_indirect(command_buffer);
			{
				::framework::gpu::memory_barrier memory_barrier;
				memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
				memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
				memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

				::framework::gpu::dependency_info dependency_info;
				dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
				dependency_info.memory_barrier_count = 1;
				dependency_info.memory_barriers = &memory_barrier;
				dependency_info.buffer_memory_barrier_count = 0;
				dependency_info.buffer_memory_barriers = nullptr;
				dependency_info.image_memory_barrier_count = 0;
				dependency_info.image_memory_barriers = nullptr;
				command_buffer->pipeline_barrier(&dependency_info);
			}
		}

		{
			struct ::framework::local::generate_forward_test_samples_push_constants push_constants
			{
				.count = inference_batch_size,
				.width = this->width,
				.height = this->height,
				.log_jacobian_buffer_stride = this->nis_normalizing_flow->get_output_stride()
			};

			::framework::profiler::scoped_timestamp scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "plot_pdf");

			command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->plot_pdf_pipeline);
			command_buffer->bind_descriptor_pool(this->device_descriptor_pool);
			command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->path_tracer_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
			command_buffer->push_constants(this->path_tracer_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
			command_buffer->dispatch((push_constants.count + 128 - 1) / 128, 1, 1);
		}
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	this->accumulate->execute(command_buffer, true);

	manual_timestamp.end_timestamp();

	assert_framework_gpu_result(command_buffer->end_command_buffer());

	::std::vector<::framework::gpu::semaphore_submit_info> wait_semaphore_infos(7 + external_wait_semaphore_info_count);
	wait_semaphore_infos[0].semaphore = this->spectrum_system->get_update_semaphore();
	wait_semaphore_infos[0].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = this->material_system->get_update_semaphore();
	wait_semaphore_infos[1].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[1].device_index = 0;

	wait_semaphore_infos[2].semaphore = this->medium_system->get_update_semaphore();
	wait_semaphore_infos[2].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[2].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[2].device_index = 0;

	wait_semaphore_infos[3].semaphore = this->scene->get_initialize_geometries_semaphore();
	wait_semaphore_infos[3].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[3].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[3].device_index = 0;

	wait_semaphore_infos[4].semaphore = this->scene->get_update_transforms_semaphore();
	wait_semaphore_infos[4].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[4].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[4].device_index = 0;

	wait_semaphore_infos[5].semaphore = this->scene->get_update_lights_semaphore();
	wait_semaphore_infos[5].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[5].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[5].device_index = 0;

	wait_semaphore_infos[6].semaphore = this->scene->get_update_semaphore();
	wait_semaphore_infos[6].value = this->get_last_frame_index() + 1;
	wait_semaphore_infos[6].stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	wait_semaphore_infos[6].device_index = 0;

	if (external_wait_semaphore_info_count)
	{
		::std::memcpy(&wait_semaphore_infos[7], external_wait_semaphore_infos, sizeof(::framework::gpu::semaphore_submit_info) * external_wait_semaphore_info_count);
	}

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = command_buffer;
	command_buffer_info.device_mask = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->get_frame_semaphore();
	signal_semaphore_info.value = this->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_info.device_index = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = static_cast<::std::uint32_t>(wait_semaphore_infos.size());
	submit_info.wait_semaphore_infos = wait_semaphore_infos.data();
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = 1;
	submit_info.signal_semaphore_infos = &signal_semaphore_info;
	co_await this->get_command_buffer_manager().get_graphics_queue()->submit(1, &submit_info, frame_fence);

	this->staging_buffer_current_offset = 0;
	++this->frame_index;
}

void ::framework::graphics::ray_tracing::scene_renderer::set_scene(::framework::graphics::scene * scene)
{
	this->scene = static_cast<::framework::graphics::ray_tracing::scene *>(scene);

	if (this->gpu_context->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->scene_descriptor_set = this->scene->get_descriptor_set();
	}
	else
	{
		::framework::gpu::descriptor_set * const scene_host_descriptor_set = this->scene->get_descriptor_set();

		::framework::gpu::copy_descriptor_set copy_descriptor_sets[11];
		for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(::std::size(copy_descriptor_sets)); i++)
		{
			copy_descriptor_sets[i].src_set = scene_host_descriptor_set;
			copy_descriptor_sets[i].src_binding = i;
			copy_descriptor_sets[i].src_array_element = 0;
			copy_descriptor_sets[i].dst_set = this->scene_descriptor_set;
			copy_descriptor_sets[i].dst_binding = i;
			copy_descriptor_sets[i].dst_array_element = 0;
			copy_descriptor_sets[i].descriptor_count = 1;
		}
		// uniform_buffer
		copy_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		// raytracing_acceleration_structure
		copy_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::acceleration_structure;
		// world_matrices
		copy_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// inverse_transpose_world_matrices
		copy_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// geometry_instances
		copy_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// indices
		copy_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// vertices
		copy_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// lights
		copy_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// infinite_lights
		copy_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// emissive_triangles
		copy_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		// instance_offset_buffer
		copy_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;

		this->gpu_context->get_device()->update_descriptor_sets(0, nullptr, static_cast<::std::uint32_t>(::std::size(copy_descriptor_sets)), copy_descriptor_sets);
	}

	this->scene->bind_descriptor_set();
}

void ::framework::graphics::ray_tracing::scene_renderer::set_scene_views(::std::uint32_t scene_view_count, ::framework::graphics::scene_view ** scene_views)
{
	this->scene_views.resize(scene_view_count);

	for (::std::uint32_t i = 0; i < scene_view_count; i++)
	{
		this->scene_views[i] = static_cast<::framework::graphics::ray_tracing::scene_view_perspective *>(scene_views[i]);
	}
}

::framework::graphics::scene * ::framework::graphics::ray_tracing::scene_renderer::create_scene(::framework::graphics::scene_create_info * create_info)
{
	return new ::framework::graphics::ray_tracing::scene(this, create_info);
}

::framework::graphics::triangle_mesh_geometry * (::framework::graphics::ray_tracing::scene_renderer::create_triangle_mesh_geometry)(::framework::graphics::triangle_mesh_geometry_create_info const & create_info)
{
	::framework::graphics::ray_tracing::triangle_mesh_geometry * triangle_mesh_geometry = new ::framework::graphics::ray_tracing::triangle_mesh_geometry(this->triangle_mesh_geometry_counter++, this, create_info);
	return triangle_mesh_geometry;
}

::framework::graphics::image * (::framework::graphics::ray_tracing::scene_renderer::create_image)(::framework::graphics::image_create_info const & create_info)
{
	::framework::graphics::ray_tracing::image * image = new ::framework::graphics::ray_tracing::image(this->texture_counter++, this, create_info);
	return image;
}

::framework::graphics::material * ::framework::graphics::ray_tracing::scene_renderer::create_material(::framework::graphics::material_create_info const & create_info)
{
	return this->material_system->create_material(create_info);
}

::framework::graphics::medium * ::framework::graphics::ray_tracing::scene_renderer::create_medium()
{
	return this->medium_system->create_medium();
}

::framework::graphics::scene_view_perspective * ::framework::graphics::ray_tracing::scene_renderer::create_scene_view_perspective(::framework::graphics::scene_view_perspective_create_info * create_info)
{
	return new ::framework::graphics::ray_tracing::scene_view_perspective(this, create_info);
}