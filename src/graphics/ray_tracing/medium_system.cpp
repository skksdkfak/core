#include "coroutine/sync_wait.hpp"
#include "graphics/ray_tracing/medium_system.hpp"
#include "graphics/ray_tracing/medium.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "gpu/utility.hpp"

::framework::graphics::ray_tracing::medium_system::medium_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer) :
	scene_renderer(scene_renderer),
	medium_counter(0)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		// media
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 3;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->medium_system_descriptor_set_layout));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::graphics::ray_tracing::medium_data_blob) * 64;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->medium_data_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->medium_data_buffer;
		debug_utils_object_name_info.object_name = "medium_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->medium_data_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->medium_data_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->medium_data_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->medium_data_buffer;
		bind_buffer_memory_info.memory = this->medium_data_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_host_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->medium_system_descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->medium_system_host_descriptor_set));
	}

	if (scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->medium_system_device_descriptor_set = this->medium_system_host_descriptor_set;
	}
	else
	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->medium_system_descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->medium_system_device_descriptor_set));
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_info;
	descriptor_buffer_info.buffer = this->medium_data_buffer;
	descriptor_buffer_info.offset = 0;
	descriptor_buffer_info.range = ::framework::gpu::whole_size;
	descriptor_buffer_info.stride = 128;

	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = this->medium_system_host_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_info;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	this->bind_descriptor_set();

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_semaphore));
}

::framework::graphics::ray_tracing::medium_system::~medium_system()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	device->destroy_buffer(this->medium_data_buffer, nullptr);
	device->free_memory(this->medium_data_device_memory, nullptr);
}

::framework::graphics::medium * ::framework::graphics::ray_tracing::medium_system::create_medium()
{
	return new ::framework::graphics::ray_tracing::medium(this, this->medium_counter++);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::medium_system::update)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const transfer_command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->media_to_update_mutex.scoped_lock();
		for (::framework::graphics::ray_tracing::medium * medium : this->media_to_update)
		{
			co_await transfer_command_context->update_buffer(this->medium_data_buffer, co_await medium->get_device_data_and_commit(), medium->get_id());
		}
		this->media_to_update.clear();
	}
	assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::medium_system::add_medium_to_update)(::framework::graphics::ray_tracing::medium * medium)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->media_to_update_mutex.scoped_lock();
	this->media_to_update.push_back(medium);
}

void ::framework::graphics::ray_tracing::medium_system::bind_descriptor_set()
{
	if (this->scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		return;
	}

	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	::framework::gpu::copy_descriptor_set copy_descriptor_set;
	copy_descriptor_set.src_set = this->medium_system_host_descriptor_set;
	copy_descriptor_set.src_binding = 0;
	copy_descriptor_set.src_array_element = 0;
	copy_descriptor_set.dst_set = this->medium_system_device_descriptor_set;
	copy_descriptor_set.dst_binding = 0;
	copy_descriptor_set.dst_array_element = 0;
	copy_descriptor_set.descriptor_count = 1;
	copy_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;

	device->update_descriptor_sets(0, nullptr, 1, &copy_descriptor_set);
}
