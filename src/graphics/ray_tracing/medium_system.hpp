#pragma once

#include "command_context.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "graphics/ray_tracing/medium.hpp"
#include <glm/glm.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class medium_system
	{
	public:
		medium_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		~medium_system();

		::framework::graphics::medium * create_medium();

		::framework::coroutine::immediate_task<void> update();

		::framework::coroutine::immediate_task<void> add_medium_to_update(::framework::graphics::ray_tracing::medium * medium);

		void bind_descriptor_set();

		::framework::graphics::ray_tracing::scene_renderer * get_scene_renderer() const noexcept;

		::framework::gpu::semaphore * get_update_semaphore() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::descriptor_set_layout * medium_system_descriptor_set_layout;
		::framework::gpu::descriptor_set * medium_system_host_descriptor_set;
		::framework::gpu::descriptor_set * medium_system_device_descriptor_set;
		::framework::gpu::buffer * medium_data_buffer;
		::framework::gpu::device_memory * medium_data_device_memory;
		::framework::gpu::semaphore * update_semaphore;
		::std::vector<::framework::graphics::ray_tracing::medium *> media_to_update;
		::framework::coroutine::mutex media_to_update_mutex;
		::std::uint32_t medium_counter;
	};
}

#include "graphics/ray_tracing/medium_system.inl"