#include "coroutine/sync_wait.hpp"
#include "graphics/ray_tracing/material_system.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "gpu/utility.hpp"
#include <numbers>
#include <span>

constexpr float Inv4Pi = 0.07957747154594766788;

struct BSSRDFTable
{
	::std::vector<float> rhoSamples, radiusSamples;
	::std::vector<float> profile;
	::std::vector<float> rhoEff;
	::std::vector<float> profileCDF;

	BSSRDFTable(int nRhoSamples, int nRadiusSamples) :
		rhoSamples(nRhoSamples),
		radiusSamples(nRadiusSamples),
		profile(nRadiusSamples * nRhoSamples),
		rhoEff(nRhoSamples),
		profileCDF(nRadiusSamples * nRhoSamples)
	{}
};

float SampleExponential(float u, float a)
{
	return -std::log(1 - u) / a;
}

float FresnelMoment1(float eta)
{
	float eta2 = eta * eta, eta3 = eta2 * eta, eta4 = eta3 * eta, eta5 = eta4 * eta;
	if (eta < 1)
		return 0.45966f - 1.73965f * eta + 3.37668f * eta2 - 3.904945 * eta3 +
		2.49277f * eta4 - 0.68441f * eta5;
	else
		return -4.61686f + 11.1136f * eta - 10.4646f * eta2 + 5.11455f * eta3 -
		1.27198f * eta4 + 0.12746f * eta5;
}

float FresnelMoment2(float eta)
{
	float eta2 = eta * eta, eta3 = eta2 * eta, eta4 = eta3 * eta, eta5 = eta4 * eta;
	if (eta < 1) {
		return 0.27614f - 0.87350f * eta + 1.12077f * eta2 - 0.65095f * eta3 +
			0.07883f * eta4 + 0.04860f * eta5;
	}
	else {
		float r_eta = 1 / eta, r_eta2 = r_eta * r_eta, r_eta3 = r_eta2 * r_eta;
		return -547.033f + 45.3087f * r_eta3 - 218.725f * r_eta2 + 458.843f * r_eta +
			404.557f * eta - 189.519f * eta2 + 54.9327f * eta3 - 9.00603f * eta4 +
			0.63942f * eta5;
	}
}

float IntegrateCatmullRom(::std::span<const float> nodes, ::std::span<const float> f, ::std::span<float> cdf)
{
	float sum = 0;
	cdf[0] = 0;
	for (int i = 0; i < nodes.size() - 1; ++i)
	{
		// Look up $x_i$ and function values of spline segment _i_
		float x0 = nodes[i], x1 = nodes[i + 1];
		float f0 = f[i], f1 = f[i + 1];
		float width = x1 - x0;

		// Approximate derivatives using finite differences
		float d0 = (i > 0) ? width * (f1 - f[i - 1]) / (x1 - nodes[i - 1]) : (f1 - f0);
		float d1 = (i + 2 < nodes.size()) ? width * (f[i + 2] - f0) / (nodes[i + 2] - x0)
			: (f1 - f0);

		// Keep a running sum and build a cumulative distribution function
		sum += width * ((f0 + f1) / 2 + (d0 - d1) / 12);
		cdf[i + 1] = sum;
	}
	return sum;
}

template <typename T>
inline constexpr T Sqr(T v)
{
	return v * v;
}
inline float SafeSqrt(float x)
{
	return std::sqrt(std::max(0.f, x));
}

template <typename T, typename U, typename V>
inline constexpr T Clamp(T val, U low, V high)
{
	if (val < low)
		return T(low);
	else if (val > high)
		return T(high);
	else
		return val;
}

inline float HenyeyGreenstein(float cosTheta, float g)
{
	float denom = 1 + Sqr(g) + 2 * g * cosTheta;
	return Inv4Pi * (1 - Sqr(g)) / (denom * SafeSqrt(denom));
}
inline float FrDielectric(float cosTheta_i, float eta)
{
	cosTheta_i = Clamp(cosTheta_i, -1, 1);
	// Potentially flip interface orientation for Fresnel equations
	if (cosTheta_i < 0) {
		eta = 1 / eta;
		cosTheta_i = -cosTheta_i;
	}

	// Compute $\cos\,\theta_\roman{t}$ for Fresnel equations using Snell's law
	float sin2Theta_i = 1 - Sqr(cosTheta_i);
	float sin2Theta_t = sin2Theta_i / Sqr(eta);
	if (sin2Theta_t >= 1)
		return 1.f;
	float cosTheta_t = SafeSqrt(1 - sin2Theta_t);

	float r_parl = (eta * cosTheta_i - cosTheta_t) / (eta * cosTheta_i + cosTheta_t);
	float r_perp = (cosTheta_i - eta * cosTheta_t) / (cosTheta_i + eta * cosTheta_t);
	return (Sqr(r_parl) + Sqr(r_perp)) / 2;
}

float BeamDiffusionMS(float sigma_s, float sigma_a, float g, float eta, float r)
{
	const int nSamples = 100;
	float Ed = 0;
	// Precompute information for dipole integrand
	// Compute reduced scattering coefficients $\sigmaps, \sigmapt$ and albedo $\rhop$
	float sigmap_s = sigma_s * (1 - g);
	float sigmap_t = sigma_a + sigmap_s;
	float rhop = sigmap_s / sigmap_t;

	// Compute non-classical diffusion coefficient $D_\roman{G}$ using Equation
	// $(\ref{eq:diffusion-coefficient-grosjean})$
	float D_g = (2 * sigma_a + sigmap_s) / (3 * sigmap_t * sigmap_t);

	// Compute effective transport coefficient $\sigmatr$ based on $D_\roman{G}$
	float sigma_tr = ::std::sqrt(::std::max(0.f, sigma_a / D_g));

	// Determine linear extrapolation distance $\depthextrapolation$ using Equation
	// $(\ref{eq:dipole-boundary-condition})$
	float fm1 = FresnelMoment1(eta), fm2 = FresnelMoment2(eta);
	float ze = -2 * D_g * (1 + 3 * fm2) / (1 - 2 * fm1);

	// Determine exitance scale factors using Equations $(\ref{eq:kp-exitance-phi})$ and
	// $(\ref{eq:kp-exitance-e})$
	float cPhi = 0.25f * (1 - 2 * fm1), cE = 0.5f * (1 - 3 * fm2);

	for (int i = 0; i < nSamples; ++i)
	{
		// Sample real point source depth $\depthreal$
		float zr = SampleExponential((i + 0.5f) / nSamples, sigmap_t);

		// Evaluate dipole integrand $E_{\roman{d}}$ at $\depthreal$ and add to _Ed_
		float zv = -zr + 2 * ze;
		float dr = std::sqrt(Sqr(r) + Sqr(zr)), dv = std::sqrt(Sqr(r) + Sqr(zv));
		// Compute dipole fluence rate $\dipole(r)$ using Equation
		// $(\ref{eq:diffusion-dipole})$
		float phiD = Inv4Pi / D_g * (::std::exp(-sigma_tr * dr) / dr - ::std::exp(-sigma_tr * dv) / dv);

		// Compute dipole vector irradiance $-\N{}\cdot\dipoleE(r)$ using Equation
		// $(\ref{eq:diffusion-dipole-vector-irradiance-normal})$
		float EDn = Inv4Pi * (
			zr * (1 + sigma_tr * dr) * ::std::exp(-sigma_tr * dr) / (::std::powf(dr, 3)) -
			zv * (1 + sigma_tr * dv) * ::std::exp(-sigma_tr * dv) / (::std::powf(dv, 3)));

		// Add contribution from dipole for depth $\depthreal$ to _Ed_
		float E = phiD * cPhi + EDn * cE;
		float kappa = 1 - ::std::exp(-2 * sigmap_t * (dr + zr));
		Ed += kappa * rhop * rhop * E;
	}
	return Ed / nSamples;
}

float BeamDiffusionSS(float sigma_s, float sigma_a, float g, float eta, float r)
{
	// Compute material parameters and minimum $t$ below the critical angle
	float sigma_t = sigma_a + sigma_s, rho = sigma_s / sigma_t;
	float tCrit = r * ::std::sqrt(::std::max(0.f, (eta * eta) - 1));

	float Ess = 0;
	const int nSamples = 100;
	for (int i = 0; i < nSamples; ++i)
	{
		// Evaluate single-scattering integrand and add to _Ess_
		float ti = tCrit + SampleExponential((i + 0.5f) / nSamples, sigma_t);
		// Determine length $d$ of connecting segment and $\cos\theta_\roman{o}$
		float d = std::sqrt(Sqr(r) + Sqr(ti));
		float cosTheta_o = ti / d;

		// Add contribution of single scattering at depth $t$
		Ess += rho * ::std::exp(-sigma_t * (d + tCrit)) / Sqr(d) *
			HenyeyGreenstein(cosTheta_o, g) * (1 - FrDielectric(-cosTheta_o, eta)) *
			std::abs(cosTheta_o);
	}
	return Ess / nSamples;
}

void ComputeBeamDiffusionBSSRDF(float g, float eta, BSSRDFTable * t)
{
	// Choose radius values of the diffusion profile discretization
	t->radiusSamples[0] = 0;
	t->radiusSamples[1] = 2.5e-3f;
	for (int i = 2; i < t->radiusSamples.size(); ++i)
		t->radiusSamples[i] = t->radiusSamples[i - 1] * 1.2f;

	// Choose albedo values of the diffusion profile discretization
	for (int i = 0; i < t->rhoSamples.size(); ++i)
		t->rhoSamples[i] = (1 - ::std::exp(-8 * i / (float)(t->rhoSamples.size() - 1))) / (1 - ::std::exp(-8));

	for (int i = 2; i < t->rhoSamples.size(); ++i)
	{
		// Compute the diffusion profile for the _i_th albedo sample
		// Compute scattering profile for chosen albedo $\rho$
		size_t nSamples = t->radiusSamples.size();
		for (int j = 0; j < nSamples; ++j)
		{
			float rho = t->rhoSamples[i];
			float r = t->radiusSamples[j];
			t->profile[i * nSamples + j] = 2 * ::std::numbers::pi * r * (BeamDiffusionSS(rho, 1 - rho, g, eta, r) + BeamDiffusionMS(rho, 1 - rho, g, eta, r));
		}

		// Compute effective albedo $\rho_{\roman{eff}}$ and CDF for importance sampling
		t->rhoEff[i] = IntegrateCatmullRom(
			t->radiusSamples,
			::std::span<const float>(&t->profile[i * nSamples], nSamples),
			::std::span<float>(&t->profileCDF[i * nSamples], nSamples));
	}
}

::framework::graphics::ray_tracing::material_system::material_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer) :
	scene_renderer(scene_renderer),
	max_texture_count(2048),
	material_counter(0)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::sampler_create_info sampler_create_info;
		sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
		sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
		sampler_create_info.min_filter = ::framework::gpu::filter::linear;
		sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
		sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::repeat;
		sampler_create_info.mip_lod_bias = 0.0f;
		sampler_create_info.anisotropy_enable = false;
		sampler_create_info.max_anisotropy = 1.0f;
		sampler_create_info.compare_enable = false;
		sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
		sampler_create_info.min_lod = 0.0f;
		sampler_create_info.max_lod = 0.0f;
		sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
		sampler_create_info.unnormalized_coordinates = false;
		assert_framework_gpu_result(device->create_sampler(&sampler_create_info, nullptr, &this->sampler));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		// triangle_data
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 2;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// triangle_data
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 2;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::any_hit_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// linear_sampler
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 2;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampler;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::miss_bit | ::framework::gpu::shader_stage_flags::any_hit_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = &this->sampler;
		// textures
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::variable_descriptor_count_bit | ::framework::gpu::descriptor_binding_flags::partially_bound_bit | ::framework::gpu::descriptor_binding_flags::update_after_bind_bit;
		descriptor_set_layout_bindings[3].binding = ::framework::graphics::ray_tracing::material_system::textures_binding;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 2;
		descriptor_set_layout_bindings[3].descriptor_count = this->max_texture_count;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit | ::framework::gpu::shader_stage_flags::miss_bit | ::framework::gpu::shader_stage_flags::any_hit_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::update_after_bind_pool_bit;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->material_system_descriptor_set_layout));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::graphics::ray_tracing::material_device_data) * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->material_data_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->material_data_buffer;
		debug_utils_object_name_info.object_name = "material_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->material_data_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->material_data_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->material_data_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->material_data_buffer;
		bind_buffer_memory_info.memory = this->material_data_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(float) * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->bssrdf_table_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->bssrdf_table_buffer;
		debug_utils_object_name_info.object_name = "material_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->bssrdf_table_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->bssrdf_table_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->bssrdf_table_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->bssrdf_table_buffer;
		bind_buffer_memory_info.memory = this->bssrdf_table_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->material_system_descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = &this->max_texture_count;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set));
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	descriptor_buffer_infos[0].buffer = this->material_data_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 128;

	descriptor_buffer_infos[1].buffer = this->bssrdf_table_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = sizeof(float);

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	write_descriptor_sets[0].dst_set = this->descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;

	write_descriptor_sets[1].dst_set = this->descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_semaphore));
}

::framework::graphics::ray_tracing::material_system::~material_system()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	device->destroy_buffer(this->material_data_buffer, nullptr);
	device->free_memory(this->material_data_device_memory, nullptr);
}

::framework::graphics::material * ::framework::graphics::ray_tracing::material_system::create_material(::framework::graphics::material_create_info const & create_info)
{
	return new ::framework::graphics::ray_tracing::material(this, create_info, this->material_counter++);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::material_system::add_material_to_update)(::framework::graphics::ray_tracing::material * material)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->materials_to_update_mutex.scoped_lock();
	this->materials_to_update.push_back(material);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::material_system::update)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const transfer_command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->materials_to_update_mutex.scoped_lock();
		for (::framework::graphics::ray_tracing::material * material : this->materials_to_update)
		{
			co_await transfer_command_context->update_buffer(this->material_data_buffer, co_await material->get_data_blob_and_commit(), material->get_id());
		}
		this->materials_to_update.clear();
	}

	static bool init_bssrdf_table = true;
	if (init_bssrdf_table)
	{
		::BSSRDFTable bssrdf_table(100, 64);
		::ComputeBeamDiffusionBSSRDF(0.0f, 1.5f, &bssrdf_table);

		::std::uint32_t offset = 0;
		::std::uint32_t size = bssrdf_table.rhoSamples.size() * sizeof(float);
		co_await transfer_command_context->update_buffer(this->bssrdf_table_buffer, bssrdf_table.rhoSamples.data(), offset, size);
		offset += size;
		size = bssrdf_table.radiusSamples.size() * sizeof(float);
		co_await transfer_command_context->update_buffer(this->bssrdf_table_buffer, bssrdf_table.radiusSamples.data(), offset, size);
		offset += size;
		size = bssrdf_table.profile.size() * sizeof(float);
		co_await transfer_command_context->update_buffer(this->bssrdf_table_buffer, bssrdf_table.profile.data(), offset, size);
		offset += size;
		size = bssrdf_table.rhoEff.size() * sizeof(float);
		co_await transfer_command_context->update_buffer(this->bssrdf_table_buffer, bssrdf_table.rhoEff.data(), offset, size);
		offset += size;
		size = bssrdf_table.profileCDF.size() * sizeof(float);
		co_await transfer_command_context->update_buffer(this->bssrdf_table_buffer, bssrdf_table.profileCDF.data(), offset, size);

		init_bssrdf_table = false;
	}
	assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
}
