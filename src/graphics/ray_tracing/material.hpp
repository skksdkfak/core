#pragma once

#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "graphics/material.hpp"
#include "graphics/ray_tracing/image.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "gpu/core.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::ray_tracing
{
	class material_system;

	struct material_device_data
	{
		::glm::vec2 alpha;
		typename ::framework::graphics::ray_tracing::spectrum::spectrum_pointer ior;
		::framework::graphics::ray_tracing::image::device_blob diffuse_image;
		::framework::graphics::ray_tracing::image::device_blob emissive_image;
	};

	enum class material_type
	{
		standard_material = 0,
	};

	struct material_header
	{
		::glm::uvec2 data;

		enum material_flags
		{
			material_type_bits = 16,

			material_type_offset = 0,
			emissive_flag_offset = ::framework::graphics::ray_tracing::material_header::material_flags::material_type_offset + ::framework::graphics::ray_tracing::material_header::material_flags::material_type_bits,
			thin_surface_flag_offset = ::framework::graphics::ray_tracing::material_header::material_flags::emissive_flag_offset + 1
		};
	};

	struct material_payload
	{
		::std::uint32_t data[30];
	};

	struct material_data_blob
	{
		::framework::graphics::ray_tracing::material_header header;
		::framework::graphics::ray_tracing::material_payload payload;
	};

	class material : public ::framework::graphics::material
	{
	public:
		explicit material(::framework::graphics::ray_tracing::material_system * material_system, ::framework::graphics::material_create_info const & create_info, ::std::uint32_t id);

		void set_thin_surface(bool thin_surface) override;

		void set_roughness(::glm::vec2 roughness) override;

		::framework::graphics::image * get_diffuse_texture() const override;

		void set_diffuse_texture(::framework::graphics::image * image) override;

		void set_emissive_image(::framework::graphics::image * emissive_image) override;

		::std::uint32_t get_id() const noexcept;

		bool is_emissive() const noexcept;

		::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::material_data_blob> get_data_blob_and_commit();

	private:
		::framework::coroutine::self_destructible_coroutine initialize_device() noexcept;

		::framework::coroutine::self_destructible_coroutine set_thin_surface_device(bool thin_surface) noexcept;

		::framework::coroutine::self_destructible_coroutine set_roughness_device(::glm::vec2 roughness) noexcept;

		::framework::coroutine::self_destructible_coroutine set_diffuse_image_device(::framework::graphics::image * image) noexcept;

		::framework::coroutine::self_destructible_coroutine set_emissive_color_device(::framework::graphics::image * emissive_image) noexcept;

		bool add_to_update() noexcept;

		enum device_data_state : ::std::uint8_t
		{
			perform_update = 1 << 0,
			ior_set = 1 << 1,
			image_set = 1 << 2,

			ready_for_update = ::framework::graphics::ray_tracing::material::device_data_state::perform_update | ::framework::graphics::ray_tracing::material::device_data_state::ior_set | ::framework::graphics::ray_tracing::material::device_data_state::image_set
		};

		::framework::graphics::ray_tracing::material_system * material_system;
		::std::uint32_t id;
		::framework::graphics::ray_tracing::material_header header;
		::framework::graphics::ray_tracing::material_device_data device_data;
		::std::uint8_t device_data_state;
		::framework::coroutine::mutex device_data_mutex;
		::framework::graphics::ray_tracing::spectrum * ior;
		::framework::graphics::ray_tracing::image * diffuse_image;
		::framework::graphics::ray_tracing::image * emissive_image;
	};
}

#include "graphics/ray_tracing/material.inl"