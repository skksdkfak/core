#include "ray_tracing/nis_path_integrator/config.hlsl"

[[vk::binding(3, 0)]]
RWStructuredBuffer<float16_t> nis_samples_buffer : register(u2, space0);

[[vk::binding(4, 0)]]
RWStructuredBuffer<float16_t> nis_spatial_features_buffer : register(u3, space0);

[[vk::binding(6, 0)]]
RWStructuredBuffer<float16_t> nis_target_buffer : register(u5, space0);

[[vk::binding(9, 0)]]
RWStructuredBuffer<float16_t> sample_buffer : register(u8, space0);

[[vk::binding(10, 0)]]
RWStructuredBuffer<uint32_t> batch_size_buffer : register(u9, space0);

[[vk::binding(12, 0)]]
RWStructuredBuffer<float16_t> nis_features_buffer : register(u2, space0);

[[vk::binding(13, 0)]]
RWStructuredBuffer<uint32_t> inference_batch_size_buffer : register(u12, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint32_t size = inference_batch_size_buffer[0];
	const uint32_t global_invocation_id = dispatch_thread_id.x;
	if (global_invocation_id >= size)
	{
		return;
	}

	const uint32_t nis_spatial_features_offset = global_invocation_id * framework::nis_spatial_feature_stride;
	nis_features_buffer[global_invocation_id + ::framework::x_a_row_index * size] = nis_samples_buffer[global_invocation_id * 2 + 0];
}
