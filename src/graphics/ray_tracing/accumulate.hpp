#pragma once

#include "gpu/core.hpp"
#include <glm/glm.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class accumulate
	{
	public:
		accumulate(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		~accumulate();

		void execute(::framework::gpu::command_buffer * command_buffer, bool enable_tone_mapping);

		void reset();

		void resize();

		void recompile_shaders();

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		void create_screen_buffers();

		void create_pipelines();

		void update_descriptor_set();

		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::descriptor_set_layout * accumulate_descriptor_set_layout;
		::framework::gpu::descriptor_set * accumulate_descriptor_set;
		::framework::gpu::shader_module * accumulate_shader_module;
		::framework::gpu::pipeline_layout * accumulate_pipeline_layout;
		::framework::gpu::pipeline * accumulate_pipeline;
		::framework::gpu::image * last_frame_sum_low_image;
		::framework::gpu::image_view * last_frame_sum_low_image_view;
		::framework::gpu::device_memory * last_frame_sum_low_device_memory;
		::framework::gpu::image * last_frame_sum_high_image;
		::framework::gpu::image_view * last_frame_sum_high_image_view;
		::framework::gpu::device_memory * last_frame_sum_high_device_memory;
		::framework::gpu::host_descriptor_handle last_frame_sum_low_host_descriptor_handle;
		::framework::gpu::host_descriptor_handle last_frame_sum_high_host_descriptor_handle;
		::std::uint32_t sample_count;
	};
}