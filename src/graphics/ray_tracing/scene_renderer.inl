inline void(::framework::graphics::ray_tracing::scene_renderer::reset_nis)() noexcept
{
	this->is_reset_nis = true;
}

inline bool ::framework::graphics::ray_tracing::scene_renderer::get_enable_nee() const noexcept
{
	return this->enable_nee;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_enable_nee(bool enable_nee) noexcept
{
	this->enable_nee = enable_nee;
}

inline bool ::framework::graphics::ray_tracing::scene_renderer::get_enable_ncv() const noexcept
{
	return this->enable_ncv;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_enable_ncv(bool enable_ncv) noexcept
{
	this->enable_ncv = enable_ncv;
}

inline bool ::framework::graphics::ray_tracing::scene_renderer::get_enable_nis() const noexcept
{
	return this->enable_nis;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_enable_nis(bool enable_nis) noexcept
{
	this->enable_nis = enable_nis;
}

inline bool ::framework::graphics::ray_tracing::scene_renderer::get_enable_nis_training() const noexcept
{
	return this->enable_nis_training;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_enable_nis_training(bool enable_nis_training) noexcept
{
	this->enable_nis_training = enable_nis_training;
}

inline enum class ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type(::framework::graphics::ray_tracing::scene_renderer::get_nis_visualization_type)() const noexcept
{
	return this->nis_visualization_type;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_nis_visualization_type(enum class ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type nis_visualization_type) noexcept
{
	this->nis_visualization_type = nis_visualization_type;
}

inline void ::framework::graphics::ray_tracing::scene_renderer::set_nis_visualization_screen_coordinates(::glm::uvec2 const & coordinates) noexcept
{
	this->nis_visualization_coordinates = coordinates;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::scene_renderer::get_current_resource_index)() const noexcept
{
	return this->current_resource_index;
}

inline ::framework::graphics::ray_tracing::scene_view_perspective * const * ::framework::graphics::ray_tracing::scene_renderer::get_scene_views() const noexcept
{
	return this->scene_views.data();
}

inline ::framework::command_buffer_manager & ::framework::graphics::ray_tracing::scene_renderer::get_command_buffer_manager() noexcept
{
	return this->command_buffer_manager;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::scene_renderer::get_frame_semaphore)() const noexcept
{
	return this->frame_semaphore;
}

inline ::std::uint64_t(::framework::graphics::ray_tracing::scene_renderer::get_last_frame_index)() const noexcept
{
	return this->frame_index;
}

inline ::framework::gpu_context * ::framework::graphics::ray_tracing::scene_renderer::get_gpu_context() const noexcept
{
	return this->gpu_context;
}

inline ::framework::profiler * (::framework::graphics::ray_tracing::scene_renderer::get_profiler)() const noexcept
{
	return this->profiler;
}

inline ::framework::graphics::ray_tracing::material_system * ::framework::graphics::ray_tracing::scene_renderer::get_material_system() const noexcept
{
	return this->material_system.get();
}

inline ::framework::graphics::ray_tracing::aov * (::framework::graphics::ray_tracing::scene_renderer::get_aov)() const noexcept
{
	return this->aov.get();
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::scene_renderer::get_frame_buffer_count)() const noexcept
{
	return this->frame_buffer_count;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::scene_renderer::get_width)() const noexcept
{
	return this->width;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::scene_renderer::get_height)() const noexcept
{
	return this->height;
}

inline ::framework::gpu::image * ::framework::graphics::ray_tracing::scene_renderer::get_color_image() const noexcept
{
	return this->color_image;
}

inline ::framework::gpu::image_view * ::framework::graphics::ray_tracing::scene_renderer::get_color_image_view() const noexcept
{
	return this->color_image_view;
}

inline ::framework::gpu::image_view * ::framework::graphics::ray_tracing::scene_renderer::get_output_color_image_view() const noexcept
{
	return this->output_color_image_view;
}

inline ::framework::gpu::image * ::framework::graphics::ray_tracing::scene_renderer::get_nis_pdf_image() const noexcept
{
	return this->nis_pdf_image;
}

inline ::framework::gpu::image_view * framework::graphics::ray_tracing::scene_renderer::get_nis_pdf_image_view() const noexcept
{
	return this->nis_pdf_image_view;
}

inline ::framework::gpu::image * ::framework::graphics::ray_tracing::scene_renderer::get_ground_truth_distribution_image() const noexcept
{
	return this->ground_truth_distribution_image;
}

inline ::framework::gpu::image_view * framework::graphics::ray_tracing::scene_renderer::get_ground_truth_distribution_image_view() const noexcept
{
	return this->ground_truth_distribution_image_view;
}

inline ::framework::gpu::descriptor_set * ::framework::graphics::ray_tracing::scene_renderer::get_attachments_descriptor_set() const noexcept
{
	return this->attachments_descriptor_set;
}

inline ::framework::gpu::pipeline_layout * ::framework::graphics::ray_tracing::scene_renderer::get_path_tracer_pipeline_layout() const noexcept
{
	return this->path_tracer_pipeline_layout;
}

inline ::framework::gpu::descriptor_set_layout * ::framework::graphics::ray_tracing::scene_renderer::get_path_tracer_descriptor_set_layout() const noexcept
{
	return this->path_tracer_descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set_layout * ::framework::graphics::ray_tracing::scene_renderer::get_scene_descriptor_set_layout() const noexcept
{
	return this->scene_descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set * ::framework::graphics::ray_tracing::scene_renderer::get_path_tracer_descriptor_set() const noexcept
{
	return this->path_tracer_descriptor_set;
}

inline ::framework::gpu::descriptor_set * ::framework::graphics::ray_tracing::scene_renderer::get_scene_descriptor_set() const noexcept
{
	return this->scene_descriptor_set;
}

inline ::framework::gpu::device_size(::framework::graphics::ray_tracing::scene_renderer::get_light_field_grid_encoding_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = 4u * sizeof(::framework::gpu::dispatch_indirect_command); /* [0-1] - scan, [2] - concatenate_features_pipeline, [3] - loss */
	::framework::gpu::device_size const dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(dispatch_indirect_command_offset, this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);
	return dispatch_indirect_command_aligned_offset;
}

inline ::framework::gpu::device_size(::framework::graphics::ray_tracing::scene_renderer::get_light_field_one_blob_encoding_dispatch_indirect_command_offset)() const noexcept
{
	::framework::gpu::device_size const dispatch_indirect_command_offset = this->get_light_field_grid_encoding_dispatch_indirect_command_offset() + this->light_field_grid_encoding_execution_policy->get_dispatch_indirect_command_size();
	::framework::gpu::device_size const dispatch_indirect_command_aligned_offset = ::framework::numeric::align_up<::framework::gpu::device_size>(dispatch_indirect_command_offset, this->gpu_context->get_physical_device_properties().limits.min_storage_buffer_offset_alignment);
	return dispatch_indirect_command_aligned_offset;
}