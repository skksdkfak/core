#define GLM_FORCE_SWIZZLE
#include "graphics/ray_tracing/light.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"

::framework::graphics::ray_tracing::point_light::point_light(::framework::graphics::ray_tracing::scene * scene) :
	scene(scene)
{
	this->device_blob.position_and_scale = ::glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

void ::framework::graphics::ray_tracing::point_light::set_position(::glm::vec3 const & position)
{
	this->set_position_device(position);
}

void ::framework::graphics::ray_tracing::point_light::set_intensity(::framework::physics::spectrum const & intensity)
{
	this->set_intensity_device(intensity);
}

void ::framework::graphics::ray_tracing::point_light::set_scale(float scale)
{
	this->set_scale_device(scale);
}

::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::light::device_blob>(::framework::graphics::ray_tracing::point_light::get_data)()
{
	::framework::graphics::ray_tracing::light::device_blob device_blob;
	device_blob.header = static_cast<::std::uint32_t>(::framework::graphics::ray_tracing::light::type::point_light);
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	static_assert(sizeof(this->device_blob) <= sizeof(device_blob.data));
	::std::memcpy(device_blob.data, &this->device_blob, sizeof(this->device_blob));
	co_return device_blob;
}

::framework::coroutine::self_destructible_coroutine (::framework::graphics::ray_tracing::point_light::set_position_device)(::glm::vec3 const position) noexcept
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	this->device_blob.position_and_scale.xyz = position;
	this->changes |= ::framework::graphics::ray_tracing::light::changes::position;
}

::framework::coroutine::self_destructible_coroutine (::framework::graphics::ray_tracing::point_light::set_intensity_device)(::framework::physics::spectrum const & intensity) noexcept
{
	::framework::graphics::ray_tracing::densely_sampled_spectrum * densely_sampled_spectrum = this->scene->get_scene_renderer()->get_spectrum_system()->create_densely_sampled_spectrum(intensity, false);
	co_await densely_sampled_spectrum->get_initialization_shared_task();
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->intensity = ::std::shared_ptr<::framework::graphics::ray_tracing::densely_sampled_spectrum>(densely_sampled_spectrum);
		this->device_blob.intensity = this->intensity->get_densely_sampled_spectrum_device_blob();
		this->changes |= ::framework::graphics::ray_tracing::light::changes::intensity;
	}
}

::framework::coroutine::self_destructible_coroutine (::framework::graphics::ray_tracing::point_light::set_scale_device)(float scale) noexcept
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	this->device_blob.position_and_scale.w = scale;
	this->changes |= ::framework::graphics::ray_tracing::light::changes::intensity;
}

::framework::graphics::ray_tracing::image_infinite_light::image_infinite_light(::framework::graphics::ray_tracing::scene * scene) :
	scene(scene)
{
	this->device_blob.scale = 1.0f;
	this->changes |= ::framework::graphics::ray_tracing::light::changes::active;
}

void ::framework::graphics::ray_tracing::image_infinite_light::set_image(::framework::graphics::image * image)
{
	this->set_image_device(image);
}

void ::framework::graphics::ray_tracing::image_infinite_light::set_scale(float scale)
{
	this->set_scale_device(scale);
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::image_infinite_light::set_image_device)(::framework::graphics::image * image) noexcept
{
	co_await image->get_initialization_shared_task();
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->image = static_cast<::framework::graphics::ray_tracing::image *>(image);
		this->device_blob.image = this->image->get_device_blob();
	}
	co_await this->scene->add_light(this);
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::image_infinite_light::set_scale_device)(float scale) noexcept
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	this->device_blob.scale = scale;
}

::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::light::device_blob>(::framework::graphics::ray_tracing::image_infinite_light::get_data)()
{
	::framework::graphics::ray_tracing::light::device_blob device_blob;
	device_blob.header = static_cast<::std::uint32_t>(::framework::graphics::ray_tracing::light::type::image_infinite_light);
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	static_assert(sizeof(this->device_blob) <= sizeof(device_blob.data));
	::std::memcpy(device_blob.data, &this->device_blob, sizeof(this->device_blob));
	co_return device_blob;
}

::framework::coroutine::immediate_task<struct::framework::graphics::ray_tracing::image_infinite_light::device_blob> (::framework::graphics::ray_tracing::image_infinite_light::get_image_infinite_light_device_data)() noexcept
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	co_return this->device_blob;
}
