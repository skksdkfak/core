#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "gpu/utility.hpp"
#include <glm/glm.hpp>

::framework::graphics::ray_tracing::triangle_mesh_geometry::triangle_mesh_geometry(::std::uint32_t id, ::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::triangle_mesh_geometry_create_info const & create_info) :
	id(id),
	scene_renderer(scene_renderer),
	initialization_shared_task(this->initialize(create_info))
{
}

::framework::graphics::ray_tracing::triangle_mesh_geometry::~triangle_mesh_geometry()
{
	this->scene_renderer->get_gpu_context()->get_device()->destroy_acceleration_structure_geometry_set(this->bottom_acceleration_structure_geometry_set, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_buffer(this->bottom_level_acceleration_structure_buffer, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_acceleration_structure(this->bottom_level_acceleration_structure, nullptr);
}

::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::triangle_mesh_geometry::initialize)(::framework::graphics::triangle_mesh_geometry_create_info const & create_info)
{
	this->submeshes.insert(this->submeshes.begin(), create_info.submeshes, create_info.submeshes + create_info.submesh_count);

	class ::framework::command_context * transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::command_context * compute_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->acquire_command_context();

	::framework::gpu::device_size const indices_memory_size = sizeof(::std::uint32_t) * create_info.index_count;
	::framework::gpu::device_size const vertices_memory_size = sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout) * create_info.vertex_count;
	{
		struct ::framework::linear_allocator::allocation const allocation = co_await transfer_command_context->get_linear_allocator().allocate(indices_memory_size + vertices_memory_size, 1);
		::framework::gpu::command_buffer * transfer_command_buffer = transfer_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));

		::framework::gpu::device_size staging_buffer_data_offset = allocation.offset;
		{
			this->indices_memory_offset = this->scene_renderer->allocate_indices_device_memory(indices_memory_size, 1);

			assert(this->indices_memory_offset + indices_memory_size <= this->scene_renderer->get_indices_buffer_size());

			::std::memcpy(static_cast<::std::byte *>(allocation.mapped_buffer_data) + staging_buffer_data_offset, create_info.indices, indices_memory_size);

			::framework::gpu::buffer_copy buffer_copy;
			buffer_copy.src_offset = staging_buffer_data_offset;
			buffer_copy.dst_offset = this->indices_memory_offset;
			buffer_copy.size = indices_memory_size;
			transfer_command_buffer->copy_buffer(allocation.buffer, this->scene_renderer->get_indices_buffer(), 1, &buffer_copy);

			staging_buffer_data_offset += indices_memory_size;
		}

		::std::vector<::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout> vertices(create_info.vertex_count);
		for (::std::uint32_t i = 0; i < create_info.vertex_count; i++)
		{
			vertices[i].position = create_info.positions[i];
			vertices[i].normal = create_info.normals[i];
			if (create_info.uvs)
				vertices[i].uv = create_info.uvs[i];
		}

		{
			this->vertices_memory_offset = this->scene_renderer->allocate_vertices_device_memory(vertices_memory_size, 1);

			assert(this->vertices_memory_offset + vertices_memory_size <= this->scene_renderer->get_vertices_buffer_size());

			::std::memcpy(static_cast<::std::byte *>(allocation.mapped_buffer_data) + staging_buffer_data_offset, vertices.data(), vertices_memory_size);

			::framework::gpu::buffer_copy buffer_copy;
			buffer_copy.src_offset = staging_buffer_data_offset;
			buffer_copy.dst_offset = this->vertices_memory_offset;
			buffer_copy.size = vertices_memory_size;

			transfer_command_buffer->copy_buffer(allocation.buffer, this->scene_renderer->get_vertices_buffer(), 1, &buffer_copy);

			staging_buffer_data_offset += vertices_memory_size;
		}

		{
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().compute;
			buffer_memory_barriers[0].buffer = this->scene_renderer->get_indices_buffer();
			buffer_memory_barriers[0].offset = this->indices_memory_offset;
			buffer_memory_barriers[0].size = indices_memory_size;

			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().compute;
			buffer_memory_barriers[1].buffer = this->scene_renderer->get_vertices_buffer();
			buffer_memory_barriers[1].offset = this->vertices_memory_offset;
			buffer_memory_barriers[1].size = vertices_memory_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			transfer_command_buffer->pipeline_barrier(&dependency_info);
		}

		assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = compute_command_context->get_semaphore();
		signal_semaphore_info.value = 0;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		signal_semaphore_info.device_index = 0;
		co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit(transfer_command_context, 0, nullptr, 1, &signal_semaphore_info);
	}

	{
		::framework::gpu::acceleration_structure_build_sizes_info acceleration_structure_build_sizes_info;
		::std::vector<::framework::gpu::acceleration_structure_geometry> acceleration_structure_geometries(create_info.submesh_count);

		for (::std::uint32_t i = 0; i < create_info.submesh_count; i++)
		{
			acceleration_structure_geometries[i].flags = ::framework::gpu::geometry_flags::opaque_bit | ::framework::gpu::geometry_flags::no_duplicate_any_hit_invocation_bit;
			acceleration_structure_geometries[i].primitive_count = create_info.submeshes[i].index_count / 3;
			acceleration_structure_geometries[i].primitive_offset = create_info.submeshes[i].index_offset * sizeof(::std::uint32_t);
			acceleration_structure_geometries[i].geometry.triangles.vertex_format = ::framework::gpu::format::r32g32b32_sfloat;
			acceleration_structure_geometries[i].geometry.triangles.vertex_data.device_address = this->scene_renderer->get_vertices_device_address() + this->vertices_memory_offset;
			acceleration_structure_geometries[i].geometry.triangles.vertex_stride = sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout);
			acceleration_structure_geometries[i].geometry.triangles.vertex_count = create_info.vertex_count;
			acceleration_structure_geometries[i].geometry.triangles.first_vertex = create_info.submeshes[i].vertex_offeset;
			acceleration_structure_geometries[i].geometry.triangles.index_type = ::framework::gpu::index_type::uint32;
			acceleration_structure_geometries[i].geometry.triangles.index_data.device_address = this->scene_renderer->get_indices_device_address() + this->indices_memory_offset;
			acceleration_structure_geometries[i].geometry.triangles.transform_data.device_address = 0;
			acceleration_structure_geometries[i].geometry.triangles.transform_offset = 0;
		}

		::framework::gpu::acceleration_structure_geometry_set_create_info acceleration_structure_geometry_set_create_info;
		acceleration_structure_geometry_set_create_info.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
		acceleration_structure_geometry_set_create_info.geometry_count = create_info.submesh_count;
		acceleration_structure_geometry_set_create_info.geometry_type = ::framework::gpu::geometry_type::triangles;
		assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->create_acceleration_structure_geometry_set(&acceleration_structure_geometry_set_create_info, nullptr, &this->bottom_acceleration_structure_geometry_set));

		::framework::gpu::write_acceleration_structure_geometry write_acceleration_structure_geometry;
		write_acceleration_structure_geometry.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
		write_acceleration_structure_geometry.first_geometry = 0;
		write_acceleration_structure_geometry.geometry_count = create_info.submesh_count;
		write_acceleration_structure_geometry.geometry_type = ::framework::gpu::geometry_type::triangles;
		write_acceleration_structure_geometry.data.geometries = acceleration_structure_geometries.data();
		this->bottom_acceleration_structure_geometry_set->write_data(write_acceleration_structure_geometry);

		::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
		acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
		acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
		acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
		acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.dst_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.geometry_count = create_info.submesh_count;
		acceleration_structure_build_geometry_info.first_geometry = 0;
		acceleration_structure_build_geometry_info.geometries = this->bottom_acceleration_structure_geometry_set;
		acceleration_structure_build_geometry_info.scratch_data.device_address = 0;
		this->scene_renderer->get_gpu_context()->get_device()->get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type::device, &acceleration_structure_build_geometry_info, &acceleration_structure_build_sizes_info);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::raytracing_acceleration_structure_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->create_buffer(&buffer_create_info, nullptr, &this->bottom_level_acceleration_structure_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->bottom_level_acceleration_structure_buffer;
		this->scene_renderer->get_gpu_context()->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->bottom_level_acceleration_structure_buffer;
		bind_buffer_memory_info.memory = this->scene_renderer->get_bottom_level_acceleration_structure_device_memory();
		bind_buffer_memory_info.memory_offset = this->scene_renderer->allocate_bottom_level_acceleration_structure_device_memory(memory_requirements.size, memory_requirements.alignment);
		assert(bind_buffer_memory_info.memory_offset + memory_requirements.size <= this->scene_renderer->get_bottom_level_acceleration_structure_device_memory_size());
		assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::acceleration_structure_create_info acceleration_structure_create_info;
		acceleration_structure_create_info.create_flags = ::framework::gpu::acceleration_structure_create_flags::none;
		acceleration_structure_create_info.buffer = this->bottom_level_acceleration_structure_buffer;
		acceleration_structure_create_info.offset = 0;
		acceleration_structure_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
		acceleration_structure_create_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
		acceleration_structure_create_info.device_address = 0;
		assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->create_acceleration_structure(&acceleration_structure_create_info, nullptr, &this->bottom_level_acceleration_structure));
	}

	{
		::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
		acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
		acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
		acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
		acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.dst_acceleration_structure = this->bottom_level_acceleration_structure;
		acceleration_structure_build_geometry_info.geometry_count = create_info.submesh_count;
		acceleration_structure_build_geometry_info.first_geometry = 0;
		acceleration_structure_build_geometry_info.geometries = this->bottom_acceleration_structure_geometry_set;
		acceleration_structure_build_geometry_info.scratch_data.device_address = this->scene_renderer->get_bottom_level_acceleration_structure_scratch_device_or_host_address().device_address;

		::framework::gpu::command_buffer * compute_command_buffer = compute_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
		command_buffer_begin_info.inheritance_info = nullptr;
		compute_command_buffer->begin_command_buffer(&command_buffer_begin_info);

		{
			::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
			buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
			buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
			buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().compute;
			buffer_memory_barriers[0].buffer = this->scene_renderer->get_indices_buffer();
			buffer_memory_barriers[0].offset = this->indices_memory_offset;
			buffer_memory_barriers[0].size = indices_memory_size;

			buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
			buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
			buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
			buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
			buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
			buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
			buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
			buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
			buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().compute;
			buffer_memory_barriers[1].buffer = this->scene_renderer->get_vertices_buffer();
			buffer_memory_barriers[1].offset = this->vertices_memory_offset;
			buffer_memory_barriers[1].size = vertices_memory_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
			dependency_info.buffer_memory_barriers = buffer_memory_barriers;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			compute_command_buffer->pipeline_barrier(&dependency_info);
		}

		compute_command_buffer->build_acceleration_structures(1, &acceleration_structure_build_geometry_info);
		assert_framework_gpu_result(compute_command_buffer->end_command_buffer());

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = compute_command_context->get_semaphore();
		wait_semaphore_info.value = 0;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		wait_semaphore_info.device_index = 0;
		co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->submit_and_wait(compute_command_context, 1, &wait_semaphore_info, 0, nullptr);
		co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->release_command_context(compute_command_context);
		co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);

		this->scene_renderer->get_gpu_context()->get_device()->destroy_acceleration_structure_geometry_set(this->bottom_acceleration_structure_geometry_set, nullptr);

		::framework::gpu::acceleration_structure_device_address_info acceleration_structure_device_address_info;
		acceleration_structure_device_address_info.acceleration_structure = this->bottom_level_acceleration_structure;
		this->bottom_level_acceleration_structure_device_address = this->scene_renderer->get_gpu_context()->get_device()->get_acceleration_structure_device_address(&acceleration_structure_device_address_info);
	}
}