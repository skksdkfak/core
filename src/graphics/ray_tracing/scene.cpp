#include "command_context.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "coroutine/thread_pool_task.hpp"
#include "coroutine/when_all_ready.hpp"
#include "gpu/utility.hpp"
#include "graphics/ray_tracing/light.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/scene_geometry.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include "graphics/ray_tracing/volume_geometry.hpp"
#include "numeric.hpp"
#include "profiler.hpp"
#include <glm/glm.hpp>
#include <cstddef>

::std::uint32_t constexpr num_objects = 1 << 14;

::framework::graphics::ray_tracing::scene::scene(::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::scene_create_info * create_info) :
	scene_renderer(scene_renderer),
	geometry_instance_id_counter(0),
	instance_counter(0)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::semaphore_create_info semaphore_create_info;
		semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
		semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
		semaphore_create_info.initial_value = 0;
		assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->initialize_geometries_semaphore));
		assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_transforms_semaphore));
		assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_lights_semaphore));
		assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_semaphore));
	}

	{
		this->uniform_buffer_alignment = ::framework::numeric::align_up(sizeof(struct ::framework::graphics::ray_tracing::scene::uniform_buffer), this->scene_renderer->get_gpu_context()->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->uniform_buffer_alignment * scene_renderer->get_frame_buffer_count();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->uniform_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->uniform_buffer;
		debug_utils_object_name_info.object_name = "uniform_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->uniform_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->uniform_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->uniform_buffer_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->uniform_buffer;
		bind_buffer_memory_info.memory = this->uniform_buffer_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(device->map_memory(this->uniform_buffer_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&this->uniform_buffer_mapped_data)));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(struct ::framework::graphics::ray_tracing::light::device_blob) * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->lights_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->lights_buffer;
		debug_utils_object_name_info.object_name = "lights_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->lights_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->lights_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->lights_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->lights_buffer;
		bind_buffer_memory_info.memory = this->lights_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(struct ::framework::graphics::ray_tracing::image_infinite_light::device_blob) * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->infinite_lights_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->infinite_lights_buffer;
		debug_utils_object_name_info.object_name = "infinite_lights_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->infinite_lights_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->infinite_lights_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->lights_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->infinite_lights_buffer;
		bind_buffer_memory_info.memory = this->lights_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::glm::mat4) * 4096;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->world_matrices_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->world_matrices_buffer;
		debug_utils_object_name_info.object_name = "world_matrices_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->world_matrices_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->world_matrices_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->world_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->world_matrices_buffer;
		bind_buffer_memory_info.memory = this->world_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::glm::mat4) * 4096;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->inverse_transpose_world_matrices_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->inverse_transpose_world_matrices_buffer;
		debug_utils_object_name_info.object_name = "inverse_transpose_world_matrices_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->inverse_transpose_world_matrices_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->inverse_transpose_world_matrices_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->inverse_transpose_world_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->inverse_transpose_world_matrices_buffer;
		bind_buffer_memory_info.memory = this->inverse_transpose_world_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	// instances buffer
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->max_primitive_count * sizeof(::framework::gpu::acceleration_structure_instance);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::shader_device_address_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::acceleration_structure_build_input_read_only_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->instances_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->instances_buffer;
		debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::scene::instances_buffer";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->instances_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->instances_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->instances_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->instances_buffer;
		bind_buffer_memory_info.memory = this->instances_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->instances_buffer;
		this->instances_device_or_host_address.device_address = device->get_buffer_device_address(&buffer_device_address_info);
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->geometry_instances_buffer));

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->geometry_instances_buffer;

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->geometry_instances_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->geometry_instances_buffer;
		bind_buffer_memory_info.memory = this->geometry_instances_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	// top level acceleration structure memory
	::framework::gpu::acceleration_structure_build_sizes_info acceleration_structure_build_sizes_info;
	{
		::framework::gpu::acceleration_structure_geometry acceleration_structure_geometries[1];
		acceleration_structure_geometries[0].flags = ::framework::gpu::geometry_flags::opaque_bit;
		acceleration_structure_geometries[0].primitive_count = this->max_primitive_count;
		acceleration_structure_geometries[0].primitive_offset = 0;
		acceleration_structure_geometries[0].geometry.instances.array_of_pointers = false;
		acceleration_structure_geometries[0].geometry.instances.data.device_address = this->instances_device_or_host_address.device_address;

		::framework::gpu::acceleration_structure_geometry_set_create_info acceleration_structure_geometry_set_create_info;
		acceleration_structure_geometry_set_create_info.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
		acceleration_structure_geometry_set_create_info.geometry_count = static_cast<::std::uint32_t>(::std::size(acceleration_structure_geometries));
		acceleration_structure_geometry_set_create_info.geometry_type = ::framework::gpu::geometry_type::instances;
		assert_framework_gpu_result(device->create_acceleration_structure_geometry_set(&acceleration_structure_geometry_set_create_info, nullptr, &this->top_acceleration_structure_geometry_set));

		::framework::gpu::write_acceleration_structure_geometry write_acceleration_structure_geometry;
		write_acceleration_structure_geometry.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
		write_acceleration_structure_geometry.first_geometry = 0;
		write_acceleration_structure_geometry.geometry_count = static_cast<::std::uint32_t>(::std::size(acceleration_structure_geometries));
		write_acceleration_structure_geometry.geometry_type = ::framework::gpu::geometry_type::instances;
		write_acceleration_structure_geometry.data.geometries = acceleration_structure_geometries;
		this->top_acceleration_structure_geometry_set->write_data(write_acceleration_structure_geometry);

		::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
		acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::top_level;
		acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
		acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
		acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.dst_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.geometry_count = 1;
		acceleration_structure_build_geometry_info.first_geometry = 0;
		acceleration_structure_build_geometry_info.geometries = this->top_acceleration_structure_geometry_set;
		acceleration_structure_build_geometry_info.scratch_data.device_address = 0;
		device->get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type::device, &acceleration_structure_build_geometry_info, &acceleration_structure_build_sizes_info);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::raytracing_acceleration_structure_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &top_level_acceleration_structure_buffer));

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = top_level_acceleration_structure_buffer;

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &top_level_acceleration_structure_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = top_level_acceleration_structure_buffer;
		bind_buffer_memory_info.memory = top_level_acceleration_structure_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::acceleration_structure_create_info acceleration_structure_create_info;
		acceleration_structure_create_info.create_flags = ::framework::gpu::acceleration_structure_create_flags::none;
		acceleration_structure_create_info.buffer = top_level_acceleration_structure_buffer;
		acceleration_structure_create_info.offset = 0;
		acceleration_structure_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
		acceleration_structure_create_info.type = ::framework::gpu::acceleration_structure_type::top_level;
		acceleration_structure_create_info.device_address = 0;
		assert_framework_gpu_result(device->create_acceleration_structure(&acceleration_structure_create_info, nullptr, &this->top_level_acceleration_structure));
	}

	// scratch buffer
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = acceleration_structure_build_sizes_info.build_scratch_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &top_level_acceleration_structure_scratch_buffer));

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = top_level_acceleration_structure_scratch_buffer;

		::framework::gpu::memory_requirements memory_requirements;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &top_level_acceleration_structure_scratch_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = top_level_acceleration_structure_scratch_buffer;
		bind_buffer_memory_info.memory = top_level_acceleration_structure_scratch_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = top_level_acceleration_structure_scratch_buffer;
		top_level_acceleration_structure_scratch_device_or_host_address.device_address = device->get_buffer_device_address(&buffer_device_address_info);
	}

	this->light_collection = ::std::make_unique<::framework::graphics::ray_tracing::light_collection>(this);

	::framework::gpu::descriptor_set_layout * const scene_descriptor_set_layout = this->scene_renderer->get_scene_descriptor_set_layout();

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->scene_renderer->get_host_descriptor_pool();
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &scene_descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = 0;
	assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set));

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[10];
	// uniform_buffer
	descriptor_buffer_infos[0].buffer = this->uniform_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = this->uniform_buffer_alignment;
	descriptor_buffer_infos[0].stride = this->uniform_buffer_alignment;
	// world_matrices
	descriptor_buffer_infos[1].buffer = this->world_matrices_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = sizeof(::glm::mat3x4);
	// inverse_transpose_world_matrices
	descriptor_buffer_infos[2].buffer = this->inverse_transpose_world_matrices_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = sizeof(::glm::mat3x4);
	// geometry_instances
	descriptor_buffer_infos[3].buffer = this->geometry_instances_buffer;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::geometry_instance);
	// indices
	descriptor_buffer_infos[4].buffer = this->scene_renderer->get_indices_buffer();
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = sizeof(::std::uint32_t);
	// vertices
	descriptor_buffer_infos[5].buffer = this->scene_renderer->get_vertices_buffer();
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout);
	// lights
	descriptor_buffer_infos[6].buffer = this->lights_buffer;
	descriptor_buffer_infos[6].offset = 0;
	descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[6].stride = sizeof(::std::uint32_t);
	// infinite_lights
	descriptor_buffer_infos[7].buffer = this->infinite_lights_buffer;
	descriptor_buffer_infos[7].offset = 0;
	descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[7].stride = sizeof(::std::uint32_t);
	// emissive_triangles
	descriptor_buffer_infos[8].buffer = this->light_collection->emissive_triangle_list_buffer;
	descriptor_buffer_infos[8].offset = 0;
	descriptor_buffer_infos[8].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[8].stride = sizeof(::std::uint32_t);
	// instance_offset_buffer
	descriptor_buffer_infos[9].buffer = this->light_collection->instance_offset_buffer;
	descriptor_buffer_infos[9].offset = 0;
	descriptor_buffer_infos[9].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[9].stride = sizeof(::std::uint32_t);

	::framework::gpu::write_descriptor_set write_descriptor_sets[11];
	// uniform_buffer
	write_descriptor_sets[0].dst_set = this->descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	// raytracing_acceleration_structure
	write_descriptor_sets[1].dst_set = this->descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::acceleration_structure;
	write_descriptor_sets[1].acceleration_structures = &this->top_level_acceleration_structure;
	// world_matrices
	write_descriptor_sets[2].dst_set = this->descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[1];
	// inverse_transpose_world_matrices
	write_descriptor_sets[3].dst_set = this->descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[2];
	// geometry_instances
	write_descriptor_sets[4].dst_set = this->descriptor_set;
	write_descriptor_sets[4].dst_binding = 4;
	write_descriptor_sets[4].dst_array_element = 0;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[3];
	// indices
	write_descriptor_sets[5].dst_set = this->descriptor_set;
	write_descriptor_sets[5].dst_binding = 5;
	write_descriptor_sets[5].dst_array_element = 0;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[4];
	// vertices
	write_descriptor_sets[6].dst_set = this->descriptor_set;
	write_descriptor_sets[6].dst_binding = 6;
	write_descriptor_sets[6].dst_array_element = 0;
	write_descriptor_sets[6].descriptor_count = 1;
	write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[5];
	// lights
	write_descriptor_sets[7].dst_set = this->descriptor_set;
	write_descriptor_sets[7].dst_binding = 7;
	write_descriptor_sets[7].dst_array_element = 0;
	write_descriptor_sets[7].descriptor_count = 1;
	write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[6];
	// infinite_lights
	write_descriptor_sets[8].dst_set = this->descriptor_set;
	write_descriptor_sets[8].dst_binding = 8;
	write_descriptor_sets[8].dst_array_element = 0;
	write_descriptor_sets[8].descriptor_count = 1;
	write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[7];
	// emissive_triangles
	write_descriptor_sets[9].dst_set = this->descriptor_set;
	write_descriptor_sets[9].dst_binding = 9;
	write_descriptor_sets[9].dst_array_element = 0;
	write_descriptor_sets[9].descriptor_count = 1;
	write_descriptor_sets[9].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[9].buffer_info = &descriptor_buffer_infos[8];
	// instance_offset_buffer
	write_descriptor_sets[10].dst_set = this->descriptor_set;
	write_descriptor_sets[10].dst_binding = 10;
	write_descriptor_sets[10].dst_array_element = 0;
	write_descriptor_sets[10].descriptor_count = 1;
	write_descriptor_sets[10].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[10].buffer_info = &descriptor_buffer_infos[9];

	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	::framework::gpu::acceleration_structure_device_address_info acceleration_structure_device_address_info;
	acceleration_structure_device_address_info.acceleration_structure = this->top_level_acceleration_structure;
	top_level_acceleration_structure_device_address = device->get_acceleration_structure_device_address(&acceleration_structure_device_address_info);

	::std::uint32_t const shader_group_count = 4;
	::std::uint32_t const shader_binding_table_size = this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size * shader_group_count;

	::std::vector<::std::byte> shader_binding_table_data(shader_binding_table_size);
	assert_framework_gpu_result(device->get_ray_tracing_shader_group_handles(this->scene_renderer->get_pipeline(), 0, shader_group_count, shader_binding_table_size, shader_binding_table_data.data()));

	{
		this->closest_hit_shader_group_stride = ::framework::numeric::next_multiple<::framework::gpu::device_size>(this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size + sizeof(::framework::graphics::ray_tracing::scene::instance_shader_binding_table_record), this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment);
		this->closest_hit_shader_binding_table_size = ::framework::numeric::next_multiple<::framework::gpu::device_size>(this->closest_hit_shader_group_stride * this->hit_group_count * ::num_objects, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment);
		::framework::gpu::device_size const miss_shader_binding_table_size = ::framework::numeric::next_multiple<::framework::gpu::device_size>(this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_base_alignment) * 2;
		::framework::gpu::device_size const shader_binding_table_size = miss_shader_binding_table_size + this->closest_hit_shader_binding_table_size;

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = shader_binding_table_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::shader_binding_table_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->shader_binding_table_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->shader_binding_table_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->shader_binding_table_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->shader_binding_table_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->shader_binding_table_buffer;
		bind_buffer_memory_info.memory = this->shader_binding_table_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_device_address_info buffer_device_address_info;
		buffer_device_address_info.buffer = this->shader_binding_table_buffer;
		this->miss_shader_binding_table_device_address = device->get_buffer_device_address(&buffer_device_address_info);
		this->closest_hit_shader_binding_table_device_address = this->miss_shader_binding_table_device_address + miss_shader_binding_table_size;
		this->closest_hit_shader_binding_table_base_offset = miss_shader_binding_table_size;
		this->closest_hit_shader_binding_table_offset_to_shader_record = this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size;

		::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
		{
			::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(transfer_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			// miss shader binding table
			{
				co_await transfer_command_context->update_buffer(this->shader_binding_table_buffer, shader_binding_table_data.data(), 0, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
				::std::byte const * const shader_group_handle = shader_binding_table_data.data() + this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size;
				co_await transfer_command_context->update_buffer(this->shader_binding_table_buffer, shader_group_handle, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_alignment, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
			}

			// closest_hit shader binding table
			{
				::std::byte const * const shader_group_handle = shader_binding_table_data.data() + this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size * 2;
				//::std::byte const * const volume_shader_group_handle = shader_binding_table_data.data() + this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size * 4;

				for (::std::uint32_t i = 0; i < ::num_objects; i++)
				{
					::framework::gpu::device_size shader_group_base_offer = this->closest_hit_shader_binding_table_base_offset + i * this->closest_hit_shader_group_stride * this->hit_group_count;
					co_await transfer_command_context->update_buffer(this->shader_binding_table_buffer, shader_group_handle, shader_group_base_offer, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
				}
			}

			// subsurface scattering shader binding table
			{
				::std::byte const * const shader_group_handle = shader_binding_table_data.data() + this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size * 3;

				for (::std::uint32_t i = 0; i < ::num_objects; i++)
				{
					::framework::gpu::device_size shader_group_base_offer = this->closest_hit_shader_binding_table_base_offset + i * this->closest_hit_shader_group_stride * this->hit_group_count + this->closest_hit_shader_group_stride;
					co_await transfer_command_context->update_buffer(this->shader_binding_table_buffer, shader_group_handle, shader_group_base_offer, this->scene_renderer->get_gpu_context()->get_physical_device_properties().ray_tracing_pipeline_properties.shader_group_handle_size);
				}
			}

			assert_framework_gpu_result(transfer_command_context->get_command_buffer()->end_command_buffer());
			co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 0, nullptr, 0, nullptr);
			co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
		}());
	}
}

::framework::graphics::ray_tracing::scene::~scene()
{
	this->scene_renderer->get_gpu_context()->get_device()->unmap_memory(this->shader_binding_table_device_memory);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_acceleration_structure_geometry_set(this->top_acceleration_structure_geometry_set, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_buffer(this->top_level_acceleration_structure_scratch_buffer, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_buffer(this->instances_buffer, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_buffer(this->geometry_instances_buffer, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->free_memory(this->top_level_acceleration_structure_scratch_device_memory, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->free_memory(this->instances_device_memory, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->free_memory(this->geometry_instances_device_memory, nullptr);
}

::framework::graphics::triangle_mesh_scene_geometry * ::framework::graphics::ray_tracing::scene::create_triangle_mesh_scene_geometry(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info)
{
	::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry = new ::framework::graphics::ray_tracing::triangle_mesh_scene_geometry(this, create_info);
	return triangle_mesh_scene_geometry;
}

::framework::graphics::point_light * (::framework::graphics::ray_tracing::scene::create_point_light)()
{
	::framework::graphics::ray_tracing::point_light * point_light = new ::framework::graphics::ray_tracing::point_light(this);
	this->lights.push_back(point_light);
	return point_light;
}

::framework::graphics::image_infinite_light * (::framework::graphics::ray_tracing::scene::create_image_infinite_light)()
{
	::framework::graphics::ray_tracing::image_infinite_light * image_infinite_light = new ::framework::graphics::ray_tracing::image_infinite_light(this);
	return image_infinite_light;
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::update)()
{
	co_await this->initialize_geometries();
	co_await this->update_transforms();
	co_await this->update_lights();
	co_await this->light_collection->update();

	this->get_current_frame_uniform_buffer_mapped_data()->camera = this->scene_renderer->get_scene_views()[0]->get_device_data();

	class ::framework::command_context * const compute_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const compute_command_buffer = compute_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(compute_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		//::framework::profiler::scoped_timestamp const scoped_timestamp(*this->scene_renderer->get_profiler(), compute_command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "::framework::graphics::ray_tracing::scene::update");

		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::acceleration_structure_read_bit;
		buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
		buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_memory_barriers[0].buffer = this->instances_buffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		compute_command_buffer->pipeline_barrier(&dependency_info);

		::framework::gpu::acceleration_structure_geometry acceleration_structure_geometries[1];
		acceleration_structure_geometries[0].flags = ::framework::gpu::geometry_flags::none;
		acceleration_structure_geometries[0].primitive_count = this->geometry_instance_id_counter;
		acceleration_structure_geometries[0].primitive_offset = 0;
		acceleration_structure_geometries[0].geometry.instances.array_of_pointers = false;
		acceleration_structure_geometries[0].geometry.instances.data.device_address = this->instances_device_or_host_address.device_address;

		::framework::gpu::write_acceleration_structure_geometry write_acceleration_structure_geometry;
		write_acceleration_structure_geometry.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
		write_acceleration_structure_geometry.first_geometry = 0;
		write_acceleration_structure_geometry.geometry_count = static_cast<::std::uint32_t>(::std::size(acceleration_structure_geometries));
		write_acceleration_structure_geometry.geometry_type = ::framework::gpu::geometry_type::instances;
		write_acceleration_structure_geometry.data.geometries = acceleration_structure_geometries;
		this->top_acceleration_structure_geometry_set->write_data(write_acceleration_structure_geometry);

		::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
		acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::top_level;
		acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
		acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
		acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
		acceleration_structure_build_geometry_info.dst_acceleration_structure = this->top_level_acceleration_structure;
		acceleration_structure_build_geometry_info.geometry_count = 1;
		acceleration_structure_build_geometry_info.first_geometry = 0;
		acceleration_structure_build_geometry_info.geometries = this->top_acceleration_structure_geometry_set;
		acceleration_structure_build_geometry_info.scratch_data.device_address = this->top_level_acceleration_structure_scratch_device_or_host_address.device_address;
		compute_command_buffer->build_acceleration_structures(1, &acceleration_structure_build_geometry_info);
	}
	assert_framework_gpu_result(compute_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
	wait_semaphore_infos[0].semaphore = this->get_initialize_geometries_semaphore();
	wait_semaphore_infos[0].value = this->get_scene_renderer()->get_last_frame_index() + 1;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = this->get_initialize_geometries_semaphore();
	wait_semaphore_infos[1].value = this->get_scene_renderer()->get_last_frame_index() + 1;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[1].device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_semaphore;
	signal_semaphore_info.value = this->get_scene_renderer()->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->submit_and_wait(compute_command_context, static_cast<::std::uint32_t>(::std::size(wait_semaphore_infos)), wait_semaphore_infos, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_compute_queue()->release_command_context(compute_command_context);
}

void ::framework::graphics::ray_tracing::scene::bind_descriptor_set()
{
	this->light_collection->bind_descriptor_set();
}

struct ::framework::graphics::ray_tracing::scene::uniform_buffer * ::framework::graphics::ray_tracing::scene::get_current_frame_uniform_buffer_mapped_data()
{
	return reinterpret_cast<struct ::framework::graphics::ray_tracing::scene::uniform_buffer *>(this->uniform_buffer_mapped_data + this->uniform_buffer_alignment * this->scene_renderer->get_current_resource_index());
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::add_triangle_mesh_scene_geometry_to_initialize)(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_to_initialize.mutex.scoped_lock();
	this->triangle_mesh_scene_geometries_to_initialize.container.push_back(triangle_mesh_scene_geometry);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::add_triangle_mesh_scene_geometry_to_update_transform)(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_to_update_transform.mutex.scoped_lock();
	this->triangle_mesh_scene_geometries_to_update_transform.container.push_back(triangle_mesh_scene_geometry);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::add_light)(::framework::graphics::ray_tracing::image_infinite_light * image_infinite_light)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->infinite_lights.mutex.scoped_lock();
	this->infinite_lights.container.push_back(image_infinite_light);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::update_transforms)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const transfer_command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));

	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_to_update_transform.mutex.scoped_lock();
		for (auto triangle_mesh_scene_geometry : this->triangle_mesh_scene_geometries_to_update_transform.container)
		{
			::glm::mat3x4 const local_to_world_matrix = co_await triangle_mesh_scene_geometry->get_local_to_world_matrix();
			::glm::mat3x4 const inverse_transpose_local_to_world_matrix = ::glm::inverse(::glm::transpose(::glm::mat4(local_to_world_matrix)));
			::std::size_t const transform_offset = offsetof(::framework::gpu::acceleration_structure_instance, transform);

			co_await transfer_command_context->update_buffer(this->world_matrices_buffer, local_to_world_matrix, triangle_mesh_scene_geometry->get_instance_index());
			co_await transfer_command_context->update_buffer(this->inverse_transpose_world_matrices_buffer, inverse_transpose_local_to_world_matrix, triangle_mesh_scene_geometry->get_instance_index());
			co_await transfer_command_context->update_buffer(this->instances_buffer, &local_to_world_matrix, (triangle_mesh_scene_geometry->get_instance_index() * sizeof(::framework::gpu::acceleration_structure_instance)) + transform_offset, sizeof(::framework::gpu::transform_matrix));
		}
		this->triangle_mesh_scene_geometries_to_update_transform.container.clear();
	}

	assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_transforms_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::initialize_geometries)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const transfer_command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_to_initialize.mutex.scoped_lock();
		for (auto triangle_mesh_scene_geometry : this->triangle_mesh_scene_geometries_to_initialize.container)
		{
			co_await triangle_mesh_scene_geometry->initialize(transfer_command_context);
		}
		this->triangle_mesh_scene_geometries_to_initialize.container.clear();
	}
	assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->initialize_geometries_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::scene::update_lights)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

	::std::uint32_t light_index = 0;
	for (auto & light : this->lights)
	{
		if (light->get_changes() != ::framework::graphics::ray_tracing::light::changes::none)
		{
			co_await transfer_command_context->update_buffer(this->lights_buffer, co_await light->get_data(), light_index);
		}
		light_index++;
	}

	::std::uint32_t infinite_light_count = 0;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->infinite_lights.mutex.scoped_lock();
		for (auto & light : this->infinite_lights.container)
		{
			if (light->get_changes() != ::framework::graphics::ray_tracing::light::changes::none)
			{
				co_await transfer_command_context->update_buffer(this->infinite_lights_buffer, co_await light->get_image_infinite_light_device_data(), infinite_light_count);
			}
			infinite_light_count++;
		}
	}

	assert_framework_gpu_result(command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_lights_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);

	this->get_current_frame_uniform_buffer_mapped_data()->light_count = light_index;
	this->get_current_frame_uniform_buffer_mapped_data()->infinite_light_count = infinite_light_count;
}