inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::light_collection::get_update_semaphore)() const noexcept
{
	return this->update_semaphore;
}