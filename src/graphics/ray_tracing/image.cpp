#include "gpu/utility.hpp"
#include "graphics/ray_tracing/image.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include <numeric>

::framework::graphics::ray_tracing::image::image(::std::uint32_t id, ::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::image_create_info const & create_info) :
	id(id),
	scene_renderer(scene_renderer),
	initialization_shared_task(this->initialize(create_info))
{
}

::framework::graphics::ray_tracing::image::~image()
{
	this->scene_renderer->get_gpu_context()->get_device()->destroy_image_view(this->image_view, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->destroy_image(this->device_image, nullptr);
	this->scene_renderer->get_gpu_context()->get_device()->free_memory(this->device_memory, nullptr);
}

::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::image::initialize)(::framework::graphics::image_create_info const & create_info)
{
	::std::uint32_t	width = create_info.width;
	::std::uint32_t	height = create_info.height;
	::framework::gpu::device_size row_count;
	bool const is_texture_compression_bc_format = ::framework::gpu::utility::is_texture_compression_bc_format(create_info.format);
	bool const texture_compression_bc_unaligned = this->scene_renderer->get_gpu_context()->get_physical_device_features().texture_compression_bc_unaligned;

	auto const calculate_subresource_info = [is_texture_compression_bc_format, texture_compression_bc_unaligned](::framework::gpu::device_size & row_count, ::std::uint32_t & width, ::std::uint32_t & height)
	{
		if (is_texture_compression_bc_format)
		{
			row_count = height / 4;

			if (row_count == 0)
			{
				row_count = 1;
			}

			if (!texture_compression_bc_unaligned)
			{
				width = (width + 3) & ~3;
				height = (height + 3) & ~3;
			}
		}
		else
		{
			row_count = height;
		}
	};

	calculate_subresource_info(row_count, width, height);

	::framework::gpu::image_create_info image_create_info;
	image_create_info.flags = ::framework::gpu::image_create_flags::none;
	image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
	image_create_info.format = create_info.format;
	image_create_info.extent.width = width;
	image_create_info.extent.height = height;
	image_create_info.extent.depth = 1;
	image_create_info.mip_levels = create_info.mip_levels;
	image_create_info.array_layers = 1;
	image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
	image_create_info.usage = ::framework::gpu::image_usage_flags::transfer_dst_bit | ::framework::gpu::image_usage_flags::sampled_bit;
	image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	image_create_info.queue_family_index_count = 0;
	image_create_info.queue_family_indices = nullptr;
	image_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
	image_create_info.initial_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
	image_create_info.optimized_clear_value = nullptr;
	assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->create_image(&image_create_info, nullptr, &this->device_image));

	::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
	debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
	debug_utils_object_name_info.object = this->device_image;
	debug_utils_object_name_info.object_name = "::framework::graphics::ray_tracing::device_image";
	assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->set_object_name(&debug_utils_object_name_info));

	::framework::gpu::memory_requirements memory_requirements;

	::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
	image_memory_requirements_info.image = this->device_image;
	this->scene_renderer->get_gpu_context()->get_device()->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

	::framework::gpu::memory_allocate_info memory_allocate_info;
	memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
	memory_allocate_info.device_mask = 0;
	memory_allocate_info.allocation_size = memory_requirements.size;
	memory_allocate_info.allocation_alignment = memory_requirements.alignment;
	memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
	memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
	memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
	memory_allocate_info.opaque_capture_address = 0;
	assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->device_memory));

	::framework::gpu::bind_image_memory_info bind_image_memory_info;
	bind_image_memory_info.image = this->device_image;
	bind_image_memory_info.memory = this->device_memory;
	bind_image_memory_info.memory_offset = 0;
	assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->bind_image_memory(1, &bind_image_memory_info));

	::framework::gpu::image_view_create_info image_view_create_info;
	image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
	image_view_create_info.image = this->device_image;
	image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
	image_view_create_info.format = create_info.format;
	image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	image_view_create_info.subresource_range.base_mip_level = 0;
	image_view_create_info.subresource_range.level_count = create_info.mip_levels;
	image_view_create_info.subresource_range.base_array_layer = 0;
	image_view_create_info.subresource_range.layer_count = 1;
	image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
	image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
	assert_framework_gpu_result(this->scene_renderer->get_gpu_context()->get_device()->create_image_view(&image_view_create_info, nullptr, &this->image_view));

	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::command_context * const graphics_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();
	{
		::framework::gpu::command_buffer * command_buffer = transfer_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = create_info.mip_levels;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::framework::gpu::image_memory_barrier image_memory_barrier;
		image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::none;
		image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
		image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barrier.src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
		image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
		image_memory_barrier.image = this->device_image;
		image_memory_barrier.subresource_range = subresource_range;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 1;
		dependency_info.image_memory_barriers = &image_memory_barrier;

		command_buffer->pipeline_barrier(&dependency_info);

		::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment = this->scene_renderer->get_gpu_context()->get_physical_device_properties().limits.optimal_buffer_copy_row_pitch_alignment;
		::framework::gpu::device_size const optimal_buffer_copy_offset_alignment = this->scene_renderer->get_gpu_context()->get_physical_device_properties().limits.optimal_buffer_copy_offset_alignment;
		::framework::gpu::device_size const optimal_buffer_copy_row_pitch_alignment_mask = optimal_buffer_copy_row_pitch_alignment - 1;
		::framework::gpu::device_size const optimal_buffer_copy_offset_alignment_mask = optimal_buffer_copy_offset_alignment - 1;
		::framework::gpu::device_size const required_size = ::std::accumulate(create_info.subresource_infos, create_info.subresource_infos + create_info.subresource_count, ::framework::gpu::device_size(0),
			[&](::framework::gpu::device_size const size, ::framework::graphics::texture_subresource_info const & subresource_info)
			{
				::framework::gpu::device_size const aligned_row_pitch = (subresource_info.row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
				::std::uint32_t const mip_level = subresource_info.mip_level;
				::std::uint32_t mip_width = width >> mip_level;
				::std::uint32_t mip_height = height >> mip_level;
				calculate_subresource_info(row_count, mip_width, mip_height);
				return size + row_count * aligned_row_pitch + optimal_buffer_copy_offset_alignment_mask;
			}
		);
		struct ::framework::linear_allocator::allocation const allocation = co_await transfer_command_context->get_linear_allocator().allocate(required_size, 1);
		::framework::gpu::device_size dst_offset = allocation.offset;
		::std::vector<::framework::gpu::buffer_image_copy> buffer_image_copy(create_info.subresource_count);
		for (::std::uint32_t i = 0; i < create_info.subresource_count; ++i)
		{
			dst_offset = (dst_offset + optimal_buffer_copy_offset_alignment_mask) & ~optimal_buffer_copy_offset_alignment_mask;

			::framework::gpu::device_size const aligned_row_pitch = (create_info.subresource_infos[i].row_pitch + optimal_buffer_copy_row_pitch_alignment_mask) & ~optimal_buffer_copy_row_pitch_alignment_mask;
			::std::uint32_t const mip_level = create_info.subresource_infos[i].mip_level;
			::std::uint32_t mip_width = width >> mip_level;
			::std::uint32_t mip_height = height >> mip_level;

			calculate_subresource_info(row_count, mip_width, mip_height);

			buffer_image_copy[i].buffer_offset = dst_offset;
			buffer_image_copy[i].buffer_row_length = 0;
			buffer_image_copy[i].buffer_image_height = 0;
			buffer_image_copy[i].image_subresource.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			buffer_image_copy[i].image_subresource.mip_level = mip_level;
			buffer_image_copy[i].image_subresource.base_array_layer = create_info.subresource_infos[i].array_layer;
			buffer_image_copy[i].image_subresource.layer_count = 1;
			buffer_image_copy[i].image_offset = { 0, 0, 0 };
			buffer_image_copy[i].image_extent = { mip_width, mip_height, 1u };

			::std::byte const * src_address = reinterpret_cast<::std::byte const *>(create_info.subresource_infos[i].data);
			for (::std::uint32_t y = 0; y < row_count; ++y)
			{
				::std::memcpy(reinterpret_cast<::std::byte *>(allocation.mapped_buffer_data) + dst_offset, src_address, create_info.subresource_infos[i].row_pitch);
				dst_offset += aligned_row_pitch;
				src_address += create_info.subresource_infos[i].row_pitch;
			}
		}
		command_buffer->copy_buffer_to_image(allocation.buffer, this->device_image, ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit, static_cast<::std::uint32_t>(buffer_image_copy.size()), buffer_image_copy.data());

		image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
		image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
		image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
		image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		image_memory_barrier.src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
		image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;

		command_buffer->pipeline_barrier(&dependency_info);
		assert_framework_gpu_result(command_buffer->end_command_buffer());

		::framework::gpu::semaphore_submit_info signal_semaphore_info;
		signal_semaphore_info.semaphore = graphics_command_context->get_semaphore();
		signal_semaphore_info.value = 0;
		signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		signal_semaphore_info.device_index = 0;
		co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit(transfer_command_context, 0, nullptr, 1, &signal_semaphore_info);
	}

	{
		::framework::gpu::command_buffer * command_buffer = graphics_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

		::framework::gpu::image_subresource_range subresource_range;
		subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		subresource_range.base_mip_level = 0;
		subresource_range.level_count = create_info.mip_levels;
		subresource_range.base_array_layer = 0;
		subresource_range.layer_count = 1;

		::framework::gpu::image_memory_barrier image_memory_barrier;
		image_memory_barrier.src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		image_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::none;
		image_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		image_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit;
		image_memory_barrier.old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		image_memory_barrier.new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;
		image_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		image_memory_barrier.src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().transfer;
		image_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
		image_memory_barrier.dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		image_memory_barrier.image = this->device_image;
		image_memory_barrier.subresource_range = subresource_range;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 1;
		dependency_info.image_memory_barriers = &image_memory_barrier;
		command_buffer->pipeline_barrier(&dependency_info);
		assert_framework_gpu_result(command_buffer->end_command_buffer());

		::framework::gpu::semaphore_submit_info wait_semaphore_info;
		wait_semaphore_info.semaphore = graphics_command_context->get_semaphore();
		wait_semaphore_info.value = 0;
		wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		wait_semaphore_info.device_index = 0;
		co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->submit_and_wait(graphics_command_context, 1, &wait_semaphore_info, 0, nullptr);
	}

	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
	co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->release_command_context(graphics_command_context);

	::framework::gpu::descriptor_image_info descriptor_image_info;
	descriptor_image_info.sampler = nullptr;
	descriptor_image_info.image_view = this->image_view;
	descriptor_image_info.image_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_set;
	write_descriptor_set.dst_set = this->scene_renderer->get_material_system()->get_descriptor_set();
	write_descriptor_set.dst_binding = ::framework::graphics::ray_tracing::material_system::textures_binding;
	write_descriptor_set.dst_array_element = this->id;
	write_descriptor_set.descriptor_count = 1;
	write_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
	write_descriptor_set.image_info = &descriptor_image_info;
	this->scene_renderer->get_gpu_context()->get_device()->update_descriptor_sets(1, &write_descriptor_set, 0, nullptr);
}