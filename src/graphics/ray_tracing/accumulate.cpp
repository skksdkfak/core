#include "command_context.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "graphics/ray_tracing/accumulate.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "gpu/utility.hpp"

::framework::graphics::ray_tracing::accumulate::accumulate(::framework::graphics::ray_tracing::scene_renderer * scene_renderer) :
	scene_renderer(scene_renderer),
	sample_count(0)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[4];
		// currrent_frame
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// last_frame_sum_low
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// last_frame_sum_high
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// output_frame
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->accumulate_descriptor_set_layout));
	}

	{
		::framework::gpu::push_constant_range push_constant_ranges[1];
		push_constant_ranges[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges[0].binding = 0;
		push_constant_ranges[0].offset = 0;
		push_constant_ranges[0].size = sizeof(::std::uint32_t) * 4;
		push_constant_ranges[0].hlsl_shader_register = 0;
		push_constant_ranges[0].hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = 1;
		pipeline_layout_create_info.descriptor_set_layouts = &this->accumulate_descriptor_set_layout;
		pipeline_layout_create_info.push_constant_range_count = static_cast<::std::uint32_t>(::std::size(push_constant_ranges));
		pipeline_layout_create_info.push_constant_ranges = push_constant_ranges;
		device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->accumulate_pipeline_layout);
	}

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &this->accumulate_descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->accumulate_descriptor_set));

	this->create_screen_buffers();
	this->create_pipelines();
	this->update_descriptor_set();
}

::framework::graphics::ray_tracing::accumulate::~accumulate()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	//device->free_descriptor_sets(scene_renderer->get_device_descriptor_pool(), 1, &this->accumulate_descriptor_set); // todo investigate crash
	device->destroy_shader_module(this->accumulate_shader_module, nullptr);
	device->destroy_descriptor_set_layout(this->accumulate_descriptor_set_layout, nullptr);
	device->destroy_pipeline_layout(this->accumulate_pipeline_layout, nullptr);
	device->destroy_pipeline(this->accumulate_pipeline, nullptr);
	device->destroy_image_view(this->last_frame_sum_low_image_view, nullptr);
	device->destroy_image_view(this->last_frame_sum_high_image_view, nullptr);
	device->destroy_image(this->last_frame_sum_low_image, nullptr);
	device->destroy_image(this->last_frame_sum_high_image, nullptr);
	device->free_memory(this->last_frame_sum_low_device_memory, nullptr);
	device->free_memory(this->last_frame_sum_high_device_memory, nullptr);
}

void ::framework::graphics::ray_tracing::accumulate::execute(::framework::gpu::command_buffer * command_buffer, bool enable_tone_mapping)
{
	::framework::gpu::image_subresource_range subresource_range;
	subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
	subresource_range.base_mip_level = 0;
	subresource_range.level_count = 1;
	subresource_range.base_array_layer = 0;
	subresource_range.layer_count = 1;

	if (this->sample_count == 0)
	{
		{
			::framework::gpu::image_memory_barrier image_memory_barriers[2];
			image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
			image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::clear_color_image_bit;
			image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::clear_color_image_bit;
			image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::clear_color_image_general_bit;
			image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].image = this->last_frame_sum_low_image;
			image_memory_barriers[0].subresource_range = subresource_range;

			image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
			image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::clear_color_image_bit;
			image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::clear_color_image_bit;
			image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::clear_color_image_general_bit;
			image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].image = this->last_frame_sum_high_image;
			image_memory_barriers[1].subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
			dependency_info.image_memory_barriers = image_memory_barriers;

			command_buffer->pipeline_barrier(&dependency_info);
		}
		::framework::gpu::clear_color_value const clear_value = { { 0.0f, 0.0f, 0.0f, 0.0f } };
		command_buffer->clear_color_image(this->last_frame_sum_low_image, ::framework::gpu::image_layout_flags::clear_color_image_general_bit | ::framework::gpu::image_layout_flags::general_bit, &clear_value, 1, &subresource_range, this->last_frame_sum_low_host_descriptor_handle);
		command_buffer->clear_color_image(this->last_frame_sum_high_image, ::framework::gpu::image_layout_flags::clear_color_image_general_bit | ::framework::gpu::image_layout_flags::general_bit, &clear_value, 1, &subresource_range, this->last_frame_sum_high_host_descriptor_handle);

		{
			::framework::gpu::image_memory_barrier image_memory_barriers[2];
			image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::clear_color_image_bit;
			image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
			image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::clear_color_image_bit;
			image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::clear_color_image_general_bit;
			image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].image = this->last_frame_sum_low_image;
			image_memory_barriers[0].subresource_range = subresource_range;

			image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::clear_color_image_bit;
			image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
			image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::clear_color_image_bit;
			image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::clear_color_image_general_bit;
			image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].image = this->last_frame_sum_high_image;
			image_memory_barriers[1].subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
			dependency_info.image_memory_barriers = image_memory_barriers;

			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	struct
	{
		::glm::uvec2 resolution;
		::std::uint32_t accumulation_count;
		::framework::gpu::bool32_t enable_tone_mapping;
	}
	push_constants
	{
		.resolution = { this->scene_renderer->get_width(), this->scene_renderer->get_height()},
		.accumulation_count = this->sample_count,
		.enable_tone_mapping = enable_tone_mapping
	};

	::framework::gpu::image_memory_barrier image_memory_barriers[3];
	image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
	image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_sampled_read_bit;
	image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
	image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;
	image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[0].image = this->scene_renderer->get_color_image();
	image_memory_barriers[0].subresource_range = subresource_range;

	image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::shader_sampled_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
	image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_sampled_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
	image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
	image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
	image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[1].image = this->last_frame_sum_low_image;
	image_memory_barriers[1].subresource_range = subresource_range;

	image_memory_barriers[2].src_access_mask = ::framework::gpu::access_flags::shader_sampled_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
	image_memory_barriers[2].dst_access_mask = ::framework::gpu::access_flags::shader_sampled_read_bit | ::framework::gpu::access_flags::shader_storage_write_bit;
	image_memory_barriers[2].src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[2].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	image_memory_barriers[2].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
	image_memory_barriers[2].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
	image_memory_barriers[2].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[2].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[2].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
	image_memory_barriers[2].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
	image_memory_barriers[2].image = this->last_frame_sum_high_image;
	image_memory_barriers[2].subresource_range = subresource_range;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = 0;
	dependency_info.memory_barriers = nullptr;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
	dependency_info.image_memory_barriers = image_memory_barriers;

	::framework::profiler::scoped_timestamp scoped_timestamp(*this->scene_renderer->get_profiler(), command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "accumulate");
	command_buffer->pipeline_barrier(&dependency_info);
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->accumulate_pipeline);
	command_buffer->bind_descriptor_pool(this->scene_renderer->get_device_descriptor_pool());
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->accumulate_pipeline_layout, 0, 1, &this->accumulate_descriptor_set, 0, nullptr);
	command_buffer->push_constants(this->accumulate_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch(this->scene_renderer->get_width() / 16, this->scene_renderer->get_height() / 16, 1);

	this->sample_count++;
}

void ::framework::graphics::ray_tracing::accumulate::reset()
{
	this->sample_count = 0;
}

void ::framework::graphics::ray_tracing::accumulate::resize()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	device->destroy_image_view(this->last_frame_sum_low_image_view, nullptr);
	device->destroy_image(this->last_frame_sum_low_image, nullptr);
	device->free_memory(this->last_frame_sum_low_device_memory, nullptr);
	device->destroy_image_view(this->last_frame_sum_high_image_view, nullptr);
	device->destroy_image(this->last_frame_sum_high_image, nullptr);
	device->free_memory(this->last_frame_sum_high_device_memory, nullptr);

	this->create_screen_buffers();
	this->update_descriptor_set();

	this->sample_count = 0;
}

void ::framework::graphics::ray_tracing::accumulate::recompile_shaders()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	device->destroy_shader_module(this->accumulate_shader_module, nullptr);
	device->destroy_pipeline(this->accumulate_pipeline, nullptr);

	this->create_pipelines();
}

void ::framework::graphics::ray_tracing::accumulate::create_screen_buffers()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::clear_value const clear_value = { .color = { { 0.0f, 0.0f, 0.0f, 0.0f } } };

		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r32g32b32a32_uint;
		image_create_info.extent.width = this->scene_renderer->get_width();
		image_create_info.extent.height = this->scene_renderer->get_height();
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::allow_clear_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = &clear_value;
		assert_framework_gpu_result(device->create_image(&image_create_info, nullptr, &this->last_frame_sum_low_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->last_frame_sum_low_image;
		device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->last_frame_sum_low_device_memory));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->last_frame_sum_low_image;
		debug_utils_object_name_info.object_name = "last_frame_sum_image";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->last_frame_sum_low_image;
		bind_image_memory_info.memory = this->last_frame_sum_low_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->last_frame_sum_low_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r32g32b32a32_uint;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(device->create_image_view(&image_view_create_info, nullptr, &this->last_frame_sum_low_image_view));
	}

	{
		::framework::gpu::clear_value const clear_value = { .color = { { 0.0f, 0.0f, 0.0f, 0.0f } } };

		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r32g32b32a32_uint;
		image_create_info.extent.width = this->scene_renderer->get_width();
		image_create_info.extent.height = this->scene_renderer->get_height();
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::allow_clear_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = &clear_value;
		assert_framework_gpu_result(device->create_image(&image_create_info, nullptr, &this->last_frame_sum_high_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->last_frame_sum_high_image;
		device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->last_frame_sum_high_device_memory));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->last_frame_sum_high_image;
		debug_utils_object_name_info.object_name = "last_frame_sum_image";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->last_frame_sum_high_image;
		bind_image_memory_info.memory = this->last_frame_sum_high_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->last_frame_sum_high_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r32g32b32a32_uint;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(device->create_image_view(&image_view_create_info, nullptr, &this->last_frame_sum_high_image_view));
	}

	if (!this->scene_renderer->get_gpu_context()->get_physical_device_features().any_image_initial_layout)
	{
		::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
		{
			::framework::command_context * const graphics_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			::framework::gpu::image_subresource_range subresource_range;
			subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			subresource_range.base_mip_level = 0;
			subresource_range.level_count = 1;
			subresource_range.base_array_layer = 0;
			subresource_range.layer_count = 1;

			::framework::gpu::image_memory_barrier image_memory_barriers[2];
			image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].image = this->last_frame_sum_low_image;
			image_memory_barriers[0].subresource_range = subresource_range;

			image_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[1].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[1].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[1].image = this->last_frame_sum_high_image;
			image_memory_barriers[1].subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
			dependency_info.image_memory_barriers = image_memory_barriers;
			graphics_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);

			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
			co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->release_command_context(graphics_command_context);
		}());
	}
}

void ::framework::graphics::ray_tracing::accumulate::create_pipelines()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::resource::slang_source_info slang_source_info;
		slang_source_info.file_name = "accumulate.cs.slang";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = &slang_source_info;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->accumulate_shader_module = this->scene_renderer->get_resource_manager()->load_shader_module(device, shader_module_info);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->accumulate_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->accumulate_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->accumulate_pipeline);
	}
}

void ::framework::graphics::ray_tracing::accumulate::update_descriptor_set()
{
	::framework::gpu::descriptor_image_info descriptor_image_infos[6];
	descriptor_image_infos[0].sampler = nullptr;
	descriptor_image_infos[0].image_view = this->scene_renderer->get_color_image_view();
	descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::non_fragment_shader_read_only_optimal_bit;

	descriptor_image_infos[1].sampler = nullptr;
	descriptor_image_infos[1].image_view = this->last_frame_sum_low_image_view;
	descriptor_image_infos[1].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[2].sampler = nullptr;
	descriptor_image_infos[2].image_view = this->last_frame_sum_high_image_view;
	descriptor_image_infos[2].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[3].sampler = nullptr;
	descriptor_image_infos[3].image_view = this->scene_renderer->get_output_color_image_view();
	descriptor_image_infos[3].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	descriptor_image_infos[4].sampler = nullptr;
	descriptor_image_infos[4].image_view = this->last_frame_sum_low_image_view;
	descriptor_image_infos[4].image_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	descriptor_image_infos[5].sampler = nullptr;
	descriptor_image_infos[5].image_view = this->last_frame_sum_high_image_view;
	descriptor_image_infos[5].image_layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[6];
	write_descriptor_sets[0].dst_set = this->accumulate_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_image;
	write_descriptor_sets[0].image_info = &descriptor_image_infos[0];
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;

	write_descriptor_sets[1].dst_set = this->accumulate_descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[1].image_info = &descriptor_image_infos[1];
	write_descriptor_sets[1].buffer_info = nullptr;
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;

	write_descriptor_sets[2].dst_set = this->accumulate_descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[2].image_info = &descriptor_image_infos[2];
	write_descriptor_sets[2].buffer_info = nullptr;
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;

	write_descriptor_sets[3].dst_set = this->accumulate_descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[3].image_info = &descriptor_image_infos[3];
	write_descriptor_sets[3].buffer_info = nullptr;
	write_descriptor_sets[3].texel_buffer_view = nullptr;
	write_descriptor_sets[3].acceleration_structures = nullptr;

	write_descriptor_sets[4].dst_set = this->scene_renderer->get_attachments_descriptor_set();
	write_descriptor_sets[4].dst_binding = 0;
	write_descriptor_sets[4].dst_array_element = 1;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::color_attachment;
	write_descriptor_sets[4].image_info = &descriptor_image_infos[4];
	write_descriptor_sets[4].buffer_info = nullptr;
	write_descriptor_sets[4].texel_buffer_view = nullptr;
	write_descriptor_sets[4].acceleration_structures = nullptr;

	write_descriptor_sets[5].dst_set = this->scene_renderer->get_attachments_descriptor_set();
	write_descriptor_sets[5].dst_binding = 0;
	write_descriptor_sets[5].dst_array_element = 2;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::color_attachment;
	write_descriptor_sets[5].image_info = &descriptor_image_infos[5];
	write_descriptor_sets[5].buffer_info = nullptr;
	write_descriptor_sets[5].texel_buffer_view = nullptr;
	write_descriptor_sets[5].acceleration_structures = nullptr;
	this->scene_renderer->get_gpu_context()->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	{
		::framework::gpu::descriptor_handle_info descriptor_handle_info;
		descriptor_handle_info.flags = ::framework::gpu::descriptor_handle_info_flags::none;
		descriptor_handle_info.device_descriptor_handle = false;
		descriptor_handle_info.set = this->scene_renderer->get_attachments_descriptor_set();
		descriptor_handle_info.binding = 0;
		descriptor_handle_info.array_element = 1;
		this->last_frame_sum_low_host_descriptor_handle = this->scene_renderer->get_gpu_context()->get_device()->get_descriptor_handle(&descriptor_handle_info).host_descriptor_handle;
	}
	{
		::framework::gpu::descriptor_handle_info descriptor_handle_info;
		descriptor_handle_info.flags = ::framework::gpu::descriptor_handle_info_flags::none;
		descriptor_handle_info.device_descriptor_handle = false;
		descriptor_handle_info.set = this->scene_renderer->get_attachments_descriptor_set();
		descriptor_handle_info.binding = 0;
		descriptor_handle_info.array_element = 2;
		this->last_frame_sum_high_host_descriptor_handle = this->scene_renderer->get_gpu_context()->get_device()->get_descriptor_handle(&descriptor_handle_info).host_descriptor_handle;
	}
}
