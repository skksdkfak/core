#include "volume_geometry.hpp"
#include "scene_renderer.hpp"
#include "gpu/utility.hpp"
#include <glm/glm.hpp>

::framework::graphics::ray_tracing::volume_geometry::volume_geometry(::std::uint32_t id) :
	id(id)
{
}

::framework::graphics::ray_tracing::volume_geometry::~volume_geometry()
{
	//this->scene_renderer->get_device()->destroy_acceleration_structure_geometry_set(this->bottom_acceleration_structure_geometry_set, nullptr);
	//this->scene_renderer->get_device()->destroy_buffer(this->bottom_level_acceleration_structure_buffer, nullptr);
	//this->scene_renderer->get_device()->destroy_acceleration_structure(this->bottom_level_acceleration_structure, nullptr);
}

::framework::coroutine::lazy_task<void>(::framework::graphics::ray_tracing::volume_geometry::initialize)(::framework::graphics::ray_tracing::scene_renderer * scene_renderer)
{
	//this->scene_renderer = scene_renderer;

	//::std::uint32_t const volume_geometry_count = 1;
	//::std::uint32_t const volume_per_geometry_count = 2;

	//::framework::coroutine::lazy_task<::std::uint32_t> acquire_device_transfer_resources = scene_renderer->acquire_device_transfer_resources();
	//::std::uint32_t const device_transfer_resources_index = co_await acquire_device_transfer_resources;
	//struct ::framework::graphics::ray_tracing::scene_renderer::device_transfer_resources & device_transfer_resources = scene_renderer->get_device_transfer_resources(device_transfer_resources_index);

	//::framework::coroutine::lazy_task<::std::uint32_t> acquire_acceleration_structure_build_resources = scene_renderer->acquire_acceleration_structure_build_resources();
	//::std::uint32_t const acquire_acceleration_structure_build_resources_index = co_await acquire_acceleration_structure_build_resources;
	//struct ::framework::graphics::ray_tracing::scene_renderer::acceleration_structure_build_resources & acceleration_structure_build_resources = scene_renderer->get_acceleration_structure_build_resources(acquire_acceleration_structure_build_resources_index);

	//::framework::gpu::device_size staging_buffer_data_offset = 0;

	//::std::byte * mapped_staging_buffer_data;
	//assert_framework_gpu_result(scene_renderer->get_device()->map_memory(scene_renderer->get_staging_device_memory(), 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&mapped_staging_buffer_data)));

	//::framework::gpu::command_buffer * command_buffer = device_transfer_resources.command_buffer;

	//::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	//command_buffer_begin_info.inheritance_info = nullptr;
	//assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

	//::std::vector<struct ::framework::gpu::aabb_positions> vertices(volume_per_geometry_count * volume_geometry_count);
	//for (::std::uint32_t i = 0; i < volume_per_geometry_count * volume_geometry_count; i++)
	//{
	//	vertices[i].min_x = -0.5 + 0.5 * i;
	//	vertices[i].min_y = -0.5 + 0.5 * i;
	//	vertices[i].min_z = -0.5 + 0.5 * i;
	//	vertices[i].max_x = 0.5 + 0.5 * i;
	//	vertices[i].max_y = 0.5 + 0.5 * i;
	//	vertices[i].max_z = 0.5 + 0.5 * i;
	//}

	//{
	//	::framework::gpu::device_size size = sizeof(struct ::framework::gpu::aabb_positions) * volume_per_geometry_count * volume_geometry_count;
	//	this->aabbs_memory_offset = this->scene_renderer->allocate_vertices_device_memory(size, 1);

	//	assert(staging_buffer_data_offset + size <= this->scene_renderer->get_staging_buffer_size());
	//	assert(this->aabbs_memory_offset + size <= this->scene_renderer->get_vertices_buffer_size());

	//	::std::memcpy(mapped_staging_buffer_data + staging_buffer_data_offset, vertices.data(), size);

	//	::framework::gpu::buffer_copy buffer_copy;
	//	buffer_copy.src_offset = staging_buffer_data_offset;
	//	buffer_copy.dst_offset = this->aabbs_memory_offset;
	//	buffer_copy.size = size;

	//	command_buffer->copy_buffer(scene_renderer->get_staging_buffer(), scene_renderer->get_vertices_buffer(), 1, &buffer_copy);

	//	staging_buffer_data_offset += size;
	//}

	//{
	//	::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//	buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::host_write_bit;
	//	buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//	buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
	//	buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
	//	buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
	//	buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
	//	buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//	buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().transfer;
	//	buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//	buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().compute;
	//	buffer_memory_barriers[0].buffer = scene_renderer->get_indices_buffer();
	//	buffer_memory_barriers[0].offset = 0;
	//	buffer_memory_barriers[0].size = ::framework::gpu::whole_size; // todo: determine size

	//	buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::host_write_bit;
	//	buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//	buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
	//	buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
	//	buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
	//	buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
	//	buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//	buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().transfer;
	//	buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//	buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().compute;
	//	buffer_memory_barriers[1].buffer = scene_renderer->get_vertices_buffer();
	//	buffer_memory_barriers[1].offset = 0;
	//	buffer_memory_barriers[1].size = ::framework::gpu::whole_size; // todo: determine size

	//	::framework::gpu::dependency_info dependency_info;
	//	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	//	dependency_info.memory_barrier_count = 0;
	//	dependency_info.memory_barriers = nullptr;
	//	dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
	//	dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	//	dependency_info.image_memory_barrier_count = 0;
	//	dependency_info.image_memory_barriers = nullptr;
	//	command_buffer->pipeline_barrier(&dependency_info);
	//}

	//assert_framework_gpu_result(command_buffer->end_command_buffer());

	//scene_renderer->get_device()->unmap_memory(scene_renderer->get_staging_device_memory());

	//::framework::gpu::semaphore_submit_info signal_semaphore_info;
	//signal_semaphore_info.semaphore = acceleration_structure_build_resources.semaphore;
	//signal_semaphore_info.value = 0;
	//signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	//signal_semaphore_info.device_index = 0;

	//auto release_device_transfer_resources = scene_renderer->release_device_transfer_resources(device_transfer_resources_index, 1, &signal_semaphore_info);
	//co_await release_device_transfer_resources;

	//{
	//	::framework::gpu::acceleration_structure_build_sizes_info acceleration_structure_build_sizes_info;

	//	::std::vector<::framework::gpu::acceleration_structure_geometry> acceleration_structure_geometries(volume_geometry_count);

	//	for (::std::uint32_t i = 0; i < volume_geometry_count; i++)
	//	{
	//		acceleration_structure_geometries[i].flags = ::framework::gpu::geometry_flags::opaque_bit;
	//		acceleration_structure_geometries[i].primitive_count = volume_per_geometry_count;
	//		acceleration_structure_geometries[i].primitive_offset = 0;
	//		acceleration_structure_geometries[i].geometry.aabbs.data.device_address = this->scene_renderer->get_vertices_device_address() + this->aabbs_memory_offset;
	//		acceleration_structure_geometries[i].geometry.aabbs.stride = sizeof(struct ::framework::gpu::aabb_positions);
	//	}

	//	::framework::gpu::acceleration_structure_geometry_set_create_info acceleration_structure_geometry_set_create_info;
	//	acceleration_structure_geometry_set_create_info.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
	//	acceleration_structure_geometry_set_create_info.geometry_count = volume_geometry_count;
	//	acceleration_structure_geometry_set_create_info.geometry_type = ::framework::gpu::geometry_type::aabbs;
	//	assert_framework_gpu_result(this->scene_renderer->get_device()->create_acceleration_structure_geometry_set(&acceleration_structure_geometry_set_create_info, nullptr, &this->bottom_acceleration_structure_geometry_set));

	//	::framework::gpu::write_acceleration_structure_geometry write_acceleration_structure_geometry;
	//	write_acceleration_structure_geometry.layout = ::framework::gpu::acceleration_structure_geometry_set_layout::array;
	//	write_acceleration_structure_geometry.first_geometry = 0;
	//	write_acceleration_structure_geometry.geometry_count = volume_geometry_count;
	//	write_acceleration_structure_geometry.geometry_type = ::framework::gpu::geometry_type::aabbs;
	//	write_acceleration_structure_geometry.data.geometries = acceleration_structure_geometries.data();
	//	this->bottom_acceleration_structure_geometry_set->write_data(write_acceleration_structure_geometry);

	//	::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
	//	acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
	//	acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
	//	acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
	//	acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
	//	acceleration_structure_build_geometry_info.dst_acceleration_structure = nullptr;
	//	acceleration_structure_build_geometry_info.geometry_count = volume_geometry_count;
	//	acceleration_structure_build_geometry_info.first_geometry = 0;
	//	acceleration_structure_build_geometry_info.geometries = this->bottom_acceleration_structure_geometry_set;
	//	acceleration_structure_build_geometry_info.scratch_data.device_address = 0;
	//	this->scene_renderer->get_device()->get_acceleration_structure_build_sizes(::framework::gpu::acceleration_structure_build_type::device, &acceleration_structure_build_geometry_info, &acceleration_structure_build_sizes_info);

	//	::framework::gpu::buffer_create_info buffer_create_info;
	//	buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
	//	buffer_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
	//	buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::acceleration_structure_storage_bit | ::framework::gpu::buffer_usage_flags::shader_device_address_bit;
	//	buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
	//	buffer_create_info.queue_family_index_count = 0;
	//	buffer_create_info.queue_family_indices = nullptr;
	//	buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::raytracing_acceleration_structure_bit;
	//	buffer_create_info.opaque_capture_address = 0;
	//	assert_framework_gpu_result(this->scene_renderer->get_device()->create_buffer(&buffer_create_info, nullptr, &this->bottom_level_acceleration_structure_buffer));

	//	::framework::gpu::memory_requirements memory_requirements;

	//	::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
	//	buffer_memory_requirements_info.buffer = this->bottom_level_acceleration_structure_buffer;
	//	this->scene_renderer->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

	//	::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
	//	bind_buffer_memory_info.buffer = this->bottom_level_acceleration_structure_buffer;
	//	bind_buffer_memory_info.memory = this->scene_renderer->get_bottom_level_acceleration_structure_device_memory();
	//	bind_buffer_memory_info.memory_offset = this->scene_renderer->allocate_bottom_level_acceleration_structure_device_memory(memory_requirements.size, memory_requirements.alignment);
	//	assert(bind_buffer_memory_info.memory_offset + memory_requirements.size <= this->scene_renderer->get_bottom_level_acceleration_structure_device_memory_size());
	//	assert_framework_gpu_result(this->scene_renderer->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));

	//	::framework::gpu::acceleration_structure_create_info acceleration_structure_create_info;
	//	acceleration_structure_create_info.create_flags = ::framework::gpu::acceleration_structure_create_flags::none;
	//	acceleration_structure_create_info.buffer = this->bottom_level_acceleration_structure_buffer;
	//	acceleration_structure_create_info.offset = 0;
	//	acceleration_structure_create_info.size = acceleration_structure_build_sizes_info.acceleration_structure_size;
	//	acceleration_structure_create_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
	//	acceleration_structure_create_info.device_address = 0;
	//	assert_framework_gpu_result(this->scene_renderer->get_device()->create_acceleration_structure(&acceleration_structure_create_info, nullptr, &this->bottom_level_acceleration_structure));
	//}

	//{
	//	::framework::gpu::acceleration_structure_build_geometry_info acceleration_structure_build_geometry_info;
	//	acceleration_structure_build_geometry_info.type = ::framework::gpu::acceleration_structure_type::bottom_level;
	//	acceleration_structure_build_geometry_info.flags = ::framework::gpu::build_acceleration_structure_flags::prefer_fast_trace_bit;
	//	acceleration_structure_build_geometry_info.mode = ::framework::gpu::build_acceleration_structure_mode::build;
	//	acceleration_structure_build_geometry_info.src_acceleration_structure = nullptr;
	//	acceleration_structure_build_geometry_info.dst_acceleration_structure = this->bottom_level_acceleration_structure;
	//	acceleration_structure_build_geometry_info.geometry_count = volume_geometry_count;
	//	acceleration_structure_build_geometry_info.first_geometry = 0;
	//	acceleration_structure_build_geometry_info.geometries = this->bottom_acceleration_structure_geometry_set;
	//	acceleration_structure_build_geometry_info.scratch_data.device_address = this->scene_renderer->get_bottom_level_acceleration_structure_scratch_device_or_host_address().device_address;

	//	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	//	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	//	command_buffer_begin_info.inheritance_info = nullptr;

	//	acceleration_structure_build_resources.command_buffer->begin_command_buffer(&command_buffer_begin_info);

	//	{
	//		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[2];
	//		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::host_write_bit;
	//		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//		buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
	//		buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
	//		buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
	//		buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
	//		buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//		buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().transfer;
	//		buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//		buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().compute;
	//		buffer_memory_barriers[0].buffer = scene_renderer->get_indices_buffer();
	//		buffer_memory_barriers[0].offset = 0;
	//		buffer_memory_barriers[0].size = ::framework::gpu::whole_size; // todo: specify proper size

	//		buffer_memory_barriers[1].src_access_mask = ::framework::gpu::access_flags::host_write_bit;
	//		buffer_memory_barriers[1].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
	//		buffer_memory_barriers[1].src_stage_mask = ::framework::gpu::pipeline_stage_flags::host_bit;
	//		buffer_memory_barriers[1].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::acceleration_structure_build_bit;
	//		buffer_memory_barriers[1].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
	//		buffer_memory_barriers[1].new_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
	//		buffer_memory_barriers[1].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//		buffer_memory_barriers[1].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().transfer;
	//		buffer_memory_barriers[1].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::none;
	//		buffer_memory_barriers[1].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_queue_family_indices().compute;
	//		buffer_memory_barriers[1].buffer = scene_renderer->get_vertices_buffer();
	//		buffer_memory_barriers[1].offset = 0;
	//		buffer_memory_barriers[1].size = ::framework::gpu::whole_size; // todo: specify proper size

	//		::framework::gpu::dependency_info dependency_info;
	//		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	//		dependency_info.memory_barrier_count = 0;
	//		dependency_info.memory_barriers = nullptr;
	//		dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
	//		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
	//		dependency_info.image_memory_barrier_count = 0;
	//		dependency_info.image_memory_barriers = nullptr;
	//		acceleration_structure_build_resources.command_buffer->pipeline_barrier(&dependency_info);
	//	}

	//	acceleration_structure_build_resources.command_buffer->build_acceleration_structures(1, &acceleration_structure_build_geometry_info);
	//	acceleration_structure_build_resources.command_buffer->end_command_buffer();

	//	auto release_acceleration_structure_build_resources = scene_renderer->release_acceleration_structure_build_resources(acquire_acceleration_structure_build_resources_index, true);
	//	co_await release_acceleration_structure_build_resources;

	//	this->scene_renderer->get_device()->destroy_acceleration_structure_geometry_set(this->bottom_acceleration_structure_geometry_set, nullptr);

	//	::framework::gpu::acceleration_structure_device_address_info acceleration_structure_device_address_info;
	//	acceleration_structure_device_address_info.acceleration_structure = this->bottom_level_acceleration_structure;
	//	this->bottom_level_acceleration_structure_device_address = this->scene_renderer->get_device()->get_acceleration_structure_device_address(&acceleration_structure_device_address_info);
	//}

	co_return;
}