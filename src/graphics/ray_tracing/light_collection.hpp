#pragma once

#include "command_context.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;
	class scene;
	class triangle_mesh_scene_geometry;

	class light_collection
	{
	public:
		struct uniform_data
		{
			::std::uint32_t triangle_count;
			::std::uint32_t active_triangle_count;
			::glm::uvec2 padding;
		};

		light_collection(::framework::graphics::ray_tracing::scene * scene);

		~light_collection();

		::framework::coroutine::immediate_task<void> update();

		void bind_descriptor_set();

		::framework::coroutine::immediate_task<void> add_triangle_mesh_scene_geometry(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry);

		::framework::gpu::semaphore * get_update_semaphore() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene;

		::framework::graphics::ray_tracing::scene * scene;
		::framework::gpu::buffer * emissive_triangle_list_buffer;
		::framework::gpu::device_memory * emissive_triangle_list_device_memory;
		::framework::gpu::buffer * instance_offset_buffer;
		::framework::gpu::device_memory * instance_offset_device_memory;
		::framework::gpu::descriptor_set * build_emissive_triangle_list_host_descriptor_set;
		::framework::gpu::descriptor_set * build_emissive_triangle_list_device_descriptor_set;
		::framework::gpu::descriptor_set_layout * build_emissive_triangle_list_descriptor_set_layout;
		::framework::gpu::pipeline_layout * build_emissive_triangle_list_pipeline_layout;
		::framework::gpu::pipeline * build_emissive_triangle_list_pipeline;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::shader_module * build_emissive_triangle_list_shader_module;
		::framework::gpu::semaphore * update_semaphore;
		::std::vector<::framework::graphics::ray_tracing::triangle_mesh_scene_geometry *> triangle_mesh_scene_geometries;
		::framework::coroutine::mutex triangle_mesh_scene_geometries_mutex;
	};
}

#include "graphics/ray_tracing/light_collection.inl"