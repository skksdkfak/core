#include "gpu/core.hpp"
#include "gpu/utility.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include <algorithm>

::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::triangle_mesh_scene_geometry(::framework::graphics::ray_tracing::scene * scene, ::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info) :
	scene(scene),
	geometry(static_cast<::framework::graphics::ray_tracing::triangle_mesh_geometry *>(create_info.triangle_mesh_geometry)),
	materials(reinterpret_cast<::framework::graphics::ray_tracing::material * const *>(create_info.materials), reinterpret_cast<::framework::graphics::ray_tracing::material * const *>(create_info.materials) + create_info.material_count),
	media(reinterpret_cast<::framework::graphics::ray_tracing::medium * const *>(create_info.media), reinterpret_cast<::framework::graphics::ray_tracing::medium * const *>(create_info.media) + create_info.material_count)
{
	this->initialization_shared_task = this->initialize(create_info);
}

::framework::coroutine::shared_task<void> (::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::initialize)(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info)
{
	co_await this->geometry->get_initialization_shared_task();
	this->instance_id = this->scene->allocate_geometry_instance_id(this->geometry->get_geometry_count());
	this->instance_index = this->scene->allocate_instance();
	co_await this->scene->add_triangle_mesh_scene_geometry_to_initialize(this);
	if (::std::ranges::find_if(this->materials,
		[](::framework::graphics::ray_tracing::material * material)
		{
			return material->is_emissive();
		}) != this->materials.end())
	{
		co_await this->scene->get_light_collection()->add_triangle_mesh_scene_geometry(this);
	}
}

void ::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::set_transform(::glm::mat3x4 const & transform)
{
	this->set_transform_device(transform);
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::set_transform_device)(::glm::mat3x4 const transform) noexcept
{
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->local_to_world_matrix = transform;
	}
	co_await this->scene->add_triangle_mesh_scene_geometry_to_update_transform(this);
}

::framework::coroutine::immediate_task<void> (::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::initialize)(::framework::command_context * transfer_command_context)
{
	::std::uint32_t const submesh_count = this->geometry->get_geometry_count();
	::framework::graphics::submesh const * submeshes = this->geometry->get_submeshes();
	for (::std::uint32_t i = 0; i < submesh_count; i++)
	{
		struct ::framework::graphics::ray_tracing::scene::instance_shader_binding_table_record const instance_shader_binding_table_record
		{
			.material_index = this->materials[i]->get_id(),
			.index_offset = static_cast<::std::uint32_t>(this->geometry->get_indices_memory_offset() / sizeof(::std::uint32_t)) + submeshes[i].index_offset,
			.vertex_offset = static_cast<::std::uint32_t>(this->geometry->get_vertices_memory_offset() / sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout))
		};

		co_await transfer_command_context->update_buffer(this->scene->get_shader_binding_table_buffer(), &instance_shader_binding_table_record, this->scene->get_closest_hit_shader_group_offset(this->get_geometry_instance_id(i)), sizeof(instance_shader_binding_table_record));

		struct
		{
			::std::uint32_t flags;
			::std::uint32_t vertex_offset;
			::std::uint32_t index_offset;
			::std::uint32_t matrix_index;
			::std::uint32_t material_index;
			::std::uint32_t medium_index;
			::std::uint32_t padding[2];
		}
		const geometry_instance
		{
			.flags = 0,
			.vertex_offset = static_cast<::std::uint32_t>(this->geometry->get_vertices_memory_offset() / sizeof(::framework::graphics::ray_tracing::triangle_mesh_geometry::vertex_device_layout)),
			.index_offset = static_cast<::std::uint32_t>(this->geometry->get_indices_memory_offset() / sizeof(::std::uint32_t)) + submeshes[i].index_offset,
			.matrix_index = this->get_instance_index(),
			.material_index = this->materials[i]->get_id(),
			.medium_index = this->media[i] ? this->media[i]->get_id() : ::std::uint32_t(-1)
		};

		co_await transfer_command_context->update_buffer(this->scene->get_geometry_instances_buffer(), geometry_instance, this->get_geometry_instance_id(i));
	}

	::framework::gpu::transform_matrix transform_matrix;
	::glm::mat3x4 const local_to_world_matrix = co_await this->get_local_to_world_matrix();
	::std::memcpy(&transform_matrix, &local_to_world_matrix, sizeof(transform_matrix));

	::framework::gpu::acceleration_structure_instance acceleration_structure_instance;
	acceleration_structure_instance.transform = transform_matrix;
	acceleration_structure_instance.instance_custom_index = this->get_instance_id();
	acceleration_structure_instance.mask = 0xff;
	acceleration_structure_instance.instance_shader_binding_table_record_offset = this->get_instance_index() * this->scene->get_hit_group_count();
	acceleration_structure_instance.flags = ::framework::gpu::geometry_instance_flags::triangle_facing_cull_disable_bit;
	acceleration_structure_instance.acceleration_structure_reference = static_cast<::framework::graphics::ray_tracing::triangle_mesh_geometry *>(this->geometry)->get_bottom_level_acceleration_structure_device_address();
	
	co_await transfer_command_context->update_buffer(this->scene->get_instances_buffer(), acceleration_structure_instance, this->get_instance_index());
}
