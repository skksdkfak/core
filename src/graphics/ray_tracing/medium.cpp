#include "graphics/ray_tracing/medium.hpp"
#include "graphics/ray_tracing/medium_system.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "physics/color_space.hpp"
#include "physics/spectrum.hpp"
#include <cstddef>
#include <cstring>

::framework::graphics::ray_tracing::medium::medium(::framework::graphics::ray_tracing::medium_system * medium_system, ::std::uint32_t id) :
	medium_system(medium_system),
	id(id)
{
	this->initialization_shared_task = this->initialize();
}

::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::medium::initialize)()
{
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->header.data.x |= (0u & ::framework::graphics::ray_tracing::medium_header::medium_flags::medium_type_bits) << ::framework::graphics::ray_tracing::medium_header::medium_flags::medium_type_offset;

		::framework::physics::rgb_illuminant_spectrum sigma_a_rgb_illuminant_spectrum(*::framework::physics::rgb_color_space::srgb, ::glm::vec3(0.0021f, 0.0015f, 0.0018f) * 0.0015f);
		::framework::physics::rgb_illuminant_spectrum sigma_s_rgb_illuminant_spectrum(*::framework::physics::rgb_color_space::srgb, ::glm::vec3(0.10615f, 0.10725f, 0.10835f) * 0.0010f);
		::framework::physics::constant_spectrum le_rgb_illuminant_spectrum(0.0f);
		this->sigma_a_spec = this->medium_system->get_scene_renderer()->get_spectrum_system()->create_densely_sampled_spectrum(sigma_a_rgb_illuminant_spectrum, false);
		co_await this->sigma_a_spec->get_initialization_shared_task();
		this->sigma_s_spec = this->medium_system->get_scene_renderer()->get_spectrum_system()->create_densely_sampled_spectrum(sigma_s_rgb_illuminant_spectrum, false);
		co_await this->sigma_s_spec->get_initialization_shared_task();
		this->le_spec = this->medium_system->get_scene_renderer()->get_spectrum_system()->create_densely_sampled_spectrum(le_rgb_illuminant_spectrum, false);
		co_await this->le_spec->get_initialization_shared_task();
		this->device_blob.sigma_a_spec = this->sigma_a_spec->get_densely_sampled_spectrum_device_blob();
		this->device_blob.sigma_s_spec = this->sigma_s_spec->get_densely_sampled_spectrum_device_blob();
		this->device_blob.le_spec = this->le_spec->get_densely_sampled_spectrum_device_blob();
	}

	co_await this->medium_system->add_medium_to_update(this);
}

::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::medium_data_blob>(::framework::graphics::ray_tracing::medium::get_device_data_and_commit)()
{
	::framework::graphics::ray_tracing::medium_data_blob medium_data_blob;
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	medium_data_blob.header = this->header;
	static_assert(sizeof(struct ::framework::graphics::ray_tracing::medium::device_blob) <= sizeof(struct ::framework::graphics::ray_tracing::medium_payload));
	::std::memcpy(&medium_data_blob.payload, &this->device_blob, sizeof(struct ::framework::graphics::ray_tracing::medium::device_blob));
	co_return medium_data_blob;
}
