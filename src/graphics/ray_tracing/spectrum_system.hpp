#pragma once

#include "command_context.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/shared_task.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "physics/color.hpp"
#include "physics/spectrum.hpp"
#include <glm/glm.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;
	class spectrum_system;

	class spectrum
	{
	public:
		struct spectrum_pointer
		{
			::std::uint32_t pointer;
			::glm::uvec3 padding;
		};

		enum type
		{
			constant_spectrum,
			densely_sampled_spectrum,
			piecewise_linear_spectrum,
			blackbody_spectrum,
			rgb_albedo_spectrum,
			rgb_unbounded_spectrum,
			rgb_illuminant_spectrum
		};

		spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer);

		::framework::coroutine::shared_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system);

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept;

		virtual ::std::size_t get_size() const = 0;

		virtual ::framework::graphics::ray_tracing::spectrum::spectrum_pointer get_pointer() const = 0;

		virtual ::framework::coroutine::immediate_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context) = 0;

		static constexpr ::std::uint32_t tag_shift = 28;
		static constexpr ::std::uint32_t tag_bits = 32 - ::framework::graphics::ray_tracing::spectrum::tag_shift;
		static constexpr ::std::uint32_t tag_mask = ((1u << ::framework::graphics::ray_tracing::spectrum::tag_bits) - 1) << ::framework::graphics::ray_tracing::spectrum::tag_shift;
		static constexpr ::std::uint32_t ptr_mask = ~::framework::graphics::ray_tracing::spectrum::tag_mask;

	protected:
		::framework::coroutine::shared_task<void> initialization_shared_task;
		bool is_pointer;
	};

	class constant_spectrum : public ::framework::graphics::ray_tracing::spectrum
	{
	public:
		constant_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::constant_spectrum const & constant_spectrum);

		::std::size_t get_size() const override;

		::framework::graphics::ray_tracing::spectrum::spectrum_pointer get_pointer() const override;

		::framework::coroutine::immediate_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context) override;

	private:
		::std::uint32_t four_byte_address;
		float c;
	};

	class densely_sampled_spectrum : public ::framework::graphics::ray_tracing::spectrum
	{
	public:
		struct device_blob
		{
			::std::uint16_t lambda_min;
			::std::uint16_t lambda_max;
			::std::uint32_t four_byte_address;
			::std::uint32_t padding[2];
		};

		densely_sampled_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::densely_sampled_spectrum const & densely_sampled_spectrum);

		::std::size_t get_size() const override;

		::framework::graphics::ray_tracing::spectrum::spectrum_pointer get_pointer() const override;

		::framework::coroutine::immediate_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context) override;

		struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob get_densely_sampled_spectrum_device_blob() const;

	private:
		::std::uint16_t lambda_min;
		::std::uint16_t lambda_max;
		::std::uint32_t four_byte_address;
		::std::vector<float> values;
	};

	class piecewise_linear_spectrum : public ::framework::graphics::ray_tracing::spectrum
	{
	public:
		struct device_blob
		{
			::std::uint32_t four_byte_address;
			::std::uint32_t four_byte_size;
			::std::uint32_t padding[2];
		};

		piecewise_linear_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::piecewise_linear_spectrum const & piecewise_linear_spectrum);

		::std::size_t get_size() const override;

		::framework::graphics::ray_tracing::spectrum::spectrum_pointer get_pointer() const override;

		::framework::coroutine::immediate_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context) override;

		struct ::framework::graphics::ray_tracing::piecewise_linear_spectrum::device_blob get_piecewise_linear_spectrum_device_blob() const;

	private:
		::std::uint32_t four_byte_address;
		::std::vector<float> lambdas;
		::std::vector<float> values;
	};

	class blackbody_spectrum : public ::framework::graphics::ray_tracing::spectrum
	{
	public:
		struct device_blob
		{
			float T;
			float normalization_factor;
			::std::uint32_t padding[2];
		};

		blackbody_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::blackbody_spectrum const & blackbody_spectrum);

		::std::size_t get_size() const override;

		::framework::graphics::ray_tracing::spectrum::spectrum_pointer get_pointer() const override;

		::framework::coroutine::immediate_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context) override;

		struct ::framework::graphics::ray_tracing::blackbody_spectrum::device_blob get_blackbody_spectrum_device_blob() const;

	private:
		::std::uint32_t four_byte_address;
		struct ::framework::graphics::ray_tracing::blackbody_spectrum::device_blob device_blob;
	};

	class rgb_to_spectrum_table
	{
	public:
		struct device_blob
		{
			::std::uint32_t offset;
			::std::uint32_t padding[3];
		};

		::framework::graphics::ray_tracing::rgb_to_spectrum_table(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, ::std::uint32_t offset, ::std::vector<float> && data);

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept;

		struct ::framework::graphics::ray_tracing::rgb_to_spectrum_table::device_blob get_device_blob() const;

	private:
		::framework::coroutine::shared_task<void> initialize(::framework::graphics::ray_tracing::spectrum_system * spectrum_system);

		friend ::framework::graphics::ray_tracing::spectrum_system;
		struct ::framework::graphics::ray_tracing::rgb_to_spectrum_table::device_blob device_blob;
		::std::vector<float> data;
		::framework::coroutine::shared_task<void> initialization_shared_task;
	};

	class rgb_color_space
	{
	public:
		struct device_blob
		{
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob illuminant;
			struct ::framework::graphics::ray_tracing::rgb_to_spectrum_table::device_blob rgb_to_spectrum_table;
		};

		::framework::graphics::ray_tracing::rgb_color_space(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, ::framework::graphics::ray_tracing::densely_sampled_spectrum const & illuminant, ::framework::graphics::ray_tracing::rgb_to_spectrum_table const & rgb_to_spectrum_table);

		struct ::framework::graphics::ray_tracing::rgb_color_space::device_blob get_device_blob() const;

	private:
		::framework::graphics::ray_tracing::densely_sampled_spectrum const & illuminant;
		::framework::graphics::ray_tracing::rgb_to_spectrum_table const & rgb_to_spectrum_table;
	};

	class spectrum_system
	{
	public:
		struct uniform_buffer
		{
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob x;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob y;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob z;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob r_bar;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob g_bar;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob b_bar;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob cie_illum_d6500;
			struct ::framework::graphics::ray_tracing::rgb_color_space::device_blob srgb_rgb_color_space;
		};

		spectrum_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		~spectrum_system();

		::framework::graphics::ray_tracing::rgb_to_spectrum_table * create_rgb_to_spectrum_table(::framework::physics::rgb_to_spectrum_table const & rgb_to_spectrum_table);

		::framework::graphics::ray_tracing::constant_spectrum * create_constant_spectrum(::framework::physics::constant_spectrum const & constant_spectrum, bool is_pointer);

		::framework::graphics::ray_tracing::densely_sampled_spectrum * create_densely_sampled_spectrum(::framework::physics::densely_sampled_spectrum const & densely_sampled_spectrum, bool is_pointer);

		::framework::graphics::ray_tracing::piecewise_linear_spectrum * create_piecewise_linear_spectrum(::framework::physics::piecewise_linear_spectrum const & piecewise_linear_spectrum, bool is_pointer);
		
		::framework::graphics::ray_tracing::blackbody_spectrum * create_blackbody_spectrum(::framework::physics::blackbody_spectrum const & blackbody_spectrum, bool is_pointer);

		::framework::coroutine::immediate_task<void> add_rgb_to_spectrum_table_to_initialize(::framework::graphics::ray_tracing::rgb_to_spectrum_table * rgb_to_spectrum_table);

		::framework::coroutine::immediate_task<void> add_spectrum_to_initialize(::framework::graphics::ray_tracing::spectrum * spectrum);

		::framework::coroutine::immediate_task<void> update();

		void bind_descriptor_set();

		::framework::coroutine::immediate_task<void> upload_spectrum_data(class ::framework::command_context * const command_context, ::framework::gpu::device_size offset, ::framework::gpu::device_size size, void const * data);

		struct ::framework::graphics::ray_tracing::spectrum_system::uniform_buffer * get_current_frame_uniform_buffer_mapped_data() const noexcept;

		::framework::gpu::semaphore * get_update_semaphore() const noexcept;

		::std::uint32_t get_uniform_buffer_alignment() const { return this->uniform_buffer_alignment; }

		::framework::graphics::ray_tracing::scene_renderer * get_scene_renderer() const;

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_set * host_descriptor_set;
		::framework::gpu::descriptor_set * device_descriptor_set;
		::framework::gpu::buffer * uniform_buffer;
		::framework::gpu::device_memory * uniform_buffer_device_memory;
		::framework::gpu::buffer * rgb_spectrum_table_buffer;
		::framework::gpu::device_memory * rgb_spectrum_table_device_memory;
		::framework::gpu::buffer * spectrum_buffer;
		::framework::gpu::device_memory * spectrum_device_memory;
		::framework::gpu::semaphore * update_semaphore;
		::std::vector<::framework::graphics::ray_tracing::rgb_to_spectrum_table *> rgb_to_spectrum_tables_to_initialize;
		::std::vector<::framework::graphics::ray_tracing::spectrum *> spectra_to_initialize;
		::framework::coroutine::mutex rgb_to_spectrum_tables_to_initialize_mutex;
		::framework::coroutine::mutex spectra_to_initialize_mutex;
		::framework::graphics::ray_tracing::rgb_to_spectrum_table * srgb_rgb_to_spectrum_table;
		::framework::graphics::ray_tracing::rgb_color_space * srgb_rgb_color_space;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * r_bar;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * g_bar;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * b_bar;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * x;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * y;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * z;
		::framework::graphics::ray_tracing::densely_sampled_spectrum * cie_illum_d6500;
		::std::uint32_t uniform_buffer_alignment;
		::std::byte * uniform_buffer_mapped_data;
		::std::uint32_t rgb_to_spectrum_table_pointer;
		::std::uint32_t spectrum_pointer;
		::std::uint32_t rgb_to_spectrum_table_resolution = 64;
	};
}

#include "graphics/ray_tracing/spectrum_system.inl"