#pragma once

#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "graphics/light.hpp"
#include "graphics/ray_tracing/image.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "macros.hpp"
#include "physics/spectrum.hpp"
#include <glm/glm.hpp>
#include <memory>

namespace framework::graphics::ray_tracing
{
	class scene;
	class scene_renderer;

	class light : public virtual ::framework::graphics::light
	{
	public:
		enum class changes
		{
			none = 0x0,
			active = 0x1,
			position = 0x2,
			direction = 0x4,
			intensity = 0x8,
			surface_area = 0x10,
		};
		
		enum type
		{
			image_infinite_light,
			emissive_triangle,
			point_light
		};

		struct device_blob
		{
			::std::uint32_t header;
			::std::uint32_t data[21];
		};

		enum ::framework::graphics::ray_tracing::light::changes get_changes() const noexcept;

		virtual ::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::light::device_blob> get_data() = 0;

	protected:
		enum ::framework::graphics::ray_tracing::light::changes changes = ::framework::graphics::ray_tracing::light::changes::none;
	};

	class point_light : public ::framework::graphics::ray_tracing::light, public ::framework::graphics::point_light
	{
	public:
		struct device_blob
		{
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob intensity;
			::glm::vec4 position_and_scale;
		};

		point_light(::framework::graphics::ray_tracing::scene * scene);

		void set_position(::glm::vec3 const & position) override;

		void set_intensity(::framework::physics::spectrum const & intensity) override;

		void set_scale(float scale) override;

		::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::light::device_blob> get_data() override;

	private:
		::framework::coroutine::self_destructible_coroutine set_position_device(::glm::vec3 const position) noexcept;

		::framework::coroutine::self_destructible_coroutine set_intensity_device(::framework::physics::spectrum const & intensity) noexcept;

		::framework::coroutine::self_destructible_coroutine set_scale_device(float scale) noexcept;

		::framework::graphics::ray_tracing::scene * scene;
		::std::shared_ptr<::framework::graphics::ray_tracing::densely_sampled_spectrum> intensity;
		struct ::framework::graphics::ray_tracing::point_light::device_blob device_blob;
		::framework::coroutine::mutex device_data_mutex;
	};

	class image_infinite_light : public ::framework::graphics::ray_tracing::light, public ::framework::graphics::image_infinite_light
	{
	public:
		struct device_blob
		{
			::framework::graphics::ray_tracing::image::device_blob image;
			float scale;
		};

		image_infinite_light(::framework::graphics::ray_tracing::scene * scene);

		::std::uint32_t get_id() const noexcept;

		void set_image(::framework::graphics::image * image) override;

		void set_scale(float scale) override;

		::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::light::device_blob> get_data() override;

		::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::image_infinite_light::device_blob> get_image_infinite_light_device_data() noexcept;

	private:
		::framework::coroutine::self_destructible_coroutine set_image_device(::framework::graphics::image * image) noexcept;

		::framework::coroutine::self_destructible_coroutine set_scale_device(float scale) noexcept;

		::framework::graphics::ray_tracing::scene * scene;
		struct ::framework::graphics::ray_tracing::image_infinite_light::device_blob device_blob;
		::std::uint8_t device_data_state;
		::framework::graphics::ray_tracing::image * image;
		::framework::coroutine::mutex device_data_mutex;
	};

	ENUM_CLASS_FLAGS(enum ::framework::graphics::ray_tracing::light::changes)
}

#include "graphics/ray_tracing/light.inl"