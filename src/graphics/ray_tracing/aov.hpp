#pragma once

#include "gpu/core.hpp"
#include "graphics/aov.hpp"
#include <cstdint>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class aov : public ::framework::graphics::aov
	{
	public:
		aov(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		~aov();

		::framework::gpu::image * get_image(::framework::graphics::aov::type type) const noexcept override;

		void resize();

		char const * get_aov_preprocessor_define(::framework::graphics::aov::type type);

		::framework::gpu::image_view * get_geometry_instance_id_image_view() const noexcept;

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		::framework::gpu::descriptor_set * get_descriptor_set() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		void create_screen_buffers();

		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::image * geometry_instance_id_image;
		::framework::gpu::image_view * geometry_instance_id_image_view;
		::framework::gpu::device_memory * geometry_instance_id_device_memory;
	};
}

#include "graphics/ray_tracing/aov.inl"