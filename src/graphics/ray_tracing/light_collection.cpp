#include "graphics/ray_tracing/light_collection.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "gpu/utility.hpp"
#include "profiler.hpp"

::framework::graphics::ray_tracing::light_collection::light_collection(::framework::graphics::ray_tracing::scene * scene) :
	scene(scene)
{
	::framework::graphics::ray_tracing::scene_renderer * const scene_renderer = this->scene->get_scene_renderer();
	::framework::gpu::device * const device = scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		// triangle_data
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 2;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->build_emissive_triangle_list_descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			scene_renderer->get_path_tracer_descriptor_set_layout(),
			scene_renderer->get_scene_descriptor_set_layout(),
			this->build_emissive_triangle_list_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(::std::uint32_t) * 5;
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 3;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->build_emissive_triangle_list_pipeline_layout));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(struct ::framework::graphics::ray_tracing::light::device_blob) * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->emissive_triangle_list_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->emissive_triangle_list_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->emissive_triangle_list_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->emissive_triangle_list_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->emissive_triangle_list_buffer;
		bind_buffer_memory_info.memory = this->emissive_triangle_list_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::std::uint32_t) * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->instance_offset_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->instance_offset_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->instance_offset_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->instance_offset_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->instance_offset_buffer;
		bind_buffer_memory_info.memory = this->instance_offset_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::resource::slang_source_info slang_source_info;
		slang_source_info.file_name = "build_emissive_triangle_list.slang";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = &slang_source_info;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;

		this->build_emissive_triangle_list_shader_module = scene_renderer->get_resource_manager()->load_shader_module(device, shader_module_info);
	}

	{
		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->build_emissive_triangle_list_shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = nullptr;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->build_emissive_triangle_list_pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(device->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->build_emissive_triangle_list_pipeline));
	}

	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_host_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->build_emissive_triangle_list_descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->build_emissive_triangle_list_host_descriptor_set));
	}

	if (scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->build_emissive_triangle_list_device_descriptor_set = this->build_emissive_triangle_list_host_descriptor_set;
	}
	else
	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->build_emissive_triangle_list_descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->build_emissive_triangle_list_device_descriptor_set));
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_info;
	descriptor_buffer_info.buffer = this->emissive_triangle_list_buffer;
	descriptor_buffer_info.offset = 0;
	descriptor_buffer_info.range = ::framework::gpu::whole_size;
	descriptor_buffer_info.stride = 48;

	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = this->build_emissive_triangle_list_host_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_info;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_semaphore));
}

::framework::graphics::ray_tracing::light_collection::~light_collection()
{
	::framework::gpu::device * const device = this->scene->get_scene_renderer()->get_gpu_context()->get_device();
	device->destroy_pipeline(this->build_emissive_triangle_list_pipeline, nullptr);
	device->destroy_descriptor_set_layout(this->build_emissive_triangle_list_descriptor_set_layout, nullptr);
	device->destroy_pipeline_layout(this->build_emissive_triangle_list_pipeline_layout, nullptr);
	device->free_descriptor_sets(this->scene->get_scene_renderer()->get_host_descriptor_pool(), 1, &this->build_emissive_triangle_list_host_descriptor_set);
	if (!this->scene->get_scene_renderer()->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		device->free_descriptor_sets(this->scene->get_scene_renderer()->get_device_descriptor_pool(), 1, &this->build_emissive_triangle_list_device_descriptor_set);
	}
	device->destroy_semaphore(this->update_semaphore, nullptr);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::light_collection::update)()
{
	class ::framework::command_context * const compute_command_context = co_await this->scene->get_scene_renderer()->get_command_buffer_manager().get_compute_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const compute_command_buffer = compute_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(compute_command_buffer->begin_command_buffer(&command_buffer_begin_info));

	::framework::graphics::ray_tracing::scene_renderer * const scene_renderer = this->scene->get_scene_renderer();

	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		scene_renderer->get_path_tracer_descriptor_set(),
		scene_renderer->get_scene_descriptor_set(),
		this->build_emissive_triangle_list_device_descriptor_set
	};

	::std::uint32_t triangle_offset = 0;
	::std::uint32_t const dynamic_offsets[2]{ 0, 0 };

	{
		//::framework::profiler::scoped_timestamp const scoped_timestamp(*this->scene->get_scene_renderer()->get_profiler(), compute_command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "::framework::graphics::ray_tracing::light_collection::update");
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_mutex.scoped_lock();
		for (auto triangle_mesh_scene_geometry : this->triangle_mesh_scene_geometries)
		{
			::framework::graphics::ray_tracing::triangle_mesh_geometry * const geometry = static_cast<::framework::graphics::ray_tracing::triangle_mesh_geometry *>(triangle_mesh_scene_geometry->get_geometry());
			::std::uint32_t const submesh_count = geometry->get_geometry_count();
			::framework::graphics::submesh const * submeshes = geometry->get_submeshes();

			for (::std::uint32_t submesh_index = 0; submesh_index < submesh_count; submesh_index++)
			{
				::framework::graphics::ray_tracing::material * const material = static_cast<::framework::graphics::ray_tracing::material *>(triangle_mesh_scene_geometry->get_material(submesh_index));
				if (!material->is_emissive())
				{
					continue;
				}

				::std::uint32_t const triangle_count = submeshes[submesh_index].index_count / 3;
				struct
				{
					::std::uint32_t light_index;
					::std::uint32_t material_id;
					::std::uint32_t instance_id;
					::std::uint32_t triangle_count;
					::std::uint32_t triangle_offset;
				}
				const push_constants
				{
					.light_index = 0,
					.material_id = material->get_id(),
					.instance_id = triangle_mesh_scene_geometry->get_geometry_instance_id(submesh_index),
					.triangle_count = triangle_count,
					.triangle_offset = triangle_offset
				};

				co_await compute_command_context->update_buffer(this->instance_offset_buffer, triangle_offset, triangle_mesh_scene_geometry->get_geometry_instance_id(submesh_index));
				compute_command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->build_emissive_triangle_list_pipeline);
				compute_command_buffer->bind_descriptor_pool(scene_renderer->get_device_descriptor_pool());
				compute_command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->build_emissive_triangle_list_pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), dynamic_offsets);
				compute_command_buffer->push_constants(this->build_emissive_triangle_list_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
				compute_command_buffer->dispatch((triangle_count + 64 - 1) / 64, 1, 1);

				triangle_offset += triangle_count;
			}
		}
	}
	assert_framework_gpu_result(compute_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_infos[2];
	wait_semaphore_infos[0].semaphore = this->scene->get_initialize_geometries_semaphore();
	wait_semaphore_infos[0].value = this->scene->get_scene_renderer()->get_last_frame_index() + 1;
	wait_semaphore_infos[0].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[0].device_index = 0;

	wait_semaphore_infos[1].semaphore = this->scene->get_initialize_geometries_semaphore();
	wait_semaphore_infos[1].value = this->scene->get_scene_renderer()->get_last_frame_index() + 1;
	wait_semaphore_infos[1].stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	wait_semaphore_infos[1].device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_semaphore;
	signal_semaphore_info.value = this->scene->get_scene_renderer()->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene->get_scene_renderer()->get_command_buffer_manager().get_compute_queue()->submit_and_wait(compute_command_context, static_cast<::std::uint32_t>(::std::size(wait_semaphore_infos)), wait_semaphore_infos, 1, &signal_semaphore_info);
	co_await this->scene->get_scene_renderer()->get_command_buffer_manager().get_compute_queue()->release_command_context(compute_command_context);

	this->scene->get_current_frame_uniform_buffer_mapped_data()->light_collection.triangle_count = triangle_offset;
	this->scene->get_current_frame_uniform_buffer_mapped_data()->light_collection.active_triangle_count = triangle_offset;
}

void ::framework::graphics::ray_tracing::light_collection::bind_descriptor_set()
{
	::framework::graphics::ray_tracing::scene_renderer * const scene_renderer = this->scene->get_scene_renderer();
	if (scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		return;
	}

	::framework::gpu::device * const device = scene_renderer->get_gpu_context()->get_device();

	::framework::gpu::copy_descriptor_set copy_descriptor_set;
	copy_descriptor_set.src_set = this->build_emissive_triangle_list_host_descriptor_set;
	copy_descriptor_set.src_binding = 0;
	copy_descriptor_set.src_array_element = 0;
	copy_descriptor_set.dst_set = this->build_emissive_triangle_list_device_descriptor_set;
	copy_descriptor_set.dst_binding = 0;
	copy_descriptor_set.dst_array_element = 0;
	copy_descriptor_set.descriptor_count = 1;
	copy_descriptor_set.descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;

	device->update_descriptor_sets(0, nullptr, 1, &copy_descriptor_set);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::light_collection::add_triangle_mesh_scene_geometry)(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->triangle_mesh_scene_geometries_mutex.scoped_lock();
	this->triangle_mesh_scene_geometries.push_back(triangle_mesh_scene_geometry);
}
