inline ::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::image::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::image::get_id)() const noexcept
{
	return this->id;
}

inline ::framework::graphics::ray_tracing::image::device_blob(::framework::graphics::ray_tracing::image::get_device_blob)() const noexcept
{
	return ::framework::graphics::ray_tracing::image::device_blob{ this->id };
}