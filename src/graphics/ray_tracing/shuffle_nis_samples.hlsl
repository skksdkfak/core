#include "algorithm/philox_bijective_function.hlsl"
#include "pcg32_sample_generator.hlsl"
#include "ray_tracing/nis_path_integrator/config.hlsl"
#include "spectrum.hlsl"

[[vk::binding(0, 1)]] cbuffer size_buffer_t : register(b0, space1)
{
    uint size;
};

[[vk::binding(1, 1)]] cbuffer seed_buffer_t : register(b1, space1)
{
    uint seed;
};

[[vk::binding(2, 1)]]
StructuredBuffer<float16_t> sample_buffer : register(t0, space1);

[[vk::binding(3, 1)]]
RWStructuredBuffer<float16_t> nis_samples_buffer : register(u0, space1);

[[vk::binding(4, 1)]]
RWStructuredBuffer<float16_t> nis_spatial_features_buffer : register(u1, space1);

[[vk::binding(5, 1)]]
RWStructuredBuffer<float16_t> nis_features_buffer : register(u2, space1);

[[vk::binding(6, 1)]]
RWStructuredBuffer<float16_t> nis_target_buffer : register(u3, space1);

[[vk::binding(7, 1)]]
RWStructuredBuffer<float16_t> nis_sample_pdf_buffer : register(u4, space1);

[[vk::binding(8, 1)]]
RWStructuredBuffer<float16_t> light_field_target_buffer : register(u5, space1);

[[vk::binding(9, 1)]]
RWStructuredBuffer<float16_t> light_field_one_blob_encode_features_buffer : register(u6, space1);

[[vk::binding(10, 1)]]
RWStructuredBuffer<float16_t> cv_shape_target_buffer : register(u7, space1);

[[vk::binding(11, 1)]]
RWStructuredBuffer<float16_t> cv_shape_normalizing_flow_x_buffer : register(u8, space1);

[[vk::binding(12, 1)]]
RWStructuredBuffer<float16_t> cv_shape_normalizing_flow_spatial_features_buffer : register(u9, space1);

[[vk::binding(13, 1)]]
RWStructuredBuffer<float16_t> cv_shape_normalizing_flow_features_buffer : register(u10, space1);

namespace framework
{
    inline float16_t filter_target_value(float16_t value)
    {
        return isinf(value) || isnan(value) ? 0.0f : value;
    }

    struct shuffle_nis_samples_scan_executor
    {
        inline void initialize()
        {
            this.sample_generator = ::framework::pcg32_sample_generator::create(seed);
            this.philox_bijective_function.init(size, sample_generator);
        }

        inline uint4 load(uint global_index, uint local_index)
        {
            this.gather_keys[local_index].x = this.philox_bijective_function(global_index * 4 + 0);
            this.gather_keys[local_index].y = this.philox_bijective_function(global_index * 4 + 1);
            this.gather_keys[local_index].z = this.philox_bijective_function(global_index * 4 + 2);
            this.gather_keys[local_index].w = this.philox_bijective_function(global_index * 4 + 3);
            return this.gather_keys[local_index] < size;
        }

        inline void store(uint global_index, uint local_index, uint4 data)
        {
            const uint4 flags = this.gather_keys[local_index] < size;
            if (flags.x)
            {
                this.store_nis_sample(data.x, this.gather_keys[local_index].x);
            }
            if (flags.y)
            {
                this.store_nis_sample(data.y, this.gather_keys[local_index].y);
            }
            if (flags.z)
            {
                this.store_nis_sample(data.z, this.gather_keys[local_index].z);
            }
            if (flags.w)
            {
                this.store_nis_sample(data.w, this.gather_keys[local_index].w);
            }
        }

        inline void store_nis_sample(uint scatter_index, uint gather_index)
        {
            const uint gather_offset = gather_index * ::framework::sample_buffer_stride;
            const uint scatter_x_offset = scatter_index * 2;
            const uint nis_spatial_features_offset = scatter_index * ::framework::nis_spatial_feature_stride;
            const uint light_field_one_blob_encode_features_offset = scatter_index * ::framework::light_field_one_blob_encode_features_stride;
            [unroll]
            for (uint i = 0; i < ::framework::nis_spatial_feature_count; i++)
            {
                nis_spatial_features_buffer[nis_spatial_features_offset + i] = sample_buffer[gather_offset + i];
            }
            [unroll]
            for (uint i = 0; i < ::framework::nis_spatial_feature_count; i++)
            {
                // note: currently the same for nis & cv_shape
                //cv_shape_normalizing_flow_spatial_features_buffer[nis_spatial_features_offset + i] = sample_buffer[gather_offset + i];
            }
            light_field_one_blob_encode_features_buffer[light_field_one_blob_encode_features_offset + 0] = sample_buffer[gather_offset + 3];
            light_field_one_blob_encode_features_buffer[light_field_one_blob_encode_features_offset + 1] = sample_buffer[gather_offset + 4];
            light_field_one_blob_encode_features_buffer[light_field_one_blob_encode_features_offset + 2] = sample_buffer[gather_offset + 5];
            light_field_one_blob_encode_features_buffer[light_field_one_blob_encode_features_offset + 3] = sample_buffer[gather_offset + 6];
            [unroll]
            for (uint i = 0; i < framework::sampled_spectrum::n; i++)
            {
                light_field_one_blob_encode_features_buffer[light_field_one_blob_encode_features_offset + 4 + i] = sample_buffer[gather_offset + 7 + i];
            }
            nis_target_buffer[scatter_index] = ::framework::filter_target_value(sample_buffer[gather_offset + 8]);
            cv_shape_target_buffer[scatter_index] = ::framework::filter_target_value(sample_buffer[gather_offset + 9]);
            [unroll]
            for (uint i = 0; i < framework::sampled_spectrum::n; i++)
            {
                light_field_target_buffer[scatter_index * ::framework::sampled_spectrum::n + i] = ::framework::filter_target_value(sample_buffer[gather_offset + 10 + i]);
            }
            vector<float16_t, 2> x = vector<float16_t, 2>(sample_buffer[gather_offset + 11], sample_buffer[gather_offset + 12]);
            nis_samples_buffer[scatter_x_offset + 0] = x.x;
            nis_samples_buffer[scatter_x_offset + 1] = x.y;
            cv_shape_normalizing_flow_x_buffer[scatter_x_offset + 0] = x.x;
            cv_shape_normalizing_flow_x_buffer[scatter_x_offset + 1] = x.y;
            nis_features_buffer[scatter_index + ::framework::x_a_row_index * size] = x.x;
            cv_shape_normalizing_flow_features_buffer[scatter_index + ::framework::x_a_row_index * size] = x.x;
            nis_sample_pdf_buffer[scatter_index] = sample_buffer[gather_offset + 13];
        }

        ::framework::pcg32_sample_generator sample_generator;
        ::framework::philox_bijective_function philox_bijective_function;
        uint4 gather_keys[UINT4_PER_THREAD];
    };
}