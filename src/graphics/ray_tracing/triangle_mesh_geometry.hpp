#pragma once

#include "graphics/triangle_mesh_geometry.hpp"
#include "coroutine/shared_task.hpp"
#include "coroutine/task.hpp"
#include <gpu/core.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class triangle_mesh_geometry : public ::framework::graphics::triangle_mesh_geometry
	{
	public:
		struct vertex_device_layout
		{
			::glm::vec3 position;
			::glm::vec3 normal;
			::glm::vec2 uv;
		};

		struct geometry_instance
		{
			::glm::uvec4 data0;
			::glm::uvec4 data1;
		};

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept override;

		::std::uint32_t get_geometry_count() const noexcept override;

		::framework::gpu::acceleration_structure * get_bottom_level_acceleration_structure() const noexcept;

		::framework::gpu::device_address get_bottom_level_acceleration_structure_device_address() const noexcept;

		::framework::graphics::submesh const * get_submeshes() const noexcept;

		::framework::gpu::device_size get_indices_memory_offset() const noexcept;

		::framework::gpu::device_size get_vertices_memory_offset() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		triangle_mesh_geometry(::std::uint32_t id, ::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::triangle_mesh_geometry_create_info const & create_info);

		~triangle_mesh_geometry();

		::framework::coroutine::shared_task<void> initialize(::framework::graphics::triangle_mesh_geometry_create_info const & create_info);

		::std::uint32_t const id;
		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::acceleration_structure_geometry_set * bottom_acceleration_structure_geometry_set;
		::framework::gpu::device_address bottom_level_acceleration_structure_device_address;
		::framework::gpu::buffer * bottom_level_acceleration_structure_buffer;
		::framework::gpu::acceleration_structure * bottom_level_acceleration_structure;
		::framework::gpu::device_size indices_memory_offset;
		::framework::gpu::device_size vertices_memory_offset;
		::std::vector<::framework::graphics::submesh> submeshes;
		::framework::coroutine::shared_task<void> initialization_shared_task;
	};
}

#include "graphics/ray_tracing/triangle_mesh_geometry.inl"