#pragma once

#include "coroutine/mutex.hpp"
#include "coroutine/shared_task.hpp"
#include "coroutine/task.hpp"
#include "graphics/medium.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "gpu/core.hpp"
#include <glm/glm.hpp>

namespace framework::graphics::ray_tracing
{
	class medium_system;

	enum class medium_type
	{
		standard_medium = 0,
	};

	struct medium_header
	{
		::glm::uvec1 data;

		enum medium_flags
		{
			medium_type_bits = 16,

			medium_type_offset = 0,
		};
	};

	struct medium_payload
	{
		::std::uint32_t data[15];
	};

	struct medium_data_blob
	{
		::framework::graphics::ray_tracing::medium_header header;
		::framework::graphics::ray_tracing::medium_payload payload;
	};

	class medium : public ::framework::graphics::medium
	{
	public:
		struct device_blob
		{
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob sigma_a_spec;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob sigma_s_spec;
			struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob le_spec;
		};

		explicit medium(::framework::graphics::ray_tracing::medium_system * medium_system, ::std::uint32_t id);

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept override;

		::std::uint32_t get_id() const noexcept;

		::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::medium_data_blob> get_device_data_and_commit();

	private:
		::framework::coroutine::shared_task<void> initialize();

		::framework::graphics::ray_tracing::medium_system * medium_system;
		::std::uint32_t id;
		::framework::graphics::ray_tracing::medium_header header;
		struct ::framework::graphics::ray_tracing::medium::device_blob device_blob;
		framework::graphics::ray_tracing::densely_sampled_spectrum * sigma_a_spec;
		framework::graphics::ray_tracing::densely_sampled_spectrum * sigma_s_spec;
		framework::graphics::ray_tracing::densely_sampled_spectrum * le_spec;
		::framework::coroutine::shared_task<void> initialization_shared_task;
		::framework::coroutine::mutex device_data_mutex;
	};
}

#include "graphics/ray_tracing/medium.inl"