inline ::framework::graphics::ray_tracing::scene_renderer * (::framework::graphics::ray_tracing::medium_system::get_scene_renderer)() const noexcept
{
	return this->scene_renderer;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::medium_system::get_update_semaphore)() const noexcept
{
	return this->update_semaphore;
}
