#pragma once

#include "gpu/core.hpp"
#include "graphics/scene_geometry.hpp"

namespace framework::graphics::ray_tracing
{
	class scene;

	class scene_geometry : virtual public ::framework::graphics::scene_geometry
	{
	};
}