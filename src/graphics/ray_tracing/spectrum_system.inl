inline ::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::spectrum::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}

inline ::framework::gpu::semaphore * framework::graphics::ray_tracing::spectrum_system::get_update_semaphore() const noexcept
{
	return this->update_semaphore;
}

inline ::framework::graphics::ray_tracing::scene_renderer * ::framework::graphics::ray_tracing::spectrum_system::get_scene_renderer() const
{
	return this->scene_renderer;
}

inline ::framework::coroutine::shared_task<void> (::framework::graphics::ray_tracing::rgb_to_spectrum_table::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}