#pragma once

#include "concurrency/thread_pool.h"
#include "RadixSort.h"

namespace framework
{
	class camera;
	class CTransform;

	struct PathTracingATRBVHCreateInfo
	{
		camera * camera;
		CTransform * cameraTransform;
		::std::uint32_t samples;
		::std::uint32_t bounces;
		::std::uint32_t maxPrimitives;
		::std::uint32_t treeletSize;
		::std::uint32_t iterations;
	};

	class CPathTracingATRBVH
	{
	public:
		~CPathTracingATRBVH();

		static void allocate(PathTracingATRBVHCreateInfo const * create_info, CPathTracingATRBVH ** ppCPathTracingATRBVH);

		void configure(class ::framework::gpu::device * device);

		void update(::std::uint32_t signal_semaphore_count, class ::framework::gpu::semaphore * const * signal_semaphores);

		void performTriangleListFillPass();

		void buildBVH();

		void trace(::std::uint32_t signal_semaphore_count, class ::framework::gpu::semaphore * const * signal_semaphores);

		//void AssignMeshRenderer(::std::shared_ptr<CEntity> &entity);
		
		class ::framework::gpu::sampler * GetGetRayTracedSampler() const;

		class ::framework::gpu::image * GetRayTracedImage() const;

		class ::framework::gpu::image_view * GetRayTracedImageView() const;

	private:
		struct Bound
		{
			::glm::vec3 min;
			::glm::float32 pad0;
			::glm::vec3 max;
			::glm::float32 pad1;
		};

		struct CompactTreeNode
		{
			::glm::uvec4 field0; // parent, left, right, triangleID
			::glm::vec4 field1; // boundMin.xyz, cost
			::glm::vec4 field2; // boundMax.xyz, area
		};

		//struct TreeNode
		//{
		//	::INT triangleID;
		//	::INT leftNode;
		//	::INT rightNode;
		//	::INT parentNode;
		//	Bound bound;
		//	FLOAT cost;
		//	FLOAT area;
		//};

		struct CompactTriangle
		{
			::glm::vec4	field0; // vert0.xyz, t0.x
			::glm::vec4	field1; // vert1.xyz, t1.y
			::glm::vec4	field2; // vert2.xyz, materialID
			::glm::vec4	field3; // t1.xy, t2.xy
			::glm::mat3x4	tbn;
		};

		struct CameraBufferData
		{
			::glm::mat4	viewMatrix;
			::glm::mat4	projMatrix;
			::glm::mat4	viewProjMatrix;
			::glm::mat4	invViewProjMatrix;
			::glm::vec4	position;
			::glm::vec4	direction;
			float		nearClipDistance;
			float		farClipDistance;
			float		nearFarClipDistance;
		};

		struct TransformBufferData
		{
			::glm::mat4	mObject2World;
			::glm::mat4	mWorld2Object;
			::glm::mat4	mModelView;
			::glm::mat4	mModelViewProjection;
			::glm::mat4	mTransposeModelView;
			::glm::mat4	mInverseTransposeModelView;
		};

		struct VariousBufferData
		{
			::glm::vec4	vTime;
			::glm::vec4	vSinTime;
			::glm::vec4	vCosTime;
			::glm::vec4	vDeltaTime;
			::glm::vec4	vScreenResolution;
		};

		struct thread_data
		{
			struct CommandBufferData
			{
				bool			is_command_buffer_open;
				::std::uint32_t		drawCallCount;
			};

			class ::framework::gpu::command_pool*					command_pool;
			::std::vector<class ::framework::gpu::command_buffer*>		commandBuffers;
			::std::vector<CommandBufferData>	commandBufferData;
		};
		CPathTracingATRBVH(PathTracingATRBVHCreateInfo const * create_info);

		void printBVH(::std::uint32_t root, CompactTreeNode * nodes);

		bool check_bound(CompactTreeNode * p, CompactTreeNode * l, CompactTreeNode * r);

		bool check_sanity(::std::uint32_t n, CompactTreeNode * nodes);

		class ::framework::gpu::device*						device;
		class ::framework::gpu::shader_module const *			m_pTriangleListFillVertexShaderModule;
		class ::framework::gpu::shader_module const *			m_pTriangleListFillGeometryShaderModule;
		class ::framework::gpu::shader_module const *			m_pGlobalBoundComputingShader;
		class ::framework::gpu::shader_module const *			m_pReductionGlobalBoundComputingShader;
		class ::framework::gpu::shader_module const *			m_pGenerateMortonCodesShader;
		class ::framework::gpu::shader_module const *			m_pConstructRadixTreeShader;
		class ::framework::gpu::shader_module const *			m_pCalculateNodeBoundingBoxesShader;
		class ::framework::gpu::shader_module const *			m_pAgglomerativeTreeletOptimizerShader;
		class ::framework::gpu::shader_module const *			m_pAgglomerativeSmallTreeletOptimizerShader;
		class ::framework::gpu::shader_module const *			m_pCreateNodesShader;
		class ::framework::gpu::shader_module const *			m_pCreateWoopifyTrianglesShader;
		class ::framework::gpu::shader_module const *			m_pRayGenShader;
		class ::framework::gpu::shader_module const *			m_pRayTracingShader;
		class ::framework::gpu::shader_module const *			m_pPrimaryRayHitsProcessingShader;
		class ::framework::gpu::shader_module const *			m_pSecondaryRayHitsProcessingShader;
		class ::framework::gpu::shader_module const *			m_pRaySamplingShader;
		class ::framework::gpu::shader_module const *			m_pPathTracingShader;
		class ::framework::gpu::sampler*					m_pRaySampleSampler;
		class ::framework::gpu::image*						m_pRaySampleImage;
		class ::framework::gpu::device_memory*				m_pRaySampleDeviceMemory;
		class ::framework::gpu::image_view*					m_pRaySampleImageView;
		class ::framework::gpu::buffer*						m_pRadixTreeBuffer;
		class ::framework::gpu::device_memory*				m_pRadixTreeDeviceMemory;
		class ::framework::gpu::buffer*						m_pRadixTreeReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pRadixTreeReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pScheduleBuffer;
		class ::framework::gpu::device_memory*				m_pScheduleDeviceMemory;
		class ::framework::gpu::buffer*						m_pDistanceMatrixBuffer;
		class ::framework::gpu::device_memory*				m_pDistanceMatrixDeviceMemory;
		class ::framework::gpu::buffer*						m_pSubtreeTrianglesCountBuffer;
		class ::framework::gpu::device_memory*				m_pSubtreeTrianglesCountDeviceMemory;
		class ::framework::gpu::buffer*						m_pCounterBuffer;
		class ::framework::gpu::device_memory*				m_pCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pBoundsBuffer;
		class ::framework::gpu::device_memory*				m_pBoundsDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitivesCounterBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitivesCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pPrimitiveIndicesBuffer;
		class ::framework::gpu::device_memory*				m_pPrimitiveIndicesDeviceMemory;
		class ::framework::gpu::buffer*						m_pMortonCodesBuffer;
		class ::framework::gpu::device_memory*				m_pMortonCodesDeviceMemory;
		class ::framework::gpu::buffer*						m_pNodesBuffer;
		class ::framework::gpu::device_memory*				m_pNodesDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pNodesBufferView;
		class ::framework::gpu::buffer*						m_pTriWoopBuffer;
		class ::framework::gpu::device_memory*				m_pTriWoopDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pTriWoopBufferView;
		class ::framework::gpu::buffer*						m_pTriIndexBuffer;
		class ::framework::gpu::device_memory*				m_pTriIndexDeviceMemory;
		class ::framework::gpu::buffer_view*					m_pTriIndexBufferView;
		class ::framework::gpu::buffer*						m_pNodesUploadBuffer;
		class ::framework::gpu::device_memory*				m_pNodesUploadDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriWoopUploadBuffer;
		class ::framework::gpu::device_memory*				m_pTriWoopUploadDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriIndexUploadBuffer;
		class ::framework::gpu::device_memory*				m_pTriIndexUploadDeviceMemory;
		//StructuredBuffer			m_primitiveBuffer;
		//StructuredBuffer			m_rays;
		//StructuredBuffer			m_hits;
		//TypedBuffer rayOrigin(DXGI_FORMAT_R32G32B32A32_FLOAT);
		//TypedBuffer rayDirection(DXGI_FORMAT_R32G32B32A32_FLOAT);
		//TypedBuffer hitIndex(DXGI_FORMAT_R32_SINT);
		//TypedBuffer hitDistance(DXGI_FORMAT_R32_FLOAT);
		//TypedBuffer hitBarycentricUV(DXGI_FORMAT_R32G32_FLOAT);
		//TypedBuffer					m_materialIndices;
		//TypedBuffer					m_nodes;
		//TypedBuffer					m_triWoop;
		//TypedBuffer					m_triIndex;
		//TypedBuffer					m_colorMask;
		//TypedBuffer					m_accumulatedColor;
		class ::framework::gpu::buffer*						m_pReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pUnsortedDataReadback;
		class ::framework::gpu::device_memory*				m_pUnsortedDataReadbackMemory;
		class ::framework::gpu::buffer*						m_pBoundsReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pBoundsReadbackDeviceMemory;
		class ::framework::gpu::buffer*						m_pTriangleListFillUboBuffer;
		class ::framework::gpu::device_memory*				m_pTriangleListFillUboDeviceMemory;
		void*						m_pTriangleListFillUboMappedData;
		class ::framework::gpu::buffer*						m_pCameraUboBuffer;
		class ::framework::gpu::device_memory*				m_pCameraUboDeviceMemory;
		class ::framework::gpu::buffer*						m_pVariousUboBuffer;
		class ::framework::gpu::device_memory*				m_pVariousUboDeviceMemory;
		class ::framework::gpu::buffer*						m_pDebugCounterBuffer;
		class ::framework::gpu::device_memory*				m_pDebugCounterDeviceMemory;
		class ::framework::gpu::buffer*						m_pDebugBuffer;
		class ::framework::gpu::device_memory*				m_pDebugDeviceMemory;
		class ::framework::gpu::buffer*						m_pDebugReadbackBuffer;
		class ::framework::gpu::device_memory*				m_pDebugReadbackDeviceMemory;
		class ::framework::gpu::command_pool*				this->primary_command_pool;
		class ::framework::gpu::command_buffer*				m_pPrimaryCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pPrimitiveCounterServicingCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pGlobalBoundComputingCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pGenerateMortonCodesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pConstructRadixTreeCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCalculateNodeBoundingBoxesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pAgglomerativeTreeletOptimizerCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCreateNodesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pCreateWoopifyTrianglesCommandBuffer;
		class ::framework::gpu::command_buffer*				m_pPathTracingCommandBuffer;
		class ::framework::gpu::semaphore*					m_pTriangleListFillCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pGlobalBoundComputingCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pGenerateMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pSortMortonCodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pConstructRadixTreeCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCalculateNodeBoundingBoxesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pAgglomerativeTreeletOptimizerCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCreateNodesCompleteSemaphore;
		class ::framework::gpu::semaphore*					m_pCreateWoopifyTrianglesCompleteSemaphore;
		class ::framework::gpu::frame_buffer*					m_pFramebuffer;
		class ::framework::gpu::render_pass*					render_pass;
		class ::framework::gpu::pipeline_layout*				m_pTriangleListFillPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pReductionGlobalBoundComputingPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pGenerateMortonCodesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pConstructRadixTreePipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCalculateNodeBoundingBoxesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pAgglomerativeTreeletOptimizerPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pAgglomerativeSmallTreeletOptimizerPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCreateNodesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pCreateWoopifyTrianglesPipelineLayout;
		class ::framework::gpu::pipeline_layout*				m_pPathTracingPipelineLayout;
		class ::framework::gpu::pipeline *					m_pTriangleListFillPipeline;
		class ::framework::gpu::pipeline *					m_pGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline *					m_pReductionGlobalBoundComputingPipeline;
		class ::framework::gpu::pipeline *					m_pGenerateMortonCodesPipeline;
		class ::framework::gpu::pipeline *					m_pConstructRadixTreePipeline;
		class ::framework::gpu::pipeline *					m_pCalculateNodeBoundingBoxesPipeline;
		class ::framework::gpu::pipeline *					m_pAgglomerativeTreeletOptimizerPipeline;
		class ::framework::gpu::pipeline *					m_pAgglomerativeSmallTreeletOptimizerPipeline;
		class ::framework::gpu::pipeline *					m_pCreateNodesPipeline;
		class ::framework::gpu::pipeline *					m_pCreateWoopifyTrianglesPipeline;
		class ::framework::gpu::pipeline *					m_pPathTracingPipeline;
		class ::framework::gpu::descriptor_pool*				descriptor_pool;
		class ::framework::gpu::descriptor_set_layout*		m_pTriangleListFillDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pReductionGlobalBoundComputingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pGenerateMortonCodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pConstructRadixTreeDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCalculateNodeBoundingBoxesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pAgglomerativeTreeletOptimizerDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pAgglomerativeSmallTreeletOptimizerDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCreateNodesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pCreateWoopifyTrianglesDescriptorSetLayout;
		class ::framework::gpu::descriptor_set_layout*		m_pPathTracingDescriptorSetLayout;
		class ::framework::gpu::descriptor_set*				m_pTriangleListFillDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pReductionGlobalBoundComputingDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pGenerateMortonCodesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pConstructRadixTreeDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCalculateNodeBoundingBoxesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pAgglomerativeTreeletOptimizerDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pAgglomerativeSmallTreeletOptimizerDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCreateNodesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pCreateWoopifyTrianglesDescriptorSet;
		class ::framework::gpu::descriptor_set*				m_pPathTracingDescriptorSet;
		class DescriptorTable*			m_pTriangleListFillDescriptorTable;
		class DescriptorTable*			m_pGlobalBoundComputingDescriptorTable;
		class DescriptorTable*			m_pReductionGlobalBoundComputingDescriptorTable;
		class DescriptorTable*			m_pGenerateMortonCodesDescriptorTable;
		class DescriptorTable*			m_pConstructRadixTreeDescriptorTable;
		class DescriptorTable*			m_pCalculateNodeBoundingBoxesDescriptorTable;
		class DescriptorTable*			m_pAgglomerativeTreeletOptimizerDescriptorTable;
		class DescriptorTable*			m_pAgglomerativeSmallTreeletOptimizerDescriptorTable;
		class DescriptorTable*			m_pCreateNodesDescriptorTable;
		class DescriptorTable*			m_pCreateWoopifyTrianglesDescriptorTable;
		class DescriptorTable*			m_pPathTracingDescriptorTable;
		lsd_radix_sort::Data*			m_pRadixSortData;
		::std::uint32_t					thread_count;
		::std::vector<thread_data>		thread_data;
		job_system					thread_pool;
		class ::framework::gpu::fence*						m_pBuildBVHFence;
		class ::framework::gpu::fence*						m_pPrimitiveCounterServicingCompleteFence;
		const ::std::uint64_t				m_maxUBOs;
		const ::std::uint64_t				m_maxPrimitives;
		camera *					m_camera;
		CTransform *				m_cameraTransform;
		::std::uint32_t					m_treeletSize;
		::std::uint32_t					m_iterations;
		::std::uint32_t					m_scheduleSize;
		::std::uint32_t					m_samples;
		::std::uint32_t					m_bounces;
		::std::uint32_t					m_numTriangles;
	};
}