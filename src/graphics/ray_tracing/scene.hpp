#pragma once

#include "command_context.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "graphics/scene.hpp"
#include "graphics/scene_geometry.hpp"
#include "graphics/ray_tracing/light.hpp"
#include "graphics/ray_tracing/light_collection.hpp"
#include "graphics/ray_tracing/scene_view_perspective.hpp"
#include "gpu/core.hpp"
#include <vector>
#include <memory>
#include <mutex>

namespace framework::graphics::ray_tracing
{
	class light;
	class scene_renderer;
	class triangle_mesh_scene_geometry;

	class scene : public ::framework::graphics::scene
	{
	public:
		struct uniform_buffer
		{
			typename ::std::uint32_t light_count;
			typename ::std::uint32_t infinite_light_count;
			typename ::glm::uvec2 padding;
			struct ::framework::graphics::ray_tracing::light_collection::uniform_data light_collection;
			struct ::framework::graphics::ray_tracing::scene_view_perspective::device_data camera;
		};

		struct instance_shader_binding_table_record
		{
			::std::uint32_t material_index;
			::std::uint32_t index_offset;
			::std::uint32_t vertex_offset;
		};

		scene(::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::scene_create_info * create_info);

		~scene();

		::framework::graphics::triangle_mesh_scene_geometry * create_triangle_mesh_scene_geometry(::framework::graphics::triangle_mesh_scene_geometry_create_info const & create_info) override;

		::framework::graphics::point_light * create_point_light() override;

		::framework::graphics::image_infinite_light * create_image_infinite_light() override;

		::framework::coroutine::immediate_task<void> update();

		void bind_descriptor_set();

		::std::uint32_t allocate_geometry_instance_id(::std::uint32_t geometry_count) noexcept;

		::std::uint32_t allocate_instance() noexcept;

		::framework::graphics::ray_tracing::scene_renderer * get_scene_renderer() const noexcept;

		::framework::gpu::semaphore * get_initialize_geometries_semaphore() const noexcept;

		::framework::gpu::semaphore * get_update_transforms_semaphore() const noexcept;

		::framework::gpu::semaphore * get_update_lights_semaphore() const noexcept;

		::framework::gpu::semaphore * get_update_semaphore() const noexcept;

		::framework::gpu::acceleration_structure const * get_top_level_acceleration_structure() const { return this->top_level_acceleration_structure; }

		::framework::gpu::buffer * get_shader_binding_table_buffer() const noexcept;

		::std::uint64_t get_miss_shader_binding_table_device_address() const { return this->miss_shader_binding_table_device_address; }

		::std::uint64_t get_closest_hit_shader_binding_table_device_address() const noexcept;

		::framework::gpu::device_size get_closest_hit_shader_group_stride() const { return this->closest_hit_shader_group_stride; }

		::framework::gpu::device_size get_closest_hit_shader_binding_table_size() const { return this->closest_hit_shader_binding_table_size; }

		::framework::gpu::descriptor_set * get_descriptor_set() const { return this->descriptor_set; }

		::std::uint32_t get_hit_group_count() const { return this->hit_group_count; }

		struct ::framework::graphics::ray_tracing::scene::uniform_buffer * get_current_frame_uniform_buffer_mapped_data();

		::framework::gpu::device_size get_closest_hit_shader_group_offset(::std::uint32_t geometry_instance_index) const noexcept;

		::std::uint32_t get_uniform_buffer_alignment() const { return this->uniform_buffer_alignment; }

		::framework::gpu::buffer * get_instances_buffer() const { return this->instances_buffer; }

		::framework::gpu::buffer * get_geometry_instances_buffer() const { return this->geometry_instances_buffer; }

		::framework::graphics::ray_tracing::light_collection * get_light_collection() const noexcept;

		::framework::coroutine::immediate_task<void> add_triangle_mesh_scene_geometry_to_initialize(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry);
		
		::framework::coroutine::immediate_task<void> add_triangle_mesh_scene_geometry_to_update_transform(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry * triangle_mesh_scene_geometry);

		::framework::coroutine::immediate_task<void> add_light(::framework::graphics::ray_tracing::image_infinite_light * image_infinite_light);

	private:
		::framework::coroutine::immediate_task<void> update_transforms();

		::framework::coroutine::immediate_task<void> initialize_geometries();

		::framework::coroutine::immediate_task<void> update_lights();

		template<typename T>
		struct container
		{
			::std::vector<T> container;
			::framework::coroutine::mutex mutex;
		};

		::framework::graphics::ray_tracing::scene_renderer * const scene_renderer;
		::std::uint32_t const max_primitive_count = 1024 * 8;
		::std::uint32_t const hit_group_count = 2;
		::std::atomic_uint32_t geometry_instance_id_counter;
		::std::atomic_uint32_t instance_counter;
		::framework::gpu::buffer * lights_buffer;
		::framework::gpu::device_memory * lights_device_memory;
		::framework::gpu::buffer * infinite_lights_buffer;
		::framework::gpu::device_memory * infinite_lights_device_memory;
		::framework::gpu::buffer * uniform_buffer;
		::framework::gpu::device_memory * uniform_buffer_device_memory;
		::framework::gpu::buffer * instances_buffer;
		::framework::gpu::device_memory * instances_device_memory;
		::framework::gpu::device_or_host_address instances_device_or_host_address;
		::framework::gpu::buffer * geometry_instances_buffer;
		::framework::gpu::device_memory * geometry_instances_device_memory;
		::framework::gpu::buffer * world_matrices_buffer;
		::framework::gpu::device_memory * world_matrices_device_memory;
		::framework::gpu::buffer * inverse_transpose_world_matrices_buffer;
		::framework::gpu::device_memory * inverse_transpose_world_matrices_device_memory;
		::framework::gpu::acceleration_structure * top_level_acceleration_structure;
		::framework::gpu::buffer * top_level_acceleration_structure_buffer;
		::framework::gpu::device_memory * top_level_acceleration_structure_device_memory;
		::std::uint64_t top_level_acceleration_structure_device_address;
		::framework::gpu::buffer * top_level_acceleration_structure_scratch_buffer;
		::framework::gpu::device_memory * top_level_acceleration_structure_scratch_device_memory;
		::framework::gpu::device_or_host_address top_level_acceleration_structure_scratch_device_or_host_address;
		::framework::gpu::acceleration_structure_geometry_set * top_acceleration_structure_geometry_set;
		::framework::gpu::buffer * shader_binding_table_buffer;
		::framework::gpu::device_memory * shader_binding_table_device_memory;
		::framework::gpu::descriptor_set * descriptor_set;
		::std::uint64_t miss_shader_binding_table_device_address;
		::std::uint64_t closest_hit_shader_binding_table_device_address;
		::framework::gpu::device_size closest_hit_shader_group_stride;
		::framework::gpu::device_size closest_hit_shader_binding_table_size;
		::framework::gpu::device_size closest_hit_shader_binding_table_base_offset;
		::framework::gpu::device_size closest_hit_shader_binding_table_offset_to_shader_record;
		::framework::gpu::semaphore * initialize_geometries_semaphore;
		::framework::gpu::semaphore * update_transforms_semaphore;
		::framework::gpu::semaphore * update_lights_semaphore;
		::framework::gpu::semaphore * update_semaphore;
		::std::unique_ptr<::framework::graphics::ray_tracing::light_collection> light_collection;
		::std::vector<::framework::graphics::ray_tracing::light *> lights;
		::framework::graphics::ray_tracing::scene::container<::framework::graphics::ray_tracing::image_infinite_light *> infinite_lights;
		::framework::graphics::ray_tracing::scene::container<::framework::graphics::ray_tracing::triangle_mesh_scene_geometry *> triangle_mesh_scene_geometries_to_initialize;
		::framework::graphics::ray_tracing::scene::container<::framework::graphics::ray_tracing::triangle_mesh_scene_geometry *> triangle_mesh_scene_geometries_to_update_transform;
		::std::uint32_t uniform_buffer_alignment;
		::std::byte * uniform_buffer_mapped_data;
	};
}

#include "graphics/ray_tracing/scene.inl"