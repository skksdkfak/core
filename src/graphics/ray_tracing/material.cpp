#include "graphics/ray_tracing/image.hpp"
#include "graphics/ray_tracing/material.hpp"
#include "graphics/ray_tracing/material_system.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "physics/scattering.hpp"
#include <cstddef>
#include <cstring>

::framework::graphics::ray_tracing::material::material(::framework::graphics::ray_tracing::material_system * material_system, ::framework::graphics::material_create_info const & create_info, ::std::uint32_t id) :
	material_system(material_system),
	id(id),
	header(),
	device_data(),
	device_data_state(::framework::graphics::ray_tracing::material::device_data_state::perform_update)
{
	this->initialize_device();
}

void ::framework::graphics::ray_tracing::material::set_thin_surface(bool thin_surface)
{
	this->set_thin_surface_device(thin_surface);
}

void ::framework::graphics::ray_tracing::material::set_roughness(::glm::vec2 roughness)
{
	this->set_roughness_device(roughness);
}

::framework::graphics::image * ::framework::graphics::ray_tracing::material::get_diffuse_texture() const
{
	return nullptr;
}

void ::framework::graphics::ray_tracing::material::set_diffuse_texture(::framework::graphics::image * image)
{
	this->set_diffuse_image_device(image);
}

void ::framework::graphics::ray_tracing::material::set_emissive_image(::framework::graphics::image * emissive_image)
{
	this->set_emissive_color_device(emissive_image);
}

::framework::coroutine::immediate_task<struct ::framework::graphics::ray_tracing::material_data_blob>(::framework::graphics::ray_tracing::material::get_data_blob_and_commit)()
{
	::framework::graphics::ray_tracing::material_data_blob material_data_blob;
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	this->device_data_state |= ::framework::graphics::ray_tracing::material::device_data_state::perform_update;
	material_data_blob.header = this->header;
	static_assert(sizeof(this->device_data) <= sizeof(material_data_blob.payload));
	::std::memcpy(&material_data_blob.payload, &this->device_data, sizeof(this->device_data));
	co_return material_data_blob;
}

::framework::coroutine::self_destructible_coroutine (::framework::graphics::ray_tracing::material::initialize_device)() noexcept
{
	::framework::physics::constant_spectrum const ior_constant_spectrum(1.5f);
	float const ior_piecewise_linear_spectrum_samples[]
	{
		300.0f, 1.1f,
		400.0f, 1.3f,
		600.0f, 1.6f,
		800.0f, 1.8f
	};
	::framework::physics::piecewise_linear_spectrum const ior_piecewise_linear_spectrum(ior_piecewise_linear_spectrum_samples, ::std::size(ior_piecewise_linear_spectrum_samples), false);
	::framework::graphics::ray_tracing::spectrum * ior = this->material_system->get_scene_renderer()->get_spectrum_system()->create_constant_spectrum(ior_constant_spectrum, true);
	//::framework::graphics::ray_tracing::spectrum * ior = this->material_system->get_scene_renderer()->get_spectrum_system()->create_piecewise_linear_spectrum(ior_piecewise_linear_spectrum, true);
	co_await ior->get_initialization_shared_task();
	bool add_material_to_update;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->ior = ior;
		this->header.data.x |= (0u & ::framework::graphics::ray_tracing::material_header::material_flags::material_type_bits) << ::framework::graphics::ray_tracing::material_header::material_flags::material_type_offset;
		this->device_data.alpha = ::framework::physics::trowbridge_reitz_distribution::roughness_to_alpha(::glm::vec2(0.1f));
		this->device_data.ior = this->ior->get_pointer();
		this->device_data_state |= ::framework::graphics::ray_tracing::material::device_data_state::ior_set;
		add_material_to_update = this->add_to_update();
	}
	if (add_material_to_update)
	{
		co_await this->material_system->add_material_to_update(this);
	}
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::material::set_thin_surface_device)(bool thin_surface) noexcept
{
	bool add_material_to_update;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->header.data.x |= 1u << ::framework::graphics::ray_tracing::material_header::material_flags::thin_surface_flag_offset;
		add_material_to_update = this->add_to_update();
	}
	if (add_material_to_update)
	{
		co_await this->material_system->add_material_to_update(this);
	}
}

::framework::coroutine::self_destructible_coroutine (::framework::graphics::ray_tracing::material::set_roughness_device)(::glm::vec2 roughness) noexcept
{
	bool add_material_to_update;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->device_data.alpha = ::framework::physics::trowbridge_reitz_distribution::roughness_to_alpha(roughness);
		add_material_to_update = this->add_to_update();
	}
	if (add_material_to_update)
	{
		co_await this->material_system->add_material_to_update(this);
	}
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::material::set_diffuse_image_device)(::framework::graphics::image * image) noexcept
{
	co_await image->get_initialization_shared_task();
	bool add_material_to_update;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->diffuse_image = static_cast<::framework::graphics::ray_tracing::image *>(image);
		this->device_data.diffuse_image = this->diffuse_image->get_device_blob();
		this->device_data_state |= ::framework::graphics::ray_tracing::material::device_data_state::image_set;
		add_material_to_update = this->add_to_update();
	}
	if (add_material_to_update)
	{
		co_await this->material_system->add_material_to_update(this);
	}
}

::framework::coroutine::self_destructible_coroutine(::framework::graphics::ray_tracing::material::set_emissive_color_device)(::framework::graphics::image * emissive_image) noexcept
{
	if (emissive_image)
	{
		co_await emissive_image->get_initialization_shared_task();
	}
	bool add_material_to_update;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
		this->emissive_image = static_cast<::framework::graphics::ray_tracing::image *>(emissive_image);
		if (emissive_image)
		{
			this->device_data.emissive_image = this->emissive_image->get_device_blob();
			this->header.data.x |= 1u << ::framework::graphics::ray_tracing::material_header::material_flags::emissive_flag_offset;
		}
		else
		{
			this->header.data.x &= ~(1u << ::framework::graphics::ray_tracing::material_header::material_flags::emissive_flag_offset);
		}
		add_material_to_update = this->add_to_update();
	}
	if (add_material_to_update)
	{
		co_await this->material_system->add_material_to_update(this);
	}
}
