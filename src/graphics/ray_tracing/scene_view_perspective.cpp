#include "scene_view_perspective.hpp"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

::framework::graphics::ray_tracing::scene_view_perspective::scene_view_perspective(scene_renderer * scene_renderer, scene_view_perspective_create_info * create_info)
{
}

::framework::graphics::ray_tracing::scene_view_perspective::~scene_view_perspective()
{
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_eye(::glm::vec3 const & eye)
{
	this->eye = eye;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_center(::glm::vec3 const & center)
{
	this->center = center;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_up(::glm::vec3 const & up)
{
	this->up = up;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_fov(float vertical_fov_radians)
{
	m_verticalFOV = vertical_fov_radians;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_aspect_ratio(float aspect_height_over_width)
{
	m_aspectRatio = aspect_height_over_width;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_near_z(float near_z_clip)
{
	m_nearClip = near_z_clip;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::set_far_z(float far_z_clip)
{
	m_farClip = far_z_clip;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::reverse_z(bool enable)
{
	m_reverseZ = enable;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::inverse_y(bool enable)
{
	m_inverseY = enable;
}

void ::framework::graphics::ray_tracing::scene_view_perspective::update()
{
	float Y = 1.0f / ::std::tan(m_verticalFOV * 0.5f);
	float X = Y * m_aspectRatio;

	float Q1, Q2;

	// ReverseZ puts far plane at Z=0 and near plane at Z=1.  This is never a bad idea, and it's
	// actually a great idea with F32 depth buffers to redistribute precision more evenly across
	// the entire range.  It requires clearing Z to 0.0f and using a GREATER variant depth test.
	// Some care must also be done to properly reconstruct linear W in a pixel shader from hyperbolic Z.
	if (m_reverseZ)
	{
		Q1 = m_nearClip / (m_farClip - m_nearClip);
		Q2 = Q1 * m_farClip;
	}
	else
	{
		Q1 = m_farClip / (m_nearClip - m_farClip);
		Q2 = Q1 * m_nearClip;
	}

	this->device_data.projection = ::glm::mat4(
		X, 0.0f, 0.0f, 0.0f,
		0.0f, m_inverseY ? -Y : Y, 0.0f, 0.0f,
		0.0f, 0.0f, Q1, -1.0f,
		0.0f, 0.0f, Q2, 0.0f
	);
	this->device_data.view = ::glm::lookAt(this->eye, this->center, this->up);
	this->device_data.view_projection = this->device_data.projection * this->device_data.view;
	this->device_data.view_inverse = ::glm::inverse(this->device_data.view);
	this->device_data.projection_inverse = ::glm::inverse(this->device_data.projection);
}