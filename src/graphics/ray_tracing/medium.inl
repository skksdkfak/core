inline ::framework::coroutine::shared_task<void> (::framework::graphics::ray_tracing::medium::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::medium::get_id)() const noexcept
{
	return this->id;
}