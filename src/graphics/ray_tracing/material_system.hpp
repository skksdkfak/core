#pragma once

#include "command_context.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "graphics/ray_tracing/material.hpp"
#include <glm/glm.hpp>
#include <cstdint>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class material_system
	{
	public:
		static constexpr ::std::uint32_t textures_binding = 3;

		material_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer);

		~material_system();

		::framework::graphics::material * create_material(::framework::graphics::material_create_info const & create_info);

		::framework::coroutine::immediate_task<void> add_material_to_update(::framework::graphics::ray_tracing::material * material);

		::framework::coroutine::immediate_task<void> update();

		::framework::graphics::ray_tracing::scene_renderer * get_scene_renderer() const noexcept;

		::framework::gpu::descriptor_set * get_descriptor_set() const noexcept;

		::framework::gpu::semaphore * get_update_semaphore() const noexcept;

	private:
		friend class ::framework::graphics::ray_tracing::scene_renderer;

		::framework::graphics::ray_tracing::scene_renderer * scene_renderer;
		::framework::gpu::descriptor_set_layout * material_system_descriptor_set_layout;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::buffer * material_data_buffer;
		::framework::gpu::device_memory * material_data_device_memory;
		::framework::gpu::buffer * bssrdf_table_buffer;
		::framework::gpu::device_memory * bssrdf_table_device_memory;
		::framework::gpu::sampler * sampler;
		::framework::gpu::semaphore * update_semaphore;
		::std::vector<::framework::graphics::ray_tracing::material *> materials_to_update;
		::framework::coroutine::mutex materials_to_update_mutex;
		::std::uint32_t const max_texture_count;
		::std::uint32_t material_counter;
	};
}

#include "graphics/ray_tracing/material_system.inl"