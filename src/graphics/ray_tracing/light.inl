inline enum ::framework::graphics::ray_tracing::light::changes (::framework::graphics::ray_tracing::light::get_changes)() const noexcept
{
	return this->changes;
}