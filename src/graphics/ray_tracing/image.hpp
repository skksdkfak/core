#pragma once

#include "coroutine/shared_task.hpp"
#include "gpu/core.hpp"
#include "graphics/image.hpp"

namespace framework::graphics::ray_tracing
{
	class scene_renderer;

	class image : public ::framework::graphics::image
	{
	public:
		struct device_blob
		{
			::std::uint32_t id;
		};

		explicit image(::std::uint32_t id, ::framework::graphics::ray_tracing::scene_renderer * scene_renderer, ::framework::graphics::image_create_info const & create_info);

		~image();

		::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept override;

		::std::uint32_t get_id() const noexcept;

		::framework::graphics::ray_tracing::image::device_blob get_device_blob() const noexcept;

	private:
		::framework::coroutine::shared_task<void> initialize(::framework::graphics::image_create_info const & create_info);

		::std::uint32_t const id;
		::framework::graphics::ray_tracing::scene_renderer * const scene_renderer;
		::framework::gpu::image * device_image;
		::framework::gpu::device_memory * device_memory;
		::framework::gpu::image_view * image_view;
		::framework::coroutine::shared_task<void> initialization_shared_task;
	};
}

#include "graphics/ray_tracing/image.inl"