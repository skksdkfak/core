inline ::std::uint32_t(::framework::graphics::ray_tracing::triangle_mesh_geometry::get_geometry_count)() const noexcept
{
	return static_cast<::std::uint32_t>(this->submeshes.size());
}

inline ::framework::coroutine::shared_task<void> (::framework::graphics::ray_tracing::triangle_mesh_geometry::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}

inline ::framework::gpu::acceleration_structure * ::framework::graphics::ray_tracing::triangle_mesh_geometry::get_bottom_level_acceleration_structure() const noexcept
{
	return this->bottom_level_acceleration_structure;
}

inline ::framework::gpu::device_address (::framework::graphics::ray_tracing::triangle_mesh_geometry::get_bottom_level_acceleration_structure_device_address)() const noexcept
{
	return this->bottom_level_acceleration_structure_device_address;
}

inline ::framework::graphics::submesh const * ::framework::graphics::ray_tracing::triangle_mesh_geometry::get_submeshes() const noexcept
{
	return this->submeshes.data();
}

inline ::framework::gpu::device_size (::framework::graphics::ray_tracing::triangle_mesh_geometry::get_indices_memory_offset)() const noexcept
{
	return this->indices_memory_offset;
}

inline ::framework::gpu::device_size (::framework::graphics::ray_tracing::triangle_mesh_geometry::get_vertices_memory_offset)() const noexcept
{
	return this->vertices_memory_offset;
}
