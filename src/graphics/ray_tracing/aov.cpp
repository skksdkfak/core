#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "graphics/ray_tracing/aov.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "gpu/utility.hpp"
#include <utility>

::framework::graphics::ray_tracing::aov::aov(::framework::graphics::ray_tracing::scene_renderer * scene_renderer) :
	scene_renderer(scene_renderer)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[1];
		// geometry_instance_id
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 6;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::all;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	this->create_screen_buffers();
}

::framework::graphics::ray_tracing::aov::~aov()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	device->destroy_descriptor_set_layout(this->descriptor_set_layout, nullptr);
	device->destroy_image_view(this->geometry_instance_id_image_view, nullptr);
	device->destroy_image(this->geometry_instance_id_image, nullptr);
	device->free_memory(this->geometry_instance_id_device_memory, nullptr);
}

::framework::gpu::image * (::framework::graphics::ray_tracing::aov::get_image)(::framework::graphics::aov::type type) const noexcept
{
	switch (type)
	{
	case framework::graphics::aov::type::geometry_instance_id:
		return this->geometry_instance_id_image;
	default:
		::std::unreachable();
	}
}

void ::framework::graphics::ray_tracing::aov::resize()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	device->destroy_image_view(this->geometry_instance_id_image_view, nullptr);
	device->destroy_image(this->geometry_instance_id_image, nullptr);
	device->free_memory(this->geometry_instance_id_device_memory, nullptr);

	this->create_screen_buffers();
}

char const * (::framework::graphics::ray_tracing::aov::get_aov_preprocessor_define)(::framework::graphics::ray_tracing::aov::type type)
{
	switch (type)
	{
	case ::framework::graphics::ray_tracing::aov::type::geometry_instance_id:
		return "framework_aov_geometry_instance_id";
	default:
		::std::unreachable();
	}
}

void ::framework::graphics::ray_tracing::aov::create_screen_buffers()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	{
		::framework::gpu::clear_value const clear_value = { .color = { { 0ul, 0ul, 0ul, 0ul } } };

		::framework::gpu::image_create_info image_create_info;
		image_create_info.flags = ::framework::gpu::image_create_flags::none;
		image_create_info.image_type = ::framework::gpu::image_type::two_dimensional;
		image_create_info.format = ::framework::gpu::format::r32_uint;
		image_create_info.extent.width = this->scene_renderer->get_width();
		image_create_info.extent.height = this->scene_renderer->get_height();
		image_create_info.extent.depth = 1;
		image_create_info.mip_levels = 1;
		image_create_info.array_layers = 1;
		image_create_info.samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
		image_create_info.tiling = ::framework::gpu::image_tiling::optimal;
		image_create_info.usage = ::framework::gpu::image_usage_flags::storage_bit | ::framework::gpu::image_usage_flags::transfer_src_bit | ::framework::gpu::image_usage_flags::allow_clear_bit;
		image_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		image_create_info.queue_family_index_count = 0;
		image_create_info.queue_family_indices = nullptr;
		image_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		image_create_info.initial_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::general_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		image_create_info.optimized_clear_value = &clear_value;
		assert_framework_gpu_result(device->create_image(&image_create_info, nullptr, &this->geometry_instance_id_image));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::image_memory_requirements_info image_memory_requirements_info;
		image_memory_requirements_info.image = this->geometry_instance_id_image;
		device->get_image_memory_requirements(&image_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->geometry_instance_id_device_memory));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::image;
		debug_utils_object_name_info.object = this->geometry_instance_id_image;
		debug_utils_object_name_info.object_name = "last_frame_sum_image";
		assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::bind_image_memory_info bind_image_memory_info;
		bind_image_memory_info.image = this->geometry_instance_id_image;
		bind_image_memory_info.memory = this->geometry_instance_id_device_memory;
		bind_image_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_image_memory(1, &bind_image_memory_info));

		::framework::gpu::image_view_create_info image_view_create_info;
		image_view_create_info.flags = ::framework::gpu::image_view_create_flags::none;
		image_view_create_info.image = this->geometry_instance_id_image;
		image_view_create_info.view_type = ::framework::gpu::image_view_type::two_dimensional;
		image_view_create_info.format = ::framework::gpu::format::r32_uint;
		image_view_create_info.components.r = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.g = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.b = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.components.a = ::framework::gpu::component_swizzle::identity;
		image_view_create_info.subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		image_view_create_info.subresource_range.base_mip_level = 0;
		image_view_create_info.subresource_range.level_count = 1;
		image_view_create_info.subresource_range.base_array_layer = 0;
		image_view_create_info.subresource_range.layer_count = 1;
		assert_framework_gpu_result(device->create_image_view(&image_view_create_info, nullptr, &this->geometry_instance_id_image_view));
	}

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &this->descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set));

	::framework::gpu::descriptor_image_info descriptor_image_infos[1];
	descriptor_image_infos[0].sampler = nullptr;
	descriptor_image_infos[0].image_view = this->geometry_instance_id_image_view;
	descriptor_image_infos[0].image_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;

	::framework::gpu::write_descriptor_set write_descriptor_sets[1];
	write_descriptor_sets[0].dst_set = this->descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_image;
	write_descriptor_sets[0].image_info = &descriptor_image_infos[0];
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	if (!this->scene_renderer->get_gpu_context()->get_physical_device_features().any_image_initial_layout)
	{
		::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
		{
			::framework::command_context * const graphics_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->acquire_command_context();

			::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
			command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
			command_buffer_begin_info.inheritance_info = nullptr;
			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

			::framework::gpu::image_subresource_range subresource_range;
			subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
			subresource_range.base_mip_level = 0;
			subresource_range.level_count = 1;
			subresource_range.base_array_layer = 0;
			subresource_range.layer_count = 1;

			::framework::gpu::image_memory_barrier image_memory_barriers[1];
			image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::none;
			image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::none;
			image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
			image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::shader_read_write_optimal_bit;
			image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
			image_memory_barriers[0].image = this->geometry_instance_id_image;
			image_memory_barriers[0].subresource_range = subresource_range;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(image_memory_barriers));
			dependency_info.image_memory_barriers = image_memory_barriers;
			graphics_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);

			assert_framework_gpu_result(graphics_command_context->get_command_buffer()->end_command_buffer());
			co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
			co_await this->scene_renderer->get_command_buffer_manager().get_graphics_queue()->release_command_context(graphics_command_context);
		}());
	}
}