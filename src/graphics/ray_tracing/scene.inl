inline ::std::uint32_t(::framework::graphics::ray_tracing::scene::allocate_geometry_instance_id)(::std::uint32_t geometry_count) noexcept
{
	return this->geometry_instance_id_counter.fetch_add(geometry_count, ::std::memory_order::relaxed);
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::scene::allocate_instance)() noexcept
{
	return this->instance_counter.fetch_add(1, ::std::memory_order::relaxed);
}

inline ::framework::graphics::ray_tracing::scene_renderer * (::framework::graphics::ray_tracing::scene::get_scene_renderer)() const noexcept
{
	return this->scene_renderer;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::scene::get_initialize_geometries_semaphore)() const noexcept
{
	return this->initialize_geometries_semaphore;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::scene::get_update_transforms_semaphore)() const noexcept
{
	return this->update_transforms_semaphore;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::scene::get_update_lights_semaphore)() const noexcept
{
	return this->update_lights_semaphore;
}

inline ::framework::gpu::semaphore * (::framework::graphics::ray_tracing::scene::get_update_semaphore)() const noexcept
{
	return this->update_semaphore;
}

inline ::framework::gpu::buffer * (::framework::graphics::ray_tracing::scene::get_shader_binding_table_buffer)() const noexcept
{
	return this->shader_binding_table_buffer;
}

inline ::std::uint64_t (::framework::graphics::ray_tracing::scene::get_closest_hit_shader_binding_table_device_address)() const noexcept
{
	return this->closest_hit_shader_binding_table_device_address;
}

inline ::framework::gpu::device_size (::framework::graphics::ray_tracing::scene::get_closest_hit_shader_group_offset)(::std::uint32_t geometry_instance_index) const noexcept
{
	return this->closest_hit_shader_binding_table_base_offset + geometry_instance_index * this->closest_hit_shader_group_stride * this->hit_group_count + this->closest_hit_shader_binding_table_offset_to_shader_record;
}

inline ::framework::graphics::ray_tracing::light_collection * (::framework::graphics::ray_tracing::scene::get_light_collection)() const noexcept
{
	return this->light_collection.get();
}