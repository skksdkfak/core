#pragma once

#include "algorithm/scan.hpp"
#include "algorithm/xavier_uniform.hpp"
#include "command_context.hpp"
#include "coroutine/async_auto_reset_event.hpp"
#include "coroutine/mutex.hpp"
#include "coroutine/static_thread_pool.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "graphics/ray_tracing/accumulate.hpp"
#include "graphics/ray_tracing/aov.hpp"
#include "graphics/ray_tracing/material_system.hpp"
#include "graphics/ray_tracing/medium_system.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/scene_view_perspective.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "graphics/scene_renderer.hpp"
#include "memory/allocator.hpp"
#include "normalizing_flows/normalizing_flow.hpp"
#include "nn/optimizers/adam.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "nn/grid/encoding.hpp"
#include "nn/one_blob/encoding.hpp"
#include "nn/losses/relative_l2.hpp"
#include "profiler.hpp"
#include "resource/resource_manager.hpp"
#include <glm/glm.hpp>
#include <memory>
#include <vector>

namespace framework::graphics::ray_tracing
{
	class scene;
	class material;
	class medium;

	struct scene_renderer_create_info
	{
		::framework::gpu_context * gpu_context;
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::std::uint32_t width;
		::std::uint32_t height;
		::std::uint32_t frame_buffer_count;
		::framework::gpu::format output_color_format;
		::framework::gpu::image * output_color_image;
		::framework::gpu::image_view * output_color_image_view;
		::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::gpu::descriptor_pool * device_descriptor_pool;
		::framework::gpu::descriptor_set * attachments_descriptor_set;
		::framework::memory::allocator * single_frame_allocator;
		::framework::resource::resource_manager * resource_manager;
		::framework::coroutine::static_thread_pool * thread_pool;
		::framework::command_buffer_manager * command_buffer_manager;
		::framework::profiler * profiler;
		::framework::gpu_log * gpu_log;
	};

	struct scene_renderer_resize_info
	{
		::std::uint32_t width;
		::std::uint32_t height;
		::framework::gpu::image * output_color_image;
		::framework::gpu::image_view * output_color_image_view;
	};

	class scene_renderer : public ::framework::graphics::scene_renderer
	{
	public:
		enum class nis_visualization_type
		{
			none,
			ground_truth,
			nis_pdf,
			neural_light_field
		};

		scene_renderer() = delete;

		explicit scene_renderer(::framework::graphics::ray_tracing::scene_renderer_create_info const * create_info);

		~scene_renderer();

		void set_scene(::framework::graphics::scene * scene) override;

		void set_scene_views(::std::uint32_t scene_view_count, ::framework::graphics::scene_view ** scene_views) override;

		::framework::graphics::scene * create_scene(::framework::graphics::scene_create_info * create_info) override;

		::framework::graphics::triangle_mesh_geometry * create_triangle_mesh_geometry(::framework::graphics::triangle_mesh_geometry_create_info const & create_info) override;

		::framework::graphics::image * create_image(::framework::graphics::image_create_info const & create_info) override;

		::framework::graphics::material * create_material(::framework::graphics::material_create_info const & create_info) override;

		::framework::graphics::medium * create_medium() override;

		//::framework::graphics::material_node * create_material_node(::framework::graphics::material_create_info const & create_info) override;

		::framework::graphics::scene_view_perspective * create_scene_view_perspective(::framework::graphics::scene_view_perspective_create_info * create_info) override;

		::framework::gpu::semaphore * get_frame_semaphore() const noexcept override;

		::std::uint64_t get_last_frame_index() const noexcept override;

		void resize(::framework::graphics::ray_tracing::scene_renderer_resize_info const * resize_info);

		void reset();
		
		void reset_nis() noexcept;

		bool get_enable_nee() const noexcept;

		void set_enable_nee(bool enable_nee) noexcept;

		bool get_enable_ncv() const noexcept;

		void set_enable_ncv(bool enable_ncv) noexcept;
		
		bool get_enable_nis() const noexcept;

		void set_enable_nis(bool enable_nis) noexcept;

		bool get_enable_nis_training() const noexcept;

		void set_enable_nis_training(bool enable_nis_training) noexcept;
		
		enum class ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type get_nis_visualization_type() const noexcept;

		void set_nis_visualization_type(enum class ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type nis_visualization_type) noexcept;

		void set_nis_visualization_screen_coordinates(::glm::uvec2 const & coordinates) noexcept;

		void recompile_shaders();

		::std::uint32_t get_current_resource_index() const noexcept;

		::framework::graphics::ray_tracing::scene_view_perspective * const * get_scene_views() const noexcept;

		::framework::coroutine::immediate_task<void> update_command_buffer(::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos);

		::framework::coroutine::static_thread_pool & get_thread_pool() { return this->thread_pool; }

		::framework::command_buffer_manager & get_command_buffer_manager() noexcept;

		::framework::gpu::pipeline * get_pipeline() const { return this->path_tracer_pipeline; }

		::framework::gpu::pipeline_layout * get_path_tracer_pipeline_layout() const noexcept;

		::framework::gpu::descriptor_set_layout * get_path_tracer_descriptor_set_layout() const noexcept;

		::framework::gpu::descriptor_set_layout * get_scene_descriptor_set_layout() const noexcept;

		::framework::gpu::descriptor_set * get_path_tracer_descriptor_set() const noexcept;

		::framework::gpu::descriptor_set * get_scene_descriptor_set() const noexcept;

		::framework::gpu::device_size get_staging_buffer_size() const { return this->staging_buffer_size; }

		::framework::gpu::device_or_host_address const & get_bottom_level_acceleration_structure_scratch_device_or_host_address() const { return this->bottom_level_acceleration_structure_scratch_device_or_host_address; }

		::framework::gpu::device_size get_indices_buffer_size() const { return this->indices_buffer_size; }

		::framework::gpu::buffer * get_indices_buffer() const { return this->indices_buffer; }

		::framework::gpu::device_memory * get_indices_device_memory() const { return this->indices_device_memory; }

		::framework::gpu::device_size allocate_indices_device_memory(::framework::gpu::device_size size, ::framework::gpu::device_size alignment) { ::framework::gpu::device_size const offset = (this->indices_device_memory_offset + alignment - 1) & ~(alignment - 1); this->indices_device_memory_offset = offset + size; return offset; }

		::framework::gpu::device_size get_indices_device_memory_usage() { return this->indices_device_memory_offset; }

		::framework::gpu::device_size get_vertices_buffer_size() const { return this->vertices_buffer_size; }

		::framework::gpu::buffer * get_vertices_buffer() const { return this->vertices_buffer; }

		::framework::gpu::device_memory * get_vertices_device_memory() const { return this->vertices_device_memory; }

		::framework::gpu::device_address get_indices_device_address() const { return this->indices_device_address; }

		::framework::gpu::device_address get_vertices_device_address() const { return this->vertices_device_address; }

		::framework::gpu::descriptor_pool * get_host_descriptor_pool() const { return this->host_descriptor_pool; }

		::framework::gpu::descriptor_pool * get_device_descriptor_pool() const { return this->device_descriptor_pool; }

		::framework::resource::resource_manager * get_resource_manager() const { return this->resource_manager; }

		::framework::graphics::ray_tracing::spectrum_system * get_spectrum_system() const { return this->spectrum_system.get(); }

		::framework::gpu::device_size allocate_vertices_device_memory(::framework::gpu::device_size size, ::framework::gpu::device_size alignment) { ::framework::gpu::device_size const offset = (this->vertices_device_memory_offset + alignment - 1) & ~(alignment - 1); this->vertices_device_memory_offset = offset + size; return offset; }

		::framework::gpu::device_size get_bottom_level_acceleration_structure_device_memory_size() const { return this->bottom_level_acceleration_structure_device_memory_size; }

		::framework::gpu::device_memory * get_bottom_level_acceleration_structure_device_memory() const { return this->bottom_level_acceleration_structure_device_memory; }

		::framework::gpu::device_size allocate_bottom_level_acceleration_structure_device_memory(::framework::gpu::device_size size, ::framework::gpu::device_size alignment) { ::framework::gpu::device_size const offset = (this->bottom_level_acceleration_structure_device_memory_offset + alignment - 1) & ~(alignment - 1); this->bottom_level_acceleration_structure_device_memory_offset = offset + size; return offset; }

		::framework::gpu::device_size get_bottom_level_acceleration_structure_device_memory_usage() { return this->bottom_level_acceleration_structure_device_memory_offset; }

		::framework::gpu_context * get_gpu_context() const noexcept;

		::framework::profiler * get_profiler() const noexcept;

		::framework::graphics::ray_tracing::material_system * get_material_system() const noexcept;

		::framework::graphics::ray_tracing::aov * get_aov() const noexcept;

		::std::uint32_t get_frame_buffer_count() const noexcept;

		::std::uint32_t get_width() const noexcept;

		::std::uint32_t get_height() const noexcept;

		::framework::gpu::image * get_color_image() const noexcept;

		::framework::gpu::image_view * get_color_image_view() const noexcept;
		
		::framework::gpu::image_view * get_output_color_image_view() const noexcept;

		::framework::gpu::image * get_nis_pdf_image() const noexcept;

		::framework::gpu::image_view * get_nis_pdf_image_view() const noexcept;
		
		::framework::gpu::image * get_ground_truth_distribution_image() const noexcept;

		::framework::gpu::image_view * get_ground_truth_distribution_image_view() const noexcept;

		::framework::gpu::descriptor_set * get_attachments_descriptor_set() const noexcept;

	private:
		void create_pipelines();

		void create_screen_buffers();

		::framework::gpu::device_size get_light_field_grid_encoding_dispatch_indirect_command_offset() const noexcept;

		::framework::gpu::device_size get_light_field_one_blob_encoding_dispatch_indirect_command_offset() const noexcept;

		struct path_tracer_parameters
		{
			float ray_eps;
			::std::uint32_t seed;
			::std::uint32_t sample_count;
			::framework::gpu::bool32_t enable_ncv;
			::framework::gpu::bool32_t enable_nis;
			::framework::gpu::bool32_t enable_nis_debug_train;
			::framework::gpu::bool32_t enable_nee;
			::framework::gpu::bool32_t visualize_neural_light_field;
			::std::uint32_t log_jacobian_buffer_stride;
			::glm::uvec2 nis_visualization_coordinates;
			::std::uint32_t pad0;
		};

		struct frame_resources
		{
			::std::unique_ptr<::framework::command_context> graphics_command_context;
			::std::unique_ptr<::framework::fence> fence;
		};

		::std::vector<struct ::framework::graphics::ray_tracing::scene_renderer::frame_resources> frame_resources;
		::std::uint32_t current_resource_index;
		::framework::coroutine::static_thread_pool & thread_pool;
		::framework::command_buffer_manager & command_buffer_manager;
		::std::uint32_t frame_buffer_count;
		::std::uint32_t width, height;
		::framework::gpu::format output_color_format;
		::framework::gpu::image * output_color_image;
		::framework::gpu::image_view * output_color_image_view;
		::framework::gpu::image * nis_pdf_image;
		::framework::gpu::device_memory * nis_pdf_device_memory;
		::framework::gpu::image_view * nis_pdf_image_view;
		::framework::gpu::image * ground_truth_distribution_image;
		::framework::gpu::device_memory * ground_truth_distribution_device_memory;
		::framework::gpu::image_view * ground_truth_distribution_image_view;
		::framework::gpu::image * color_image;
		::framework::gpu::device_memory * color_device_memory;
		::framework::gpu::image_view * color_image_view;
		::framework::gpu::image * accumulator_image;
		::framework::gpu::image_view * accumulator_image_view;
		::framework::gpu::device_memory * accumulator_device_memory;
		::framework::gpu::descriptor_set * attachments_descriptor_set;
		::framework::gpu::device_size staging_buffer_current_offset;
		::framework::gpu::device_size staging_buffer_size;
		::framework::gpu::device_size indices_buffer_size;
		::framework::gpu::device_size vertices_buffer_size;
		::framework::gpu::device_size bottom_level_acceleration_structure_device_memory_size;
		::framework::gpu::buffer * raygen_shader_binding_table_buffer;
		::framework::gpu::device_memory * raygen_shader_binding_table_device_memory;
		::std::uint64_t raygen_shader_binding_table_device_address;
		::framework::gpu::buffer * bottom_level_acceleration_structure_scratch_buffer;
		::framework::gpu::device_memory * bottom_level_acceleration_structure_scratch_device_memory;
		::framework::gpu::device_or_host_address bottom_level_acceleration_structure_scratch_device_or_host_address;
		::framework::gpu::device_size indices_device_memory_offset;
		::framework::gpu::buffer * indices_buffer;
		::framework::gpu::device_memory * indices_device_memory;
		::framework::gpu::device_address indices_device_address;
		::framework::gpu::device_size vertices_device_memory_offset;
		::framework::gpu::buffer * vertices_buffer;
		::framework::gpu::device_memory * vertices_device_memory;
		::framework::gpu::device_address vertices_device_address;
		::framework::gpu::buffer * sample_buffer;
		::framework::gpu::device_memory * sample_device_memory;
		::framework::gpu::buffer * nis_spatial_features_buffer;
		::framework::gpu::device_memory * nis_spatial_features_device_memory;
		::framework::gpu::buffer * nis_target_buffer;
		::framework::gpu::device_memory * nis_target_device_memory;
		::framework::gpu::buffer * cv_shape_target_buffer;
		::framework::gpu::device_memory * cv_shape_target_device_memory;
		::framework::gpu::buffer * light_field_target_buffer;
		::framework::gpu::device_memory * light_field_target_device_memory;
		::framework::gpu::buffer * nis_sample_pdf_buffer;
		::framework::gpu::device_memory * nis_sample_pdf_device_memory;
		::framework::gpu::buffer * path_state_buffer;
		::framework::gpu::device_memory * path_state_device_memory;
		::framework::gpu::buffer * path_vertex_state_buffer;
		::framework::gpu::device_memory * path_vertex_state_device_memory;
		::framework::gpu::buffer * visualize_hit_info_buffer;
		::framework::gpu::device_memory * visualize_hit_info_device_memory;
		::framework::gpu::buffer * batch_size_buffer;
		::framework::gpu::device_memory * batch_size_device_memory;
		::framework::gpu::buffer * shuffle_seed_buffer;
		::framework::gpu::device_memory * shuffle_seed_device_memory;
		::framework::gpu::buffer * scan_constant_buffer;
		::framework::gpu::device_memory * scan_constant_device_memory;
		::framework::gpu::buffer * scan_index_buffer;
		::framework::gpu::device_memory * scan_index_device_memory;
		::framework::gpu::buffer * scan_thread_block_reduction_buffer;
		::framework::gpu::device_memory * scan_thread_block_reduction_device_memory;
		::framework::gpu::buffer * light_field_one_blob_encode_features_buffer;
		::framework::gpu::device_memory * light_field_one_blob_encode_features_device_memory;
		::framework::gpu::buffer * light_field_encoded_features_buffer;
		::framework::gpu::device_memory * light_field_encoded_features_device_memory;
		::framework::gpu::buffer * light_field_dloss_dnetwork_input_buffer;
		::framework::gpu::device_memory * light_field_dloss_dnetwork_input_device_memory;
		::framework::gpu::buffer * dispatch_indirect_command_buffer;
		::framework::gpu::device_memory * dispatch_indirect_command_device_memory;
		::framework::gpu::device_size bottom_level_acceleration_structure_device_memory_offset;
		::framework::gpu::device_memory * bottom_level_acceleration_structure_device_memory;
		::framework::gpu::descriptor_pool * host_descriptor_pool;
		::framework::gpu::descriptor_pool * device_descriptor_pool;
		::framework::graphics::ray_tracing::scene * scene;
		::std::vector<::framework::graphics::ray_tracing::scene_view_perspective *> scene_views;
		::framework::memory::allocator * single_frame_allocator;
		::framework::gpu_context * gpu_context;
		::framework::resource::resource_manager * resource_manager;
		::framework::gpu::device_size ubo_alignment;
		::framework::gpu::buffer * ubo;
		::framework::gpu::device_memory * ubo_device_memory;
		void * mapped_data;
		::framework::gpu::shader_module * trace_primary_rays_shader_module;
		::framework::gpu::shader_module * next_vertex_shader_module;
		::framework::gpu::shader_module * path_tracer_shader_module;
		::framework::gpu::shader_module * miss_shader_module;
		::framework::gpu::shader_module * subsurface_scattering_miss_shader_module;
		::framework::gpu::shader_module * closest_hit_shader_module;
		::framework::gpu::shader_module * subsurface_scattering_any_hit_shader_module;
		::framework::gpu::shader_module * volume_closest_hit_shader_module;
		::framework::gpu::shader_module * volume_intersection_shader_module;
		::framework::gpu::shader_module * spatial_resampling_shader_module;
		::framework::gpu::shader_module * generate_forward_test_samples_shader_module;
		::framework::gpu::shader_module * plot_pdf_shader_module;
		::framework::gpu::shader_module * align_batch_size_shader_module;
		::framework::gpu::shader_module * concatenate_features_shader_module;
		::framework::gpu::shader_module * cv_shape_concatenate_features_shader_module;
		::framework::gpu::descriptor_set_layout * path_tracer_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * scene_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * local_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * gpu_log_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * shuffle_nis_samples_descriptor_set_layout;
		::framework::gpu::descriptor_set * path_tracer_descriptor_set;
		::framework::gpu::descriptor_set * scene_descriptor_set;
		::framework::gpu::descriptor_set * gpu_log_descriptor_set;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::descriptor_set * scan_descriptor_set;
		::framework::gpu::descriptor_set * scan_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * shuffle_nis_samples_descriptor_set;
		::framework::gpu::descriptor_set * light_field_grid_encoding_initialize_parameters_descriptor_set;
		::framework::gpu::descriptor_set * light_field_grid_encoding_descriptor_set;
		::framework::gpu::descriptor_set * light_field_grid_encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * light_field_one_blob_encoding_descriptor_set;
		::framework::gpu::descriptor_set * light_field_one_blob_encoding_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * light_field_network_descriptor_set;
		::framework::gpu::descriptor_set * light_field_network_initialize_dispatch_indirect_command_descriptor_set;
		::framework::gpu::descriptor_set * light_field_network_initialize_parameters_descriptor_set;
		::framework::gpu::descriptor_set * light_field_encoding_optimizer_descriptor_set;
		::framework::gpu::descriptor_set * light_field_network_optimizer_descriptor_set;
		::framework::gpu::descriptor_set * light_field_loss_descriptor_set;
		::framework::gpu::pipeline_layout * path_tracer_pipeline_layout;
		::framework::gpu::pipeline_layout * local_pipeline_layout;
		::framework::gpu::pipeline * path_tracer_pipeline;
		::framework::gpu::pipeline * spatial_resampling_pipeline;
		::framework::gpu::pipeline * generate_forward_test_samples_pipeline;
		::framework::gpu::pipeline * plot_pdf_pipeline;
		::framework::gpu::pipeline * align_batch_size_pipeline;
		::framework::gpu::pipeline * concatenate_features_pipeline;
		::framework::gpu::pipeline * cv_shape_concatenate_features_pipeline;
		::framework::gpu::semaphore * frame_semaphore;
		::std::unique_ptr<::framework::graphics::ray_tracing::material_system> material_system;
		::std::unique_ptr<::framework::graphics::ray_tracing::medium_system> medium_system;
		::std::unique_ptr<::framework::graphics::ray_tracing::spectrum_system> spectrum_system;
		::std::unique_ptr<::framework::graphics::ray_tracing::accumulate> accumulate;
		::std::unique_ptr<::framework::graphics::ray_tracing::aov> aov;
		::std::unique_ptr<::framework::normalizing_flows::normalizing_flow> cv_shape_normalizing_flow;
		::std::unique_ptr<::framework::normalizing_flows::normalizing_flow> nis_normalizing_flow;
		::std::unique_ptr<::framework::algorithm::xavier_uniform> xavier_uniform;
		::std::unique_ptr<::framework::nn::grid::execution_policy> light_field_grid_encoding_execution_policy;
		::std::unique_ptr<::framework::nn::grid::encoding> light_field_grid_encoding;
		::std::unique_ptr<::framework::nn::one_blob::execution_policy> light_field_one_blob_encoding_execution_policy;
		::std::unique_ptr<::framework::nn::one_blob::encoding> light_field_one_blob_encoding;
		::std::unique_ptr<::framework::nn::fully_fused_mlp::execution_policy> light_field_network_execution_policy;
		::std::unique_ptr<::framework::nn::fully_fused_mlp::network> light_field_neural_network;
		::std::unique_ptr<::framework::nn::adam> light_field_encoding_optimizer;
		::std::unique_ptr<::framework::nn::adam> light_field_network_optimizer;
		::std::unique_ptr<::framework::nn::relative_l2> light_field_loss;
		::std::unique_ptr<::framework::algorithm::scan> scan;
		::std::unique_ptr<::framework::algorithm::scan::executor> scan_executor;
		::framework::profiler * profiler;
		::framework::gpu_log * gpu_log;
		::std::uint32_t triangle_mesh_geometry_counter;
		::std::uint32_t texture_counter;
		::std::uint64_t frame_index;
		enum class ::framework::graphics::ray_tracing::scene_renderer::nis_visualization_type nis_visualization_type;
		::glm::uvec2 nis_visualization_coordinates;
		bool enable_nee;
		bool enable_ncv;
		bool enable_nis;
		bool enable_nis_training;
		bool is_reset_nis;
	};
}

#include "graphics/ray_tracing/scene_renderer.inl"