#include "coroutine/sync_wait.hpp"
#include "gpu/utility.hpp"
#include "graphics/ray_tracing/scene.hpp"
#include "graphics/ray_tracing/scene_renderer.hpp"
#include "graphics/ray_tracing/spectrum_system.hpp"
#include "graphics/ray_tracing/triangle_mesh_geometry.hpp"
#include "graphics/ray_tracing/triangle_mesh_scene_geometry.hpp"

::framework::graphics::ray_tracing::spectrum::spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer) :
	is_pointer(is_pointer),
	initialization_shared_task(this->initialize(spectrum_system))
{
}

::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::spectrum::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system)
{
	co_await spectrum_system->add_spectrum_to_initialize(this);
}

::framework::graphics::ray_tracing::constant_spectrum::constant_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::constant_spectrum const & constant_spectrum) :
	::framework::graphics::ray_tracing::spectrum::spectrum(spectrum_system, is_pointer),
	four_byte_address(four_byte_address),
	c(constant_spectrum.get_constant())
{
}

::std::size_t(::framework::graphics::ray_tracing::constant_spectrum::get_size)() const
{
	return this->is_pointer ? 1 : 0;
}

::framework::graphics::ray_tracing::spectrum::spectrum_pointer(::framework::graphics::ray_tracing::constant_spectrum::get_pointer)() const
{
	return { this->four_byte_address | (::framework::graphics::ray_tracing::spectrum::type::constant_spectrum << ::framework::graphics::ray_tracing::spectrum::tag_shift) };
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::constant_spectrum::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context)
{
	if (this->is_pointer)
	{
		co_await spectrum_system->upload_spectrum_data(command_context, this->four_byte_address * 4, 4, &this->c);
	}
}

::framework::graphics::ray_tracing::densely_sampled_spectrum::densely_sampled_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::densely_sampled_spectrum const & densely_sampled_spectrum) :
	::framework::graphics::ray_tracing::spectrum::spectrum(spectrum_system, is_pointer)
{
	this->lambda_min = densely_sampled_spectrum.get_lambda_min();
	this->lambda_max = densely_sampled_spectrum.get_lambda_max();
	this->four_byte_address = four_byte_address;
	this->values = densely_sampled_spectrum.get_values();
}

::std::size_t(::framework::graphics::ray_tracing::densely_sampled_spectrum::get_size)() const
{
	::std::size_t const value_size = this->lambda_max - this->lambda_min + 1;
	return this->is_pointer ? 1 + value_size : value_size;
}

::framework::graphics::ray_tracing::spectrum::spectrum_pointer(::framework::graphics::ray_tracing::densely_sampled_spectrum::get_pointer)() const
{
	return { this->four_byte_address | (::framework::graphics::ray_tracing::spectrum::type::densely_sampled_spectrum << ::framework::graphics::ray_tracing::spectrum::tag_shift) };
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::densely_sampled_spectrum::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context)
{
	::framework::gpu::device_size offset = this->four_byte_address * 4;
	if (this->is_pointer)
	{
		struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob const device_blob = this->get_densely_sampled_spectrum_device_blob();
		co_await spectrum_system->upload_spectrum_data(command_context, offset, 4, &device_blob);
		offset += 4;
	}
	co_await spectrum_system->upload_spectrum_data(command_context, offset, this->values.size() * sizeof(float), this->values.data());
	this->values.clear();
}

struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob(::framework::graphics::ray_tracing::densely_sampled_spectrum::get_densely_sampled_spectrum_device_blob)() const
{
	struct ::framework::graphics::ray_tracing::densely_sampled_spectrum::device_blob device_blob
	{
		this->lambda_min,
		this->lambda_max,
		this->is_pointer ? this->four_byte_address + 1 : this->four_byte_address
	};
	return device_blob;
}

::framework::graphics::ray_tracing::piecewise_linear_spectrum::piecewise_linear_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::piecewise_linear_spectrum const & piecewise_linear_spectrum) :
	::framework::graphics::ray_tracing::spectrum::spectrum(spectrum_system, is_pointer),
	four_byte_address(four_byte_address),
	lambdas(piecewise_linear_spectrum.get_lambdas()),
	values(piecewise_linear_spectrum.get_values())
{
}

::std::size_t(::framework::graphics::ray_tracing::piecewise_linear_spectrum::get_size)() const
{
	::std::size_t const value_size = this->lambdas.size() * 2;
	return this->is_pointer ? 1 + value_size : value_size;
}

::framework::graphics::ray_tracing::spectrum::spectrum_pointer(::framework::graphics::ray_tracing::piecewise_linear_spectrum::get_pointer)() const
{
	return { this->four_byte_address | (::framework::graphics::ray_tracing::spectrum::type::piecewise_linear_spectrum << ::framework::graphics::ray_tracing::spectrum::tag_shift) };
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::piecewise_linear_spectrum::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context)
{
	::framework::gpu::device_size offset = this->four_byte_address * 4;
	if (this->is_pointer)
	{
		::std::uint32_t const four_byte_size = static_cast<::std::uint32_t>(this->values.size());
		co_await spectrum_system->upload_spectrum_data(command_context, offset, 4, &four_byte_size);
		offset += 4;
	}
	co_await spectrum_system->upload_spectrum_data(command_context, offset, this->lambdas.size() * sizeof(float), this->lambdas.data());
	offset += this->lambdas.size() * sizeof(float);
	co_await spectrum_system->upload_spectrum_data(command_context, offset, this->values.size() * sizeof(float), this->values.data());
	this->lambdas.clear();
	this->values.clear();
}

struct ::framework::graphics::ray_tracing::piecewise_linear_spectrum::device_blob(::framework::graphics::ray_tracing::piecewise_linear_spectrum::get_piecewise_linear_spectrum_device_blob)() const
{
	struct ::framework::graphics::ray_tracing::piecewise_linear_spectrum::device_blob device_blob
	{
		this->is_pointer ? this->four_byte_address + 1 : this->four_byte_address,
			static_cast<::std::uint32_t>(this->lambdas.size())
	};
	return device_blob;
}

::framework::graphics::ray_tracing::blackbody_spectrum::blackbody_spectrum(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, bool is_pointer, ::std::uint32_t four_byte_address, ::framework::physics::blackbody_spectrum const & blackbody_spectrum) :
	::framework::graphics::ray_tracing::spectrum::spectrum(spectrum_system, is_pointer),
	device_blob{ blackbody_spectrum.get_temperature(), blackbody_spectrum.get_normalization_factor() }
{
}

::std::size_t(::framework::graphics::ray_tracing::blackbody_spectrum::get_size)() const
{
	return this->is_pointer ? 2 : 0;
}

::framework::graphics::ray_tracing::spectrum::spectrum_pointer(::framework::graphics::ray_tracing::blackbody_spectrum::get_pointer)() const
{
	return { this->four_byte_address | (::framework::graphics::ray_tracing::spectrum::tag_shift << ::framework::graphics::ray_tracing::spectrum::type::blackbody_spectrum) };
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::blackbody_spectrum::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, class ::framework::command_context * const command_context)
{
	if (this->is_pointer)
	{
		co_await spectrum_system->upload_spectrum_data(command_context, this->four_byte_address * 4, sizeof(struct ::framework::graphics::ray_tracing::blackbody_spectrum::device_blob), &this->device_blob);
	}
}

struct ::framework::graphics::ray_tracing::blackbody_spectrum::device_blob(::framework::graphics::ray_tracing::blackbody_spectrum::get_blackbody_spectrum_device_blob)() const
{
	return this->device_blob;
}

::framework::graphics::ray_tracing::rgb_to_spectrum_table::rgb_to_spectrum_table(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, ::std::uint32_t offset, ::std::vector<float> && data) :
	data(::std::move(data)),
	initialization_shared_task(this->initialize(spectrum_system))
{
	this->device_blob.offset = offset;
}

::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::rgb_to_spectrum_table::initialize)(::framework::graphics::ray_tracing::spectrum_system * spectrum_system)
{
	co_await spectrum_system->add_rgb_to_spectrum_table_to_initialize(this);
}

struct ::framework::graphics::ray_tracing::rgb_to_spectrum_table::device_blob(::framework::graphics::ray_tracing::rgb_to_spectrum_table::get_device_blob)() const
{
	return this->device_blob;
}

::framework::graphics::ray_tracing::rgb_color_space::rgb_color_space(::framework::graphics::ray_tracing::spectrum_system * spectrum_system, ::framework::graphics::ray_tracing::densely_sampled_spectrum const & illuminant, ::framework::graphics::ray_tracing::rgb_to_spectrum_table const & rgb_to_spectrum_table) :
	illuminant(illuminant),
	rgb_to_spectrum_table(rgb_to_spectrum_table)
{
}

::framework::graphics::ray_tracing::rgb_color_space::device_blob(::framework::graphics::ray_tracing::rgb_color_space::get_device_blob)() const
{
	return ::framework::graphics::ray_tracing::rgb_color_space::device_blob
	{
		this->illuminant.get_densely_sampled_spectrum_device_blob(),
		this->rgb_to_spectrum_table.get_device_blob()
	};
}

::framework::graphics::ray_tracing::spectrum_system::spectrum_system(::framework::graphics::ray_tracing::scene_renderer * scene_renderer) :
	scene_renderer(scene_renderer),
	rgb_to_spectrum_table_pointer(0),
	spectrum_pointer(0)
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	::std::uint32_t const rgb_to_spectrum_table_max_count = 1;

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// uniform_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 4;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// rgb_spectrum_table
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 4;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// spectrum
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 4;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::raygen_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		this->uniform_buffer_alignment = (sizeof(struct ::framework::graphics::ray_tracing::spectrum_system::uniform_buffer) + this->scene_renderer->get_gpu_context()->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(this->scene_renderer->get_gpu_context()->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);

		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->uniform_buffer_alignment * scene_renderer->get_frame_buffer_count();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->uniform_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->uniform_buffer;
		debug_utils_object_name_info.object_name = "uniform_buffer";
		//assert_framework_gpu_result(device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->uniform_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->uniform_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->uniform_buffer_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->uniform_buffer;
		bind_buffer_memory_info.memory = this->uniform_buffer_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(device->map_memory(this->uniform_buffer_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&this->uniform_buffer_mapped_data)));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = rgb_to_spectrum_table_max_count * (this->rgb_to_spectrum_table_resolution + rgb_to_spectrum_table_max_count * this->rgb_to_spectrum_table_resolution * this->rgb_to_spectrum_table_resolution * this->rgb_to_spectrum_table_resolution * 9) * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->rgb_spectrum_table_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->rgb_spectrum_table_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->rgb_spectrum_table_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->rgb_spectrum_table_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->rgb_spectrum_table_buffer;
		bind_buffer_memory_info.memory = this->rgb_spectrum_table_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 16 * 1024 * 1024;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->scene_renderer->get_gpu_context()->get_queue_family_indices().graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::non_fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->create_buffer(&buffer_create_info, nullptr, &this->spectrum_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->spectrum_buffer;
		device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(this->scene_renderer->get_gpu_context()->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->spectrum_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(device->allocate_memory(&memory_allocate_info, nullptr, &this->spectrum_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->spectrum_buffer;
		bind_buffer_memory_info.memory = this->spectrum_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_host_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->host_descriptor_set));
	}

	if (scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		this->device_descriptor_set = this->host_descriptor_set;
	}
	else
	{
		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = scene_renderer->get_device_descriptor_pool();
		descriptor_set_allocate_info.descriptor_set_count = 1;
		descriptor_set_allocate_info.set_layouts = &this->descriptor_set_layout;
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->device_descriptor_set));
	}

	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
	// uniform_buffer
	descriptor_buffer_infos[0].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
	descriptor_buffer_infos[0].buffer = this->uniform_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = this->uniform_buffer_alignment;
	descriptor_buffer_infos[0].stride = this->uniform_buffer_alignment;

	descriptor_buffer_infos[1].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
	descriptor_buffer_infos[1].buffer = this->rgb_spectrum_table_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = sizeof(float);

	descriptor_buffer_infos[2].flags = ::framework::gpu::descriptor_buffer_info_flags::raw_bit;
	descriptor_buffer_infos[2].buffer = this->spectrum_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = sizeof(float);

	::framework::gpu::write_descriptor_set write_descriptor_sets[3];
	// uniform_buffer
	write_descriptor_sets[0].dst_set = this->host_descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	// rgb_spectrum_table
	write_descriptor_sets[1].dst_set = this->host_descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// spectrum
	write_descriptor_sets[2].dst_set = this->host_descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;
	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	this->bind_descriptor_set();

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::timeline;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(device->create_semaphore(&semaphore_create_info, nullptr, &this->update_semaphore));

	this->x = this->create_densely_sampled_spectrum(::framework::physics::piecewise_linear_spectrum(::framework::physics::cie_lambda, ::framework::physics::cie_x, ::framework::physics::cie_sample_count), false);
	this->y = this->create_densely_sampled_spectrum(::framework::physics::piecewise_linear_spectrum(::framework::physics::cie_lambda, ::framework::physics::cie_y, ::framework::physics::cie_sample_count), false);
	this->z = this->create_densely_sampled_spectrum(::framework::physics::piecewise_linear_spectrum(::framework::physics::cie_lambda, ::framework::physics::cie_z, ::framework::physics::cie_sample_count), false);
	this->r_bar = this->x;
	this->g_bar = this->y;
	this->b_bar = this->z;
	this->cie_illum_d6500 = this->create_densely_sampled_spectrum(*::framework::physics::spectra::cie_illum_d6500, false);
	this->srgb_rgb_to_spectrum_table = this->create_rgb_to_spectrum_table(*::framework::physics::rgb_to_spectrum_table::srgb);
	this->srgb_rgb_color_space = new ::framework::graphics::ray_tracing::rgb_color_space(this, *this->cie_illum_d6500, *this->srgb_rgb_to_spectrum_table);
	::framework::coroutine::sync_wait(this->x->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->y->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->z->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->r_bar->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->g_bar->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->b_bar->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->cie_illum_d6500->get_initialization_shared_task());
	::framework::coroutine::sync_wait(this->srgb_rgb_to_spectrum_table->get_initialization_shared_task());
}

::framework::graphics::ray_tracing::spectrum_system::~spectrum_system()
{
	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();
	device->destroy_buffer(this->rgb_spectrum_table_buffer, nullptr);
	device->destroy_buffer(this->spectrum_buffer, nullptr);
	device->free_memory(this->rgb_spectrum_table_device_memory, nullptr);
	device->free_memory(this->spectrum_device_memory, nullptr);
}

::framework::graphics::ray_tracing::rgb_to_spectrum_table * (::framework::graphics::ray_tracing::spectrum_system::create_rgb_to_spectrum_table)(::framework::physics::rgb_to_spectrum_table const & rgb_to_spectrum_table)
{
	::framework::gpu::device_size const rgb_spectrum_table_scale_count = this->rgb_to_spectrum_table_resolution;
	::framework::gpu::device_size const rgb_spectrum_table_coefficients_count = this->rgb_to_spectrum_table_resolution * this->rgb_to_spectrum_table_resolution * this->rgb_to_spectrum_table_resolution * 9;
	::framework::gpu::device_size const rgb_spectrum_table_size = rgb_spectrum_table_scale_count + rgb_spectrum_table_coefficients_count;
	::std::vector<float> data(rgb_spectrum_table_size);
	::std::memcpy(&data[0], rgb_to_spectrum_table.get_z_nodes(), rgb_spectrum_table_scale_count * sizeof(float));
	::std::memcpy(&data[rgb_spectrum_table_scale_count], rgb_to_spectrum_table.get_coefficient(), rgb_spectrum_table_coefficients_count * sizeof(float));
	::framework::graphics::ray_tracing::rgb_to_spectrum_table * ray_tracing_rgb_to_spectrum_table = new ::framework::graphics::ray_tracing::rgb_to_spectrum_table(this, this->rgb_to_spectrum_table_pointer, ::std::move(data));
	this->rgb_to_spectrum_table_pointer += rgb_spectrum_table_size;
	return ray_tracing_rgb_to_spectrum_table;
}

::framework::graphics::ray_tracing::constant_spectrum * (::framework::graphics::ray_tracing::spectrum_system::create_constant_spectrum)(::framework::physics::constant_spectrum const & constant_spectrum, bool is_pointer)
{
	::framework::graphics::ray_tracing::constant_spectrum * ray_tracing_constant_spectrum = new ::framework::graphics::ray_tracing::constant_spectrum(this, is_pointer, this->spectrum_pointer, constant_spectrum);
	this->spectrum_pointer += ray_tracing_constant_spectrum->get_size();
	return ray_tracing_constant_spectrum;
}

::framework::graphics::ray_tracing::densely_sampled_spectrum * (::framework::graphics::ray_tracing::spectrum_system::create_densely_sampled_spectrum)(::framework::physics::densely_sampled_spectrum const & densely_sampled_spectrum, bool is_pointer)
{
	::framework::graphics::ray_tracing::densely_sampled_spectrum * ray_tracing_densely_sampled_spectrum = new ::framework::graphics::ray_tracing::densely_sampled_spectrum(this, is_pointer, this->spectrum_pointer, densely_sampled_spectrum);
	this->spectrum_pointer += ray_tracing_densely_sampled_spectrum->get_size();
	return ray_tracing_densely_sampled_spectrum;
}

::framework::graphics::ray_tracing::piecewise_linear_spectrum * (::framework::graphics::ray_tracing::spectrum_system::create_piecewise_linear_spectrum)(::framework::physics::piecewise_linear_spectrum const & piecewise_linear_spectrum, bool is_pointer)
{
	::framework::graphics::ray_tracing::piecewise_linear_spectrum * ray_tracing_piecewise_linear_spectrum = new ::framework::graphics::ray_tracing::piecewise_linear_spectrum(this, is_pointer, this->spectrum_pointer, piecewise_linear_spectrum);
	this->spectrum_pointer += ray_tracing_piecewise_linear_spectrum->get_size();
	return ray_tracing_piecewise_linear_spectrum;
}

::framework::graphics::ray_tracing::blackbody_spectrum * (::framework::graphics::ray_tracing::spectrum_system::create_blackbody_spectrum)(::framework::physics::blackbody_spectrum const & blackbody_spectrum, bool is_pointer)
{
	::framework::graphics::ray_tracing::blackbody_spectrum * ray_tracing_blackbody_spectrum = new ::framework::graphics::ray_tracing::blackbody_spectrum(this, is_pointer, this->spectrum_pointer, blackbody_spectrum);
	this->spectrum_pointer += ray_tracing_blackbody_spectrum->get_size();
	return ray_tracing_blackbody_spectrum;
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::spectrum_system::add_rgb_to_spectrum_table_to_initialize)(::framework::graphics::ray_tracing::rgb_to_spectrum_table * rgb_to_spectrum_table)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->rgb_to_spectrum_tables_to_initialize_mutex.scoped_lock();
	this->rgb_to_spectrum_tables_to_initialize.push_back(rgb_to_spectrum_table);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::spectrum_system::add_spectrum_to_initialize)(::framework::graphics::ray_tracing::spectrum * spectrum)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->spectra_to_initialize_mutex.scoped_lock();
	this->spectra_to_initialize.push_back(spectrum);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::spectrum_system::update)()
{
	class ::framework::command_context * const transfer_command_context = co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->acquire_command_context();
	class ::framework::gpu::command_buffer * const transfer_command_buffer = transfer_command_context->get_command_buffer();

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
	command_buffer_begin_info.inheritance_info = nullptr;
	assert_framework_gpu_result(transfer_command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->spectra_to_initialize_mutex.scoped_lock();
		for (::framework::graphics::ray_tracing::spectrum * spectrum : this->spectra_to_initialize)
		{
			co_await spectrum->initialize(this, transfer_command_context);
		}
		this->spectra_to_initialize.clear();
	}
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->rgb_to_spectrum_tables_to_initialize_mutex.scoped_lock();
		for (::framework::graphics::ray_tracing::rgb_to_spectrum_table * rgb_to_spectrum_table : this->rgb_to_spectrum_tables_to_initialize)
		{
			co_await transfer_command_context->update_buffer(this->rgb_spectrum_table_buffer, rgb_to_spectrum_table->data.data(), rgb_to_spectrum_table->device_blob.offset * sizeof(float), rgb_to_spectrum_table->data.size() * sizeof(float));
			rgb_to_spectrum_table->data.clear();
		}
		this->rgb_to_spectrum_tables_to_initialize.clear();
	}

	this->get_current_frame_uniform_buffer_mapped_data()->x = this->x->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->y = this->y->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->z = this->z->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->r_bar = this->r_bar->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->g_bar = this->g_bar->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->b_bar = this->b_bar->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->cie_illum_d6500 = this->cie_illum_d6500->get_densely_sampled_spectrum_device_blob();
	this->get_current_frame_uniform_buffer_mapped_data()->srgb_rgb_color_space = this->srgb_rgb_color_space->get_device_blob();

	assert_framework_gpu_result(transfer_command_buffer->end_command_buffer());

	::framework::gpu::semaphore_submit_info wait_semaphore_info;
	wait_semaphore_info.semaphore = this->scene_renderer->get_frame_semaphore();
	wait_semaphore_info.value = this->scene_renderer->get_last_frame_index();
	wait_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
	wait_semaphore_info.device_index = 0;

	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = this->update_semaphore;
	signal_semaphore_info.value = this->scene_renderer->get_last_frame_index() + 1;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
	signal_semaphore_info.device_index = 0;
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->submit_and_wait(transfer_command_context, 1, &wait_semaphore_info, 1, &signal_semaphore_info);
	co_await this->scene_renderer->get_command_buffer_manager().get_transfer_queue()->release_command_context(transfer_command_context);
}

void ::framework::graphics::ray_tracing::spectrum_system::bind_descriptor_set()
{
	if (this->scene_renderer->get_gpu_context()->get_physical_device_features().shader_visible_host_descriptors)
	{
		return;
	}

	::framework::gpu::device * const device = this->scene_renderer->get_gpu_context()->get_device();

	::framework::gpu::copy_descriptor_set copy_descriptor_sets[3];
	for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(::std::size(copy_descriptor_sets)); i++)
	{
		copy_descriptor_sets[i].src_set = this->host_descriptor_set;
		copy_descriptor_sets[i].src_binding = i;
		copy_descriptor_sets[i].src_array_element = 0;
		copy_descriptor_sets[i].dst_set = this->device_descriptor_set;
		copy_descriptor_sets[i].dst_binding = i;
		copy_descriptor_sets[i].dst_array_element = 0;
		copy_descriptor_sets[i].descriptor_count = 1;
	}
	// uniform_buffer
	copy_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	// rgb_spectrum_table
	copy_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	// spectrum
	copy_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	device->update_descriptor_sets(0, nullptr, static_cast<::std::uint32_t>(::std::size(copy_descriptor_sets)), copy_descriptor_sets);
}

::framework::coroutine::immediate_task<void>(::framework::graphics::ray_tracing::spectrum_system::upload_spectrum_data)(class ::framework::command_context * const command_context, ::framework::gpu::device_size offset, ::framework::gpu::device_size size, void const * data)
{
	return command_context->update_buffer(this->spectrum_buffer, data, offset, size);
}

struct ::framework::graphics::ray_tracing::spectrum_system::uniform_buffer * (::framework::graphics::ray_tracing::spectrum_system::get_current_frame_uniform_buffer_mapped_data)() const noexcept
{
	return reinterpret_cast<struct ::framework::graphics::ray_tracing::spectrum_system::uniform_buffer *>(this->uniform_buffer_mapped_data + this->uniform_buffer_alignment * this->scene_renderer->get_current_resource_index());
}