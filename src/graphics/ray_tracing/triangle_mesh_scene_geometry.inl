inline ::framework::coroutine::shared_task<void>(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_initialization_shared_task)() const noexcept
{
	return this->initialization_shared_task;
}

inline ::framework::graphics::geometry * ::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_geometry() const noexcept
{
	return this->geometry;
}

inline ::framework::graphics::geometry_instance_id (::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_geometry_instance_id)(::std::uint32_t geometry_index) const noexcept
{
	return ::framework::graphics::geometry_instance_id(this->instance_id, geometry_index);
}

inline ::framework::graphics::material * ::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_material(::std::uint32_t geometry_index) const noexcept
{
	return this->materials[geometry_index];
}

inline ::framework::graphics::medium * ::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_medium(::std::uint32_t geometry_index) const noexcept
{
	return this->media[geometry_index];
}

inline ::framework::coroutine::immediate_task<::glm::mat3x4>(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_local_to_world_matrix)() noexcept
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->device_data_mutex.scoped_lock();
	co_return this->local_to_world_matrix;
}

inline ::std::uint32_t (::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_instance_id)() const noexcept
{
	return this->instance_id;
}

inline ::std::uint32_t(::framework::graphics::ray_tracing::triangle_mesh_scene_geometry::get_instance_index)() const noexcept
{
	return this->instance_index;
}