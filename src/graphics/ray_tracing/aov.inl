inline ::framework::gpu::image_view * (::framework::graphics::ray_tracing::aov::get_geometry_instance_id_image_view)() const noexcept
{
	return this->geometry_instance_id_image_view;
}

inline ::framework::gpu::descriptor_set_layout * (::framework::graphics::ray_tracing::aov::get_descriptor_set_layout)() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set * (::framework::graphics::ray_tracing::aov::get_descriptor_set)() const noexcept
{
	return this->descriptor_set;
}
