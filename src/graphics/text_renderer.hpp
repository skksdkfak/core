#pragma once

#include "engine_core.h"
#include "gpu/core.hpp"
#include "profiler.hpp"
#include <glm/glm.hpp>
#include <stb/stb_truetype.h>
#include <string>
#include <unordered_map>
#include <vector>

namespace framework::resource
{
	class resource_manager;
}

namespace framework
{
	class command_buffer_manager;
}

namespace framework::graphics
{
	struct text_renderer_create_info
	{
		typename ::framework::resource::resource_manager * resource_manager;
		typename ::framework::command_buffer_manager * command_buffer_manager;
		typename ::framework::gpu::physical_device * physical_device;
		typename ::framework::gpu::device * device;
		typename ::std::uint32_t width;
		typename ::std::uint32_t height;
		typename ::std::uint32_t max_char_count;
		typename ::std::uint32_t frame_buffer_count;
		typename ::framework::gpu::attachment_view_and_descriptor color_attachment;
		typename ::framework::gpu::attachment_view_and_descriptor depth_attachment;
		typename ::framework::gpu::attachment_description color_attachments_description;
		typename ::framework::gpu::attachment_description depth_attachment_description;
		::std::uint32_t external_dependency_count;
		::framework::gpu::subpass_dependency const * external_dependencies;
		typename ::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::profiler * profiler;
	};

	struct text_renderer_resize_info
	{
		::std::uint32_t width;
		::std::uint32_t height;
		::framework::gpu::attachment_view_and_descriptor color_attachment;
		::framework::gpu::attachment_view_and_descriptor depth_attachment;
	};

	class text_renderer
	{
	public:
		enum class text_align
		{
			align_left,
			align_center,
			align_right
		};

		enum class vertical_align
		{
			top,
			middle,
			bottom
		};

		text_renderer() = delete;

		text_renderer(::framework::graphics::text_renderer_create_info const & create_info);

		~text_renderer();

		void resize(::framework::graphics::text_renderer_resize_info const * resize_info);

		void begin_text_update();

		void add_text(::std::string const & text, float x, float y, ::framework::graphics::text_renderer::text_align align, ::framework::graphics::text_renderer::vertical_align vertical_align, ::glm::vec4 * bounds = nullptr);

		void end_text_update();

		void get_text_bounding_box(::std::string const & text, ::glm::uvec2 & min, ::glm::uvec2 & max);

		void update_command_buffer(::std::uint32_t frame_buffer_index);

		void recompile_shaders();

		::framework::gpu::command_buffer * get_command_buffer(::std::uint32_t buffer_index) { return this->command_buffers[buffer_index]; }

	private:
		void create_frame_buffer(::framework::gpu::attachment_view_and_descriptor color_attachment, ::framework::gpu::attachment_view_and_descriptor depth_attachment);

		void create_shader_modules();

		void create_pipeline();

		void load_font();

		void process_glyph(::stbtt_fontinfo const & stbtt_fontinfo, int unicode_codepoint);

		struct vertex_layout
		{
			::glm::vec4 positions;
			::std::uint32_t band_offset;
			float depth;
		};

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::framework::gpu::physical_device_properties physical_device_properties;
		::std::uint32_t max_char_count;
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::framework::gpu::command_pool * command_pool;
		::framework::gpu::command_pool * copy_command_pool;
		::std::vector<::framework::gpu::command_buffer *> command_buffers;
		::framework::gpu::frame_buffer * frame_buffer;
		::framework::gpu::render_pass * render_pass;
		::framework::gpu::render_pass_state * render_pass_state;
		::framework::gpu::shader_module * vertex_shader_module;
		::framework::gpu::shader_module * fragment_shader_module;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::buffer * vertex_buffer;
		::framework::gpu::device_memory * vertex_device_memory;
		::framework::gpu::buffer * bands_buffer;
		::framework::gpu::device_memory * bands_device_memory;
		::framework::gpu::buffer_view * bands_buffer_view;
		::framework::gpu::buffer * curves_buffer;
		::framework::gpu::device_memory * curves_device_memory;
		::framework::gpu::buffer_view * curves_buffer_view;
		::framework::gpu::sampler * sampler;
		::std::uint32_t width, height;
		::framework::graphics::text_renderer::vertex_layout * mapped_memory;
		::std::uint32_t frame_buffer_count;
		::std::uint32_t num_letters;
		typename ::framework::resource::resource_manager * resource_manager;
		typename ::framework::command_buffer_manager * command_buffer_manager;
		::framework::profiler * profiler;
		struct curve
		{
			::glm::vec2 p0;
			::glm::vec2 p1;
			::glm::vec2 p2;
			::std::uint32_t index;
			bool last;
			::std::uint_fast8_t lower_band_index;
			::std::uint_fast8_t upper_band_index;
			::std::uint_fast8_t leftmost_band_index;
			::std::uint_fast8_t rightmost_band_index;
		};

		struct glyph
		{
			signed short x0, y0, x1, y1;
			int advance;
			::std::uint32_t band_offset;
		};

		static ::std::uint32_t const vertical_band_count = 8;
		static ::std::uint32_t const horizontal_band_count = 8;
		union band_data
		{
			::glm::uvec2 uvec2;
			::glm::fvec2 fvec2;
		};
		::std::vector<union ::framework::graphics::text_renderer::band_data> bands_data;
		::std::vector<::std::uint32_t> curves_data;
		::std::vector<unsigned char> font_data;
		::std::unordered_map<int, ::framework::graphics::text_renderer::glyph> glyphs;
		float font_scale;
		int ascent;
		int descent;
		int line_gap;
	};
}