#pragma once

#include "graphics/image.hpp"
#include "physics/spectrum.hpp"
#include <glm/glm.hpp>

namespace framework::graphics
{
	class light
	{
	public:
		virtual ~light() = default;
	};

	class point_light : public ::framework::graphics::light
	{
	public:
		virtual void set_position(::glm::vec3 const & position) = 0;

		virtual void set_intensity(::framework::physics::spectrum const & intensity) = 0;

		virtual void set_scale(float scale) = 0;
	};

	class image_infinite_light : public ::framework::graphics::light
	{
	public:
		virtual void set_image(::framework::graphics::image * image) = 0;

		virtual void set_scale(float scale) = 0;
	};
}