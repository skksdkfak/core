#pragma once

#include "graphics/text_renderer.hpp"
#include "graphics/renderer_2d.hpp"
#include "input/my_input_listner.hpp"
#include <cstdint>
#include <functional>
#include <string>
#include <vector>

namespace framework::graphics
{
	struct ui_create_info
	{
		::framework::resource::resource_manager * resource_manager;
		::framework::graphics::text_renderer * text_renderer;
		::framework::graphics::renderer_2d * renderer_2d;
		::framework::input::my_input_listner * input_listner;
		::std::uint32_t width;
		::std::uint32_t height;
	};

	struct button_info
	{
		float x;
		float y;
		float width;
		float height;
		::std::string text;
		::std::function<void()> on_click_callback;
	};

	class ui
	{
	public:
		explicit ui(::framework::graphics::ui_create_info const & create_info);

		void update();

		void resize(::std::uint32_t width, ::std::uint32_t height);

		void add_button(::framework::graphics::button_info const & button_info);

	private:
		struct button
		{
			float x;
			float y;
			float width;
			float height;
			::std::string text;
			::std::uint32_t primitive_index;
			::std::function<void()> on_click_callback;
		};

		::framework::graphics::text_renderer * text_renderer;
		::framework::graphics::renderer_2d * renderer_2d;
		::framework::input::my_input_listner * input_listner;
		::std::uint32_t width;
		::std::uint32_t height;
		::std::vector<::framework::graphics::ui::button> buttons;
	};
}