#pragma once

#include "gpu/core.hpp"
#include "graphics/image.hpp"
#include "graphics/material.hpp"
#include "graphics/medium.hpp"
#include "graphics/scene.hpp"
#include "graphics/scene_view_perspective.hpp"
#include "graphics/triangle_mesh_geometry.hpp"
#include <cstdint>

namespace framework::graphics
{
	class scene_renderer
	{
	public:
		virtual ~scene_renderer() = default;

		virtual void set_scene(::framework::graphics::scene * scene) = 0;

		virtual void set_scene_views(::std::uint32_t scene_view_count, ::framework::graphics::scene_view ** scene_views) = 0;

		virtual ::framework::graphics::scene * create_scene(::framework::graphics::scene_create_info * create_info) = 0;

		virtual ::framework::graphics::triangle_mesh_geometry * create_triangle_mesh_geometry(::framework::graphics::triangle_mesh_geometry_create_info const & create_info) = 0;

		virtual ::framework::graphics::image * create_image(::framework::graphics::image_create_info const & create_info) = 0;

		virtual ::framework::graphics::material * create_material(::framework::graphics::material_create_info const & create_info) = 0;

		virtual ::framework::graphics::medium * create_medium() = 0;

		virtual ::framework::graphics::scene_view_perspective * create_scene_view_perspective(::framework::graphics::scene_view_perspective_create_info * create_info) = 0;

		virtual ::framework::gpu::semaphore * get_frame_semaphore() const noexcept = 0;

		virtual ::std::uint64_t get_last_frame_index() const noexcept = 0;
	};
}