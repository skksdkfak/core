#pragma once

#include <glm/glm.hpp>

namespace framework::graphics
{
	class scene_view
	{
	public:
		virtual ~scene_view() = default;

		virtual void set_eye(::glm::vec3 const & eye) = 0;

		virtual void set_center(::glm::vec3 const & center) = 0;

		virtual void set_up(::glm::vec3 const & up) = 0;

		virtual ::glm::vec3 const & get_eye() = 0;

		virtual ::glm::vec3 const & get_center() = 0;

		virtual ::glm::vec3 const & get_up() = 0;

		virtual void update() = 0;
	};
}