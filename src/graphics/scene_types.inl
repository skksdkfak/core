inline ::framework::graphics::geometry_instance_id::geometry_instance_id(::std::uint32_t instance_id, ::std::uint32_t geometry_index) noexcept :
	index(instance_id + geometry_index)
{
}

inline ::framework::graphics::geometry_instance_id::operator ::std::uint32_t() const noexcept
{
	return this->index;
}