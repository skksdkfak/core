#pragma once

#include <engine_core.h>
#include "gpu/core.hpp"
#include <glm/glm.hpp>
#include <vector>

namespace framework::resource
{
	class resource_manager;
}

namespace framework::graphics
{
	struct renderer_2d_create_info
	{
		typename ::framework::resource::resource_manager *	        resource_manager;
		typename ::framework::gpu::physical_device *		        physical_device;
		typename ::framework::gpu::device *					        device;
		typename ::std::uint32_t							        width;
		typename ::std::uint32_t							        height;
		typename ::std::uint32_t							        max_primitive_count;
		typename ::std::uint32_t							        max_image_count;
		typename ::std::uint32_t							        frame_buffer_count;
		typename ::framework::gpu::attachment_view_and_descriptor	color_attachment;
		typename ::framework::gpu::attachment_view_and_descriptor	depth_attachment;
		typename ::framework::gpu::attachment_description       	color_attachments_description;
		typename ::framework::gpu::attachment_description       	depth_attachment_description;
		::std::uint32_t												external_dependency_count;
		::framework::gpu::subpass_dependency const *				external_dependencies;
		typename ::framework::gpu::pipeline_cache *			        pipeline_cache;
	};

	struct renderer_2d_resize_info
	{
		::std::uint32_t				width;
		::std::uint32_t				height;
		::framework::gpu::attachment_view_and_descriptor color_attachment;
		::framework::gpu::attachment_view_and_descriptor depth_attachment;
	};

	class renderer_2d
	{
	public:
		renderer_2d() = delete;

		renderer_2d(::framework::graphics::renderer_2d_create_info const & create_info);

		~renderer_2d();

		void resize(::framework::graphics::renderer_2d_resize_info const * resize_info);

		void bind_image(::framework::gpu::image_view * image_view, ::framework::gpu::image_layout_flags image_layout, ::std::uint32_t slot);

		void begin_update();

		::std::uint32_t add_primitive();

		void update_primitive(::std::uint32_t primitive_index, float x0, float y0, float x1, float y1, float s0, float t0, float s1, float t1, ::std::uint32_t image);

		void end_update();

		void update_command_buffer(::std::uint32_t buffer_index);

		void recompile_shaders();

		::framework::gpu::command_buffer * get_command_buffer(::std::uint32_t buffer_index) const { return this->command_buffers[buffer_index]; }

	private:
		void create_frame_buffer(::framework::gpu::attachment_view_and_descriptor color_attachment, ::framework::gpu::attachment_view_and_descriptor depth_attachments);

		void create_pipeline();

		struct vertex_layout
		{
			::glm::vec4 positions;
			::glm::vec4 uvs;
			::std::uint32_t image_index;
			float depth;
		};

		struct
		{
			::std::uint32_t graphics;
			::std::uint32_t compute;
			::std::uint32_t transfer;
		} queue_family_indices;
		struct
		{
			::framework::gpu::queue * graphics;
			::framework::gpu::queue * compute;
			::framework::gpu::queue * transfer;
		} queues;
		::framework::gpu::physical_device_memory_properties physical_device_memory_properties;
		::std::uint32_t max_primitive_count;
		::std::uint32_t max_image_count;
		::framework::gpu::physical_device * physical_device;
		::framework::gpu::device * device;
		::framework::gpu::command_pool * command_pool;
		::framework::gpu::command_pool * copy_command_pool;
		::std::vector<::framework::gpu::command_buffer *> command_buffers;
		::framework::gpu::frame_buffer * frame_buffer;
		::framework::gpu::render_pass * render_pass;
		::framework::gpu::render_pass_state * render_pass_state;
		::framework::gpu::shader_module * vertex_shader_module;
		::framework::gpu::shader_module * fragment_shader_module;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_cache * pipeline_cache;
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_pool * descriptor_pool;
		::framework::gpu::descriptor_set * descriptor_set;
		::framework::gpu::buffer * buffer;
		::framework::gpu::device_memory * device_memory;
		::framework::gpu::sampler * sampler;
		::std::uint32_t width, height;
		::framework::graphics::renderer_2d::vertex_layout * mapped_memory;
		::std::uint32_t frame_buffer_count;
		::std::uint32_t num_primitives;
		typename ::framework::resource::resource_manager * resource_manager;
	};
}