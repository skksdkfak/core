#include "command_context.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/task.hpp"
#include "gpu/utility.hpp"
#include "graphics/text_renderer.hpp"
#include "resource/resource_manager.hpp"
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_truetype.h>
#include <algorithm>
#include <cfloat>
#include <cmath>
#include <fstream>
#include <list>
#include <numeric>
#include <vector>

::framework::graphics::text_renderer::text_renderer(::framework::graphics::text_renderer_create_info const & create_info) :
	physical_device(create_info.physical_device),
	device(create_info.device),
	width(create_info.width),
	height(create_info.height),
	frame_buffer_count(create_info.frame_buffer_count),
	max_char_count(create_info.max_char_count),
	pipeline_cache(create_info.pipeline_cache),
	resource_manager(create_info.resource_manager),
	command_buffer_manager(create_info.command_buffer_manager),
	profiler(create_info.profiler),
	mapped_memory(nullptr)
{
	this->command_buffers.resize(create_info.frame_buffer_count);

	this->physical_device->get_properties(&this->physical_device_properties);

	physical_device->get_memory_properties(&physical_device_memory_properties);

	::std::uint32_t queue_family_property_count;
	physical_device->get_queue_family_properties(&queue_family_property_count, nullptr);
	assert(queue_family_property_count > 0);

	::std::vector<::framework::gpu::queue_family_properties> queue_family_properties(queue_family_property_count);
	physical_device->get_queue_family_properties(&queue_family_property_count, queue_family_properties.data());

	queue_family_indices =
	{
		.graphics = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::graphics_bit),
		.compute = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::compute_bit),
		.transfer = ::framework::gpu::utility::get_queue_family_index(static_cast<::std::uint32_t>(queue_family_properties.size()), queue_family_properties.data(), ::framework::gpu::queue_flags::transfer_bit)
	};

	this->device->get_queue(queue_family_indices.graphics, 0, &queues.graphics);
	this->device->get_queue(queue_family_indices.compute, 0, &queues.compute);
	this->device->get_queue(queue_family_indices.transfer, 0, &queues.transfer);

	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::reset_command_buffer_bit;
	command_pool_create_info.queue_family_index = queue_family_indices.graphics;
	assert_framework_gpu_result(this->device->create_command_pool(&command_pool_create_info, nullptr, &this->command_pool));

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = static_cast<::std::uint32_t>(this->command_buffers.size());
	assert_framework_gpu_result(this->device->allocate_command_buffers(&command_buffer_allocate_info, this->command_buffers.data()));

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->max_char_count * 4 * sizeof(::glm::vec4);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::vertex_buffer_bit | ::framework::gpu::buffer_usage_flags::host_write_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::vertex_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->vertex_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->vertex_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_write_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->vertex_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->vertex_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->vertex_buffer;
		bind_buffer_memory_info.memory = this->vertex_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 1024 * 1024 * sizeof(::glm::uvec2);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->bands_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->bands_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->bands_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->bands_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->bands_buffer;
		bind_buffer_memory_info.memory = this->bands_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.flags = ::framework::gpu::buffer_view_create_flags::none;
		buffer_view_create_info.buffer = this->bands_buffer;
		buffer_view_create_info.format = ::framework::gpu::format::r32g32_uint;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = ::framework::gpu::whole_size;
		assert_framework_gpu_result(this->device->create_buffer_view(&buffer_view_create_info, nullptr, &this->bands_buffer_view));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = 1024 * 1024 * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::uniform_texel_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = this->queue_family_indices.graphics;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::fragment_shader_resource_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->curves_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->curves_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->curves_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->curves_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->curves_buffer;
		bind_buffer_memory_info.memory = this->curves_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		::framework::gpu::buffer_view_create_info buffer_view_create_info;
		buffer_view_create_info.flags = ::framework::gpu::buffer_view_create_flags::none;
		buffer_view_create_info.buffer = this->curves_buffer;
		buffer_view_create_info.format = ::framework::gpu::format::r16g16_unorm;
		buffer_view_create_info.offset = 0;
		buffer_view_create_info.range = ::framework::gpu::whole_size;
		assert_framework_gpu_result(this->device->create_buffer_view(&buffer_view_create_info, nullptr, &this->curves_buffer_view));
	}

	::framework::gpu::sampler_create_info sampler_create_info;
	sampler_create_info.flags = ::framework::gpu::sampler_create_flags::none;
	sampler_create_info.mag_filter = ::framework::gpu::filter::linear;
	sampler_create_info.min_filter = ::framework::gpu::filter::linear;
	sampler_create_info.mip_map_mode = ::framework::gpu::sampler_mipmap_mode::linear;
	sampler_create_info.address_mode_u = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.address_mode_v = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.address_mode_w = ::framework::gpu::sampler_address_mode::repeat;
	sampler_create_info.mip_lod_bias = 0.0f;
	sampler_create_info.anisotropy_enable = false;
	sampler_create_info.max_anisotropy = 0.0f;
	sampler_create_info.compare_enable = false;
	sampler_create_info.compare_op = ::framework::gpu::compare_op::never;
	sampler_create_info.min_lod = 0.0f;
	sampler_create_info.max_lod = 1.0f;
	sampler_create_info.border_color = ::framework::gpu::border_color::float_opaque_white;
	sampler_create_info.unnormalized_coordinates = false;
	assert_framework_gpu_result(this->device->create_sampler(&sampler_create_info, nullptr, &this->sampler));

	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
	descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].hlsl_register_space = 0;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	descriptor_set_layout_bindings[0].immutable_samplers = nullptr;

	descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[1].binding = 1;
	descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
	descriptor_set_layout_bindings[1].hlsl_register_space = 0;
	descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_set_layout_bindings[1].descriptor_count = 1;
	descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::fragment_bit;
	descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
	descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	assert_framework_gpu_result(this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));

	::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
	pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
	pipeline_layout_create_info.descriptor_set_layout_count = 1;
	pipeline_layout_create_info.descriptor_set_layouts = &this->descriptor_set_layout;
	pipeline_layout_create_info.push_constant_range_count = 0;
	pipeline_layout_create_info.push_constant_ranges = nullptr;
	assert_framework_gpu_result(this->device->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &pipeline_layout));

	::framework::gpu::descriptor_pool_size descriptor_pool_sizes[1];
	descriptor_pool_sizes[0].type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	descriptor_pool_sizes[0].descriptor_count = 2;

	::framework::gpu::descriptor_pool_create_info descriptor_pool_create_info;
	descriptor_pool_create_info.flags = ::framework::gpu::descriptor_pool_create_flags::none;
	descriptor_pool_create_info.max_sets = 1;
	descriptor_pool_create_info.pool_size_count = static_cast<::std::uint32_t>(::std::size(descriptor_pool_sizes));
	descriptor_pool_create_info.pool_sizes = descriptor_pool_sizes;
	assert_framework_gpu_result(this->device->create_descriptor_pool(&descriptor_pool_create_info, nullptr, &this->descriptor_pool));

	::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
	descriptor_set_allocate_info.descriptor_pool = this->descriptor_pool;
	descriptor_set_allocate_info.descriptor_set_count = 1;
	descriptor_set_allocate_info.set_layouts = &this->descriptor_set_layout;
	descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
	device->allocate_descriptor_sets(&descriptor_set_allocate_info, &this->descriptor_set);

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	write_descriptor_sets[0].dst_set = this->descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = nullptr;
	write_descriptor_sets[0].texel_buffer_view = &this->bands_buffer_view;

	write_descriptor_sets[1].dst_set = this->descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::uniform_texel_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = nullptr;
	write_descriptor_sets[1].texel_buffer_view = &this->curves_buffer_view;
	this->device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);

	::framework::gpu::attachment_reference color_reference;
	color_reference.attachment = 0;
	color_reference.layout = ::framework::gpu::image_layout_flags::color_attachment_optimal_bit;

	::framework::gpu::attachment_reference depth_reference;
	depth_reference.attachment = 1;
	depth_reference.layout = ::framework::gpu::image_layout_flags::depth_stencil_attachment_optimal_bit;

	::framework::gpu::subpass_description subpass_description;
	subpass_description.flags = ::framework::gpu::subpass_description_flags::none;
	subpass_description.pipeline_bind_point = ::framework::gpu::pipeline_bind_point::graphics;
	subpass_description.input_attachment_count = 0;
	subpass_description.input_attachments = nullptr;
	subpass_description.color_attachment_count = 1;
	subpass_description.color_attachments = &color_reference;
	subpass_description.resolve_attachments = nullptr;
	subpass_description.depth_stencil_attachment = &depth_reference;
	subpass_description.preserve_attachment_count = 0;
	subpass_description.preserve_attachments = nullptr;

	::framework::gpu::attachment_description attachment_descriptions[2]
	{
		create_info.color_attachments_description,
		create_info.depth_attachment_description
	};

	::framework::gpu::render_pass_create_info render_pass_create_info;
	render_pass_create_info.flags = ::framework::gpu::render_pass_create_flags::none;
	render_pass_create_info.attachment_count = static_cast<::std::uint32_t>(::std::size(attachment_descriptions));
	render_pass_create_info.attachments = attachment_descriptions;
	render_pass_create_info.subpass_count = 1;
	render_pass_create_info.subpasses = &subpass_description;
	render_pass_create_info.dependency_count = create_info.external_dependency_count;
	render_pass_create_info.dependencies = create_info.external_dependencies;
	assert_framework_gpu_result(this->device->create_render_pass(&render_pass_create_info, nullptr, &this->render_pass));

	::framework::gpu::render_pass_state_create_info render_pass_state_create_info;
	render_pass_state_create_info.render_pass = this->render_pass;
	render_pass_state_create_info.max_clear_value_count = 2;
	assert_framework_gpu_result(this->device->create_render_pass_state(&render_pass_state_create_info, nullptr, &this->render_pass_state));

	this->create_frame_buffer(create_info.color_attachment, create_info.depth_attachment);
	this->create_shader_modules();
	this->create_pipeline();
	this->load_font();
}

::framework::graphics::text_renderer::~text_renderer()
{
	this->device->destroy_command_pool(this->command_pool, nullptr);
}

void ::framework::graphics::text_renderer::resize(::framework::graphics::text_renderer_resize_info const * resize_info)
{
	this->width = resize_info->width;
	this->height = resize_info->height;
	this->device->destroy_frame_buffer(this->frame_buffer, nullptr);
	this->device->destroy_pipeline(this->pipeline, nullptr);
	this->create_frame_buffer(resize_info->color_attachment, resize_info->depth_attachment);
	this->create_pipeline();
}

void ::framework::graphics::text_renderer::begin_text_update()
{
	assert_framework_gpu_result(this->device->map_memory(this->vertex_device_memory, 0, ::framework::gpu::whole_size, ::framework::gpu::memory_map_flags::none, reinterpret_cast<void **>(&this->mapped_memory)));
	this->num_letters = 0;
}

void ::framework::graphics::text_renderer::add_text(::std::string const & text, float x, float y, ::framework::graphics::text_renderer::text_align align, ::framework::graphics::text_renderer::vertical_align vertical_align, ::glm::vec4 * bounds)
{
	int pos_x = 0;
	int pos_y = 0;
	float line_width = 0;
	float scale = 2.f / this->height * this->font_scale;
	::glm::vec2 min_bound;
	::glm::vec2 max_bound;

	min_bound.x = FLT_MAX;
	max_bound.x = FLT_MIN;

	auto const process_line = [&](::std::string::const_iterator current_line_begin_it, ::std::string::const_iterator current_line_end_it)
	{
		switch (align)
		{
		case ::framework::graphics::text_renderer::text_align::align_right:
			pos_x -= line_width;
			break;
		case ::framework::graphics::text_renderer::text_align::align_center:
			pos_x -= line_width / 2.0f;
			break;
		}

		min_bound.x = ::std::fminf(min_bound.x, pos_x * scale);

		for (::std::string::const_iterator it = current_line_begin_it; it != current_line_end_it; ++it)
		{
			auto glyph_it = this->glyphs.find(*it);
			if (glyph_it == this->glyphs.end())
			{
				continue;
			}

			::framework::graphics::text_renderer::glyph const & glyph = glyph_it->second;

			switch (*it)
			{
			case ' ':
				pos_x += glyph.advance;
				continue;
			}

			this->mapped_memory[this->num_letters].positions.x = (pos_x + glyph.x0) * scale + x;
			this->mapped_memory[this->num_letters].positions.y = (pos_x + glyph.x1) * scale + x;
			this->mapped_memory[this->num_letters].positions.z = (pos_y + glyph.y0 - this->ascent) * scale - y;
			this->mapped_memory[this->num_letters].positions.w = (pos_y + glyph.y1 - this->ascent) * scale - y;
			this->mapped_memory[this->num_letters].band_offset = glyph.band_offset;
			this->mapped_memory[this->num_letters].depth = 0.2f;

			pos_x += glyph.advance;

			this->num_letters++;
		}

		max_bound.x = ::std::fmaxf(max_bound.x, pos_x * scale);
	};

	int const line_count = ::std::count(text.begin(), text.end(), '\n') + 1;
	int const text_height = (this->ascent - this->descent) * line_count + this->line_gap * (line_count - 1);

	switch (vertical_align)
	{
	case ::framework::graphics::text_renderer::vertical_align::middle:
		pos_y += text_height / 2.0f;
		break;
	case ::framework::graphics::text_renderer::vertical_align::bottom:
		pos_y += text_height;
		break;
	}

	min_bound.y = pos_y * scale;
	max_bound.y = (pos_y - text_height) * scale;

	::std::string::const_iterator current_line_begin_it = text.begin();
	::std::string::const_iterator it = text.begin();
	while (true)
	{
		if (it == text.end())
		{
			process_line(current_line_begin_it, text.end());
			break;
		}

		auto glyph_it = this->glyphs.find(*it);
		if (glyph_it != this->glyphs.end())
		{
			::framework::graphics::text_renderer::glyph const & glyph = glyph_it->second;
			line_width += glyph.advance;
		}

		switch (*it)
		{
		case '\n':
			process_line(current_line_begin_it, it);
			current_line_begin_it = it;
			line_width = 0;
			pos_y -= this->ascent - this->descent + this->line_gap;
			break;
		case '\r':
			process_line(current_line_begin_it, it);
			current_line_begin_it = it;
			line_width = 0;
			pos_x = 0;
			break;
		}

		++it;
	}

	if (bounds)
	{
		::glm::vec2 const offset(x, y);
		*bounds = ::glm::vec4(offset + min_bound, offset + max_bound);
	}
}

void ::framework::graphics::text_renderer::end_text_update()
{
	this->device->unmap_memory(this->vertex_device_memory);
	this->mapped_memory = nullptr;
}

void ::framework::graphics::text_renderer::get_text_bounding_box(::std::string const & text, ::glm::uvec2 & min, ::glm::uvec2 & max)
{
}

void ::framework::graphics::text_renderer::update_command_buffer(::std::uint32_t frame_buffer_index)
{
	::framework::gpu::device_size offsets = 0;

	::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
	command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::none;
	command_buffer_begin_info.inheritance_info = nullptr;

	::framework::gpu::clear_value clear_values[2];
	clear_values[0].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
	clear_values[1].depth_stencil = { 1.0f, 0 };

	::framework::gpu::render_pass_state_info render_pass_state_info;
	render_pass_state_info.frame_buffer = this->frame_buffer;
	render_pass_state_info.render_area.offset.x = 0;
	render_pass_state_info.render_area.offset.y = 0;
	render_pass_state_info.render_area.extent.width = this->width;
	render_pass_state_info.render_area.extent.height = this->height;
	render_pass_state_info.clear_value_count = static_cast<::std::uint32_t>(::std::size(clear_values));
	render_pass_state_info.clear_values = clear_values;
	this->render_pass_state->set_state(render_pass_state_info);

	::framework::gpu::render_pass_begin_info render_pass_begin_info;
	render_pass_begin_info.render_pass_state = this->render_pass_state;

	::framework::gpu::command_buffer * command_buffer = this->command_buffers[frame_buffer_index];

	assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));
	{
		::framework::profiler::scoped_timestamp scoped_timestamp(*this->profiler, command_buffer, ::framework::gpu::pipeline_stage_flags::top_of_pipe_bit, ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit, "text_renderer");
		command_buffer->begin_render_pass(&render_pass_begin_info, ::framework::gpu::subpass_contents::inline_);
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::graphics, this->pipeline);
		command_buffer->bind_descriptor_pool(this->descriptor_pool);
		command_buffer->bind_vertex_buffers(0, 1, &this->vertex_buffer, &offsets);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::graphics, this->pipeline_layout, 0, 1, &this->descriptor_set, 0, nullptr);
		command_buffer->draw(4, this->num_letters, 0, 0);
		command_buffer->end_render_pass();
	}
	assert_framework_gpu_result(command_buffer->end_command_buffer());
}

void ::framework::graphics::text_renderer::recompile_shaders()
{
	this->device->wait_idle();

	this->device->destroy_shader_module(this->vertex_shader_module, nullptr);
	this->device->destroy_shader_module(this->fragment_shader_module, nullptr);
	this->device->destroy_pipeline(this->pipeline, nullptr);

	this->create_shader_modules();
	this->create_pipeline();
}

void ::framework::graphics::text_renderer::create_frame_buffer(::framework::gpu::attachment_view_and_descriptor color_attachment, ::framework::gpu::attachment_view_and_descriptor depth_attachment)
{
	::framework::gpu::attachment_view_and_descriptor attachments[2];
	attachments[0] = color_attachment;
	attachments[1] = depth_attachment;

	::framework::gpu::frame_buffer_create_info frame_buffer_create_info;
	frame_buffer_create_info.flags = ::framework::gpu::frame_buffer_create_flags::none;
	frame_buffer_create_info.render_pass = this->render_pass;
	frame_buffer_create_info.attachment_count = static_cast<::std::uint32_t>(::std::size(attachments));
	frame_buffer_create_info.attachments = attachments;
	frame_buffer_create_info.width = this->width;
	frame_buffer_create_info.height = this->height;
	frame_buffer_create_info.layers = 1;
	assert_framework_gpu_result(this->device->create_frame_buffer(&frame_buffer_create_info, nullptr, &this->frame_buffer));
}

void ::framework::graphics::text_renderer::create_shader_modules()
{
	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/glyph.vsh";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::vertex_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->vertex_shader_module = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "core/src/kernels/hlsl/glyph.psh";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::fragment_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->fragment_shader_module = this->resource_manager->load_shader_module(this->device, shader_module_info);
	}
}

void ::framework::graphics::text_renderer::create_pipeline()
{
	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[2];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::vertex_bit;
	pipeline_shader_stage_create_infos[0].module = this->vertex_shader_module;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::fragment_bit;
	pipeline_shader_stage_create_infos[1].module = this->fragment_shader_module;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	bool const flip_y = !this->physical_device_properties.limits.viewport_y_up;

	::framework::gpu::viewport viewport;
	viewport.x = 0.0f;
	viewport.y = flip_y ? static_cast<float>(this->height) : 0.0f;
	viewport.width = static_cast<float>(this->width);
	viewport.height = flip_y ? -static_cast<float>(this->height) : static_cast<float>(this->height);
	viewport.min_depth = 0.0f;
	viewport.max_depth = 1.0f;

	::framework::gpu::rect_2d scissor;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = this->width;
	scissor.extent.height = this->height;

	::framework::gpu::pipeline_viewport_state_create_info pipeline_viewport_state_create_info;
	pipeline_viewport_state_create_info.viewport_count = 1;
	pipeline_viewport_state_create_info.scissors = &scissor;
	pipeline_viewport_state_create_info.scissor_count = 1;
	pipeline_viewport_state_create_info.viewports = &viewport;

	::framework::gpu::pipeline_rasterization_state_create_info pipeline_rasterization_state_create_info;
	pipeline_rasterization_state_create_info.depth_clamp_enable = false;
	pipeline_rasterization_state_create_info.rasterizer_discard_enable = false;
	pipeline_rasterization_state_create_info.polygon_mode = ::framework::gpu::polygon_mode::fill;
	pipeline_rasterization_state_create_info.cull_mode = ::framework::gpu::cull_mode_flags::none;
	pipeline_rasterization_state_create_info.front_face = ::framework::gpu::front_face::clockwise;
	pipeline_rasterization_state_create_info.depth_bias_enable = false;
	pipeline_rasterization_state_create_info.depth_bias_constant_factor = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_clamp = 0.0f;
	pipeline_rasterization_state_create_info.depth_bias_slope_factor = 0.0f;
	pipeline_rasterization_state_create_info.line_width = 1.0f;

	::framework::gpu::pipeline_multisample_state_create_info pipeline_multisample_state_create_info;
	pipeline_multisample_state_create_info.flags = ::framework::gpu::pipeline_multisample_state_create_flags::none;
	pipeline_multisample_state_create_info.rasterization_samples = ::framework::gpu::sample_count_flags::sample_count_1_bit;
	pipeline_multisample_state_create_info.sample_shading_enable = false;
	pipeline_multisample_state_create_info.min_sample_shading = 0.0f;
	pipeline_multisample_state_create_info.sample_mask = nullptr;
	pipeline_multisample_state_create_info.alpha_to_coverage_enable = false;
	pipeline_multisample_state_create_info.alpha_to_one_enable = false;

	::framework::gpu::pipeline_color_blend_attachment_state pipeline_color_blend_attachment_state;
	pipeline_color_blend_attachment_state.blend_enable = true;
	pipeline_color_blend_attachment_state.src_color_blend_factor = ::framework::gpu::blend_factor::src_alpha;
	pipeline_color_blend_attachment_state.dst_color_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
	pipeline_color_blend_attachment_state.color_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.src_alpha_blend_factor = ::framework::gpu::blend_factor::one_minus_src_alpha;
	pipeline_color_blend_attachment_state.dst_alpha_blend_factor = ::framework::gpu::blend_factor::zero;
	pipeline_color_blend_attachment_state.alpha_blend_op = ::framework::gpu::blend_op::add;
	pipeline_color_blend_attachment_state.color_write_mask = ::framework::gpu::color_component_flags::r_bit | ::framework::gpu::color_component_flags::g_bit | ::framework::gpu::color_component_flags::b_bit | ::framework::gpu::color_component_flags::a_bit;

	::framework::gpu::pipeline_color_blend_state_create_info pipeline_color_blend_state_create_info;
	pipeline_color_blend_state_create_info.logic_op_enable = false;
	pipeline_color_blend_state_create_info.logic_op = ::framework::gpu::logic_op::clear;
	pipeline_color_blend_state_create_info.attachment_count = 1;
	pipeline_color_blend_state_create_info.attachments = &pipeline_color_blend_attachment_state;
	pipeline_color_blend_state_create_info.blend_constants[0] = 0;
	pipeline_color_blend_state_create_info.blend_constants[1] = 0;
	pipeline_color_blend_state_create_info.blend_constants[2] = 0;
	pipeline_color_blend_state_create_info.blend_constants[3] = 0;

	::framework::gpu::vertex_input_binding_description vertex_input_binding_descriptions[1];
	vertex_input_binding_descriptions[0].binding = 0;
	vertex_input_binding_descriptions[0].stride = sizeof(::framework::graphics::text_renderer::vertex_layout);
	vertex_input_binding_descriptions[0].input_rate = ::framework::gpu::vertex_input_rate::instance;

	::framework::gpu::vertex_input_attribute_description vertex_input_attribute_descriptions[3];
	// Location 0 : position
	vertex_input_attribute_descriptions[0].location = 0;
	vertex_input_attribute_descriptions[0].binding = 0;
	vertex_input_attribute_descriptions[0].format = ::framework::gpu::format::r32g32b32a32_sfloat;
	vertex_input_attribute_descriptions[0].offset = offsetof(::framework::graphics::text_renderer::vertex_layout, positions);
	// Location 1 : band_offset
	vertex_input_attribute_descriptions[1].location = 1;
	vertex_input_attribute_descriptions[1].binding = 0;
	vertex_input_attribute_descriptions[1].format = ::framework::gpu::format::r32_uint;
	vertex_input_attribute_descriptions[1].offset = offsetof(::framework::graphics::text_renderer::vertex_layout, band_offset);
	// Location 2 : depth
	vertex_input_attribute_descriptions[2].location = 2;
	vertex_input_attribute_descriptions[2].binding = 0;
	vertex_input_attribute_descriptions[2].format = ::framework::gpu::format::r32_sfloat;
	vertex_input_attribute_descriptions[2].offset = offsetof(::framework::graphics::text_renderer::vertex_layout, depth);

	::framework::gpu::pipeline_vertex_input_state_create_info vertex_input_state;
	vertex_input_state.vertex_binding_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_binding_descriptions));
	vertex_input_state.vertex_binding_descriptions = vertex_input_binding_descriptions;
	vertex_input_state.vertex_attribute_description_count = static_cast<::std::uint32_t>(::std::size(vertex_input_attribute_descriptions));
	vertex_input_state.vertex_attribute_descriptions = vertex_input_attribute_descriptions;

	::framework::gpu::pipeline_input_assembly_state_create_info pipeline_input_assembly_state_create_info;
	pipeline_input_assembly_state_create_info.topology = ::framework::gpu::primitive_topology::triangle_strip;
	pipeline_input_assembly_state_create_info.primitive_restart_enable = false;

	::framework::gpu::pipeline_depth_stencil_state_create_info pipeline_depth_stencil_state_create_info;
	pipeline_depth_stencil_state_create_info.depth_test_enable = true;
	pipeline_depth_stencil_state_create_info.depth_write_enable = true;
	pipeline_depth_stencil_state_create_info.depth_compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.depth_bounds_test_enable = false;
	pipeline_depth_stencil_state_create_info.stencil_test_enable = false;
	pipeline_depth_stencil_state_create_info.back.fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.pass_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.depth_fail_op = ::framework::gpu::stencil_op::keep;
	pipeline_depth_stencil_state_create_info.back.compare_op = ::framework::gpu::compare_op::always;
	pipeline_depth_stencil_state_create_info.back.compare_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.write_mask = 0x0;
	pipeline_depth_stencil_state_create_info.back.reference = 0;
	pipeline_depth_stencil_state_create_info.front = pipeline_depth_stencil_state_create_info.back;
	pipeline_depth_stencil_state_create_info.min_depth_bounds = 0.0f;
	pipeline_depth_stencil_state_create_info.max_depth_bounds = 0.0f;

	::framework::gpu::graphics_pipeline_create_info graphics_pipeline_create_info;
	graphics_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
	graphics_pipeline_create_info.stage_count = ::std::size(pipeline_shader_stage_create_infos);
	graphics_pipeline_create_info.stages = pipeline_shader_stage_create_infos;
	graphics_pipeline_create_info.vertex_input_state = &vertex_input_state;
	graphics_pipeline_create_info.input_assembly_state = &pipeline_input_assembly_state_create_info;
	graphics_pipeline_create_info.tessellation_state = nullptr;
	graphics_pipeline_create_info.viewport_state = &pipeline_viewport_state_create_info;
	graphics_pipeline_create_info.rasterization_state = &pipeline_rasterization_state_create_info;
	graphics_pipeline_create_info.multisample_state = &pipeline_multisample_state_create_info;
	graphics_pipeline_create_info.depth_stencil_state = &pipeline_depth_stencil_state_create_info;
	graphics_pipeline_create_info.color_blend_state = &pipeline_color_blend_state_create_info;
	graphics_pipeline_create_info.dynamic_state = nullptr;
	graphics_pipeline_create_info.layout = this->pipeline_layout;
	graphics_pipeline_create_info.render_pass = this->render_pass;
	graphics_pipeline_create_info.subpass = 0;
	graphics_pipeline_create_info.base_pipeline_index = -1;
	graphics_pipeline_create_info.base_pipeline = nullptr;
	assert_framework_gpu_result(this->device->create_graphics_pipelines(this->pipeline_cache, 1, &graphics_pipeline_create_info, nullptr, &this->pipeline));
}

void ::framework::graphics::text_renderer::load_font()
{
	::std::filesystem::path font_path("C:/Windows/Fonts/calibri.ttf"/*"/usr/share/fonts/liberation-fonts/LiberationMono-Bold.ttf"*/);
	::std::ifstream ifstream(font_path, ::std::ios::binary);

	if (!ifstream.is_open())
	{
		throw ::std::exception();
	}

	this->font_data = ::std::vector<unsigned char>(::std::istreambuf_iterator<char>(ifstream), {});

	::stbtt_fontinfo stbtt_fontinfo;
	if (!::stbtt_InitFont(&stbtt_fontinfo, this->font_data.data(), 0))
	{
		throw ::std::exception();
	}

	for (int i = 32; i <= 126/*stbtt_fontinfo.numGlyphs*/; i++)
	{
		this->process_glyph(stbtt_fontinfo, i);
	}

	int fx0, fy0, fx1, fy1;
	::stbtt_GetFontBoundingBox(&stbtt_fontinfo, &fx0, &fy0, &fx1, &fy1);

	::stbtt_GetFontVMetrics(&stbtt_fontinfo, &this->ascent, &this->descent, &this->line_gap);

	this->font_scale = ::stbtt_ScaleForPixelHeight(&stbtt_fontinfo, 16);

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		::framework::command_context * const transfer_command_context = co_await this->command_buffer_manager->get_transfer_queue()->acquire_command_context();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(transfer_command_context->get_command_buffer()->begin_command_buffer(&command_buffer_begin_info));

		//::framework::gpu::buffer_memory_barrier buffer_memory_barriers[]
		//{
		//	{
		//		.src_access_mask = ::framework::gpu::access_flags::none,
		//		.dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit,
		//		.src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit,
		//		.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit,
		//		.old_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit,
		//		.new_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit,
		//		.src_queue_family_ownership =
		//		{
		//			.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored,
		//			.queue_family_index = this->queue_family_indices.graphics
		//		},
		//		.dst_queue_family_ownership =
		//		{
		//			.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored,
		//			.queue_family_index = this->queue_family_indices.graphics
		//		},
		//		.buffer = this->uniform_buffer,
		//		.offset = 0,
		//		.size = ::framework::gpu::whole_size
		//	}
		//};

		//::framework::gpu::image_memory_barrier image_memory_barriers[1];
		//image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::none;
		//image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		//image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::bottom_of_pipe_bit;
		//image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit | ::framework::gpu::image_layout_flags::undefined_bit;
		//image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		//image_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		//image_memory_barriers[0].src_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		//image_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		//image_memory_barriers[0].dst_queue_family_ownership.queue_family_index = this->queue_family_indices.graphics;
		//image_memory_barriers[0].image = image;
		//image_memory_barriers[0].subresource_range.aspect_mask = ::framework::gpu::image_aspect_flags::color_bit;
		//image_memory_barriers[0].subresource_range.base_mip_level = 0;
		//image_memory_barriers[0].subresource_range.level_count = 1;
		//image_memory_barriers[0].subresource_range.base_array_layer = 0;
		//image_memory_barriers[0].subresource_range.layer_count = 1;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit | ::framework::gpu::dependency_flags::by_region_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;

		transfer_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);
		co_await transfer_command_context->update_buffer(this->bands_buffer, this->bands_data.data(), 0, this->bands_data.size() * sizeof(::glm::uvec2));
		co_await transfer_command_context->update_buffer(this->curves_buffer, this->curves_data.data(), 0, this->curves_data.size() * sizeof(::std::uint32_t));

		//image_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		//image_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		//image_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//image_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
		//image_memory_barriers[0].old_layout = ::framework::gpu::image_layout_flags::transfer_dst_optimal_bit;
		//image_memory_barriers[0].new_layout = ::framework::gpu::image_layout_flags::fragment_shader_read_only_optimal_bit;

		//buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_write_bit;
		//buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::shader_read_bit;
		//buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		//buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::fragment_shader_bit;
		//buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		//buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;

		transfer_command_context->get_command_buffer()->pipeline_barrier(&dependency_info);

		assert_framework_gpu_result(transfer_command_context->get_command_buffer()->end_command_buffer());
		co_await this->command_buffer_manager->get_transfer_queue()->submit_and_wait(transfer_command_context, 0, nullptr, 0, nullptr);
		co_await this->command_buffer_manager->get_transfer_queue()->release_command_context(transfer_command_context);
	}());
}

void ::framework::graphics::text_renderer::process_glyph(::stbtt_fontinfo const & stbtt_fontinfo, int unicode_codepoint)
{
	int const glyph_index = ::stbtt_FindGlyphIndex(&stbtt_fontinfo, unicode_codepoint);
	if (glyph_index == 0)
	{
		return;
	}

	int advance_width;
	int left_side_bearing;
	::stbtt_GetGlyphHMetrics(&stbtt_fontinfo, glyph_index, &advance_width, &left_side_bearing);

	int gx0, gy0, gx1, gy1;
	::stbtt_GetGlyphBox(&stbtt_fontinfo, glyph_index, &gx0, &gy0, &gx1, &gy1);

	::std::uint32_t band_offset = static_cast<::std::uint32_t>(this->bands_data.size());

	::framework::graphics::text_renderer::glyph glyph;
	glyph.x0 = gx0;
	glyph.x1 = gx1;
	glyph.y0 = gy0;
	glyph.y1 = gy1;
	glyph.advance = advance_width;
	glyph.band_offset = band_offset;
	this->glyphs[unicode_codepoint] = glyph;

	::glm::vec2 const glyph_min(gx0, gy0);
	::glm::vec2 const glyph_max(gx1, gy1);
	::glm::vec2 const glyph_width(glyph_max - glyph_min);

	::std::vector<::framework::graphics::text_renderer::curve> curves;
	::std::uint32_t curve_offset = static_cast<::std::uint32_t>(this->curves_data.size());

	::stbtt_vertex * vertices;
	int const vertex_count = ::stbtt_GetGlyphShape(&stbtt_fontinfo, glyph_index, &vertices);
	if (vertex_count == 0)
	{
		return;
	}

	::framework::graphics::text_renderer::curve curve;
	curve.last = false;
	curve.index = 0;

	int vertex_index = 0;
	{
		::stbtt_vertex const & vertex = vertices[vertex_index];

		if (vertex.type == ::STBTT_vmove)
		{
			curve.p2 = (::glm::vec2(vertex.x, vertex.y) - glyph_min) / glyph_width;
			vertex_index = 1;
		}
	}
	for (; vertex_index < vertex_count; vertex_index++)
	{
		::stbtt_vertex const & vertex = vertices[vertex_index];

		switch (vertex.type)
		{
		case ::STBTT_vmove:
		{
			curves.back().last = true;
			curve.index++;
			curve.p2 = (::glm::vec2(vertex.x, vertex.y) - glyph_min) / glyph_width;
			break;
		}
		case ::STBTT_vline:
		{
			curve.p0 = curve.p2;
			curve.p2 = (::glm::vec2(vertex.x, vertex.y) - glyph_min) / glyph_width;
			curve.p1 = (curve.p0 + curve.p2) / 2.0f;
			curves.push_back(curve);
			curve.index += 2;
			break;
		}
		case ::STBTT_vcurve:
		{
			curve.p0 = curve.p2;
			curve.p1 = (::glm::vec2(vertex.cx, vertex.cy) - glyph_min) / glyph_width;
			curve.p2 = (::glm::vec2(vertex.x, vertex.y) - glyph_min) / glyph_width;
			curves.push_back(curve);
			curve.index += 2;
			break;
		}
		default:
			throw ::std::runtime_error("Unsupported vertex type");
			break;
		}
	}

	curves.back().last = true;

	for (auto & c : curves)
	{
		if (c.p1 == c.p0 || c.p1 == c.p2)
		{
			c.p1 = (c.p0 + c.p2) / 2.0f;
		}
	}

	::std::vector<::std::list<::std::uint32_t>> h_p_band_curves(this->horizontal_band_count);
	::std::vector<::std::list<::std::uint32_t>> h_n_band_curves(this->horizontal_band_count);
	::std::vector<::std::list<::std::uint32_t>> v_p_band_curves(this->vertical_band_count);
	::std::vector<::std::list<::std::uint32_t>> v_n_band_curves(this->vertical_band_count);

	::std::vector<::std::uint32_t> h_p_curve_indices(curves.size());
	::std::vector<::std::uint32_t> h_n_curve_indices(curves.size());
	::std::vector<::std::uint32_t> v_p_curve_indices(curves.size());
	::std::vector<::std::uint32_t> v_n_curve_indices(curves.size());
	::std::iota(::std::begin(h_p_curve_indices), ::std::end(h_p_curve_indices), 0);
	::std::iota(::std::begin(h_n_curve_indices), ::std::end(h_n_curve_indices), 0);
	::std::iota(::std::begin(v_p_curve_indices), ::std::end(v_p_curve_indices), 0);
	::std::iota(::std::begin(v_n_curve_indices), ::std::end(v_n_curve_indices), 0);

	::std::sort(::std::begin(h_p_curve_indices), ::std::end(h_p_curve_indices),
	[&](::std::uint32_t a_index, ::std::uint32_t b_index)
	{
		::framework::graphics::text_renderer::curve const & a = curves[a_index];
		::framework::graphics::text_renderer::curve const & b = curves[b_index];

		return ::std::max({ a.p0.x, a.p1.x, a.p2.x }) > ::std::max({ b.p0.x, b.p1.x, b.p2.x });
	});

	::std::sort(::std::begin(h_n_curve_indices), ::std::end(h_n_curve_indices),
	[&](::std::uint32_t a_index, ::std::uint32_t b_index)
	{
		::framework::graphics::text_renderer::curve const & a = curves[a_index];
		::framework::graphics::text_renderer::curve const & b = curves[b_index];

		return ::std::min({ a.p0.x, a.p1.x, a.p2.x }) < ::std::min({ b.p0.x, b.p1.x, b.p2.x });
	});

	::std::sort(::std::begin(v_p_curve_indices), ::std::end(v_p_curve_indices),
	[&](::std::uint32_t a_index, ::std::uint32_t b_index)
	{
		::framework::graphics::text_renderer::curve const & a = curves[a_index];
		::framework::graphics::text_renderer::curve const & b = curves[b_index];

		return ::std::max({ a.p0.y, a.p1.y, a.p2.y }) > ::std::max({ b.p0.y, b.p1.y, b.p2.y });
	});

	::std::sort(::std::begin(v_n_curve_indices), ::std::end(v_n_curve_indices),
	[&](::std::uint32_t a_index, ::std::uint32_t b_index)
	{
		::framework::graphics::text_renderer::curve const & a = curves[a_index];
		::framework::graphics::text_renderer::curve const & b = curves[b_index];

		return ::std::min({ a.p0.y, a.p1.y, a.p2.y }) < ::std::min({ b.p0.y, b.p1.y, b.p2.y });
	});

	this->bands_data.resize(band_offset + this->horizontal_band_count + this->vertical_band_count);

	for (::std::uint32_t i = 0; i < this->horizontal_band_count; i++)
	{
		this->bands_data[band_offset + i].fvec2.y = 0.0f;
	}

	for (::std::uint32_t i = 0; i < this->vertical_band_count; i++)
	{
		this->bands_data[band_offset + this->horizontal_band_count + i].fvec2.y = 0.0f;
	}

	for (::std::uint32_t curve_index = 0; curve_index < curves.size(); curve_index++)
	{
		::framework::graphics::text_renderer::curve & curve = curves[curve_index];

		float const min_y = ::std::min({ curve.p0.y, curve.p1.y, curve.p2.y });
		float const max_y = ::std::max({ curve.p0.y, curve.p1.y, curve.p2.y });

		curve.lower_band_index = ::std::max<float>(0, ::std::ceil(min_y * this->horizontal_band_count) - 1);
		curve.upper_band_index = ::std::max<float>(0, ::std::ceil(max_y * this->horizontal_band_count) - 1);

		for (::std::uint32_t j = curve.lower_band_index; j <= curve.upper_band_index; j++)
		{
			this->bands_data[band_offset + j].fvec2.y += curve.p0.x + curve.p1.x + curve.p2.x;
		}

		float const min_x = ::std::min({ curve.p0.x, curve.p1.x, curve.p2.x });
		float const max_x = ::std::max({ curve.p0.x, curve.p1.x, curve.p2.x });

		curve.leftmost_band_index = ::std::max<float>(0, ::std::ceil(min_x * this->vertical_band_count) - 1);
		curve.rightmost_band_index = ::std::max<float>(0, ::std::ceil(max_x * this->vertical_band_count) - 1);

		for (::std::uint32_t j = curve.leftmost_band_index; j <= curve.rightmost_band_index; j++)
		{
			this->bands_data[band_offset + this->horizontal_band_count + j].fvec2.y += curve.p0.y + curve.p1.y + curve.p2.y;
		}
	}

	for (auto & i : h_p_curve_indices)
	{
		::framework::graphics::text_renderer::curve const & curve = curves[i];

		for (::std::uint32_t j = curve.lower_band_index; j <= curve.upper_band_index; j++)
		{
			h_p_band_curves[j].push_back(curve.index);
		}
	}

	for (auto & i : h_n_curve_indices)
	{
		::framework::graphics::text_renderer::curve const & curve = curves[i];

		for (::std::uint32_t j = curve.lower_band_index; j <= curve.upper_band_index; j++)
		{
			h_n_band_curves[j].push_back(curve.index);
		}
	}

	for (auto & i : v_p_curve_indices)
	{
		::framework::graphics::text_renderer::curve const & curve = curves[i];

		for (::std::uint32_t j = curve.leftmost_band_index; j <= curve.rightmost_band_index; j++)
		{
			v_p_band_curves[j].push_back(curve.index);
		}
	}

	for (auto & i : v_n_curve_indices)
	{
		::framework::graphics::text_renderer::curve const & curve = curves[i];

		for (::std::uint32_t j = curve.leftmost_band_index; j <= curve.rightmost_band_index; j++)
		{
			v_n_band_curves[j].push_back(curve.index);
		}
	}

	for (::std::uint32_t curve_index = 0; curve_index < curves.size(); curve_index++)
	{
		::framework::graphics::text_renderer::curve const & c = curves[curve_index];

		this->curves_data.emplace_back(::glm::packUnorm2x16(c.p0));
		this->curves_data.emplace_back(::glm::packUnorm2x16(c.p1));

		if (c.last)
		{
			this->curves_data.emplace_back(::glm::packUnorm2x16(c.p2));
		}
	}

	for (::std::uint32_t i = 0; i < this->horizontal_band_count; i++)
	{
		if (h_p_band_curves[i].empty())
		{
			continue;
		}

		this->bands_data[band_offset + i].uvec2.x = static_cast<::std::uint32_t>(this->bands_data.size());

		auto h_p_band_curves_it = h_p_band_curves[i].begin();
		auto h_n_band_curves_it = h_n_band_curves[i].begin();
		for (; h_p_band_curves_it != h_p_band_curves[i].end(); h_p_band_curves_it++, h_n_band_curves_it++)
		{
			this->bands_data.push_back({ {*h_p_band_curves_it + curve_offset, *h_n_band_curves_it + curve_offset} });
		}

		float const median_pos = this->bands_data[band_offset + i].fvec2.y / (h_p_band_curves[i].size() * 3);
		assert(median_pos <= 1.0f && median_pos >= 0.0f);
		assert(h_p_band_curves[i].size() <= ::std::numeric_limits<::std::uint16_t>::max());
		this->bands_data[band_offset + i].uvec2.y = (static_cast<::std::uint32_t>(::std::round(::std::clamp(median_pos, 0.0f, 1.0f) * 65535.0f)) << 16u) | static_cast<::std::uint32_t>(h_p_band_curves[i].size());
	}

	band_offset += this->horizontal_band_count;

	for (::std::uint32_t i = 0; i < this->vertical_band_count; i++)
	{
		if (v_p_band_curves[i].empty())
		{
			continue;
		}

		this->bands_data[band_offset + i].uvec2.x = static_cast<::std::uint32_t>(this->bands_data.size());

		auto v_p_band_curves_it = v_p_band_curves[i].begin();
		auto v_n_band_curves_it = v_n_band_curves[i].begin();
		for (; v_p_band_curves_it != v_p_band_curves[i].end(); v_p_band_curves_it++, v_n_band_curves_it++)
		{
			this->bands_data.push_back({ {*v_p_band_curves_it + curve_offset, *v_n_band_curves_it + curve_offset} });
		}

		float const median_pos = this->bands_data[band_offset + i].fvec2.y / (v_p_band_curves[i].size() * 3);
		assert(median_pos <= 1.0f && median_pos >= 0.0f);
		assert(v_p_band_curves[i].size() <= ::std::numeric_limits<::std::uint16_t>::max());
		this->bands_data[band_offset + i].uvec2.y = (static_cast<::std::uint32_t>(::std::round(::std::clamp(median_pos, 0.0f, 1.0f) * 65535.0f)) << 16u) | static_cast<::std::uint32_t>(v_p_band_curves[i].size());
	}
}
