#pragma once

#include "graphics/scene_geometry.hpp"
#include <glm/glm.hpp>

namespace framework::graphics
{
	class triangle_mesh_geometry;
	class material;
	class medium;

	struct triangle_mesh_scene_geometry_create_info
	{
        ::framework::graphics::triangle_mesh_geometry *	triangle_mesh_geometry;
		::std::uint32_t									material_count;
        ::framework::graphics::material * const *		materials;
        ::framework::graphics::medium * const *			media;
	};

	class triangle_mesh_scene_geometry : virtual public ::framework::graphics::scene_geometry
	{
	public:
		virtual ~triangle_mesh_scene_geometry() = default;

		virtual void set_transform(::glm::mat3x4 const & transform) = 0;
	};
}