#pragma once

#include "gpu/core.hpp"
#include <cstdint>

namespace framework::graphics
{
	class aov
	{
	public:
		enum class type
		{
			geometry_instance_id
		};

		virtual ::framework::gpu::image * get_image(::framework::graphics::aov::type type) const noexcept = 0;
	};
}