#include "graphics/text_renderer.hpp"
#include "graphics/ui.hpp"
#include <cinttypes>

::framework::graphics::ui::ui(::framework::graphics::ui_create_info const & create_info) :
	text_renderer(create_info.text_renderer),
	renderer_2d(create_info.renderer_2d),
	input_listner(create_info.input_listner),
	width(create_info.width),
	height(create_info.height)
{
}

void ::framework::graphics::ui::update()
{
	for (auto & button : this->buttons)
	{
		::glm::vec4 text_bound;
		this->text_renderer->add_text(button.text, button.x, button.y, ::framework::graphics::text_renderer::text_align::align_center, ::framework::graphics::text_renderer::vertical_align::middle, &text_bound);

		if (button.width != 0.0f)
		{
			text_bound.x = button.x - button.width / 2.0f;
			text_bound.z = button.x + button.width / 2.0f;
		}

		if (button.height != 0.0f)
		{
			text_bound.y = button.x - button.height / 2.0f;
			text_bound.w = button.x + button.height / 2.0f;
		}

		this->renderer_2d->update_primitive(button.primitive_index, text_bound.x, text_bound.y, text_bound.z, text_bound.w, 0.0f, 0.0f, 1.0f, 1.0f, 0);

		if (button.on_click_callback)
		{
			if (this->input_listner->get_button_state(::framework::input::button::button_1))
			{
				::std::int16_t mouse_x;
				::std::int16_t mouse_y;
				this->input_listner->get_mouse_position(mouse_x, mouse_y);
				float const mouse_position_ndc_x = (mouse_x / float(this->width)) * 2.0f - 1.0f;
				float const mouse_position_ndc_y = (mouse_y / float(this->height)) * 2.0f - 1.0f;
				if (text_bound.x <= mouse_position_ndc_x && text_bound.y >= mouse_position_ndc_y && text_bound.z >= mouse_position_ndc_x && text_bound.w <= mouse_position_ndc_y)
				{
					button.on_click_callback();
				}
			}
		}
	}
}

void ::framework::graphics::ui::resize(::std::uint32_t width, ::std::uint32_t height)
{
	this->width = width;
	this->height = height;
}

void ::framework::graphics::ui::add_button(::framework::graphics::button_info const & button_info)
{
	this->buttons.push_back({ button_info.x, button_info.y, button_info.width, button_info.height, button_info.text, this->renderer_2d->add_primitive(), button_info.on_click_callback });
}
