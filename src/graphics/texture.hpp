#pragma once

#include "gpu/core.hpp"
#include <cstdint>

namespace framework::graphics
{
	struct texture_subresource_info
	{
		void const * data;
		::std::size_t row_pitch;
		::std::size_t slice_pitch;
		::std::uint32_t mip_level;
		::std::uint32_t array_layer;
	};

	struct image_create_info
	{
		::std::uint32_t	width;
		::std::uint32_t	height;
		::framework::gpu::format format;
		::std::uint32_t mip_levels;
		::std::uint32_t subresource_count;
		::framework::graphics::texture_subresource_info const * subresource_infos;
	};

	class image
	{
	};
}