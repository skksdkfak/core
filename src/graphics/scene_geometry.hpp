#pragma once

#include "coroutine/shared_task.hpp"
#include "graphics/scene_types.hpp"

namespace framework::graphics
{
	class geometry;
	class material;
	class medium;

	class scene_geometry
	{
	public:
		virtual ::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept = 0;
		
		virtual ::framework::graphics::geometry * get_geometry() const noexcept = 0;

		virtual ::framework::graphics::geometry_instance_id get_geometry_instance_id(::std::uint32_t geometry_index) const noexcept = 0;

		virtual ::framework::graphics::material * get_material(::std::uint32_t geometry_index) const noexcept = 0;
		
		virtual ::framework::graphics::medium * get_medium(::std::uint32_t geometry_index) const noexcept = 0;
	};
}