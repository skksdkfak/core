#pragma once

#include "coroutine/shared_task.hpp"
#include "graphics/geometry.hpp"
#include <glm/glm.hpp>
#include <cstdint>

namespace framework::graphics
{
	struct submesh
	{
		::std::uint32_t index_offset;
		::std::uint32_t vertex_offeset;
		::std::uint32_t index_count;
	};

	struct triangle_mesh_geometry_create_info
	{
		::std::uint32_t vertex_count;
		::glm::vec3 const * positions;
		::glm::vec3 const * normals;
		::glm::vec3 const * binormals;
		::glm::vec3 const * tangents;
		::glm::vec2 const * uvs;
		::std::uint32_t	index_count;
		::std::uint32_t const * indices;
		::std::uint32_t submesh_count;
		::framework::graphics::submesh const * submeshes;
	};

	class triangle_mesh_geometry : public ::framework::graphics::geometry
	{
	public:
		virtual ::framework::coroutine::shared_task<void> get_initialization_shared_task() const noexcept = 0;
	};
}