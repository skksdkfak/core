#pragma once

#include <vector>
#include <cstdint>
#include <glm/glm.hpp>

namespace framework::acp
{
	struct material_info
	{
		::std::uint32_t diffuse_texture;
		::std::uint32_t emissive_texture;
		bool thin_surface;
	};

	struct submesh
	{
		::std::uint32_t		material;
		::std::uint32_t		index_offset;
		::std::uint32_t		vertex_offeset;
		::std::uint32_t		index_count;
	};

	struct mesh_info
	{
		::std::uint32_t						vertex_count;
		::glm::vec3 const *					positions;
		::glm::vec3 const *					normals;
		::glm::vec3 const *					binormals;
		::glm::vec3 const *					tangents;
		::glm::vec2 const *					uvs;
		::std::uint32_t						index_count;
		::std::uint32_t const *				indices;
		::std::uint32_t						submesh_count;
		::framework::acp::submesh const *	submeshes;
	};

	struct node_create_info
	{
		::glm::mat4									transform;
		::std::uint32_t								material_count;
		::framework::acp::material_info const *		materials;
		::framework::acp::mesh_info const *			mesh_info;
	};

	class node
	{
	public:
		virtual void add_child(::framework::acp::node * node) = 0;
	};
}