inline ::framework::memory::allocator * ::framework::acp::fbx_parser::get_allocator() const noexcept
{
	return this->allocator;
}

inline ::fbxsdk::FbxManager * ::framework::acp::fbx_parser::get_fbx_sdk_manager() const noexcept
{
	return this->fbx_sdk_manager;
}