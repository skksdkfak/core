inline ::framework::graphics::scene & ::framework::acp::ecs::scene::get_graphics_scene() const noexcept
{
	return this->graphics_scene;
}

inline ::framework::graphics::scene_renderer & ::framework::acp::ecs::scene::get_scene_renderer() const noexcept
{
	return this->scene_renderer;
}

inline ::framework::resource::resource_manager & ::framework::acp::ecs::scene::get_resource_manager() const noexcept
{
	return this->resource_manager;
}

inline ::framework::coroutine::static_thread_pool & ::framework::acp::ecs::scene::get_thread_pool() const noexcept
{
	return this->thread_pool;
}

inline ::framework::acp::ecs::scene::texture_load_state const & ::framework::acp::ecs::scene::get_texture_load_state(::std::uint32_t texture_index) const noexcept
{
	return this->textures[texture_index];
}

inline ::framework::acp::ecs::node * const & ::framework::acp::ecs::scene::get_root_node() const noexcept
{
	return this->root_node;
}

inline ::framework::acp::ecs::scene::texture_load_state const & ::framework::acp::ecs::scene::get_default_image() const noexcept
{
	return this->default_image;
}