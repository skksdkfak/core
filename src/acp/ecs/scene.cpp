#include "scene.hpp"
#include "node.hpp"

::framework::acp::ecs::scene::scene(::framework::graphics::scene & graphics_scene, ::framework::graphics::scene_renderer & scene_renderer, ::framework::resource::resource_manager & resource_manager, ::framework::coroutine::static_thread_pool & thread_pool) :
	graphics_scene(graphics_scene),
	scene_renderer(scene_renderer),
	resource_manager(resource_manager),
	thread_pool(thread_pool),
	root_node(nullptr)
{
}

void ::framework::acp::ecs::scene::setup_scene(::framework::acp::scene_info const & scene_info)
{
	this->texture_count = scene_info.texture_count;
	this->textures = ::std::make_unique<::framework::acp::ecs::scene::texture_load_state[]>(scene_info.texture_count);

	for (::std::uint32_t i = 0; i < scene_info.texture_count; i++)
	{
		this->textures[i].texture_name = ::std::string(scene_info.textures[i].texture_name);
	}

	this->load_textures_task = this->load_textures(this->thread_pool);
}

::framework::coroutine::lazy_task<::framework::acp::node *>(::framework::acp::ecs::scene::create_node)(::framework::acp::node_create_info const & node_create_info)
{
	::framework::acp::ecs::node * node = new ::framework::acp::ecs::node();
	co_await node->initialize(this, node_create_info);
	co_return node;
}

void ::framework::acp::ecs::scene::set_root_node(::framework::acp::node * root_node)
{
	this->root_node = static_cast<::framework::acp::ecs::node *>(root_node);
}

::framework::coroutine::thread_pool_task<void>(::framework::acp::ecs::scene::load_textures)(::framework::coroutine::static_thread_pool & thread_pool)
{
	{
		::std::uint32_t const texel_rgba = 0;

		::framework::graphics::texture_subresource_info subresource_info;
		subresource_info.data = &texel_rgba;
		subresource_info.row_pitch = sizeof(texel_rgba);
		subresource_info.slice_pitch = sizeof(texel_rgba);
		subresource_info.mip_level = 0;
		subresource_info.array_layer = 0;

		::framework::graphics::image_create_info image_create_info;
		image_create_info.width = 1;
		image_create_info.height = 1;
		image_create_info.format = ::framework::gpu::format::r8g8b8a8_srgb;
		image_create_info.mip_levels = 1;
		image_create_info.subresource_count = 1;
		image_create_info.subresource_infos = &subresource_info;
		this->default_image.image = this->scene_renderer.create_image(image_create_info);
		co_await this->default_image.image->get_initialization_shared_task();
		this->default_image.async_manual_reset_event.set();
	}

	struct texture_load_callback : public ::framework::resource::texture_load_callback
	{
		explicit texture_load_callback(::framework::acp::ecs::scene * scene, ::framework::graphics::image *& image) :
			scene(scene),
			image(image)
		{}

		::framework::coroutine::lazy_task<void> operator()(::framework::resource::texture_load_callback::texture_load_info const & texture_load_info) override
		{
			::std::vector<::framework::graphics::texture_subresource_info> subresource_infos(texture_load_info.subresource_count);
			for (::std::uint32_t i = 0; i < texture_load_info.subresource_count; i++)
			{
				subresource_infos[i].data = texture_load_info.subresource_data[i].data;
				subresource_infos[i].row_pitch = texture_load_info.subresource_data[i].row_pitch;
				subresource_infos[i].slice_pitch = texture_load_info.subresource_data[i].slice_pitch;
				subresource_infos[i].mip_level = texture_load_info.subresource_data[i].mip_level;
				subresource_infos[i].array_layer = texture_load_info.subresource_data[i].array_layer;
			}

			::framework::graphics::image_create_info image_create_info;
			image_create_info.width = texture_load_info.width;
			image_create_info.height = texture_load_info.height;
			image_create_info.format = texture_load_info.format;
			image_create_info.mip_levels = texture_load_info.mip_levels;
			image_create_info.subresource_count = texture_load_info.subresource_count;
			image_create_info.subresource_infos = subresource_infos.data();
			this->image = this->scene->scene_renderer.create_image(image_create_info);
			co_await this->image->get_initialization_shared_task();
		};

		::framework::acp::ecs::scene * scene;
		::framework::graphics::image *& image;
	};

	for (::std::uint32_t i = 0; i < this->texture_count; i++)
	{
		struct texture_load_callback texture_load_callback(this, this->textures[i].image);

		::framework::resource::texture_load_info texture_load_info;
		texture_load_info.file_name = this->textures[i].texture_name.c_str();
		texture_load_info.texture_load_callback = &texture_load_callback;
		auto load_texture = this->resource_manager.load_texture(texture_load_info);
		co_await load_texture;

		this->textures[i].async_manual_reset_event.set();
	}

	co_return;
}