#pragma once

#include "acp/scene.hpp"
#include "coroutine/async_manual_reset_event.hpp"
#include "coroutine/thread_pool_task.hpp"
#include "graphics/scene.hpp"
#include "graphics/scene_renderer.hpp"
#include "resource/resource_manager.hpp"
#include <forward_list>
#include <memory>
#include <string>
#include <vector>

namespace framework::acp::ecs
{
	class node;

	class scene : public ::framework::acp::scene
	{
	public:
		struct texture_load_state
		{
			::std::string texture_name;
			::framework::graphics::image * image;
			::framework::coroutine::async_manual_reset_event async_manual_reset_event;
		};

		explicit scene(::framework::graphics::scene & graphics_scene, ::framework::graphics::scene_renderer & scene_renderer, ::framework::resource::resource_manager & resource_manager, ::framework::coroutine::static_thread_pool & thread_pool);

		void setup_scene(::framework::acp::scene_info const & scene_info) override;

		::framework::coroutine::lazy_task<::framework::acp::node *> create_node(::framework::acp::node_create_info const & node_create_info) override;

		void set_root_node(::framework::acp::node * root_node) override;

		::framework::coroutine::thread_pool_task<void> load_textures(::framework::coroutine::static_thread_pool & thread_pool);

		::framework::graphics::scene & get_graphics_scene() const noexcept;

		::framework::graphics::scene_renderer & get_scene_renderer() const noexcept;

		::framework::resource::resource_manager & get_resource_manager() const noexcept;

		::framework::coroutine::static_thread_pool & get_thread_pool() const noexcept;

		::framework::acp::ecs::scene::texture_load_state const & get_texture_load_state(::std::uint32_t texture_index) const noexcept;

		::framework::acp::ecs::node * const & get_root_node() const noexcept;

		::framework::acp::ecs::scene::texture_load_state const & get_default_image() const noexcept;

	private:
		::framework::graphics::scene & graphics_scene;
		::framework::graphics::scene_renderer & scene_renderer;
		::framework::resource::resource_manager & resource_manager;
		::framework::coroutine::static_thread_pool & thread_pool;
		::std::uint32_t texture_count;
		::std::unique_ptr<::framework::acp::ecs::scene::texture_load_state[]> textures;
		::framework::acp::ecs::scene::texture_load_state default_image;
		::framework::coroutine::thread_pool_task<void> load_textures_task;
		::framework::acp::ecs::node * root_node;
	};
}

#include "acp/ecs/scene.inl"