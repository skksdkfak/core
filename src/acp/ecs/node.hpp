#pragma once

#include "acp/node.hpp"
#include "graphics/material.hpp"
#include "graphics/medium.hpp"
#include "graphics/triangle_mesh_geometry.hpp"
#include "graphics/triangle_mesh_scene_geometry.hpp"
#include "coroutine/task.hpp"
#include "coroutine/static_thread_pool.hpp"
#include <forward_list>
#include <glm/glm.hpp>
#include <vector>

namespace framework::acp::ecs
{
	class scene;

	class node : public ::framework::acp::node
	{
	public:
		::framework::coroutine::lazy_task<void> initialize(::framework::acp::ecs::scene * scene, ::framework::acp::node_create_info const & node_create_info);

		void add_child(::framework::acp::node * node) override;

		::framework::graphics::triangle_mesh_scene_geometry * get_scene_geometry() const noexcept;

		::glm::mat4 const & get_transform() const noexcept;

		::std::forward_list<::framework::acp::ecs::node *> const & get_child_nodes() const noexcept;

	private:
		::glm::mat4 transform;
		::framework::graphics::triangle_mesh_geometry * geometry;
		::framework::graphics::triangle_mesh_scene_geometry * scene_geometry;
		::std::vector<::framework::acp::material_info> material_infos;
		::std::vector<::framework::graphics::material *> materials;
		::std::vector<::framework::graphics::medium *> media;
		::std::forward_list<::framework::acp::ecs::node *> nodes;
	};
}

#include "acp/ecs/node.inl"