inline ::framework::graphics::triangle_mesh_scene_geometry * ::framework::acp::ecs::node::get_scene_geometry() const noexcept
{
	return this->scene_geometry;
}

inline ::glm::mat4 const & (::framework::acp::ecs::node::get_transform)() const noexcept
{
	return this->transform;
}

inline ::std::forward_list<::framework::acp::ecs::node *> const & (::framework::acp::ecs::node::get_child_nodes)() const noexcept
{
	return this->nodes;
}