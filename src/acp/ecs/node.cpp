#include "acp/ecs/node.hpp"
#include "acp/ecs/scene.hpp"
#include <vector>

::framework::coroutine::lazy_task<void>(::framework::acp::ecs::node::initialize)(::framework::acp::ecs::scene * scene, ::framework::acp::node_create_info const & node_create_info)
{
	if (node_create_info.mesh_info)
	{
		::std::vector<::framework::graphics::submesh> submeshes(node_create_info.mesh_info->submesh_count);
		for (::std::uint32_t i = 0; i < node_create_info.mesh_info->submesh_count; i++)
		{
			submeshes[i] =
			{
				.index_offset = node_create_info.mesh_info->submeshes[i].index_offset,
				.vertex_offeset = node_create_info.mesh_info->submeshes[i].vertex_offeset,
				.index_count = node_create_info.mesh_info->submeshes[i].index_count
			};
		}

		this->material_infos.insert(this->material_infos.begin(), node_create_info.materials, node_create_info.materials + node_create_info.material_count);

		::framework::graphics::triangle_mesh_geometry_create_info triangle_mesh_geometry_create_info;
		triangle_mesh_geometry_create_info.vertex_count = node_create_info.mesh_info->vertex_count;
		triangle_mesh_geometry_create_info.positions = node_create_info.mesh_info->positions;
		triangle_mesh_geometry_create_info.normals = node_create_info.mesh_info->normals;
		triangle_mesh_geometry_create_info.binormals = node_create_info.mesh_info->binormals;
		triangle_mesh_geometry_create_info.tangents = node_create_info.mesh_info->tangents;
		triangle_mesh_geometry_create_info.uvs = node_create_info.mesh_info->uvs;
		triangle_mesh_geometry_create_info.index_count = node_create_info.mesh_info->index_count;
		triangle_mesh_geometry_create_info.indices = node_create_info.mesh_info->indices;
		triangle_mesh_geometry_create_info.submesh_count = node_create_info.mesh_info->submesh_count;
		triangle_mesh_geometry_create_info.submeshes = submeshes.data();
		this->geometry = scene->get_scene_renderer().create_triangle_mesh_geometry(triangle_mesh_geometry_create_info);
		co_await this->geometry->get_initialization_shared_task();

		this->materials.resize(this->material_infos.size());
		this->media.resize(this->material_infos.size());
		for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(this->material_infos.size()); i++)
		{
			::framework::graphics::material_create_info material_create_info;
			this->materials[i] = scene->get_scene_renderer().create_material(material_create_info);

			::framework::acp::ecs::scene::texture_load_state const & texture_load_state = this->material_infos[i].diffuse_texture != static_cast<::std::uint32_t>(-1) ?
				scene->get_texture_load_state(this->material_infos[i].diffuse_texture) :
				scene->get_default_image();
			co_await texture_load_state.async_manual_reset_event;
			::framework::graphics::image * diffuse_image;
			if (texture_load_state.image)
			{
				diffuse_image = texture_load_state.image;
			}
			else
			{
				co_await scene->get_default_image().async_manual_reset_event;
				diffuse_image = scene->get_default_image().image;
			}

			if (this->material_infos[i].emissive_texture != static_cast<::std::uint32_t>(-1))
			{
				::framework::acp::ecs::scene::texture_load_state const & emissive_texture_load_state = scene->get_texture_load_state(this->material_infos[i].emissive_texture);
				co_await emissive_texture_load_state.async_manual_reset_event;
				this->materials[i]->set_emissive_image(emissive_texture_load_state.image);
			}

			this->materials[i]->set_diffuse_texture(diffuse_image);
			if (this->material_infos[i].thin_surface)
			{
				this->media[i] = scene->get_scene_renderer().create_medium();
				this->materials[i]->set_thin_surface(true);
			}
		}

		::framework::graphics::triangle_mesh_scene_geometry_create_info triangle_mesh_scene_geometry_create_info;
		triangle_mesh_scene_geometry_create_info.triangle_mesh_geometry = this->geometry;
		triangle_mesh_scene_geometry_create_info.material_count = static_cast<::std::uint32_t>(this->material_infos.size());
		triangle_mesh_scene_geometry_create_info.materials = this->materials.data();
		triangle_mesh_scene_geometry_create_info.media = this->media.data();
		this->scene_geometry = scene->get_graphics_scene().create_triangle_mesh_scene_geometry(triangle_mesh_scene_geometry_create_info);
		co_await this->scene_geometry->get_initialization_shared_task();

		this->transform = node_create_info.transform;
		this->scene_geometry->set_transform(this->transform);
	}
}

void ::framework::acp::ecs::node::add_child(::framework::acp::node * node)
{
	this->nodes.push_front(static_cast<::framework::acp::ecs::node *>(node));
}
