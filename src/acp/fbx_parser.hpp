#pragma once

#include "acp/scene.hpp"
#include "coroutine/task.hpp"
#include "coroutine/static_thread_pool.hpp"
#include "memory/allocator.hpp"
#include <glm/glm.hpp>
#include <fbxsdk.h>

namespace framework::acp
{
	class fbx_parser;

	struct asset_load_Info
	{
		char const * file_name;
		::framework::coroutine::static_thread_pool * static_thread_pool;
		::framework::acp::scene * scene;
	};

	class fbx_mesh_cache
	{
	public:
		fbx_mesh_cache(::framework::acp::fbx_parser * fbx_parser, ::framework::acp::node_create_info & node_create_info, ::fbxsdk::FbxMesh * fbx_mesh);

		~fbx_mesh_cache();

	private:
		::framework::acp::fbx_parser * fbx_parser;
        ::framework::acp::mesh_info mesh_info;
		::fbxsdk::FbxArray<::framework::acp::submesh> submeshes;
	};

	class fbx_parser
	{
	public:
		fbx_parser(::framework::memory::allocator * allocator);

		~fbx_parser();

		void parse(::framework::acp::asset_load_Info * load_info);

		::framework::memory::allocator * get_allocator() const noexcept;

		::fbxsdk::FbxManager * get_fbx_sdk_manager() const noexcept;

	private:
		void parse_scene(::framework::acp::scene * scene, ::fbxsdk::FbxScene * fbx_scene);

		::framework::coroutine::immediate_task<::framework::acp::node *> parse_node(::fbxsdk::FbxNode * fbx_node);

		::fbxsdk::FbxManager * fbx_sdk_manager;
		::fbxsdk::FbxImporter * fbx_importer;
		::framework::memory::allocator * allocator;
		::framework::coroutine::static_thread_pool * static_thread_pool;
		::framework::acp::scene * scene;
	};
}

#include "acp/fbx_parser.inl"