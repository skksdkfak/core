#pragma once

#include "acp/node.hpp"
#include "coroutine/task.hpp"
#include <cstdint>

namespace framework::acp
{
	struct texture_info
	{
		char const *	texture_name;
	};

	struct scene_info
	{
        ::std::uint32_t							texture_count;
        ::framework::acp::texture_info const *	textures;
	};

	class scene
	{
	public:
		virtual void setup_scene(::framework::acp::scene_info const & scene_info) = 0;

		virtual ::framework::coroutine::lazy_task<::framework::acp::node *> create_node(::framework::acp::node_create_info const & node_create_info) = 0;

		virtual void set_root_node(::framework::acp::node * root_node) = 0;
	};
}