﻿#include "acp/fbx_parser.hpp"
#include "coroutine/schedule_on.hpp"
#include "coroutine/sync_wait.hpp"
#include "coroutine/thread_pool_task.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <memory>
#include <numbers>

::framework::acp::fbx_mesh_cache::fbx_mesh_cache(::framework::acp::fbx_parser * fbx_parser, ::framework::acp::node_create_info & node_create_info, ::fbxsdk::FbxMesh * fbx_mesh) :
	fbx_parser(fbx_parser)
{
	this->submeshes.Resize(1);

	::framework::memory::allocator * const allocator = this->fbx_parser->get_allocator();

	//::fbxsdk::FbxGeometryConverter fbx_geometry_converter(fbx_parser->get_fbx_sdk_manager());
	//fbx_geometry_converter.EmulateNormalsByPolygonVertex(fbx_mesh);

	int const polygon_count = fbx_mesh->GetPolygonCount();
	int const polygon_size = 3;

	::fbxsdk::FbxLayerElementArrayTemplate<int> * material_indice = NULL;
	::fbxsdk::FbxGeometryElement::EMappingMode material_mapping_mode = ::fbxsdk::FbxGeometryElement::eNone;
	if (fbx_mesh->GetElementMaterial())
	{
		material_indice = &fbx_mesh->GetElementMaterial()->GetIndexArray();
		material_mapping_mode = fbx_mesh->GetElementMaterial()->GetMappingMode();
		if (material_indice && material_mapping_mode == ::fbxsdk::FbxGeometryElement::eByPolygon)
		{
			FBX_ASSERT(material_indice->GetCount() == polygon_count);
			if (material_indice->GetCount() == polygon_count)
			{
				for (int lPolygonIndex = 0; lPolygonIndex < polygon_count; ++lPolygonIndex)
				{
					const int material_index = material_indice->GetAt(lPolygonIndex);
					if (this->submeshes.GetCount() <= material_index)
					{
						this->submeshes.Resize(material_index + 1);
					}
					this->submeshes[material_index].index_count += polygon_size;
				}

				const int material_count = this->submeshes.GetCount();
				::std::uint32_t index_offset = 0;
				for (int lIndex = 0; lIndex < material_count; ++lIndex)
				{
					this->submeshes[lIndex].index_offset = index_offset;
					index_offset += this->submeshes[lIndex].index_count;
					this->submeshes[lIndex].index_count = 0;
				}
				FBX_ASSERT(index_offset == polygon_count * polygon_size);
			}
		}
	}

	bool has_normal = fbx_mesh->GetElementNormalCount() > 0 || fbx_mesh->GenerateNormals();
	bool has_binormal = fbx_mesh->GetElementBinormalCount() > 0;
	bool has_tangent = fbx_mesh->GetElementTangentCount() > 0;
	bool has_uv = fbx_mesh->GetElementUVCount() > 0;
	bool is_all_by_control_point = true;
	::fbxsdk::FbxGeometryElement::EMappingMode normal_mapping_mode = ::fbxsdk::FbxGeometryElement::eNone;
	::fbxsdk::FbxGeometryElement::EMappingMode uv_mapping_mode = ::fbxsdk::FbxGeometryElement::eNone;
	if (has_normal)
	{
		normal_mapping_mode = fbx_mesh->GetElementNormal(0)->GetMappingMode();
		if (normal_mapping_mode == ::fbxsdk::FbxGeometryElement::eNone)
		{
			has_normal = false;
		}
		if (has_normal && normal_mapping_mode != ::fbxsdk::FbxGeometryElement::eByControlPoint)
		{
			is_all_by_control_point = false;
		}
	}
	if (has_uv)
	{
		uv_mapping_mode = fbx_mesh->GetElementUV(0)->GetMappingMode();
		if (uv_mapping_mode == ::fbxsdk::FbxGeometryElement::eNone)
		{
			has_uv = false;
		}
		if (has_uv && uv_mapping_mode != ::fbxsdk::FbxGeometryElement::eByControlPoint)
		{
			is_all_by_control_point = false;
		}
	}

	int const vertex_count = is_all_by_control_point ? fbx_mesh->GetControlPointsCount() : polygon_count * polygon_size;

	::std::uint32_t * indices = allocator->allocate<::std::uint32_t>(polygon_count * polygon_size);
	::glm::vec3 * vertices = allocator->allocate<::glm::vec3>(vertex_count);
	::glm::vec3 * normals = has_normal ? allocator->allocate<::glm::vec3>(vertex_count) : nullptr;
	::glm::vec3 * binormals = has_binormal ? allocator->allocate<::glm::vec3>(vertex_count) : nullptr;
	::glm::vec3 * tangents = has_tangent ? allocator->allocate<::glm::vec3>(vertex_count) : nullptr;
	::glm::vec2 * uvs = has_uv ? allocator->allocate<::glm::vec2>(vertex_count) : nullptr;

	::fbxsdk::FbxStringList uv_names;
	fbx_mesh->GetUVSetNames(uv_names);
	char const * uv_name = nullptr;
	if (has_uv && uv_names.GetCount())
	{
		uv_name = uv_names[0];
	}

	::fbxsdk::FbxVector4 const * control_points = fbx_mesh->GetControlPoints();
	if (is_all_by_control_point)
	{
		::fbxsdk::FbxGeometryElementNormal const * normal_element = fbx_mesh->GetElementNormal(0);
		::fbxsdk::FbxGeometryElementBinormal const * binormal_element = fbx_mesh->GetElementBinormal(0);
		::fbxsdk::FbxGeometryElementTangent const * tangent_element = fbx_mesh->GetElementTangent(0);
		::fbxsdk::FbxGeometryElementUV const * uv_element = fbx_mesh->GetElementUV(0);

		for (int vertex_index = 0; vertex_index < vertex_count; ++vertex_index)
		{
			::fbxsdk::FbxVector4 const current_vertex = control_points[vertex_index];
			vertices[vertex_index] = ::glm::vec3(static_cast<float>(current_vertex[0]), static_cast<float>(current_vertex[1]), static_cast<float>(current_vertex[2]));

			if (has_normal)
			{
				int normal_index = vertex_index;
				if (normal_element->GetReferenceMode() == ::fbxsdk::FbxLayerElement::eIndexToDirect)
				{
					normal_index = normal_element->GetIndexArray().GetAt(vertex_index);
				}
				::fbxsdk::FbxVector4 const current_normal = normal_element->GetDirectArray().GetAt(normal_index);
				normals[vertex_index] = ::glm::vec3(static_cast<float>(current_normal[0]), static_cast<float>(current_normal[1]), static_cast<float>(current_normal[2]));
			}

			if (has_binormal)
			{
				int binormal_index = vertex_index;
				if (binormal_element->GetReferenceMode() == ::fbxsdk::FbxLayerElement::eIndexToDirect)
				{
					binormal_index = binormal_element->GetIndexArray().GetAt(vertex_index);
				}
				::fbxsdk::FbxVector4 const current_binormal = binormal_element->GetDirectArray().GetAt(binormal_index);
				binormals[vertex_index] = ::glm::vec3(static_cast<float>(current_binormal[0]), static_cast<float>(current_binormal[1]), static_cast<float>(current_binormal[2]));
			}

			if (has_tangent)
			{
				int tangent_index = vertex_index;
				if (tangent_element->GetReferenceMode() == ::fbxsdk::FbxLayerElement::eIndexToDirect)
				{
					tangent_index = tangent_element->GetIndexArray().GetAt(vertex_index);
				}
				::fbxsdk::FbxVector4 const current_tangent = tangent_element->GetDirectArray().GetAt(tangent_index);
				tangents[vertex_index] = ::glm::vec3(static_cast<float>(current_tangent[0]), static_cast<float>(current_tangent[1]), static_cast<float>(current_tangent[2]));
			}

			if (has_uv)
			{
				int uv_index = vertex_index;
				if (uv_element->GetReferenceMode() == ::fbxsdk::FbxLayerElement::eIndexToDirect)
				{
					uv_index = uv_element->GetIndexArray().GetAt(vertex_index);
				}
				::fbxsdk::FbxVector2 const current_uv = uv_element->GetDirectArray().GetAt(uv_index);
				uvs[vertex_index] = ::glm::vec2(current_uv[0], current_uv[1]);
			}
		}

		for (int polygon_index = 0; polygon_index < polygon_count; polygon_index++)
		{
			int material_index = 0;
			if (material_indice && material_mapping_mode == ::fbxsdk::FbxGeometryElement::eByPolygon)
			{
				material_index = material_indice->GetAt(polygon_index);
			}

			int const index_offset = this->submeshes[material_index].index_offset + this->submeshes[material_index].index_count;
			for (int polygon_vertice_index = 0; polygon_vertice_index < polygon_size; polygon_vertice_index++)
			{
				indices[index_offset + polygon_vertice_index] = static_cast<::std::uint32_t>(fbx_mesh->GetPolygonVertex(polygon_index, polygon_vertice_index));
			}
			this->submeshes[material_index].index_count += polygon_size;
		}
	}
	else
	{
		::std::uint32_t vertex_index = 0;
		for (int polygon_index = 0; polygon_index < polygon_count; ++polygon_index)
		{
			int materia_index = 0;
			if (material_indice && material_mapping_mode == ::fbxsdk::FbxGeometryElement::eByPolygon)
			{
				materia_index = material_indice->GetAt(polygon_index);
			}

			::std::uint32_t const index_offset = this->submeshes[materia_index].index_offset + this->submeshes[materia_index].index_count;
			for (::std::uint32_t vertice_index = 0; vertice_index < polygon_size; ++vertice_index)
			{
				indices[index_offset + vertice_index] = vertex_index;

				::fbxsdk::FbxVector4 const current_vertex = control_points[fbx_mesh->GetPolygonVertex(polygon_index, vertice_index)];
				vertices[vertex_index] = ::glm::vec3(static_cast<float>(current_vertex[0]), static_cast<float>(current_vertex[1]), static_cast<float>(current_vertex[2]));

				::fbxsdk::FbxVector4 current_normal;
				fbx_mesh->GetPolygonVertexNormal(polygon_index, vertice_index, current_normal);
				normals[vertex_index] = ::glm::vec3(static_cast<float>(current_normal[0]), static_cast<float>(current_normal[1]), static_cast<float>(current_normal[2]));

				if (has_binormal || has_tangent)
				{
					::glm::vec3 tangent;
					::glm::vec3 c1 = ::glm::cross(normals[vertex_index], ::glm::vec3(0.0, 0.0, 1.0));
					::glm::vec3 c2 = ::glm::cross(normals[vertex_index], ::glm::vec3(0.0, 1.0, 0.0));
					tangent = (::glm::length(c1) > ::glm::length(c2)) ? c1 : c2;
					if (has_binormal)
						binormals[vertex_index] = ::glm::normalize(tangent);
					if (has_tangent)
						tangents[vertex_index] = ::glm::normalize(::glm::cross(normals[vertex_index], tangent));
				}

				if (has_uv)
				{
					::fbxsdk::FbxVector2 current_uv;
					bool unmapped_uv;
					fbx_mesh->GetPolygonVertexUV(polygon_index, vertice_index, uv_name, current_uv, unmapped_uv);
					uvs[vertex_index] = ::glm::vec2(static_cast<float>(current_uv[0]), static_cast<float>(current_uv[1]));
				}

				++vertex_index;
			}
			this->submeshes[materia_index].index_count += polygon_size;
		}
	}

	this->mesh_info.vertex_count = vertex_count;
	this->mesh_info.positions = vertices;
	this->mesh_info.normals = normals;
	this->mesh_info.binormals = binormals;
	this->mesh_info.tangents = tangents;
	this->mesh_info.uvs = uvs;
	this->mesh_info.index_count = polygon_count * polygon_size;
	this->mesh_info.indices = indices;
	this->mesh_info.submesh_count = static_cast<::std::uint32_t>(this->submeshes.Size());
	this->mesh_info.submeshes = this->submeshes.GetArray();

	node_create_info.mesh_info = &this->mesh_info;
}

::framework::acp::fbx_mesh_cache::~fbx_mesh_cache()
{
	::framework::memory::allocator * const allocator = this->fbx_parser->get_allocator();

	if (this->mesh_info.uvs)
		allocator->deallocate((void *)this->mesh_info.uvs);
	if (this->mesh_info.tangents)
		allocator->deallocate((void *)this->mesh_info.tangents);
	if (this->mesh_info.binormals)
		allocator->deallocate((void *)this->mesh_info.binormals);
	if (this->mesh_info.normals)
		allocator->deallocate((void *)this->mesh_info.normals);
	if (this->mesh_info.positions)
		allocator->deallocate((void *)this->mesh_info.positions);
	if (this->mesh_info.indices)
		allocator->deallocate((void *)this->mesh_info.indices);
}

::framework::acp::fbx_parser::fbx_parser(::framework::memory::allocator * allocator) :
	allocator(allocator)
{
	this->fbx_sdk_manager = ::fbxsdk::FbxManager::Create();

	::fbxsdk::FbxIOSettings * ios = ::fbxsdk::FbxIOSettings::Create(this->fbx_sdk_manager, IOSROOT);
	this->fbx_sdk_manager->SetIOSettings(ios);

	::fbxsdk::FbxString path = ::fbxsdk::FbxGetApplicationDirectory();
	this->fbx_sdk_manager->LoadPluginsDirectory(path.Buffer());
}

::framework::acp::fbx_parser::~fbx_parser()
{
	this->fbx_sdk_manager->Destroy();
}

void ::framework::acp::fbx_parser::parse(::framework::acp::asset_load_Info * load_info)
{
	this->static_thread_pool = load_info->static_thread_pool;
	this->scene = load_info->scene;

	::fbxsdk::FbxScene * fbx_scene = ::fbxsdk::FbxScene::Create(this->fbx_sdk_manager, "My Scene");

	int file_format = -1;
	this->fbx_importer = ::fbxsdk::FbxImporter::Create(this->fbx_sdk_manager, "");
	if (!this->fbx_sdk_manager->GetIOPluginRegistry()->DetectReaderFileFormat(load_info->file_name, file_format))
	{
		file_format = this->fbx_sdk_manager->GetIOPluginRegistry()->FindReaderIDByDescription("FBX binary (*.fbx)");
	}

	if (this->fbx_importer->Initialize(load_info->file_name, file_format) && this->fbx_importer->Import(fbx_scene))
	{
		::fbxsdk::FbxAxisSystem scene_axis_system = fbx_scene->GetGlobalSettings().GetAxisSystem();
		::fbxsdk::FbxAxisSystem our_axis_system(::fbxsdk::FbxAxisSystem::eYAxis, ::fbxsdk::FbxAxisSystem::eParityOdd, ::fbxsdk::FbxAxisSystem::eRightHanded);
		if (scene_axis_system != our_axis_system)
		{
			our_axis_system.ConvertScene(fbx_scene);
		}

		::fbxsdk::FbxSystemUnit SceneSystemUnit = fbx_scene->GetGlobalSettings().GetSystemUnit();
		if (SceneSystemUnit.GetScaleFactor() != 1.0)
		{
			::fbxsdk::FbxSystemUnit::cm.ConvertScene(fbx_scene);
		}

		::fbxsdk::FbxGeometryConverter fbx_geometry_converter(this->fbx_sdk_manager);
		fbx_geometry_converter.Triangulate(fbx_scene, true);

		this->parse_scene(this->scene, fbx_scene);
	}

	::fbxsdk::FbxVector4 bbox_min, bbox_max, bbox_center;
	if (fbx_scene->ComputeBoundingBoxMinMaxCenter(bbox_min, bbox_max, bbox_center))
	{
		printf("bbox_min(%f, %f, %f) bbox_max(%f, %f, %f) bbox_center(%f, %f, %f)", bbox_min[0], bbox_min[1], bbox_min[2], bbox_max[0], bbox_max[1], bbox_max[2], bbox_center[0], bbox_center[1], bbox_center[2]);
	}

	this->fbx_importer->Destroy();
}

void ::framework::acp::fbx_parser::parse_scene(::framework::acp::scene * scene, ::fbxsdk::FbxScene * fbx_scene)
{
	::framework::acp::scene_info scene_info{};

	::std::unique_ptr<::framework::acp::texture_info[]> textures;
	::std::unique_ptr<::fbxsdk::FbxString[]> texture_names;

	scene_info.texture_count = static_cast<::std::uint32_t>(fbx_scene->GetTextureCount());
	if (scene_info.texture_count)
	{
		textures = ::std::make_unique<::framework::acp::texture_info[]>(scene_info.texture_count);
		texture_names = ::std::make_unique<::fbxsdk::FbxString[]>(scene_info.texture_count);

		for (int texture_index = 0; texture_index < fbx_scene->GetTextureCount(); texture_index++)
		{
			::fbxsdk::FbxTexture * image = fbx_scene->GetTexture(texture_index);
			::fbxsdk::FbxFileTexture * fbx_file_texture = ::fbxsdk::FbxCast<::fbxsdk::FbxFileTexture>(image);
			if (fbx_file_texture)
			{
				::std::uintptr_t const texture_index_data = static_cast<::std::uintptr_t>(texture_index);
				fbx_file_texture->SetUserDataPtr(reinterpret_cast<void *>(texture_index_data));

				texture_names[texture_index] = ::fbxsdk::FbxPathUtils::GetFileName(fbx_file_texture->GetFileName());

				textures[texture_index].texture_name = texture_names[texture_index].Buffer();
			}
		}
	}

	scene_info.textures = textures.get();
	scene->setup_scene(scene_info);
	scene->set_root_node(::framework::coroutine::sync_wait(this->parse_node(fbx_scene->GetRootNode())));
}

::framework::coroutine::immediate_task<::framework::acp::node *>(::framework::acp::fbx_parser::parse_node)(::fbxsdk::FbxNode * fbx_node)
{
	::framework::acp::node_create_info node_create_info{};

	::fbxsdk::FbxAMatrix const global_transform = fbx_node->EvaluateGlobalTransform();
	::fbxsdk::FbxVector4 const translation = global_transform.GetT();
	::fbxsdk::FbxVector4 const rotation = global_transform.GetR() * (::std::numbers::pi / 180.0f);
	::fbxsdk::FbxVector4 const scale = global_transform.GetS();

	node_create_info.transform = ::glm::transpose(::glm::translate(::glm::mat4(1.0f), ::glm::vec3(translation.mData[0], translation.mData[1], translation.mData[2])) * ::glm::mat4_cast(::glm::quat(::glm::vec3(rotation.mData[0], rotation.mData[1], rotation.mData[2]))) * ::glm::scale(::glm::mat4(1.0f), ::glm::vec3(scale.mData[0], scale.mData[1], scale.mData[2])));
	node_create_info.material_count = static_cast<::std::uint32_t>(fbx_node->GetMaterialCount());

	::std::unique_ptr<::framework::acp::material_info[]> materials;
	if (node_create_info.material_count)
	{
		materials = ::std::make_unique<::framework::acp::material_info[]>(node_create_info.material_count);
		int const material_count = fbx_node->GetMaterialCount();
		for (int material_index = 0; material_index < material_count; ++material_index)
		{
			materials[material_index].diffuse_texture = -1;
			materials[material_index].emissive_texture = -1;
			materials[material_index].thin_surface = false;

			char const dragon_name[] = "default";
			auto name = fbx_node->GetName();
			if (::std::memcmp(name, dragon_name, sizeof(dragon_name)) == 0)
			{
				materials[material_index].thin_surface = true;
			}

			::fbxsdk::FbxSurfaceMaterial * fbx_surface_material = fbx_node->GetMaterial(material_index);

			::fbxsdk::FbxProperty fbx_property_specular = fbx_surface_material->FindProperty(fbx_surface_material->sSpecular);
			if (fbx_property_specular.IsValid())
			{
				::fbxsdk::FbxFileTexture const * fbx_file_texture = fbx_property_specular.GetSrcObject<::fbxsdk::FbxFileTexture>();
				if (fbx_file_texture)
				{
					//__debugbreak();
					//materials[material_index].diffuse_texture = static_cast<::std::uint32_t>(reinterpret_cast<::std::uintptr_t>(fbx_file_texture->GetUserDataPtr()));
				}
				for (int src_property_index = 0; src_property_index < fbx_property_specular.GetSrcPropertyCount(); ++src_property_index)
				{
					::fbxsdk::FbxProperty specular_src_property = fbx_property_specular.GetSrcProperty(src_property_index);
					auto t0 = specular_src_property.GetPropertyDataType();
					printf("fbx_property_emissive has src propery, but did not handled.");
				}
			}

			::fbxsdk::FbxProperty fbx_property_specular_factor = fbx_surface_material->FindProperty(fbx_surface_material->sSpecularFactor);
			if (fbx_property_specular_factor.IsValid())
			{
				::fbxsdk::FbxFileTexture const * fbx_file_texture = fbx_property_specular_factor.GetSrcObject<::fbxsdk::FbxFileTexture>();
				if (fbx_file_texture)
				{
					__debugbreak();
					//materials[material_index].diffuse_texture = static_cast<::std::uint32_t>(reinterpret_cast<::std::uintptr_t>(fbx_file_texture->GetUserDataPtr()));
				}
			}

			::fbxsdk::FbxProperty fbx_property_shininess = fbx_surface_material->FindProperty(fbx_surface_material->sShininess);
			if (fbx_property_shininess.IsValid())
			{
				::fbxsdk::FbxFileTexture const * fbx_file_texture = fbx_property_shininess.GetSrcObject<::fbxsdk::FbxFileTexture>();
				if (fbx_file_texture)
				{
					__debugbreak();
					//materials[material_index].diffuse_texture = static_cast<::std::uint32_t>(reinterpret_cast<::std::uintptr_t>(fbx_file_texture->GetUserDataPtr()));
				}
			}

			::fbxsdk::FbxProperty fbx_property_diffuse = fbx_surface_material->FindProperty(fbx_surface_material->sDiffuse);
			if (fbx_property_diffuse.IsValid())
			{
				::fbxsdk::FbxFileTexture const * fbx_file_texture = fbx_property_diffuse.GetSrcObject<::fbxsdk::FbxFileTexture>();
				if (fbx_file_texture)
				{
					materials[material_index].diffuse_texture = static_cast<::std::uint32_t>(reinterpret_cast<::std::uintptr_t>(fbx_file_texture->GetUserDataPtr()));
				}
			}

			::fbxsdk::FbxProperty const fbx_property_emissive = fbx_surface_material->FindProperty(fbx_surface_material->sEmissive);
			if (fbx_property_emissive.IsValid())
			{
				for (int src_object_index = 0; src_object_index < fbx_property_emissive.GetSrcObjectCount(); ++src_object_index)
				{
					::fbxsdk::FbxFileTexture const * fbx_file_texture = fbx_property_emissive.GetSrcObject<::fbxsdk::FbxFileTexture>(src_object_index);
					materials[material_index].emissive_texture = static_cast<::std::uint32_t>(reinterpret_cast<::std::uintptr_t>(fbx_file_texture->GetUserDataPtr()));
				}
			}
		}
	}

	node_create_info.materials = materials.get();

	::std::unique_ptr<::framework::acp::fbx_mesh_cache> fbx_mesh_cache;
	::fbxsdk::FbxNodeAttribute * const node_attribute = fbx_node->GetNodeAttribute();
	if (node_attribute)
	{
		switch (node_attribute->GetAttributeType())
		{
		case ::fbxsdk::FbxNodeAttribute::eMesh:
			fbx_mesh_cache = ::std::make_unique<::framework::acp::fbx_mesh_cache>(this, node_create_info, fbx_node->GetMesh());
			break;
		case ::fbxsdk::FbxNodeAttribute::eLight:
			break;
		}
	}

	int const child_count = fbx_node->GetChildCount();
	::std::vector<::framework::acp::node *> child_nodes(child_count);
	for (int child_index = 0; child_index < child_count; child_index++)
	{
		::framework::acp::node * child_node = co_await this->parse_node(fbx_node->GetChild(child_index));
		child_nodes.push_back(child_node);
	}

	::framework::acp::node * node = co_await ::framework::coroutine::schedule_on(*this->static_thread_pool, this->scene->create_node(node_create_info));
	for (::framework::acp::node * child_node : child_nodes)
	{
		node->add_child(child_node);
	}

	co_return node;
}