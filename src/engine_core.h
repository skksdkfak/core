#pragma once

#include "platform/connection.hpp"
#include "input/my_input_listner.hpp"

namespace framework
{
	class application
	{
	public:
		virtual ~application() = default;

		virtual void startup(void) = 0;

		virtual void cleanup(void) = 0;

		virtual bool is_done(void);

		virtual void update(float deltaT) = 0;
	};

	class engine_core
	{
	public:
		static void run_application(::framework::application * application, char const * class_name);

		static void initialize();

		static void terminate_application();

		static ::framework::platform::connection_properties connection_properties;
		static ::framework::application * application;
		static ::framework::platform::connection * connection;
		static ::framework::input::my_input_listner * input_listner;
	};
}

#if defined(_WIN32)
#define CREATE_APPLICATION( app_class )																\
	int wmain(int argc, wchar_t** argv)																\
	{																								\
		::framework::engine_core::run_application(new app_class(), #app_class);						\
		return 0;																					\
	}
#elif defined(__linux__)
#define CREATE_APPLICATION( app_class )																\
	int main(const int argc, char const * argv[])												    \
	{																								\
		::framework::engine_core::run_application(new app_class(), #app_class);						\
		return 0;																					\
	}
#elif defined(__ANDROID__)
#define CREATE_APPLICATION( app_class )																\
	void android_main(android_app * state)															\
	{																								\
		::framework::EngineCore::run_application(new app_class(), #app_class);						\
	}
#endif