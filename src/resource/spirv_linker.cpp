#include "resource/spirv_linker.hpp"
#include <spirv-tools/linker.hpp>
#include <cstring>

::framework::resource::spirv_linker::spirv_linker(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
}

::framework::coroutine::lazy_task<void>(::framework::resource::spirv_linker::link)(::framework::resource::shader_link_info const & shader_link_info, ::framework::resource::shader_link_callback & shader_link_callback)
{
	::std::vector<::std::uint32_t const *> spirv_binaries(shader_link_info.shader_info_count);
	::std::vector<::std::size_t> spirv_binary_sizes(shader_link_info.shader_info_count);
	::std::vector<::std::uint32_t> spirv_linked_binary;

	for (::std::uint32_t i = 0; i < shader_link_info.shader_info_count; i++)
	{
		spirv_binaries[i] = static_cast<::std::uint32_t const *>(shader_link_info.shader_infos[i].code);
		spirv_binary_sizes[i] = shader_link_info.shader_infos[i].code_size / sizeof(::std::uint32_t);
	}

	::spvtools::Context context(::spv_target_env::SPV_ENV_VULKAN_1_3);
	::spvtools::LinkerOptions linker_options;
	linker_options.SetCreateLibrary(true);

	::spv_result_t spv_result = ::spvtools::Link(context, spirv_binaries.data(), spirv_binary_sizes.data(), spirv_binary_sizes.size(), &spirv_linked_binary, linker_options);
	if (spv_result != ::spv_result_t::SPV_SUCCESS)
	{
		throw ::std::exception();
	}

	::framework::resource::shader_link_callback::shader_link_result shader_link_result;
	shader_link_result.code_size = spirv_linked_binary.size() * sizeof(::std::uint32_t);
	shader_link_result.code = spirv_linked_binary.data();
	auto shader_link_callback_task = shader_link_callback(shader_link_result);
	co_await shader_link_callback_task;

	co_return;
}

void ::framework::resource::spirv_linker::get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages)
{
	::framework::resource::shader_language const supported_source_shader_languages[]
	{
		::framework::resource::shader_language::spirv_1_5
	};

	if (source_shader_languages)
	{
		::std::memcpy(source_shader_languages, supported_source_shader_languages, ::std::min(static_cast<::std::size_t>(source_shader_language_count), ::std::size(supported_source_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		source_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_source_shader_languages));
	}
}

void ::framework::resource::spirv_linker::get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages)
{
	::framework::resource::shader_language const supported_target_shader_languages[]
	{
		::framework::resource::shader_language::spirv_1_5
	};

	if (target_shader_languages)
	{
		::std::memcpy(target_shader_languages, supported_target_shader_languages, ::std::min(static_cast<::std::size_t>(target_shader_language_count), ::std::size(supported_target_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		target_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_languages));
	}
}

void ::framework::resource::spirv_linker::get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments)
{
	::framework::resource::shader_environment const supported_target_shader_environments[]
	{
		::framework::resource::shader_environment::vulkan_1_3
	};

	if (target_shader_environments)
	{
		::std::memcpy(target_shader_environments, supported_target_shader_environments, ::std::min(static_cast<::std::size_t>(target_shader_environment_count), ::std::size(supported_target_shader_environments)) * sizeof(::framework::resource::shader_environment));
	}
	else
	{
		target_shader_environment_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_environments));
	}
}