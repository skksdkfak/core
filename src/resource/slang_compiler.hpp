#pragma once

#include "resource/shader_compiler.hpp"
#include <slang/slang.h>
#include <slang/slang-com-ptr.h>

namespace framework::resource
{
	class resource_manager;
	class slang_compiler;

	class slang_program_reflection : public ::framework::resource::program_reflection
	{
	public:
	};

	class slang_program : public ::framework::resource::program
	{
	public:
		slang_program(::framework::resource::slang_compiler & slang_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info);

		::std::size_t get_code_size() const override;

		void const * get_code() const override;

		::framework::resource::program_reflection * create_program_reflection() const override;

	private:
		::Slang::ComPtr<::slang::IComponentType> program;
		::Slang::ComPtr<::slang::IBlob> code_blob;
	};

	class slang_compiler : public ::framework::resource::shader_compiler
	{
	public:
		slang_compiler(::framework::resource::resource_manager const & resource_manager);

		::framework::coroutine::lazy_task<::framework::resource::program *> compile(::framework::resource::shader_compilation_info const & shader_compilation_info) override;

		void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) override;

		void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) override;

		void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) override;

	private:
		friend class ::framework::resource::slang_program;

		::framework::resource::resource_manager const & resource_manager;
		::Slang::ComPtr<::slang::IGlobalSession> slang_global_session;
	};
}