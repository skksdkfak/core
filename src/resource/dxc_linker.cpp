#include "resource/dxc_linker.hpp"
#include "resource/resource_manager.hpp"
#include <string>
#include <vector>
#include <format>

::framework::resource::dxc_linker::dxc_linker(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
	::HRESULT hresult;
	hresult = ::DxcCreateInstance(::CLSID_DxcUtils, IID_PPV_ARGS(&this->dxc_utils));
}

::framework::coroutine::lazy_task<void>(::framework::resource::dxc_linker::link)(::framework::resource::shader_link_info const & shader_link_info, ::framework::resource::shader_link_callback & shader_link_callback)
{
	::HRESULT hresult;

	::CComPtr<::IDxcLinker> dxc_linker;
	hresult = ::DxcCreateInstance(::CLSID_DxcLinker, IID_PPV_ARGS(&dxc_linker));

	struct library_compilation_result
	{
		::CComPtr<::IDxcBlobEncoding> dxil_library;
		::std::wstring library_name;
	};

	::std::vector<struct library_compilation_result> library_compilation_results(shader_link_info.shader_info_count);
	::std::vector<::LPCWSTR> library_names(shader_link_info.shader_info_count);

	for (::std::uint32_t i = 0; i < shader_link_info.shader_info_count; i++)
	{
		struct library_compilation_result & library_compilation_result = library_compilation_results[i];

		hresult = this->dxc_utils->CreateBlobFromPinned(shader_link_info.shader_infos[i].code, shader_link_info.shader_infos[i].code_size, CP_ACP, &library_compilation_result.dxil_library);

		library_compilation_result.library_name = ::std::format(L"library_{}", i);
		library_names[i] = library_compilation_result.library_name.c_str();

		hresult = dxc_linker->RegisterLibrary(library_compilation_result.library_name.c_str(), library_compilation_result.dxil_library);
	}

	::std::vector<::LPCWSTR> arguments;
	::CComPtr<::IDxcOperationResult> dxc_link_result;

	hresult = dxc_linker->Link(L"", L"lib_6_6", library_names.data(), static_cast<::UINT32>(library_names.size()), arguments.data(), static_cast<::UINT32>(arguments.size()), &dxc_link_result);

	/*hresult = */dxc_link_result->GetStatus(&hresult);

	if (FAILED(hresult))
	{
		if (dxc_link_result)
		{
			::CComPtr<::IDxcBlobEncoding> errors_blob;
			hresult = dxc_link_result->GetErrorBuffer(&errors_blob);

			if (errors_blob)
			{
				::OutputDebugStringA(static_cast<::LPCSTR>(errors_blob->GetBufferPointer()));
			}
		}
	}

	::CComPtr<::IDxcBlob> dxil_library;
	hresult = dxc_link_result->GetResult(&dxil_library);

	::framework::resource::shader_link_callback::shader_link_result shader_link_result;
	shader_link_result.code_size = dxil_library->GetBufferSize();
	shader_link_result.code = dxil_library->GetBufferPointer();
	auto shader_link_callback_task = shader_link_callback(shader_link_result);
	co_await shader_link_callback_task;

	co_return;
}

void ::framework::resource::dxc_linker::get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages)
{
	::framework::resource::shader_language const supported_source_shader_languages[]
	{
		::framework::resource::shader_language::dxil
	};

	if (source_shader_languages)
	{
		::std::memcpy(source_shader_languages, supported_source_shader_languages, ::std::min(static_cast<::std::size_t>(source_shader_language_count), ::std::size(supported_source_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		source_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_source_shader_languages));
	}
}

void ::framework::resource::dxc_linker::get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages)
{
	::framework::resource::shader_language const supported_target_shader_languages[]
	{
		::framework::resource::shader_language::dxil
	};

	if (target_shader_languages)
	{
		::std::memcpy(target_shader_languages, supported_target_shader_languages, ::std::min(static_cast<::std::size_t>(target_shader_language_count), ::std::size(supported_target_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		target_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_languages));
	}
}

void ::framework::resource::dxc_linker::get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments)
{
	::framework::resource::shader_environment const supported_target_shader_environments[]
	{
		::framework::resource::shader_environment::d3d12
	};

	if (target_shader_environments)
	{
		::std::memcpy(target_shader_environments, supported_target_shader_environments, ::std::min(static_cast<::std::size_t>(target_shader_environment_count), ::std::size(supported_target_shader_environments)) * sizeof(::framework::resource::shader_environment));
	}
	else
	{
		target_shader_environment_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_environments));
	}
}