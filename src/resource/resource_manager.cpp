#include "resource_manager.hpp"
#include "acp/fbx_parser.hpp"
#include "memory/std_allocator.hpp"
#include "gpu/core.hpp"
#include "gpu/utility.hpp"
#include "gpu_log.hpp"
#include "resource/dxc_compiler.hpp"
#include "resource/glslang_compiler.hpp"
#include "resource/slang_compiler.hpp"
#include "resource/dxc_linker.hpp"
#include "resource/spirv_linker.hpp"
#include "resource/dds_texture_loader.hpp"
#include "resource/stbi_texture_loader.hpp"
#include "resource/open_exr_texture_loader.hpp"
#include "coroutine/sync_wait.hpp"
#include <iostream>
#include <fstream>
#include <forward_list>
#include <sstream>
#include <limits>
#include <format>
#include <set>

::framework::memory::std_allocator g_std_allocator;

namespace framework::local
{
	namespace
	{
		::std::filesystem::path const shader_cache_directory = "shader_cache";
	}
}

::framework::resource::resource_manager::resource_manager(::framework::resource::resource_manager_create_info const & create_info) :
	target_shader_language(create_info.shader_language),
	target_shader_environment(create_info.shader_environment),
	cache_shader_module_refs(true)
{
	this->frontend_shader_language_preferences.assign(create_info.frontend_shader_language_preferences, create_info.frontend_shader_language_preferences + create_info.frontend_shader_language_preference_count);

	this->shader_compilers.push_back(::std::make_unique<::framework::resource::slang_compiler>(*this));
	this->shader_compilers.push_back(::std::make_unique<::framework::resource::dxc_compiler>(*this));
	this->shader_compilers.push_back(::std::make_unique<::framework::resource::glslang_compiler>(*this));

	this->shader_linkers.push_back(::std::make_unique<::framework::resource::dxc_linker>(*this));
	this->shader_linkers.push_back(::std::make_unique<::framework::resource::spirv_linker>(*this));

	this->texture_loaders.push_back(::std::make_unique<::framework::resource::dds_texture_loader>(*this));
	this->texture_loaders.push_back(::std::make_unique<::framework::resource::open_exr_texture_loader>(*this));
	this->texture_loaders.push_back(::std::make_unique<::framework::resource::stbi_texture_loader>(*this));

	::std::filesystem::create_directories(::framework::local::shader_cache_directory);
}

::framework::resource::resource_manager::~resource_manager()
{
}

void ::framework::resource::resource_manager::add_search_directory(char const * directory)
{
	for (unsigned int i = 0; i < this->directories.size(); i++)
	{
		if (this->directories[i] == directory)
		{
			return;
		}
	}

	this->directories.push_back(directory);
}

bool ::framework::resource::resource_manager::find_file(::std::filesystem::path & file_path, ::std::filesystem::path file_name) const
{
	for (::std::size_t i = 0; i < this->directories.size(); i++)
	{
		file_path = this->directories[i];
		file_path /= file_name;

		if (::std::filesystem::exists(file_path))
		{
			return true;
		}
	}

	return false;
}

void ::framework::resource::resource_manager::load_fbx_scene(::framework::acp::scene * scene, char const * file_name, ::framework::coroutine::static_thread_pool * static_thread_pool, bool use_custom_path)
{
	::std::filesystem::path file_path;
	if (use_custom_path)
	{
		file_path = file_name;
	}
	else
	{
		if (!this->find_file(file_path, file_name))
		{
			return;
		}
	}

	if (!this->fbx_parser) // todo: lazy initialization due to crash on before main initialization
	{
		this->fbx_parser = ::std::make_unique<::framework::acp::fbx_parser>(&g_std_allocator);
	}

	::std::string const & file_path_string = file_path.string();

	::framework::acp::asset_load_Info asset_load_Info;
	asset_load_Info.file_name = file_path_string.c_str();
	asset_load_Info.scene = scene;
	asset_load_Info.static_thread_pool = static_thread_pool;
	this->fbx_parser->parse(&asset_load_Info);
}

::framework::gpu::shader_module * ::framework::resource::resource_manager::load_shader_module(::framework::gpu::device * device, ::framework::resource::shader_module_info const & shader_module_info)
{
	struct shader_stage_cached_info
	{
		::std::filesystem::path path;
		char const * source_entry_point;
		::framework::resource::shader_language source_shader_language;
		struct compilation_pipeline_stage
		{
			::framework::resource::shader_compiler * shader_compiler;
			::framework::resource::shader_language target_shader_language;
		};
		::std::vector<struct shader_stage_cached_info::compilation_pipeline_stage> compilation_pipeline;
	};

	::std::filesystem::file_time_type last_write_time;
	::std::vector<struct shader_stage_cached_info> shader_stage_cached_infos(shader_module_info.shader_stage_info_count);
	::std::vector<::framework::resource::shader_language> source_shader_languages;
	::std::vector<::framework::resource::shader_language> target_shader_languages;
	::std::vector<::framework::resource::shader_environment> target_shader_environments;
	::std::size_t hash = ::std::hash<::framework::resource::shader_language>()(this->target_shader_language);
	hash ^= ::std::hash<::framework::resource::shader_environment>()(this->target_shader_environment) + 0x9e3779b9 + (hash << 6) + (hash >> 2);

	for (::std::uint32_t i = 0; i < shader_module_info.shader_stage_info_count; i++)
	{
		char const * file_name = nullptr;
		char const * opted_source_entry_point;
		::framework::resource::shader_compiler * opted_shader_compiler;
		::framework::resource::shader_language opted_source_shader_language;
		::std::size_t preferable_frontend_shader_language = ::std::numeric_limits<::std::size_t>::max();

		for (auto & shader_compiler : this->shader_compilers)
		{
			::std::uint32_t qurey_count;

			shader_compiler->get_source_shader_languages(qurey_count, nullptr);
			source_shader_languages.resize(qurey_count);
			shader_compiler->get_source_shader_languages(qurey_count, source_shader_languages.data());

			shader_compiler->get_target_shader_languages(qurey_count, nullptr);
			target_shader_languages.resize(qurey_count);
			shader_compiler->get_target_shader_languages(qurey_count, target_shader_languages.data());

			shader_compiler->get_target_shader_environments(qurey_count, nullptr);
			target_shader_environments.resize(qurey_count);
			shader_compiler->get_target_shader_environments(qurey_count, target_shader_environments.data());

			if (::std::find(target_shader_languages.begin(), target_shader_languages.end(), this->target_shader_language) == target_shader_languages.end() ||
				::std::find(target_shader_environments.begin(), target_shader_environments.end(), this->target_shader_environment) == target_shader_environments.end())
			{
				continue;
			}

			auto search_preferable_frontend_shader_language = [&](::framework::resource::shader_language search_shader_language, char const * search_file_name, char const * source_entry_point) -> bool
			{
				if (search_file_name && ::std::find(source_shader_languages.begin(), source_shader_languages.end(), search_shader_language) != source_shader_languages.end())
				{
					::std::size_t current_frontend_shader_language_precedence;
					auto frontend_shader_language_preferences_iterator = ::std::find(this->frontend_shader_language_preferences.begin(), this->frontend_shader_language_preferences.end(), search_shader_language);
					if (frontend_shader_language_preferences_iterator != this->frontend_shader_language_preferences.end())
					{
						current_frontend_shader_language_precedence = frontend_shader_language_preferences_iterator - this->frontend_shader_language_preferences.begin();
					}
					else
					{
						current_frontend_shader_language_precedence = ::std::numeric_limits<::std::size_t>::max();
					}

					if (current_frontend_shader_language_precedence <= preferable_frontend_shader_language)
					{
						preferable_frontend_shader_language = current_frontend_shader_language_precedence;
						opted_shader_compiler = shader_compiler.get();
						opted_source_shader_language = search_shader_language;
						opted_source_entry_point = source_entry_point;
						file_name = search_file_name;

						if (current_frontend_shader_language_precedence == 0)
						{
							return true;
						}
					}
				}

				return false;
			};

			if (shader_module_info.shader_stage_infos[i].slang_source_info && search_preferable_frontend_shader_language(::framework::resource::shader_language::slang, shader_module_info.shader_stage_infos[i].slang_source_info->file_name, nullptr) ||
				shader_module_info.shader_stage_infos[i].glsl_source_info && search_preferable_frontend_shader_language(::framework::resource::shader_language::glsl, shader_module_info.shader_stage_infos[i].glsl_source_info->file_name, shader_module_info.shader_stage_infos[i].glsl_source_info->source_entry_point) ||
				shader_module_info.shader_stage_infos[i].hlsl_source_info && search_preferable_frontend_shader_language(::framework::resource::shader_language::hlsl, shader_module_info.shader_stage_infos[i].hlsl_source_info->file_name, shader_module_info.shader_stage_infos[i].hlsl_source_info->source_entry_point))
				break;
		}

		if (!file_name || !this->find_file(shader_stage_cached_infos[i].path, file_name))
		{
			throw;
		}

		shader_stage_cached_infos[i].source_entry_point = opted_source_entry_point;
		shader_stage_cached_infos[i].source_shader_language = opted_source_shader_language;

		if (this->target_shader_environment == ::framework::resource::shader_environment::d3d12 &&
			opted_shader_compiler == this->shader_compilers[0].get() &&
			opted_source_shader_language == ::framework::resource::shader_language::slang &&
			shader_module_info.shader_stage_infos[i].stage == ::framework::gpu::shader_stage_flags::compute_bit) // workaround for compute_pipeline creation issue 
		{
			shader_stage_cached_infos[i].compilation_pipeline.resize(2);
			shader_stage_cached_infos[i].compilation_pipeline[0] = { opted_shader_compiler, ::framework::resource::shader_language::hlsl };
			shader_stage_cached_infos[i].compilation_pipeline[1] = { this->shader_compilers[1].get(), this->target_shader_language };
		}
		else
		{
			shader_stage_cached_infos[i].compilation_pipeline.resize(1);
			shader_stage_cached_infos[i].compilation_pipeline[0] = { opted_shader_compiler, this->target_shader_language };
		}

		::std::error_code error_code;
		::std::filesystem::file_time_type current_last_write_time = ::std::filesystem::last_write_time(shader_stage_cached_infos[i].path, error_code);
		if (current_last_write_time > last_write_time)
		{
			last_write_time = current_last_write_time;
		}

		// Hash combine: https://stackoverflow.com/a/2595226
		hash ^= ::std::hash<::std::string>()(file_name) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		hash ^= ::std::hash<::std::string>()(shader_module_info.shader_stage_infos[i].entry_point) + 0x9e3779b9 + (hash << 6) + (hash >> 2);

		for (::std::uint32_t j = 0; j < shader_module_info.shader_stage_infos[i].preprocessor_define_count; j++)
		{
			hash ^= ::std::hash<::std::string>()(shader_module_info.shader_stage_infos[i].preprocessor_defines[j].name) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
			hash ^= ::std::hash<::std::string>()(shader_module_info.shader_stage_infos[i].preprocessor_defines[j].value) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		}

		for (::std::uint32_t j = 0; j < shader_module_info.shader_stage_infos[i].include_override_count; j++)
		{
			hash ^= ::std::hash<::std::filesystem::path>()(shader_module_info.shader_stage_infos[i].include_overrides[j].src) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
			hash ^= ::std::hash<::std::filesystem::path>()(shader_module_info.shader_stage_infos[i].include_overrides[j].dst) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		}
	}

	::std::filesystem::path shader_cache_path = ::framework::local::shader_cache_directory / ::std::format("{:x}", hash);
	::std::filesystem::path refs_path = ::std::filesystem::path(shader_cache_path).replace_extension(".refs");

	::std::error_code shader_cache_error_code;
	::std::filesystem::file_time_type const shader_cache_last_write_time = ::std::filesystem::last_write_time(shader_cache_path, shader_cache_error_code);

	if (shader_cache_last_write_time >= last_write_time)
	{
		::std::wifstream wifstream(refs_path, ::std::ios::binary);
		bool valid_cache = !this->cache_shader_module_refs || wifstream.is_open();
		if (wifstream.is_open())
		{
			::std::filesystem::path include_path;
			wifstream >> include_path;
			while (!wifstream.eof())
			{
				::std::error_code error_code;
				if (shader_cache_last_write_time < ::std::filesystem::last_write_time(include_path, error_code))
				{
					valid_cache = false;
					break;
				}

				wifstream >> include_path;
			}
		}

		if (valid_cache)
		{
			::std::ifstream ifstream(shader_cache_path, ::std::ios::binary);

			if (ifstream.is_open())
			{
				::std::vector<char> shader_code_data(::std::istreambuf_iterator<char>(ifstream), {});

				::framework::gpu::shader_module * shader_module;

				::framework::gpu::shader_module_create_info shader_module_create_info;
				shader_module_create_info.code_size = shader_code_data.size();
				shader_module_create_info.code = shader_code_data.data();
				assert_framework_gpu_result(device->create_shader_module(&shader_module_create_info, nullptr, &shader_module));

				return shader_module;
			}
		}
	}

	struct compilation_state
	{
		::std::vector<char> code;
	};

	::std::vector<struct compilation_state> compiled_shaders(shader_module_info.shader_stage_info_count);
	::std::set<::std::filesystem::path> include_files;

	for (::std::uint32_t i = 0; i < shader_module_info.shader_stage_info_count; i++)
	{
	retry_shader_compile:

		::std::ifstream ifstream(shader_stage_cached_infos[i].path, ::std::ios::binary);
		if (!ifstream.is_open())
		{
			return nullptr;
		}

		compiled_shaders[i].code = ::std::vector<char>(::std::istreambuf_iterator<char>(ifstream), {});

		struct shader_compile_include_handler : ::framework::resource::shader_compile_include_handler
		{
			shader_compile_include_handler(::framework::resource::resource_manager const & resource_manager, ::std::filesystem::path source_file_path, ::framework::resource::shader_stage_info const & shader_stage_info) :
				resource_manager(resource_manager),
				source_file_path(source_file_path),
				shader_stage_info(shader_stage_info)
			{
			}

			bool load_source(::std::filesystem::path const & file_name, ::std::filesystem::path const & includer_name, ::std::string & code) override
			{
				::std::filesystem::path overriten_file_name = file_name;
				for (::std::uint32_t i = 0; i < this->shader_stage_info.include_override_count; i++)
				{
					if (overriten_file_name.filename() == this->shader_stage_info.include_overrides[i].src)
					{
						if (this->shader_stage_info.include_overrides[i].dst.empty())
						{
							return true;
						}
						overriten_file_name = this->shader_stage_info.include_overrides[i].dst;
						break;
					}
				}

				::std::filesystem::path file_path = this->source_file_path / overriten_file_name;
				if (!::std::filesystem::exists(file_path) && !this->resource_manager.find_file(file_path, overriten_file_name))
				{
					return false;
				}

				::std::ifstream ifstream(file_path);
				if (!ifstream.is_open())
				{
					return false;
				}

				::std::stringstream shader_code;
				shader_code << ifstream.rdbuf();

				code = shader_code.str();

				this->included_files.push_front(file_path.lexically_normal());

				return true;
			}

			void notify_dependency(::std::filesystem::path dependency_file_path) override
			{
				this->included_files.push_front(dependency_file_path.lexically_normal());
			}

			::framework::resource::resource_manager const & resource_manager;
			::std::filesystem::path source_file_path;
			::framework::resource::shader_stage_info const & shader_stage_info;
			::std::forward_list<::std::filesystem::path> included_files;
		} shader_compile_include_handler(*this, shader_stage_cached_infos[i].path.parent_path(), shader_module_info.shader_stage_infos[i]);

		try
		{
			::framework::resource::shader_language source_shader_language = shader_stage_cached_infos[i].source_shader_language;

			for (auto & pipeline_stage : shader_stage_cached_infos[i].compilation_pipeline)
			{
				::framework::resource::shader_compilation_info shader_compilation_info;
				shader_compilation_info.source_shader_language = source_shader_language;
				shader_compilation_info.target_shader_language = pipeline_stage.target_shader_language;
				shader_compilation_info.target_shader_environment = this->target_shader_environment;
				shader_compilation_info.shader_compile_include_handler = &shader_compile_include_handler;
				shader_compilation_info.preprocessor_define_count = shader_module_info.shader_stage_infos[i].preprocessor_define_count;
				shader_compilation_info.preprocessor_defines = shader_module_info.shader_stage_infos[i].preprocessor_defines;
				shader_compilation_info.type_conformance_count = shader_module_info.shader_stage_infos[i].type_conformance_count;
				shader_compilation_info.type_conformances = shader_module_info.shader_stage_infos[i].type_conformances;
				shader_compilation_info.code_size = compiled_shaders[i].code.size();
				shader_compilation_info.code = compiled_shaders[i].code.data();
				shader_compilation_info.entry_point = shader_module_info.shader_stage_infos[i].entry_point;
				shader_compilation_info.source_entry_point = shader_stage_cached_infos[i].source_entry_point;
				shader_compilation_info.source_file_path = shader_stage_cached_infos[i].path;
				shader_compilation_info.stage = shader_module_info.shader_stage_infos[i].stage;
				auto shader_compile_task = pipeline_stage.shader_compiler->compile(shader_compilation_info);
				::framework::coroutine::sync_wait(shader_compile_task);

				::std::unique_ptr<::framework::resource::program> program = ::std::unique_ptr<::framework::resource::program>(shader_compile_task.result());
				::std::size_t code_size = program->get_code_size();
				void const * code = program->get_code();
				compiled_shaders[i].code.assign(static_cast<char const *>(code), static_cast<char const *>(code) + code_size);

				source_shader_language = pipeline_stage.target_shader_language;

				for (auto & included_file : shader_compile_include_handler.included_files)
				{
					include_files.insert(included_file);
				}
			}
		}
		catch (...)
		{
#ifdef WIN32
			int result = MessageBox(0, "Failed to compile shader", 0, MB_RETRYCANCEL | MB_ICONHAND);

			if (result == IDRETRY)
			{
				goto retry_shader_compile;
			}
			else
			{
				throw;
			}
#endif
		}
	}

	::framework::resource::shader_linker * shader_linker;

	for (auto & shader_linker_iterator : this->shader_linkers)
	{
		::std::uint32_t qurey_count;

		shader_linker_iterator->get_source_shader_languages(qurey_count, nullptr);
		source_shader_languages.resize(qurey_count);
		shader_linker_iterator->get_source_shader_languages(qurey_count, source_shader_languages.data());

		shader_linker_iterator->get_target_shader_languages(qurey_count, nullptr);
		target_shader_languages.resize(qurey_count);
		shader_linker_iterator->get_target_shader_languages(qurey_count, target_shader_languages.data());

		shader_linker_iterator->get_target_shader_environments(qurey_count, nullptr);
		target_shader_environments.resize(qurey_count);
		shader_linker_iterator->get_target_shader_environments(qurey_count, target_shader_environments.data());

		if (::std::find(source_shader_languages.begin(), source_shader_languages.end(), this->target_shader_language) == source_shader_languages.end() ||
			::std::find(target_shader_languages.begin(), target_shader_languages.end(), this->target_shader_language) == target_shader_languages.end() ||
			::std::find(target_shader_environments.begin(), target_shader_environments.end(), this->target_shader_environment) == target_shader_environments.end())
		{
			continue;
		}

		shader_linker = shader_linker_iterator.get();

		goto linker_found;
	}

	throw;

linker_found:

	struct shader_link_callback : public ::framework::resource::shader_link_callback
	{
		shader_link_callback(::std::vector<::std::byte> & linked_shader) :
			linked_shader(linked_shader)
		{
		}

		::framework::coroutine::lazy_task<void> operator()(::framework::resource::shader_link_callback::shader_link_result const & shader_link_result) override
		{
			this->linked_shader.assign(static_cast<::std::byte const *>(shader_link_result.code), static_cast<::std::byte const *>(shader_link_result.code) + shader_link_result.code_size);

			co_return;
		};

		::std::vector<::std::byte> & linked_shader;
	};

	::std::vector<::std::byte> linked_shaders;
	struct shader_link_callback shader_link_callback(linked_shaders);

	::std::vector<::framework::resource::shader_info> shader_infos(shader_module_info.shader_stage_info_count);
	for (::std::uint32_t i = 0; i < shader_module_info.shader_stage_info_count; i++)
	{
		shader_infos[i].code_size = compiled_shaders[i].code.size();
		shader_infos[i].code = compiled_shaders[i].code.data();
	}

	::framework::resource::shader_link_info shader_link_info;
	shader_link_info.shader_info_count = shader_module_info.shader_stage_info_count;
	shader_link_info.shader_infos = shader_infos.data();
	auto shader_link_task = shader_linker->link(shader_link_info, shader_link_callback);
	::framework::coroutine::sync_wait(shader_link_task);

	::std::ofstream ofstream(shader_cache_path, ::std::ios::binary | ::std::ios::out);

	if (ofstream.is_open())
	{
		ofstream.write(reinterpret_cast<char const *>(linked_shaders.data()), linked_shaders.size());
	}

	::std::wofstream wofstream(refs_path, ::std::ios::binary | ::std::ios::out);

	if (wofstream.is_open())
	{
		for (auto & include_file : include_files)
		{
			wofstream << include_file.c_str() << ::std::endl;
		}
	}

	::framework::gpu::shader_module * shader_module;

	::framework::gpu::shader_module_create_info shader_module_create_info;
	shader_module_create_info.code_size = linked_shaders.size();
	shader_module_create_info.code = linked_shaders.data();
	assert_framework_gpu_result(device->create_shader_module(&shader_module_create_info, nullptr, &shader_module));

	return shader_module;
}

::framework::coroutine::immediate_task<void>(::framework::resource::resource_manager::load_texture)(::framework::resource::texture_load_info const & texture_load_info)
{
	if (::std::filesystem::path(texture_load_info.file_name).extension() == ".dds")
	{
		co_await this->texture_loaders[0]->load_texture(texture_load_info);
	}
	else if (::std::filesystem::path(texture_load_info.file_name).extension() == ".exr")
	{
		co_await this->texture_loaders[1]->load_texture(texture_load_info);
	}
	else
	{
		co_await this->texture_loaders[2]->load_texture(texture_load_info);
	}

	co_return;
}