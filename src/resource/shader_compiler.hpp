#pragma once

#include "resource/shader.hpp"
#include "gpu/core.hpp"
#include "coroutine/task.hpp"
#include <filesystem>

namespace framework::resource
{
	struct shader_compile_include_handler
	{
		virtual bool load_source(::std::filesystem::path const & file_name, ::std::filesystem::path const & includer_name, ::std::string & code) = 0;

		virtual void notify_dependency(::std::filesystem::path dependency_file_path) = 0;
	};

	class program_reflection
	{
	public:
		virtual ~program_reflection() = default;
	};

	class program
	{
	public:
		virtual ~program() = default;

		virtual ::std::size_t get_code_size() const = 0;

		virtual void const * get_code() const = 0;

		virtual ::framework::resource::program_reflection * create_program_reflection() const = 0;
	};

	struct preprocessor_define
	{
		char const * name;
		char const * value;
	};

	struct type_conformance
	{
		char const * type_name;
		char const * interface_name;
		::std::uint32_t id;
	};
	
	struct shader_compilation_info
	{
		::framework::resource::shader_language source_shader_language;
		::framework::resource::shader_language target_shader_language;
		::framework::resource::shader_environment target_shader_environment;
		::framework::resource::shader_compile_include_handler * shader_compile_include_handler;
		::std::uint32_t preprocessor_define_count;
		::framework::resource::preprocessor_define const * preprocessor_defines;
		::std::uint32_t type_conformance_count;
		::framework::resource::type_conformance const * type_conformances;
		::std::size_t code_size;
		void const * code;
		char const * entry_point;
		char const * source_entry_point;
		::std::filesystem::path source_file_path;
		::framework::gpu::shader_stage_flags stage;
	};

	class shader_compiler
	{
	public:
		virtual ::framework::coroutine::lazy_task<::framework::resource::program *> compile(::framework::resource::shader_compilation_info const & shader_compilation_info) = 0;

		virtual void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) = 0;

		virtual void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) = 0;

		virtual void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) = 0;
	};
}