#include "resource/dxc_compiler.hpp"
#include "resource/resource_manager.hpp"
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

namespace framework::local
{
	namespace
	{
		::std::wstring convert_utf_8_to_wide(char const * string, ::std::size_t length)
		{
			int const count = ::MultiByteToWideChar(CP_UTF8, 0, string, length, NULL, 0);
			std::wstring wstring(count, 0);
			::MultiByteToWideChar(CP_UTF8, 0, string, length, wstring.data(), count);
			return wstring;
		}

		::std::wstring convert_utf_8_to_wide(char const * string)
		{
			return ::framework::local::convert_utf_8_to_wide(string, ::strlen(string));
		}

		struct include_handler : public ::IDxcIncludeHandler
		{
			explicit include_handler(::framework::resource::dxc_compiler & dxc_compiler, ::framework::resource::shader_compile_include_handler * shader_compile_include_handler, ::std::filesystem::path source_file_path) :
				dxc_compiler(dxc_compiler), shader_compile_include_handler(shader_compile_include_handler), source_file_path(source_file_path)
			{
			}

			::HRESULT STDMETHODCALLTYPE LoadSource(_In_z_::LPCWSTR file_name, _COM_Outptr_result_maybenull_::IDxcBlob ** include_source) override
			{
				::CComPtr<::IDxcBlobEncoding> dxc_blob_encoding;
				::HRESULT hresult;
				::std::string code;
				if (this->shader_compile_include_handler->load_source(file_name, this->source_file_path.filename(), code))
				{
					hresult = this->dxc_compiler.get_dxc_utils()->CreateBlob(code.data(), code.size(), CP_ACP, &dxc_blob_encoding);
				}
				else
				{
					hresult = E_FAIL;
				}

				*include_source = dxc_blob_encoding.Detach();

				return hresult;
			}

			HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject) override
			{
				return E_FAIL;
			}
			ULONG STDMETHODCALLTYPE AddRef(void) override { return 0; }
			ULONG STDMETHODCALLTYPE Release(void) override { return 0; }

			::framework::resource::dxc_compiler & dxc_compiler;
			::framework::resource::shader_compile_include_handler * shader_compile_include_handler;
			::std::filesystem::path source_file_path;
		};
	}
}

::framework::resource::dxc_compiler::dxc_compiler(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
	::HRESULT hresult;
	hresult = ::DxcCreateInstance(::CLSID_DxcUtils, IID_PPV_ARGS(&this->dxc_utils));
	hresult = ::DxcCreateInstance(::CLSID_DxcCompiler, IID_PPV_ARGS(&this->_dxc_compiler));
}

::framework::coroutine::lazy_task<::framework::resource::program *>(::framework::resource::dxc_compiler::compile)(::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	co_return new ::framework::resource::dxc_program(*this, shader_compilation_info);
}

void ::framework::resource::dxc_compiler::get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages)
{
	::framework::resource::shader_language const supported_source_shader_languages[]
	{
		::framework::resource::shader_language::hlsl
	};

	if (source_shader_languages)
	{
		::std::memcpy(source_shader_languages, supported_source_shader_languages, ::std::min(static_cast<::std::size_t>(source_shader_language_count), ::std::size(supported_source_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		source_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_source_shader_languages));
	}
}

void ::framework::resource::dxc_compiler::get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages)
{
	::framework::resource::shader_language const supported_target_shader_languages[]
	{
		::framework::resource::shader_language::spirv_1_5,
		::framework::resource::shader_language::dxil,
	};

	if (target_shader_languages)
	{
		::std::memcpy(target_shader_languages, supported_target_shader_languages, ::std::min(static_cast<::std::size_t>(target_shader_language_count), ::std::size(supported_target_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		target_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_languages));
	}
}

void ::framework::resource::dxc_compiler::get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments)
{
	::framework::resource::shader_environment const supported_target_shader_environments[]
	{
		::framework::resource::shader_environment::vulkan_1_3,
		::framework::resource::shader_environment::d3d12
	};

	if (target_shader_environments)
	{
		::std::memcpy(target_shader_environments, supported_target_shader_environments, ::std::min(static_cast<::std::size_t>(target_shader_environment_count), ::std::size(supported_target_shader_environments)) * sizeof(::framework::resource::shader_environment));
	}
	else
	{
		target_shader_environment_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_environments));
	}
}

::framework::resource::dxc_program::dxc_program(::framework::resource::dxc_compiler & dxc_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	::CComPtr<::IDxcBlobEncoding> dxc_blob;

	::std::vector<::LPCWSTR> arguments;

	arguments.push_back(L"-HV 2021");
	arguments.push_back(L"-enable-16bit-types");

	switch (shader_compilation_info.target_shader_language)
	{
	case ::framework::resource::shader_language::spirv_1_5:
		arguments.push_back(L"-spirv");
		break;
	}

	switch (shader_compilation_info.target_shader_environment)
	{
	case ::framework::resource::shader_environment::vulkan_1_3:
		arguments.push_back(L"-fspv-target-env=vulkan1.3");
		arguments.push_back(L"-fvk-use-dx-layout");
		break;
	}

	::HRESULT hresult;

	hresult = dxc_compiler.dxc_utils->CreateBlob(shader_compilation_info.code, shader_compilation_info.code_size, CP_ACP, &dxc_blob);

	::CComPtr<::IDxcOperationResult> dxc_compile_result;
	::framework::local::include_handler include_handler(dxc_compiler, shader_compilation_info.shader_compile_include_handler, shader_compilation_info.source_file_path.parent_path());
	::std::wstring const file_name = shader_compilation_info.source_file_path.filename().wstring();
	::std::unique_ptr<::DxcDefine[]> dxc_defines = ::std::make_unique<::DxcDefine[]>(shader_compilation_info.preprocessor_define_count);
	::std::unique_ptr<::std::pair<::std::wstring, ::std::wstring>[]> wstring_preprocessor_defines = ::std::make_unique<::std::pair<::std::wstring, ::std::wstring>[]>(shader_compilation_info.preprocessor_define_count);
	for (::std::uint32_t i = 0; i < shader_compilation_info.preprocessor_define_count; i++)
	{
		wstring_preprocessor_defines[i] = ::std::make_pair(::framework::local::convert_utf_8_to_wide(shader_compilation_info.preprocessor_defines[i].name), ::framework::local::convert_utf_8_to_wide(shader_compilation_info.preprocessor_defines[i].value));
		dxc_defines[i] = ::DxcDefine{ wstring_preprocessor_defines[i].first.c_str(), wstring_preprocessor_defines[i].second.c_str() };
	}
	hresult = dxc_compiler._dxc_compiler->Compile(dxc_blob, file_name.c_str(), L"", L"lib_6_6", arguments.data(), static_cast<::UINT32>(arguments.size()), dxc_defines.get(), shader_compilation_info.preprocessor_define_count, &include_handler, &dxc_compile_result);

	::HRESULT hresult1 = dxc_compile_result->GetStatus(&hresult);

	if (FAILED(hresult1) || FAILED(hresult))
	{
		if (dxc_compile_result)
		{
			::CComPtr<::IDxcBlobEncoding> errors_blob;
			hresult = dxc_compile_result->GetErrorBuffer(&errors_blob);

			if (SUCCEEDED(hresult) && errors_blob)
			{
				::std::cerr << static_cast<char const *>(static_cast<::LPCSTR>(errors_blob->GetBufferPointer())) << ::std::endl;
				::OutputDebugStringA(static_cast<::LPCSTR>(errors_blob->GetBufferPointer()));
			}
		}

		throw ::std::exception();
	}

	hresult = dxc_compile_result->GetResult(&this->dxil_library);
}

::std::size_t(::framework::resource::dxc_program::get_code_size)() const
{
	return this->dxil_library->GetBufferSize();
}

void const * (::framework::resource::dxc_program::get_code)() const
{
	return this->dxil_library->GetBufferPointer();
}