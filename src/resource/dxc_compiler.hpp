#pragma once

#include "resource/shader_compiler.hpp"
#ifdef _WIN32
#define NOMINMAX
#include <atlbase.h>
#include <guiddef.h>
#endif
#include <dxc/dxcapi.h>

namespace framework::resource
{
	class resource_manager;
	class dxc_compiler;

	class dxc_program_reflection : public ::framework::resource::program_reflection
	{
	public:
	};

	class dxc_program : public ::framework::resource::program
	{
	public:
		dxc_program(::framework::resource::dxc_compiler & dxc_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info);

		::std::size_t get_code_size() const override;

		void const * get_code() const override;

		::framework::resource::program_reflection * create_program_reflection() const override;

	private:
		::CComPtr<::IDxcBlob> dxil_library;
	};

	class dxc_compiler : public ::framework::resource::shader_compiler
	{
	public:
		dxc_compiler(::framework::resource::resource_manager const & resource_manager);

		::framework::coroutine::lazy_task<::framework::resource::program *> compile(::framework::resource::shader_compilation_info const & shader_compilation_info) override;

		void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) override;

		void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) override;

		void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) override;

		::IDxcUtils * get_dxc_utils() const noexcept;

	private:
		friend class ::framework::resource::dxc_program::dxc_program;

		::framework::resource::resource_manager const & resource_manager;
		::CComPtr<::IDxcUtils> dxc_utils;
		::CComPtr<::IDxcCompiler> _dxc_compiler;
	};
}

#include "resource/dxc_compiler.inl"