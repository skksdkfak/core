#include "resource/glslang_compiler.hpp"
#include "resource/resource_manager.hpp"
#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <spirv-tools/linker.hpp>
#include <filesystem>
#include <forward_list>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace
{
	::TBuiltInResource const default_built_in_resource =
	{
		/*.maxLights = */ 8,         // From OpenGL 3.0 table 6.46.
		/*.maxClipPlanes = */ 6,     // From OpenGL 3.0 table 6.46.
		/*.maxTextureUnits = */ 2,   // From OpenGL 3.0 table 6.50.
		/*.maxTextureCoords = */ 8,  // From OpenGL 3.0 table 6.50.
		/*.maxVertexAttribs = */ 16,
		/*.maxVertexUniformComponents = */ 4096,
		/*.maxVaryingFloats = */ 60,  // From OpenGLES 3.1 table 6.44.
		/*.maxVertexTextureImageUnits = */ 16,
		/*.maxCombinedTextureImageUnits = */ 80,
		/*.maxTextureImageUnits = */ 16,
		/*.maxFragmentUniformComponents = */ 1024,
		/*.maxDrawBuffers = */ 8,
		/*.maxVertexUniformVectors = */ 256,
		/*.maxVaryingVectors = */ 15,  // From OpenGLES 3.1 table 6.44.
		/*.maxFragmentUniformVectors = */ 256,
		/*.maxVertexOutputVectors = */ 16,   // maxVertexOutputComponents / 4
		/*.maxFragmentInputVectors = */ 15,  // maxFragmentInputComponents / 4
		/*.minProgramTexelOffset = */ -8,
		/*.maxProgramTexelOffset = */ 7,
		/*.maxClipDistances = */ 8,
		/*.maxComputeWorkGroupCountX = */ 65535,
		/*.maxComputeWorkGroupCountY = */ 65535,
		/*.maxComputeWorkGroupCountZ = */ 65535,
		/*.maxComputeWorkGroupSizeX = */ 1024,
		/*.maxComputeWorkGroupSizeX = */ 1024,
		/*.maxComputeWorkGroupSizeZ = */ 64,
		/*.maxComputeUniformComponents = */ 512,
		/*.maxComputeTextureImageUnits = */ 16,
		/*.maxComputeImageUniforms = */ 8,
		/*.maxComputeAtomicCounters = */ 8,
		/*.maxComputeAtomicCounterBuffers = */ 1,  // From OpenGLES 3.1 Table 6.43
		/*.maxVaryingComponents = */ 60,
		/*.maxVertexOutputComponents = */ 64,
		/*.maxGeometryInputComponents = */ 64,
		/*.maxGeometryOutputComponents = */ 128,
		/*.maxFragmentInputComponents = */ 128,
		/*.maxImageUnits = */ 8,  // This does not seem to be defined anywhere,
		// set to ImageUnits.
		/*.maxCombinedImageUnitsAndFragmentOutputs = */ 8,
		/*.maxCombinedShaderOutputResources = */ 8,
		/*.maxImageSamples = */ 0,
		/*.maxVertexImageUniforms = */ 0,
		/*.maxTessControlImageUniforms = */ 0,
		/*.maxTessEvaluationImageUniforms = */ 0,
		/*.maxGeometryImageUniforms = */ 0,
		/*.maxFragmentImageUniforms = */ 8,
		/*.maxCombinedImageUniforms = */ 8,
		/*.maxGeometryTextureImageUnits = */ 16,
		/*.maxGeometryOutputVertices = */ 256,
		/*.maxGeometryTotalOutputComponents = */ 1024,
		/*.maxGeometryUniformComponents = */ 512,
		/*.maxGeometryVaryingComponents = */ 60,  // Does not seem to be defined
		// anywhere, set equal to
		// maxVaryingComponents.
		/*.maxTessControlInputComponents = */ 128,
		/*.maxTessControlOutputComponents = */ 128,
		/*.maxTessControlTextureImageUnits = */ 16,
		/*.maxTessControlUniformComponents = */ 1024,
		/*.maxTessControlTotalOutputComponents = */ 4096,
		/*.maxTessEvaluationInputComponents = */ 128,
		/*.maxTessEvaluationOutputComponents = */ 128,
		/*.maxTessEvaluationTextureImageUnits = */ 16,
		/*.maxTessEvaluationUniformComponents = */ 1024,
		/*.maxTessPatchComponents = */ 120,
		/*.maxPatchVertices = */ 32,
		/*.maxTessGenLevel = */ 64,
		/*.maxViewports = */ 16,
		/*.maxVertexAtomicCounters = */ 0,
		/*.maxTessControlAtomicCounters = */ 0,
		/*.maxTessEvaluationAtomicCounters = */ 0,
		/*.maxGeometryAtomicCounters = */ 0,
		/*.maxFragmentAtomicCounters = */ 8,
		/*.maxCombinedAtomicCounters = */ 8,
		/*.maxAtomicCounterBindings = */ 1,
		/*.maxVertexAtomicCounterBuffers = */ 0,  // From OpenGLES 3.1 Table 6.41.
		// ARB_shader_atomic_counters.
		/*.maxTessControlAtomicCounterBuffers = */ 0,
		/*.maxTessEvaluationAtomicCounterBuffers = */ 0,
		/*.maxGeometryAtomicCounterBuffers = */ 0,
		// /ARB_shader_atomic_counters.
		/*.maxFragmentAtomicCounterBuffers = */ 0,  // From OpenGLES 3.1 Table 6.43.
		/*.maxCombinedAtomicCounterBuffers = */ 1,
		/*.maxAtomicCounterBufferSize = */ 32,
		/*.maxTransformFeedbackBuffers = */ 4,
		/*.maxTransformFeedbackInterleavedComponents = */ 64,
		/*.maxCullDistances = */ 8,                 // ARB_cull_distance.
		/*.maxCombinedClipAndCullDistances = */ 8,  // ARB_cull_distance.
		/*.maxSamples = */ 4,
		/* .maxMeshOutputVerticesNV = */ 256,
		/* .maxMeshOutputPrimitivesNV = */ 512,
		/* .maxMeshWorkGroupSizeX_NV = */ 32,
		/* .maxMeshWorkGroupSizeY_NV = */ 1,
		/* .maxMeshWorkGroupSizeZ_NV = */ 1,
		/* .maxTaskWorkGroupSizeX_NV = */ 32,
		/* .maxTaskWorkGroupSizeY_NV = */ 1,
		/* .maxTaskWorkGroupSizeZ_NV = */ 1,
		/* .maxMeshViewCountNV = */ 4,
		/* .maxMeshOutputVerticesEXT = */ 256,
		/* .maxMeshOutputPrimitivesEXT = */ 256,
		/* .maxMeshWorkGroupSizeX_EXT = */ 128,
		/* .maxMeshWorkGroupSizeY_EXT = */ 128,
		/* .maxMeshWorkGroupSizeZ_EXT = */ 128,
		/* .maxTaskWorkGroupSizeX_EXT = */ 128,
		/* .maxTaskWorkGroupSizeY_EXT = */ 128,
		/* .maxTaskWorkGroupSizeZ_EXT = */ 128,
		/* .maxMeshViewCountEXT = */ 4,
		/* .maxDualSourceDrawBuffersEXT = */ 1,
		/*.limits = */
		{
			/*.nonInductiveForLoops = */ 1,
			/*.whileLoops = */ 1,
			/*.doWhileLoops = */ 1,
			/*.generalUniformIndexing = */ 1,
			/*.generalAttributeMatrixVectorIndexing = */ 1,
			/*.generalVaryingIndexing = */ 1,
			/*.generalSamplerIndexing = */ 1,
			/*.generalVariableIndexing = */ 1,
			/*.generalConstantMatrixVectorIndexing = */ 1,
			}
	};

	class includer : public ::glslang::TShader::Includer
	{
	public:
		struct include_result
		{
			include_result(::std::string header_name, ::std::string header_data) :
				header_name(header_name),
				header_data(header_data),
				glslang_include_result(this->header_name, this->header_data.c_str(), this->header_data.size(), this)
			{
			}

			::std::string header_name;
			::std::string header_data;
			::glslang::TShader::Includer::IncludeResult glslang_include_result;
		};

		includer(::framework::resource::glslang_compiler & glslang_compiler, ::framework::resource::shader_compile_include_handler * shader_compile_include_handler, ::std::filesystem::path source_file_path) :
			glslang_compiler(glslang_compiler),
			shader_compile_include_handler(shader_compile_include_handler),
			source_file_path(source_file_path)
		{
		}

		::glslang::TShader::Includer::IncludeResult * includeSystem(char const * header_name, const char * includer_name, ::std::size_t inclusion_depth) override
		{
			::std::string code;
			if (this->shader_compile_include_handler->load_source(header_name, includer_name, code))
			{
				return &this->include_results.emplace_front(::std::string(header_name), code).glslang_include_result;

			}
			else
			{
				return nullptr;
			}
		}

		::glslang::TShader::Includer::IncludeResult * includeLocal(char const * header_name, const char * includer_name, ::std::size_t inclusion_depth) override
		{
			::std::string code;
			if (this->shader_compile_include_handler->load_source(header_name, includer_name, code))
			{
				return &this->include_results.emplace_front(::std::string(header_name), code).glslang_include_result;
			}
			else
			{
				return nullptr;
			}
		}

		void releaseInclude(::glslang::TShader::Includer::IncludeResult * include_result) override
		{
		}

	private:
		::framework::resource::glslang_compiler & glslang_compiler;
		::framework::resource::shader_compile_include_handler * shader_compile_include_handler;
		::std::forward_list<::includer::include_result> include_results;
		::std::filesystem::path source_file_path;
	};
}

::framework::resource::glslang_compiler::glslang_compiler(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
	::glslang::InitializeProcess();
}

::framework::coroutine::lazy_task<::framework::resource::program *>(::framework::resource::glslang_compiler::compile)(::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	co_return new ::framework::resource::glslang_program(*this, shader_compilation_info);
}

void ::framework::resource::glslang_compiler::get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages)
{
	::framework::resource::shader_language const supported_source_shader_languages[]
	{
		::framework::resource::shader_language::glsl
	};

	if (source_shader_languages)
	{
		::std::memcpy(source_shader_languages, supported_source_shader_languages, ::std::min(static_cast<::std::size_t>(source_shader_language_count), ::std::size(supported_source_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		source_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_source_shader_languages));
	}
}

void ::framework::resource::glslang_compiler::get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages)
{
	::framework::resource::shader_language const supported_target_shader_languages[]
	{
		::framework::resource::shader_language::spirv_1_5
	};

	if (target_shader_languages)
	{
		::std::memcpy(target_shader_languages, supported_target_shader_languages, ::std::min(static_cast<::std::size_t>(target_shader_language_count), ::std::size(supported_target_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		target_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_languages));
	}
}

void ::framework::resource::glslang_compiler::get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments)
{
	::framework::resource::shader_environment const supported_target_shader_environments[]
	{
		::framework::resource::shader_environment::vulkan_1_3
	};

	if (target_shader_environments)
	{
		::std::memcpy(target_shader_environments, supported_target_shader_environments, ::std::min(static_cast<::std::size_t>(target_shader_environment_count), ::std::size(supported_target_shader_environments)) * sizeof(::framework::resource::shader_environment));
	}
	else
	{
		target_shader_environment_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_environments));
	}
}

::framework::resource::glslang_program::glslang_program(::framework::resource::glslang_compiler & glslang_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	::EShLanguage shader_stage;
	switch (shader_compilation_info.stage)
	{
	case ::framework::gpu::shader_stage_flags::vertex_bit:
		shader_stage = ::EShLanguage::EShLangVertex;
		break;
	case ::framework::gpu::shader_stage_flags::tessellation_control_bit:
		shader_stage = ::EShLanguage::EShLangTessControl;
		break;
	case ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit:
		shader_stage = ::EShLanguage::EShLangTessEvaluation;
		break;
	case ::framework::gpu::shader_stage_flags::geometry_bit:
		shader_stage = ::EShLanguage::EShLangGeometry;
		break;
	case ::framework::gpu::shader_stage_flags::fragment_bit:
		shader_stage = ::EShLanguage::EShLangFragment;
		break;
	case ::framework::gpu::shader_stage_flags::compute_bit:
		shader_stage = ::EShLanguage::EShLangCompute;
		break;
	case ::framework::gpu::shader_stage_flags::raygen_bit:
		shader_stage = ::EShLanguage::EShLangRayGen;
		break;
	case ::framework::gpu::shader_stage_flags::any_hit_bit:
		shader_stage = ::EShLanguage::EShLangAnyHit;
		break;
	case ::framework::gpu::shader_stage_flags::closest_hit_bit:
		shader_stage = ::EShLanguage::EShLangClosestHit;
		break;
	case ::framework::gpu::shader_stage_flags::miss_bit:
		shader_stage = ::EShLanguage::EShLangMiss;
		break;
	case ::framework::gpu::shader_stage_flags::intersection_bit:
		shader_stage = ::EShLanguage::EShLangIntersect;
		break;
	case ::framework::gpu::shader_stage_flags::callable_bit:
		shader_stage = ::EShLanguage::EShLangCallable;
		break;
	case ::framework::gpu::shader_stage_flags::task_bit:
		shader_stage = ::EShLanguage::EShLangTaskNV;
		break;
	case ::framework::gpu::shader_stage_flags::mesh_bit:
		shader_stage = ::EShLanguage::EShLangMeshNV;
		break;
	default:
		throw ::std::exception();
	}

	::glslang::EShSource source_language;
	switch (shader_compilation_info.source_shader_language)
	{
	case ::framework::resource::shader_language::glsl:
		source_language = ::glslang::EShSource::EShSourceGlsl;
		break;
	case ::framework::resource::shader_language::hlsl:
		source_language = ::glslang::EShSource::EShSourceHlsl;
		break;
	default:
		throw ::std::exception();
	}

	::glslang::EShTargetLanguage target_language;
	::glslang::EShTargetLanguageVersion target_language_version;
	switch (shader_compilation_info.target_shader_language)
	{
	case ::framework::resource::shader_language::spirv_1_5:
		target_language = ::glslang::EShTargetLanguage::EShTargetSpv;
		target_language_version = ::glslang::EShTargetLanguageVersion::EShTargetSpv_1_5;
		break;
	default:
		throw ::std::exception();
	}

	::glslang::EShClient target_client;
	::glslang::EShTargetClientVersion target_client_version;
	switch (shader_compilation_info.target_shader_environment)
	{
	case ::framework::resource::shader_environment::vulkan_1_3:
		target_client = ::glslang::EShClient::EShClientVulkan;
		target_client_version = ::glslang::EShTargetClientVersion::EShTargetVulkan_1_3;
		break;
	default:
		throw ::std::exception();
	}

	::glslang::TShader shader(shader_stage);
	::std::string const file_name = shader_compilation_info.source_file_path.filename().string();
	char const * code = static_cast<char const *>(shader_compilation_info.code);
	char const * name = static_cast<char const *>(file_name.c_str());
	int code_size = static_cast<int>(shader_compilation_info.code_size);
	::std::string preamble;
	for (::std::uint32_t i = 0; i < shader_compilation_info.preprocessor_define_count; i++)
	{
		preamble += ::std::string("#define ") + shader_compilation_info.preprocessor_defines[i].name + " " + shader_compilation_info.preprocessor_defines[i].value + "\n";
	}
	shader.setPreamble(preamble.c_str());
	shader.setStringsWithLengthsAndNames(&code, &code_size, &name, 1);
	shader.setEnvInput(source_language, shader_stage, target_client, 460);
	shader.setEnvClient(target_client, target_client_version);
	shader.setEnvTarget(target_language, target_language_version);
	shader.setSourceEntryPoint(shader_compilation_info.source_entry_point ? shader_compilation_info.source_entry_point : "forward_main");
	shader.setEntryPoint(shader_compilation_info.entry_point);

	::includer includer(glslang_compiler, shader_compilation_info.shader_compile_include_handler, shader_compilation_info.source_file_path.parent_path());

	if (!shader.parse(&::default_built_in_resource, 460, false, ::EShMessages::EShMsgDefault, includer))
	{
		::std::cout << shader.getInfoLog() << ::std::endl;
		::std::cout << shader.getInfoDebugLog() << ::std::endl;
		throw ::std::exception();
	}

	::glslang::TProgram program;
	program.addShader(&shader);

	if (!program.link(::EShMessages::EShMsgDefault))
	{
		::std::cout << shader.getInfoLog() << ::std::endl;
		::std::cout << shader.getInfoDebugLog() << ::std::endl;
		throw ::std::exception();
	}

	::spv::SpvBuildLogger logger;

	::glslang::SpvOptions spv_options;
	spv_options.disableOptimizer = false;

	::glslang::GlslangToSpv(*program.getIntermediate(shader_stage), this->spirv_code, &logger, &spv_options);

	::std::cout << logger.getAllMessages() << ::std::endl;
}

::std::size_t(::framework::resource::glslang_program::get_code_size)() const
{
	return this->spirv_code.size() * sizeof(::std::uint32_t);
}

void const * (::framework::resource::glslang_program::get_code)() const
{
	return this->spirv_code.data();
}

::framework::resource::program_reflection * (::framework::resource::glslang_program::create_program_reflection)() const
{
	return nullptr;
}
