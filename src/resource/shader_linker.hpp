#pragma once

#include "resource/shader.hpp"
#include "coroutine/task.hpp"
#include <cstdint>

namespace framework::resource
{
	struct shader_info
	{
		::std::size_t code_size;
		void const * code;
	};

	struct shader_link_info
	{
		::std::uint32_t shader_info_count;
		::framework::resource::shader_info const * shader_infos;
	};

	struct shader_link_callback
	{
		struct shader_link_result
		{
			::std::size_t code_size;
			void const * code;
		};

		virtual ::framework::coroutine::lazy_task<void> operator()(::framework::resource::shader_link_callback::shader_link_result const & shader_link_result) = 0;
	};

	class shader_linker
	{
	public:
		virtual ::framework::coroutine::lazy_task<void> link(::framework::resource::shader_link_info const & shader_link_info, ::framework::resource::shader_link_callback & shader_link_callback) = 0;

		virtual void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) = 0;

		virtual void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) = 0;

		virtual void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) = 0;
	};
}