inline ::std::vector<::std::filesystem::path> const & ::framework::resource::resource_manager::get_search_directories() const noexcept
{
	return this->directories;
}