inline ::framework::resource::program_reflection * (::framework::resource::dxc_program::create_program_reflection)() const
{
	return nullptr;
}

inline ::IDxcUtils * (::framework::resource::dxc_compiler::get_dxc_utils)() const noexcept
{
	return this->dxc_utils;
}