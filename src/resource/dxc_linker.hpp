#pragma once

#include "resource/shader_linker.hpp"
#ifdef _WIN32
#define NOMINMAX
#include <atlbase.h>
#include <guiddef.h>
#endif
#include <dxc/dxcapi.h>

namespace framework::resource
{
	class resource_manager;

	class dxc_linker : public ::framework::resource::shader_linker
	{
	public:
		dxc_linker(::framework::resource::resource_manager const & resource_manager);

		::framework::coroutine::lazy_task<void> link(::framework::resource::shader_link_info const & shader_link_info, ::framework::resource::shader_link_callback & shader_link_callback) override;

		void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) override;

		void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) override;

		void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) override;

	private:
		::framework::resource::resource_manager const & resource_manager;
		::CComPtr<::IDxcUtils> dxc_utils;
	};
}