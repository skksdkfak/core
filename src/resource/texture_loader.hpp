#pragma once

#include "gpu/core.hpp"
#include "coroutine/task.hpp"

namespace framework::resource
{
	struct texture_load_callback
	{
		struct subresource_data
		{
			void const * data;
			::std::size_t row_pitch;
			::std::size_t slice_pitch;
			::std::uint32_t mip_level;
			::std::uint32_t array_layer;
		};

		struct texture_load_info
		{
			::std::uint32_t width;
			::std::uint32_t height;
			::framework::gpu::format format;
			::std::uint32_t mip_levels;
			::std::uint32_t subresource_count;
			::framework::resource::texture_load_callback::subresource_data const * subresource_data;
		};

		virtual ::framework::coroutine::lazy_task<void> operator()(::framework::resource::texture_load_callback::texture_load_info const & texture_load_info) = 0;
	};

	struct texture_load_info
	{
		char const * file_name;
		::framework::resource::texture_load_callback * texture_load_callback;
	};

	class texture_loader
	{
	public:
		virtual ::framework::coroutine::lazy_task<void> load_texture(::framework::resource::texture_load_info const & texture_load_info) = 0;
	};
}