#pragma once

namespace framework::resource
{
	enum class shader_language
	{
		spirv_1_5,
		dxil,
		glsl,
		hlsl,
		slang
	};

	enum class shader_environment
	{
		vulkan_1_3,
		d3d12
	};
}