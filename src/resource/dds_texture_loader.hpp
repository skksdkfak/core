#pragma once

#include "resource/texture_loader.hpp"

namespace framework::resource
{
	class resource_manager;

	class dds_texture_loader : public ::framework::resource::texture_loader
	{
	public:
		dds_texture_loader(::framework::resource::resource_manager const & resource_manager);

		::framework::coroutine::lazy_task<void> load_texture(::framework::resource::texture_load_info const & texture_load_info) override;

	private:
		::framework::resource::resource_manager const & resource_manager;
	};
}