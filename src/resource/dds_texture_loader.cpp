#include "resource/dds_texture_loader.hpp"
#include "resource/resource_manager.hpp"
#include <filesystem>
#include <fstream>

namespace
{

#pragma pack(push,1)
    typedef enum DXGI_FORMAT {
        DXGI_FORMAT_UNKNOWN = 0,
        DXGI_FORMAT_R32G32B32A32_TYPELESS = 1,
        DXGI_FORMAT_R32G32B32A32_FLOAT = 2,
        DXGI_FORMAT_R32G32B32A32_UINT = 3,
        DXGI_FORMAT_R32G32B32A32_SINT = 4,
        DXGI_FORMAT_R32G32B32_TYPELESS = 5,
        DXGI_FORMAT_R32G32B32_FLOAT = 6,
        DXGI_FORMAT_R32G32B32_UINT = 7,
        DXGI_FORMAT_R32G32B32_SINT = 8,
        DXGI_FORMAT_R16G16B16A16_TYPELESS = 9,
        DXGI_FORMAT_R16G16B16A16_FLOAT = 10,
        DXGI_FORMAT_R16G16B16A16_UNORM = 11,
        DXGI_FORMAT_R16G16B16A16_UINT = 12,
        DXGI_FORMAT_R16G16B16A16_SNORM = 13,
        DXGI_FORMAT_R16G16B16A16_SINT = 14,
        DXGI_FORMAT_R32G32_TYPELESS = 15,
        DXGI_FORMAT_R32G32_FLOAT = 16,
        DXGI_FORMAT_R32G32_UINT = 17,
        DXGI_FORMAT_R32G32_SINT = 18,
        DXGI_FORMAT_R32G8X24_TYPELESS = 19,
        DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20,
        DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21,
        DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22,
        DXGI_FORMAT_R10G10B10A2_TYPELESS = 23,
        DXGI_FORMAT_R10G10B10A2_UNORM = 24,
        DXGI_FORMAT_R10G10B10A2_UINT = 25,
        DXGI_FORMAT_R11G11B10_FLOAT = 26,
        DXGI_FORMAT_R8G8B8A8_TYPELESS = 27,
        DXGI_FORMAT_R8G8B8A8_UNORM = 28,
        DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29,
        DXGI_FORMAT_R8G8B8A8_UINT = 30,
        DXGI_FORMAT_R8G8B8A8_SNORM = 31,
        DXGI_FORMAT_R8G8B8A8_SINT = 32,
        DXGI_FORMAT_R16G16_TYPELESS = 33,
        DXGI_FORMAT_R16G16_FLOAT = 34,
        DXGI_FORMAT_R16G16_UNORM = 35,
        DXGI_FORMAT_R16G16_UINT = 36,
        DXGI_FORMAT_R16G16_SNORM = 37,
        DXGI_FORMAT_R16G16_SINT = 38,
        DXGI_FORMAT_R32_TYPELESS = 39,
        DXGI_FORMAT_D32_FLOAT = 40,
        DXGI_FORMAT_R32_FLOAT = 41,
        DXGI_FORMAT_R32_UINT = 42,
        DXGI_FORMAT_R32_SINT = 43,
        DXGI_FORMAT_R24G8_TYPELESS = 44,
        DXGI_FORMAT_D24_UNORM_S8_UINT = 45,
        DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46,
        DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47,
        DXGI_FORMAT_R8G8_TYPELESS = 48,
        DXGI_FORMAT_R8G8_UNORM = 49,
        DXGI_FORMAT_R8G8_UINT = 50,
        DXGI_FORMAT_R8G8_SNORM = 51,
        DXGI_FORMAT_R8G8_SINT = 52,
        DXGI_FORMAT_R16_TYPELESS = 53,
        DXGI_FORMAT_R16_FLOAT = 54,
        DXGI_FORMAT_D16_UNORM = 55,
        DXGI_FORMAT_R16_UNORM = 56,
        DXGI_FORMAT_R16_UINT = 57,
        DXGI_FORMAT_R16_SNORM = 58,
        DXGI_FORMAT_R16_SINT = 59,
        DXGI_FORMAT_R8_TYPELESS = 60,
        DXGI_FORMAT_R8_UNORM = 61,
        DXGI_FORMAT_R8_UINT = 62,
        DXGI_FORMAT_R8_SNORM = 63,
        DXGI_FORMAT_R8_SINT = 64,
        DXGI_FORMAT_A8_UNORM = 65,
        DXGI_FORMAT_R1_UNORM = 66,
        DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67,
        DXGI_FORMAT_R8G8_B8G8_UNORM = 68,
        DXGI_FORMAT_G8R8_G8B8_UNORM = 69,
        DXGI_FORMAT_BC1_TYPELESS = 70,
        DXGI_FORMAT_BC1_UNORM = 71,
        DXGI_FORMAT_BC1_UNORM_SRGB = 72,
        DXGI_FORMAT_BC2_TYPELESS = 73,
        DXGI_FORMAT_BC2_UNORM = 74,
        DXGI_FORMAT_BC2_UNORM_SRGB = 75,
        DXGI_FORMAT_BC3_TYPELESS = 76,
        DXGI_FORMAT_BC3_UNORM = 77,
        DXGI_FORMAT_BC3_UNORM_SRGB = 78,
        DXGI_FORMAT_BC4_TYPELESS = 79,
        DXGI_FORMAT_BC4_UNORM = 80,
        DXGI_FORMAT_BC4_SNORM = 81,
        DXGI_FORMAT_BC5_TYPELESS = 82,
        DXGI_FORMAT_BC5_UNORM = 83,
        DXGI_FORMAT_BC5_SNORM = 84,
        DXGI_FORMAT_B5G6R5_UNORM = 85,
        DXGI_FORMAT_B5G5R5A1_UNORM = 86,
        DXGI_FORMAT_B8G8R8A8_UNORM = 87,
        DXGI_FORMAT_B8G8R8X8_UNORM = 88,
        DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89,
        DXGI_FORMAT_B8G8R8A8_TYPELESS = 90,
        DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91,
        DXGI_FORMAT_B8G8R8X8_TYPELESS = 92,
        DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93,
        DXGI_FORMAT_BC6H_TYPELESS = 94,
        DXGI_FORMAT_BC6H_UF16 = 95,
        DXGI_FORMAT_BC6H_SF16 = 96,
        DXGI_FORMAT_BC7_TYPELESS = 97,
        DXGI_FORMAT_BC7_UNORM = 98,
        DXGI_FORMAT_BC7_UNORM_SRGB = 99,
        DXGI_FORMAT_AYUV = 100,
        DXGI_FORMAT_Y410 = 101,
        DXGI_FORMAT_Y416 = 102,
        DXGI_FORMAT_NV12 = 103,
        DXGI_FORMAT_P010 = 104,
        DXGI_FORMAT_P016 = 105,
        DXGI_FORMAT_420_OPAQUE = 106,
        DXGI_FORMAT_YUY2 = 107,
        DXGI_FORMAT_Y210 = 108,
        DXGI_FORMAT_Y216 = 109,
        DXGI_FORMAT_NV11 = 110,
        DXGI_FORMAT_AI44 = 111,
        DXGI_FORMAT_IA44 = 112,
        DXGI_FORMAT_P8 = 113,
        DXGI_FORMAT_A8P8 = 114,
        DXGI_FORMAT_B4G4R4A4_UNORM = 115,
        DXGI_FORMAT_P208 = 130,
        DXGI_FORMAT_V208 = 131,
        DXGI_FORMAT_V408 = 132,
        DXGI_FORMAT_SAMPLER_FEEDBACK_MIN_MIP_OPAQUE,
        DXGI_FORMAT_SAMPLER_FEEDBACK_MIP_REGION_USED_OPAQUE,
        DXGI_FORMAT_FORCE_UINT = 0xffffffff
    } ;
	const uint32_t DDS_MAGIC = 0x20534444; // "DDS "

	struct DDS_PIXELFORMAT
	{
		uint32_t    size;
		uint32_t    flags;
		uint32_t    fourCC;
		uint32_t    RGBBitCount;
		uint32_t    RBitMask;
		uint32_t    GBitMask;
		uint32_t    BBitMask;
		uint32_t    ABitMask;
	};

#define DDS_FOURCC      0x00000004  // DDPF_FOURCC
#define DDS_RGB         0x00000040  // DDPF_RGB
#define DDS_RGBA        0x00000041  // DDPF_RGB | DDPF_ALPHAPIXELS
#define DDS_LUMINANCE   0x00020000  // DDPF_LUMINANCE
#define DDS_LUMINANCEA  0x00020001  // DDPF_LUMINANCE | DDPF_ALPHAPIXELS
#define DDS_ALPHA       0x00000002  // DDPF_ALPHA
#define DDS_PAL8        0x00000020  // DDPF_PALETTEINDEXED8

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((uint32_t)(uint8_t)(ch0) | ((uint32_t)(uint8_t)(ch1) << 8) |       \
                ((uint32_t)(uint8_t)(ch2) << 16) | ((uint32_t)(uint8_t)(ch3) << 24 ))
#endif /* defined(MAKEFOURCC) */

	const DDS_PIXELFORMAT DDSPF_DXT1 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','1'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_DXT2 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','2'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_DXT3 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','3'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_DXT4 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','4'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_DXT5 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','5'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_BC4_UNORM =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('B','C','4','U'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_BC4_SNORM =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('B','C','4','S'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_BC5_UNORM =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('B','C','5','U'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_BC5_SNORM =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('B','C','5','S'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_R8G8_B8G8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('R','G','B','G'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_G8R8_G8B8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('G','R','G','B'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_YUY2 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('Y','U','Y','2'), 0, 0, 0, 0, 0 };

	const DDS_PIXELFORMAT DDSPF_A8R8G8B8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000 };

	const DDS_PIXELFORMAT DDSPF_X8R8G8B8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGB,  0, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000 };

	const DDS_PIXELFORMAT DDSPF_A8B8G8R8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000 };

	const DDS_PIXELFORMAT DDSPF_X8B8G8R8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGB,  0, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0x00000000 };

	const DDS_PIXELFORMAT DDSPF_G16R16 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGB,  0, 32, 0x0000ffff, 0xffff0000, 0x00000000, 0x00000000 };

	const DDS_PIXELFORMAT DDSPF_R5G6B5 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 16, 0x0000f800, 0x000007e0, 0x0000001f, 0x00000000 };

	const DDS_PIXELFORMAT DDSPF_A1R5G5B5 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00007c00, 0x000003e0, 0x0000001f, 0x00008000 };

	const DDS_PIXELFORMAT DDSPF_A4R4G4B4 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00000f00, 0x000000f0, 0x0000000f, 0x0000f000 };

	const DDS_PIXELFORMAT DDSPF_R8G8B8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 24, 0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000 };

	const DDS_PIXELFORMAT DDSPF_L8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_LUMINANCE, 0,  8, 0xff, 0x00, 0x00, 0x00 };

	const DDS_PIXELFORMAT DDSPF_L16 =
	{ sizeof(DDS_PIXELFORMAT), DDS_LUMINANCE, 0, 16, 0xffff, 0x0000, 0x0000, 0x0000 };

	const DDS_PIXELFORMAT DDSPF_A8L8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_LUMINANCEA, 0, 16, 0x00ff, 0x0000, 0x0000, 0xff00 };

	const DDS_PIXELFORMAT DDSPF_A8 =
	{ sizeof(DDS_PIXELFORMAT), DDS_ALPHA, 0, 8, 0x00, 0x00, 0x00, 0xff };

	// D3DFMT_A2R10G10B10/D3DFMT_A2B10G10R10 should be written using DX10 extension to avoid D3DX 10:10:10:2 reversal issue

	// This indicates the DDS_HEADER_DXT10 extension is present (the format is in dxgiFormat)
	const DDS_PIXELFORMAT DDSPF_DX10 =
	{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','1','0'), 0, 0, 0, 0, 0 };

#define DDS_HEADER_FLAGS_TEXTURE        0x00001007  // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT 
#define DDS_HEADER_FLAGS_MIPMAP         0x00020000  // DDSD_MIPMAPCOUNT
#define DDS_HEADER_FLAGS_VOLUME         0x00800000  // DDSD_DEPTH
#define DDS_HEADER_FLAGS_PITCH          0x00000008  // DDSD_PITCH
#define DDS_HEADER_FLAGS_LINEARSIZE     0x00080000  // DDSD_LINEARSIZE

#define DDS_HEIGHT 0x00000002 // DDSD_HEIGHT
#define DDS_WIDTH  0x00000004 // DDSD_WIDTH

#define DDS_SURFACE_FLAGS_TEXTURE 0x00001000 // DDSCAPS_TEXTURE
#define DDS_SURFACE_FLAGS_MIPMAP  0x00400008 // DDSCAPS_COMPLEX | DDSCAPS_MIPMAP
#define DDS_SURFACE_FLAGS_CUBEMAP 0x00000008 // DDSCAPS_COMPLEX

#define DDS_CUBEMAP_POSITIVEX 0x00000600 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX
#define DDS_CUBEMAP_NEGATIVEX 0x00000a00 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEX
#define DDS_CUBEMAP_POSITIVEY 0x00001200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEY
#define DDS_CUBEMAP_NEGATIVEY 0x00002200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEY
#define DDS_CUBEMAP_POSITIVEZ 0x00004200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEZ
#define DDS_CUBEMAP_NEGATIVEZ 0x00008200 // DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEZ

#define DDS_CUBEMAP_ALLFACES ( DDS_CUBEMAP_POSITIVEX | DDS_CUBEMAP_NEGATIVEX |\
                               DDS_CUBEMAP_POSITIVEY | DDS_CUBEMAP_NEGATIVEY |\
                               DDS_CUBEMAP_POSITIVEZ | DDS_CUBEMAP_NEGATIVEZ )

#define DDS_CUBEMAP 0x00000200 // DDSCAPS2_CUBEMAP

#define DDS_FLAGS_VOLUME 0x00200000 // DDSCAPS2_VOLUME

	// Subset here matches D3D10_RESOURCE_DIMENSION and D3D11_RESOURCE_DIMENSION
	enum DDS_RESOURCE_DIMENSION
	{
		DDS_DIMENSION_TEXTURE1D = 2,
		DDS_DIMENSION_TEXTURE2D = 3,
		DDS_DIMENSION_TEXTURE3D = 4,
	};

	// Subset here matches D3D10_RESOURCE_MISC_FLAG and D3D11_RESOURCE_MISC_FLAG
	enum DDS_RESOURCE_MISC_FLAG
	{
		DDS_RESOURCE_MISC_TEXTURECUBE = 0x4L,
	};

	enum DDS_MISC_FLAGS2
	{
		DDS_MISC_FLAGS2_ALPHA_MODE_MASK = 0x7L,
	};

	struct DDS_HEADER
	{
		uint32_t        size;
		uint32_t        flags;
		uint32_t        height;
		uint32_t        width;
		uint32_t        pitchOrLinearSize;
		uint32_t        depth; // only if DDS_HEADER_FLAGS_VOLUME is set in flags
		uint32_t        mipMapCount;
		uint32_t        reserved1[11];
		DDS_PIXELFORMAT ddspf;
		uint32_t        caps;
		uint32_t        caps2;
		uint32_t        caps3;
		uint32_t        caps4;
		uint32_t        reserved2;
	};

	struct DDS_HEADER_DXT10
	{
		DXGI_FORMAT     dxgiFormat;
		uint32_t        resourceDimension;
		uint32_t        miscFlag; // see D3D11_RESOURCE_MISC_FLAG
		uint32_t        arraySize;
		uint32_t        miscFlags2; // see DDS_MISC_FLAGS2
	};

#pragma pack(pop)

	static_assert(sizeof(DDS_HEADER) == 124, "DDS Header size mismatch");
	static_assert(sizeof(DDS_HEADER_DXT10) == 20, "DDS DX10 Extended Header size mismatch");

}; // namespace


//--------------------------------------------------------------------------------------
// Return the BPP for a particular format
//--------------------------------------------------------------------------------------
size_t BitsPerPixel(::framework::gpu::format format)
{
	switch (format)
	{
		//case DXGI_FORMAT_R32G32B32A32_TYPELESS:
		//case DXGI_FORMAT_R32G32B32A32_FLOAT:
		//case DXGI_FORMAT_R32G32B32A32_UINT:
		//case DXGI_FORMAT_R32G32B32A32_SINT:
		//	return 128;
		//
		//case DXGI_FORMAT_R32G32B32_TYPELESS:
		//case DXGI_FORMAT_R32G32B32_FLOAT:
		//case DXGI_FORMAT_R32G32B32_UINT:
		//case DXGI_FORMAT_R32G32B32_SINT:
		//	return 96;
		//
		//case DXGI_FORMAT_R16G16B16A16_TYPELESS:
		//case DXGI_FORMAT_R16G16B16A16_FLOAT:
		//case DXGI_FORMAT_R16G16B16A16_UNORM:
		//case DXGI_FORMAT_R16G16B16A16_UINT:
		//case DXGI_FORMAT_R16G16B16A16_SNORM:
		//case DXGI_FORMAT_R16G16B16A16_SINT:
		//case DXGI_FORMAT_R32G32_TYPELESS:
		//case DXGI_FORMAT_R32G32_FLOAT:
		//case DXGI_FORMAT_R32G32_UINT:
		//case DXGI_FORMAT_R32G32_SINT:
		//case DXGI_FORMAT_R32G8X24_TYPELESS:
		//case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		//case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		//case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
		//case DXGI_FORMAT_Y416:
		//case DXGI_FORMAT_Y210:
		//case DXGI_FORMAT_Y216:
		//	return 64;
		//
		//case DXGI_FORMAT_R10G10B10A2_TYPELESS:
		//case DXGI_FORMAT_R10G10B10A2_UNORM:
		//case DXGI_FORMAT_R10G10B10A2_UINT:
		//case DXGI_FORMAT_R11G11B10_FLOAT:
		//case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case ::framework::gpu::format::r8g8b8a8_unorm:
		//case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
		//case DXGI_FORMAT_R8G8B8A8_UINT:
		//case DXGI_FORMAT_R8G8B8A8_SNORM:
		//case DXGI_FORMAT_R8G8B8A8_SINT:
		//case DXGI_FORMAT_R16G16_TYPELESS:
		//case DXGI_FORMAT_R16G16_FLOAT:
		//case DXGI_FORMAT_R16G16_UNORM:
		//case DXGI_FORMAT_R16G16_UINT:
		//case DXGI_FORMAT_R16G16_SNORM:
		//case DXGI_FORMAT_R16G16_SINT:
		//case DXGI_FORMAT_R32_TYPELESS:
		//case DXGI_FORMAT_D32_FLOAT:
		//case DXGI_FORMAT_R32_FLOAT:
		//case DXGI_FORMAT_R32_UINT:
		//case DXGI_FORMAT_R32_SINT:
		//case DXGI_FORMAT_R24G8_TYPELESS:
		//case DXGI_FORMAT_D24_UNORM_S8_UINT:
		//case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
		//case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
		//case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
		//case DXGI_FORMAT_R8G8_B8G8_UNORM:
		//case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case ::framework::gpu::format::b8g8r8a8_unorm:
		//case DXGI_FORMAT_B8G8R8X8_UNORM:
		//case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
		//case DXGI_FORMAT_B8G8R8A8_TYPELESS:
		//case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
		//case DXGI_FORMAT_B8G8R8X8_TYPELESS:
		//case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
		//case DXGI_FORMAT_AYUV:
		//case DXGI_FORMAT_Y410:
		//case DXGI_FORMAT_YUY2:
		return 32;
		//
		//case DXGI_FORMAT_P010:
		//case DXGI_FORMAT_P016:
		//	return 24;
		//
		//case DXGI_FORMAT_R8G8_TYPELESS:
		//case DXGI_FORMAT_R8G8_UNORM:
		//case DXGI_FORMAT_R8G8_UINT:
		//case DXGI_FORMAT_R8G8_SNORM:
		//case DXGI_FORMAT_R8G8_SINT:
		//case DXGI_FORMAT_R16_TYPELESS:
		//case DXGI_FORMAT_R16_FLOAT:
		//case DXGI_FORMAT_D16_UNORM:
		//case DXGI_FORMAT_R16_UNORM:
		//case DXGI_FORMAT_R16_UINT:
		//case DXGI_FORMAT_R16_SNORM:
		//case DXGI_FORMAT_R16_SINT:
		//case DXGI_FORMAT_B5G6R5_UNORM:
		//case DXGI_FORMAT_B5G5R5A1_UNORM:
		//case DXGI_FORMAT_A8P8:
		//case DXGI_FORMAT_B4G4R4A4_UNORM:
		//	return 16;
		//
		//case DXGI_FORMAT_NV12:
		//case DXGI_FORMAT_420_OPAQUE:
		//case DXGI_FORMAT_NV11:
		//	return 12;
		//
		//case DXGI_FORMAT_R8_TYPELESS:
		//case DXGI_FORMAT_R8_UNORM:
		//case DXGI_FORMAT_R8_UINT:
		//case DXGI_FORMAT_R8_SNORM:
		//case DXGI_FORMAT_R8_SINT:
		//case DXGI_FORMAT_A8_UNORM:
		//case DXGI_FORMAT_AI44:
		//case DXGI_FORMAT_IA44:
		//case DXGI_FORMAT_P8:
		//	return 8;
		//
		//case DXGI_FORMAT_R1_UNORM:
		//	return 1;
		//
		//case DXGI_FORMAT_BC1_TYPELESS:
		//case DXGI_FORMAT_BC1_UNORM:
		//case DXGI_FORMAT_BC1_UNORM_SRGB:
		//case DXGI_FORMAT_BC4_TYPELESS:
		//case DXGI_FORMAT_BC4_UNORM:
		//case DXGI_FORMAT_BC4_SNORM:
		//	return 4;
		//
		//case DXGI_FORMAT_BC2_TYPELESS:
		//case DXGI_FORMAT_BC2_UNORM:
		//case DXGI_FORMAT_BC2_UNORM_SRGB:
		//case DXGI_FORMAT_BC3_TYPELESS:
		//case DXGI_FORMAT_BC3_UNORM:
		//case DXGI_FORMAT_BC3_UNORM_SRGB:
		//case DXGI_FORMAT_BC5_TYPELESS:
		//case DXGI_FORMAT_BC5_UNORM:
		//case DXGI_FORMAT_BC5_SNORM:
		//case DXGI_FORMAT_BC6H_TYPELESS:
		//case DXGI_FORMAT_BC6H_UF16:
		//case DXGI_FORMAT_BC6H_SF16:
		//case DXGI_FORMAT_BC7_TYPELESS:
		//case DXGI_FORMAT_BC7_UNORM:
		//case DXGI_FORMAT_BC7_UNORM_SRGB:
		//	return 8;
		//
	default:
		return 0;
	}
}


//--------------------------------------------------------------------------------------
// Get surface information for a particular format
//--------------------------------------------------------------------------------------
static void GetSurfaceInfo( size_t width,
							 size_t height,
							::framework::gpu::format format,
							 size_t * outNumBytes,
							 size_t * outRowBytes,
							 size_t * outNumRows)
{
	size_t numBytes = 0;
	size_t rowBytes = 0;
	size_t numRows = 0;

	bool bc = false;
	bool packed = false;
	bool planar = false;
	size_t bpe = 0;
	switch (format)
	{
	case ::framework::gpu::format::bc1_rgb_unorm_block:
	case ::framework::gpu::format::bc1_rgb_srgb_block:
	case ::framework::gpu::format::bc1_rgba_unorm_block:
	case ::framework::gpu::format::bc1_rgba_srgb_block:
	case ::framework::gpu::format::bc4_unorm_block:
	case ::framework::gpu::format::bc4_snorm_block:
		bc = true;
		bpe = 8;
		break;
	case ::framework::gpu::format::bc2_unorm_block:
	case ::framework::gpu::format::bc2_srgb_block:
	case ::framework::gpu::format::bc3_unorm_block:
	case ::framework::gpu::format::bc3_srgb_block:
	case ::framework::gpu::format::bc5_unorm_block:
	case ::framework::gpu::format::bc5_snorm_block:
	case ::framework::gpu::format::bc6h_ufloat_block:
	case ::framework::gpu::format::bc6h_sfloat_block:
	case ::framework::gpu::format::bc7_unorm_block:
	case ::framework::gpu::format::bc7_srgb_block:
		bc = true;
		bpe = 16;
		break;
	}
	//switch (format)
	//{
	//case DXGI_FORMAT_BC1_TYPELESS:
	//case DXGI_FORMAT_BC1_UNORM:
	//case DXGI_FORMAT_BC1_UNORM_SRGB:
	//case DXGI_FORMAT_BC4_TYPELESS:
	//case DXGI_FORMAT_BC4_UNORM:
	//case DXGI_FORMAT_BC4_SNORM:
	//	bc = true;
	//	bpe = 8;
	//	break;

	//case DXGI_FORMAT_BC2_TYPELESS:
	//case DXGI_FORMAT_BC2_UNORM:
	//case DXGI_FORMAT_BC2_UNORM_SRGB:
	//case DXGI_FORMAT_BC3_TYPELESS:
	//case DXGI_FORMAT_BC3_UNORM:
	//case DXGI_FORMAT_BC3_UNORM_SRGB:
	//case DXGI_FORMAT_BC5_TYPELESS:
	//case DXGI_FORMAT_BC5_UNORM:
	//case DXGI_FORMAT_BC5_SNORM:
	//case DXGI_FORMAT_BC6H_TYPELESS:
	//case DXGI_FORMAT_BC6H_UF16:
	//case DXGI_FORMAT_BC6H_SF16:
	//case DXGI_FORMAT_BC7_TYPELESS:
	//case DXGI_FORMAT_BC7_UNORM:
	//case DXGI_FORMAT_BC7_UNORM_SRGB:
	//	bc = true;
	//	bpe = 16;
	//	break;

	//case DXGI_FORMAT_R8G8_B8G8_UNORM:
	//case DXGI_FORMAT_G8R8_G8B8_UNORM:
	//case DXGI_FORMAT_YUY2:
	//	packed = true;
	//	bpe = 4;
	//	break;

	//case DXGI_FORMAT_Y210:
	//case DXGI_FORMAT_Y216:
	//	packed = true;
	//	bpe = 8;
	//	break;

	//case DXGI_FORMAT_NV12:
	//case DXGI_FORMAT_420_OPAQUE:
	//	planar = true;
	//	bpe = 2;
	//	break;

	//case DXGI_FORMAT_P010:
	//case DXGI_FORMAT_P016:
	//	planar = true;
	//	bpe = 4;
	//	break;

	//}

	if (bc)
	{
		size_t numBlocksWide = 0;
		if (width > 0)
		{
			numBlocksWide = std::max<size_t>(1, (width + 3) / 4);
		}
		size_t numBlocksHigh = 0;
		if (height > 0)
		{
			numBlocksHigh = std::max<size_t>(1, (height + 3) / 4);
		}
		rowBytes = numBlocksWide * bpe;
		numRows = numBlocksHigh;
		numBytes = rowBytes * numBlocksHigh;
	}
	else if (packed)
	{
		rowBytes = ((width + 1) >> 1) * bpe;
		numRows = height;
		numBytes = rowBytes * height;
	}
	//else if (fmt == DXGI_FORMAT_NV11)
	//{
	//	rowBytes = ((width + 3) >> 2) * 4;
	//	numRows = height * 2; // Direct3D makes this simplifying assumption, although it is larger than the 4:1:1 data
	//	numBytes = rowBytes * numRows;
	//}
	else if (planar)
	{
		rowBytes = ((width + 1) >> 1) * bpe;
		numBytes = (rowBytes * height) + ((rowBytes * height + 1) >> 1);
		numRows = height + ((height + 1) >> 1);
	}
	else
	{
		size_t bpp = BitsPerPixel(format);
		rowBytes = (width * bpp + 7) / 8; // round up to nearest byte
		numRows = height;
		numBytes = rowBytes * height;
	}

	if (outNumBytes)
	{
		*outNumBytes = numBytes;
	}
	if (outRowBytes)
	{
		*outRowBytes = rowBytes;
	}
	if (outNumRows)
	{
		*outNumRows = numRows;
	}
}


//--------------------------------------------------------------------------------------
#define ISBITMASK( r,g,b,a ) ( ddpf.RBitMask == r && ddpf.GBitMask == g && ddpf.BBitMask == b && ddpf.ABitMask == a )

static ::framework::gpu::format GetDXGIFormat(const DDS_PIXELFORMAT & ddpf)
{
	if (ddpf.flags & DDS_RGB)
	{
		// Note that sRGB formats are written using the "DX10" extended header

		switch (ddpf.RGBBitCount)
		{
		case 32:
			if (ISBITMASK(0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000))
			{
				//return DXGI_FORMAT_R8G8B8A8_UNORM;
				return ::framework::gpu::format::r8g8b8a8_unorm;
			}

			if (ISBITMASK(0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000))
			{
				//return DXGI_FORMAT_B8G8R8A8_UNORM;
				return ::framework::gpu::format::b8g8r8a8_unorm;
			}

			if (ISBITMASK(0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000))
			{
				//return DXGI_FORMAT_B8G8R8X8_UNORM;
				return ::framework::gpu::format::b8g8r8a8_unorm;
			}

			// No DXGI format maps to ISBITMASK(0x000000ff,0x0000ff00,0x00ff0000,0x00000000) aka D3DFMT_X8B8G8R8

			// Note that many common DDS reader/writers (including D3DX) swap the
			// the RED/BLUE masks for 10:10:10:2 formats. We assumme
			// below that the 'backwards' header mask is being used since it is most
			// likely written by D3DX. The more robust solution is to use the 'DX10'
			// header extension and specify the DXGI_FORMAT_R10G10B10A2_UNORM format directly

			// For 'correct' writers, this should be 0x000003ff,0x000ffc00,0x3ff00000 for RGB data
			if (ISBITMASK(0x3ff00000, 0x000ffc00, 0x000003ff, 0xc0000000))
			{
				//return DXGI_FORMAT_R10G10B10A2_UNORM;
				return ::framework::gpu::format::a2r10g10b10_unorm_pack32;
			}

			// No DXGI format maps to ISBITMASK(0x000003ff,0x000ffc00,0x3ff00000,0xc0000000) aka D3DFMT_A2R10G10B10

			if (ISBITMASK(0x0000ffff, 0xffff0000, 0x00000000, 0x00000000))
			{
				//return DXGI_FORMAT_R16G16_UNORM;
				return ::framework::gpu::format::r16g16_unorm;
			}

			if (ISBITMASK(0xffffffff, 0x00000000, 0x00000000, 0x00000000))
			{
				// Only 32-bit color channel format in D3D9 was R32F
				//return DXGI_FORMAT_R32_FLOAT; // D3DX writes this out as a FourCC of 114
				return ::framework::gpu::format::r32_sfloat; // D3DX writes this out as a FourCC of 114
			}
			break;

			//case 24:
			//	// No 24bpp DXGI formats aka D3DFMT_R8G8B8
			//	break;

			//case 16:
			//	if (ISBITMASK(0x7c00, 0x03e0, 0x001f, 0x8000))
			//	{
			//		return DXGI_FORMAT_B5G5R5A1_UNORM;
			//	}
			//	if (ISBITMASK(0xf800, 0x07e0, 0x001f, 0x0000))
			//	{
			//		return DXGI_FORMAT_B5G6R5_UNORM;
			//	}

			//	// No DXGI format maps to ISBITMASK(0x7c00,0x03e0,0x001f,0x0000) aka D3DFMT_X1R5G5B5

			//	if (ISBITMASK(0x0f00, 0x00f0, 0x000f, 0xf000))
			//	{
			//		return DXGI_FORMAT_B4G4R4A4_UNORM;
			//	}

			//	// No DXGI format maps to ISBITMASK(0x0f00,0x00f0,0x000f,0x0000) aka D3DFMT_X4R4G4B4

			//	// No 3:3:2, 3:3:2:8, or paletted DXGI formats aka D3DFMT_A8R3G3B2, D3DFMT_R3G3B2, D3DFMT_P8, D3DFMT_A8P8, etc.
			//	break;
			assert(false);
		}
	}
	else if (ddpf.flags & DDS_LUMINANCE)
	{
		//	if (8 == ddpf.RGBBitCount)
		//	{
		//		if (ISBITMASK(0x000000ff, 0x00000000, 0x00000000, 0x00000000))
		//		{
		//			return DXGI_FORMAT_R8_UNORM; // D3DX10/11 writes this out as DX10 extension
		//		}

		//		// No DXGI format maps to ISBITMASK(0x0f,0x00,0x00,0xf0) aka D3DFMT_A4L4
		//	}

		//	if (16 == ddpf.RGBBitCount)
		//	{
		//		if (ISBITMASK(0x0000ffff, 0x00000000, 0x00000000, 0x00000000))
		//		{
		//			return DXGI_FORMAT_R16_UNORM; // D3DX10/11 writes this out as DX10 extension
		//		}
		//		if (ISBITMASK(0x000000ff, 0x00000000, 0x00000000, 0x0000ff00))
		//		{
		//			return DXGI_FORMAT_R8G8_UNORM; // D3DX10/11 writes this out as DX10 extension
		//		}
		//	}
		assert(false);
	}
	else if (ddpf.flags & DDS_ALPHA)
	{
		//	if (8 == ddpf.RGBBitCount)
		//	{
		//		return DXGI_FORMAT_A8_UNORM;
		//	}
		assert(false);
	}
	else if (ddpf.flags & DDS_FOURCC)
	{
		if (MAKEFOURCC('D', 'X', 'T', '1') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc1_rgba_unorm_block;
		}
		if (MAKEFOURCC('D', 'X', 'T', '3') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc2_unorm_block;
		}
		if (MAKEFOURCC('D', 'X', 'T', '5') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc3_unorm_block;
		}

		// While pre-mulitplied alpha isn't directly supported by the DXGI formats,
		// they are basically the same as these BC formats so they can be mapped
		if (MAKEFOURCC('D', 'X', 'T', '2') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc2_unorm_block;
		}
		if (MAKEFOURCC('D', 'X', 'T', '4') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc3_unorm_block;
		}

		if (MAKEFOURCC('A', 'T', 'I', '1') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc4_unorm_block;
		}
		if (MAKEFOURCC('B', 'C', '4', 'U') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc4_unorm_block;
		}
		if (MAKEFOURCC('B', 'C', '4', 'S') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc4_snorm_block;
		}

		if (MAKEFOURCC('A', 'T', 'I', '2') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc5_unorm_block;
		}
		if (MAKEFOURCC('B', 'C', '5', 'U') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc5_unorm_block;
		}
		if (MAKEFOURCC('B', 'C', '5', 'S') == ddpf.fourCC)
		{
			return ::framework::gpu::format::bc5_snorm_block;
		}

		// BC6H and BC7 are written using the "DX10" extended header

		//if (MAKEFOURCC('R', 'G', 'B', 'G') == ddpf.fourCC)
		//{
		//	return DXGI_FORMAT_R8G8_B8G8_UNORM;
		//}
		//if (MAKEFOURCC('G', 'R', 'G', 'B') == ddpf.fourCC)
		//{
		//	return DXGI_FORMAT_G8R8_G8B8_UNORM;
		//}

		//if (MAKEFOURCC('Y', 'U', 'Y', '2') == ddpf.fourCC)
		//{
		//	return DXGI_FORMAT_YUY2;
		//}

		//// Check for D3DFORMAT enums being set here
		//switch (ddpf.fourCC)
		//{
		//case 36: // D3DFMT_A16B16G16R16
		//	return DXGI_FORMAT_R16G16B16A16_UNORM;

		//case 110: // D3DFMT_Q16W16V16U16
		//	return DXGI_FORMAT_R16G16B16A16_SNORM;

		//case 111: // D3DFMT_R16F
		//	return DXGI_FORMAT_R16_FLOAT;

		//case 112: // D3DFMT_G16R16F
		//	return DXGI_FORMAT_R16G16_FLOAT;

		//case 113: // D3DFMT_A16B16G16R16F
		//	return DXGI_FORMAT_R16G16B16A16_FLOAT;

		//case 114: // D3DFMT_R32F
		//	return DXGI_FORMAT_R32_FLOAT;

		//case 115: // D3DFMT_G32R32F
		//	return DXGI_FORMAT_R32G32_FLOAT;

		//case 116: // D3DFMT_A32B32G32R32F
		//	return DXGI_FORMAT_R32G32B32A32_FLOAT;
		//}
		assert(false);
	}

	return ::framework::gpu::format::undefined;
}

::framework::resource::dds_texture_loader::dds_texture_loader(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
}

::framework::coroutine::lazy_task<void>(::framework::resource::dds_texture_loader::load_texture)(::framework::resource::texture_load_info const & texture_load_info)
{
	::std::filesystem::path file_path;
	if (!this->resource_manager.find_file(file_path, texture_load_info.file_name))
	{
		co_return;
	}

	::std::ifstream ifstream(file_path, ::std::ios::binary);
	if (!ifstream.is_open())
	{
		co_return;
	}

	::std::vector<char> texture_data(::std::istreambuf_iterator<char>(ifstream), {});

	auto header = reinterpret_cast<const DDS_HEADER *>(texture_data.data() + sizeof(uint32_t));

	size_t offset = sizeof(DDS_HEADER) + sizeof(uint32_t);

	// Check for extensions
	if (header->ddspf.flags & DDS_FOURCC)
	{
		if (MAKEFOURCC('D', 'X', '1', '0') == header->ddspf.fourCC)
			offset += sizeof(DDS_HEADER_DXT10);
	}

	char const * bitData = texture_data.data() + offset;
	size_t bitSize = texture_data.size() - offset;
	::std::uint32_t width = header->width;
	::std::uint32_t height = header->height;
	::std::uint32_t depth = header->depth;

	//uint32_t resDim = D3D12_RESOURCE_DIMENSION_UNKNOWN;
	::std::uint32_t arraySize = 1;
	::framework::gpu::format format = ::framework::gpu::format::undefined;
	bool isCubeMap = false;

	size_t mip_levels = header->mipMapCount;
	if (0 == mip_levels)
	{
		mip_levels = 1;
	}

	if ((header->ddspf.flags & DDS_FOURCC) && (MAKEFOURCC('D', 'X', '1', '0') == header->ddspf.fourCC))
	{
		auto d3d10ext = reinterpret_cast<const DDS_HEADER_DXT10 *>((const char *)header + sizeof(DDS_HEADER));

		arraySize = d3d10ext->arraySize;
		if (arraySize == 0)
		{
			co_return;
		}

		switch (d3d10ext->dxgiFormat)
		{
		case DXGI_FORMAT_BC6H_UF16:
			format = ::framework::gpu::format::bc6h_ufloat_block;
			break;
		case DXGI_FORMAT_AI44:
		case DXGI_FORMAT_IA44:
		case DXGI_FORMAT_P8:
		case DXGI_FORMAT_A8P8:
			co_return;
			//default:
				//if (BitsPerPixel(d3d10ext->dxgiFormat) == 0)
				//{
				//	co_return;
				//}
		}

		switch (d3d10ext->resourceDimension)
		{
		case 2:
			// D3DX writes 1D textures with a fixed Height of 1
			if ((header->flags & DDS_HEIGHT) && height != 1)
			{
			}
			height = depth = 1;
			break;

		case 3:
			if (d3d10ext->miscFlag & DDS_RESOURCE_MISC_TEXTURECUBE)
			{
				arraySize *= 6;
				isCubeMap = true;
			}
			depth = 1;
			break;

		case 4:
			if (!(header->flags & DDS_HEADER_FLAGS_VOLUME))
			{
			}

			if (arraySize > 1)
			{
			}
			break;
		}

		//resDim = d3d10ext->resourceDimension;
	}
	else
	{
		format = GetDXGIFormat(header->ddspf);

		if (header->flags & DDS_HEADER_FLAGS_VOLUME)
		{
			//resDim = D3D12_RESOURCE_DIMENSION_TEXTURE3D;
		}
		else
		{
			if (header->caps2 & DDS_CUBEMAP)
			{
				// We require all six faces to be defined
				if ((header->caps2 & DDS_CUBEMAP_ALLFACES) != DDS_CUBEMAP_ALLFACES)
				{
				}

				arraySize = 6;
				isCubeMap = true;
			}

			depth = 1;
			//resDim = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

			// Note there's no way for a legacy Direct3D 9 DDS to express a '1D' texture
		}
	}

	// Bound sizes (for security purposes we don't trust DDS file metadata larger than the D3D 11.x hardware requirements)
//	if (mip_levels > D3D12_REQ_MIP_LEVELS)
//	{
//	}

//	switch (resDim)
//	{
//	case D3D12_RESOURCE_DIMENSION_TEXTURE1D:
//		if ((arraySize > D3D12_REQ_TEXTURE1D_ARRAY_AXIS_DIMENSION) ||
//			(width > D3D12_REQ_TEXTURE1D_U_DIMENSION))
//		{
//		}
//		break;
//
//	case D3D12_RESOURCE_DIMENSION_TEXTURE2D:
//		if (isCubeMap)
//		{
//			// This is the right bound because we set arraySize to (NumCubes*6) above
//			if ((arraySize > D3D12_REQ_TEXTURE2D_ARRAY_AXIS_DIMENSION) ||
//				(width > D3D12_REQ_TEXTURECUBE_DIMENSION) ||
//				(height > D3D12_REQ_TEXTURECUBE_DIMENSION))
//			{
//			}
//		}
//		else if ((arraySize > D3D12_REQ_TEXTURE2D_ARRAY_AXIS_DIMENSION) ||
//					(width > D3D12_REQ_TEXTURE2D_U_OR_V_DIMENSION) ||
//					(height > D3D12_REQ_TEXTURE2D_U_OR_V_DIMENSION))
//		{
//		}
//		break;
//
//	case D3D12_RESOURCE_DIMENSION_TEXTURE3D:
//		if ((arraySize > 1) ||
//			(width > D3D12_REQ_TEXTURE3D_U_V_OR_W_DIMENSION) ||
//			(height > D3D12_REQ_TEXTURE3D_U_V_OR_W_DIMENSION) ||
//			(depth > D3D12_REQ_TEXTURE3D_U_V_OR_W_DIMENSION))
//		{
//		}
//		break;
//	}

	// Create the texture
	::std::uint32_t subresource_count = static_cast<::std::uint32_t>(mip_levels) * arraySize;
	::std::unique_ptr<::framework::resource::texture_load_callback::subresource_data[]> subresource_data(new (::std::nothrow) ::framework::resource::texture_load_callback::subresource_data[subresource_count]);

	size_t NumBytes = 0;
	size_t RowBytes = 0;
	size_t NumRows = 0;
	char const * pSrcBits = bitData;

	size_t index = 0;
	for (size_t j = 0; j < arraySize; j++)
	{
		size_t w = width;
		size_t h = height;
		size_t d = depth;
		for (size_t i = 0; i < mip_levels; i++)
		{
			GetSurfaceInfo(w, h, format, &NumBytes, &RowBytes, &NumRows);

			assert(index < mip_levels * arraySize);
			subresource_data[index].data = (const void *)pSrcBits;
			subresource_data[index].row_pitch = RowBytes;
			subresource_data[index].slice_pitch = NumBytes;
			subresource_data[index].mip_level = i;
			subresource_data[index].array_layer = j;

			++index;

			pSrcBits += NumBytes * d;

			w = w >> 1;
			h = h >> 1;
			d = d >> 1;
			if (w == 0)
			{
				w = 1;
			}
			if (h == 0)
			{
				h = 1;
			}
			if (d == 0)
			{
				d = 1;
			}
		}
	}

	::framework::resource::texture_load_callback::texture_load_info texture_load_callback_info;
	texture_load_callback_info.width = width;
	texture_load_callback_info.height = height;
	texture_load_callback_info.format = format;
	texture_load_callback_info.mip_levels = mip_levels;
	texture_load_callback_info.subresource_count = subresource_count;
	texture_load_callback_info.subresource_data = subresource_data.get();
	auto texture_load_callback = (*texture_load_info.texture_load_callback)(texture_load_callback_info);
	co_await texture_load_callback;

	co_return;
}
