#include "resource/open_exr_texture_loader.hpp"
#include "resource/resource_manager.hpp"
#include <ImfRgbaFile.h>
#include <fstream>
#include <filesystem>

::framework::resource::open_exr_texture_loader::open_exr_texture_loader(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
}

::framework::coroutine::lazy_task<void>(::framework::resource::open_exr_texture_loader::load_texture)(::framework::resource::texture_load_info const & texture_load_info)
{
	::std::filesystem::path file_path;
	if (!this->resource_manager.find_file(file_path, texture_load_info.file_name))
	{
		co_return;
	}

	::std::ifstream ifstream(file_path, ::std::ios::binary);
	if (!ifstream.is_open())
	{
		co_return;
	}

	::Imf::RgbaInputFile file(file_path.string().c_str());
	::Imath::Box2i dw = file.dataWindow();

	::std::uint32_t width = dw.max.x - dw.min.x + 1;
	::std::uint32_t height = dw.max.y - dw.min.y + 1;

	::std::vector<::Imf::Rgba> pixels(width * height);
	file.setFrameBuffer(&pixels[0] - dw.min.x - dw.min.y * width, 1, width);
	file.readPixels(dw.min.y, dw.max.y);

	::framework::resource::texture_load_callback::subresource_data subresource_data;
	subresource_data.data = pixels.data();
	subresource_data.row_pitch = 8 * width;
	subresource_data.slice_pitch = 8 * width * height;
	subresource_data.mip_level = 0;
	subresource_data.array_layer = 0;

	::framework::resource::texture_load_callback::texture_load_info texture_load_callback_info;
	texture_load_callback_info.width = static_cast<::std::uint32_t>(width);
	texture_load_callback_info.height = static_cast<::std::uint32_t>(height);
	texture_load_callback_info.format = ::framework::gpu::format::r16g16b16a16_sfloat;
	texture_load_callback_info.mip_levels = 1;
	texture_load_callback_info.subresource_count = 1;
	texture_load_callback_info.subresource_data = &subresource_data;
	auto texture_load_callback = texture_load_info.texture_load_callback->operator()(texture_load_callback_info);
	co_await texture_load_callback;

	co_return;
}