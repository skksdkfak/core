#pragma once

#include "resource/shader_compiler.hpp"

namespace framework::resource
{
	class resource_manager;
	class glslang_compiler;

	class glslang_program_reflection : public ::framework::resource::program_reflection
	{
	public:
	};

	class glslang_program : public ::framework::resource::program
	{
	public:
		glslang_program(::framework::resource::glslang_compiler & glslang_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info);

		::std::size_t get_code_size() const override;

		void const * get_code() const override;

		::framework::resource::program_reflection * create_program_reflection() const override;

	private:
		::std::vector<::std::uint32_t> spirv_code;
	};

	class glslang_compiler : public ::framework::resource::shader_compiler
	{
	public:
		glslang_compiler(::framework::resource::resource_manager const & resource_manager);

		::framework::coroutine::lazy_task<::framework::resource::program *> compile(::framework::resource::shader_compilation_info const & shader_compilation_info) override;

		void get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages) override;

		void get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages) override;

		void get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments) override;

		::framework::resource::resource_manager const & get_resource_manager() const
		{
			return this->resource_manager;
		}

	private:
		::framework::resource::resource_manager const & resource_manager;
	};
}