#include "resource/slang_compiler.hpp"
#include "resource/resource_manager.hpp"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

::framework::resource::slang_compiler::slang_compiler(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
	::slang::createGlobalSession(this->slang_global_session.writeRef());
}

::framework::coroutine::lazy_task<::framework::resource::program *>(::framework::resource::slang_compiler::compile)(::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	co_return new ::framework::resource::slang_program(*this, shader_compilation_info);
}

void ::framework::resource::slang_compiler::get_source_shader_languages(::std::uint32_t & source_shader_language_count, ::framework::resource::shader_language * source_shader_languages)
{
	::framework::resource::shader_language const supported_source_shader_languages[]
	{
		::framework::resource::shader_language::hlsl,
		::framework::resource::shader_language::slang
	};

	if (source_shader_languages)
	{
		::std::memcpy(source_shader_languages, supported_source_shader_languages, ::std::min(static_cast<::std::size_t>(source_shader_language_count), ::std::size(supported_source_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		source_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_source_shader_languages));
	}
}

void ::framework::resource::slang_compiler::get_target_shader_languages(::std::uint32_t & target_shader_language_count, ::framework::resource::shader_language * target_shader_languages)
{
	::framework::resource::shader_language const supported_target_shader_languages[]
	{
		::framework::resource::shader_language::spirv_1_5,
		::framework::resource::shader_language::dxil,
		::framework::resource::shader_language::glsl,
		::framework::resource::shader_language::hlsl
	};

	if (target_shader_languages)
	{
		::std::memcpy(target_shader_languages, supported_target_shader_languages, ::std::min(static_cast<::std::size_t>(target_shader_language_count), ::std::size(supported_target_shader_languages)) * sizeof(::framework::resource::shader_language));
	}
	else
	{
		target_shader_language_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_languages));
	}
}

void ::framework::resource::slang_compiler::get_target_shader_environments(::std::uint32_t & target_shader_environment_count, ::framework::resource::shader_environment * target_shader_environments)
{
	::framework::resource::shader_environment const supported_target_shader_environments[]
	{
		::framework::resource::shader_environment::vulkan_1_3,
		::framework::resource::shader_environment::d3d12
	};

	if (target_shader_environments)
	{
		::std::memcpy(target_shader_environments, supported_target_shader_environments, ::std::min(static_cast<::std::size_t>(target_shader_environment_count), ::std::size(supported_target_shader_environments)) * sizeof(::framework::resource::shader_environment));
	}
	else
	{
		target_shader_environment_count = static_cast<::std::uint32_t>(::std::size(supported_target_shader_environments));
	}
}

::framework::resource::slang_program::slang_program(::framework::resource::slang_compiler & slang_compiler, ::framework::resource::shader_compilation_info const & shader_compilation_info)
{
	::slang::SessionDesc session_desc = {};
	::slang::TargetDesc target_desc = {};

	::SlangSourceLanguage source_language;
	switch (shader_compilation_info.source_shader_language)
	{
	case ::framework::resource::shader_language::glsl:
		source_language = ::SlangSourceLanguage::SLANG_SOURCE_LANGUAGE_GLSL;
		break;
	case ::framework::resource::shader_language::hlsl:
		source_language = ::SlangSourceLanguage::SLANG_SOURCE_LANGUAGE_HLSL;
		break;
	case ::framework::resource::shader_language::slang:
		source_language = ::SlangSourceLanguage::SLANG_SOURCE_LANGUAGE_SLANG;
		break;
	default:
		throw ::std::exception();
	}

	switch (shader_compilation_info.target_shader_language)
	{
	case ::framework::resource::shader_language::spirv_1_5:
		target_desc.format = ::SlangCompileTarget::SLANG_SPIRV;
		target_desc.profile = slang_compiler.slang_global_session->findProfile("glsl_460");
		target_desc.forceGLSLScalarBufferLayout = true;
		break;
	case ::framework::resource::shader_language::dxil:
		target_desc.format = ::SlangCompileTarget::SLANG_DXIL;
		target_desc.profile = slang_compiler.slang_global_session->findProfile("lib_6_6");
		break;
	case ::framework::resource::shader_language::glsl:
		target_desc.format = ::SlangCompileTarget::SLANG_GLSL;
		target_desc.profile = slang_compiler.slang_global_session->findProfile("glsl_460");
		break;
	case ::framework::resource::shader_language::hlsl:
		target_desc.format = ::SlangCompileTarget::SLANG_HLSL;
		target_desc.profile = slang_compiler.slang_global_session->findProfile("lib_6_6");
		break;
	default:
		throw ::std::exception();
	}

	session_desc.targets = &target_desc;
	session_desc.targetCount = 1;

	::std::vector<::std::filesystem::path> const & search_directories = slang_compiler.resource_manager.get_search_directories();
	::std::vector<::std::string> search_paths_strings(search_directories.size());
	::std::vector<char const *> search_paths(search_directories.size());
	for (::std::size_t i = 0; i < search_directories.size(); i++)
	{
		search_paths_strings[i] = search_directories[i].string();
		search_paths[i] = search_paths_strings[i].c_str();
	}
	session_desc.searchPaths = search_paths.data();
	session_desc.searchPathCount = search_paths.size();

	::SlangResult slang_result;
	::Slang::ComPtr<::slang::ISession> session;
	slang_result = slang_compiler.slang_global_session->createSession(session_desc, session.writeRef());
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}

	::Slang::ComPtr<::ISlangBlob> diagnostic_blob;
	auto path_str = shader_compilation_info.source_file_path.filename().string();
	char const * module_name = path_str.c_str();

	::Slang::ComPtr<::slang::ICompileRequest> compile_request;
	slang_result = session->createCompileRequest(compile_request.writeRef());
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}

	int target_index = compile_request->addCodeGenTarget(target_desc.format);
	compile_request->setTargetProfile(target_index, target_desc.profile);
	compile_request->setTargetForceGLSLScalarBufferLayout(target_index, true);
	compile_request->setCompileFlags(::SLANG_COMPILE_FLAG_NO_CODEGEN);
	compile_request->setMatrixLayoutMode(::SlangMatrixLayoutMode::SLANG_MATRIX_LAYOUT_ROW_MAJOR);
	for (::std::uint32_t i = 0; i < shader_compilation_info.preprocessor_define_count; i++)
	{
		compile_request->addPreprocessorDefine(shader_compilation_info.preprocessor_defines[i].name, shader_compilation_info.preprocessor_defines[i].value);
	}
	char const * command_line_arguments[] =
	{
		"-Xdxc", "-no-legacy-cbuf-layout", // todo: does not work
		"-Xdxc", "-not_use_legacy_cbuf_load"
	};
	slang_result = compile_request->processCommandLineArguments(command_line_arguments, static_cast<int>(::std::size(command_line_arguments)));
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}

	int translation_unit = compile_request->addTranslationUnit(source_language, module_name);
	compile_request->addTranslationUnitSourceStringSpan(translation_unit, module_name, static_cast<char const *>(shader_compilation_info.code), static_cast<char const *>(shader_compilation_info.code) + shader_compilation_info.code_size);
	::SlangStage slang_stage;
	switch (shader_compilation_info.stage)
	{
	case ::framework::gpu::shader_stage_flags::vertex_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_VERTEX;
		break;
	case ::framework::gpu::shader_stage_flags::tessellation_control_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_HULL;
		break;
	case ::framework::gpu::shader_stage_flags::tessellation_evaluation_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_DOMAIN;
		break;
	case ::framework::gpu::shader_stage_flags::geometry_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_GEOMETRY;
		break;
	case ::framework::gpu::shader_stage_flags::fragment_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_FRAGMENT;
		break;
	case ::framework::gpu::shader_stage_flags::compute_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_COMPUTE;
		break;
	case ::framework::gpu::shader_stage_flags::raygen_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_RAY_GENERATION;
		break;
	case ::framework::gpu::shader_stage_flags::any_hit_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_ANY_HIT;
		break;
	case ::framework::gpu::shader_stage_flags::closest_hit_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_CLOSEST_HIT;
		break;
	case ::framework::gpu::shader_stage_flags::miss_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_MISS;
		break;
	case ::framework::gpu::shader_stage_flags::intersection_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_INTERSECTION;
		break;
	case ::framework::gpu::shader_stage_flags::callable_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_CALLABLE;
		break;
	case ::framework::gpu::shader_stage_flags::task_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_AMPLIFICATION;
		break;
	case ::framework::gpu::shader_stage_flags::mesh_bit:
		slang_stage = ::SlangStage::SLANG_STAGE_MESH;
		break;
	default:
		throw ::std::exception();
	}
	int entry_point = compile_request->addEntryPoint(translation_unit, shader_compilation_info.entry_point, slang_stage);

	slang_result = compile_request->compile();
	if (SLANG_FAILED(slang_result))
	{
		slang_result = compile_request->getDiagnosticOutputBlob(diagnostic_blob.writeRef());
		if (SLANG_SUCCEEDED(slang_result))
		{
			if (diagnostic_blob)
			{
				::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
			}
		}

		throw ::std::exception();
	}

	slang_result = compile_request->getDiagnosticOutputBlob(diagnostic_blob.writeRef());
	if (diagnostic_blob)
	{
		::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
	}
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}
	int dependency_file_count = compile_request->getDependencyFileCount();
	for (int dependency_index = 0; dependency_index < dependency_file_count; dependency_index++)
	{
		shader_compilation_info.shader_compile_include_handler->notify_dependency(compile_request->getDependencyFilePath(dependency_index));
	}

	::Slang::ComPtr<::slang::IComponentType> etnry_point_component;
	::Slang::ComPtr<::slang::IComponentType> type_conformances_composite_component;
	::Slang::ComPtr<::slang::IComponentType> type_comformance_specialized_entry_point;
	::Slang::ComPtr<::slang::IComponentType> linked_entry_point;
	::std::vector<::Slang::ComPtr<::slang::ITypeConformance>> type_conformance_components(shader_compilation_info.type_conformance_count);
	::std::vector<::slang::IComponentType *> raw_type_conformance_components(shader_compilation_info.type_conformance_count);

	compile_request->getEntryPoint(entry_point, etnry_point_component.writeRef());
	compile_request->getProgram(this->program.writeRef());

	if (shader_compilation_info.type_conformance_count > 0)
	{
		::slang::ProgramLayout * program_layout = this->program->getLayout();
		for (::std::uint32_t i = 0; i < shader_compilation_info.type_conformance_count; i++)
		{
			::slang::TypeReflection * slang_type = program_layout->findTypeByName(shader_compilation_info.type_conformances[i].type_name);
			::slang::TypeReflection * slang_interface_type = program_layout->findTypeByName(shader_compilation_info.type_conformances[i].interface_name);
			slang_result = session->createTypeConformanceComponentType(slang_type, slang_interface_type, type_conformance_components[i].writeRef(), shader_compilation_info.type_conformances[i].id, diagnostic_blob.writeRef());
			raw_type_conformance_components[i] = type_conformance_components[i].get();
			if (diagnostic_blob)
			{
				::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
			}
			if (SLANG_FAILED(slang_result))
			{
				throw ::std::exception();
			}
		}

		slang_result = session->createCompositeComponentType(raw_type_conformance_components.data(), static_cast<::std::uint32_t>(raw_type_conformance_components.size()), type_conformances_composite_component.writeRef(), diagnostic_blob.writeRef());
		if (diagnostic_blob)
		{
			::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
		}
		if (SLANG_FAILED(slang_result))
		{
			throw ::std::exception();
		}

		::slang::IComponentType * component_types[2];
		component_types[0] = etnry_point_component;
		component_types[1] = type_conformances_composite_component;
		slang_result = session->createCompositeComponentType(component_types, 2, type_comformance_specialized_entry_point.writeRef(), diagnostic_blob.writeRef());
		if (diagnostic_blob)
		{
			::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
		}
		if (SLANG_FAILED(slang_result))
		{
			throw ::std::exception();
		}
	}
	else
	{
		type_comformance_specialized_entry_point = etnry_point_component;
	}

	::slang::IComponentType * component_types[2];
	component_types[0] = this->program;
	component_types[1] = type_comformance_specialized_entry_point;
	slang_result = session->createCompositeComponentType(component_types, 2, linked_entry_point.writeRef(), diagnostic_blob.writeRef());
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}
	if (diagnostic_blob)
	{
		::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
	}

	slang_result = linked_entry_point->getEntryPointCode(entry_point, target_index, this->code_blob.writeRef(), diagnostic_blob.writeRef());
	if (diagnostic_blob)
	{
		::std::cerr << static_cast<char const *>(diagnostic_blob->getBufferPointer()) << ::std::endl;
	}
	if (SLANG_FAILED(slang_result))
	{
		throw ::std::exception();
	}
}

::std::size_t(::framework::resource::slang_program::get_code_size)() const
{
	return this->code_blob->getBufferSize();
}

void const * ::framework::resource::slang_program::get_code() const
{
	return this->code_blob->getBufferPointer();
}

::framework::resource::program_reflection * ::framework::resource::slang_program::create_program_reflection() const
{
	return nullptr;
}
