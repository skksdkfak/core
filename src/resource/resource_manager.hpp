#pragma once

#include "acp/scene.hpp"
#include "gpu/core.hpp"
#include "coroutine/static_thread_pool.hpp"
#include "coroutine/task.hpp"
#include "resource/texture_loader.hpp"
#include "resource/shader_compiler.hpp"
#include "resource/shader_linker.hpp"
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <filesystem>

namespace framework::acp
{
	class fbx_parser;
};

namespace framework
{
	class gpu_log;
};

namespace framework::resource
{
	struct resource_manager_create_info
	{
		::std::uint32_t frontend_shader_language_preference_count;
		::framework::resource::shader_language const * frontend_shader_language_preferences;
		::framework::resource::shader_language shader_language;
		::framework::resource::shader_environment shader_environment;
	};

	struct hlsl_source_info
	{
		char const * file_name;
		char const * source_entry_point;
	};

	struct glsl_source_info
	{
		char const * file_name;
		char const * source_entry_point;
	};

	struct slang_source_info
	{
		char const * file_name;
	};

	struct include_override
	{
		::std::filesystem::path src;
		::std::filesystem::path dst;
	};

	struct shader_stage_info
	{
		::framework::resource::hlsl_source_info const * hlsl_source_info;
		::framework::resource::glsl_source_info const * glsl_source_info;
		::framework::resource::slang_source_info const * slang_source_info;
		::std::uint32_t preprocessor_define_count;
		::framework::resource::preprocessor_define const * preprocessor_defines;
		::std::uint32_t type_conformance_count;
		::framework::resource::type_conformance const * type_conformances;
		::std::uint32_t include_override_count;
		::framework::resource::include_override const * include_overrides;
		char const * entry_point;
		::framework::gpu::shader_stage_flags stage;
	};
	
	struct shader_module_info
	{
		::std::uint32_t shader_stage_info_count;
		::framework::resource::shader_stage_info const * shader_stage_infos;
	};

	class resource_manager
	{
	public:
		struct default_resources
		{
			::framework::gpu::descriptor_set_layout * gpu_log_host_descriptor_set_layout;
			::framework::gpu::descriptor_set_layout * gpu_log_device_descriptor_set_layout;
			::framework::gpu::descriptor_set * gpu_log_host_descriptor_set;
			::framework::gpu::descriptor_set * gpu_log_device_descriptor_set;
		};

		resource_manager(::framework::resource::resource_manager_create_info const & create_info);

		~resource_manager();

		void add_search_directory(char const * directory);

		bool find_file(::std::filesystem::path & file_path, ::std::filesystem::path file_name) const;

		void load_fbx_scene(::framework::acp::scene * scene, char const * file_name, ::framework::coroutine::static_thread_pool * static_thread_pool, bool use_custom_path = false);

		::framework::gpu::shader_module * load_shader_module(::framework::gpu::device * device, ::framework::resource::shader_module_info const & shader_module_info);

		::framework::coroutine::immediate_task<void> load_texture(::framework::resource::texture_load_info const & texture_load_info);
				
		::std::vector<::std::filesystem::path> const & get_search_directories() const noexcept;

		struct ::framework::resource::resource_manager::default_resources default_resources;

	private:
		::std::vector<::std::filesystem::path> directories;
		::std::unique_ptr<::framework::acp::fbx_parser> fbx_parser;
		::std::vector<::std::unique_ptr<::framework::resource::shader_compiler>> shader_compilers;
		::std::vector<::std::unique_ptr<::framework::resource::shader_linker>> shader_linkers;
		::std::vector<::std::unique_ptr<::framework::resource::texture_loader>> texture_loaders;
		::std::vector<::framework::resource::shader_language> frontend_shader_language_preferences;
		::framework::resource::shader_language target_shader_language;
		::framework::resource::shader_environment target_shader_environment;
		bool cache_shader_module_refs;
	};
}

#include "resource/resource_manager.inl"