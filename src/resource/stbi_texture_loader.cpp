#include "resource/stbi_texture_loader.hpp"
#include "resource/resource_manager.hpp"
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include <stb/stb_image.h>
#include <fstream>
#include <filesystem>

::framework::resource::stbi_texture_loader::stbi_texture_loader(::framework::resource::resource_manager const & resource_manager) :
	resource_manager(resource_manager)
{
}

::framework::coroutine::lazy_task<void>(::framework::resource::stbi_texture_loader::load_texture)(::framework::resource::texture_load_info const & texture_load_info)
{
	::std::filesystem::path file_path;
	if (!this->resource_manager.find_file(file_path, texture_load_info.file_name))
	{
		co_return;
	}

	::std::ifstream ifstream(file_path, ::std::ios::binary);
	if (!ifstream.is_open())
	{
		co_return;
	}

	::std::vector<::stbi_uc> texture_data(::std::istreambuf_iterator<char>(ifstream), {});
	int width, height, channels;
	bool const is_hdr = ::stbi_is_hdr_from_memory(texture_data.data(), static_cast<int>(texture_data.size()));

	void * result;
	if (is_hdr)
	{
		result = ::stbi_loadf_from_memory(texture_data.data(), static_cast<int>(texture_data.size()), &width, &height, &channels, ::STBI_rgb_alpha);
	}
	else
	{
		result = ::stbi_load_from_memory(texture_data.data(), static_cast<int>(texture_data.size()), &width, &height, &channels, ::STBI_rgb_alpha);
	}

	if (result)
	{
		::framework::gpu::format format;
		::framework::resource::texture_load_callback::subresource_data subresource_data;

		switch (channels)
		{
		case 1:
			format = is_hdr ? ::framework::gpu::format::r32_sfloat : ::framework::gpu::format::r8_unorm;
			break;
		case 3:
			format = is_hdr ? ::framework::gpu::format::r32g32b32a32_sfloat : ::framework::gpu::format::r8g8b8a8_unorm;
			channels = 4;
			break;
		case 4:
			format = is_hdr ? ::framework::gpu::format::r32g32b32a32_sfloat : ::framework::gpu::format::r8g8b8a8_unorm;
			break;
		default:
			throw ::std::exception();
			break;
		}

		subresource_data.data = result;
		subresource_data.row_pitch = (is_hdr ? 4 : 1) * channels * width;
		subresource_data.slice_pitch = subresource_data.row_pitch * height;
		subresource_data.mip_level = 0;
		subresource_data.array_layer = 0;

		::framework::resource::texture_load_callback::texture_load_info texture_load_callback_info;
		texture_load_callback_info.width = static_cast<::std::uint32_t>(width);
		texture_load_callback_info.height = static_cast<::std::uint32_t>(height);
		texture_load_callback_info.format = format;
		texture_load_callback_info.mip_levels = 1;
		texture_load_callback_info.subresource_count = 1;
		texture_load_callback_info.subresource_data = &subresource_data;
		auto texture_load_callback = texture_load_info.texture_load_callback->operator()(texture_load_callback_info);
		co_await texture_load_callback;
	}

	::stbi_image_free(result);

	co_return;
}