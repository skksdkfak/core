inline bool(::framework::input::my_input_listner::get_key_state)(::framework::input::key key) const noexcept
{
	return this->key_state[0][key];
}

inline bool(::framework::input::my_input_listner::get_prev_key_state)(::framework::input::key key) const noexcept
{
	return this->key_state[1][key];
}

inline bool(::framework::input::my_input_listner::is_key_first_pressed)(::framework::input::key key) const noexcept
{
	return this->key_state[0][key] && !this->key_state[1][key];
}

inline bool(::framework::input::my_input_listner::is_key_first_released)(::framework::input::key key) const noexcept
{
	return !this->key_state[0][key] && this->key_state[1][key];
}

inline bool ::framework::input::my_input_listner::is_button_first_pressed(::framework::input::button button) const noexcept
{
	return this->button_state[0][button] && !this->button_state[1][button];
}

inline bool ::framework::input::my_input_listner::is_button_first_released(::framework::input::button button) const noexcept
{
	return !this->button_state[0][button] && this->button_state[1][button];
}

inline bool(::framework::input::my_input_listner::get_button_state)(::framework::input::button button) const noexcept
{
	return this->button_state[0][button];
}

inline bool ::framework::input::my_input_listner::get_prev_button_state(::framework::input::button button) const noexcept
{
	return this->button_state[1][button];
}

inline void(::framework::input::my_input_listner::get_mouse_position)(::std::int16_t & x, ::std::int16_t & y) const noexcept
{
	x = this->mouse_x;
	y = this->mouse_y;
}

inline void(::framework::input::my_input_listner::get_mouse_delta)(::std::int16_t & x, ::std::int16_t & y) const noexcept
{
	x = this->mouse_delta_x;
	y = this->mouse_delta_y;
}

inline void(::framework::input::my_input_listner::get_mouse_wheel_delta)(::std::int16_t & mouse_wheel_delta) const noexcept
{
	mouse_wheel_delta = this->mouse_wheel_delta;
}