#include "input/my_input_listner.hpp"
#include <cstring>

::framework::input::my_input_listner::my_input_listner() :
	key_state{},
	button_state{},
	mouse_x(0),
	mouse_y(0),
	prev_mouse_x(0),
	prev_mouse_y(0),
	mouse_delta_x(0),
	mouse_delta_y(0),
	mouse_wheel_delta(0),
	prev_mouse_wheel_delta(0)
{
}

void ::framework::input::my_input_listner::begin_frame_update()
{
	this->mouse_delta_x = this->mouse_x - this->prev_mouse_x;
	this->mouse_delta_y = this->mouse_y - this->prev_mouse_y;
	this->prev_mouse_x = this->mouse_x;
	this->prev_mouse_y = this->mouse_y;
	this->mouse_wheel_delta = this->prev_mouse_wheel_delta;
	this->prev_mouse_wheel_delta = 0;
}

void ::framework::input::my_input_listner::end_frame_update()
{
	::std::memcpy(this->key_state[1], this->key_state[0], sizeof(this->key_state[0]));
	::std::memcpy(this->button_state[1], this->button_state[0], sizeof(this->button_state[0]));
}

void ::framework::input::my_input_listner::button_down(::framework::input::button button)
{
	this->button_state[0][button] = true;
}

void ::framework::input::my_input_listner::button_up(::framework::input::button button)
{
	this->button_state[0][button] = false;
}

void ::framework::input::my_input_listner::key_down(::framework::input::key key)
{
	this->key_state[0][key] = true;
}

void ::framework::input::my_input_listner::key_up(::framework::input::key key)
{
	this->key_state[0][key] = false;
}

void ::framework::input::my_input_listner::mouse_move(::std::int16_t x, ::std::int16_t y)
{
	this->mouse_x = x;
	this->mouse_y = y;
}

void ::framework::input::my_input_listner::mouse_wheel(::std::int16_t delta)
{
	this->prev_mouse_wheel_delta = delta;
}
