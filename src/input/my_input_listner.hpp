#pragma once

#include "input/input_listner.hpp"

namespace framework::input
{
	class my_input_listner : public ::framework::input::input_listner
	{
	public:
		my_input_listner();

		void button_down(::framework::input::button button) override;

		void button_up(::framework::input::button button) override;

		void key_down(::framework::input::key key) override;

		void key_up(::framework::input::key key) override;

		void mouse_move(::std::int16_t x, ::std::int16_t y) override;

		void mouse_wheel(::std::int16_t delta) override;

		void begin_frame_update();

		void end_frame_update();

		bool get_key_state(::framework::input::key key) const noexcept;

		bool get_prev_key_state(::framework::input::key key) const noexcept;

		bool is_key_first_pressed(::framework::input::key key) const noexcept;

		bool is_key_first_released(::framework::input::key key) const noexcept;
		
		bool is_button_first_pressed(::framework::input::button button) const noexcept;

		bool is_button_first_released(::framework::input::button button) const noexcept;

		bool get_button_state(::framework::input::button button) const noexcept;
		
		bool get_prev_button_state(::framework::input::button button) const noexcept;

		void get_mouse_position(::std::int16_t & x, ::std::int16_t & y) const noexcept;

		void get_mouse_delta(::std::int16_t & x, ::std::int16_t & y) const noexcept;

		void get_mouse_wheel_delta(::std::int16_t & mouse_wheel_delta) const noexcept;

	private:
		bool key_state[2][256];
		bool button_state[2][10];
		::std::int16_t mouse_x;
		::std::int16_t mouse_y;
		::std::int16_t prev_mouse_x;
		::std::int16_t prev_mouse_y;
		::std::int16_t mouse_delta_x;
		::std::int16_t mouse_delta_y;
		::std::int16_t mouse_wheel_delta;
		::std::int16_t prev_mouse_wheel_delta;
	};
}

#include "input/my_input_listner.inl"