#pragma once

#include <cstdint>

namespace framework::input
{
    enum key
    {
        key_none = 0,
        key_a = 4,
        key_b = 5,
        key_c = 6,
        key_d = 7,
        key_e = 8,
        key_f = 9,
        key_g = 10,
        key_h = 11,
        key_i = 12,
        key_j = 13,
        key_k = 14,
        key_l = 15,
        key_m = 16,
        key_n = 17,
        key_o = 18,
        key_p = 19,
        key_q = 20,
        key_r = 21,
        key_s = 22,
        key_t = 23,
        key_u = 24,
        key_v = 25,
        key_w = 26,
        key_x = 27,
        key_y = 28,
        key_z = 29,
        key_1 = 30, // 1 and !
        key_2 = 31, // 2 and @
        key_3 = 32, // 3 and #
        key_4 = 33, // 4 and $
        key_5 = 34, // 5 and %
        key_6 = 35, // 6 and ^
        key_7 = 36, // 7 and &
        key_8 = 37, // 8 and *
        key_9 = 38, // 9 and (
        key_0 = 39, // 0 and )
        key_enter = 40, // (return)
        key_escape = 41,
        key_delete = 42,
        key_tab = 43,
        key_space = 44,
        key_minus = 45, // - and (underscore)
        key_equals = 46, // = and +
        key_left_bracket = 47, // [ and {
        key_right_bracket = 48, // ] and }
        key_backslash = 49, // \ and |
        // key_nonushash     = 50, // # and ~
        key_semicolon = 51, // ; and :
        key_quote = 52, // ' and "
        key_grave = 53,
        key_comma = 54, // , and <
        key_period = 55, // . and >
        key_slash = 56, // / and ?
        key_caps_lock = 57,
        key_f1 = 58,
        key_f2 = 59,
        key_f3 = 60,
        key_f4 = 61,
        key_f5 = 62,
        key_f6 = 63,
        key_f7 = 64,
        key_f8 = 65,
        key_f9 = 66,
        key_f10 = 67,
        key_f11 = 68,
        key_f12 = 69,
        key_print_screen = 70,
        key_scroll_lock = 71,
        key_pause = 72,
        key_insert = 73,
        key_home = 74,
        key_pageup = 75,
        key_delete_forward = 76,
        key_end = 77,
        key_pagedown = 78,
        key_right = 79, // right arrow
        key_left = 80, // left arrow
        key_down = 81, // down arrow
        key_up = 82, // up arrow
        kp_numlock = 83,
        kp_divide = 84,
        kp_multiply = 85,
        kp_subtract = 86,
        kp_add = 87,
        kp_enter = 88,
        kp_1 = 89,
        kp_2 = 90,
        kp_3 = 91,
        kp_4 = 92,
        kp_5 = 93,
        kp_6 = 94,
        kp_7 = 95,
        kp_8 = 96,
        kp_9 = 97,
        kp_0 = 98,
        kp_point = 99, // . and del
        kp_equals = 103,
        key_f13 = 104,
        key_f14 = 105,
        key_f15 = 106,
        key_f16 = 107,
        key_f17 = 108,
        key_f18 = 109,
        key_f19 = 110,
        key_f20 = 111,
        key_f21 = 112,
        key_f22 = 113,
        key_f23 = 114,
        key_f24 = 115,
        // key_help          = 117,
        key_menu = 118,
        key_mute = 127,
        key_volume_up = 128,
        key_volume_down = 129,
        key_left_control = 224, // warning : android has no ctrl keys.
        key_left_shift = 225,
        key_left_alt = 226,
        key_left_gui = 227,
        key_right_control = 228,
        key_right_shift = 229, // warning : win32 fails to send a wm_keyup message if both shift keys are pressed, and one released.
        key_right_alt = 230,
        key_right_gui = 231
    };

    enum button
    {
        button_none = 0,
        button_1 = 1,
        button_2 = 2,
        button_3 = 3,
        button_4 = 4,
        button_5 = 5,
        button_6 = 6,
        button_7 = 7,
        button_8 = 8,
        button_9 = 9,
    };

    class input_listner
    {
    public:
		virtual void button_down(::framework::input::button button) = 0;

		virtual void button_up(::framework::input::button button) = 0;

		virtual void key_down(::framework::input::key key) = 0;

		virtual void key_up(::framework::input::key key) = 0;

		virtual void mouse_move(::std::int16_t x, ::std::int16_t y) = 0;

		virtual void mouse_wheel(::std::int16_t delta) = 0;
    };
}