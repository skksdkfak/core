#pragma once

namespace framework
{
	namespace linalg
	{
		enum class matrix_layout
		{
			row_major_,
			column_major_
		};
	}
}
