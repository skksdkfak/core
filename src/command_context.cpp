#include "command_context.hpp"
#include "coroutine/fmap.hpp"
#include "coroutine/schedule_on.hpp"
#include "coroutine/static_thread_pool.hpp"
#include "gpu/utility.hpp"
#include <memory>
#include <vector>

::framework::fence::fence(::framework::fence_pool * fence_pool) :
	non_pool(false)
{
	::framework::gpu::fence_create_info fence_create_info;
	fence_create_info.flags = ::framework::gpu::fence_create_flags::none;
	fence_pool->get_gpu_context()->get_device()->create_fence(&fence_create_info, nullptr, &this->device_fence);

	::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
	debug_utils_object_name_info.object_type = ::framework::gpu::object_type::fence;
	debug_utils_object_name_info.object = this->device_fence;
	debug_utils_object_name_info.object_name = "::framework::fence::fence::device_fence";
	assert_framework_gpu_result(fence_pool->get_gpu_context()->get_device()->set_object_name(&debug_utils_object_name_info));
}

::framework::fence::fence(::framework::gpu::fence * fence) :
	non_pool(true),
	device_fence(fence)
{
}

::framework::coroutine::immediate_task<::framework::fence *>(::framework::fence_pool::request_fence)()
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->mutex.scoped_lock();
	::framework::fence * fence;
	if (this->available_fences.empty())
	{
		fence = new ::framework::fence(this);
		this->fence_pool_vector.emplace_back(fence);
	}
	else
	{
		fence = this->available_fences.front();
		this->available_fences.pop();
	}
	co_return fence;
}

::framework::coroutine::immediate_task<void>(::framework::fence_pool::discard_fence)(::framework::fence * fence)
{
	::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->mutex.scoped_lock();
	this->available_fences.push(fence);
	fence->reset();
}

::framework::coroutine::self_destructible_coroutine(::framework::fence_pool::retire_fence)(::framework::fence * fence)
{
	bool signal_event;
	{
		::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->retired_fences_mutex.scoped_lock();
		signal_event = this->retired_fences.empty();
		this->retired_fences.push_back(fence);
	}
	if (signal_event)
	{
		this->single_consumer_event.set();
	}
}

::framework::coroutine::immediate_task<void>(::framework::fence_pool::wait_for_fences)()
{
	try
	{
		while (true)
		{
			co_await this->single_consumer_event;
			this->single_consumer_event.reset();
			co_await this->retired_fences_mutex.lock();

			::std::vector<::framework::gpu::fence *> retired_device_fences;
			while (!this->retired_fences.empty())
			{
				retired_device_fences.resize(this->retired_fences.size());
				for (::std::size_t i = 0; i < this->retired_fences.size(); i++)
				{
					retired_device_fences[i] = this->retired_fences[i]->get_fence();
				}
				if (this->gpu_context->get_device()->wait_for_fences(static_cast<::std::uint32_t>(retired_device_fences.size()), retired_device_fences.data(), false, 0) == ::framework::gpu::result::success)
				{
					::std::size_t retired_fence_count = this->retired_fences.size();
					for (::std::size_t i = 0; i < retired_fence_count - 1;)
					{
						if (this->gpu_context->get_device()->wait_for_fences(1, &retired_device_fences[i], true, 0) == ::framework::gpu::result::success)
						{
							--retired_fence_count;
							::std::swap(this->retired_fences[i], this->retired_fences[retired_fence_count]);
							::std::swap(retired_device_fences[i], retired_device_fences[retired_fence_count]);
						}
						else
						{
							++i;
						}
					}

					if (this->gpu_context->get_device()->wait_for_fences(1, &retired_device_fences[retired_fence_count - 1], true, 0) == ::framework::gpu::result::success)
					{
						--retired_fence_count;
					}

					if (retired_device_fences.size() > retired_fence_count)
					{
						[[maybe_unused]] ::framework::gpu::result const result = this->gpu_context->get_device()->reset_fences(static_cast<::std::uint32_t>(retired_device_fences.size() - retired_fence_count), &retired_device_fences[retired_fence_count]);
						assert(result == ::framework::gpu::result::success);
						for (::std::size_t j = retired_fence_count; j < this->retired_fences.size(); ++j)
						{
							this->retired_fences[j]->signal(*this->static_thread_pool);
						}
					}

					this->retired_fences.resize(retired_fence_count);
					retired_device_fences.resize(retired_fence_count);
				}
				else
				{
					this->retired_fences_mutex.unlock();
					co_await this->static_thread_pool->schedule();
					co_await this->retired_fences_mutex.lock();
				}
			}

			this->retired_fences_mutex.unlock();
		}
	} catch (...)
	{
		this->retired_fences_mutex.unlock();
		throw;
	}
}

::framework::command_context::command_context(::framework::command_queue & command_queue) :
	command_queue(command_queue),
	linear_allocator(*command_queue.get_command_buffer_manager().get_linear_allocator_page_manager())
{
	::framework::gpu::command_pool_create_info command_pool_create_info;
	command_pool_create_info.flags = ::framework::gpu::command_pool_create_flags::none;
	command_pool_create_info.queue_family_index = command_queue.get_queue_family_index();
	assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->create_command_pool(&command_pool_create_info, nullptr, &this->command_pool));

	{
		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::command_pool;
		debug_utils_object_name_info.object = this->command_pool;
		debug_utils_object_name_info.object_name = "::framework::command_context::command_pool";
		assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->set_object_name(&debug_utils_object_name_info));
	}

	::framework::gpu::command_buffer_allocate_info command_buffer_allocate_info;
	command_buffer_allocate_info.command_pool = this->command_pool;
	command_buffer_allocate_info.level = ::framework::gpu::command_buffer_level::primary;
	command_buffer_allocate_info.subbuffer_count = 1;
	command_buffer_allocate_info.command_buffer_count = 1;
	assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->allocate_command_buffers(&command_buffer_allocate_info, &this->command_buffer));

	{
		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::command_buffer;
		debug_utils_object_name_info.object = this->command_buffer;
		debug_utils_object_name_info.object_name = "::framework::command_context::command_buffer";
		assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->set_object_name(&debug_utils_object_name_info));
	}

	::framework::gpu::semaphore_create_info semaphore_create_info;
	semaphore_create_info.flags = ::framework::gpu::semaphore_create_flags::none;
	semaphore_create_info.semaphore_type = ::framework::gpu::semaphore_type::binary;
	semaphore_create_info.initial_value = 0;
	assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->create_semaphore(&semaphore_create_info, nullptr, &this->semaphore));

	{
		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::semaphore;
		debug_utils_object_name_info.object = this->semaphore;
		debug_utils_object_name_info.object_name = "::framework::command_context::semaphore";
		assert_framework_gpu_result(this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->set_object_name(&debug_utils_object_name_info));
	}
}

::framework::command_context::~command_context()
{
	this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->destroy_semaphore(this->semaphore, nullptr);
	this->command_queue.get_command_buffer_manager().get_gpu_context()->get_device()->destroy_command_pool(this->command_pool, nullptr);
}

::framework::coroutine::immediate_task<void>(::framework::command_context::update_buffer)(::framework::gpu::buffer * dst_buffer, void const * data, ::framework::gpu::device_size offset, ::framework::gpu::device_size size)
{
	struct ::framework::linear_allocator::allocation const allocation = co_await this->linear_allocator.allocate(size, 1);

	::std::memcpy(static_cast<::std::byte *>(allocation.mapped_buffer_data) + allocation.offset, data, size);

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = allocation.offset;
	buffer_copy.dst_offset = offset;
	buffer_copy.size = size;
	this->command_buffer->copy_buffer(allocation.buffer, dst_buffer, 1, &buffer_copy);
}

::framework::coroutine::immediate_task<struct ::framework::linear_allocator::allocation> framework::command_context::read_buffer(::framework::linear_allocator & cpu_read_linear_allocator, ::framework::gpu::buffer * src_buffer, ::framework::gpu::device_size offset, ::framework::gpu::device_size size)
{
	struct ::framework::linear_allocator::allocation const allocation = co_await cpu_read_linear_allocator.allocate(size, 1);

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = offset;
	buffer_copy.dst_offset = allocation.offset;
	buffer_copy.size = size;
	this->command_buffer->copy_buffer(src_buffer, allocation.buffer, 1, &buffer_copy);

	co_return allocation;
}

::framework::command_queue::command_queue(::framework::command_buffer_manager & command_buffer_manager, ::std::uint32_t queue_family_index, ::std::uint32_t queue_index) :
	command_buffer_manager(command_buffer_manager),
	queue_family_index(queue_family_index),
	queue_index(queue_index)
{
	this->command_buffer_manager.get_gpu_context()->get_device()->get_queue(this->queue_family_index, this->queue_index, &this->queue);
}

::framework::command_queue::~command_queue()
{
}

::framework::coroutine::immediate_task<class::framework::command_context *> framework::command_queue::acquire_command_context()
{
	class ::framework::command_context * command_context;
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->command_pool_mutex.scoped_lock();

	if (!this->ready_command_contexts.empty())
	{
		command_context = this->ready_command_contexts.front();
		this->ready_command_contexts.pop();
	}
	else
	{
		command_context = new class ::framework::command_context(*this);
		this->command_context_pool.push_back(command_context);
	}

	co_return command_context;
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::release_command_context)(class ::framework::command_context * command_context)
{
	co_await this->reset_command_context(command_context);
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->command_pool_mutex.scoped_lock();
	this->ready_command_contexts.push(command_context);
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::reset_command_context)(::framework::command_context * command_context)
{
	assert_framework_gpu_result(this->get_command_buffer_manager().get_gpu_context()->get_device()->reset_command_pool(command_context->get_command_pool(), ::framework::gpu::command_pool_reset_flags::none));
	co_await command_context->get_linear_allocator().cleanup_used_pages();
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::submit)(class ::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos, ::framework::gpu::fence * fence)
{
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->queue_mutex.scoped_lock();

	::framework::gpu::command_buffer_submit_info command_buffer_info;
	command_buffer_info.command_buffer = command_context->get_command_buffer();
	command_buffer_info.device_mask = 0;

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = wait_semaphore_info_count;
	submit_info.wait_semaphore_infos = wait_semaphore_infos;
	submit_info.command_buffer_info_count = 1;
	submit_info.command_buffer_infos = &command_buffer_info;
	submit_info.signal_semaphore_info_count = signal_semaphore_info_count;
	submit_info.signal_semaphore_infos = signal_semaphore_infos;
	assert_framework_gpu_result(this->queue->submit(1, &submit_info, fence));
}

::framework::coroutine::immediate_task<void> framework::command_queue::submit(::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos)
{
	co_await this->submit(command_context, wait_semaphore_info_count, wait_semaphore_infos, signal_semaphore_info_count, signal_semaphore_infos, nullptr);
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::submit_and_wait)(::framework::command_context * command_context, ::std::uint32_t wait_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * wait_semaphore_infos, ::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos)
{
	::framework::fence * fence = co_await this->command_buffer_manager.get_fence_pool().request_fence();
	co_await this->submit(command_context, wait_semaphore_info_count, wait_semaphore_infos, signal_semaphore_info_count, signal_semaphore_infos, fence->get_fence());
	this->command_buffer_manager.get_fence_pool().retire_fence(fence);
	co_await *fence;
	co_await this->command_buffer_manager.get_fence_pool().discard_fence(fence);
}

::framework::coroutine::immediate_task<::framework::gpu::result>(::framework::command_queue::submit)(::std::uint32_t submit_count, ::framework::gpu::submit_info const * submits, class ::framework::gpu::fence * fence)
{
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->queue_mutex.scoped_lock();
	co_return this->queue->submit(submit_count, submits, fence);
}

::framework::coroutine::immediate_task<::framework::gpu::result>(::framework::command_queue::present)(::framework::gpu::present_info const * present_info)
{
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->queue_mutex.scoped_lock();
	co_return this->queue->present(present_info);
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::signal_semaphores)(::std::uint32_t signal_semaphore_info_count, ::framework::gpu::semaphore_submit_info const * signal_semaphore_infos)
{
	class ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const scoped_lock = co_await this->queue_mutex.scoped_lock();

	::framework::fence * fence = co_await this->get_command_buffer_manager().get_fence_pool().request_fence();

	::framework::gpu::submit_info submit_info;
	submit_info.flags = ::framework::gpu::submit_flags::none;
	submit_info.wait_semaphore_info_count = 0;
	submit_info.wait_semaphore_infos = nullptr;
	submit_info.command_buffer_info_count = 0;
	submit_info.command_buffer_infos = nullptr;
	submit_info.signal_semaphore_info_count = signal_semaphore_info_count;
	submit_info.signal_semaphore_infos = signal_semaphore_infos;
	assert_framework_gpu_result(this->queue->submit(1, &submit_info, fence->get_fence()));
	this->command_buffer_manager.get_fence_pool().retire_fence(fence);
	co_await *fence;
	co_await this->command_buffer_manager.get_fence_pool().discard_fence(fence);
}

::framework::coroutine::immediate_task<void>(::framework::command_queue::signal_semaphore)(::framework::gpu::semaphore * semaphore)
{
	::framework::gpu::semaphore_submit_info signal_semaphore_info;
	signal_semaphore_info.semaphore = semaphore;
	signal_semaphore_info.value = 0;
	signal_semaphore_info.stage_mask = ::framework::gpu::pipeline_stage_flags::none;
	signal_semaphore_info.device_index = 0;
	co_await this->signal_semaphores(1, &signal_semaphore_info);
}

::framework::command_buffer_manager::command_buffer_manager(::framework::gpu_context * gpu_context, ::framework::linear_allocator_page_manager * linear_allocator_page_manager, ::framework::coroutine::static_thread_pool * static_thread_pool) :
	gpu_context(gpu_context),
	linear_allocator_page_manager(linear_allocator_page_manager)
{
	this->fence_pool = ::std::make_unique<::framework::fence_pool>(gpu_context, static_thread_pool);
	this->graphics_queue = ::std::make_unique<::framework::command_queue>(*this, this->gpu_context->get_queue_family_indices().graphics, 0);
	this->compute_queue = ::std::make_unique<::framework::command_queue>(*this, this->gpu_context->get_queue_family_indices().compute, 0);
	this->transfer_queue = ::std::make_unique<::framework::command_queue>(*this, this->gpu_context->get_queue_family_indices().transfer, 0);
}

::framework::command_buffer_manager::~command_buffer_manager()
{
}

::framework::coroutine::immediate_task<void>(::framework::command_buffer_manager::wait_idle)()
{
	for (::framework::command_queue * queue : { this->graphics_queue.get(), this->compute_queue.get(), this->transfer_queue.get() })
	{
		co_await queue->get_queue_mutex().lock();
	}

	this->gpu_context->get_device()->wait_idle();

	for (::framework::command_queue * queue : { this->graphics_queue.get(), this->compute_queue.get(), this->transfer_queue.get() })
	{
		queue->get_queue_mutex().unlock();
	}
}
