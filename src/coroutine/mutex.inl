#include <cassert>

inline ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock::scoped_lock(::framework::coroutine::mutex & mutex, ::std::adopt_lock_t) noexcept :
	mutex(&mutex)
{
}

inline ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock::scoped_lock(::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock && other) noexcept :
	mutex(other.mutex)
{
	other.mutex = nullptr;
}

inline ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock::~scoped_lock()
{
	if (this->mutex)
	{
		this->mutex->unlock();
	}
}

inline ::framework::coroutine::mutex::mutex() noexcept :
	state(::framework::coroutine::mutex::not_locked),
	waiters(nullptr)
{
}

inline ::framework::coroutine::mutex::~mutex() noexcept
{
	[[maybe_unused]] ::std::uintptr_t state = this->state.load(::std::memory_order::relaxed);
	assert(state == ::framework::coroutine::mutex::not_locked || state == ::framework::coroutine::mutex::locked_no_waiters);
	assert(this->waiters == nullptr);
}

inline ::framework::coroutine::mutex::awaiter_lock(::framework::coroutine::mutex::lock)() noexcept
{
	return ::framework::coroutine::mutex::awaiter_lock(*this);
}

inline bool ::framework::coroutine::mutex::try_lock() noexcept
{
	::std::uintptr_t old_state = ::framework::coroutine::mutex::not_locked;
	return this->state.compare_exchange_strong(old_state, ::framework::coroutine::mutex::locked_no_waiters, ::std::memory_order::acquire, ::std::memory_order::relaxed);
}

inline ::framework::coroutine::mutex::awaiter_scoped_lock(::framework::coroutine::mutex::scoped_lock)() noexcept
{
	return ::framework::coroutine::mutex::awaiter_scoped_lock(*this);
}

inline void ::framework::coroutine::mutex::unlock() noexcept
{
	assert(this->state.load(::std::memory_order::relaxed) != ::framework::coroutine::mutex::not_locked);

	::framework::coroutine::mutex::awaiter_lock * waiters_head = this->waiters;
	if (waiters_head == nullptr)
	{
		::std::uintptr_t old_state = ::framework::coroutine::mutex::locked_no_waiters;
		if (this->state.compare_exchange_strong(old_state, ::framework::coroutine::mutex::not_locked, ::std::memory_order::release, ::std::memory_order::relaxed))
		{
			return;
		}

		// At least one new waiter.
		// Acquire the list of new waiter operations atomically.
		old_state = this->state.exchange(::framework::coroutine::mutex::locked_no_waiters, ::std::memory_order::acquire);

		assert(old_state != ::framework::coroutine::mutex::locked_no_waiters && old_state != ::framework::coroutine::mutex::not_locked);

		// Transfer the list to waiters, reversing the list in the process so
		// that the head of the list is the first to be resumed.
		::framework::coroutine::mutex::awaiter_lock * next = reinterpret_cast<::framework::coroutine::mutex::awaiter_lock *>(old_state);
		do
		{
			::framework::coroutine::mutex::awaiter_lock * temp = next->next;
			next->next = waiters_head;
			waiters_head = next;
			next = temp;
		} while (next != nullptr);
	}

	assert(waiters_head != nullptr);

	this->waiters = waiters_head->next;

	// Resume the waiter.
	// This will pass the ownership of the lock on to that operation/coroutine.
	waiters_head->awaiter.resume();
}

inline ::framework::coroutine::mutex::awaiter_scoped_lock::awaiter_scoped_lock(::framework::coroutine::mutex & mutex) noexcept :
	::framework::coroutine::mutex::awaiter_lock(mutex)
{
}

inline decltype(auto) ::framework::coroutine::mutex::awaiter_scoped_lock::await_resume() const noexcept
{
	return ::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock(this->mutex, ::std::adopt_lock);
}

inline ::framework::coroutine::mutex::awaiter_lock::awaiter_lock(::framework::coroutine::mutex & mutex) noexcept :
	mutex(mutex),
	next(nullptr)
{
}

inline bool ::framework::coroutine::mutex::awaiter_lock::await_ready() noexcept
{
	return false;
}

inline bool ::framework::coroutine::mutex::awaiter_lock::await_suspend(::std::coroutine_handle<> awaiter) noexcept
{
	this->awaiter = awaiter;

	::std::uintptr_t old_state = this->mutex.state.load(::std::memory_order::acquire);
	while (true)
	{
		if (old_state == ::framework::coroutine::mutex::not_locked)
		{
			if (this->mutex.state.compare_exchange_weak(old_state, ::framework::coroutine::mutex::locked_no_waiters, ::std::memory_order::acquire, ::std::memory_order::relaxed))
			{
				// Acquired lock, don't suspend.
				return false;
			}
		}
		else
		{
			this->next = reinterpret_cast<::framework::coroutine::mutex::awaiter_lock *>(old_state);
			if (this->mutex.state.compare_exchange_weak(old_state, reinterpret_cast<::std::uintptr_t>(this), ::std::memory_order::release, ::std::memory_order::relaxed))
			{
				// Queued operation to waiters list, suspend now.
				return true;
			}
		}
	}
}

inline void ::framework::coroutine::mutex::awaiter_lock::await_resume() noexcept
{
}
