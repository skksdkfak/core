template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::thread_pool_task() noexcept :
	coroutine_handle(nullptr)
{
}

template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::thread_pool_task(::std::coroutine_handle<typename ::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle) noexcept :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::thread_pool_task(::framework::coroutine::thread_pool_task<T> && other) noexcept :
	coroutine_handle(other.coroutine_handle)
{
	other.coroutine_handle = nullptr;
}

template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::~thread_pool_task()
{
	if (this->coroutine_handle)
	{
		this->coroutine_handle.destroy();
	}
}

template<typename T>
::framework::coroutine::thread_pool_task<T> & ::framework::coroutine::thread_pool_task<T>::operator=(::framework::coroutine::thread_pool_task<T> && other) noexcept
{
	if (::std::addressof(other) != this)
	{
		if (this->coroutine_handle)
		{
			this->coroutine_handle.destroy();
		}

		this->coroutine_handle = other.coroutine_handle;
		other.coroutine_handle = nullptr;
	}

	return *this;
}

template<typename T>
inline constexpr ::framework::coroutine::thread_pool_task<T>::operator bool() const noexcept
{
	return this->coroutine_handle != nullptr;
}

template<typename T>
inline auto ::framework::coroutine::thread_pool_task<T>::operator co_await() const & noexcept
{
	struct awaitable : public ::framework::coroutine::thread_pool_task<T>::awaitable_base
	{
		using ::framework::coroutine::thread_pool_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return this->coroutine_handle.promise().result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline auto ::framework::coroutine::thread_pool_task<T>::operator co_await() const && noexcept
{
	struct awaitable : public ::framework::coroutine::thread_pool_task<T>::awaitable_base
	{
		using ::framework::coroutine::thread_pool_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return ::std::move(this->coroutine_handle.promise()).result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline bool ::framework::coroutine::thread_pool_task<T>::done() const noexcept
{
	return !this->coroutine_handle || this->coroutine_handle.done();
}

template<typename T>
inline decltype(auto) ::framework::coroutine::thread_pool_task<T>::result()
{
	return this->coroutine_handle.promise().result();
}

template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::promise_type::promise_type(::framework::coroutine::static_thread_pool & thread_pool) noexcept :
	thread_pool(thread_pool)
{
}

template<typename T>
template<typename ... TYPES>
inline ::framework::coroutine::thread_pool_task<T>::promise_type::promise_type(::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept :
	thread_pool(thread_pool)
{
}

template<typename T>
template<typename TYPE>
inline ::framework::coroutine::thread_pool_task<T>::promise_type::promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool) noexcept :
	thread_pool(thread_pool)
{
}

template<typename T>
template<typename TYPE, typename ... TYPES>
inline ::framework::coroutine::thread_pool_task<T>::promise_type::promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept :
	thread_pool(thread_pool)
{
}

template<typename T>
inline auto ::framework::coroutine::thread_pool_task<T>::promise_type::get_return_object() noexcept
{
	return ::framework::coroutine::thread_pool_task<T>(::std::coroutine_handle<typename ::framework::coroutine::thread_pool_task<T>::promise_type>::from_promise(static_cast<typename ::framework::coroutine::thread_pool_task<T>::promise_type &>(*this)));
}

template<typename T>
inline auto ::framework::coroutine::thread_pool_task<T>::promise_type::initial_suspend() const
{
	return this->thread_pool.schedule();
}

template<typename T>
inline auto ::framework::coroutine::thread_pool_task<T>::promise_type::final_suspend() const noexcept
{
	struct awaitable
	{
		bool await_ready() noexcept
		{
			return false;
		}

		::std::coroutine_handle<> await_suspend(::std::coroutine_handle<::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle) noexcept
		{
			if (coroutine_handle.promise().has_continuation.exchange(true, ::std::memory_order::acquire))
			{
				return coroutine_handle.promise().continuation;
			}
			else
			{
				return ::std::noop_coroutine();
			}
		}

		void await_resume() noexcept
		{
		}
	};

	return awaitable();
}

template<typename T>
inline bool ::framework::coroutine::thread_pool_task<T>::promise_type::set_continuation(::std::coroutine_handle<> continuation) noexcept
{
	this->continuation = continuation;
	return this->has_continuation.exchange(true, ::std::memory_order::release) == false;
}

template<typename T>
inline ::framework::coroutine::thread_pool_task<T>::awaitable_base::awaitable_base(::std::coroutine_handle<::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle) :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline bool ::framework::coroutine::thread_pool_task<T>::awaitable_base::await_ready() noexcept
{
	return this->coroutine_handle.done();
}

template<typename T>
inline bool framework::coroutine::thread_pool_task<T>::awaitable_base::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	return this->coroutine_handle.promise().set_continuation(coroutine_handle);
}

inline auto ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::get_return_object() const noexcept
{
	return ::framework::coroutine::self_destructible_thread_pool_coroutine();
}

inline void ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::unhandled_exception() const noexcept
{
	assert(false);
}

inline void ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::return_void() const noexcept
{
}

inline auto ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::initial_suspend() const noexcept
{
	return this->thread_pool.schedule();
}

inline auto ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::final_suspend() const noexcept
{
	return ::std::suspend_never();
}

inline ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::promise_type(::framework::coroutine::static_thread_pool & thread_pool) noexcept :
	thread_pool(thread_pool)
{
}

template<typename ... TYPES>
inline ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::promise_type(::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept :
	thread_pool(thread_pool)
{
}

template<typename TYPE>
inline ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool) noexcept :
	thread_pool(thread_pool)
{
}

template<typename TYPE, typename ... TYPES>
inline ::framework::coroutine::self_destructible_thread_pool_coroutine::promise_type::promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept :
	thread_pool(thread_pool)
{
}