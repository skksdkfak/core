#if framework_multiqueue_thread_pool
inline ::std::uint32_t(::framework::coroutine::thread::get_id)() noexcept
{
	return ::framework::coroutine::thread::id;
}

inline constexpr ::framework::coroutine::thread::offset_t(::framework::coroutine::thread::difference)(::std::size_t a, ::std::size_t b)
{
	return static_cast<::framework::coroutine::thread::offset_t>(a - b);
}

inline ::framework::coroutine::static_thread_pool::schedule_operation(::framework::coroutine::static_thread_pool::schedule)() noexcept
{
	return ::framework::coroutine::static_thread_pool::schedule_operation(this);
}

inline ::framework::coroutine::static_thread_pool::schedule_operation::schedule_operation(::framework::coroutine::static_thread_pool * static_thread_pool) noexcept :
	static_thread_pool(static_thread_pool)
{
}

inline bool ::framework::coroutine::static_thread_pool::schedule_operation::await_ready() noexcept
{
	return false;
}

inline void ::framework::coroutine::static_thread_pool::schedule_operation::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	this->coroutine_handle = coroutine_handle;
	this->static_thread_pool->schedule(this);
}

inline void ::framework::coroutine::static_thread_pool::schedule_operation::await_resume() noexcept
{
}
#else
inline ::framework::coroutine::static_thread_pool::schedule_operation(::framework::coroutine::static_thread_pool::schedule)() noexcept
{
	return ::framework::coroutine::static_thread_pool::schedule_operation(this);
}

inline ::framework::coroutine::static_thread_pool::schedule_operation::schedule_operation(::framework::coroutine::static_thread_pool const * static_thread_pool) noexcept :
	static_thread_pool(static_thread_pool)
{
}

inline bool ::framework::coroutine::static_thread_pool::schedule_operation::await_ready() noexcept
{
	return false;
}

inline void ::framework::coroutine::static_thread_pool::schedule_operation::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	this->static_thread_pool->schedule(coroutine_handle);
}

inline void ::framework::coroutine::static_thread_pool::schedule_operation::await_resume() noexcept
{
}
#endif