#pragma once

#include <atomic>
#include <coroutine>
#include <cstdint>
#include <ranges>

namespace framework::coroutine
{
	class when_all_counter
	{
	public:
		when_all_counter(::std::size_t count) noexcept
			: count(count + 1)
			, awaiting_coroutine_handle(nullptr)
		{
		}

		bool is_ready() const noexcept
		{
			return static_cast<bool>(this->awaiting_coroutine_handle);
		}

		bool try_await(std::coroutine_handle<> coroutine_handle) noexcept
		{
			this->awaiting_coroutine_handle = coroutine_handle;
			return this->count.fetch_sub(1, ::std::memory_order::acq_rel) > 1;
		}

		void notify_awaitable_completed() noexcept
		{
			if (this->count.fetch_sub(1, ::std::memory_order::acq_rel) == 1)
			{
				this->awaiting_coroutine_handle.resume();
			}
		}

	private:
		::std::atomic_size_t count;
		::std::coroutine_handle<> awaiting_coroutine_handle;
	};

	template<typename RESULT>
	class when_all_task
	{
	public:
		class promise_type
		{
		public:
			auto get_return_object() noexcept
			{
				return ::std::coroutine_handle<promise_type>::from_promise(*this);
			}

			::std::suspend_always initial_suspend() noexcept
			{
				return {};
			}

			auto final_suspend() noexcept
			{
				struct completion_notifier
				{
					bool await_ready() const noexcept
					{
						return false;
					}

					void await_suspend(::std::coroutine_handle<promise_type> coroutine_handle) const noexcept
					{
						coroutine_handle.promise().counter->notify_awaitable_completed();
					}

					void await_resume() const noexcept
					{
					}
				};

				return completion_notifier{};
			}

			void unhandled_exception() noexcept
			{
				this->exception = ::std::current_exception();
			}

			void return_void() const noexcept
			{
			}

			void start(::framework::coroutine::when_all_counter & counter) noexcept
			{
				this->counter = &counter;
				::std::coroutine_handle<promise_type>::from_promise(*this).resume();
			}

			::framework::coroutine::when_all_counter * counter;
			::std::exception_ptr exception;
		};

		when_all_task(::std::coroutine_handle<promise_type> coroutine_handle) noexcept :
			coroutine_handle(coroutine_handle)
		{
		}

		when_all_task(when_all_task && other) noexcept :
			coroutine_handle(::std::exchange(other.coroutine_handle, ::std::coroutine_handle<promise_type>{}))
		{
		}

		~when_all_task() noexcept
		{
			if (this->coroutine_handle)
			{
				this->coroutine_handle.destroy();
			}
		}

		void start(::framework::coroutine::when_all_counter & counter) noexcept
		{
			this->coroutine_handle.promise().start(counter);
		}

	private:
		::std::coroutine_handle<promise_type> coroutine_handle;
	};

	template<typename TASK_CONTAINER>
	class when_all_ready_awaitable
	{
	public:
		when_all_ready_awaitable(TASK_CONTAINER && tasks) noexcept :
			counter(tasks.size()),
			tasks(::std::forward<TASK_CONTAINER>(tasks))
		{
		}

		auto operator co_await() noexcept
		{
			struct awaiter
			{
				bool await_ready() const noexcept
				{
					return this->awaitable.is_ready();
				}

				bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
				{
					return this->awaitable.try_await(coroutine_handle);
				}

				TASK_CONTAINER & await_resume() noexcept
				{
					return this->awaitable.tasks;
				}

				::framework::coroutine::when_all_ready_awaitable<TASK_CONTAINER> & awaitable;
			};

			return awaiter{ *this };
		}

	private:
		bool is_ready() const noexcept
		{
			return this->counter.is_ready();
		}

		bool try_await(::std::coroutine_handle<> coroutine_handle) noexcept
		{
			for (auto && task : this->tasks)
			{
				task.start(this->counter);
			}

			return this->counter.try_await(coroutine_handle);
		}

		::framework::coroutine::when_all_counter counter;
		TASK_CONTAINER tasks;
	};

	template<typename AWAITABLE>
	::framework::coroutine::when_all_task<void> make_when_all_task(AWAITABLE awaitable)
	{
		co_await static_cast<AWAITABLE &&>(awaitable);
	}

	auto when_all_ready(::std::ranges::input_range auto && awaitables)
	{
		::std::vector<::framework::coroutine::when_all_task<void>> tasks;
		for (auto & awaitable : awaitables)
		{
			tasks.emplace_back(::framework::coroutine::make_when_all_task(::std::move(awaitable)));
		}

		return ::framework::coroutine::when_all_ready_awaitable<::std::vector<::framework::coroutine::when_all_task<void>>>(::std::move(tasks));
	}
}