#pragma once

#include <atomic>
#include <coroutine>
#include <cstdint>
#include <mutex>

namespace framework::coroutine
{
	class mutex
	{
	public:
		class awaiter_lock
		{
		public:
			awaiter_lock(::framework::coroutine::mutex & mutex) noexcept;

			bool await_ready() noexcept;

			bool await_suspend(::std::coroutine_handle<> awaiter) noexcept;

			void await_resume() noexcept;

		protected:
			::framework::coroutine::mutex & mutex;

		private:
			friend class ::framework::coroutine::mutex;

			::framework::coroutine::mutex::awaiter_lock * next;
			::std::coroutine_handle<> awaiter;
		};

		class awaiter_scoped_lock : public ::framework::coroutine::mutex::awaiter_lock
		{
		public:
			class scoped_lock
			{
			public:
				explicit scoped_lock(::framework::coroutine::mutex & mutex, ::std::adopt_lock_t) noexcept;

				scoped_lock(::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock && other) noexcept;

				scoped_lock(::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const & other) = delete;

				~scoped_lock();
				
				::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock & operator=(::framework::coroutine::mutex::awaiter_scoped_lock::scoped_lock const & other) = delete;

			private:
				::framework::coroutine::mutex * mutex;
			};

			using ::framework::coroutine::mutex::awaiter_lock::awaiter_lock;

			awaiter_scoped_lock(::framework::coroutine::mutex & mutex) noexcept;

			decltype(auto) await_resume() const noexcept;
		};

		mutex() noexcept;

		~mutex() noexcept;

		::framework::coroutine::mutex::awaiter_lock lock() noexcept;

		::framework::coroutine::mutex::awaiter_scoped_lock scoped_lock() noexcept;

		bool try_lock() noexcept;

		void unlock() noexcept;

	private:
		friend class ::framework::coroutine::mutex::awaiter_lock;

		static constexpr ::std::uintptr_t not_locked = 1;
		static constexpr ::std::uintptr_t locked_no_waiters = 0;
		static_assert(::framework::coroutine::mutex::locked_no_waiters == reinterpret_cast<::std::uintptr_t>(static_cast<void *>(nullptr)));

		mutable ::std::atomic<::std::uintptr_t> state;
		mutable ::framework::coroutine::mutex::awaiter_lock * waiters;
	};
}

#include "coroutine/mutex.inl"