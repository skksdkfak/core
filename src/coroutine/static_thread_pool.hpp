#pragma once

#define framework_multiqueue_thread_pool 0

#if framework_multiqueue_thread_pool
#include "concurrency/spin_mutex.hpp"
#include <atomic>
#include <condition_variable>
#include <coroutine>
#include <cstdint>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

namespace framework::coroutine
{
	class thread;

	class static_thread_pool
	{
	public:
		class schedule_operation
		{
		public:
			schedule_operation(::framework::coroutine::static_thread_pool * static_thread_pool) noexcept;

			bool await_ready() noexcept;

			void await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			void await_resume() noexcept;

		private:
			friend class ::framework::coroutine::static_thread_pool;

			::framework::coroutine::static_thread_pool * static_thread_pool;
			::std::coroutine_handle<> coroutine_handle;
			::framework::coroutine::static_thread_pool::schedule_operation * next;
		};

		static_thread_pool();

		explicit static_thread_pool(::std::uint32_t thread_count);

		~static_thread_pool();

		::framework::coroutine::static_thread_pool::schedule_operation schedule() noexcept;

	private:
		friend class ::framework::coroutine::static_thread_pool::schedule_operation;

		void run_worker_thread(::std::uint32_t const thread_index) noexcept;

		void shutdown();

		void schedule(::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation) noexcept;

		void remote_enqueue(::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation) noexcept;

		bool has_any_queued_work_for(::std::uint32_t const thread_index) noexcept;

		bool approx_has_any_queued_work_for(::std::uint32_t const thread_index) const noexcept;

		bool is_shutdown_requested() const noexcept;

		void notify_intent_to_sleep(::std::uint32_t const thread_index) noexcept;

		void try_clear_intent_to_sleep(::std::uint32_t const thread_index) noexcept;

		void wake_one_thread() noexcept;

		::framework::coroutine::static_thread_pool::schedule_operation * try_global_dequeue() noexcept;

		/// Try to steal a task from another thread.
		///
		/// \return
		/// A pointer to the operation that was stolen if one could be stolen
		/// from another thread. Otherwise returns nullptr if none of the other
		/// threads had any tasks that could be stolen.
		::framework::coroutine::static_thread_pool::schedule_operation * try_steal_from_other_thread(::std::uint32_t const this_thread_index) noexcept;

		static thread_local ::framework::coroutine::thread * current_state;
		static thread_local ::framework::coroutine::static_thread_pool * current_thread_pool;

		::std::uint32_t const thread_count;
		::std::unique_ptr<::framework::coroutine::thread[]> thread_states;
		::std::vector<::std::thread> threads;
		::std::atomic_bool stop_requested;
		::std::mutex global_queue_mutex;
		::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *> global_queue_head;
		::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *> global_queue_tail;
		::std::atomic_uint32_t sleeping_thread_count;
	};

	class thread
	{
	public:
		explicit thread();

		~thread();

		bool try_wake_up();

		bool notify_intent_to_sleep() noexcept;

		void sleep_until_woken() noexcept;

		bool approx_has_any_queued_work() const noexcept;

		bool has_any_queued_work() noexcept;

		bool try_local_enqueue(::framework::coroutine::static_thread_pool::schedule_operation *& operation) noexcept;

		::framework::coroutine::static_thread_pool::schedule_operation * try_local_pop() noexcept;

		::framework::coroutine::static_thread_pool::schedule_operation * try_steal(bool * lock_unavailable = nullptr) noexcept;

		static ::std::uint32_t get_id() noexcept;

	private:
		friend class ::framework::coroutine::static_thread_pool;

		using offset_t = ::std::make_signed_t<::std::size_t>;

		static constexpr ::framework::coroutine::thread::offset_t difference(::std::size_t a, ::std::size_t b);

		static thread_local ::std::uint32_t id;
		::std::thread worker_thread;
		::std::unique_ptr<::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *>[]> local_queue;
		::std::size_t mask;
		::std::atomic_size_t head;
		::std::atomic_size_t tail;
		::framework::concurrency::spin_mutex remote_mutex;
		::std::atomic_bool is_sleeping;
	};
}
#else
#include <condition_variable>
#include <coroutine>
#include <cstdint>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

namespace framework::coroutine
{
	class static_thread_pool;

	class thread
	{
	public:
		thread(::framework::coroutine::static_thread_pool & static_thread_pool);

		~thread();

		void run_worker_thread() noexcept;

	private:
		::framework::coroutine::static_thread_pool & static_thread_pool;
		::std::thread worker_thread;
	};

	class static_thread_pool
	{
	public:
		class schedule_operation
		{
		public:
			schedule_operation(::framework::coroutine::static_thread_pool const * static_thread_pool) noexcept;

			bool await_ready() noexcept;

			void await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			void await_resume() noexcept;

		private:
			::framework::coroutine::static_thread_pool const * static_thread_pool;
		};

		static_thread_pool();

		~static_thread_pool();

		explicit static_thread_pool(::std::uint32_t thread_count);

		bool get_job(::std::coroutine_handle<> & coroutine_handle);

		::framework::coroutine::static_thread_pool::schedule_operation schedule() noexcept;

		void schedule(::std::coroutine_handle<> coroutine_handle) const noexcept;

	private:
		friend class ::framework::coroutine::static_thread_pool::schedule_operation;

		mutable ::std::vector<::std::unique_ptr<::framework::coroutine::thread>> threads;
		mutable ::std::queue<::std::coroutine_handle<>> tasks;
		mutable ::std::mutex queue_mutex;
		mutable ::std::condition_variable condition;
		bool destroying = false;
	};
}
#endif

#include "coroutine/static_thread_pool.inl"