#define NOMINMAX
#include <algorithm>
#include <cassert>

inline constexpr ::std::uint32_t(::framework::coroutine::async_auto_reset_event::get_set_count)(::std::uint64_t state)
{
	return static_cast<::std::uint32_t>(state);
}

inline constexpr ::std::uint32_t(::framework::coroutine::async_auto_reset_event::get_waiter_count)(::std::uint64_t state)
{
	return static_cast<::std::uint32_t>(state >> 32);
}

inline constexpr ::std::uint32_t(::framework::coroutine::async_auto_reset_event::get_resumable_waiter_count)(::std::uint64_t state)
{
	return ::std::min(::framework::coroutine::async_auto_reset_event::get_set_count(state), ::framework::coroutine::async_auto_reset_event::get_waiter_count(state));
}

inline ::framework::coroutine::async_auto_reset_event::async_auto_reset_event() noexcept :
	::framework::coroutine::async_auto_reset_event(false)
{
}

inline ::framework::coroutine::async_auto_reset_event::async_auto_reset_event(bool state) noexcept :
	state(state ? ::framework::coroutine::async_auto_reset_event::set_increment : 0),
	new_waiters(nullptr),
	waiters(nullptr)
{
}

inline ::framework::coroutine::async_auto_reset_event::~async_auto_reset_event() noexcept
{
	assert(this->new_waiters.load(::std::memory_order::relaxed) == nullptr);
	assert(this->waiters == nullptr);
}

inline ::framework::coroutine::async_auto_reset_event::awaiter(::framework::coroutine::async_auto_reset_event::operator co_await)() const noexcept
{
	::std::uint64_t old_state = this->state.load(::std::memory_order::relaxed);
	if (::framework::coroutine::async_auto_reset_event::get_set_count(old_state) > ::framework::coroutine::async_auto_reset_event::get_waiter_count(old_state))
	{
		if (this->state.compare_exchange_strong(old_state, old_state - ::framework::coroutine::async_auto_reset_event::set_increment, ::std::memory_order::acquire, ::std::memory_order::relaxed))
		{
			return ::framework::coroutine::async_auto_reset_event::awaiter();
		}
	}

	return ::framework::coroutine::async_auto_reset_event::awaiter(this);
}

inline void ::framework::coroutine::async_auto_reset_event::set() noexcept
{
	::std::uint64_t old_state = this->state.load(::std::memory_order::relaxed);
	do
	{
		if (::framework::coroutine::async_auto_reset_event::get_set_count(old_state) > ::framework::coroutine::async_auto_reset_event::get_waiter_count(old_state))
		{
			return;
		}
	} while (!this->state.compare_exchange_weak(old_state, old_state + ::framework::coroutine::async_auto_reset_event::set_increment, ::std::memory_order::acq_rel, ::std::memory_order::acquire));

	if (old_state != 0 && framework::coroutine::async_auto_reset_event::get_set_count(old_state) == 0)
	{
		this->resume_waiters(old_state + ::framework::coroutine::async_auto_reset_event::set_increment);
	}
}

inline void ::framework::coroutine::async_auto_reset_event::reset() noexcept
{
	::std::uint64_t old_state = this->state.load(::std::memory_order::relaxed);
	while (::framework::coroutine::async_auto_reset_event::get_set_count(old_state) > ::framework::coroutine::async_auto_reset_event::get_waiter_count(old_state))
	{
		if (this->state.compare_exchange_weak(old_state, old_state - ::framework::coroutine::async_auto_reset_event::set_increment, ::std::memory_order::relaxed))
		{
			return;
		}
	}
}

inline void ::framework::coroutine::async_auto_reset_event::resume_waiters(::std::uint64_t initial_state) const noexcept
{
	::framework::coroutine::async_auto_reset_event::awaiter * waiters_to_resume_list = nullptr;
	::framework::coroutine::async_auto_reset_event::awaiter ** waiters_to_resume_list_end = &waiters_to_resume_list;

	::std::uint32_t waiters_to_resume_count = ::framework::coroutine::async_auto_reset_event::get_resumable_waiter_count(initial_state);

	assert(waiters_to_resume_count > 0);

	do
	{
		for (::std::uint32_t i = 0; i < waiters_to_resume_count; i++)
		{
			if (this->waiters == nullptr)
			{
				::framework::coroutine::async_auto_reset_event::awaiter * new_waiters = this->new_waiters.exchange(nullptr, ::std::memory_order::acquire);
				assert(new_waiters != nullptr);
				do
				{
					::framework::coroutine::async_auto_reset_event::awaiter * next = new_waiters->next;
					new_waiters->next = this->waiters;
					this->waiters = new_waiters;
					new_waiters = next;
				} while (new_waiters != nullptr);
			}

			assert(this->waiters != nullptr);

			::framework::coroutine::async_auto_reset_event::awaiter * waiter_to_resume = this->waiters;
			this->waiters = this->waiters->next;

			waiter_to_resume->next = nullptr;
			*waiters_to_resume_list_end = waiter_to_resume;
			waiters_to_resume_list_end = &waiter_to_resume->next;
		}
		::std::uint64_t const delta = ::std::uint64_t(waiters_to_resume_count) | ::std::uint64_t(waiters_to_resume_count) << 32;
		::std::uint64_t const new_state = this->state.fetch_sub(delta, ::std::memory_order::acq_rel) - delta;

		waiters_to_resume_count = ::framework::coroutine::async_auto_reset_event::get_resumable_waiter_count(new_state);
	} while (waiters_to_resume_count > 0);

	assert(waiters_to_resume_list != nullptr);

	do
	{
		::framework::coroutine::async_auto_reset_event::awaiter * const waiter = waiters_to_resume_list;
		::framework::coroutine::async_auto_reset_event::awaiter * next = waiters_to_resume_list->next;

		if (waiter->ref_count.fetch_sub(1, ::std::memory_order::release) == 1)
		{
			waiter->coroutine_handle.resume();
		}

		waiters_to_resume_list = next;
	} while (waiters_to_resume_list != nullptr);
}

inline ::framework::coroutine::async_auto_reset_event::awaiter::awaiter() noexcept :
	event(nullptr)
{
}

inline ::framework::coroutine::async_auto_reset_event::awaiter::awaiter(::framework::coroutine::async_auto_reset_event const * event) noexcept :
	event(event),
	ref_count(2)
{
}

inline ::framework::coroutine::async_auto_reset_event::awaiter::awaiter(::framework::coroutine::async_auto_reset_event::awaiter const & other) noexcept :
	event(other.event),
	ref_count(2)
{
}

inline bool ::framework::coroutine::async_auto_reset_event::awaiter::await_ready() noexcept
{
	return this->event == nullptr;
}

inline bool ::framework::coroutine::async_auto_reset_event::awaiter::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	this->coroutine_handle = coroutine_handle;

	::framework::coroutine::async_auto_reset_event::awaiter * head = this->event->new_waiters.load(::std::memory_order::relaxed);
	do
	{
		this->next = head;
	} while (!this->event->new_waiters.compare_exchange_weak(head, this, ::std::memory_order::release, ::std::memory_order::relaxed));

	const ::std::uint64_t old_state = this->event->state.fetch_add(::framework::coroutine::async_auto_reset_event::waiter_increment, ::std::memory_order::acq_rel);
	if (old_state != 0 && ::framework::coroutine::async_auto_reset_event::get_waiter_count(old_state) == 0)
	{
		this->event->resume_waiters(old_state + ::framework::coroutine::async_auto_reset_event::waiter_increment);
	}

	return this->ref_count.fetch_sub(1, ::std::memory_order::acquire) != 1;
}

inline void ::framework::coroutine::async_auto_reset_event::awaiter::await_resume() noexcept
{
}