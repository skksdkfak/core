#include <cassert>

template<typename T>
inline ::framework::coroutine::lazy_task<T>::lazy_task() noexcept :
	coroutine_handle(nullptr)
{
}

template<typename T>
inline ::framework::coroutine::lazy_task<T>::lazy_task(::std::coroutine_handle<typename ::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle) noexcept :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline ::framework::coroutine::lazy_task<T>::lazy_task(::framework::coroutine::lazy_task<T> && other) noexcept :
	coroutine_handle(other.coroutine_handle)
{
	other.coroutine_handle = nullptr;
}

template<typename T>
inline ::framework::coroutine::lazy_task<T>::~lazy_task()
{
	if (this->coroutine_handle)
	{
		this->coroutine_handle.destroy();
	}
}

template<typename T>
::framework::coroutine::lazy_task<T> & ::framework::coroutine::lazy_task<T>::operator=(::framework::coroutine::lazy_task<T> && other) noexcept
{
	if (::std::addressof(other) != this)
	{
		if (this->coroutine_handle)
		{
			this->coroutine_handle.destroy();
		}

		this->coroutine_handle = other.coroutine_handle;
		other.coroutine_handle = nullptr;
	}

	return *this;
}

template<typename T>
inline auto ::framework::coroutine::lazy_task<T>::operator co_await() const & noexcept
{
	struct awaitable : public ::framework::coroutine::lazy_task<T>::awaitable_base
	{
		using ::framework::coroutine::lazy_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return this->coroutine_handle.promise().result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline auto ::framework::coroutine::lazy_task<T>::operator co_await() const && noexcept
{
	struct awaitable : public ::framework::coroutine::lazy_task<T>::awaitable_base
	{
		using ::framework::coroutine::lazy_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return ::std::move(this->coroutine_handle.promise()).result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline bool ::framework::coroutine::lazy_task<T>::done() const noexcept
{
	return !this->coroutine_handle || this->coroutine_handle.done();
}

template<typename T>
inline decltype(auto) ::framework::coroutine::lazy_task<T>::result()
{
	return this->coroutine_handle.promise().result();
}

template<typename T>
inline auto ::framework::coroutine::lazy_task<T>::promise_type::get_return_object() noexcept
{
	return ::framework::coroutine::lazy_task<T>(::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type>::from_promise(static_cast<::framework::coroutine::lazy_task<T>::promise_type &>(*this)));
}

template<typename T>
inline auto ::framework::coroutine::lazy_task<T>::promise_type::initial_suspend() const
{
	return ::std::suspend_always();
}

template<typename T>
inline auto ::framework::coroutine::lazy_task<T>::promise_type::final_suspend() const noexcept
{
	struct awaitable
	{
		bool await_ready() noexcept
		{
			return false;
		}

		::std::coroutine_handle<> await_suspend(::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle) noexcept
		{
			return coroutine_handle.promise().continuation;
		}

		void await_resume() noexcept
		{
		}
	};

	return awaitable();
}

template<typename T>
inline void ::framework::coroutine::lazy_task<T>::promise_type::set_continuation(::std::coroutine_handle<> continuation) noexcept
{
	this->continuation = continuation;
}

template<typename T>
inline ::framework::coroutine::lazy_task<T>::awaitable_base::awaitable_base(::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle) :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline bool ::framework::coroutine::lazy_task<T>::awaitable_base::await_ready() noexcept
{
	return this->coroutine_handle.done();
}

template<typename T>
inline ::std::coroutine_handle<> framework::coroutine::lazy_task<T>::awaitable_base::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	this->coroutine_handle.promise().set_continuation(coroutine_handle);
	return this->coroutine_handle;
}

template<typename T>
inline ::framework::coroutine::immediate_task<T>::immediate_task() noexcept :
	coroutine_handle(nullptr)
{
}

template<typename T>
inline ::framework::coroutine::immediate_task<T>::immediate_task(::std::coroutine_handle<typename ::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle) noexcept :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline ::framework::coroutine::immediate_task<T>::immediate_task(::framework::coroutine::immediate_task<T> && other) noexcept :
	coroutine_handle(other.coroutine_handle)
{
	other.coroutine_handle = nullptr;
}

template<typename T>
inline ::framework::coroutine::immediate_task<T>::~immediate_task()
{
	if (this->coroutine_handle)
	{
		this->coroutine_handle.destroy();
	}
}

template<typename T>
::framework::coroutine::immediate_task<T> & ::framework::coroutine::immediate_task<T>::operator=(::framework::coroutine::immediate_task<T> && other) noexcept
{
	if (::std::addressof(other) != this)
	{
		if (this->coroutine_handle)
		{
			this->coroutine_handle.destroy();
		}

		this->coroutine_handle = other.coroutine_handle;
		other.coroutine_handle = nullptr;
	}

	return *this;
}

template<typename T>
inline auto ::framework::coroutine::immediate_task<T>::operator co_await() const & noexcept
{
	struct awaitable : public ::framework::coroutine::immediate_task<T>::awaitable_base
	{
		using ::framework::coroutine::immediate_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return this->coroutine_handle.promise().result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline auto ::framework::coroutine::immediate_task<T>::operator co_await() const && noexcept
{
	struct awaitable : public ::framework::coroutine::immediate_task<T>::awaitable_base
	{
		using ::framework::coroutine::immediate_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return ::std::move(this->coroutine_handle.promise()).result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline bool ::framework::coroutine::immediate_task<T>::done() const noexcept
{
	return !this->coroutine_handle || this->coroutine_handle.done();
}

template<typename T>
inline decltype(auto) ::framework::coroutine::immediate_task<T>::result()
{
	return this->coroutine_handle.promise().result();
}

template<typename T>
inline auto ::framework::coroutine::immediate_task<T>::promise_type::get_return_object() noexcept
{
	return ::framework::coroutine::immediate_task<T>(::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type>::from_promise(static_cast<::framework::coroutine::immediate_task<T>::promise_type &>(*this)));
}

template<typename T>
inline auto ::framework::coroutine::immediate_task<T>::promise_type::initial_suspend() const
{
	return ::std::suspend_never();
}

template<typename T>
inline auto ::framework::coroutine::immediate_task<T>::promise_type::final_suspend() const noexcept
{
	struct awaitable
	{
		bool await_ready() noexcept
		{
			return false;
		}

		::std::coroutine_handle<> await_suspend(::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle) noexcept
		{
			if (coroutine_handle.promise().has_continuation.exchange(true, ::std::memory_order::acquire))
			{
				return coroutine_handle.promise().continuation;
			}
			else
			{
				return ::std::noop_coroutine();
			}
		}

		void await_resume() noexcept
		{
		}
	};

	return awaitable();
}

template<typename T>
inline bool ::framework::coroutine::immediate_task<T>::promise_type::set_continuation(::std::coroutine_handle<> continuation) noexcept
{
	this->continuation = continuation;
	return this->has_continuation.exchange(true, ::std::memory_order::release) == false;
}

template<typename T>
inline ::framework::coroutine::immediate_task<T>::awaitable_base::awaitable_base(::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle) :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline bool ::framework::coroutine::immediate_task<T>::awaitable_base::await_ready() noexcept
{
	return this->coroutine_handle.done();
}

template<typename T>
inline bool framework::coroutine::immediate_task<T>::awaitable_base::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	return this->coroutine_handle.promise().set_continuation(coroutine_handle);
}

inline auto ::framework::coroutine::self_destructible_coroutine::promise_type::get_return_object() const noexcept
{
	return ::framework::coroutine::self_destructible_coroutine();
}

inline void ::framework::coroutine::self_destructible_coroutine::promise_type::unhandled_exception() const noexcept
{
	assert(false);
}

inline void ::framework::coroutine::self_destructible_coroutine::promise_type::return_void() const noexcept
{
}

inline auto ::framework::coroutine::self_destructible_coroutine::promise_type::initial_suspend() const noexcept
{
	return ::std::suspend_never();
}

inline auto ::framework::coroutine::self_destructible_coroutine::promise_type::final_suspend() const noexcept
{
	return ::std::suspend_never();
}

template<typename T>
inline ::framework::coroutine::promise<T>::promise() noexcept
{
}

template<typename T>
inline ::framework::coroutine::promise<T>::~promise()
{
	switch (this->result_type)
	{
	case ::framework::coroutine::promise<T>::result_type::value:
		this->value.~T();
		break;
	case ::framework::coroutine::promise<T>::result_type::exception:
		this->exception.~exception_ptr();
		break;
	default:
		break;
	}
}

template<typename T>
inline void ::framework::coroutine::promise<T>::unhandled_exception() noexcept
{
	::new (static_cast<void *>(::std::addressof(this->exception))) ::std::exception_ptr(::std::current_exception());
	this->result_type = ::framework::coroutine::promise<T>::result_type::exception;
}

template<typename T>
template<typename VALUE, typename>
inline void ::framework::coroutine::promise<T>::return_value(VALUE && value) noexcept(::std::is_nothrow_constructible_v<T, VALUE &&>)
{
	::new (static_cast<void *>(::std::addressof(this->value))) T(::std::forward<VALUE>(value));
	this->result_type = ::framework::coroutine::promise<T>::result_type::value;
}

template<typename T>
inline T & ::framework::coroutine::promise<T>::result() &
{
	if (this->result_type == ::framework::coroutine::promise<T>::result_type::exception)
	{
		::std::rethrow_exception(this->exception);
	}

	return this->value;
}

template<typename T>
inline T && ::framework::coroutine::promise<T>::result() &&
{
	if (this->result_type == ::framework::coroutine::promise<T>::result_type::exception)
	{
		::std::rethrow_exception(this->exception);
	}

	return ::std::move(this->value);
}

template<typename T>
inline void ::framework::coroutine::promise<T &>::unhandled_exception() noexcept
{
	this->exception = ::std::current_exception();
}

template<typename T>
inline void ::framework::coroutine::promise<T &>::return_value(T & value) noexcept
{
	this->value = ::std::addressof(value);
}

template<typename T>
inline T & ::framework::coroutine::promise<T &>::result()
{
	if (this->exception)
	{
		::std::rethrow_exception(this->exception);
	}

	return *this->value;
}

inline void ::framework::coroutine::promise<void>::unhandled_exception() noexcept
{
	this->exception = ::std::current_exception();
}

inline void ::framework::coroutine::promise<void>::return_void() noexcept
{
}

inline void ::framework::coroutine::promise<void>::result()
{
	if (this->exception)
	{
		::std::rethrow_exception(this->exception);
	}
}