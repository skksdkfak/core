#include <cassert>

inline framework::coroutine::async_manual_reset_event::async_manual_reset_event() noexcept :
	::framework::coroutine::async_manual_reset_event(false)
{
}

inline ::framework::coroutine::async_manual_reset_event::async_manual_reset_event(bool state) noexcept :
	state(state ? this : nullptr)
{
}

inline ::framework::coroutine::async_manual_reset_event::~async_manual_reset_event() noexcept
{
	[[maybe_unused]] void * const state = this->state.load(::std::memory_order::relaxed);
	assert(state == nullptr || state == this);
}

inline ::framework::coroutine::async_manual_reset_event::awaiter(::framework::coroutine::async_manual_reset_event::operator co_await)() const noexcept
{
	return ::framework::coroutine::async_manual_reset_event::awaiter(this);
}

inline bool ::framework::coroutine::async_manual_reset_event::is_set() const noexcept
{
	return this->state.load(::std::memory_order::acquire) == this;
}

inline void ::framework::coroutine::async_manual_reset_event::set() noexcept
{
	void * const set_state = this;
	void * old_state = this->state.exchange(set_state, ::std::memory_order::acq_rel);
	if (old_state != set_state)
	{
		::framework::coroutine::async_manual_reset_event::awaiter * current = static_cast<::framework::coroutine::async_manual_reset_event::awaiter *>(old_state);
		while (current != nullptr)
		{
			::framework::coroutine::async_manual_reset_event::awaiter * next = current->next;
			current->coroutine_handle.resume();
			current = next;
		}
	}
}

inline void ::framework::coroutine::async_manual_reset_event::reset() noexcept
{
	void * old_state = this;
	this->state.compare_exchange_strong(old_state, nullptr, ::std::memory_order::relaxed);
}

inline ::framework::coroutine::async_manual_reset_event::awaiter::awaiter(::framework::coroutine::async_manual_reset_event const * event) noexcept :
	event(event)
{
}

inline bool ::framework::coroutine::async_manual_reset_event::awaiter::await_ready() noexcept
{
	return this->event->is_set();
}

inline bool ::framework::coroutine::async_manual_reset_event::awaiter::await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
{
	this->coroutine_handle = coroutine_handle;

	void const * set_state = this->event;
	void * old_state = this->event->state.load(::std::memory_order::acquire);
	do
	{
		if (old_state == set_state)
		{
			return false;
		}

		this->next = static_cast<::framework::coroutine::async_manual_reset_event::awaiter *>(old_state);
	} while (!this->event->state.compare_exchange_weak(old_state, this, ::std::memory_order::release, ::std::memory_order::acquire));

	return true;
}

inline void ::framework::coroutine::async_manual_reset_event::awaiter::await_resume() noexcept
{
}