#pragma once

#include "coroutine/task.hpp"
#include "coroutine/type_traits.hpp"
#include <type_traits>
#include <utility>

namespace framework::coroutine
{
	template<typename SCHEDULER, typename AWAITABLE, typename AWAIT_RESULT = typename ::framework::coroutine::awaitable_traits<AWAITABLE>::await_result_t>
	auto schedule_on(SCHEDULER & scheduler, AWAITABLE awaitable) -> ::framework::coroutine::lazy_task<::std::conditional_t<::std::is_lvalue_reference_v<AWAIT_RESULT>, AWAIT_RESULT, ::std::remove_reference_t<AWAIT_RESULT>>>
	{
		co_await scheduler.schedule();
		co_return co_await ::std::move(awaitable);
	}
}