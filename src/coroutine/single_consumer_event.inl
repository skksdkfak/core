inline ::framework::coroutine::single_consumer_event::single_consumer_event(bool state) noexcept :
	state(state ? this : nullptr)
{
}

inline bool ::framework::coroutine::single_consumer_event::is_set() const noexcept
{
	void const * const set_state = this;
	return this->state.load(::std::memory_order::acquire) == set_state;
}

inline void ::framework::coroutine::single_consumer_event::set()
{
	void * const not_set_state = nullptr;
	void * const set_state = this;
	void * const old_state = this->state.exchange(set_state, ::std::memory_order::acq_rel);
	if (old_state != not_set_state && old_state != set_state)
	{
		static_cast<::framework::coroutine::single_consumer_event::awaiter *>(old_state)->coroutine_handle.resume();
	}
}

inline void ::framework::coroutine::single_consumer_event::reset() noexcept
{
	void * const not_set_state = nullptr;
	void * const set_state = this;
	void * old_state = set_state;
	this->state.compare_exchange_strong(old_state, not_set_state, ::std::memory_order::relaxed);
}

inline auto ::framework::coroutine::single_consumer_event::operator co_await() noexcept
{
	return ::framework::coroutine::single_consumer_event::awaiter(*this);
}

inline ::framework::coroutine::single_consumer_event::awaiter::awaiter(::framework::coroutine::single_consumer_event & event) :
	single_consumer_event(event)
{
}

inline bool ::framework::coroutine::single_consumer_event::awaiter::await_ready() const noexcept
{
	return this->single_consumer_event.is_set();
}

inline bool ::framework::coroutine::single_consumer_event::awaiter::await_suspend(::std::coroutine_handle<> awaiter)
{
	this->coroutine_handle = awaiter;
	void * const not_set_state = nullptr;
	void * const not_set_consumer_waiting = this;
	void * old_state = not_set_state;
	return this->single_consumer_event.state.compare_exchange_strong(old_state, not_set_consumer_waiting, ::std::memory_order::release, ::std::memory_order::acquire);
}

inline void ::framework::coroutine::single_consumer_event::awaiter::await_resume() noexcept
{
}