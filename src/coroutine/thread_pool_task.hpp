#pragma once

#include "coroutine/static_thread_pool.hpp"
#include "coroutine/task.hpp"
#include "coroutine/type_traits.hpp"
#include <coroutine>

namespace framework::coroutine
{
	template<typename T>
	class thread_pool_task final
	{
	public:
		class promise_type;

		thread_pool_task() noexcept;

		explicit thread_pool_task(::std::coroutine_handle<typename ::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle) noexcept;

		thread_pool_task(::framework::coroutine::thread_pool_task<T> && other) noexcept;

		thread_pool_task(::framework::coroutine::thread_pool_task<T> const &) = delete;

		~thread_pool_task();

		::framework::coroutine::thread_pool_task<T> & operator=(::framework::coroutine::thread_pool_task<T> const &) = delete;

		::framework::coroutine::thread_pool_task<T> & operator=(::framework::coroutine::thread_pool_task<T> && other) noexcept;

		constexpr explicit operator bool() const noexcept;

		auto operator co_await() const & noexcept;

		auto operator co_await() const && noexcept;

		bool done() const noexcept;

		decltype(auto) result();

	private:
		struct awaitable_base
		{
			explicit awaitable_base(::std::coroutine_handle<::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle);

			bool await_ready() noexcept;

			bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			::std::coroutine_handle<::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle;
		};

		::std::coroutine_handle<::framework::coroutine::thread_pool_task<T>::promise_type> coroutine_handle;
	};

	template<typename T>
	class thread_pool_task<T>::promise_type : public ::framework::coroutine::promise<T>
	{
	public:
		promise_type(::framework::coroutine::static_thread_pool & thread_pool) noexcept;

		template<typename ... TYPES>
		promise_type(::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept;

		template<typename TYPE>
		promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool) noexcept;

		template<typename TYPE, typename ... TYPES>
		promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept;

		auto get_return_object() noexcept;

		auto initial_suspend() const;

		auto final_suspend() const noexcept;

		bool set_continuation(::std::coroutine_handle<> continuation) noexcept;

	private:
		::framework::coroutine::static_thread_pool & thread_pool;
		::std::atomic_bool has_continuation = false;
		::std::coroutine_handle<> continuation;
	};

	class self_destructible_thread_pool_coroutine final
	{
	public:
		class promise_type
		{
		public:
			promise_type(::framework::coroutine::static_thread_pool & thread_pool) noexcept;

			template<typename ... TYPES>
			promise_type(::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept;

			template<typename TYPE>
			promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool) noexcept;

			template<typename TYPE, typename ... TYPES>
			promise_type(TYPE &, ::framework::coroutine::static_thread_pool & thread_pool, TYPES ...) noexcept;

			auto get_return_object() const noexcept;

			void unhandled_exception() const noexcept;

			void return_void() const noexcept;

			auto initial_suspend() const noexcept;

			auto final_suspend() const noexcept;

		private:
			::framework::coroutine::static_thread_pool & thread_pool;
		};

		self_destructible_thread_pool_coroutine() noexcept = default;

		self_destructible_thread_pool_coroutine(::framework::coroutine::self_destructible_thread_pool_coroutine const &) = delete;

		::framework::coroutine::self_destructible_thread_pool_coroutine & operator=(::framework::coroutine::self_destructible_thread_pool_coroutine const &) = delete;
	};
}

#include "coroutine/thread_pool_task.inl"