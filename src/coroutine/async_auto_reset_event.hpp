#pragma once

#include <atomic>
#include <coroutine>
#include <cstdint>

namespace framework::coroutine
{
	class async_auto_reset_event
	{
	public:
		class awaiter
		{
		public:
			awaiter() noexcept;

			awaiter(::framework::coroutine::async_auto_reset_event const * event) noexcept;

			awaiter(::framework::coroutine::async_auto_reset_event::awaiter const & other) noexcept;

			bool await_ready() noexcept;

			bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			void await_resume() noexcept;

		protected:
			friend class ::framework::coroutine::async_auto_reset_event;

			::framework::coroutine::async_auto_reset_event const * event;
			::framework::coroutine::async_auto_reset_event::awaiter * next;
			::std::coroutine_handle<> coroutine_handle;
			::std::atomic_uint32_t ref_count;
		};

		async_auto_reset_event() noexcept;

		async_auto_reset_event(bool state) noexcept;

		~async_auto_reset_event() noexcept;

		::framework::coroutine::async_auto_reset_event::awaiter operator co_await() const noexcept;

		void set() noexcept;

		void reset() noexcept;

	private:
		static constexpr ::std::uint64_t set_increment = 1;
		static constexpr ::std::uint64_t waiter_increment = ::std::uint64_t(1) << 32;

		static constexpr ::std::uint32_t get_set_count(::std::uint64_t state);

		static constexpr ::std::uint32_t get_waiter_count(::std::uint64_t state);

		static constexpr ::std::uint32_t get_resumable_waiter_count(::std::uint64_t state);

		void resume_waiters(::std::uint64_t initial_state) const noexcept;

		mutable ::std::atomic_uint64_t state;
		mutable ::std::atomic<::framework::coroutine::async_auto_reset_event::awaiter *> new_waiters;
		mutable ::framework::coroutine::async_auto_reset_event::awaiter * waiters;
	};
}

#include "coroutine/async_auto_reset_event.inl"