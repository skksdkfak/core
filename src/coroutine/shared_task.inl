#include <stdexcept>
#include <utility>

inline bool ::framework::coroutine::shared_task_promise_base::final_awaiter::await_ready() const noexcept
{
	return false;
}

template<typename PROMISE>
inline void ::framework::coroutine::shared_task_promise_base::final_awaiter::await_suspend(::std::coroutine_handle<PROMISE> coroutine_handle) noexcept
{
	::framework::coroutine::shared_task_promise_base & promise = coroutine_handle.promise();

	// Exchange operation needs to be 'release' so that subsequent awaiters have
	// visibility of the result. Also needs to be 'acquire' so we have visibility
	// of writes to the waiters list.
	void * const value_ready_value = &promise;
	void * waiters = promise.waiters.exchange(value_ready_value, ::std::memory_order::acq_rel);
	if (waiters != nullptr)
	{
		::framework::coroutine::shared_task_waiter * waiter = static_cast<::framework::coroutine::shared_task_waiter *>(waiters);
		while (waiter->next != nullptr)
		{
			// Read the next pointer before resuming the coroutine
			// since resuming the coroutine may destroy the shared_task_waiter value.
			::framework::coroutine::shared_task_waiter * next = waiter->next;
			waiter->continuation.resume();
			waiter = next;
		}

		// Resume last waiter in tail position to allow it to potentially
		// be compiled as a tail-call.
		waiter->continuation.resume();
	}
}

inline void ::framework::coroutine::shared_task_promise_base::final_awaiter::await_resume() noexcept
{
}

inline ::framework::coroutine::shared_task_promise_base::shared_task_promise_base() noexcept :
	ref_count(1),
	waiters(&this->waiters),
	exception(nullptr)
{
}

inline ::std::suspend_always(::framework::coroutine::shared_task_promise_base::initial_suspend)() noexcept
{
	return ::std::suspend_always();
}

inline ::framework::coroutine::shared_task_promise_base::final_awaiter(::framework::coroutine::shared_task_promise_base::final_suspend)() noexcept
{
	return ::framework::coroutine::shared_task_promise_base::final_awaiter();
}

inline void ::framework::coroutine::shared_task_promise_base::unhandled_exception() noexcept
{
	this->exception = ::std::current_exception();
}

inline bool ::framework::coroutine::shared_task_promise_base::is_ready() const noexcept
{
	const void * const value_ready_value = this;
	return this->waiters.load(::std::memory_order::acquire) == value_ready_value;
}

inline void ::framework::coroutine::shared_task_promise_base::add_ref() noexcept
{
	this->ref_count.fetch_add(1, ::std::memory_order::relaxed);
}

inline bool ::framework::coroutine::shared_task_promise_base::try_detach() noexcept
{
	return this->ref_count.fetch_sub(1, ::std::memory_order::acq_rel) != 1;
}

inline bool ::framework::coroutine::shared_task_promise_base::try_await(::framework::coroutine::shared_task_waiter * waiter, ::std::coroutine_handle<> coroutine)
{
	void * const value_ready_value = this;
	void * const not_started_value = &this->waiters;
	constexpr void * started_no_waiters_value = static_cast<::framework::coroutine::shared_task_waiter *>(nullptr);

	// NOTE: If the coroutine is not yet started then the first waiter
	// will start the coroutine before enqueuing itself up to the list
	// of suspended waiters waiting for completion. We split this into
	// two steps to allow the first awaiter to return without suspending.
	// This avoids recursively resuming the first waiter inside the call to
	// coroutine.resume() in the case that the coroutine completes
	// synchronously, which could otherwise lead to stack-overflow if
	// the awaiting coroutine awaited many synchronously-completing
	// tasks in a row.

	// Start the coroutine if not already started.
	void * old_waiters = this->waiters.load(::std::memory_order::acquire);
	if (old_waiters == not_started_value && this->waiters.compare_exchange_strong(old_waiters, started_no_waiters_value, ::std::memory_order::relaxed))
	{
		// Start the task executing.
		coroutine.resume();
		old_waiters = this->waiters.load(::std::memory_order::acquire);
	}

	// Enqueue the waiter into the list of waiting coroutines.
	do
	{
		if (old_waiters == value_ready_value)
		{
			// Coroutine already completed, don't suspend.
			return false;
		}

		waiter->next = static_cast<::framework::coroutine::shared_task_waiter *>(old_waiters);
	} while (!this->waiters.compare_exchange_weak(old_waiters, static_cast<void *>(waiter), ::std::memory_order::release, ::std::memory_order::acquire));

	return true;
}

inline bool ::framework::coroutine::shared_task_promise_base::completed_with_unhandled_exception()
{
	return this->exception != nullptr;
}

inline void ::framework::coroutine::shared_task_promise_base::rethrow_if_unhandled_exception()
{
	if (this->exception != nullptr)
	{
		::std::rethrow_exception(this->exception);
	}
}

template<typename T>
inline ::framework::coroutine::shared_task_promise<T>::~shared_task_promise()
{
	if (this->is_ready() && !this->completed_with_unhandled_exception())
	{
		reinterpret_cast<T *>(&this->value_storage)->~T();
	}
}

template<typename T>
inline ::framework::coroutine::shared_task<T>(::framework::coroutine::shared_task_promise<T>::get_return_object)() noexcept
{
	return ::framework::coroutine::shared_task<T>{::std::coroutine_handle<::framework::coroutine::shared_task_promise<T>>::from_promise(*this)};
}

template<typename T>
template<typename VALUE, typename>
inline void ::framework::coroutine::shared_task_promise<T>::return_value(VALUE && value) noexcept(::std::is_nothrow_constructible_v<T, VALUE &&>)
{
	new (&this->value_storage) T(::std::forward<VALUE>(value));
}

template<typename T>
inline T & ::framework::coroutine::shared_task_promise<T>::result()
{
	this->rethrow_if_unhandled_exception();
	return *reinterpret_cast<T *>(&this->value_storage);
}

inline ::framework::coroutine::shared_task<void>(::framework::coroutine::shared_task_promise<void>::get_return_object)() noexcept
{
	return ::framework::coroutine::shared_task<void>{::std::coroutine_handle<::framework::coroutine::shared_task_promise<void>>::from_promise(*this)};
}

inline void ::framework::coroutine::shared_task_promise<void>::return_void() noexcept
{
}

inline void ::framework::coroutine::shared_task_promise<void>::result()
{
	this->rethrow_if_unhandled_exception();
}

template<typename T>
inline ::framework::coroutine::shared_task<T &>(::framework::coroutine::shared_task_promise<T &>::get_return_object)() noexcept
{
	return ::framework::coroutine::shared_task<T &>{::std::coroutine_handle<::framework::coroutine::shared_task_promise<T &>>::from_promise(*this)};
}

template<typename T>
inline void ::framework::coroutine::shared_task_promise<T &>::return_value(T & value) noexcept
{
	this->value = ::std::addressof(value);
}

template<typename T>
inline T & ::framework::coroutine::shared_task_promise<T &>::result()
{
	this->rethrow_if_unhandled_exception();
	return *this->value;
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::awaitable_base::awaitable_base(::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine_handle) noexcept :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline bool ::framework::coroutine::shared_task<T>::awaitable_base::await_ready() const noexcept
{
	return !this->coroutine_handle || this->coroutine_handle.promise().is_ready();
}

template<typename T>
inline bool ::framework::coroutine::shared_task<T>::awaitable_base::await_suspend(::std::coroutine_handle<> awaiter) noexcept
{
	this->waiter.continuation = awaiter;
	return this->coroutine_handle.promise().try_await(&this->waiter, this->coroutine_handle);
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::shared_task() noexcept :
	coroutine_handle(nullptr)
{
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::shared_task(::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine_handle) :
	coroutine_handle(coroutine_handle)
{
	// Don't increment the ref-count here since it has already been
	// initialised to 2 (one for shared_task and one for coroutine)
	// in the shared_task_promise constructor.
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::shared_task(::framework::coroutine::shared_task<T> && other) noexcept :
	coroutine_handle(other.coroutine_handle)
{
	other.coroutine_handle = nullptr;
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::shared_task(::framework::coroutine::shared_task<T> const & other) noexcept :
	coroutine_handle(other.coroutine_handle)
{
	if (this->coroutine_handle)
	{
		this->coroutine_handle.promise().add_ref();
	}
}

template<typename T>
inline ::framework::coroutine::shared_task<T>::~shared_task()
{
	this->destroy();
}

template<typename T>
inline ::framework::coroutine::shared_task<T> & ::framework::coroutine::shared_task<T>::operator=(::framework::coroutine::shared_task<T> && other) noexcept
{
	if (&other != this)
	{
		this->destroy();
		this->coroutine_handle = other.coroutine_handle;
		other.coroutine_handle = nullptr;
	}

	return *this;
}

template<typename T>
inline ::framework::coroutine::shared_task<T> & ::framework::coroutine::shared_task<T>::operator=(::framework::coroutine::shared_task<T> const & other) noexcept
{
	if (this->coroutine_handle != other.coroutine_handle)
	{
		this->destroy();
		this->coroutine_handle = other.coroutine_handle;
		if (this->coroutine_handle)
		{
			this->coroutine_handle.promise().add_ref();
		}
	}

	return *this;
}

template<typename T>
inline constexpr ::framework::coroutine::shared_task<T>::operator bool() const noexcept
{
	return this->coroutine_handle != nullptr;
}

template<typename T>
inline void ::framework::coroutine::shared_task<T>::swap(::framework::coroutine::shared_task<T> & other) noexcept
{
	::std::swap(this->coroutine_handle, other.coroutine_handle);
}

template<typename T>
inline bool ::framework::coroutine::shared_task<T>::is_ready() const noexcept
{
	return !this->coroutine_handle || this->coroutine_handle.promise().is_ready();
}

template<typename T>
inline auto ::framework::coroutine::shared_task<T>::operator co_await() const noexcept
{
	struct awaitable : ::framework::coroutine::shared_task<T>::awaitable_base
	{
		using ::framework::coroutine::shared_task<T>::awaitable_base::awaitable_base;

		decltype(auto) await_resume()
		{
			if (!this->coroutine_handle)
			{
				throw ::std::logic_error("broken promise");
			}

			return this->coroutine_handle.promise().result();
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline auto ::framework::coroutine::shared_task<T>::when_ready() const noexcept
{
	struct awaitable : ::framework::coroutine::shared_task<T>::awaitable_base
	{
		using ::framework::coroutine::shared_task<T>::awaitable_base::awaitable_base;

		void await_resume() const noexcept
		{
		}
	};

	return awaitable(this->coroutine_handle);
}

template<typename T>
inline void ::framework::coroutine::shared_task<T>::destroy() noexcept
{
	if (this->coroutine_handle)
	{
		if (!this->coroutine_handle.promise().try_detach())
		{
			this->coroutine_handle.destroy();
		}
	}
}

template<typename T>
bool ::framework::coroutine::operator==(::framework::coroutine::shared_task<T> const & lhs, ::framework::coroutine::shared_task<T> const & rhs) noexcept
{
	return lhs.coroutine_handle == rhs.coroutine_handle;
}

template<typename T>
bool ::framework::coroutine::operator!=(::framework::coroutine::shared_task<T> const & lhs, ::framework::coroutine::shared_task<T> const & rhs) noexcept
{
	return !(lhs == rhs);
}

template<typename T>
void ::framework::coroutine::swap(::framework::coroutine::shared_task<T> & a, ::framework::coroutine::shared_task<T> & b) noexcept
{
	a.swap(b);
}

template<typename AWAITABLE, typename AWAIT_RESULT>
auto ::framework::coroutine::make_shared_task(AWAITABLE awaitable) -> ::framework::coroutine::shared_task<::std::conditional_t<::std::is_lvalue_reference_v<AWAIT_RESULT>, AWAIT_RESULT, ::std::remove_reference_t<AWAIT_RESULT>>>
{
	co_return co_await static_cast<AWAITABLE &&>(awaitable);
}