#pragma once

#include "coroutine/async_manual_reset_event.hpp"
#include <atomic>

namespace framework::coroutine
{
	class async_latch
	{
	public:
		async_latch(::std::ptrdiff_t value) noexcept;

		bool is_ready() const noexcept;

		void count_down(::std::ptrdiff_t n) noexcept;

		auto operator co_await() const noexcept;

	private:
		::std::atomic<::std::ptrdiff_t> value;
		::framework::coroutine::async_manual_reset_event event;
	};
}

#include "coroutine/async_latch.inl"