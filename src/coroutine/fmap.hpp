#pragma once

#include "coroutine/type_traits.hpp"
#include <coroutine>
#include <functional>
#include <type_traits>
#include <utility>

namespace framework::coroutine
{
	template<typename FUNCTION, typename AWAITABLE>
	class fmap_awaiter
	{
		using awaiter_t = typename ::framework::coroutine::awaitable_traits<AWAITABLE &&>::awaiter_t;
	public:
		fmap_awaiter(FUNCTION && function, AWAITABLE && awaitable)
			noexcept(
				::std::is_nothrow_move_constructible_v<awaiter_t> &&
				noexcept(::framework::coroutine::get_awaiter(static_cast<AWAITABLE &&>(awaitable)))) :
			function(static_cast<FUNCTION &&>(function)),
			awaiter(::framework::coroutine::get_awaiter(static_cast<AWAITABLE &&>(awaitable)))
		{}

		decltype(auto) await_ready()
			noexcept(noexcept(static_cast<awaiter_t &&>(this->awaiter).await_ready()))
		{
			return static_cast<awaiter_t &&>(this->awaiter).await_ready();
		}

		template<typename PROMISE>
		decltype(auto) await_suspend(::std::coroutine_handle<PROMISE> coro)
			noexcept(noexcept(static_cast<awaiter_t &&>(this->awaiter).await_suspend(::std::move(coro))))
		{
			return static_cast<awaiter_t &&>(this->awaiter).await_suspend(::std::move(coro));
		}

		template<
			typename AWAIT_RESULT = decltype(::std::declval<awaiter_t>().await_resume()),
			::std::enable_if_t<::std::is_void_v<AWAIT_RESULT>, int> = 0>
		decltype(auto) await_resume()
			noexcept(noexcept(::std::invoke(static_cast<FUNCTION &&>(this->function))))
		{
			static_cast<awaiter_t &&>(this->awaiter).await_resume();
			return ::std::invoke(static_cast<FUNCTION &&>(this->function));
		}

		template<
			typename AWAIT_RESULT = decltype(::std::declval<awaiter_t>().await_resume()),
			::std::enable_if_t<!::std::is_void_v<AWAIT_RESULT>, int> = 0>
		decltype(auto) await_resume()
			noexcept(noexcept(::std::invoke(static_cast<FUNCTION &&>(this->function), static_cast<awaiter_t &&>(this->awaiter).await_resume())))
		{
			return ::std::invoke(
				static_cast<FUNCTION &&>(this->function),
				static_cast<awaiter_t &&>(this->awaiter).await_resume());
		}

	private:
		FUNCTION && function;
		awaiter_t awaiter;
	};

	template<typename FUNCTION, typename AWAITABLE>
	class fmap_awaitable
	{
		static_assert(!::std::is_lvalue_reference_v<FUNCTION>);
		static_assert(!::std::is_lvalue_reference_v<AWAITABLE>);
	public:
		template<
			typename FUNC_ARG,
			typename AWAITABLE_ARG,
			::std::enable_if_t<
			::std::is_constructible_v<FUNCTION, FUNC_ARG &&> &&
			::std::is_constructible_v<AWAITABLE, AWAITABLE_ARG &&>, int> = 0>
		explicit fmap_awaitable(FUNC_ARG && function, AWAITABLE_ARG && awaitable)
			noexcept(
				::std::is_nothrow_constructible_v<FUNCTION, FUNC_ARG &&> &&
				::std::is_nothrow_constructible_v<AWAITABLE, AWAITABLE_ARG &&>) :
			function(static_cast<FUNC_ARG &&>(function)),
			awaitable(static_cast<AWAITABLE_ARG &&>(awaitable))
		{}

		auto operator co_await() const &
		{
			return ::framework::coroutine::fmap_awaiter<const FUNCTION &, const AWAITABLE &>(this->function, this->awaitable);
		}

		auto operator co_await() &
		{
			return ::framework::coroutine::fmap_awaiter<FUNCTION &, AWAITABLE &>(this->function, this->awaitable);
		}

		auto operator co_await() &&
		{
			return ::framework::coroutine::fmap_awaiter<FUNCTION &&, AWAITABLE &&>(
				static_cast<FUNCTION &&>(this->function),
				static_cast<AWAITABLE &&>(this->awaitable));
		}

	private:
		FUNCTION function;
		AWAITABLE awaitable;
	};

	template<typename FUNCTION>
	struct fmap_transform
	{
		explicit fmap_transform(FUNCTION && f)
			noexcept(::std::is_nothrow_move_constructible_v<FUNCTION>)
			: function(::std::forward<FUNCTION>(f))
		{}

		FUNCTION function;
	};

	template<
		typename FUNCTION,
		typename AWAITABLE,
		::std::enable_if_t<::framework::coroutine::is_awaitable_v<AWAITABLE>, int> = 0>
	auto fmap(FUNCTION && function, AWAITABLE && awaitable)
	{
		return ::framework::coroutine::fmap_awaitable<
			::std::remove_cv_t<::std::remove_reference_t<FUNCTION>>,
			::std::remove_cv_t<::std::remove_reference_t<AWAITABLE>>>(
			::std::forward<FUNCTION>(function),
			::std::forward<AWAITABLE>(awaitable));
	}

	template<typename FUNCTION>
	auto fmap(FUNCTION && function)
	{
		return ::framework::coroutine::fmap_transform<FUNCTION>{ ::std::forward<FUNCTION>(function) };
	}

	template<typename T, typename FUNCTION>
	decltype(auto) operator|(T && value, ::framework::coroutine::fmap_transform<FUNCTION> && transform)
	{
		// Use ADL for finding fmap() overload.
		return ::framework::coroutine::fmap(::std::forward<FUNCTION>(transform.function), ::std::forward<T>(value));
	}

	template<typename T, typename FUNCTION>
	decltype(auto) operator|(T && value, const ::framework::coroutine::fmap_transform<FUNCTION> & transform)
	{
		// Use ADL for finding fmap() overload.
		return ::framework::coroutine::fmap(transform.function, ::std::forward<T>(value));
	}

	template<typename T, typename FUNCTION>
	decltype(auto) operator|(T && value, ::framework::coroutine::fmap_transform<FUNCTION> & transform)
	{
		// Use ADL for finding fmap() overload.
		return ::framework::coroutine::fmap(transform.function, ::std::forward<T>(value));
	}
}