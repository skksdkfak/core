#include "concurrency/spin_wait.hpp"
#include "coroutine/static_thread_pool.hpp"
#include <algorithm>
#include <cassert>
#include <utility>

#if framework_multiqueue_thread_pool
thread_local ::std::uint32_t(::framework::coroutine::thread::id);
thread_local ::framework::coroutine::thread * ::framework::coroutine::static_thread_pool::current_state = nullptr;
thread_local ::framework::coroutine::static_thread_pool * ::framework::coroutine::static_thread_pool::current_thread_pool = nullptr;

namespace framework::local
{
	namespace
	{
		// Keep each thread's local queue under 1MB
		constexpr ::std::size_t max_local_queue_size = 1024 * 1024 / sizeof(void *);
		constexpr ::std::size_t initial_local_queue_size = 256;
	}
}

::framework::coroutine::thread::thread() :
	worker_thread(::std::move(worker_thread)),
	local_queue(::std::make_unique<::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *>[]>(::framework::local::initial_local_queue_size)),
	mask(::framework::local::initial_local_queue_size - 1),
	head(0),
	tail(0)
{
}

::framework::coroutine::thread::~thread()
{
	if (this->worker_thread.joinable())
	{
		this->worker_thread.join();
	}
}

bool ::framework::coroutine::thread::try_wake_up()
{
	if (this->is_sleeping.exchange(false, ::std::memory_order::release))
	{
		this->is_sleeping.notify_one();
		return true;
	}

	return false;
}

::framework::coroutine::static_thread_pool::schedule_operation * ::framework::coroutine::thread::try_local_pop() noexcept
{
	// Cheap, approximate, no memory-barrier check for emptiness
	::std::size_t head = this->head.load(::std::memory_order::relaxed);
	::std::size_t tail = this->tail.load(::std::memory_order::relaxed);
	if (::framework::coroutine::thread::difference(head, tail) <= 0)
	{
		// Empty
		return nullptr;
	}

	// 3 classes of interleaving of try_local_pop() and try_remote_steal()
	// - local pop completes before remote steal (easy)
	// - remote steal completes before local pop (easy)
	// - both are executed concurrently, both see each other's writes (harder)

	// Speculatively try to acquire the head item of the work queue by
	// decrementing the head cursor. This may race with a concurrent call
	// to try_remote_steal() that is also trying to speculatively increment
	// the tail cursor to steal from the other end of the queue. In the case
	// that they both try to dequeue the last/only item in the queue then we
	// need to fall back to locking to decide who wins

	::std::size_t const new_head = head - 1;
	this->head.store(new_head, ::std::memory_order::seq_cst);

	tail = this->tail.load(::std::memory_order::seq_cst);

	if (::framework::coroutine::thread::difference(new_head, tail) < 0)
	{
		// There was a race to get the last item.
		// We don't know whether the remote steal saw our write
		// and decided to back off or not, so we acquire the mutex
		// so that we wait until the remote steal has completed so
		// we can see what decision it made.
		::std::lock_guard lock(this->remote_mutex);

		// Use relaxed since the lock guarantees visibility of the writes
		// that the remote steal thread performed.
		tail = this->tail.load(::std::memory_order::relaxed);

		if (::framework::coroutine::thread::difference(new_head, tail) < 0)
		{
			// The other thread didn't see our write and stole the last item.
			// We need to restore the head back to it's old value.
			// We hold the mutex so can just use relaxed memory order for this.
			this->head.store(head, ::std::memory_order::relaxed);
			return nullptr;
		}
	}

	// We successfully acquired an item from the queue.
	return this->local_queue[new_head & this->mask].load(::std::memory_order::relaxed);
}

::framework::coroutine::static_thread_pool::schedule_operation * ::framework::coroutine::thread::try_steal(bool * lock_unavailable) noexcept
{
	if (lock_unavailable == nullptr)
	{
		this->remote_mutex.lock();
	}
	else if (!this->remote_mutex.try_lock())
	{
		*lock_unavailable = true;
		return nullptr;
	}

	::std::scoped_lock lock(::std::adopt_lock, this->remote_mutex);

	::std::size_t tail = this->tail.load(::std::memory_order::relaxed);
	::std::size_t head = this->head.load(::std::memory_order::seq_cst);
	if (::framework::coroutine::thread::difference(head, tail) <= 0)
	{
		return nullptr;
	}

	// It looks like there are items in the queue.
	// We'll speculatively try to steal one by incrementing
	// the tail cursor. As this may be running concurrently
	// with try_local_pop() which is also speculatively trying
	// to remove an item from the other end of the queue we
	// need to re-read  the 'head' cursor afterwards to see
	// if there was a potential race to dequeue the last item.
	// Use seq_cst memory order both here and in try_local_pop()
	// to ensure that either we will see their write to head or
	// they will see our write to tail or we will both see each
	// other's writes.
	this->tail.store(tail + 1, ::std::memory_order::seq_cst);
	head = this->head.load(::std::memory_order::seq_cst);

	if (::framework::coroutine::thread::difference(head, tail) > 0)
	{
		// There was still an item in the queue after incrementing tail.
		// We managed to steal an item from the bottom of the stack.
		return this->local_queue[tail & this->mask].load(::std::memory_order::relaxed);
	}
	else
	{
		// Otherwise we failed to steal the last item.
		// Restore the old tail position.
		this->tail.store(tail, ::std::memory_order::seq_cst);
		return nullptr;
	}
}

void ::framework::coroutine::thread::sleep_until_woken() noexcept
{
	this->is_sleeping.wait(true, ::std::memory_order::acquire);
}

// The return value will be true if the notification was successful, i.e.,
// if the thread was not yet marked to be asleep.
bool ::framework::coroutine::thread::notify_intent_to_sleep() noexcept
{
	return !this->is_sleeping.exchange(true, ::std::memory_order::relaxed);
}

bool ::framework::coroutine::thread::approx_has_any_queued_work() const noexcept
{
	return ::framework::coroutine::thread::difference(this->head.load(::std::memory_order::relaxed), this->tail.load(::std::memory_order::relaxed)) > 0;
}

bool ::framework::coroutine::thread::has_any_queued_work() noexcept
{
	::std::scoped_lock lock(this->remote_mutex);
	auto tail = this->tail.load(::std::memory_order::relaxed);
	auto head = this->head.load(::std::memory_order::seq_cst);
	return ::framework::coroutine::thread::difference(head, tail) > 0;
}

bool ::framework::coroutine::thread::try_local_enqueue(::framework::coroutine::static_thread_pool::schedule_operation *& operation) noexcept
{
	// Head is only ever written-to by the current thread so we
	// are safe to use relaxed memory order when reading it.
	auto head = this->head.load(::std::memory_order::relaxed);

	// It is possible this method may be running concurrently with
	// try_remote_steal() which may have just speculatively incremented this->tail
	// trying to steal the last item in the queue but has not yet read the
	// queue item. So we need to make sure we don't write to the last available
	// space (at slot this->tail - 1) as this may still contain a pointer to an
	// operation that has not yet been executed.
	//
	// Note that it's ok to read stale values from this->tail since new values
	// won't ever decrease the number of available slots by more than 1.
	// Reading a stale value can just mean that sometimes the queue appears
	// empty when it may actually have slots free.
	//
	// Here this->mask is equal to buffersize - 1 so we can only write to a slot
	// if the number of items consumed in the queue (head - tail) is less than
	// the mask.
	auto tail = this->tail.load(::std::memory_order::relaxed);
	if (::framework::coroutine::thread::difference(head, tail) < static_cast<::framework::coroutine::thread::offset_t>(this->mask))
	{
		// There is space left in the local buffer.
		this->local_queue[head & this->mask].store(operation, ::std::memory_order::relaxed);
		this->head.store(head + 1, ::std::memory_order::seq_cst);
		return true;
	}

	if (this->mask == local::max_local_queue_size)
	{
		// No space in the buffer and we don't want to grow
		// it any further.
		return false;
	}

	// Allocate the new buffer before taking out the lock so that
	// we ensure we hold the lock for as short a time as possible.
	const ::std::size_t new_size = (this->mask + 1) * 2;

	::std::unique_ptr<::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *>[]> new_local_queue{ new (::std::nothrow) ::std::atomic<::framework::coroutine::static_thread_pool::schedule_operation *>[new_size] };
	if (!new_local_queue)
	{
		// Unable to allocate more memory.
		return false;
	}

	if (!this->remote_mutex.try_lock())
	{
		// Don't wait to acquire the lock if we can't get it immediately.
		// Fail and let it be enqueued to the global queue.
		// TODO: Should we have a per-thread overflow queue instead?
		return false;
	}

	::std::scoped_lock lock(::std::adopt_lock, this->remote_mutex);

	// We can now re-read tail, guaranteed that we are not seeing a stale version.
	tail = this->tail.load(::std::memory_order::relaxed);

	// Copy the existing operations.
	const ::std::size_t new_mask = new_size - 1;
	for (::std::size_t i = tail; i != head; ++i)
	{
		new_local_queue[i & new_mask].store(this->local_queue[i & this->mask].load(::std::memory_order::relaxed), ::std::memory_order::relaxed);
	}

	// Finally, write the new operation to the queue.
	new_local_queue[head & new_mask].store(operation, ::std::memory_order::relaxed);

	this->head.store(head + 1, ::std::memory_order::relaxed);
	this->local_queue = ::std::move(new_local_queue);
	this->mask = new_mask;
	return true;
}

::framework::coroutine::static_thread_pool::static_thread_pool() : static_thread_pool(::std::thread::hardware_concurrency())
{
}

::framework::coroutine::static_thread_pool::static_thread_pool(::std::uint32_t thread_count) :
	thread_count(::std::max<::std::uint32_t>(thread_count, 1)),
	thread_states(::std::make_unique<::framework::coroutine::thread[]>(this->thread_count)),
	stop_requested(false),
	global_queue_head(nullptr),
	global_queue_tail(nullptr),
	sleeping_thread_count(0)
{
	this->threads.reserve(thread_count);
	try
	{
		for (::std::uint32_t i = 0; i < this->thread_count; ++i)
		{
			this->threads.emplace_back(&::framework::coroutine::static_thread_pool::run_worker_thread, this, i);
		}
	}
	catch (...)
	{
		try
		{
			this->shutdown();
		}
		catch (...)
		{
			::std::terminate();
		}

		throw;
	}
}

::framework::coroutine::static_thread_pool::~static_thread_pool()
{
	this->shutdown();
}

void ::framework::coroutine::static_thread_pool::run_worker_thread(::std::uint32_t const thread_index) noexcept
{
	::framework::coroutine::thread::id = thread_index;
	::framework::coroutine::thread & thread = this->thread_states[thread_index];
	::framework::coroutine::static_thread_pool::current_state = &thread;
	::framework::coroutine::static_thread_pool::current_thread_pool = this;

	auto try_get_remote = [&]()
		{
			// Try to get some new work first from the global queue
			// then if that queue is empty then try to steal from
			// the local queues of other worker threads.
			// We try to get new work from the global queue first
			// before stealing as stealing from other threads has
			// the side-effect of those threads running out of work
			// sooner and then having to steal work which increases
			// contention.
			::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation = this->try_global_dequeue();
			if (schedule_operation == nullptr)
			{
				schedule_operation = this->try_steal_from_other_thread(thread_index);
			}
			return schedule_operation;
		};

	while (true)
	{
		// Process operations from the local queue.
		::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation;

		while (true)
		{
			schedule_operation = thread.try_local_pop();
			if (schedule_operation == nullptr)
			{
				schedule_operation = try_get_remote();
				if (schedule_operation == nullptr)
				{
					break;
				}
			}

			schedule_operation->coroutine_handle.resume();
		}

		// No more operations in the local queue or remote queue.
		//
		// We spin for a little while waiting for new items
		// to be enqueued. This avoids the expensive operation
		// of putting the thread to sleep and waking it up again
		// in the case that an external thread is queueing new work

		::framework::concurrency::spin_wait spin_wait;
		while (true)
		{
			for (int i = 0; i < 30; ++i)
			{
				if (this->is_shutdown_requested())
				{
					return;
				}

				spin_wait.spin_one();

				if (this->approx_has_any_queued_work_for(thread_index))
				{
					schedule_operation = try_get_remote();
					if (schedule_operation != nullptr)
					{
						// Now that we've executed some work we can
						// return to normal processing since this work
						// might have queued some more work to the local
						// queue which we should process first.
						goto normal_processing;
					}
				}
			}

			// We didn't find any work after spinning for a while, let's
			// put ourselves to sleep and wait to be woken up.

			// First, let other threads know we're going to sleep.
			this->notify_intent_to_sleep(thread_index);

			// As notifying the other threads that we're sleeping may have
			// raced with other threads enqueueing more work, we need to
			// re-check whether there is any more work to be done so that
			// we don't get into a situation where we go to sleep and another
			// thread has enqueued some work and doesn't know to wake us up.

			if (this->has_any_queued_work_for(thread_index))
			{
				schedule_operation = try_get_remote();
				if (schedule_operation != nullptr)
				{
					// Try to clear the intent to sleep so that some other thread
					// that subsequently enqueues some work won't mistakenly try
					// to wake this threadup when we are already running as there
					// might have been some other thread that it could have woken
					// up instead which could have resulted in increased parallelism.
					//
					// However, it's possible that some other thread may have already
					// tried to wake us up, in which case the auto_reset_event used to
					// wake up this thread may already be in the 'set' state. Leaving
					// it in this state won't really hurt. It'll just mean we might get
					// a spurious wake-up next time we try to go to sleep.
					this->try_clear_intent_to_sleep(thread_index);

					goto normal_processing;
				}
			}

			if (this->is_shutdown_requested())
			{
				return;
			}

			thread.sleep_until_woken();
		}

	normal_processing:
		assert(schedule_operation != nullptr);
		schedule_operation->coroutine_handle.resume();
	}
}

void ::framework::coroutine::static_thread_pool::shutdown()
{
	this->stop_requested.store(true, ::std::memory_order::relaxed);

	for (::std::uint32_t i = 0; i < static_cast<::std::uint32_t>(this->threads.size()); ++i)
	{
		::framework::coroutine::thread & thread_state = this->thread_states[i];

		// We should not be shutting down the thread pool if there is any
		// outstanding work in the queue. It is up to the application to
		// ensure all enqueued work has completed first.
		assert(!thread_state.has_any_queued_work());

		thread_state.try_wake_up();
	}

	for (::std::thread & thread : this->threads)
	{
		thread.join();
	}
}

void ::framework::coroutine::static_thread_pool::schedule(::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation) noexcept
{
	if (::framework::coroutine::static_thread_pool::current_thread_pool != this || !::framework::coroutine::static_thread_pool::current_state->try_local_enqueue(schedule_operation))
	{
		this->remote_enqueue(schedule_operation);
	}

	this->wake_one_thread();
}

void ::framework::coroutine::static_thread_pool::remote_enqueue(::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation) noexcept
{
	auto * tail = this->global_queue_tail.load(::std::memory_order::relaxed);
	do
	{
		schedule_operation->next = tail;
	} while (!this->global_queue_tail.compare_exchange_weak(tail, schedule_operation, ::std::memory_order::seq_cst, ::std::memory_order::relaxed));
}

bool ::framework::coroutine::static_thread_pool::has_any_queued_work_for(::std::uint32_t const thread_index) noexcept
{
	if (this->global_queue_tail.load(::std::memory_order::seq_cst) != nullptr)
	{
		return true;
	}

	if (this->global_queue_head.load(::std::memory_order::seq_cst) != nullptr)
	{
		return true;
	}

	for (::std::uint32_t i = 0; i < this->thread_count; ++i)
	{
		if (i == thread_index)
		{
			continue;
		}
		if (this->thread_states[i].has_any_queued_work())
		{
			return true;
		}
	}

	return false;
}

bool ::framework::coroutine::static_thread_pool::approx_has_any_queued_work_for(::std::uint32_t const thread_index) const noexcept
{
	// Cheap, approximate, read-only implementation that checks whether any work has
	// been queued in the system somewhere. We try to avoid writes here so that we
	// don't bounce cache-lines around between threads/cores unnecessarily when
	// multiple threads are all spinning waiting for work.

	if (this->global_queue_tail.load(::std::memory_order::relaxed) != nullptr)
	{
		return true;
	}

	if (this->global_queue_head.load(::std::memory_order::relaxed) != nullptr)
	{
		return true;
	}

	for (::std::uint32_t i = 0; i < this->thread_count; ++i)
	{
		if (i == thread_index)
		{
			continue;
		}
		if (this->thread_states[i].approx_has_any_queued_work())
		{
			return true;
		}
	}

	return false;
}

bool ::framework::coroutine::static_thread_pool::is_shutdown_requested() const noexcept
{
	return this->stop_requested.load(::std::memory_order::relaxed);
}

void ::framework::coroutine::static_thread_pool::notify_intent_to_sleep(::std::uint32_t const thread_index) noexcept
{
	// First try to mark the thread as asleep
	if (this->thread_states[thread_index].notify_intent_to_sleep())
	{
		// If the thread was not yet marked before to be asleep,
		// then publish the fact that the thread is now asleep by incrementing the count
		// of threads that are asleep.
		[[maybe_unused]] ::std::uint32_t sleeping_thread_count = this->sleeping_thread_count.fetch_add(1, ::std::memory_order::seq_cst);
		assert(sleeping_thread_count < this->thread_count);
	}
}

void ::framework::coroutine::static_thread_pool::try_clear_intent_to_sleep(::std::uint32_t const thread_index) noexcept
{
	// First try to claim that we are waking up one of the threads.
	::std::uint32_t old_sleeping_count = this->sleeping_thread_count.load(::std::memory_order::relaxed);
	do
	{
		if (old_sleeping_count == 0)
		{
			// No more sleeping threads.
			// Someone must have woken us up.
			return;
		}
	} while (!this->sleeping_thread_count.compare_exchange_weak(old_sleeping_count, old_sleeping_count - 1, ::std::memory_order::acquire, ::std::memory_order::relaxed));

	// Then preferentially try to wake up our thread.
	// If some other thread has already requested that this thread wake up
	// then we will wake up another thread - the one that should have been woken
	// up by the thread that woke this thread up.
	if (!this->thread_states[thread_index].try_wake_up())
	{
		for (::std::uint32_t i = 0; i < this->thread_count; ++i)
		{
			if (i == thread_index)
			{
				continue;
			}
			if (this->thread_states[i].try_wake_up())
			{
				return;
			}
		}
	}
}

void ::framework::coroutine::static_thread_pool::wake_one_thread() noexcept
{
	// First try to claim responsibility for waking up one thread.
	// This first read must be seq_cst to ensure that either we have
	// visibility of another thread going to sleep or they have
	// visibility of our prior enqueue of an item.
	::std::uint32_t old_sleeping_thread_count = this->sleeping_thread_count.load(::std::memory_order::seq_cst);
	do
	{
		if (old_sleeping_thread_count == 0)
		{
			// No sleeping threads.
			// Someone must have woken us up.
			return;
		}
	} while (!this->sleeping_thread_count.compare_exchange_weak(old_sleeping_thread_count, old_sleeping_thread_count - 1, ::std::memory_order::acquire, ::std::memory_order::relaxed));

	// Now that we have claimed responsibility for waking a thread up
	// we need to find a sleeping thread and wake it up. We should be
	// guaranteed of finding a thread to wake-up here, but not necessarily
	// in a single pass due to threads potentially waking themselves up
	// in try_clear_intent_to_sleep().
	while (true)
	{
		for (::std::uint32_t i = 0; i < this->thread_count; ++i)
		{
			if (this->thread_states[i].try_wake_up())
			{
				return;
			}
		}
	}
}

::framework::coroutine::static_thread_pool::schedule_operation * (::framework::coroutine::static_thread_pool::try_global_dequeue)() noexcept
{
	::std::scoped_lock lock(this->global_queue_mutex);

	auto * head = this->global_queue_head.load(::std::memory_order::relaxed);
	if (head == nullptr)
	{
		// Use seq-cst memory order so that when we check for an item in the
		// global queue after signalling an intent to sleep that either we
		// will see their enqueue or they will see our signal to sleep and
		// wake us up.
		if (this->global_queue_tail.load(::std::memory_order::seq_cst) == nullptr)
		{
			return nullptr;
		}

		// Acquire the entire set of queued operations in a single operation.
		auto * tail = this->global_queue_tail.exchange(nullptr, ::std::memory_order::acquire);
		if (tail == nullptr)
		{
			return nullptr;
		}

		// Reverse the list 
		do
		{
			auto * next = ::std::exchange(tail->next, head);
			head = ::std::exchange(tail, next);
		} while (tail != nullptr);
	}

	this->global_queue_head = head->next;

	return head;
}

::framework::coroutine::static_thread_pool::schedule_operation * (::framework::coroutine::static_thread_pool::try_steal_from_other_thread)(::std::uint32_t const this_thread_index) noexcept
{
	// Try first with non-blocking steal attempts.

	bool any_locks_unavailable = false;
	for (::std::uint32_t other_thread_index = 0; other_thread_index < this->thread_count; ++other_thread_index)
	{
		if (other_thread_index == this_thread_index)
		{
			continue;
		}
		::framework::coroutine::thread & other_thread_state = this->thread_states[other_thread_index];
		::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation = other_thread_state.try_steal(&any_locks_unavailable);
		if (schedule_operation != nullptr)
		{
			return schedule_operation;
		}
	}

	if (any_locks_unavailable)
	{
		// We didn't check all of the other threads for work to steal yet.
		// Try again, this time waiting to acquire the locks.
		for (::std::uint32_t other_thread_index = 0; other_thread_index < this->thread_count; ++other_thread_index)
		{
			if (other_thread_index == this_thread_index)
			{
				continue;
			}
			::framework::coroutine::thread & other_thread_state = this->thread_states[other_thread_index];
			::framework::coroutine::static_thread_pool::schedule_operation * schedule_operation = other_thread_state.try_steal();
			if (schedule_operation != nullptr)
			{
				return schedule_operation;
			}
		}
	}

	return nullptr;
}
#else
#include "coroutine/static_thread_pool.hpp"

::framework::coroutine::thread::thread(::framework::coroutine::static_thread_pool & static_thread_pool) :
	static_thread_pool(static_thread_pool)
{
	this->worker_thread = ::std::thread(&::framework::coroutine::thread::run_worker_thread, this);
}

::framework::coroutine::thread::~thread()
{
	if (this->worker_thread.joinable())
	{
		this->worker_thread.join();
	}
}

void ::framework::coroutine::thread::run_worker_thread() noexcept
{
	::std::coroutine_handle<> coroutine_handle;
	while (this->static_thread_pool.get_job(coroutine_handle))
	{
		coroutine_handle.resume();
	}
}

::framework::coroutine::static_thread_pool::static_thread_pool() :
	::framework::coroutine::static_thread_pool(::std::thread::hardware_concurrency())
{
}

::framework::coroutine::static_thread_pool::~static_thread_pool()
{
	this->destroying = true;
	this->queue_mutex.lock();
	this->condition.notify_all();
	this->queue_mutex.unlock();
	this->threads.clear();
}

::framework::coroutine::static_thread_pool::static_thread_pool(::std::uint32_t thread_count)
{
	this->threads.reserve(thread_count);
	for (::std::uint32_t i = 0; i < thread_count; i++)
	{
		this->threads.push_back(::std::make_unique<::framework::coroutine::thread>(*this));
	}
}

bool(::framework::coroutine::static_thread_pool::get_job)(::std::coroutine_handle<> & coroutine_handle)
{
	::std::unique_lock<::std::mutex> lock(this->queue_mutex);
	this->condition.wait(lock, [this] { return !this->tasks.empty() || this->destroying; });
	[[unlikely]] if (this->destroying)
	{
		return false;
	}
	coroutine_handle = this->tasks.front();
	this->tasks.pop();
	return true;
}

void ::framework::coroutine::static_thread_pool::schedule(::std::coroutine_handle<> coroutine_handle) const noexcept
{
	::std::unique_lock<::std::mutex> lock(this->queue_mutex);
	this->tasks.push(::std::move(coroutine_handle));
	this->condition.notify_one();
}
#endif