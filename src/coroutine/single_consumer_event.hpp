#pragma once

#include <atomic>
#include <coroutine>

namespace framework::coroutine
{
	/// \brief
	/// A manual-reset event that supports only a single awaiting
	/// coroutine at a time.
	///
	/// You can co_await the event to suspend the current coroutine until
	/// some thread calls set(). If the event is already set then the
	/// coroutine will not be suspended and will continue execution.
	/// If the event was not yet set then the coroutine will be resumed
	/// on the thread that calls set() within the call to set().
	///
	/// Callers must ensure that only one coroutine is executing a
	/// co_await statement at any point in time.
	class single_consumer_event
	{
	public:
		/// \brief
		/// Construct a new event, initialising to either 'set' or 'not set' state.
		///
		/// \param initiallySet
		/// If true then initialises the event to the 'set' state.
		/// Otherwise, initialised the event to the 'not set' state.
		single_consumer_event(bool state = false) noexcept;

		/// Query if this event has been set.
		bool is_set() const noexcept;

		/// \brief
		/// Transition this event to the 'set' state if it is not already set.
		///
		/// If there was a coroutine awaiting the event then it will be resumed
		/// inside this call.
		void set();

		/// \brief
		/// Transition this event to the 'non set' state if it was in the set state.
		void reset() noexcept;

		/// \brief
		/// Wait until the event becomes set.
		///
		/// If the event is already set then the awaiting coroutine will not be suspended
		/// and will continue execution. If the event was not yet set then the coroutine
		/// will be suspended and will be later resumed inside a subsequent call to set()
		/// on the thread that calls set().
		auto operator co_await() noexcept;

	private:
		class awaiter
		{
		public:
			awaiter(::framework::coroutine::single_consumer_event & event);

			bool await_ready() const noexcept;

			bool await_suspend(::std::coroutine_handle<> awaiter);

			void await_resume() noexcept;

		private:
			friend class ::framework::coroutine::single_consumer_event;
			::framework::coroutine::single_consumer_event & single_consumer_event;
			::std::coroutine_handle<> coroutine_handle;
		};

		// Value is either
		// - nullptr - not_set
		// - this    - set
		// - other   - pointer to awaiting ::framework::coroutine::single_consumer_event::awaiter.
		::std::atomic<void *> state;
	};
}

#include "coroutine/single_consumer_event.inl"