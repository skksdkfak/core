#pragma once

#include <atomic>
#include <coroutine>
#include <cstdint>

namespace framework::coroutine
{
	class async_manual_reset_event
	{
		class awaiter
		{
		public:
			awaiter(::framework::coroutine::async_manual_reset_event const * event) noexcept;

			bool await_ready() noexcept;

			bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			void await_resume() noexcept;

		private:
			friend class ::framework::coroutine::async_manual_reset_event;

			::framework::coroutine::async_manual_reset_event const * event;
			::framework::coroutine::async_manual_reset_event::awaiter * next;
			::std::coroutine_handle<> coroutine_handle;
		};

	public:
		async_manual_reset_event() noexcept;

		async_manual_reset_event(bool state) noexcept;

		~async_manual_reset_event() noexcept;

		::framework::coroutine::async_manual_reset_event::awaiter operator co_await() const noexcept;

		bool is_set() const noexcept;

		void set() noexcept;

		void reset() noexcept;

	private:
		mutable ::std::atomic<void *> state;
	};
}

#include "coroutine/async_manual_reset_event.inl"