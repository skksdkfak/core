#include <cassert>

template<typename T>
inline ::framework::coroutine::sync_wait_task<T>::sync_wait_task(::std::coroutine_handle<::framework::coroutine::sync_wait_task<T>::promise_type> coroutine_handle) noexcept :
	coroutine_handle(coroutine_handle)
{
}

template<typename T>
inline void ::framework::coroutine::sync_wait_task<T>::wait()
{
	this->coroutine_handle.promise().wait();
}

template<typename T>
inline decltype(auto) ::framework::coroutine::sync_wait_task<T>::get_result()
{
	return this->coroutine_handle.promise().get_result();
}

inline auto ::framework::coroutine::sync_wait_task<void>::promise_type::initial_suspend() noexcept
{
	return ::std::suspend_never();
}

inline auto ::framework::coroutine::sync_wait_task<void>::promise_type::final_suspend() noexcept
{
	struct final_suspend
	{
		final_suspend(::framework::coroutine::sync_wait_task<void>::promise_type * promise_type) noexcept :
			promise_type(promise_type)
		{
		}

		bool await_ready() noexcept
		{
			return false;
		}

		void await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
		{
			this->promise_type->set();
		}

		void await_resume() noexcept
		{
		}

		::framework::coroutine::sync_wait_task<void>::promise_type * promise_type;
	};

	return final_suspend(this);
}

inline void ::framework::coroutine::sync_wait_task<void>::promise_type::return_void() noexcept
{
}

inline void ::framework::coroutine::sync_wait_task<void>::promise_type::unhandled_exception() noexcept
{
	this->exception = ::std::current_exception();
}

inline void ::framework::coroutine::sync_wait_task<void>::promise_type::get_result()
{
	if (this->exception)
	{
		::std::rethrow_exception(this->exception);
	}
}

inline auto ::framework::coroutine::sync_wait_task<void>::promise_type::get_return_object()
{
	return ::std::coroutine_handle<::framework::coroutine::sync_wait_task<void>::promise_type>::from_promise(*this);
}

inline void ::framework::coroutine::sync_wait_task<void>::promise_type::set()
{
	::std::unique_lock unique_lock(this->mutex);
	this->state = true;
	this->condition_variable.notify_all();
}

inline void ::framework::coroutine::sync_wait_task<void>::promise_type::wait()
{
	::std::unique_lock unique_lock(this->mutex);
	this->condition_variable.wait(unique_lock, [this] { return this->state; });
}

template<typename RESULT>
inline auto ::framework::coroutine::sync_wait_task<RESULT>::promise_type::get_return_object() noexcept
{
	return ::std::coroutine_handle<::framework::coroutine::sync_wait_task<RESULT>::promise_type>::from_promise(*this);
}

template<typename RESULT>
inline auto ::framework::coroutine::sync_wait_task<RESULT>::promise_type::initial_suspend() noexcept
{
	return ::std::suspend_never();
}

template<typename RESULT>
inline auto ::framework::coroutine::sync_wait_task<RESULT>::promise_type::final_suspend() noexcept
{
	struct final_suspend
	{
		final_suspend(::framework::coroutine::sync_wait_task<RESULT>::promise_type * promise_type) noexcept :
			promise_type(promise_type)
		{
		}

		bool await_ready() noexcept
		{
			return false;
		}

		void await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept
		{
			this->promise_type->set();
		}

		void await_resume() noexcept
		{
		}

		typename ::framework::coroutine::sync_wait_task<RESULT>::promise_type * promise_type;
	};

	return final_suspend(this);
}

template<typename RESULT>
inline auto ::framework::coroutine::sync_wait_task<RESULT>::promise_type::yield_value(RESULT && result) noexcept
{
	this->result = ::std::addressof(result);
	return this->final_suspend();
}

template<typename RESULT>
inline void ::framework::coroutine::sync_wait_task<RESULT>::promise_type::return_void() noexcept
{
	// The coroutine should have either yielded a value or thrown
	// an exception in which case it should have bypassed return_void().
	assert(false);
}

template<typename RESULT>
inline void ::framework::coroutine::sync_wait_task<RESULT>::promise_type::unhandled_exception() noexcept
{
	this->exception = ::std::current_exception();
}

template<typename RESULT>
inline RESULT && ::framework::coroutine::sync_wait_task<RESULT>::promise_type::get_result()
{
	if (this->exception)
	{
		::std::rethrow_exception(this->exception);
	}

	return static_cast<RESULT &&>(*this->result);
}

template<typename RESULT>
inline void ::framework::coroutine::sync_wait_task<RESULT>::promise_type::set()
{
	::std::unique_lock unique_lock(this->mutex);
	this->state = true;
	this->condition_variable.notify_all();
}

template<typename RESULT>
inline void ::framework::coroutine::sync_wait_task<RESULT>::promise_type::wait()
{
	::std::unique_lock unique_lock(this->mutex);
	this->condition_variable.wait(unique_lock, [this] { return this->state; });
}

template<typename AWAITABLE>
inline auto ::framework::coroutine::sync_wait(AWAITABLE && awaitable) -> typename ::framework::coroutine::awaitable_traits<AWAITABLE &&>::await_result_t
{
	auto sync_wait_task = ::framework::coroutine::make_sync_wait_task(::std::forward<AWAITABLE>(awaitable));
	sync_wait_task.wait();
	return sync_wait_task.get_result();
}