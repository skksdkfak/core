#pragma once

#include "coroutine/type_traits.hpp"
#include <condition_variable>
#include <coroutine>
#include <exception>
#include <memory>
#include <mutex>

namespace framework::coroutine
{
	template<typename T>
	struct sync_wait_task
	{
		struct promise_type;

		sync_wait_task(::std::coroutine_handle<::framework::coroutine::sync_wait_task<T>::promise_type> coroutine_handle) noexcept;

		void wait();

		decltype(auto) get_result();

		::std::coroutine_handle<::framework::coroutine::sync_wait_task<T>::promise_type> coroutine_handle;
	};

	template<typename RESULT>
	struct sync_wait_task<RESULT>::promise_type
	{
		auto get_return_object() noexcept;

		auto initial_suspend() noexcept;

		auto final_suspend() noexcept;

		auto yield_value(RESULT && result) noexcept;

		void return_void() noexcept;

		void unhandled_exception() noexcept;

		RESULT && get_result();

		void set();

		void wait();

	private:
		::std::condition_variable condition_variable;
		::std::mutex mutex;
		bool state = false;
		::std::remove_reference_t<RESULT> * result;
		::std::exception_ptr exception;
	};
	
	template<>
	struct sync_wait_task<void>::promise_type
	{
		auto initial_suspend() noexcept;

		auto final_suspend() noexcept;

		void return_void() noexcept;

		void unhandled_exception() noexcept;

		void get_result();

		auto get_return_object();

		void set();

		void wait();

	private:
		::std::condition_variable condition_variable;
		::std::mutex mutex;
		bool state = false;
		::std::exception_ptr exception;
	};

	template<typename AWAITABLE, typename RESULT = typename ::framework::coroutine::awaitable_traits<AWAITABLE &&>::await_result_t, ::std::enable_if_t<!::std::is_void_v<RESULT>, int> = 0>
	::framework::coroutine::sync_wait_task<RESULT> make_sync_wait_task(AWAITABLE && awaitable)
	{
		co_yield co_await ::std::forward<AWAITABLE>(awaitable);
	}

	template<typename AWAITABLE, typename RESULT = typename ::framework::coroutine::awaitable_traits<AWAITABLE &&>::await_result_t, ::std::enable_if_t<::std::is_void_v<RESULT>, int> = 0>
	::framework::coroutine::sync_wait_task<void> make_sync_wait_task(AWAITABLE && awaitable)
	{
		co_await ::std::forward<AWAITABLE>(awaitable);
	}

	template<typename AWAITABLE>
	auto sync_wait(AWAITABLE && awaitable) -> typename ::framework::coroutine::awaitable_traits<AWAITABLE &&>::await_result_t;
}

#include "coroutine/sync_wait.inl"