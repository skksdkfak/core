#pragma once

#include "coroutine/type_traits.hpp"
#include <atomic>
#include <coroutine>
#include <cstddef>
#include <exception>
#include <type_traits>

namespace framework::coroutine
{
	template<typename T>
	class shared_task;

	struct shared_task_waiter
	{
		::std::coroutine_handle<> continuation;
		::framework::coroutine::shared_task_waiter * next;
	};

	class shared_task_promise_base
	{
		friend struct final_awaiter;

		struct final_awaiter
		{
			bool await_ready() const noexcept;

			template<typename PROMISE>
			void await_suspend(::std::coroutine_handle<PROMISE> coroutine_handle) noexcept;

			void await_resume() noexcept;
		};

	public:
		shared_task_promise_base() noexcept;

		::std::suspend_always initial_suspend() noexcept;

		::framework::coroutine::shared_task_promise_base::final_awaiter final_suspend() noexcept;

		void unhandled_exception() noexcept;

		bool is_ready() const noexcept;

		void add_ref() noexcept;

		/// Decrement the reference count.
		///
		/// \return
		/// true if successfully detached, false if this was the last
		/// reference to the coroutine, in which case the caller must
		/// call destroy() on the coroutine handle.
		bool try_detach() noexcept;

		/// Try to enqueue a waiter to the list of waiters.
		///
		/// \param waiter
		/// Pointer to the state from the waiter object.
		/// Must have waiter->coroutine_handle member populated with the coroutine
		/// handle of the awaiting coroutine.
		///
		/// \param coroutine
		/// Coroutine handle for this promise object.
		///
		/// \return
		/// true if the waiter was successfully queued, in which case
		/// waiter->coroutine_handle will be resumed when the task completes.
		/// false if the coroutine was already completed and the awaiting
		/// coroutine can continue without suspending.
		bool try_await(::framework::coroutine::shared_task_waiter * waiter, ::std::coroutine_handle<> coroutine);

	protected:
		bool completed_with_unhandled_exception();

		void rethrow_if_unhandled_exception();

	private:
		::std::atomic<::std::uint32_t> ref_count;
		// Value is either
		// - nullptr        - indicates started, no waiters
		// - this           - indicates value is ready
		// - &this->waiters - indicates coroutine not started
		// - other          - pointer to head item in linked-list of waiters.
		//                    values are of type '::framework::coroutine::shared_task_waiter'.
		//                    indicates that the coroutine has been started.
		::std::atomic<void *> waiters;
		::std::exception_ptr exception;
	};

	template<typename T>
	class shared_task_promise : public ::framework::coroutine::shared_task_promise_base
	{
	public:
		shared_task_promise() noexcept = default;

		~shared_task_promise();

		::framework::coroutine::shared_task<T> get_return_object() noexcept;

		template<typename VALUE, typename = ::std::enable_if_t<::std::is_convertible_v<VALUE &&, T>>>
		void return_value(VALUE && value) noexcept(::std::is_nothrow_constructible_v<T, VALUE &&>);

		T & result();

	private:
		alignas(T) ::std::byte value_storage[sizeof(T)];
	};

	template<>
	class shared_task_promise<void> : public ::framework::coroutine::shared_task_promise_base
	{
	public:
		shared_task_promise() noexcept = default;

		::framework::coroutine::shared_task<void> get_return_object() noexcept;

		void return_void() noexcept;

		void result();
	};

	template<typename T>
	class shared_task_promise<T &> : public ::framework::coroutine::shared_task_promise_base
	{
	public:
		shared_task_promise() noexcept = default;

		::framework::coroutine::shared_task<T &> get_return_object() noexcept;

		void return_value(T & value) noexcept;

		T & result();

	private:
		T * value;
	};

	template<typename T = void>
	class [[nodiscard]] shared_task
	{
	public:
		using promise_type = ::framework::coroutine::shared_task_promise<T>;
		using value_type = T;

	private:
		struct awaitable_base
		{
			awaitable_base(::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine) noexcept;

			bool await_ready() const noexcept;

			bool await_suspend(::std::coroutine_handle<> awaiter) noexcept;

			::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine_handle;
			::framework::coroutine::shared_task_waiter waiter;
		};

	public:
		shared_task() noexcept;

		explicit shared_task(::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine_handle);

		shared_task(::framework::coroutine::shared_task<T> && other) noexcept;

		shared_task(::framework::coroutine::shared_task<T> const & other) noexcept;

		~shared_task();

		::framework::coroutine::shared_task<T> & operator=(::framework::coroutine::shared_task<T> && other) noexcept;

		::framework::coroutine::shared_task<T> & operator=(::framework::coroutine::shared_task<T> const & other) noexcept;

		constexpr explicit operator bool() const noexcept;

		void swap(::framework::coroutine::shared_task<T> & other) noexcept;

		/// \brief
		/// Query if the task result is complete.
		///
		/// Awaiting a task that is ready will not block.
		bool is_ready() const noexcept;

		auto operator co_await() const noexcept;

		/// \brief
		/// Returns an awaitable that will await completion of the task without
		/// attempting to retrieve the result.
		auto when_ready() const noexcept;

	private:
		template<typename U>
		friend bool operator==(::framework::coroutine::shared_task<U> const &, ::framework::coroutine::shared_task<U> const &) noexcept;

		void destroy() noexcept;

		::std::coroutine_handle<::framework::coroutine::shared_task<T>::promise_type> coroutine_handle;
	};

	template<typename T>
	bool operator==(::framework::coroutine::shared_task<T> const & lhs, ::framework::coroutine::shared_task<T> const & rhs) noexcept;

	template<typename T>
	bool operator!=(::framework::coroutine::shared_task<T> const & lhs, ::framework::coroutine::shared_task<T> const & rhs) noexcept;

	template<typename T>
	void swap(::framework::coroutine::shared_task<T> & a, ::framework::coroutine::shared_task<T> & b) noexcept;

	template<typename AWAITABLE, typename AWAIT_RESULT = typename ::framework::coroutine::awaitable_traits<AWAITABLE>::await_result_t>
	auto make_shared_task(AWAITABLE awaitable) -> ::framework::coroutine::shared_task<::std::conditional_t<::std::is_lvalue_reference_v<AWAIT_RESULT>, AWAIT_RESULT, ::std::remove_reference_t<AWAIT_RESULT>>>;
}

#include "coroutine/shared_task.inl"