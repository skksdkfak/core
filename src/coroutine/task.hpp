#pragma once

#include <atomic>
#include <coroutine>
#include <exception>
#include <stdexcept>

namespace framework::coroutine
{
	template<typename T>
	class lazy_task final
	{
	public:
		class promise_type;

		using value_type = T;

		lazy_task() noexcept;

		explicit lazy_task(::std::coroutine_handle<typename ::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle) noexcept;

		lazy_task(::framework::coroutine::lazy_task<T> && other) noexcept;

		lazy_task(::framework::coroutine::lazy_task<T> const &) = delete;

		~lazy_task();

		::framework::coroutine::lazy_task<T> & operator=(::framework::coroutine::lazy_task<T> const &) = delete;

		::framework::coroutine::lazy_task<T> & operator=(::framework::coroutine::lazy_task<T> && other) noexcept;

		auto operator co_await() const & noexcept;

		auto operator co_await() const && noexcept;

		bool done() const noexcept;

		decltype(auto) result();

	private:
		struct awaitable_base
		{
			explicit awaitable_base(::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle);

			bool await_ready() noexcept;

			struct ::std::coroutine_handle<> await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle;
		};

		::std::coroutine_handle<::framework::coroutine::lazy_task<T>::promise_type> coroutine_handle;
	};

	template<typename T>
	class immediate_task final
	{
	public:
		class promise_type;

		using value_type = T;

		immediate_task() noexcept;

		explicit immediate_task(::std::coroutine_handle<typename ::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle) noexcept;

		immediate_task(::framework::coroutine::immediate_task<T> && other) noexcept;

		immediate_task(::framework::coroutine::immediate_task<T> const &) = delete;

		~immediate_task();

		::framework::coroutine::immediate_task<T> & operator=(::framework::coroutine::immediate_task<T> const &) = delete;

		::framework::coroutine::immediate_task<T> & operator=(::framework::coroutine::immediate_task<T> && other) noexcept;

		auto operator co_await() const & noexcept;

		auto operator co_await() const && noexcept;

		bool done() const noexcept;

		decltype(auto) result();

	private:
		struct awaitable_base
		{
			explicit awaitable_base(::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle);

			bool await_ready() noexcept;

			bool await_suspend(::std::coroutine_handle<> coroutine_handle) noexcept;

			::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle;
		};

		::std::coroutine_handle<::framework::coroutine::immediate_task<T>::promise_type> coroutine_handle;
	};

	class self_destructible_coroutine final
	{
	public:
		class promise_type
		{
		public:
			auto get_return_object() const noexcept;

			void unhandled_exception() const noexcept;

			void return_void() const noexcept;

			auto initial_suspend() const noexcept;

			auto final_suspend() const noexcept;
		};

		self_destructible_coroutine() noexcept = default;

		self_destructible_coroutine(::framework::coroutine::self_destructible_coroutine const &) = delete;

		::framework::coroutine::self_destructible_coroutine & operator=(::framework::coroutine::self_destructible_coroutine const &) = delete;
	};

	template<typename T>
	class promise
	{
	public:
		promise() noexcept;

		~promise();

		void unhandled_exception() noexcept;

		template<typename VALUE, typename = ::std::enable_if_t<::std::is_convertible_v<VALUE &&, T>>>
		void return_value(VALUE && value) noexcept(::std::is_nothrow_constructible_v<T, VALUE &&>);

		T & result() &;

		T && result() &&;

	private:
		enum class result_type { empty, value, exception };
		::framework::coroutine::promise<T>::result_type result_type = ::framework::coroutine::promise<T>::result_type::empty;
		union
		{
			T value;
			::std::exception_ptr exception;
		};
	};

	template<typename T>
	class promise<T &>
	{
	public:
		promise() noexcept = default;

		void unhandled_exception() noexcept;

		void return_value(T & value) noexcept;

		T & result();

	private:
		T * value = nullptr;
		::std::exception_ptr exception;
	};

	template<>
	class promise<void>
	{
	public:
		promise() noexcept = default;

		void unhandled_exception() noexcept;

		void return_void() noexcept;

		void result();

	private:
		::std::exception_ptr exception;
	};

	template<typename T>
	class lazy_task<T>::promise_type : public ::framework::coroutine::promise<T>
	{
	public:
		auto get_return_object() noexcept;

		auto initial_suspend() const;

		auto final_suspend() const noexcept;

		void set_continuation(::std::coroutine_handle<> continuation) noexcept;

	protected:
		::std::coroutine_handle<> continuation;
	};
	
	template<typename T>
	class immediate_task<T>::promise_type : public ::framework::coroutine::promise<T>
	{
	public:
		auto get_return_object() noexcept;

		auto initial_suspend() const;

		auto final_suspend() const noexcept;

		bool set_continuation(::std::coroutine_handle<> continuation) noexcept;

	protected:
		::std::atomic_bool has_continuation = false;
		::std::coroutine_handle<> continuation;
	};
}

#include "coroutine/task.inl"