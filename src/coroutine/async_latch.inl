inline framework::coroutine::async_latch::async_latch(::std::ptrdiff_t value) noexcept :
	value(value),
	event(value <= 0)
{
}

inline bool ::framework::coroutine::async_latch::is_ready() const noexcept
{
	return this->event.is_set();
}

inline void ::framework::coroutine::async_latch::count_down(::std::ptrdiff_t n) noexcept
{
	if (this->value.fetch_sub(n, ::std::memory_order::acq_rel) <= n)
	{
		this->event.set();
	}
}

inline auto ::framework::coroutine::async_latch::operator co_await() const noexcept
{
	return this->event.operator co_await();
}