template<typename T>
inline constexpr T(::framework::numeric::div_round_up)(T value, T divisor)
{
	return (value + divisor - 1) / divisor;
}

template <typename T>
inline constexpr T (::framework::numeric::previous_multiple)(T val, T divisor)
{
	return (val / divisor) * divisor;
}

template<typename T>
inline constexpr T(::framework::numeric::next_multiple)(T value, T divisor)
{
	return ::framework::numeric::div_round_up(value, divisor) * divisor;
}

template<typename T>
constexpr T (::framework::numeric::align_up)(T value, T alignment)
{
	return (value + alignment - 1) & ~(alignment - 1);
}

template<typename T>
inline constexpr T (::framework::numeric::powi)(T base, T exponent)
{
	T result = 1;
	for (T i = 0; i < exponent; ++i)
	{
		result *= base;
	}

	return result;
}