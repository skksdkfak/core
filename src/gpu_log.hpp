#pragma once

#include "gpu/core.hpp"
#include <string>
#include <vector>

namespace framework
{
	struct gpu_log_create_info
	{
		::framework::gpu::device * device;
		::framework::gpu::physical_device_features const * physical_device_features;
		::framework::gpu::physical_device_properties const * physical_device_properties;
		::framework::gpu::physical_device_memory_properties const * physical_device_memory_properties;
		::framework::gpu::device_size gpu_log_buffer_size;
		::std::uint32_t gpu_log_buffer_initial_queue_family_index;
		::std::uint32_t frame_in_flight_count;
	};

	class gpu_log
	{
	public:
		gpu_log(::framework::gpu_log_create_info const & create_info);

		~gpu_log();

		::framework::gpu::result create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags descriptor_set_layout_create_flags, ::std::uint32_t hlsl_register_space, ::framework::gpu::shader_stage_flags stage_flags, ::framework::gpu::descriptor_set_layout *& descriptor_set_layout);

		void write_descriptor_set(::framework::gpu::descriptor_set * descriptor_set);

		void copy_descriptor_set(::framework::gpu::descriptor_set * src_descriptor_set, ::framework::gpu::descriptor_set * dst_descriptor_set);

		::framework::gpu::device_or_host_descriptor_handle get_gpu_log_counter_descriptor_handle(bool device_descriptor_handle, ::framework::gpu::descriptor_set * descriptor_set);

		void reset(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t frame_in_flight_index, ::framework::gpu::device_descriptor_handle gpu_log_counter_descriptor_handle, ::framework::gpu::host_descriptor_handle gpu_log_counter_host_descriptor_handle);

		void copy_gpu_log(::framework::gpu::command_buffer * command_buffer);

		void print_gpu_log(::std::uint32_t frame_in_flight_index);

	private:
		::framework::gpu::device * device;
		::framework::gpu::device_size gpu_log_buffer_size;
		::framework::gpu::buffer * gpu_log_buffer;
		::framework::gpu::device_memory * gpu_log_device_memory;
		::framework::gpu::buffer * gpu_log_readback_buffer;
		::framework::gpu::device_memory * gpu_log_readback_device_memory;
		void * gpu_log_mapped_data;
		::std::uint32_t frame_in_flight_index;
		::std::uint32_t frame_in_flight_count;
	};
}