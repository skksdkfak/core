#include "gpu_log.hpp"
#include "gpu/utility.hpp"
#include "numeric.hpp"
#include <iostream>

namespace framework::local
{
	namespace
	{
		static ::std::uint32_t constexpr log_max_size = 1024u;
	}
}

::framework::gpu_log::gpu_log(::framework::gpu_log_create_info const & create_info) :
	device(create_info.device),
	gpu_log_buffer_size(::framework::numeric::align_up<::framework::gpu::device_size>(create_info.gpu_log_buffer_size, create_info.physical_device_properties->limits.min_storage_buffer_offset_alignment)),
	frame_in_flight_count(create_info.frame_in_flight_count)
{
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->gpu_log_buffer_size + sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_src_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = create_info.gpu_log_buffer_initial_queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->gpu_log_buffer));

		::framework::gpu::debug_utils_object_name_info debug_utils_object_name_info;
		debug_utils_object_name_info.object_type = ::framework::gpu::object_type::buffer;
		debug_utils_object_name_info.object = this->gpu_log_buffer;
		debug_utils_object_name_info.object_name = "::framework::gpu_log::gpu_log_buffer";
		assert_framework_gpu_result(this->device->set_object_name(&debug_utils_object_name_info));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gpu_log_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(*create_info.physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->gpu_log_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->gpu_log_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gpu_log_buffer;
		bind_buffer_memory_info.memory = this->gpu_log_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = (this->gpu_log_buffer_size + sizeof(::std::uint32_t)) * 3;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::host_read_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = create_info.gpu_log_buffer_initial_queue_family_index;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->create_buffer(&buffer_create_info, nullptr, &this->gpu_log_readback_buffer));

		::framework::gpu::memory_requirements memory_requirements;

		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gpu_log_readback_buffer;
		this->device->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(*create_info.physical_device_memory_properties, memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::host_visible_read_bit | ::framework::gpu::memory_property_flags::host_coherent_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->gpu_log_readback_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(this->device->allocate_memory(&memory_allocate_info, nullptr, &this->gpu_log_readback_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gpu_log_readback_buffer;
		bind_buffer_memory_info.memory = this->gpu_log_readback_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(this->device->bind_buffer_memory(1, &bind_buffer_memory_info));

		assert_framework_gpu_result(this->device->map_memory(this->gpu_log_readback_device_memory, 0, buffer_create_info.size, ::framework::gpu::memory_map_flags::none, &this->gpu_log_mapped_data));
	}
}

::framework::gpu_log::~gpu_log()
{
}

::framework::gpu::result(::framework::gpu_log::create_descriptor_set_layout)(::framework::gpu::descriptor_set_layout_create_flags descriptor_set_layout_create_flags, ::std::uint32_t hlsl_register_space, ::framework::gpu::shader_stage_flags stage_flags, ::framework::gpu::descriptor_set_layout *& descriptor_set_layout)
{
	::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
	// gpu_log
	descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[0].binding = 0;
	descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
	descriptor_set_layout_bindings[0].hlsl_register_space = hlsl_register_space;
	descriptor_set_layout_bindings[0].descriptor_count = 1;
	descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_set_layout_bindings[0].stage_flags = stage_flags;
	descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
	// gpu_log_counter
	descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
	descriptor_set_layout_bindings[1].binding = 1;
	descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
	descriptor_set_layout_bindings[1].hlsl_register_space = hlsl_register_space;
	descriptor_set_layout_bindings[1].descriptor_count = 1;
	descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	descriptor_set_layout_bindings[1].stage_flags = stage_flags;
	descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

	::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
	descriptor_set_layout_create_info.flags = descriptor_set_layout_create_flags;
	descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
	descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
	return this->device->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &descriptor_set_layout);
}

void(::framework::gpu_log::write_descriptor_set)(::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	descriptor_buffer_infos[0].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
	descriptor_buffer_infos[0].buffer = this->gpu_log_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = this->gpu_log_buffer_size;
	descriptor_buffer_infos[0].stride = sizeof(::std::uint32_t);

	descriptor_buffer_infos[1].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
	descriptor_buffer_infos[1].buffer = this->gpu_log_buffer;
	descriptor_buffer_infos[1].offset = this->gpu_log_buffer_size;
	descriptor_buffer_infos[1].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[1].stride = sizeof(::std::uint32_t);

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;

	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	this->device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

void(::framework::gpu_log::copy_descriptor_set)(::framework::gpu::descriptor_set * src_descriptor_set, ::framework::gpu::descriptor_set * dst_descriptor_set)
{
	::framework::gpu::copy_descriptor_set copy_descriptor_sets[2];
	copy_descriptor_sets[0].src_set = src_descriptor_set;
	copy_descriptor_sets[0].src_binding = 0;
	copy_descriptor_sets[0].src_array_element = 0;
	copy_descriptor_sets[0].dst_set = dst_descriptor_set;
	copy_descriptor_sets[0].dst_binding = 0;
	copy_descriptor_sets[0].dst_array_element = 0;
	copy_descriptor_sets[0].descriptor_count = 1;
	copy_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;

	copy_descriptor_sets[1].src_set = src_descriptor_set;
	copy_descriptor_sets[1].src_binding = 1;
	copy_descriptor_sets[1].src_array_element = 0;
	copy_descriptor_sets[1].dst_set = dst_descriptor_set;
	copy_descriptor_sets[1].dst_binding = 1;
	copy_descriptor_sets[1].dst_array_element = 0;
	copy_descriptor_sets[1].descriptor_count = 1;
	copy_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;

	this->device->update_descriptor_sets(0, nullptr, static_cast<::std::uint32_t>(::std::size(copy_descriptor_sets)), copy_descriptor_sets);
}

::framework::gpu::device_or_host_descriptor_handle(::framework::gpu_log::get_gpu_log_counter_descriptor_handle)(bool device_descriptor_handle, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_handle_info descriptor_handle_info;
	descriptor_handle_info.flags = ::framework::gpu::descriptor_handle_info_flags::none;
	descriptor_handle_info.device_descriptor_handle = device_descriptor_handle;
	descriptor_handle_info.set = descriptor_set;
	descriptor_handle_info.binding = 1;
	descriptor_handle_info.array_element = 0;
	return this->device->get_descriptor_handle(&descriptor_handle_info);
}

void ::framework::gpu_log::reset(::framework::gpu::command_buffer * command_buffer, ::std::uint32_t frame_in_flight_index, ::framework::gpu::device_descriptor_handle gpu_log_counter_device_descriptor_handle, ::framework::gpu::host_descriptor_handle gpu_log_counter_host_descriptor_handle)
{
	command_buffer->fill_buffer(this->gpu_log_buffer, this->gpu_log_buffer_size, sizeof(::std::uint32_t), 0, 0, gpu_log_counter_device_descriptor_handle, gpu_log_counter_host_descriptor_handle);
	this->frame_in_flight_index = frame_in_flight_index;
}

void ::framework::gpu_log::copy_gpu_log(::framework::gpu::command_buffer * command_buffer)
{
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
		buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
		buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barriers[0].buffer = this->gpu_log_buffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	::framework::gpu::buffer_copy buffer_copy;
	buffer_copy.src_offset = 0;
	buffer_copy.dst_offset = this->frame_in_flight_index * (this->gpu_log_buffer_size + sizeof(::std::uint32_t));
	buffer_copy.size = this->gpu_log_buffer_size + sizeof(::std::uint32_t);
	command_buffer->copy_buffer(this->gpu_log_buffer, this->gpu_log_readback_buffer, 1, &buffer_copy);

	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barriers[1];
		buffer_memory_barriers[0].src_access_mask = ::framework::gpu::access_flags::transfer_read_bit;
		buffer_memory_barriers[0].dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		buffer_memory_barriers[0].src_stage_mask = ::framework::gpu::pipeline_stage_flags::transfer_bit;
		buffer_memory_barriers[0].dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		buffer_memory_barriers[0].old_state = ::framework::gpu::buffer_state_flags::transfer_src_bit;
		buffer_memory_barriers[0].new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barriers[0].src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barriers[0].src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barriers[0].dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barriers[0].dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barriers[0].buffer = this->gpu_log_buffer;
		buffer_memory_barriers[0].offset = 0;
		buffer_memory_barriers[0].size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = static_cast<::std::uint32_t>(::std::size(buffer_memory_barriers));
		dependency_info.buffer_memory_barriers = buffer_memory_barriers;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::gpu_log::print_gpu_log(::std::uint32_t frame_in_flight_index)
{
	::std::uint32_t const gpu_log_buffer_size_in_four_bytes = this->gpu_log_buffer_size / sizeof(::std::uint32_t);
	::std::uint32_t * current_frame_log_ptr_begin = static_cast<::std::uint32_t *>(this->gpu_log_mapped_data) + frame_in_flight_index * (gpu_log_buffer_size_in_four_bytes + 1u);
	::std::uint32_t const log_size = ::std::min(*reinterpret_cast<::std::uint32_t *>(current_frame_log_ptr_begin + gpu_log_buffer_size_in_four_bytes), ::framework::local::log_max_size);
	if (log_size)
	{
		::std::uint32_t * const current_frame_log_ptr_end = current_frame_log_ptr_begin + log_size;
		::std::uint32_t * current_frame_log_ptr = current_frame_log_ptr_begin;
		::std::uint32_t entry_count = 0u;
		while (current_frame_log_ptr < current_frame_log_ptr_end)
		{
			::std::uint32_t const header = *(current_frame_log_ptr++);
			::std::uint32_t const element_count = (header & 0xf) + 1;
			current_frame_log_ptr += element_count;
			entry_count++;
		}
		::std::cout << "[log_entry_count: " << entry_count << "]" << ::std::endl;
		current_frame_log_ptr = current_frame_log_ptr_begin;
		while (current_frame_log_ptr < current_frame_log_ptr_end)
		{
			::std::uint32_t const header = *(current_frame_log_ptr++);
			::std::uint32_t const element_count = (header & 0xf) + 1;
			for (::std::uint32_t i = 0; i < element_count; i++)
			{
				::std::uint32_t * const value = current_frame_log_ptr + i;
				::std::uint32_t const element_type = (header >> (4 + i * 2)) & 0x3;
				switch (element_type)
				{
				case 1:
					::std::cout << *reinterpret_cast<float *>(value);
					break;
				case 2:
					::std::cout << *reinterpret_cast<::std::int32_t *>(value);
					break;
				default:
					::std::cout << *reinterpret_cast<::std::uint32_t *>(value);
					break;
				}

				if (i < element_count - 1)
				{
					::std::cout << ", ";
				}
			}
			::std::cout << ::std::endl;
			current_frame_log_ptr += element_count;
		}
	}
}