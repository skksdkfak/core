#pragma once

#include <cstdint>

namespace framework::linalg
{
	enum class matrix_layout : ::std::uint32_t
	{
		row_major = 0,
		column_major = 1
	};
}