#pragma once

namespace framework::nn
{
	enum class activation
	{
		none,
		relu,
		relu6,
		leaky_relu,
		sigmoid,
		exponential
	};
}