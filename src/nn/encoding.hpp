#pragma once

#include "gpu/core.hpp"
#include <cstdint>
#include <span>

namespace framework::nn
{
	class encoding_execution_policy
	{
	public:
		virtual void initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::span<::std::uint32_t const, 2> dynamic_offsets) = 0;

		virtual void update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::descriptor_set * descriptor_set) = 0;

		virtual ::std::uint32_t get_parameter_count() const noexcept = 0;

		virtual ::framework::gpu::device_size get_dispatch_indirect_command_size() const noexcept = 0;

		virtual ::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept = 0;

		virtual ::framework::gpu::descriptor_set_layout * get_initialize_dispatch_indirect_command_descriptor_set_layout() const noexcept = 0;
	};

	class encoding
	{
	public:
		virtual void forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size) = 0;

		virtual void forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;

		virtual void backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size) = 0;

		virtual void backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;

		virtual void update_descriptor_sets(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * output_buffer, ::framework::gpu::descriptor_set * descriptor_set) = 0;

		virtual ::framework::gpu::buffer * get_full_precision_parameter_buffer() const noexcept = 0;

		virtual ::framework::gpu::buffer * get_parameter_buffer() const noexcept = 0;

		virtual ::framework::gpu::buffer * get_gradient_buffer() const noexcept = 0;
	};
}