static const uint activation_function_none = 0u;
static const uint activation_function_relu = 1u;
static const uint activation_function_relu6 = 2u;
static const uint activation_function_leaky_relu = 3u;
static const uint activation_function_sigmoid = 4u;
static const uint activation_function_exponential = 5u;

activation_input_t relu(activation_input_t x)
{
    return max(x, activation_input_t(0.0));
}

activation_input_t relu6(activation_input_t x)
{
    return clamp(x, activation_input_t(0.0), activation_input_t(6.0));
}

activation_input_t leaky_relu(activation_input_t x)
{
    return x > activation_input_t(0.0) ? x : x * activation_input_t(0.01);
}

activation_input_t logistic(activation_input_t x)
{
    return activation_input_t(1.0f / (1.0f + exp(-x)));
}

gradient_t relu_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0));
}

gradient_t relu6_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0) && x < activation_input_t(6.0));
}

gradient_t leaky_relu_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0) ? 1.0 : 0.01);
}

gradient_t logistic_derivative(activation_input_t x)
{
    return gradient_t(x * activation_input_t(1.0f - float(x)));
}

void activation_forward(inout activation_input_t activation_input, uint activation_function)
{
        switch (activation_function)
        {
        case activation_function_relu:
            activation_input = relu(activation_input);
            break;
        case activation_function_relu6:
            activation_input = relu6(activation_input);
            break;
        case activation_function_leaky_relu:
            activation_input = leaky_relu(activation_input);
            break;
        case activation_function_sigmoid:
            activation_input = logistic(activation_input);
            break;
        case activation_function_exponential:
            activation_input = exp(activation_input);
            break;
        default:
            break;
        }
}

void activation_backward(inout gradient_t gradient, in activation_input_t activation_input, uint activation_function)
{
    switch (activation_function)
    {
    case activation_function_relu:
        gradient *= relu_derivative(activation_input);
        break;
    case activation_function_relu6:
        gradient *= relu6_derivative(activation_input);
        break;
    case activation_function_leaky_relu:
        gradient *= leaky_relu_derivative(activation_input);
        break;
    case activation_function_sigmoid:
        gradient *= logistic_derivative(activation_input);
        break;
    case activation_function_exponential:
        gradient *= activation_input;
        break;
    default:
        break;
    }
}