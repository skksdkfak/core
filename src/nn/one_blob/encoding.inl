inline ::std::uint32_t(::framework::nn::one_blob::execution_policy::get_parameter_count)() const noexcept
{
	return 0u;
}

inline ::framework::gpu::device_size (::framework::nn::one_blob::execution_policy::get_dispatch_indirect_command_size)() const noexcept
{
	return sizeof(::framework::gpu::dispatch_indirect_command);
}

inline ::framework::gpu::descriptor_set_layout * (::framework::nn::one_blob::execution_policy::get_descriptor_set_layout)() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set_layout * (::framework::nn::one_blob::execution_policy::get_initialize_dispatch_indirect_command_descriptor_set_layout)() const noexcept
{
	return this->initialize_dispatch_indirect_command_descriptor_set_layout;
}

inline ::framework::gpu::buffer * (::framework::nn::one_blob::encoding::get_full_precision_parameter_buffer)() const noexcept
{
	return nullptr;
}

inline ::framework::gpu::buffer * (::framework::nn::one_blob::encoding::get_parameter_buffer)() const noexcept
{
	return nullptr;
}

inline ::framework::gpu::buffer * (::framework::nn::one_blob::encoding::get_gradient_buffer)() const noexcept
{
	return nullptr;
}