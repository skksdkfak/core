#include "gpu/gpu.hlsl"
#include "numeric.hlsl"

[[vk::constant_id(0)]] const uint32_t workgroup_size_x = 1;
[[vk::constant_id(1)]] const uint32_t workgroup_size = 1;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u0, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	dispatch_indirect_command_buffer[0] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(size * workgroup_size_x, workgroup_size), 1, 1);
}
