#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "nn/one_blob/encoding.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <cmath>
#include <string>

::framework::nn::one_blob::execution_policy::execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::gpu::component_type parameter_type, ::std::uint32_t bins, ::std::uint32_t dims_to_encode, ::framework::linalg::matrix_layout input_layout, ::framework::linalg::matrix_layout output_layout, ::std::uint32_t input_row_offset, ::std::uint32_t input_row_count, ::std::uint32_t output_row_offset, ::std::uint32_t output_row_count)
{
	::std::uint32_t const output_dims = bins * dims_to_encode;
	this->workgroup_size_x = output_layout == ::framework::linalg::matrix_layout::row_major ? dims_to_encode : output_dims;
	::std::uint32_t const workgroup_size_y = ::framework::numeric::div_round_up(128u, this->workgroup_size_x);
	::std::uint32_t const workgroup_size = this->workgroup_size_x * workgroup_size_y;
	::std::uint32_t const bins_log2_count = static_cast<::std::uint32_t>(::std::log2(bins));
	::std::string const workgroup_size_x_string = ::std::to_string(this->workgroup_size_x);
	::std::string const workgroup_size_y_string = ::std::to_string(workgroup_size_y);
	char const * const forward_entry_point = output_layout == ::framework::linalg::matrix_layout::row_major ? "main_row_major" : "main_column_major";
	char const * const parameter_type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// input_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// output_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer_dynamic;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_descriptor_set_layout));
	}

	{
		{
			::framework::resource::preprocessor_define preprocessor_defines[]
			{
				{ "parameter_t", parameter_type_name },
				{ "workgroup_size_x", workgroup_size_x_string.c_str() },
				{ "workgroup_size_y", workgroup_size_y_string.c_str() }
			};

			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/one_blob/encoding.comp.hlsl";
			hlsl_source_info.source_entry_point = forward_entry_point;

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = forward_entry_point;
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->forward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::preprocessor_define preprocessor_defines[]
			{
				{ "parameter_t", parameter_type_name },
				{ "workgroup_size_x", workgroup_size_x_string.c_str() },
				{ "workgroup_size_y", workgroup_size_y_string.c_str() }
			};

			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/one_blob/encoding.comp.hlsl";
			hlsl_source_info.source_entry_point = forward_entry_point;

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = forward_entry_point;
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->backward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/one_blob/initialize_dispatch_indirect_command.comp.hlsl";
			hlsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = 0;
			shader_stage_info.preprocessor_defines = nullptr;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->initialize_dispatch_indirect_command_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->initialize_dispatch_indirect_command_descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline_layout));
	}

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) },
		{ 2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t) },
		{ 3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t) },
		{ 4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t) },
		{ 5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t) },
		{ 6, sizeof(::std::uint32_t) * 6, sizeof(::std::uint32_t) },
		{ 7, sizeof(::std::uint32_t) * 7, sizeof(::std::uint32_t) }
	};

	::framework::gpu::specialization_map_entry const initialize_dispatch_indirect_command_specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) }
	};

	::std::uint32_t const specialization_data[]
	{
		this->workgroup_size_x,
		workgroup_size_y,
		bins_log2_count,
		static_cast<::std::int32_t>(input_layout),
		input_row_offset,
		input_row_count,
		output_row_offset,
		output_row_count
	};

	::std::uint32_t const initialize_dispatch_indirect_command_specialization_data[]
	{
		this->workgroup_size_x,
		workgroup_size
	};

	::framework::gpu::specialization_info specialization_infos[2];
	specialization_infos[0].map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_infos[0].map_entries = specialization_map_entries;
	specialization_infos[0].data_size = sizeof(specialization_data);
	specialization_infos[0].data = specialization_data;

	specialization_infos[1].map_entry_count = static_cast<::std::uint32_t>(::std::size(initialize_dispatch_indirect_command_specialization_map_entries));
	specialization_infos[1].map_entries = initialize_dispatch_indirect_command_specialization_map_entries;
	specialization_infos[1].data_size = sizeof(initialize_dispatch_indirect_command_specialization_data);
	specialization_infos[1].data = initialize_dispatch_indirect_command_specialization_data;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = this->forward_shader_module;
	pipeline_shader_stage_create_infos[0].name = forward_entry_point;
	pipeline_shader_stage_create_infos[0].specialization_info = &specialization_infos[0];

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = this->backward_shader_module;
	pipeline_shader_stage_create_infos[1].name = forward_entry_point;
	pipeline_shader_stage_create_infos[1].specialization_info = &specialization_infos[0];

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = this->initialize_dispatch_indirect_command_shader_module;
	pipeline_shader_stage_create_infos[2].name = "main";
	pipeline_shader_stage_create_infos[2].specialization_info = &specialization_infos[1];

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[3];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = this->pipeline_layout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = this->pipeline_layout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = this->initialize_dispatch_indirect_command_pipeline_layout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	::framework::gpu::pipeline * pipelines[::std::size(compute_pipeline_create_infos)];
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines));
	this->forward_pipeline = pipelines[0], this->backward_pipeline = pipelines[1], this->initialize_dispatch_indirect_command_pipeline = pipelines[2];
}

void ::framework::nn::one_blob::execution_policy::initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::span<::std::uint32_t const, 2> dynamic_offsets)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &descriptor_set, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), ::std::data(dynamic_offsets));
	command_buffer->dispatch(1, 1, 1);
}

void ::framework::nn::one_blob::execution_policy::update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	descriptor_buffer_infos[0].buffer = size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = dispatch_indirect_command_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = sizeof(::framework::gpu::dispatch_indirect_command);
	descriptor_buffer_infos[1].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	// size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// dispatch_indirect_command_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer_dynamic;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;

	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

::framework::nn::one_blob::encoding::encoding(::framework::nn::encoding_execution_policy const * execution_policy) :
	execution_policy(static_cast<::framework::nn::one_blob::execution_policy const *>(execution_policy))
{
}

void ::framework::nn::one_blob::encoding::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size)
{
	::std::uint32_t const workgroup_size_y = ::framework::numeric::div_round_up(128u, this->execution_policy->workgroup_size_x);
	::std::uint32_t const workgroup_size = this->execution_policy->workgroup_size_x * workgroup_size_y;

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch(::framework::numeric::div_round_up(size * this->execution_policy->workgroup_size_x, workgroup_size), 1, 1);
}

void ::framework::nn::one_blob::encoding::forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
}

void ::framework::nn::one_blob::encoding::backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size)
{
}

void ::framework::nn::one_blob::encoding::backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
}

void ::framework::nn::one_blob::encoding::update_descriptor_sets(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * output_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
	descriptor_buffer_infos[0].buffer = size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = input_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	descriptor_buffer_infos[2].buffer = output_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[3];
	// size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// input_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// output_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;

	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}