#include "linalg.hlsl"

//[[vk::constant_id(0)]] const int workgroup_size_x = 1;
//[[vk::constant_id(1)]] const int workgroup_size_y = 1;
[[vk::constant_id(2)]] const uint32_t num_bins_log2 = 1;
[[vk::constant_id(3)]] const int32_t input_layout = 0;
[[vk::constant_id(4)]] const uint32_t input_row_offset = 0;
[[vk::constant_id(5)]] const uint32_t input_row_count = 1;
[[vk::constant_id(6)]] const uint32_t output_row_offset = 0;
[[vk::constant_id(7)]] const uint32_t output_row_count = 1;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] StructuredBuffer<parameter_t> input_buffer : register(t0, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<parameter_t> output_buffer : register(u0, space0);

inline float quartic_cdf(const float x, const float inv_radius)
{
	const float u = x * inv_radius;
	const float u2 = u * u;
	const float u4 = u2 * u2;
	return clamp(((float)15 / 16) * u * (1 - ((float)2 / 3) * u2 + ((float)1 / 5) * u4) + 0.5f, 0.0f, 1.0f);
}

[numthreads(workgroup_size_x, workgroup_size_y, 1)]
[shader("compute")]
void main_row_major(uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint32_t i = group_id.x * workgroup_size_y + group_thread_id.y;
	const uint32_t j = group_thread_id.x;
	const uint32_t to_encode_index = j + i * workgroup_size_x;
	if (to_encode_index >= size * workgroup_size_x)
	{
		return;
	}

	const uint32_t input_row_index = input_row_offset + j;
	const uint32_t input_column_index = i;
	const uint32_t input_column_count = size;
	const uint32_t input_index = (::framework::linalg::matrix_layout)input_layout == ::framework::linalg::matrix_layout::row_major_ ? input_column_index + input_row_index * input_column_count : input_row_index + input_column_index * input_row_count;
	const float x = input_buffer[input_index];
	const uint32_t n_bins = 1u << num_bins_log2;
	const float bin_size = 1.0f / n_bins;

	float left_cdf = quartic_cdf(-x, n_bins);

	for (uint32_t k = 0; k < n_bins; ++k)
	{
		const float right_boundary = (k + 1) * bin_size;
		const float right_cdf = quartic_cdf(right_boundary - x, n_bins);

		const uint32_t output_row_index = output_row_offset + j * n_bins + k;
		const uint32_t output_column_index = i;
		const uint32_t output_column_count = size;
		output_buffer[output_column_index + output_row_index * output_column_count] = right_cdf - left_cdf;

		left_cdf = right_cdf;
	}
}

[numthreads(workgroup_size_x, workgroup_size_y, 1)]
[shader("compute")]
void main_column_major(uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint32_t i = group_id.x * workgroup_size_y + group_thread_id.y;
	const uint32_t j = group_thread_id.x;
	if (i >= size)
	{
		return;
	}

	const uint32_t n_bins = 1 << num_bins_log2;
	const uint32_t bin_index = j & (n_bins - 1);
	const float bin_size = 1.0f / n_bins;
	const uint32_t input_row_index = input_row_offset + (j >> num_bins_log2);
	const uint32_t input_column_index = i;
	const uint32_t input_column_count = size;
	const uint32_t input_index = (::framework::linalg::matrix_layout)input_layout == ::framework::linalg::matrix_layout::row_major_ ? input_column_index + input_row_index * input_column_count : input_row_index + input_column_index * input_row_count;
	const float x = input_buffer[input_index];

	const float left_boundary = bin_index * bin_size;
	const float left_cdf = quartic_cdf(left_boundary - x, n_bins);

	const float right_boundary = (bin_index + 1) * bin_size;
	const float right_cdf = quartic_cdf(right_boundary - x, n_bins);

	const uint32_t output_row_index = output_row_offset + j;
	const uint32_t output_column_index = i;
	output_buffer[output_row_index + output_column_index * output_row_count] = right_cdf - left_cdf;
}
