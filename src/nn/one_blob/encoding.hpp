#pragma once

#include "command_context.hpp"
#include "gpu/core.hpp"
#include "linalg.hpp"
#include "nn/encoding.hpp"
#include <cstdint>
#include <span>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::nn::one_blob
{
	class encoding;

	class execution_policy : public ::framework::nn::encoding_execution_policy
	{
	public:
		execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::gpu::component_type parameter_type, ::std::uint32_t bins, ::std::uint32_t dims_to_encode, ::framework::linalg::matrix_layout input_layout, ::framework::linalg::matrix_layout output_layout, ::std::uint32_t input_row_offset, ::std::uint32_t input_row_count, ::std::uint32_t output_row_offset, ::std::uint32_t output_row_count);

		void initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::span<::std::uint32_t const, 2> dynamic_offsets) override;

		void update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::descriptor_set * descriptor_set) override;

		::std::uint32_t get_parameter_count() const noexcept override;

		::framework::gpu::device_size get_dispatch_indirect_command_size() const noexcept override;

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept override;

		::framework::gpu::descriptor_set_layout * get_initialize_dispatch_indirect_command_descriptor_set_layout() const noexcept override;

	private:
		friend class ::framework::nn::one_blob::encoding;

		::framework::gpu::shader_module * forward_shader_module;
		::framework::gpu::shader_module * backward_shader_module;
		::framework::gpu::shader_module * initialize_dispatch_indirect_command_shader_module;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * initialize_dispatch_indirect_command_descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_layout * initialize_dispatch_indirect_command_pipeline_layout;
		::framework::gpu::pipeline * forward_pipeline;
		::framework::gpu::pipeline * backward_pipeline;
		::framework::gpu::pipeline * initialize_dispatch_indirect_command_pipeline;
		::std::uint32_t workgroup_size_x;
	};

	class encoding : public ::framework::nn::encoding
	{
	public:
		encoding(::framework::nn::encoding_execution_policy const * execution_policy);

		void forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size) override;

		void forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

		void backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size) override;

		void backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

		void update_descriptor_sets(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * output_buffer, ::framework::gpu::descriptor_set * descriptor_set) override;

		::framework::gpu::buffer * get_full_precision_parameter_buffer() const noexcept override;

		::framework::gpu::buffer * get_parameter_buffer() const noexcept override;

		::framework::gpu::buffer * get_gradient_buffer() const noexcept override;

	private:
		::framework::nn::one_blob::execution_policy const * execution_policy;
	};
}

#include "nn/one_blob/encoding.inl"