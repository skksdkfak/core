#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "nn/fully_fused_mlp/network.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <cassert>

namespace framework::local
{
	namespace
	{
		static char constexpr activation_backward_gemm_epilogue_glsl_path[] = "nn/fully_fused_mlp/activation_backward_gemm_epilogue.glsl";

		struct initialize_dispatch_indirect_command_push_constants
		{
			::std::uint32_t gemm_device_parameters_buffer_alignment;
			::std::uint32_t batch_size_stride;
			::framework::gpu::bool32_t inference;
		};

		constexpr inline ::framework::linalg::matrix_layout transpose(::framework::linalg::matrix_layout matrix_layout)
		{
			return matrix_layout == ::framework::linalg::matrix_layout::row_major ? ::framework::linalg::matrix_layout::column_major : ::framework::linalg::matrix_layout::row_major;
		}
	}
}

::framework::nn::fully_fused_mlp::execution_policy::execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::framework::linalg::matrix_layout input_layout, ::framework::linalg::matrix_layout output_layout, ::std::uint32_t input_width, ::std::uint32_t output_width, ::std::uint32_t padded_input_width, ::std::uint32_t padded_output_width, ::std::uint32_t hidden_width, ::std::uint32_t hidden_layer_count, ::framework::nn::activation hidden_activation, ::framework::nn::activation output_activation, ::std::uint32_t batch_alignment, ::std::uint32_t inference_batch_alignment, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, bool compute_dloss_dinput) :
	hidden_matmul_count(hidden_layer_count - 1),
	input_width(input_width),
	output_width(output_width),
	padded_input_width(padded_input_width),
	padded_output_width(padded_output_width),
	hidden_width(hidden_width),
	output_activation(output_activation),
	parameter_count(padded_input_width * hidden_width + hidden_width * hidden_width * this->hidden_matmul_count + hidden_width * padded_output_width)
{
	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[8];
		// batch_size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// input_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// weights_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// output_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// activations_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// dloss_doutput_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// activations_gradient_buffer
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// dloss_dinput_buffer
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[7].hlsl_register_space = 0;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[3];
		// batch_size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// gemm_parameters_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_descriptor_set_layout));
	}

	assert_framework_gpu_result(gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::none, 1, ::framework::gpu::shader_stage_flags::all, this->gpu_log_descriptor_set_layout));

	::std::string const hidden_activation_string = ::std::to_string(static_cast<::std::uint32_t>(hidden_activation));
	::std::string const output_activation_string = ::std::to_string(static_cast<::std::uint32_t>(output_activation));
	::framework::resource::preprocessor_define const gemm_hidden_activation_backward_epilogue_preprocessor_defines[]
	{
		{ "activation", hidden_activation_string.c_str() }
	};

	this->gemm_linear_combination_epilogue = ::framework::algorithm::gemm(gpu_context, resource_manager, parameter_type, parameter_type, parameter_type, parameter_type, ::framework::gpu::component_type::float32, 0, nullptr, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_hlsl_path, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_glsl_path);
	this->gemm_hidden_activation_backward_epilogue = ::framework::algorithm::gemm(gpu_context, resource_manager, parameter_type, parameter_type, parameter_type, parameter_type, ::framework::gpu::component_type::float32, static_cast<::std::uint32_t>(::std::size(gemm_hidden_activation_backward_epilogue_preprocessor_defines)), gemm_hidden_activation_backward_epilogue_preprocessor_defines, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_hlsl_path, ::framework::local::activation_backward_gemm_epilogue_glsl_path);
	this->inference_output_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(padded_output_width, 256u), ::std::min(inference_batch_alignment, 256u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->forward_output_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(padded_output_width, 256u), ::std::min(batch_alignment, 256u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->backward_output_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(padded_output_width, 256u), ::std::min(hidden_width, 256u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_output_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm_linear_combination_epilogue, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_hidden_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(hidden_width, 256u), ::std::min(hidden_width, 256u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_hidden_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm_linear_combination_epilogue, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_input_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(hidden_width, 256u), ::std::min(padded_input_width, 256u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::local::transpose(input_layout), ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_input_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm_linear_combination_epilogue, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_output_layer_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm_hidden_activation_backward_epilogue, gpu_context, 0u, 0u, ::std::min(hidden_width, 256u), ::std::min(batch_alignment, 256u), ::std::min(padded_output_width, 32u), false, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->backward_input_layer_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm_linear_combination_epilogue, gpu_context, 0u, 0u, ::std::min(padded_input_width, 256u), ::std::min(batch_alignment, 256u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);

	// Only let the fully fused kernel compute gradients w.r.t. the input, if the input layer has the same size & layout as the other layers
	bool const dloss_dinput_fused = padded_input_width == hidden_width && input_layout == ::framework::linalg::matrix_layout::column_major;
	this->compute_dloss_dinput_non_fused = !dloss_dinput_fused && compute_dloss_dinput;
	char const * parameter_type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);
	char const * accumulator_type_name = ::framework::gpu::utility::get_component_type_name(accumulator_type);
	::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::string const parameter_type_size_string = ::std::to_string(parameter_type_size * 8);
	::std::uint32_t const input_weights_stride = padded_input_width * hidden_width;
	::std::uint32_t const hidden_weights_stride = hidden_width * hidden_width;
	::std::uint32_t const output_weights_stride = hidden_width * padded_output_width;
	::std::uint32_t const n_iters = this->hidden_width >= 256 ? 2 : 8;
	::framework::linalg::matrix_layout const transposed_input_layout = ::framework::local::transpose(input_layout);
	::framework::linalg::matrix_layout const transposed_output_layout = ::framework::local::transpose(output_layout);
	::std::uint32_t const n_block_rows = this->hidden_width / 16; // number of warps. Each warp processes a 16x16 element-wide matrix.
	::std::uint32_t const input_elements_per_vec4 = 16 / parameter_type_size;
	::std::uint32_t const skew = hidden_width % 16 == 0 ? input_elements_per_vec4 : 0;
	::std::uint32_t const input_skew = input_elements_per_vec4;
	bool const dynamic_input_width = padded_input_width != hidden_width || transposed_input_layout != ::framework::linalg::matrix_layout::row_major;
	::std::uint32_t const forward_shared_memory_size = ::std::max(((16 + 16 * n_iters) * (hidden_width + skew)), dynamic_input_width ? (hidden_width + 16) * (padded_input_width + input_skew) : 0) / input_elements_per_vec4;
	::std::uint32_t const backward_shared_memory_size = ((16 * n_iters) * (hidden_width + skew)) / input_elements_per_vec4;
	assert(forward_shared_memory_size * 16 <= gpu_context->get_physical_device_properties().limits.max_compute_shared_memory_size);
	assert(backward_shared_memory_size * 16 <= gpu_context->get_physical_device_properties().limits.max_compute_shared_memory_size);
	this->batch_size_buffer_alignment = (sizeof(::framework::nn::fully_fused_mlp::network::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);

	{
		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "nn/fully_fused_mlp/fully_fused_mlp.comp.glsl";
		glsl_source_info.source_entry_point = "forward_main";

		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "parameter_type_size", parameter_type_size_string.c_str() },
			{ "parameter_t", parameter_type_name },
			{ "accumulator_t", accumulator_type_name }
		};

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "forward_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->forward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "nn/fully_fused_mlp/fully_fused_mlp.comp.glsl";
		glsl_source_info.source_entry_point = "backward_main";

		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "parameter_type_size", parameter_type_size_string.c_str() },
			{ "parameter_t", parameter_type_name },
			{ "accumulator_t", accumulator_type_name }
		};

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "backward_main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->backward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "nn/fully_fused_mlp/backward_output_activation.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::preprocessor_define const preprocessor_defines[]
		{
			{ "parameter_t", parameter_type_name },
			{ "activation_input_t", parameter_type_name },
			{ "gradient_t", parameter_type_name }
		};

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->backward_output_activation_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::resource::hlsl_source_info hlsl_source_info;
		hlsl_source_info.file_name = "nn/fully_fused_mlp/initialize_dispatch_indirect_command.comp.hlsl";
		hlsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = &hlsl_source_info;
		shader_stage_info.glsl_source_info = nullptr;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = 0;
		shader_stage_info.preprocessor_defines = nullptr;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->initialize_dispatch_indirect_command_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout,
			this->gpu_log_descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->initialize_dispatch_indirect_command_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::initialize_dispatch_indirect_command_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline_layout));
	}

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) },
		{ 2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t) },
		{ 3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t) },
		{ 4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t) },
		{ 5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t) },
		{ 6, sizeof(::std::uint32_t) * 6, sizeof(::std::uint32_t) },
		{ 7, sizeof(::std::uint32_t) * 7, sizeof(::std::uint32_t) },
		{ 8, sizeof(::std::uint32_t) * 8, sizeof(::std::uint32_t) },
		{ 9, sizeof(::std::uint32_t) * 9, sizeof(::std::uint32_t) },
		{ 10, sizeof(::std::uint32_t) * 10, sizeof(::std::uint32_t) },
		{ 11, sizeof(::std::uint32_t) * 11, sizeof(::std::uint32_t) },
		{ 12, sizeof(::std::uint32_t) * 12, sizeof(::std::uint32_t) },
		{ 13, sizeof(::std::uint32_t) * 13, sizeof(::std::uint32_t) },
		{ 14, sizeof(::std::uint32_t) * 14, sizeof(::std::uint32_t) },
		{ 15, sizeof(::std::uint32_t) * 15, sizeof(::std::uint32_t) }
	};

	::std::uint32_t const specialization_data[][::std::size(specialization_map_entries)]
	{
		// inference
		{
			padded_input_width,
			padded_output_width,
			this->hidden_width,
			max_batch_size,
			this->hidden_matmul_count,
			n_iters,
			::framework::gpu::bool32_t(false),
			::framework::gpu::bool32_t(true),
			static_cast<::std::uint32_t>(transposed_input_layout),
			static_cast<::std::uint32_t>(transposed_output_layout),
			padded_output_width,
			n_block_rows,
			static_cast<::framework::gpu::bool32_t>(dloss_dinput_fused),
			forward_shared_memory_size,
			static_cast<::std::uint32_t>(hidden_activation),
			static_cast<::std::uint32_t>(output_activation)
		},
		// forward
		{
			padded_input_width,
			padded_output_width,
			this->hidden_width,
			max_batch_size,
			this->hidden_matmul_count,
			n_iters,
			::framework::gpu::bool32_t(false),
			::framework::gpu::bool32_t(false),
			static_cast<::std::uint32_t>(transposed_input_layout),
			static_cast<::std::uint32_t>(transposed_output_layout),
			padded_output_width,
			n_block_rows,
			static_cast<::framework::gpu::bool32_t>(dloss_dinput_fused),
			forward_shared_memory_size,
			static_cast<::std::uint32_t>(hidden_activation),
			static_cast<::std::uint32_t>(output_activation)
		},
		// backward
		{
			padded_input_width,
			padded_output_width,
			this->hidden_width,
			max_batch_size,
			this->hidden_matmul_count,
			n_iters,
			::framework::gpu::bool32_t(true),
			::framework::gpu::bool32_t(false),
			static_cast<::std::uint32_t>(transposed_input_layout),
			static_cast<::std::uint32_t>(transposed_output_layout),
			padded_output_width,
			n_block_rows,
			static_cast<::framework::gpu::bool32_t>(dloss_dinput_fused),
			backward_shared_memory_size,
			static_cast<::std::uint32_t>(hidden_activation),
			static_cast<::std::uint32_t>(output_activation)
		}
	};

	::framework::gpu::specialization_info specialization_infos[3];
	specialization_infos[0].map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_infos[0].map_entries = specialization_map_entries;
	specialization_infos[0].data_size = sizeof(specialization_data[0]);
	specialization_infos[0].data = specialization_data[0];

	specialization_infos[1].map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_infos[1].map_entries = specialization_map_entries;
	specialization_infos[1].data_size = sizeof(specialization_data[1]);
	specialization_infos[1].data = specialization_data[1];

	specialization_infos[2].map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_infos[2].map_entries = specialization_map_entries;
	specialization_infos[2].data_size = sizeof(specialization_data[2]);
	specialization_infos[2].data = specialization_data[2];

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[5];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = this->forward_shader_module;
	pipeline_shader_stage_create_infos[0].name = "forward_main";
	pipeline_shader_stage_create_infos[0].specialization_info = &specialization_infos[0];

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = this->forward_shader_module;
	pipeline_shader_stage_create_infos[1].name = "forward_main";
	pipeline_shader_stage_create_infos[1].specialization_info = &specialization_infos[1];

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = this->backward_shader_module;
	pipeline_shader_stage_create_infos[2].name = "backward_main";
	pipeline_shader_stage_create_infos[2].specialization_info = &specialization_infos[2];

	pipeline_shader_stage_create_infos[3].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[3].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[3].module = this->backward_output_activation_shader_module;
	pipeline_shader_stage_create_infos[3].name = "main";
	pipeline_shader_stage_create_infos[3].specialization_info = &specialization_infos[2];

	pipeline_shader_stage_create_infos[4].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[4].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[4].module = this->initialize_dispatch_indirect_command_shader_module;
	pipeline_shader_stage_create_infos[4].name = "main";
	pipeline_shader_stage_create_infos[4].specialization_info = &specialization_infos[2];

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[5];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = this->pipeline_layout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = this->pipeline_layout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = this->pipeline_layout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	compute_pipeline_create_infos[3].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[3].stage = pipeline_shader_stage_create_infos[3];
	compute_pipeline_create_infos[3].layout = this->pipeline_layout;
	compute_pipeline_create_infos[3].base_pipeline = nullptr;
	compute_pipeline_create_infos[3].base_pipeline_index = -1;

	compute_pipeline_create_infos[4].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[4].stage = pipeline_shader_stage_create_infos[4];
	compute_pipeline_create_infos[4].layout = this->initialize_dispatch_indirect_command_pipeline_layout;
	compute_pipeline_create_infos[4].base_pipeline = nullptr;
	compute_pipeline_create_infos[4].base_pipeline_index = -1;

	::framework::gpu::pipeline * pipelines[::std::size(compute_pipeline_create_infos)];
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines));
	this->inference_pipeline = pipelines[0], this->forward_pipeline = pipelines[1], this->backward_pipeline = pipelines[2], this->backward_output_activation_pipeline = pipelines[3], this->initialize_dispatch_indirect_command_pipeline = pipelines[4];

	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = gemm_device_parameters_buffer_alignment * 7;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->gemm_parameters_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gemm_parameters_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->gemm_parameters_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->gemm_parameters_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gemm_parameters_buffer;
		bind_buffer_memory_info.memory = this->gemm_parameters_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = sizeof(::framework::gpu::dispatch_indirect_command) * this->get_dispatch_indirect_command_count();
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::indirect_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dispatch_indirect_command_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dispatch_indirect_command_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->dispatch_indirect_command_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->dispatch_indirect_command_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dispatch_indirect_command_buffer;
		bind_buffer_memory_info.memory = this->dispatch_indirect_command_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::std::uint32_t const max_split_k_slices = ::std::max(max_batch_size, max_inference_batch_size) / ::std::min(::std::max(max_batch_size, max_inference_batch_size), 1u << 12);
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max({ input_weights_stride, hidden_weights_stride, output_weights_stride }) * max_split_k_slices * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->split_k_slices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->split_k_slices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->split_k_slices_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->split_k_slices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->split_k_slices_buffer;
		bind_buffer_memory_info.memory = this->split_k_slices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
}

void(::framework::nn::fully_fused_mlp::execution_policy::initialize_dispatch_indirect_command)(::framework::gpu_context * gpu_context, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, bool inference)
{
	::framework::local::initialize_dispatch_indirect_command_push_constants push_constants;
	push_constants.gemm_device_parameters_buffer_alignment = ::framework::numeric::align_up(sizeof(::framework::algorithm::gemm::device_parameters), gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	push_constants.batch_size_stride = ::framework::numeric::align_up(sizeof(::std::uint32_t), gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment);
	push_constants.inference = static_cast<::framework::gpu::bool32_t>(inference);

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->push_constants(this->initialize_dispatch_indirect_command_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch(1, 1, 1);
}

void ::framework::nn::fully_fused_mlp::execution_policy::update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
	// batch_size_buffer
	descriptor_buffer_infos[0].buffer = batch_size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 4;
	// dispatch_indirect_command_buffer
	descriptor_buffer_infos[1].buffer = this->dispatch_indirect_command_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;
	// gemm_parameters_buffer
	descriptor_buffer_infos[2].buffer = this->gemm_parameters_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[3];
	// batch_size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// dispatch_indirect_command_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// gemm_parameters_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;

	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

::framework::coroutine::immediate_task<void>(::framework::nn::fully_fused_mlp::execution_policy::update_parameters)(::framework::gpu_context * gpu_context, ::framework::command_context * command_context, ::std::uint32_t batch_size, ::std::uint32_t inference_batch_size)
{
	::std::uint32_t const split_k_slices = batch_size / ::std::min(batch_size, 1u << 12);
	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->inference_output_matmul.get_device_parameters(gpu_context, this->padded_output_width, inference_batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 0 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->forward_output_matmul.get_device_parameters(gpu_context, this->padded_output_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 1 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_output_weights_gradient_matmul.get_device_parameters(gpu_context, this->padded_output_width, this->hidden_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 2 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_output_layer_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, batch_size, this->padded_output_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 3 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_hidden_weights_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, this->hidden_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 4 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_input_weights_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, this->padded_input_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 5 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_input_layer_gradient_matmul.get_device_parameters(gpu_context, this->padded_input_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 6 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
}

::framework::nn::fully_fused_mlp::network::network(::framework::nn::fully_fused_mlp::execution_policy const & execution_policy, ::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::descriptor_pool * descriptor_pool) :
	execution_policy(execution_policy)
{
	::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::string const parameter_type_size_string = ::std::to_string(parameter_type_size * 8);
	::std::uint32_t const input_weights_stride = execution_policy.padded_input_width * execution_policy.hidden_width;
	::std::uint32_t const hidden_weights_stride = execution_policy.hidden_width * execution_policy.hidden_width;
	::std::uint32_t const output_weights_stride = execution_policy.hidden_width * execution_policy.padded_output_width;
	::std::uint32_t const input_layer_stride = execution_policy.padded_input_width * max_batch_size;
	::std::uint32_t const hidden_layer_stride = execution_policy.hidden_width * max_batch_size;
	::std::uint32_t const output_layer_stride = execution_policy.padded_output_width * max_batch_size;
	::std::uint32_t const inference_hidden_layer_stride = execution_policy.hidden_width * max_inference_batch_size;
	::std::uint32_t const inference_output_layer_stride = execution_policy.padded_output_width * max_inference_batch_size;
	::std::uint32_t const hidden_layer_count = execution_policy.hidden_matmul_count + 1;
	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = execution_policy.parameter_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->weight_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->weight_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->weight_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->weight_matrices_buffer;
		bind_buffer_memory_info.memory = this->weight_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = execution_policy.parameter_count * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->full_precision_weights_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->full_precision_weights_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->full_precision_weights_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->full_precision_weights_matrices_buffer;
		bind_buffer_memory_info.memory = this->full_precision_weights_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = execution_policy.parameter_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->gradient_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gradient_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->gradient_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gradient_matrices_buffer;
		bind_buffer_memory_info.memory = this->gradient_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = output_layer_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dloss_doutput_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dloss_doutput_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->output_layer_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dloss_doutput_buffer;
		bind_buffer_memory_info.memory = this->output_layer_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = hidden_layer_stride * hidden_layer_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->activations_gradient_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->activations_gradient_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->activations_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->activations_gradient_buffer;
		bind_buffer_memory_info.memory = this->activations_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(output_layer_stride, inference_output_layer_stride) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->output_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->output_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->output_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->output_buffer;
		bind_buffer_memory_info.memory = this->output_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(execution_policy.padded_output_width > 16 ? inference_hidden_layer_stride : 0, hidden_layer_stride * (hidden_layer_count + 1)) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->activations_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->activations_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->activations_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->activations_buffer;
		bind_buffer_memory_info.memory = this->activations_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		this->gemm_descriptor_sets.resize(6ull + this->execution_policy.hidden_matmul_count);
		::std::vector<::framework::gpu::descriptor_set_layout *> descriptor_set_layouts(this->gemm_descriptor_sets.size() + 1);
		::std::vector<::framework::gpu::descriptor_set *> descriptor_sets(this->gemm_descriptor_sets.size() + 1);
		::std::fill(descriptor_set_layouts.begin(), descriptor_set_layouts.end() - 1, this->execution_policy.gemm_linear_combination_epilogue.get_descriptor_set_layout());
		descriptor_set_layouts.back() = this->execution_policy.gpu_log_descriptor_set_layout;

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(descriptor_set_layouts.size());
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts.data();
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets.data()));
		::std::copy(descriptor_sets.begin(), descriptor_sets.end() - 1, this->gemm_descriptor_sets.begin());
		this->gpu_log_descriptor_set = descriptor_sets.back();
	}

	{
		::std::uint32_t const descriptor_buffer_info_count = 6 * this->gemm_descriptor_sets.size();
		::std::uint32_t current_gemm_descriptor_set_index = 0;
		::std::vector<::framework::gpu::descriptor_buffer_info> descriptor_buffer_infos(descriptor_buffer_info_count);
		::std::vector<::framework::gpu::write_descriptor_set> write_descriptor_sets(descriptor_buffer_info_count);

		auto add_gemm_write_descriptor_sets = [&](::std::array<::framework::gpu::buffer *, 6> buffers, ::std::array<::framework::gpu::device_size, 6> offsets, ::std::array<::framework::gpu::device_size, 6> ranges)
		{
			::std::uint32_t const write_descriptor_sets_index = current_gemm_descriptor_set_index * 6;

			for (::std::uint32_t i = 0; i < 6; i++)
			{
				descriptor_buffer_infos[write_descriptor_sets_index + i].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
				descriptor_buffer_infos[write_descriptor_sets_index + i].buffer = buffers[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].offset = offsets[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].range = ranges[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].stride = parameter_type_size;

				write_descriptor_sets[write_descriptor_sets_index + i].dst_set = this->gemm_descriptor_sets[current_gemm_descriptor_set_index];
				write_descriptor_sets[write_descriptor_sets_index + i].dst_binding = i;
				write_descriptor_sets[write_descriptor_sets_index + i].dst_array_element = 0;
				write_descriptor_sets[write_descriptor_sets_index + i].descriptor_count = 1;
				write_descriptor_sets[write_descriptor_sets_index + i].descriptor_type = i != 5 ? ::framework::gpu::descriptor_type::storage_buffer : ::framework::gpu::descriptor_type::uniform_buffer;
				write_descriptor_sets[write_descriptor_sets_index + i].image_info = nullptr;
				write_descriptor_sets[write_descriptor_sets_index + i].buffer_info = &descriptor_buffer_infos[write_descriptor_sets_index + i];
				write_descriptor_sets[write_descriptor_sets_index + i].texel_buffer_view = nullptr;
				write_descriptor_sets[write_descriptor_sets_index + i].acceleration_structures = nullptr;
			}

			current_gemm_descriptor_set_index++;
		};

		// inference
		// output_weights * last hidden layer's activations = output layer's activations
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_buffer,
				this->output_buffer,
				this->output_buffer,
				this->output_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				0,
				0,
				0,
				0,
				0 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				::framework::gpu::whole_size,
				inference_output_layer_stride * parameter_type_size,
				inference_output_layer_stride * parameter_type_size,
				inference_output_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// forward
		// output_weights * last hidden layer's activations = output layer's activations
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_buffer,
				this->output_buffer,
				this->output_buffer,
				this->output_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				1 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// backward
		// loss_gradient * last_hidden_post_activations = output_weight's gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->dloss_doutput_buffer,
				this->activations_buffer,
				this->gradient_matrices_buffer,
				this->gradient_matrices_buffer,
				this->execution_policy.split_k_slices_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				0,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				0,
				2 * gemm_device_parameters_buffer_alignment
			},
			{
				// padded_output_width x batch_size
				output_layer_stride * parameter_type_size,
				// transpose(hidden_width x batch_size)
				hidden_layer_stride * parameter_type_size,
				// padded_output_width x hidden_width
				output_weights_stride * parameter_type_size,
				// padded_output_width x hidden_width
				output_weights_stride * parameter_type_size,
				::framework::gpu::whole_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// output_weight_matrix * loss_gradient = last hidden layer's activations gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->dloss_doutput_buffer,
				this->activations_buffer,
				this->activations_gradient_buffer,
				this->activations_gradient_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				0,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				3 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
		{
			// (i + 1)'th hidden_gradient * i'th hidden_post_activations = i'th hidden_weight's gradient
			add_gemm_write_descriptor_sets
			(
				{
					this->activations_gradient_buffer,
					this->activations_buffer,
					this->gradient_matrices_buffer,
					this->gradient_matrices_buffer,
					this->execution_policy.split_k_slices_buffer,
					this->execution_policy.gemm_parameters_buffer
				},
				{
					hidden_layer_stride * i * parameter_type_size,
					hidden_layer_stride * (this->execution_policy.hidden_matmul_count - 1 - i) * parameter_type_size,
					(input_weights_stride + hidden_weights_stride * (this->execution_policy.hidden_matmul_count - 1 - i)) * parameter_type_size,
					(input_weights_stride + hidden_weights_stride * (this->execution_policy.hidden_matmul_count - 1 - i)) * parameter_type_size,
					0,
					4 * gemm_device_parameters_buffer_alignment
				},
				{
					// hidden_width x batch_size
					hidden_layer_stride * parameter_type_size,
					// transpose(hidden_width x batch_size)
					hidden_layer_stride * parameter_type_size,
					// hidden_width x hidden_width
					hidden_weights_stride * parameter_type_size,
					// hidden_width x hidden_width
					hidden_weights_stride * parameter_type_size,
					::framework::gpu::whole_size,
					sizeof(::framework::algorithm::gemm::device_parameters)
				}
			);
		}

		// 0'th hidden_gradient * input = input_weight's gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->activations_gradient_buffer,
				input_buffer,
				this->gradient_matrices_buffer,
				this->gradient_matrices_buffer,
				this->execution_policy.split_k_slices_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				0,
				5 * gemm_device_parameters_buffer_alignment
			},
			{
				// hidden_width x batch_size
				hidden_layer_stride * parameter_type_size,
				// transpose(padded_input_width x batch_size)
				::framework::gpu::whole_size,
				// hidden_width x padded_input_width
				input_weights_stride * parameter_type_size,
				// hidden_width x padded_input_width
				input_weights_stride * parameter_type_size,
				::framework::gpu::whole_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// input_weight_matrix * 0'th hidden layer's activations gradient = dL_dinput
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_gradient_buffer,
				dloss_dinput_buffer,
				dloss_dinput_buffer,
				dloss_dinput_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				0,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				6 * gemm_device_parameters_buffer_alignment
			},
			{
				input_weights_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets.data(), 0, nullptr);
	}

	gpu_log->write_descriptor_set(this->gpu_log_descriptor_set);
}

void ::framework::nn::fully_fused_mlp::network::update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[8];
	descriptor_buffer_infos[0].buffer = batch_size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = input_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	descriptor_buffer_infos[2].buffer = this->weight_matrices_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	descriptor_buffer_infos[3].buffer = this->output_buffer;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = 2;

	descriptor_buffer_infos[4].buffer = this->activations_buffer;
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = 2;

	descriptor_buffer_infos[5].buffer = this->dloss_doutput_buffer;
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = 2;

	descriptor_buffer_infos[6].buffer = this->activations_gradient_buffer;
	descriptor_buffer_infos[6].offset = 0;
	descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[6].stride = 2;

	descriptor_buffer_infos[7].buffer = dloss_dinput_buffer;
	descriptor_buffer_infos[7].offset = 0;
	descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[7].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[8];
	// batch_size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// input_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// weights_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;
	// output_buffer
	write_descriptor_sets[3].dst_set = descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[3].image_info = nullptr;
	write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
	write_descriptor_sets[3].texel_buffer_view = nullptr;
	write_descriptor_sets[3].acceleration_structures = nullptr;
	// activations_buffer
	write_descriptor_sets[4].dst_set = descriptor_set;
	write_descriptor_sets[4].dst_binding = 4;
	write_descriptor_sets[4].dst_array_element = 0;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[4].image_info = nullptr;
	write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
	write_descriptor_sets[4].texel_buffer_view = nullptr;
	write_descriptor_sets[4].acceleration_structures = nullptr;
	// dloss_doutput_buffer
	write_descriptor_sets[5].dst_set = descriptor_set;
	write_descriptor_sets[5].dst_binding = 5;
	write_descriptor_sets[5].dst_array_element = 0;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[5].image_info = nullptr;
	write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
	write_descriptor_sets[5].texel_buffer_view = nullptr;
	write_descriptor_sets[5].acceleration_structures = nullptr;
	// activations_gradient_buffer
	write_descriptor_sets[6].dst_set = descriptor_set;
	write_descriptor_sets[6].dst_binding = 6;
	write_descriptor_sets[6].dst_array_element = 0;
	write_descriptor_sets[6].descriptor_count = 1;
	write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[6].image_info = nullptr;
	write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[6];
	write_descriptor_sets[6].texel_buffer_view = nullptr;
	write_descriptor_sets[6].acceleration_structures = nullptr;
	// dloss_dinput_buffer
	write_descriptor_sets[7].dst_set = descriptor_set;
	write_descriptor_sets[7].dst_binding = 7;
	write_descriptor_sets[7].dst_array_element = 0;
	write_descriptor_sets[7].descriptor_count = 1;
	write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[7].image_info = nullptr;
	write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[7];
	write_descriptor_sets[7].texel_buffer_view = nullptr;
	write_descriptor_sets[7].acceleration_structures = nullptr;

	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

void(::framework::nn::fully_fused_mlp::network::initialize_xavier_uniform)(::framework::gpu::command_buffer * command_buffer, ::framework::algorithm::xavier_uniform * xavier_uniform, ::framework::gpu::descriptor_set * descriptor_set)
{
	::std::uint32_t parameter_offset = 0u;
	::std::uint32_t parameter_count = execution_policy.padded_input_width * execution_policy.hidden_width;
	xavier_uniform->execute(command_buffer, descriptor_set, parameter_offset, parameter_count, this->execution_policy.input_width * execution_policy.hidden_width);
	parameter_offset += parameter_count;
	parameter_count = execution_policy.hidden_width * execution_policy.hidden_width;
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; i++)
	{
		xavier_uniform->execute(command_buffer, descriptor_set, parameter_offset, parameter_count, parameter_count);
		parameter_offset += parameter_count;
	}
	parameter_count = execution_policy.hidden_width * execution_policy.padded_output_width;
	xavier_uniform->execute(command_buffer, descriptor_set, parameter_offset, parameter_count, execution_policy.hidden_width * this->execution_policy.output_width);
}

void ::framework::nn::fully_fused_mlp::network::inference(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 0ull;
	::std::uint32_t const n_iters = execution_policy.hidden_width >= 256 ? 2 : 8;
	::std::uint32_t const n_elems_per_block = 16 * n_iters;
	::std::uint32_t const n_blocks = (batch_size + n_elems_per_block - 1) / n_elems_per_block;
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = 0;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.inference_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(n_blocks, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_layer_weight_matrix * last_hidden_post_activations = output_activation */
		this->execution_policy.inference_output_matmul.execute(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.padded_output_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::fully_fused_mlp::network::inference_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::std::uint32_t current_gemm_descriptor_set_index = 0ull;
	::framework::gpu::descriptor_set * const descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = 0;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.inference_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(this->execution_policy.dispatch_indirect_command_buffer, 0);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_layer_weight_matrix * last_hidden_post_activations = output_activation */
		this->execution_policy.inference_output_matmul.execute_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, sizeof(::framework::gpu::dispatch_indirect_command));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::fully_fused_mlp::network::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 1ull;
	::std::uint32_t const n_iters = execution_policy.hidden_width >= 256 ? 2 : 8;
	::std::uint32_t const n_elems_per_block = 16 * n_iters;
	::std::uint32_t const n_blocks = (batch_size + n_elems_per_block - 1) / n_elems_per_block;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = this->execution_policy.batch_size_buffer_alignment;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(n_blocks, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_layer_weight_matrix * last_hidden_post_activations = output_activation */
		this->execution_policy.forward_output_matmul.execute(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.padded_output_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::fully_fused_mlp::network::forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::std::uint32_t current_gemm_descriptor_set_index = 1ull;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = this->execution_policy.batch_size_buffer_alignment;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(this->execution_policy.dispatch_indirect_command_buffer, 2u * sizeof(::framework::gpu::dispatch_indirect_command));
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_layer_weight_matrix * last_hidden_post_activations = output_activation */
		this->execution_policy.forward_output_matmul.execute_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 3u * sizeof(::framework::gpu::dispatch_indirect_command));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::fully_fused_mlp::network::backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 2ull;
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = this->execution_policy.batch_size_buffer_alignment;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->fill_buffer(this->gradient_matrices_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});

	if (this->execution_policy.output_activation != ::framework::nn::activation::none)
	{
		::std::uint32_t const output_layer_stride = execution_policy.padded_output_width * batch_size;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.backward_output_activation_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
		command_buffer->dispatch(::framework::numeric::div_round_up(output_layer_stride, 128u), 1, 1);
	}

	::std::uint32_t const split_k_slices = batch_size / ::std::min(batch_size, 1u << 12);

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	/* GEMM_SPLIT_K: loss_gradient * last_hidden_post_activations = output_weight's gradient */
	this->execution_policy.backward_output_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.padded_output_width, execution_policy.hidden_width, split_k_slices);
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_output_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.padded_output_width, execution_policy.hidden_width);
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_weight_matrix * loss_gradient = last layer's activations gradient */
		this->execution_policy.backward_output_layer_gradient_matmul.execute(this->execution_policy.gemm_hidden_activation_backward_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.hidden_width, batch_size);
	}

	current_gemm_descriptor_set_index++;

	::std::uint32_t const n_iters = execution_policy.hidden_width >= 256 ? 2 : 8;
	::std::uint32_t const n_elems_per_block = 16 * n_iters;
	::std::uint32_t const n_blocks = (batch_size + n_elems_per_block - 1) / n_elems_per_block;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.backward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch(n_blocks, 1, 1);

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// hidden layers
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
	{
		/* GEMM_SPLIT_K: (i + 1)'th hidden_gradient * i'th hidden_post_activations = i'th hidden_weight's gradient */
		this->execution_policy.backward_hidden_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.hidden_width, execution_policy.hidden_width, split_k_slices);
		{
			::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
			buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
			buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
			buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
			buffer_memory_barrier.offset = 0;
			buffer_memory_barrier.size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 1;
			dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->execution_policy.backward_hidden_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.hidden_width, execution_policy.hidden_width);
		current_gemm_descriptor_set_index++;
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	/* GEMM_SPLIT_K: 0'th hidden_gradient * input = input_weight's gradient */
	this->execution_policy.backward_input_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.hidden_width, execution_policy.padded_input_width, split_k_slices);
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_input_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], execution_policy.hidden_width, execution_policy.padded_input_width);
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// Compute loss gradients w.r.t. input if desired.
	if (this->execution_policy.compute_dloss_dinput_non_fused)
	{
		// GEMM: input_weight_matrix * 0'th hidden layer's activations gradient = dL_dinput
		this->execution_policy.backward_input_layer_gradient_matmul.execute(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_input_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::fully_fused_mlp::network::backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::std::uint32_t current_gemm_descriptor_set_index = 2ull;
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::std::uint32_t const dynamic_offset = this->execution_policy.batch_size_buffer_alignment;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->fill_buffer(this->gradient_matrices_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});

	if (this->execution_policy.output_activation != ::framework::nn::activation::none)
	{
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.backward_output_activation_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
		command_buffer->dispatch_indirect(this->execution_policy.dispatch_indirect_command_buffer, 4u * sizeof(::framework::gpu::dispatch_indirect_command));
	}

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	/* GEMM_SPLIT_K: loss_gradient * last_hidden_post_activations = output_weight's gradient */
	this->execution_policy.backward_output_weights_gradient_matmul.execute_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 5u * sizeof(::framework::gpu::dispatch_indirect_command));
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_output_weights_gradient_reduce_split_k.execute_reduce_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 6u * sizeof(::framework::gpu::dispatch_indirect_command));
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	if (execution_policy.padded_output_width > 16)
	{
		/* GEMM: output_weight_matrix * loss_gradient = last layer's activations gradient */
		this->execution_policy.backward_output_layer_gradient_matmul.execute_indirect(this->execution_policy.gemm_hidden_activation_backward_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 7u * sizeof(::framework::gpu::dispatch_indirect_command));
	}

	current_gemm_descriptor_set_index++;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.backward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(this->execution_policy.dispatch_indirect_command_buffer, 2u * sizeof(::framework::gpu::dispatch_indirect_command));

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// hidden layers
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
	{
		/* GEMM_SPLIT_K: (i + 1)'th hidden_gradient * i'th hidden_post_activations = i'th hidden_weight's gradient */
		this->execution_policy.backward_hidden_weights_gradient_matmul.execute_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 8u * sizeof(::framework::gpu::dispatch_indirect_command));
		{
			::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
			buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
			buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
			buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
			buffer_memory_barrier.offset = 0;
			buffer_memory_barrier.size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 1;
			dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->execution_policy.backward_hidden_weights_gradient_reduce_split_k.execute_reduce_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 9u * sizeof(::framework::gpu::dispatch_indirect_command));
		current_gemm_descriptor_set_index++;
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	/* GEMM_SPLIT_K: 0'th hidden_gradient * input = input_weight's gradient */
	this->execution_policy.backward_input_weights_gradient_matmul.execute_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 10u * sizeof(::framework::gpu::dispatch_indirect_command));
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_input_weights_gradient_reduce_split_k.execute_reduce_split_k_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 11u * sizeof(::framework::gpu::dispatch_indirect_command));
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// Compute loss gradients w.r.t. input if desired.
	if (this->execution_policy.compute_dloss_dinput_non_fused)
	{
		// GEMM: input_weight_matrix * 0'th hidden layer's activations gradient = dL_dinput
		this->execution_policy.backward_input_layer_gradient_matmul.execute_indirect(this->execution_policy.gemm_linear_combination_epilogue, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.dispatch_indirect_command_buffer, 12u * sizeof(::framework::gpu::dispatch_indirect_command));
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}