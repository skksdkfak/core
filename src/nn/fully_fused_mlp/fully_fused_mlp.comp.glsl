#version 450 core

#pragma use_vulkan_memory_model

#extension GL_ARB_shading_language_include : enable
#extension GL_KHR_shader_subgroup_basic : enable
#extension GL_EXT_scalar_block_layout : enable
#extension GL_KHR_memory_scope_semantics : enable
#extension GL_KHR_cooperative_matrix : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float32 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int8 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_int32 : enable
#extension GL_EXT_control_flow_attributes : enable

layout(constant_id = 0) const uint input_width = 1;
layout(constant_id = 1) const uint output_width = 1;
layout(constant_id = 2) const uint hidden_width = 1;
layout(constant_id = 3) const uint max_batch_size = 1;
layout(constant_id = 4) const uint hidden_matmul_count = 1;
layout(constant_id = 5) const uint n_iters = 1;
layout(constant_id = 6) const bool backward = false;
layout(constant_id = 7) const bool inference = false;
layout(constant_id = 8) const int input_layout = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 9) const int output_layout = gl_CooperativeMatrixLayoutRowMajor;
layout(constant_id = 10) const uint output_stride = 1;
layout(constant_id = 11) const uint invocations_per_workgroup = 1;
layout(constant_id = 12) const bool compute_input_gradient = false;
layout(constant_id = 13) const uint shared_memory_size = 1;
layout(constant_id = 14) const uint hidden_activation = 0;
layout(constant_id = 15) const uint output_activation = 0;

#define gradient_t accumulator_t
#define activation_input_t accumulator_t
#define gradient_coopmat_t coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>
#define activation_input_coopmat_t coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>

#include "nn/activation.glsl"

layout(local_size_x = 32, local_size_y_id = 11, local_size_z = 1) in;

layout(set = 0, binding = 0) uniform batch_size_buffer_t
{
    uint32_t batch_size;
};

layout(std430, set = 0, binding = 1) readonly buffer input_buffer_t
{
    uvec4 input_buffer[];
};

layout(std430, set = 0, binding = 2) readonly buffer weights_buffer_t
{
    uvec4 weights_buffer[];
};

layout(std430, set = 0, binding = 3) writeonly buffer output_buffer_t
{
    parameter_t output_buffer[];
};

layout(std430, set = 0, binding = 4) buffer activations_buffer_t
{
    uvec4 activations_buffer[];
};

// Loss gradients w.r.t. output. Assumed to be 16 neurons wide.
layout(std430, set = 0, binding = 5) readonly buffer dloss_doutput_buffer_t
{
    parameter_t dloss_doutput_buffer[];
};

// Backpropagated activation gradients.
layout(std430, set = 0, binding = 6) buffer activations_gradient_buffer_t
{
    uvec4 activations_gradient_buffer[];
};

layout(std430, set = 0, binding = 7) buffer dloss_dinput_buffer_t
{
    uvec4 dloss_dinput_buffer[];
};

// We perform the GEMM MxK * KxN, where:
// the K is n_blocks of 16-element chunks.
// the M is number of items from batch, we multiply 16 items per one GEMM operation
// the N is the number of neurons in the layer, i.e. hidden_width
// On every GEMM operation each warp loads KxN weights from the global memory,
// where K = hidden_width, N = 16 - this is the weights_frag[n_blocks] below.
// This way each warp computes 16 elements of the next hidden layer * number of elements from batch (16).
// We do n_blocks coopMatMulAdd operations, along the K dimension,
// where on each iterations we load activations frag matrix of 16x16.
// The resulting MxN of each warp contains 16x16 activation neurons for 16 layers.
// In total each WG computes 16 x n_iters elements from the batch.

const uint input_bytes = parameter_type_size / 8;
const uint input_elements_per_vec4 = 16 / input_bytes; // 16 bytes, parameter_type_size bits per element
const uint row_pad_sh = input_elements_per_vec4;
const uint skew = hidden_width % 16 == 0 ? input_elements_per_vec4 : 0;
const uint input_skew = input_elements_per_vec4;
const uint n_blocks = hidden_width / 16;
const uint input_weights_stride = hidden_width * input_width;
const uint hidden_weights_stride = hidden_width * hidden_width;

shared uvec4 shared_memory[shared_memory_size];

void threadblock_load_input_static(uint buffer_offset)
{
    // Indices
    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint idx = input_elements_per_vec4 * (li + 32 * wi);
    const uint col = idx % hidden_width;
    const uint row = idx / hidden_width;

    [[unroll]]
    for (uint i = 0; i < n_iters; ++i)
    {
        const uint src_addr = (buffer_offset + col + (row + 16 * i) * hidden_width) / input_elements_per_vec4;
        shared_memory[(col + (row + 16 * i) * (hidden_width + skew)) / input_elements_per_vec4] = backward ? activations_gradient_buffer[src_addr] : input_buffer[src_addr];
    }
}

void threadblock_input_layer_forward_dynamic(uint elem_idx)
{
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseA> act_frag;
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseB> weights_frag;
    coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result_frag[n_iters];

    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint thread_elem_idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint col = thread_elem_idx % hidden_width;
    const uint row = thread_elem_idx / hidden_width;
    const uint weights_col = 16 * wi;
    const uint n_elems_per_load = n_blocks * 32 * input_elements_per_vec4;
    const uint n_elems_b = input_width * hidden_width;
    const uint weights_shared_memory_offset = 16 * (input_width + input_skew);
    const uint input_buffer_offset = input_layout == gl_CooperativeMatrixLayoutRowMajor ? elem_idx * input_width : elem_idx;
    const uint n_tensor_ops = input_width / 16;

    // Load input weight matrix (fits completely into shared memory)
    // Each thread can load 8 fp16 elements (16 bytes) at once; we have n_blocks warps
    [[unroll]]
    for (uint i = thread_elem_idx; i < n_elems_b; i += n_elems_per_load)
    {
        const uint i_skewed = i + i / input_width * input_skew;
        shared_memory[(weights_shared_memory_offset + i_skewed) / input_elements_per_vec4] = weights_buffer[i / input_elements_per_vec4];
    }

    if (input_layout == gl_CooperativeMatrixLayoutColumnMajor)
    {
        barrier();
    }

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        if (input_layout == gl_CooperativeMatrixLayoutRowMajor)
        {
            // Load chunk of inputs into shmem.
            // This is faster than loading it from gmem directly, even though it is only used once.
            // (Possibly due to latency hiding through staging.)
            const uint n_elems_a = 16 * input_width;

            [[unroll]]
            for (uint i = thread_elem_idx; i < n_elems_a; i += n_elems_per_load)
            {
                const uint i_skewed = i + i / input_width * input_skew;
                shared_memory[i_skewed / input_elements_per_vec4] = input_buffer[(input_buffer_offset + l * n_elems_a + i) / input_elements_per_vec4];
            }

            barrier();
        }

        result_frag[l] = coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(0.0f);

        [[unroll]]
        for (uint i = 0; i < n_tensor_ops; ++i)
        {
            // Load chunk of inputs and weights from shared memory and multiply them
            if (input_layout == gl_CooperativeMatrixLayoutRowMajor)
            {
                coopMatLoad(act_frag, shared_memory, (16 * i) / input_elements_per_vec4, (input_width + input_skew) / input_elements_per_vec4, input_layout);
            }
            else
            {
                coopMatLoad(act_frag, input_buffer, (input_buffer_offset + 16 * i * batch_size + 16 * l) / input_elements_per_vec4, batch_size / input_elements_per_vec4, input_layout);
            }
            coopMatLoad(weights_frag, shared_memory, (weights_shared_memory_offset + 16 * i + weights_col * (input_width + input_skew)) / input_elements_per_vec4, (input_width + input_skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutColumnMajor);
            result_frag[l] = coopMatMulAdd(act_frag, weights_frag, result_frag[l]);
        }

        if (input_layout == gl_CooperativeMatrixLayoutRowMajor)
        {
            barrier();
        }

        warp_activation(result_frag[l], hidden_activation);
    }

    if (input_layout == gl_CooperativeMatrixLayoutColumnMajor)
    {
        barrier();
    }

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result = coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(result_frag[l]);
        coopMatStore(result, shared_memory, (weights_col + l * 16 * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
    }

    if (!inference)
    {
        barrier();

        [[unroll]]
        for (int l = 0; l < n_iters; ++l)
        {
            activations_buffer[(elem_idx * hidden_width + col + (row + 16 * l) * hidden_width) / input_elements_per_vec4] = shared_memory[(col + (row + 16 * l) * (hidden_width + skew)) / input_elements_per_vec4];
        }
    }
}

void threadblock_layer(uint weights_offset, uint output_intermediate_offset, uint forward_offset)
{
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseA> act_frag;
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseB> weights_frag[n_blocks];
    coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result_frag[n_iters];

    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint col = idx % hidden_width;
    const uint row = idx / hidden_width;
    const uint weights_col = 16 * wi;

    barrier();

    // Load n_blocks chunks of weights from global memory into registers.
    [[unroll]]
    for (uint i = 0; i < n_blocks; ++i)
    {
        const uint weights_row = 16 * i;
        if (backward)
        {
            // If we're performing the backward pass, additional index swizzling is needed to
            // load the weights in transposed form.
            coopMatLoad(weights_frag[i], weights_buffer, (weights_offset + weights_row * hidden_width + weights_col) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
        }
        else
        {
            coopMatLoad(weights_frag[i], weights_buffer, (weights_offset + weights_col * hidden_width + weights_row) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutColumnMajor);
        }
    }

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        result_frag[l] = coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(0.0f);

        [[unroll]]
        for (uint i = 0; i < n_blocks; ++i)
        {
            // Load a chunk of intermediate activations from shared memory and multiply with chunk of weights
            coopMatLoad(act_frag, shared_memory, (16 * i + 16 * l * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
            result_frag[l] = coopMatMulAdd(act_frag, weights_frag[i], result_frag[l]);
        }

        // Activation
        if (backward)
        {
            // Load the temporary forward matrix for the relu transfer
            coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> forward_frag;
            coopMatLoad(forward_frag, activations_buffer, (forward_offset + weights_col + l * 16 * hidden_width) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
            warp_activation_backward(result_frag[l], forward_frag, hidden_activation);
        }
        else
        {
            warp_activation(result_frag[l], hidden_activation);
        }
    }

    barrier();

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result = coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(result_frag[l]);
        coopMatStore(result, shared_memory, (weights_col + l * 16 * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
    }

    if (!inference)
    {
        barrier();

        [[unroll]]
        for (int l = 0; l < n_iters; ++l)
        {
            const uint dst_addr = (output_intermediate_offset + col + (row + 16 * l) * hidden_width) / input_elements_per_vec4;
            const uvec4 src = shared_memory[(col + (row + 16 * l) * (hidden_width + skew)) / input_elements_per_vec4];
            if (backward)
            {
                activations_gradient_buffer[dst_addr] = src;
            }
            else
            {
                activations_buffer[dst_addr] = src;
            }
        }
    }
}

void threadblock_write_output_static(uint elem_idx)
{
    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint col = idx % hidden_width;
    const uint row = idx / hidden_width;

    barrier();

    [[unroll]]
    for (int i = 0; i < n_iters; ++i)
    {
        activations_buffer[(elem_idx * hidden_width + col + (row + 16 * i) * hidden_width) / input_elements_per_vec4] = shared_memory[(col + (row + 16 * i) * (hidden_width + skew)) / input_elements_per_vec4];
    }
}

void threadblock_first_layer_backward()
{
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseA> act_frag;
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseB> weights_frag[n_blocks];
    coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result_frag[n_iters];

    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint col = idx % hidden_width;
    const uint row = idx / hidden_width;
    const uint weights_col = 16 * wi;

    barrier();

    // Load n_blocks chunks of weights from global memory into registers.
    [[unroll]]
    for (uint i = 0; i < n_blocks; ++i)
    {
        const uint weights_row = 16 * i;
        // If we're performing the backward pass, additional index swizzling is needed to
        // load the weights in transposed form.
        coopMatLoad(weights_frag[i], weights_buffer, (weights_row * hidden_width + weights_col) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
    }

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        result_frag[l] = coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(0.0f);

        [[unroll]]
        for (uint i = 0; i < n_blocks; ++i)
        {
            // Load a chunk of intermediate activations from shared memory and multiply with chunk of weights
            coopMatLoad(act_frag, shared_memory, (16 * i + 16 * l * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
            result_frag[l] = coopMatMulAdd(act_frag, weights_frag[i], result_frag[l]);
        }
    }

    barrier();

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result = coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(result_frag[l]);
        coopMatStore(result, shared_memory, (weights_col + l * 16 * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
    }

    barrier();

    [[unroll]]
    for (int l = 0; l < n_iters; ++l)
    {
        dloss_dinput_buffer[(col + (row + 16 * l) * hidden_width) / input_elements_per_vec4] = shared_memory[(col + (row + 16 * l) * (hidden_width + skew)) / input_elements_per_vec4];
    }
}

void threadblock_last_layer_forward(uint elem_idx)
{
    // Fragments
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseA> act_frag;
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseB> weights_frag[n_blocks];
    coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result_frag;

    // Indices
    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint weights_idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint weights_row = weights_idx % hidden_width;
    const uint weights_col = weights_idx / hidden_width;
    const uint weights_shared_memory_offset = n_iters * 16 * (hidden_width + skew);

    // Load weight matrix into shared memory for the last multiplication.
    // Loading into shared memory as opposed to directly into registers is faster
    // because unlike in the previous layers, each warp uses the same entries of the weight matrix.
    shared_memory[(weights_shared_memory_offset + weights_row + weights_col * (hidden_width + skew)) / input_elements_per_vec4] = weights_buffer[(input_weights_stride + hidden_weights_stride * hidden_matmul_count + weights_row + weights_col * hidden_width) / input_elements_per_vec4];

    barrier();

    [[unroll]]
    for (uint i = 0; i < n_blocks; ++i)
    {
        coopMatLoad(weights_frag[i], shared_memory, (weights_shared_memory_offset + 16 * i) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutColumnMajor);
    }

    [[unroll]]
    // Perform last layer by parallelizing over iters
    for (uint l = wi; l < n_iters; l += n_blocks)
    {
        result_frag = coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(0.0f);

        [[unroll]]
        for (uint i = 0; i < n_blocks; ++i)
        {
            // Load a chunk of intermediate activations from shared memory and multiply with chunk of the weight matrix
            coopMatLoad(act_frag, shared_memory, (16 * i + 16 * l * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
            result_frag = coopMatMulAdd(act_frag, weights_frag[i], result_frag);
        }

        warp_activation(result_frag, output_activation);

        const uint output_offset = output_layout == gl_CooperativeMatrixLayoutRowMajor ? (elem_idx + l * 16) * output_stride : elem_idx + l * 16;
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result = coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(result_frag);
        coopMatStore(result, output_buffer, output_offset, output_stride, output_layout);
    }
}

void threadblock_last_layer_backward(uint elem_idx)
{
    // Fragments
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseA> act_frag;
    coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseB> weights_frag;
    coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result_frag[n_iters];

    // Indices
    const uint li = gl_LocalInvocationID.x; // index in warp ("lane index")
    const uint wi = gl_LocalInvocationID.y; // index in block ("warp index")
    const uint idx = (li + 32 * wi) * input_elements_per_vec4;
    const uint col = idx % hidden_width;
    const uint row = idx / hidden_width;
    // Load the relevant chunk of the last layer's weight matrix from global memory into registers
    const uint weights_col = 16 * wi;
    const uint hidden_layer_stride = hidden_width * max_batch_size;

    coopMatLoad(weights_frag, weights_buffer, (input_weights_stride + hidden_weights_stride * hidden_matmul_count + weights_col) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        result_frag[l] = coopmat<accumulator_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(0.0f);

		// Load a chunk of output gradients and multiply with previously loaded weights
        const uint output_offset = output_layout == gl_CooperativeMatrixLayoutRowMajor ? (elem_idx + l * 16) * output_stride : elem_idx + l * 16;
        coopMatLoad(act_frag, dloss_doutput_buffer, output_offset, output_stride, output_layout);

        // NOTE: activation transfer of the _output_ activation is expected to be done _prior_ to calling this kernel
        //       in a separate pass, because the tranfered activation gradient is also needed to compute the weight
        //       gradient of the last weight matrix (see backward()).
        result_frag[l] = coopMatMulAdd(act_frag, weights_frag, result_frag[l]);

        // Load the temporary forward matrix for the transfer function
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> forward_frag;
        coopMatLoad(forward_frag, activations_buffer, (hidden_layer_stride * hidden_matmul_count + weights_col + (elem_idx + l * 16) * hidden_width) / input_elements_per_vec4, hidden_width / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);

        warp_activation_backward(result_frag[l], forward_frag, hidden_activation);
    }

    barrier();

    [[unroll]]
    for (uint l = 0; l < n_iters; ++l)
    {
        coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator> result = coopmat<parameter_t, gl_ScopeSubgroup, 16, 16, gl_MatrixUseAccumulator>(result_frag[l]);
        coopMatStore(result, shared_memory, (weights_col + l * 16 * (hidden_width + skew)) / input_elements_per_vec4, (hidden_width + skew) / input_elements_per_vec4, gl_CooperativeMatrixLayoutRowMajor);
    }

    barrier();

    [[unroll]]
    for (int l = 0; l < n_iters; ++l)
    {
        activations_gradient_buffer[(col + (row + elem_idx + 16 * l) * hidden_width) / input_elements_per_vec4] = shared_memory[(col + (row + 16 * l) * (hidden_width + skew)) / input_elements_per_vec4];
    }
}

void forward_main()
{
    const uint bi = gl_WorkGroupID.x; // block index
    // Each block computes n_iters 16-element chunks of the batch.
    const uint elem_idx = 16 * bi * n_iters;
    const uint hidden_layer_stride = hidden_width * max_batch_size;

    if (input_width == hidden_width && input_layout == gl_CooperativeMatrixLayoutRowMajor)
    {
        // If the input has the same width & layout as the hidden layers, we can simply use the network's regular layer routine (with static size)
        // instead of using the slower dynamic input layer routine.
        threadblock_load_input_static(elem_idx * hidden_width);
        threadblock_layer(0, elem_idx * hidden_width, 0);
    }
    else
    {
        threadblock_input_layer_forward_dynamic(elem_idx);
    }

    // Hidden layers
    for (uint i = 0; i < hidden_matmul_count; i++)
    {
        threadblock_layer(input_weights_stride + hidden_weights_stride * i, hidden_layer_stride * (i + 1) + elem_idx * hidden_width, 0);
    }

    if (output_width > 16)
    {
        // In the forward pass, intermediate activations are already written out.
        if (inference)
        {
            threadblock_write_output_static(elem_idx);
        }
    }
    else
    {
        // Last layer
        threadblock_last_layer_forward(elem_idx);
    }
}

void backward_main()
{
    const uint bi = gl_WorkGroupID.x; // block index
    // Multipying one 16-row chunk of intermediate activations with the weight matrix requires all warps of the block.
    // Thus, each block computes exactly one 16-row chunk of the next layer's intermediate activations.
    const uint elem_idx = 16 * n_iters * bi;
    const uint hidden_layer_stride = hidden_width * max_batch_size;

    // Backprop through last layer
    if (output_width > 16)
    {
        // If the output width is larger than 16, we will have used separarte pass for backpropping through the last layer.
        // Load the resulting gradients.
        threadblock_load_input_static(elem_idx * hidden_width);
    }
    else
    {
        threadblock_last_layer_backward(elem_idx);
    }

    // Backprop through hidden layers
    for (uint i = 0; i < hidden_matmul_count; ++i)
    {
        threadblock_layer(input_weights_stride + hidden_weights_stride * (hidden_matmul_count - i - 1), hidden_layer_stride * (i + 1) + elem_idx * hidden_width, hidden_layer_stride * (hidden_matmul_count - 1 - i) + elem_idx * hidden_width);
    }

    // Compute loss gradients w.r.t. input if desired.
    if (compute_input_gradient)
    {
        threadblock_first_layer_backward();
    }
}
