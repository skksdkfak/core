inline ::std::uint32_t(::framework::nn::fully_fused_mlp::execution_policy::get_dispatch_indirect_command_count)() const noexcept
{
	return 13u;
}

inline ::framework::gpu::descriptor_set_layout * ::framework::nn::fully_fused_mlp::execution_policy::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set_layout * ::framework::nn::fully_fused_mlp::execution_policy::get_initialize_dispatch_indirect_command_descriptor_set_layout() const noexcept
{
	return this->initialize_dispatch_indirect_command_descriptor_set_layout;
}

inline ::std::uint32_t(::framework::nn::fully_fused_mlp::execution_policy::get_parameter_count)() const noexcept
{
	return this->parameter_count;
}

inline ::framework::gpu::buffer * ::framework::nn::fully_fused_mlp::network::get_weight_matrices_buffer() const noexcept
{
	return this->weight_matrices_buffer;
}

inline ::framework::gpu::buffer * ::framework::nn::fully_fused_mlp::network::get_full_precision_weights_matrices_buffer() const noexcept
{
	return this->full_precision_weights_matrices_buffer;
}

inline ::framework::gpu::buffer * ::framework::nn::fully_fused_mlp::network::get_gradient_matrices_buffer() const noexcept
{
	return this->gradient_matrices_buffer;
}

inline ::framework::gpu::buffer * ::framework::nn::fully_fused_mlp::network::get_dloss_doutput_buffer() const noexcept
{
	return this->dloss_doutput_buffer;
}

inline ::framework::gpu::buffer * ::framework::nn::fully_fused_mlp::network::get_output_buffer() const noexcept
{
	return this->output_buffer;
}
