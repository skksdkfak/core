#include "nn/activation.hlsl"

[[vk::constant_id(1)]] const uint output_width = 1;
[[vk::constant_id(15)]] const uint output_activation = 0;

[[vk::binding(0, 0)]] cbuffer batch_size_buffer_t : register(b0, space0)
{
	uint batch_size;
};

[[vk::binding(3, 0)]] RWStructuredBuffer<parameter_t> output_buffer : register(u1, space0);

[[vk::binding(5, 0)]] RWStructuredBuffer<parameter_t> dloss_doutput_buffer : register(u2, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint global_invocation_id = dispatch_thread_id.x;
	const uint output_layer_stride = output_width * batch_size;
	if (global_invocation_id >= output_layer_stride)
	{
		return;
	}

	activation_backward(dloss_doutput_buffer[global_invocation_id], output_buffer[global_invocation_id], output_activation);
}