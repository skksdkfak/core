#define gradient_t ACCUMULATOR_TYPE
#define activation_input_t C_TYPE
#define gradient_coopmat_t coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>
#define activation_input_coopmat_t coopmat<C_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>

#include "nn/activation.glsl"

bool is_source_needed()
{
    return true;
}

coopmat<D_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> gemm_epilogue(coopmat<ACCUMULATOR_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> accumulator, coopmat<C_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator> source)
{
    warp_activation_backward(accumulator, source, activation);

    return coopmat<D_TYPE, gl_ScopeSubgroup, l_m, l_n, gl_MatrixUseAccumulator>(accumulator);
}