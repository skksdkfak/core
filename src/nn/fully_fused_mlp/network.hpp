#pragma once

#include "algorithm/gemm.hpp"
#include "algorithm/xavier_uniform.hpp"
#include "command_context.hpp"
#include "coroutine/task.hpp"
#include "gpu/core.hpp"
#include "linalg.hpp"
#include "nn/activation.hpp"
#include "nn/network.hpp"
#include <cstdint>
#include <vector>

namespace framework
{
	class gpu_context;
	class gpu_log;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::nn::fully_fused_mlp
{
	class network;

	class execution_policy
	{
	public:
		execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::framework::linalg::matrix_layout input_layout, ::framework::linalg::matrix_layout output_layout, ::std::uint32_t input_width, ::std::uint32_t output_width, ::std::uint32_t padded_input_width, ::std::uint32_t padded_output_width, ::std::uint32_t hidden_width, ::std::uint32_t hidden_layer_count, ::framework::nn::activation hidden_activation, ::framework::nn::activation output_activation, ::std::uint32_t batch_alignment, ::std::uint32_t inference_batch_alignment, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, bool compute_dloss_dinput);

		void initialize_dispatch_indirect_command(::framework::gpu_context * gpu_context, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, bool inference);

		void update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		::framework::coroutine::immediate_task<void> update_parameters(::framework::gpu_context * gpu_context, ::framework::command_context * command_context, ::std::uint32_t batch_size, ::std::uint32_t inference_batch_size);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		::framework::gpu::descriptor_set_layout * get_initialize_dispatch_indirect_command_descriptor_set_layout() const noexcept;

		::std::uint32_t get_parameter_count() const noexcept;

		::std::uint32_t get_dispatch_indirect_command_count() const noexcept;

	private:
		friend class ::framework::nn::fully_fused_mlp::network;

		::std::uint32_t hidden_matmul_count;
		::std::uint32_t input_width;
		::std::uint32_t output_width;
		::std::uint32_t padded_input_width;
		::std::uint32_t padded_output_width;
		::std::uint32_t hidden_width;
		::framework::nn::activation output_activation;
		::std::uint32_t parameter_count;
		::framework::gpu_log * gpu_log;
		::framework::algorithm::gemm gemm_linear_combination_epilogue;
		::framework::algorithm::gemm gemm_hidden_activation_backward_epilogue;
		::framework::algorithm::gemm::executor inference_output_matmul;
		::framework::algorithm::gemm::executor forward_output_matmul;
		::framework::algorithm::gemm::executor backward_output_weights_gradient_matmul;
		::framework::algorithm::gemm::reduce_split_k_executor backward_output_weights_gradient_reduce_split_k;
		::framework::algorithm::gemm::executor backward_hidden_weights_gradient_matmul;
		::framework::algorithm::gemm::reduce_split_k_executor backward_hidden_weights_gradient_reduce_split_k;
		::framework::algorithm::gemm::executor backward_input_weights_gradient_matmul;
		::framework::algorithm::gemm::reduce_split_k_executor backward_input_weights_gradient_reduce_split_k;
		::framework::algorithm::gemm::executor backward_output_layer_gradient_matmul;
		::framework::algorithm::gemm::executor backward_input_layer_gradient_matmul;
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * initialize_dispatch_indirect_command_descriptor_set_layout;
		::framework::gpu::descriptor_set_layout * gpu_log_descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::pipeline_layout * initialize_dispatch_indirect_command_pipeline_layout;
		::framework::gpu::shader_module * initialize_dispatch_indirect_command_shader_module;
		::framework::gpu::shader_module * inference_shader_module;
		::framework::gpu::shader_module * forward_shader_module;
		::framework::gpu::shader_module * backward_shader_module;
		::framework::gpu::shader_module * backward_output_activation_shader_module;
		::framework::gpu::pipeline * initialize_dispatch_indirect_command_pipeline;
		::framework::gpu::pipeline * inference_pipeline;
		::framework::gpu::pipeline * forward_pipeline;
		::framework::gpu::pipeline * backward_pipeline;
		::framework::gpu::pipeline * backward_output_activation_pipeline;
		::framework::gpu::buffer * gemm_parameters_buffer;
		::framework::gpu::device_memory * gemm_parameters_device_memory;
		::framework::gpu::buffer * dispatch_indirect_command_buffer;
		::framework::gpu::device_memory * dispatch_indirect_command_device_memory;
		::framework::gpu::buffer * split_k_slices_buffer;
		::framework::gpu::device_memory * split_k_slices_device_memory;
		::std::uint32_t batch_size_buffer_alignment;
		bool compute_dloss_dinput_non_fused;
	};

	class network : public ::framework::nn::network
	{
	public:
		struct device_parameters
		{
			::std::uint32_t batch_size;
		};

		network(::framework::nn::fully_fused_mlp::execution_policy const & execution_policy, ::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::descriptor_pool * descriptor_pool);

		void update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void initialize_xavier_uniform(::framework::gpu::command_buffer * command_buffer, ::framework::algorithm::xavier_uniform * xavier_uniform, ::framework::gpu::descriptor_set * descriptor_set);

		void inference(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size);

		void inference_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size);

		void forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size);
		
		void backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		::framework::gpu::buffer * get_weight_matrices_buffer() const noexcept;

		::framework::gpu::buffer * get_full_precision_weights_matrices_buffer() const noexcept;

		::framework::gpu::buffer * get_gradient_matrices_buffer() const noexcept;

		::framework::gpu::buffer * get_dloss_doutput_buffer() const noexcept;

		::framework::gpu::buffer * get_output_buffer() const noexcept;

	private:
		::framework::nn::fully_fused_mlp::execution_policy const & execution_policy;
		::std::vector<::framework::gpu::descriptor_set *> gemm_descriptor_sets;
		::framework::gpu::descriptor_set * gpu_log_descriptor_set;
		::framework::gpu::buffer * weight_matrices_buffer;
		::framework::gpu::device_memory * weight_matrices_device_memory;
		::framework::gpu::buffer * full_precision_weights_matrices_buffer;
		::framework::gpu::device_memory * full_precision_weights_matrices_device_memory;
		::framework::gpu::buffer * output_buffer;
		::framework::gpu::device_memory * output_device_memory;
		::framework::gpu::buffer * activations_buffer;
		::framework::gpu::device_memory * activations_device_memory;
		::framework::gpu::buffer * gradient_matrices_buffer;
		::framework::gpu::device_memory * gradient_matrices_device_memory;
		::framework::gpu::buffer * dloss_doutput_buffer;
		::framework::gpu::device_memory * output_layer_gradient_device_memory;
		::framework::gpu::buffer * activations_gradient_buffer;
		::framework::gpu::device_memory * activations_gradient_device_memory;
	};
}

#include "nn/fully_fused_mlp/network.inl"