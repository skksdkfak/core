#include "algorithm/gemm.hlsl"
#include "gpu/gpu.hlsl"

[[vk::constant_id(0)]] const uint input_width = 1;
[[vk::constant_id(1)]] const uint output_width = 1;
[[vk::constant_id(2)]] const uint hidden_width = 1;

struct push_constants_t
{
	uint32_t gemm_device_parameters_buffer_alignment;
	uint32_t batch_size_stride;
	bool inference;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] ByteAddressBuffer batch_size_buffer : register(t0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u0, space0);

[[vk::binding(2, 0)]] RWByteAddressBuffer gemm_parameters_buffer : register(u1, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint n_iters = hidden_width >= 256 ? 2 : 8;
	const uint n_elems_per_block = 16 * n_iters;
	const uint min_tile_size = 1u;
	const uint max_tile_size = 256u;
	const uint output_width_tile_size = min(output_width, max_tile_size);

	if (push_constants.inference)
	{
		const uint inference_batch_size = batch_size_buffer.Load(0);
		const uint inference_batch_tile_size = clamp(inference_batch_size, min_tile_size, max_tile_size);

		// inference_fully_fused_mlp
		dispatch_indirect_command_buffer[0] = ::framework::gpu::dispatch_indirect_command::create((inference_batch_size + n_elems_per_block - 1) / n_elems_per_block, 1, 1);
		// inference_output_matmul
		dispatch_indirect_command_buffer[1] = ::framework::gpu::dispatch_indirect_command::create(inference_batch_size / inference_batch_tile_size, output_width / output_width_tile_size, 1);

		// inference_output_matmul
		gemm_parameters_buffer.Store(0u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(output_width, inference_batch_size, hidden_width, 1, 1.0f, 0.0f));
	}
	else
	{
		const uint batch_size = batch_size_buffer.Load(push_constants.batch_size_stride);
		const uint split_k_slices = batch_size ? batch_size / min(batch_size, 1u << 12) : 0u;
		const uint output_layer_stride = output_width * batch_size;
		const uint input_width_tile_size = min(input_width, max_tile_size);
		const uint hidden_width_tile_size = min(hidden_width, max_tile_size);
		const uint batch_tile_size = clamp(batch_size, min_tile_size, max_tile_size);

		// forward_fully_fused_mlp
		dispatch_indirect_command_buffer[2] = ::framework::gpu::dispatch_indirect_command::create((batch_size + n_elems_per_block - 1) / n_elems_per_block, 1, 1);
		// forward_output_matmul
		dispatch_indirect_command_buffer[3] = ::framework::gpu::dispatch_indirect_command::create(batch_size / batch_tile_size, output_width / output_width_tile_size, 1);
		// backward_output_activation
		dispatch_indirect_command_buffer[4] = ::framework::gpu::dispatch_indirect_command::create((output_layer_stride + 128 - 1) / 128, 1, 1);
		// backward_output_weights_gradient_matmul
		dispatch_indirect_command_buffer[5] = ::framework::gpu::dispatch_indirect_command::create(hidden_width / hidden_width_tile_size, output_width / output_width_tile_size, split_k_slices);
		// backward_output_weights_gradient_matmul reduce_split_k
		dispatch_indirect_command_buffer[6] = ::framework::gpu::dispatch_indirect_command::create(hidden_width / 16, output_width / 16, 1);
		// backward_output_layer_gradient_matmul
		dispatch_indirect_command_buffer[7] = ::framework::gpu::dispatch_indirect_command::create(batch_size / batch_tile_size, hidden_width / hidden_width_tile_size, 1);
		// backward_hidden_weights_gradient_matmul
		dispatch_indirect_command_buffer[8] = ::framework::gpu::dispatch_indirect_command::create(hidden_width / hidden_width_tile_size, hidden_width / hidden_width_tile_size, split_k_slices);
		// backward_hidden_weights_gradient_matmul reduce_split_k
		dispatch_indirect_command_buffer[9] = ::framework::gpu::dispatch_indirect_command::create(hidden_width / 16, hidden_width / 16, 1);
		// backward_input_weights_gradient_matmul
		dispatch_indirect_command_buffer[10] = ::framework::gpu::dispatch_indirect_command::create(input_width / input_width_tile_size, hidden_width / hidden_width_tile_size, split_k_slices);
		// backward_input_weights_gradient_matmul reduce_split_k
		dispatch_indirect_command_buffer[11] = ::framework::gpu::dispatch_indirect_command::create(input_width / 16, hidden_width / 16, 1);
		// backward_input_layer_gradient_matmul
		dispatch_indirect_command_buffer[12] = ::framework::gpu::dispatch_indirect_command::create(batch_size / batch_tile_size, input_width / input_width_tile_size, 1);

		// forward_output_matmul
		gemm_parameters_buffer.Store(1u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(output_width, batch_size, hidden_width, 1, 1.0f, 0.0f));
		// backward_output_weights_gradient_matmul
		gemm_parameters_buffer.Store(2u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(output_width, hidden_width, batch_size, split_k_slices, 1.0f, 0.0f));
		// backward_output_layer_gradient_matmul
		gemm_parameters_buffer.Store(3u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(hidden_width, batch_size, output_width, 1, 1.0f, 0.0f));
		// backward_hidden_weights_gradient_matmul
		gemm_parameters_buffer.Store(4u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(hidden_width, hidden_width, batch_size, split_k_slices, 1.0f, 0.0f));
		// backward_input_weights_gradient_matmul
		gemm_parameters_buffer.Store(5u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(hidden_width, input_width, batch_size, split_k_slices, 1.0f, 0.0f));
		// backward_input_layer_gradient_matmul
		gemm_parameters_buffer.Store(6u * push_constants.gemm_device_parameters_buffer_alignment, ::framework::algorithm::gemm::get_device_parameters(input_width, batch_size, hidden_width, 1, 1.0f, 0.0f));
	}
}
