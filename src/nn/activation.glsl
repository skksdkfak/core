const uint activation_function_none = 0u;
const uint activation_function_relu = 1u;
const uint activation_function_relu6 = 2u;
const uint activation_function_leaky_relu = 3u;
const uint activation_function_sigmoid = 4u;
const uint activation_function_exponential = 5u;

activation_input_t relu(activation_input_t x)
{
    return max(x, activation_input_t(0.0));
}

activation_input_t relu6(activation_input_t x)
{
    return min(max(x, activation_input_t(0.0)), activation_input_t(6.0));
}

activation_input_t leaky_relu(activation_input_t x)
{
    return x > activation_input_t(0.0) ? x : x * activation_input_t(0.01);
}

activation_input_t logistic(activation_input_t x)
{
    return activation_input_t(1.0f / (1.0f + exp(-x)));
}

gradient_t relu_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0));
}

gradient_t relu6_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0) && x < activation_input_t(6.0));
}

gradient_t leaky_relu_derivative(activation_input_t x)
{
    return gradient_t(x > activation_input_t(0.0) ? 1.0 : 0.01);
}

gradient_t logistic_derivative(activation_input_t x)
{
    return gradient_t(x * activation_input_t(1.0f - float(x)));
}

void warp_activation(inout activation_input_coopmat_t activation_input, uint activation_function)
{
    for (int t = 0; t < activation_input.length(); t++)
    {
        switch (activation_function)
        {
        case activation_function_relu:
            activation_input[t] = relu(activation_input[t]);
            break;
        case activation_function_relu6:
            activation_input[t] = relu6(activation_input[t]);
            break;
        case activation_function_leaky_relu:
            activation_input[t] = leaky_relu(activation_input[t]);
            break;
        case activation_function_sigmoid:
            activation_input[t] = logistic(activation_input[t]);
            break;
        case activation_function_exponential:
            activation_input[t] = exp(activation_input[t]);
            break;
        default:
            break;
        }
    }
}

void warp_activation_backward(inout gradient_coopmat_t gradient, in activation_input_coopmat_t activation_input, uint activation_function)
{
    for (int t = 0; t < gradient.length(); t++)
    {
        switch (activation_function)
        {
        case activation_function_relu:
            gradient[t] *= relu_derivative(activation_input[t]);
            break;
        case activation_function_relu6:
            gradient[t] *= relu6_derivative(activation_input[t]);
            break;
        case activation_function_leaky_relu:
            gradient[t] *= leaky_relu_derivative(activation_input[t]);
            break;
        case activation_function_sigmoid:
            gradient[t] *= logistic_derivative(activation_input[t]);
            break;
        case activation_function_exponential:
            gradient[t] *= activation_input[t];
            break;
        default:
            break;
        }
    }
}