#pragma once

#include "gpu/core.hpp"
#include <cstdint>

namespace framework::nn
{
	class loss
	{
	public:
		virtual void evaluate(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t size) = 0;

		virtual void evaluate_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) = 0;
	};
}