#include "pcg32_sample_generator.hlsl"

namespace framework
{
	enum class grid_type : uint32_t
	{
		hash,
		dense,
		tiled
	};

	enum class hash_type : uint32_t
	{
		prime,
		coherent_prime,
		reversed_prime,
		rng
	};

	enum class interpolation_type : uint32_t
	{
		nearest,
		linear_,
		smoothstep
	};

	inline float smoothstep(float val)
	{
		return val * val * (3.0f - 2.0f * val);
	}

	inline float smoothstep_derivative(float val)
	{
		return 6 * val * (1.0f - val);
	}

	inline float smoothstep_2nd_derivative(float val)
	{
		return 6.0f - 12.0f * val;
	}

	inline float identity_fun(float val)
	{
		return val;
	}

	inline float identity_derivative(float val)
	{
		return 1.0f;
	}

	inline float identity_2nd_derivative(float val)
	{
		return 0.0f;
	}

	inline float grid_scale(uint32_t level, float log2_per_level_scale, uint32_t base_resolution)
	{
		// The -1 means that `base_resolution` refers to the number of grid _vertices_ rather
		// than the number of cells. This is slightly different from the notation in the paper,
		// but results in nice, power-of-2-scaled parameter grids that fit better into cache lines.
		return exp2(level * log2_per_level_scale) * base_resolution - 1.0f;
	}

	inline uint32_t grid_resolution(float scale)
	{
		return (uint32_t)ceil(scale) + 1;
	}

	template<uint32_t n_dims, uint32_t n_primes>
	uint32_t lcg_hash(uint pos_grid[n_dims], uint32_t primes[n_primes])
	{
		uint32_t result = 0;

		[unroll] for (uint32_t i = 0; i < n_dims; ++i)
		{
			result ^= pos_grid[i] * primes[i];
		}

		return result;
	}

	template<uint32_t n_dims>
	uint32_t prime_hash(uint pos_grid[n_dims])
	{
		const uint32_t factors[7] = { 1958374283u, 2654435761u, 805459861u, 3674653429u, 2097192037u, 1434869437u, 2165219737u };
		return ::framework::lcg_hash<n_dims, 7>(pos_grid, factors);
	}

	template<uint32_t n_dims>
	uint32_t coherent_prime_hash(uint pos_grid[n_dims])
	{
		const uint32_t factors[7] = { 1u, 2654435761u, 805459861u, 3674653429u, 2097192037u, 1434869437u, 2165219737u };
		return ::framework::lcg_hash<n_dims, 7>(pos_grid, factors);
	}

	template<uint32_t n_dims>
	uint32_t reversed_prime_hash(uint pos_grid[n_dims])
	{
		const uint32_t factors[7] = { 2165219737u, 1434869437u, 2097192037u, 3674653429u, 805459861u, 2654435761u, 1958374283u };
		return ::framework::lcg_hash<n_dims, 7>(pos_grid, factors);
	}

	template<uint32_t n_dims>
	uint32_t rng_hash(uint pos_grid[n_dims], const uint32_t seed = 1337)
	{
		const uint32_t N_BITS_PER_DIM = 64 / n_dims;
		uint64_t step = 0;

		[unroll] for (uint32_t i = 0; i < n_dims; ++i)
		{
			step ^= (uint64_t)pos_grid[i] << (i * N_BITS_PER_DIM);
		}

		::framework::pcg32_sample_generator sample_generator = ::framework::pcg32_sample_generator::create(seed);
		sample_generator.advance((int64_t)step);
		return sample_generator.next();
	}

	template <uint32_t n_dims>
	uint32_t grid_hash(::framework::hash_type hash_type, uint pos_grid[n_dims])
	{
		switch (hash_type)
		{
		case ::framework::hash_type::prime:
			return ::framework::prime_hash<n_dims>(pos_grid);
		case ::framework::hash_type::coherent_prime:
			return ::framework::coherent_prime_hash<n_dims>(pos_grid);
		case ::framework::hash_type::reversed_prime:
			return ::framework::reversed_prime_hash<n_dims>(pos_grid);
		case ::framework::hash_type::rng:
			return ::framework::rng_hash<n_dims>(pos_grid);
		}
	}

	template <uint32_t n_dims>
	uint32_t grid_index(::framework::hash_type hash_type, ::framework::grid_type grid_type, uint32_t hashmap_size, uint32_t grid_resolution, uint pos_grid[n_dims])
	{
		uint32_t stride = 1;
		uint32_t index = 0;

		// The second part of the loop condition is needed to avoid integer overflows in finer levels.
		[unroll] for (uint32_t dim = 0; dim < n_dims && stride <= hashmap_size; ++dim)
		{
			index += pos_grid[dim] * stride;
			stride *= grid_resolution;
		}

		if (grid_type == ::framework::grid_type::hash && hashmap_size < stride)
		{
			index = ::framework::grid_hash<n_dims>(hash_type, pos_grid);
		}

		return index % hashmap_size;
	}

	inline void pos_fract(const float input, inout float pos, inout uint32_t pos_grid, float scale)
	{
		// The offset of 0.5 causes different scales to be staggered with respect to each other, thus
		// preventing spurious alignment of fractional coordinates upon integer scales (or powers thereof).
		// This is mentioned in Appendix A of the "Instant Neural Graphics Primitives" paper.
		// The offset can cause wraparound indexing in dense grids, which didn't negatively impact
		// the approximation quality in any of our tests.
		pos = mad(scale, input, 0.5f);
		float tmp = floor(pos);
		pos_grid = (uint32_t)(int)tmp;
		pos -= tmp;
	}
}

[[vk::ext_capability(6033/* AtomicFloat32AddEXT */)]]
[[vk::ext_extension("SPV_EXT_shader_atomic_float_add")]]
[[vk::ext_instruction(6035/* OpAtomicFAddEXT */)]]
vk::ext_result_id<float32_t> InterlockedAdd([[vk::ext_reference]] float32_t Pointer, uint Memory, int Semantics, float32_t Value);

[[vk::ext_capability(6095/* AtomicFloat16AddEXT */)]]
[[vk::ext_extension("SPV_EXT_shader_atomic_float16_add")]]
[[vk::ext_instruction(6035/* OpAtomicFAddEXT */)]]
vk::ext_result_id<float16_t> InterlockedAdd([[vk::ext_reference]] float16_t Pointer, uint Memory, int Semantics, float16_t Value);

[[vk::constant_id(0)]] const uint32_t features_per_level = 1;
[[vk::constant_id(1)]] const uint32_t features_per_thread = 1;
[[vk::constant_id(2)]] const uint32_t pos_dims = 1;
[[vk::constant_id(3)]] const uint32_t hash_type = 0;
[[vk::constant_id(4)]] const uint32_t grid_type = 0;
[[vk::constant_id(5)]] const uint32_t interpolation_type = 0;
[[vk::constant_id(6)]] const uint32_t num_grid_features = 0;
[[vk::constant_id(7)]] const uint32_t base_resolution = 0;
[[vk::constant_id(8)]] const float log2_per_level_scale = 0.0f;
[[vk::constant_id(9)]] const float max_level = 0.0f;
[[vk::constant_id(10)]] const bool output_encoded_positions = false;
[[vk::constant_id(11)]] const bool use_max_level_buffer = false;
[[vk::constant_id(12)]] const bool stochastic_interpolation = false;
[[vk::constant_id(13)]] const uint32_t input_stride = 1;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] StructuredBuffer<parameter_t> input_positions_buffer : register(t0, space0);

[[vk::binding(2, 0)]] StructuredBuffer<parameter_t> grid_buffer : register(t1, space0);

[[vk::binding(3, 0)]] StructuredBuffer<float> max_level_buffer : register(t2, space0);

[[vk::binding(4, 0)]] StructuredBuffer<uint32_t> offset_buffer : register(t3, space0);

[[vk::binding(5, 0)]] StructuredBuffer<parameter_t> dloss_dy_buffer : register(t4, space0);

[[vk::binding(6, 0)]] RWStructuredBuffer<gradient_t> grid_gradient_buffer : register(u0, space0);

[[vk::binding(7, 0)]] RWStructuredBuffer<gradient_t> dy_dx_buffer : register(u1, space0);

[[vk::binding(8, 0)]] RWStructuredBuffer<parameter_t> encoded_positions_buffer : register(u2, space0);

[numthreads(512, 1, 1)]
[shader("compute")]
void forward_main(uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint32_t i = group_id.x * 512 + group_thread_id.x;
	if (i >= size)
	{
		return;
	}

	const uint32_t level = group_id.y;
	float current_max_level;
	if (use_max_level_buffer)
	{
		current_max_level = (max_level_buffer[i] * num_grid_features) / features_per_level;
	}
	else
	{
		current_max_level = (max_level * num_grid_features) / features_per_level;
	}

	if (level >= current_max_level + 1e-3f)
	{
		if (output_encoded_positions)
		{
			[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
			{
				encoded_positions_buffer[i + (level * features_per_level + f) * size] = parameter_t(0.0);
			}
		}

		// Gradient is zero for zeroed-out dimensions.
		//if (dy_dx)
		//{
		//	[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
		//	{
		//		((vec<pos_dims>*)dy_dx)[i + (level * features_per_level + f) * size] = { 0.0f };
		//	}
		//}

		return;
	}

	const uint32_t grid_offset = offset_buffer[level] * features_per_level;
	const uint32_t hashmap_size = offset_buffer[level + 1] - offset_buffer[level];

	const float scale = ::framework::grid_scale(level, log2_per_level_scale, base_resolution);
	const uint32_t resolution = ::framework::grid_resolution(scale);

	float pos[pos_dims_define];
	float pos_derivative[pos_dims_define];
	uint32_t pos_grid[pos_dims_define];

	if ((::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::nearest || (::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::linear_)
	{
		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			::framework::pos_fract(input_positions_buffer[i * input_stride + dim], pos[dim], pos_grid[dim], scale);
			pos_derivative[dim] = ::framework::identity_derivative(pos[dim]);
			pos[dim] = ::framework::identity_fun(pos[dim]);
		}
	}
	else
	{
		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			::framework::pos_fract(input_positions_buffer[i * input_stride + dim], pos[dim], pos_grid[dim], scale);
			pos_derivative[dim] = ::framework::smoothstep_derivative(pos[dim]);
			pos[dim] = ::framework::smoothstep(pos[dim]);
		}
	}

	if ((::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::nearest)
	{
		if (output_encoded_positions)
		{
			const uint32_t grid_index = ::framework::grid_index<pos_dims_define>((::framework::hash_type)hash_type, (::framework::grid_type)grid_type, hashmap_size, resolution, pos_grid) * features_per_level;

			[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
			{
				encoded_positions_buffer[i + (level * features_per_level + f) * size] = grid_buffer[grid_offset + grid_index + f];
			}
		}

		// Gradient is zero when there's no interpolation.
		//if (dy_dx)
		//{
		//	[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
		//	{
		//		((vec<pos_dims>*)dy_dx)[i + (level * features_per_level + f) * size] = { 0.0f };
		//	}
		//}

		return;
	}

	if (output_encoded_positions)
	{
		// N-linear interpolation
		parameter_t result[features_per_level_define];
		[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
		{
			result[f] = parameter_t(0.0);
		}

		[unroll] for (uint32_t idx = 0; idx < (1u << pos_dims); ++idx)
		{
			float weight = 1;
			uint32_t pos_grid_local[pos_dims_define];

			[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
			{
				if ((idx & (1u << dim)) == 0)
				{
					weight *= 1 - pos[dim];
					pos_grid_local[dim] = pos_grid[dim];
				}
				else
				{
					weight *= pos[dim];
					pos_grid_local[dim] = pos_grid[dim] + 1;
				}
			}

			const uint32_t grid_index = ::framework::grid_index<pos_dims_define>((::framework::hash_type)hash_type, (::framework::grid_type)grid_type, hashmap_size, resolution, pos_grid_local) * features_per_level;
			[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
			{
				result[f] = mad(parameter_t(weight), grid_buffer[grid_offset + grid_index + f], result[f]);
			}
		}

		[unroll] for (uint32_t f = 0; f < features_per_level; ++f)
		{
			encoded_positions_buffer[i + (level * features_per_level + f) * size] = result[f];
		}
	}
}

[numthreads(256, 1, 1)]
[shader("compute")]
void backward_main(uint3 group_thread_id : SV_GroupThreadID, uint3 group_id : SV_GroupID)
{
	const uint32_t i = ((group_id.x * 256 + group_thread_id.x) * features_per_thread) / features_per_level;
	if (i >= size)
	{
		return;
	}

	const uint32_t level = group_id.y;
	const uint32_t feature = (group_id.x * 256 + group_thread_id.x) * features_per_thread - i * features_per_level;

	float current_max_level;
	if (use_max_level_buffer)
	{
		current_max_level = (max_level_buffer[i] * num_grid_features) / features_per_level;
	}
	else
	{
		current_max_level = (max_level * num_grid_features) / features_per_level;
	}

	if (level >= current_max_level + 1e-3f)
	{
		return;
	}

	const uint32_t grid_offset = offset_buffer[level] * features_per_level;
	const uint32_t hashmap_size = offset_buffer[level + 1] - offset_buffer[level];

	const float scale = ::framework::grid_scale(level, log2_per_level_scale, base_resolution);
	const uint32_t resolution = ::framework::grid_resolution(scale);

	float pos[pos_dims_define];
	uint32_t pos_grid[pos_dims_define];

	if ((::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::nearest || (::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::linear_)
	{
		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			::framework::pos_fract(input_positions_buffer[i * input_stride + dim], pos[dim], pos_grid[dim], scale);
			pos[dim] = ::framework::identity_fun(pos[dim]);
		}
	}
	else
	{
		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			::framework::pos_fract(input_positions_buffer[i * input_stride + dim], pos[dim], pos_grid[dim], scale);
			pos[dim] = ::framework::smoothstep(pos[dim]);
		}
	}

	parameter_t grad[features_per_thread_define];

	[unroll] for (uint32_t f = 0; f < features_per_thread; ++f)
	{
		grad[f] = dloss_dy_buffer[i + (level * features_per_level + feature + f) * size];
	}

	if ((::framework::interpolation_type)interpolation_type == ::framework::interpolation_type::nearest)
	{
		const uint32_t grid_index = ::framework::grid_index<pos_dims_define>((::framework::hash_type)hash_type, (::framework::grid_type)grid_type, hashmap_size, resolution, pos_grid) * features_per_level + feature;
		[unroll] for (uint32_t f = 0; f < features_per_thread; ++f)
		{
			InterlockedAdd(grid_gradient_buffer[grid_offset + grid_index + f], vk::DeviceScope/*vk::QueueFamilyScope*/, 0x0, float(grad[f]));
		}

		return;
	}

	if (stochastic_interpolation)
	{
		::framework::pcg32_sample_generator sample_generator = ::framework::pcg32_sample_generator::create(1337);
		sample_generator.advance((int64_t)(i + level * size));
		const float sample = ::framework::sample_next_1d(sample_generator);

		uint32_t pos_grid_local[pos_dims_define];

		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			if (sample >= pos[dim])
			{
				pos_grid_local[dim] = pos_grid[dim];
			}
			else
			{
				pos_grid_local[dim] = pos_grid[dim] + 1;
			}
		}

		const uint32_t grid_index = ::framework::grid_index<pos_dims_define>((::framework::hash_type)hash_type, (::framework::grid_type)grid_type, hashmap_size, resolution, pos_grid_local) * features_per_level + feature;
		[unroll] for (uint32_t f = 0; f < features_per_thread; ++f)
		{
			InterlockedAdd(grid_gradient_buffer[grid_offset + grid_index + f], vk::DeviceScope/*vk::QueueFamilyScope*/, 0x0, grad[f]);
		}

		return;
	}

	[unroll] for (uint32_t idx = 0; idx < (1u << pos_dims); ++idx)
	{
		float weight = 1;
		uint32_t pos_grid_local[pos_dims_define];

		[unroll] for (uint32_t dim = 0; dim < pos_dims; ++dim)
		{
			if ((idx & (1u << dim)) == 0)
			{
				weight *= 1 - pos[dim];
				pos_grid_local[dim] = pos_grid[dim];
			}
			else
			{
				weight *= pos[dim];
				pos_grid_local[dim] = pos_grid[dim] + 1;
			}
		}

		const uint32_t grid_index = ::framework::grid_index<pos_dims_define>((::framework::hash_type)hash_type, (::framework::grid_type)grid_type, hashmap_size, resolution, pos_grid_local) * features_per_level + feature;
		[unroll] for (uint32_t f = 0; f < features_per_thread; ++f)
		{
			InterlockedAdd(grid_gradient_buffer[grid_offset + grid_index + f], vk::DeviceScope/*vk::QueueFamilyScope*/, 0x0, (parameter_t)weight * grad[f]);
		}
	}
}
