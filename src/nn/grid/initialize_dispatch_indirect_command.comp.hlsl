#include "gpu/gpu.hlsl"
#include "numeric.hlsl"

[[vk::constant_id(0)]] const uint32_t level_count = 1;
[[vk::constant_id(1)]] const uint32_t features_per_level = 1;
[[vk::constant_id(2)]] const uint32_t features_per_thread = 1;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u0, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	dispatch_indirect_command_buffer[0] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(size, 512u), level_count, 1);
	dispatch_indirect_command_buffer[1] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(size * features_per_level / features_per_thread, 256u), level_count, 1);
}
