#include "coroutine/sync_wait.hpp"
#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "nn/grid/encoding.hpp"
#include "numeric.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <string>
#include <vector>

namespace framework::local
{
	namespace
	{
		inline float grid_scale(::std::uint32_t level, float log2_per_level_scale, ::std::uint32_t base_resolution)
		{
			// The -1 means that `base_resolution` refers to the number of grid _vertices_ rather
			// than the number of cells. This is slightly different from the notation in the paper,
			// but results in nice, power-of-2-scaled parameter grids that fit better into cache lines.
			return ::std::exp2(level * log2_per_level_scale) * base_resolution - 1.0f;
		}

		inline ::std::uint32_t grid_resolution(float scale)
		{
			return static_cast<::std::uint32_t>(::std::ceil(scale) + 1);
		}
	}
}

::framework::nn::grid::execution_policy::execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::command_buffer_manager * command_buffer_manager, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type grid_gradient_type, ::framework::nn::grid::grid_type grid_type, ::framework::nn::grid::hash_type hash_type, ::framework::nn::grid::interpolation_type interpolation_type, ::std::uint32_t max_size, ::std::uint32_t feature_count, ::std::uint32_t log2_hashmap_size, ::std::uint32_t base_resolution, float per_level_scale, bool stochastic_interpolation, ::std::uint32_t features_per_level, ::std::uint32_t pos_dims, ::std::uint32_t input_stride) :
	level_count(::framework::numeric::div_round_up(feature_count, features_per_level)), features_per_level(features_per_level)
{
	assert(grid_gradient_type == ::framework::gpu::component_type::float32);

	::std::uint32_t const features_per_thread = ::std::min(2u, features_per_level);
	::std::uint32_t const offset_table_size = this->level_count + 1;
	::std::vector<::std::uint32_t> offset_table(offset_table_size);
	::std::uint32_t const offset = this->build_offset_table(offset_table.data(), grid_type, log2_hashmap_size, base_resolution, per_level_scale, pos_dims);
	char const * const parameter_type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);
	char const * const gradient_type_name = ::framework::gpu::utility::get_component_type_name(grid_gradient_type);
	::std::string const pos_dims_string = ::std::to_string(pos_dims);
	::std::string const features_per_level_string = ::std::to_string(features_per_level);
	::std::string const features_per_thread_string = ::std::to_string(features_per_thread);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = offset_table_size * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->offset_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->offset_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->offset_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->offset_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->offset_buffer;
		bind_buffer_memory_info.memory = this->offset_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	::framework::coroutine::sync_wait([&]() -> ::framework::coroutine::immediate_task<void>
	{
		::framework::command_context * const graphics_command_context = co_await command_buffer_manager->get_compute_queue()->acquire_command_context();
		::framework::gpu::command_buffer * const command_buffer = graphics_command_context->get_command_buffer();

		::framework::gpu::command_buffer_begin_info command_buffer_begin_info;
		command_buffer_begin_info.flags = ::framework::gpu::command_buffer_usage_flags::one_time_submit_bit;
		command_buffer_begin_info.inheritance_info = nullptr;
		assert_framework_gpu_result(command_buffer->begin_command_buffer(&command_buffer_begin_info));

		co_await graphics_command_context->update_buffer(this->offset_buffer, offset_table.data(), 0, offset_table_size * sizeof(::std::uint32_t));

		assert_framework_gpu_result(command_buffer->end_command_buffer());

		co_await command_buffer_manager->get_compute_queue()->submit_and_wait(graphics_command_context, 0, nullptr, 0, nullptr);
		co_await command_buffer_manager->get_compute_queue()->release_command_context(graphics_command_context);
	}());

	this->parameter_count = offset * features_per_level;

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[9];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// input_positions_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// grid_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// max_level_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// offset_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 3;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// dloss_dy_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 4;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;
		// grid_gradient_buffer
		descriptor_set_layout_bindings[6].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[6].binding = 6;
		descriptor_set_layout_bindings[6].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[6].hlsl_register_space = 0;
		descriptor_set_layout_bindings[6].descriptor_count = 1;
		descriptor_set_layout_bindings[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[6].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[6].immutable_samplers = nullptr;
		// dy_dx_buffer
		descriptor_set_layout_bindings[7].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[7].binding = 7;
		descriptor_set_layout_bindings[7].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[7].hlsl_register_space = 0;
		descriptor_set_layout_bindings[7].descriptor_count = 1;
		descriptor_set_layout_bindings[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[7].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[7].immutable_samplers = nullptr;
		// encoded_positions_buffer
		descriptor_set_layout_bindings[8].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[8].binding = 8;
		descriptor_set_layout_bindings[8].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[8].hlsl_register_space = 0;
		descriptor_set_layout_bindings[8].descriptor_count = 1;
		descriptor_set_layout_bindings[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[8].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[8].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// size_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// dispatch_indirect_command_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer_dynamic;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_descriptor_set_layout));
	}

	{
		{
			::framework::resource::preprocessor_define const preprocessor_defines[]
			{
				{ "parameter_t", parameter_type_name },
				{ "gradient_t", gradient_type_name },
				{ "pos_dims_define", pos_dims_string.c_str() },
				{ "features_per_level_define", features_per_level_string.c_str() },
				{ "features_per_thread_define", features_per_thread_string.c_str() },
			};

			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/grid/encoding.comp.hlsl";
			hlsl_source_info.source_entry_point = "forward_main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = "forward_main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->forward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::preprocessor_define const preprocessor_defines[]
			{
				{ "parameter_t", parameter_type_name },
				{ "gradient_t", gradient_type_name },
				{ "pos_dims_define", pos_dims_string.c_str() },
				{ "features_per_level_define", features_per_level_string.c_str() },
				{ "features_per_thread_define", features_per_thread_string.c_str() },
			};

			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/grid/encoding.comp.hlsl";
			hlsl_source_info.source_entry_point = "backward_main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = "backward_main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->backward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/grid/initialize_dispatch_indirect_command.comp.hlsl";
			hlsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = 0;
			shader_stage_info.preprocessor_defines = nullptr;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.include_override_count = 0;
			shader_stage_info.include_overrides = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->initialize_dispatch_indirect_command_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->initialize_dispatch_indirect_command_descriptor_set_layout
		};

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 0;
		pipeline_layout_create_info.push_constant_ranges = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_dispatch_indirect_command_pipeline_layout));
	}

	::framework::gpu::specialization_map_entry const specialization_map_entries[]
	{
		{0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t)},
		{1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t)},
		{2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t)},
		{3, sizeof(::std::uint32_t) * 3, sizeof(::std::uint32_t)},
		{4, sizeof(::std::uint32_t) * 4, sizeof(::std::uint32_t)},
		{5, sizeof(::std::uint32_t) * 5, sizeof(::std::uint32_t)},
		{6, sizeof(::std::uint32_t) * 6, sizeof(::std::uint32_t)},
		{7, sizeof(::std::uint32_t) * 7, sizeof(::std::uint32_t)},
		{8, sizeof(::std::uint32_t) * 8, sizeof(::std::uint32_t)},
		{9, sizeof(::std::uint32_t) * 9, sizeof(::std::uint32_t)},
		{10, sizeof(::std::uint32_t) * 10, sizeof(::std::uint32_t)},
		{11, sizeof(::std::uint32_t) * 11, sizeof(::std::uint32_t)},
		{12, sizeof(::std::uint32_t) * 12, sizeof(::std::uint32_t)},
		{13, sizeof(::std::uint32_t) * 13, sizeof(::std::uint32_t)}
	};

	::framework::gpu::specialization_map_entry const initialize_dispatch_indirect_command_specialization_map_entries[]
	{
		{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
		{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) },
		{ 2, sizeof(::std::uint32_t) * 2, sizeof(::std::uint32_t) }
	};

	::framework::gpu::bool32_t const output_encoded_positions = true;
	::framework::gpu::bool32_t const use_max_level_buffer = false;
	float const log2_per_level_scale = ::std::log2(per_level_scale);
	float const max_level = 1000.f;

	::std::uint32_t const specialization_data[]
	{
		features_per_level,
		features_per_thread,
		pos_dims,
		static_cast<::std::uint32_t>(hash_type),
		static_cast<::std::uint32_t>(grid_type),
		static_cast<::std::uint32_t>(interpolation_type),
		feature_count,
		base_resolution,
		*reinterpret_cast<::std::uint32_t const *>(&log2_per_level_scale),
		*reinterpret_cast<::std::uint32_t const *>(&max_level),
		output_encoded_positions,
		use_max_level_buffer,
		static_cast<::framework::gpu::bool32_t>(stochastic_interpolation),
		input_stride
	};

	::std::uint32_t const initialize_dispatch_indirect_command_specialization_data[]
	{
		this->level_count,
		features_per_level,
		features_per_thread
	};

	::framework::gpu::specialization_info specialization_infos[2];
	specialization_infos[0].map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
	specialization_infos[0].map_entries = specialization_map_entries;
	specialization_infos[0].data_size = sizeof(specialization_data);
	specialization_infos[0].data = specialization_data;

	specialization_infos[1].map_entry_count = static_cast<::std::uint32_t>(::std::size(initialize_dispatch_indirect_command_specialization_map_entries));
	specialization_infos[1].map_entries = initialize_dispatch_indirect_command_specialization_map_entries;
	specialization_infos[1].data_size = sizeof(initialize_dispatch_indirect_command_specialization_data);
	specialization_infos[1].data = initialize_dispatch_indirect_command_specialization_data;

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[3];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = this->forward_shader_module;
	pipeline_shader_stage_create_infos[0].name = "forward_main";
	pipeline_shader_stage_create_infos[0].specialization_info = &specialization_infos[0];

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = this->backward_shader_module;
	pipeline_shader_stage_create_infos[1].name = "backward_main";
	pipeline_shader_stage_create_infos[1].specialization_info = &specialization_infos[0];

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = this->initialize_dispatch_indirect_command_shader_module;
	pipeline_shader_stage_create_infos[2].name = "main";
	pipeline_shader_stage_create_infos[2].specialization_info = &specialization_infos[1];

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[3];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = this->pipeline_layout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = this->pipeline_layout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = this->initialize_dispatch_indirect_command_pipeline_layout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	::framework::gpu::pipeline * pipelines[::std::size(compute_pipeline_create_infos)];
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines));
	this->forward_pipeline = pipelines[0], this->backward_pipeline = pipelines[1], this->initialize_dispatch_indirect_command_pipeline = pipelines[2];
}

void ::framework::nn::grid::execution_policy::initialize_dispatch_indirect_command(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::span<::std::uint32_t const, 2> dynamic_offsets)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &descriptor_set, static_cast<::std::uint32_t>(::std::size(dynamic_offsets)), ::std::data(dynamic_offsets));
	command_buffer->dispatch(1, 1, 1);
}

void ::framework::nn::grid::execution_policy::update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	descriptor_buffer_infos[0].buffer = size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = dispatch_indirect_command_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = sizeof(::framework::gpu::dispatch_indirect_command) * 2;
	descriptor_buffer_infos[1].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	// size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// dispatch_indirect_command_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer_dynamic;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;

	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

::std::uint32_t (::framework::nn::grid::execution_policy::build_offset_table)(::std::uint32_t * offset_table, ::framework::nn::grid::grid_type grid_type, ::std::uint32_t log2_hashmap_size, ::std::uint32_t base_resolution, float per_level_scale, ::std::uint32_t pos_dims) const
{
	::std::uint32_t offset = 0u;
	for (::std::uint32_t i = 0; i < this->level_count; ++i)
	{
		// Compute number of dense params required for the given level
		const ::std::uint32_t resolution = ::framework::local::grid_resolution(::framework::local::grid_scale(i, ::std::log2(per_level_scale), base_resolution));

		::std::uint32_t constexpr max_params = ::std::numeric_limits<::std::uint32_t>::max() / 2;
		::std::uint32_t params_in_level = ::std::min(::framework::numeric::powi(resolution, pos_dims), max_params);

		// Make sure memory accesses will be aligned
		params_in_level = ::framework::numeric::next_multiple(params_in_level, 8u);

		if (grid_type == ::framework::nn::grid::grid_type::tiled)
		{
			// If tiled grid needs fewer params than dense, then use fewer and tile.
			params_in_level = ::std::min(params_in_level, ::framework::numeric::powi(base_resolution, pos_dims));
		}
		else if (grid_type == ::framework::nn::grid::grid_type::hash)
		{
			// If hash table needs fewer params than dense, then use fewer and rely on the hash.
			params_in_level = ::std::min(params_in_level, (1u << log2_hashmap_size));
		}

		offset_table[i] = offset;
		offset += params_in_level;
	}

	offset_table[this->level_count] = offset;

	return offset;
}

::framework::nn::grid::encoding::encoding(::framework::nn::encoding_execution_policy const * execution_policy, ::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type grid_gradient_type) :
	execution_policy(static_cast<::framework::nn::grid::execution_policy const *>(execution_policy))
{
	::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::size_t const gradient_type_size = ::framework::gpu::utility::get_component_type_size(grid_gradient_type);

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy->parameter_count * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->full_precision_parameter_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->full_precision_parameter_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->full_precision_parameter_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->full_precision_parameter_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->full_precision_parameter_buffer;
		bind_buffer_memory_info.memory = this->full_precision_parameter_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy->parameter_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->parameter_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->parameter_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->parameter_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->parameter_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->parameter_buffer;
		bind_buffer_memory_info.memory = this->parameter_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy->parameter_count * gradient_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::uniform_buffer_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->grid_gradient_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->grid_gradient_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::none;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = memory_requirements.prefers_dedicated_allocation || memory_requirements.requires_dedicated_allocation ? this->grid_gradient_buffer : nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->grid_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->grid_gradient_buffer;
		bind_buffer_memory_info.memory = this->grid_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
}

void ::framework::nn::grid::encoding::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size)
{
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch(::framework::numeric::div_round_up(size, 512u), this->execution_policy->level_count, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::grid::encoding::forward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::grid::encoding::backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::std::uint32_t size)
{
	::std::uint32_t const features_per_thread = ::std::min(2u, this->execution_policy->features_per_level);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->fill_buffer(this->grid_gradient_buffer, 0, ::framework::gpu::whole_size, 0, {}, {}, {});
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->backward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch(::framework::numeric::div_round_up(size * this->execution_policy->features_per_level / features_per_thread, 256u), this->execution_policy->level_count, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::grid::encoding::backward_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t dynamic_offset, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset)
{
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->fill_buffer(this->grid_gradient_buffer, 0, ::framework::gpu::whole_size, 0, {}, {}, {});
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->backward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy->pipeline_layout, 0, 1, &descriptor_set, 1, &dynamic_offset);
	command_buffer->dispatch_indirect(dispatch_indirect_command_buffer, dispatch_indirect_command_offset + sizeof(::framework::gpu::dispatch_indirect_command));
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::grid::encoding::update_descriptor_sets(::framework::gpu::device * device, ::framework::gpu::buffer * size_buffer, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * output_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[9];
	descriptor_buffer_infos[0].buffer = size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = sizeof(::std::uint32_t);
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = input_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	descriptor_buffer_infos[2].buffer = this->parameter_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	descriptor_buffer_infos[3].buffer = this->parameter_buffer;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = 2;

	descriptor_buffer_infos[4].buffer = this->execution_policy->offset_buffer;
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = 2;

	descriptor_buffer_infos[5].buffer = dloss_dinput_buffer;
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = 2;

	descriptor_buffer_infos[6].buffer = this->grid_gradient_buffer;
	descriptor_buffer_infos[6].offset = 0;
	descriptor_buffer_infos[6].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[6].stride = 2;

	descriptor_buffer_infos[7].buffer = this->grid_gradient_buffer;
	descriptor_buffer_infos[7].offset = 0;
	descriptor_buffer_infos[7].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[7].stride = 2;

	descriptor_buffer_infos[8].buffer = output_buffer;
	descriptor_buffer_infos[8].offset = 0;
	descriptor_buffer_infos[8].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[8].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[9];
	// size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::uniform_buffer_dynamic;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// input_positions_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// grid_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;
	// max_level_buffer
	write_descriptor_sets[3].dst_set = descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[3].image_info = nullptr;
	write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
	write_descriptor_sets[3].texel_buffer_view = nullptr;
	write_descriptor_sets[3].acceleration_structures = nullptr;
	// offset_buffer
	write_descriptor_sets[4].dst_set = descriptor_set;
	write_descriptor_sets[4].dst_binding = 4;
	write_descriptor_sets[4].dst_array_element = 0;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[4].image_info = nullptr;
	write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
	write_descriptor_sets[4].texel_buffer_view = nullptr;
	write_descriptor_sets[4].acceleration_structures = nullptr;
	// dloss_dy_buffer
	write_descriptor_sets[5].dst_set = descriptor_set;
	write_descriptor_sets[5].dst_binding = 5;
	write_descriptor_sets[5].dst_array_element = 0;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[5].image_info = nullptr;
	write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
	write_descriptor_sets[5].texel_buffer_view = nullptr;
	write_descriptor_sets[5].acceleration_structures = nullptr;
	// grid_gradient_buffe
	write_descriptor_sets[6].dst_set = descriptor_set;
	write_descriptor_sets[6].dst_binding = 6;
	write_descriptor_sets[6].dst_array_element = 0;
	write_descriptor_sets[6].descriptor_count = 1;
	write_descriptor_sets[6].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[6].image_info = nullptr;
	write_descriptor_sets[6].buffer_info = &descriptor_buffer_infos[6];
	write_descriptor_sets[6].texel_buffer_view = nullptr;
	write_descriptor_sets[6].acceleration_structures = nullptr;
	// dy_dx_bufferr
	write_descriptor_sets[7].dst_set = descriptor_set;
	write_descriptor_sets[7].dst_binding = 7;
	write_descriptor_sets[7].dst_array_element = 0;
	write_descriptor_sets[7].descriptor_count = 1;
	write_descriptor_sets[7].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[7].image_info = nullptr;
	write_descriptor_sets[7].buffer_info = &descriptor_buffer_infos[7];
	write_descriptor_sets[7].texel_buffer_view = nullptr;
	write_descriptor_sets[7].acceleration_structures = nullptr;
	// encoded_positions_buffer
	write_descriptor_sets[8].dst_set = descriptor_set;
	write_descriptor_sets[8].dst_binding = 8;
	write_descriptor_sets[8].dst_array_element = 0;
	write_descriptor_sets[8].descriptor_count = 1;
	write_descriptor_sets[8].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[8].image_info = nullptr;
	write_descriptor_sets[8].buffer_info = &descriptor_buffer_infos[8];
	write_descriptor_sets[8].texel_buffer_view = nullptr;
	write_descriptor_sets[8].acceleration_structures = nullptr;

	device->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}