inline ::std::uint32_t(::framework::nn::grid::execution_policy::get_parameter_count)() const noexcept
{
	return this->parameter_count;
}

inline ::framework::gpu::device_size(::framework::nn::grid::execution_policy::get_dispatch_indirect_command_size)() const noexcept
{
	return sizeof(::framework::gpu::dispatch_indirect_command) * 2u;
}

inline ::framework::gpu::descriptor_set_layout * (::framework::nn::grid::execution_policy::get_descriptor_set_layout)() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::framework::gpu::descriptor_set_layout * (::framework::nn::grid::execution_policy::get_initialize_dispatch_indirect_command_descriptor_set_layout)() const noexcept
{
	return this->initialize_dispatch_indirect_command_descriptor_set_layout;
}

inline ::framework::gpu::buffer * (::framework::nn::grid::encoding::get_full_precision_parameter_buffer)() const noexcept
{
	return this->full_precision_parameter_buffer;
}

inline ::framework::gpu::buffer * (::framework::nn::grid::encoding::get_parameter_buffer)() const noexcept
{
	return this->parameter_buffer;
}

inline ::framework::gpu::buffer * (::framework::nn::grid::encoding::get_gradient_buffer)() const noexcept
{
	return this->grid_gradient_buffer;
}