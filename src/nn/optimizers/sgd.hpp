#pragma once

#include "gpu/core.hpp"
#include "nn/optimizer.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::nn
{
	class sgd : public ::framework::nn::optimizer
	{
	public:
		sgd(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type type, ::std::uint32_t parameter_count, float learning_rate);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		void update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * weights_full_precision_buffer, ::framework::gpu::buffer * weights_buffer, ::framework::gpu::buffer * gradients_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void step(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set) override;

		::std::uint32_t step() const noexcept override;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::component_type type;
		::std::uint32_t parameter_count;
		::std::uint32_t current_step;
		float loss_scale;
		float learning_rate;
	};
}

#include "nn/optimizers/sgd.inl"