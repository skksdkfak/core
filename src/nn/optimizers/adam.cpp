#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "nn/optimizers/adam.hpp"
#include "resource/resource_manager.hpp"

namespace framework::local
{
	namespace
	{
		struct push_constants
		{
			float relative_weight_decay;
			float absolute_weight_decay;
			float weight_clipping_magnitude;
			float loss_scale;
			float learning_rate;
			float beta1;
			float beta2;
			float epsilon;
			float lower_lr_bound;
			float upper_lr_bound;
			float l2_reg;
		};
	}
}

::framework::nn::adam::adam(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type gradient_type, ::std::uint32_t parameter_count, bool optimize_matrix_params, bool adabound, float weight_clipping_magnitude, float loss_scale, float learning_rate, float l2_reg) :
	parameter_count(parameter_count),
	current_step(0),
	adabound(adabound),
	weight_clipping_magnitude(weight_clipping_magnitude),
	loss_scale(loss_scale),
	learning_rate(learning_rate),
	beta1(0.9f),
	beta2(0.999f),
	l2_reg(l2_reg)
{
	char const * parameter_type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);
	char const * gradient_type_name = ::framework::gpu::utility::get_component_type_name(gradient_type);

	{
		::framework::resource::preprocessor_define preprocessor_defines[]
		{
			{ "parameter_t", parameter_type_name },
			{ "gradient_t", gradient_type_name },
		};

		::framework::resource::glsl_source_info glsl_source_info;
		glsl_source_info.file_name = "nn/optimizers/adam.comp.glsl";
		glsl_source_info.source_entry_point = "main";

		::framework::resource::shader_stage_info shader_stage_info;
		shader_stage_info.hlsl_source_info = nullptr;
		shader_stage_info.glsl_source_info = &glsl_source_info;
		shader_stage_info.slang_source_info = nullptr;
		shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
		shader_stage_info.preprocessor_defines = preprocessor_defines;
		shader_stage_info.type_conformance_count = 0;
		shader_stage_info.type_conformances = nullptr;
		shader_stage_info.include_override_count = 0;
		shader_stage_info.include_overrides = nullptr;
		shader_stage_info.entry_point = "main";
		shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

		::framework::resource::shader_module_info shader_module_info;
		shader_module_info.shader_stage_info_count = 1;
		shader_module_info.shader_stage_infos = &shader_stage_info;
		this->shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[6];
		// weights_full_precision_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// weights_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;
		// gradients_buffer
		descriptor_set_layout_bindings[2].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[2].binding = 2;
		descriptor_set_layout_bindings[2].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[2].hlsl_register_space = 0;
		descriptor_set_layout_bindings[2].descriptor_count = 1;
		descriptor_set_layout_bindings[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[2].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[2].immutable_samplers = nullptr;
		// first_moments_buffer
		descriptor_set_layout_bindings[3].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[3].binding = 3;
		descriptor_set_layout_bindings[3].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[3].hlsl_register_space = 0;
		descriptor_set_layout_bindings[3].descriptor_count = 1;
		descriptor_set_layout_bindings[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[3].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[3].immutable_samplers = nullptr;
		// second_moments_buffer
		descriptor_set_layout_bindings[4].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[4].binding = 4;
		descriptor_set_layout_bindings[4].hlsl_shader_register = 2;
		descriptor_set_layout_bindings[4].hlsl_register_space = 0;
		descriptor_set_layout_bindings[4].descriptor_count = 1;
		descriptor_set_layout_bindings[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[4].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[4].immutable_samplers = nullptr;
		// param_steps_buffer
		descriptor_set_layout_bindings[5].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[5].binding = 5;
		descriptor_set_layout_bindings[5].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[5].hlsl_register_space = 0;
		descriptor_set_layout_bindings[5].descriptor_count = 1;
		descriptor_set_layout_bindings[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[5].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[5].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout
		};
		
		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::specialization_map_entry const specialization_map_entries[]
		{
			{ 0, sizeof(::std::uint32_t) * 0, sizeof(::std::uint32_t) },
			{ 1, sizeof(::std::uint32_t) * 1, sizeof(::std::uint32_t) }
		};

		::std::uint32_t const specialization_data[]
		{
			parameter_count,
			static_cast<::framework::gpu::bool32_t>(optimize_matrix_params)
		};

		::framework::gpu::specialization_info specialization_info;
		specialization_info.map_entry_count = static_cast<::std::uint32_t>(::std::size(specialization_map_entries));
		specialization_info.map_entries = specialization_map_entries;
		specialization_info.data_size = sizeof(specialization_data);
		specialization_info.data = specialization_data;

		::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_info;
		pipeline_shader_stage_create_info.flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
		pipeline_shader_stage_create_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;
		pipeline_shader_stage_create_info.module = this->shader_module;
		pipeline_shader_stage_create_info.name = "main";
		pipeline_shader_stage_create_info.specialization_info = &specialization_info;

		::framework::gpu::compute_pipeline_create_info compute_pipeline_create_info;
		compute_pipeline_create_info.flags = ::framework::gpu::pipeline_create_flags::none;
		compute_pipeline_create_info.stage = pipeline_shader_stage_create_info;
		compute_pipeline_create_info.layout = this->pipeline_layout;
		compute_pipeline_create_info.base_pipeline = nullptr;
		compute_pipeline_create_info.base_pipeline_index = -1;
		assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, 1, &compute_pipeline_create_info, nullptr, &this->pipeline));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = parameter_count * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->first_moments_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->first_moments_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->first_moments_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->first_moments_buffer;
		bind_buffer_memory_info.memory = this->first_moments_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = parameter_count * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->second_moments_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->second_moments_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->second_moments_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->second_moments_buffer;
		bind_buffer_memory_info.memory = this->second_moments_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = parameter_count * sizeof(::std::uint32_t);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit | ::framework::gpu::buffer_usage_flags::allow_clear_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->param_steps_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->param_steps_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->param_steps_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->param_steps_buffer;
		bind_buffer_memory_info.memory = this->param_steps_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}
}

void ::framework::nn::adam::update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * weights_full_precision_buffer, ::framework::gpu::buffer * weights_buffer, ::framework::gpu::buffer * gradients_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[6];
	descriptor_buffer_infos[0].buffer = weights_full_precision_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 2;

	descriptor_buffer_infos[1].buffer = weights_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	descriptor_buffer_infos[2].buffer = gradients_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	descriptor_buffer_infos[3].buffer = this->first_moments_buffer;
	descriptor_buffer_infos[3].offset = 0;
	descriptor_buffer_infos[3].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[3].stride = 2;

	descriptor_buffer_infos[4].buffer = this->second_moments_buffer;
	descriptor_buffer_infos[4].offset = 0;
	descriptor_buffer_infos[4].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[4].stride = 2;

	descriptor_buffer_infos[5].buffer = this->param_steps_buffer;
	descriptor_buffer_infos[5].offset = 0;
	descriptor_buffer_infos[5].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[5].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[6];
	// weights_full_precision_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// weights_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// gradients_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;
	// first_moments_buffer
	write_descriptor_sets[3].dst_set = descriptor_set;
	write_descriptor_sets[3].dst_binding = 3;
	write_descriptor_sets[3].dst_array_element = 0;
	write_descriptor_sets[3].descriptor_count = 1;
	write_descriptor_sets[3].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[3].image_info = nullptr;
	write_descriptor_sets[3].buffer_info = &descriptor_buffer_infos[3];
	write_descriptor_sets[3].texel_buffer_view = nullptr;
	write_descriptor_sets[3].acceleration_structures = nullptr;
	// second_moments_buffer
	write_descriptor_sets[4].dst_set = descriptor_set;
	write_descriptor_sets[4].dst_binding = 4;
	write_descriptor_sets[4].dst_array_element = 0;
	write_descriptor_sets[4].descriptor_count = 1;
	write_descriptor_sets[4].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[4].image_info = nullptr;
	write_descriptor_sets[4].buffer_info = &descriptor_buffer_infos[4];
	write_descriptor_sets[4].texel_buffer_view = nullptr;
	write_descriptor_sets[4].acceleration_structures = nullptr;
	// param_steps_buffer
	write_descriptor_sets[5].dst_set = descriptor_set;
	write_descriptor_sets[5].dst_binding = 5;
	write_descriptor_sets[5].dst_array_element = 0;
	write_descriptor_sets[5].descriptor_count = 1;
	write_descriptor_sets[5].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[5].image_info = nullptr;
	write_descriptor_sets[5].buffer_info = &descriptor_buffer_infos[5];
	write_descriptor_sets[5].texel_buffer_view = nullptr;
	write_descriptor_sets[5].acceleration_structures = nullptr;

	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

void ::framework::nn::adam::step(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	this->current_step++;

	float lower_lr_bound = 0;
	float upper_lr_bound = ::std::numeric_limits<float>::max();

	// AdaBound paper: https://openreview.net/pdf?id=Bkg3g2R9FX
	if (this->adabound)
	{
		lower_lr_bound = 0.1f - 0.1f / ((1 - this->beta2) * static_cast<float>(this->step()) + 1);
		upper_lr_bound = 0.1f + 0.1f / ((1 - this->beta2) * static_cast<float>(this->step()));
	}

	struct ::framework::local::push_constants const push_constants
	{
		.relative_weight_decay = 0.0f,
		.absolute_weight_decay = 0.0f,
		.weight_clipping_magnitude = this->weight_clipping_magnitude,
		.loss_scale = this->loss_scale,
		.learning_rate = this->learning_rate,
		.beta1 = this->beta1,
		.beta2 = this->beta2,
		.epsilon = 1e-8f,
		.lower_lr_bound = lower_lr_bound,
		.upper_lr_bound = upper_lr_bound,
		.l2_reg = this->l2_reg
	};
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->push_constants(this->pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((this->parameter_count + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::adam::initialize(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::memory_barrier memory_barrier;
	memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
	memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
	memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

	::framework::gpu::dependency_info dependency_info;
	dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
	dependency_info.memory_barrier_count = 1;
	dependency_info.memory_barriers = &memory_barrier;
	dependency_info.buffer_memory_barrier_count = 0;
	dependency_info.buffer_memory_barriers = nullptr;
	dependency_info.image_memory_barrier_count = 0;
	dependency_info.image_memory_barriers = nullptr;
	command_buffer->pipeline_barrier(&dependency_info);
	command_buffer->fill_buffer(this->first_moments_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});
	command_buffer->fill_buffer(this->second_moments_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});
	command_buffer->fill_buffer(this->param_steps_buffer, 0, ::framework::gpu::whole_size, 0, 0, {}, {});
	command_buffer->pipeline_barrier(&dependency_info);
}