#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_scalar_block_layout : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_ARB_shading_language_include : enable

#include "utils.glsl"

layout(local_size_x = 128) in;

layout(constant_id = 0) const uint size = 0;
layout(constant_id = 1) const bool optimize_matrix_params = true;

layout(std140, push_constant) uniform push_constants
{
	float relative_weight_decay;
	float absolute_weight_decay;
	float weight_clipping_magnitude;
	float loss_scale;
	float learning_rate;
	float beta1;
	float beta2;
	float epsilon;
	float lower_lr_bound;
	float upper_lr_bound;
	float l2_reg;
};

layout(std430, set = 0, binding = 0) buffer weights_full_precision_buffer_t
{
    float weights_full_precision_buffer[];
};

layout(std430, set = 0, binding = 1) writeonly buffer weights_buffer_t
{
    parameter_t weights_buffer[];
};

layout(std430, set = 0, binding = 2) readonly buffer gradients_buffer_t
{
	gradient_t gradients_buffer[];
};

layout(std430, set = 0, binding = 3) buffer first_moments_buffer_t
{
    float first_moments_buffer[];
};

layout(std430, set = 0, binding = 4) buffer second_moments_buffer_t
{
    float second_moments_buffer[];
};

layout(std430, set = 0, binding = 5) buffer param_steps_buffer_t
{
    uint param_steps_buffer[];
};

float weight_decay(float relative_weight_decay, float absolute_weight_decay, float weight)
{
	// Relative weight decay is closely related to l2 regularization, whereas absolute weight decay corresponds to l1 regularization
	return (1.0f - relative_weight_decay) * weight - copysign(absolute_weight_decay, weight);
}

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	if (i >= size)
	{
		return;
	}

	float gradient = float(gradients_buffer[i]) / loss_scale;
	if (!optimize_matrix_params && gradient == 0)
	{
		return;
	}
	const float weight_fp = weights_full_precision_buffer[i];
	if (optimize_matrix_params)
	{
		// No L2 reg for non-matrix params
		gradient += l2_reg * weight_fp;
	}
	const float gradient_sq = gradient * gradient;
	const float first_moment = first_moments_buffer[i] = beta1 * first_moments_buffer[i] + (1 - beta1) * gradient;
	const float second_moment = second_moments_buffer[i] = beta2 * second_moments_buffer[i] + (1 - beta2) * gradient_sq;
	// Debiasing. Since some parameters might see fewer steps than others, they each need their own step counter.
	const uint current_step = ++param_steps_buffer[i];
	const float current_learning_rate = learning_rate * sqrt(1 - pow(beta2, float(current_step))) / (1 - pow(beta1, float(current_step)));
	// Follow AdaBound paradigm
	const float effective_learning_rate = min(max(current_learning_rate / (sqrt(second_moment) + epsilon), lower_lr_bound), upper_lr_bound);
	const float decayed_weight = weight_decay(relative_weight_decay * current_learning_rate, absolute_weight_decay * current_learning_rate, weight_fp);
	float new_weight = decayed_weight - effective_learning_rate * first_moment;
	if (weight_clipping_magnitude != 0.0f)
	{
		new_weight = clamp(new_weight, -weight_clipping_magnitude, weight_clipping_magnitude);
	}
	weights_full_precision_buffer[i] = new_weight;
	weights_buffer[i] = parameter_t(new_weight);
}
