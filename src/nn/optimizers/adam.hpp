#pragma once

#include "gpu/core.hpp"
#include "nn/optimizer.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::nn
{
	class adam : public ::framework::nn::optimizer
	{
	public:
		adam(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type gradient_type, ::std::uint32_t parameter_count, bool optimize_matrix_params, bool adabound, float weight_clipping_magnitude, float loss_scale, float learning_rate, float l2_reg);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		void update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * weights_full_precision_buffer, ::framework::gpu::buffer * weights_buffer, ::framework::gpu::buffer * gradients_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void step(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set) override;
		
		void initialize(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		::std::uint32_t step() const noexcept override;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
		::framework::gpu::pipeline * pipeline;
		::framework::gpu::buffer * first_moments_buffer;
		::framework::gpu::device_memory * first_moments_device_memory;
		::framework::gpu::buffer * second_moments_buffer;
		::framework::gpu::device_memory * second_moments_device_memory;
		::framework::gpu::buffer * param_steps_buffer;
		::framework::gpu::device_memory * param_steps_device_memory;
		::std::uint32_t parameter_count;
		::std::uint32_t current_step;
		bool adabound;
		float weight_clipping_magnitude;
		float loss_scale;
		float learning_rate;
		float beta1;
		float beta2;
		float l2_reg;
	};
}

#include "nn/optimizers/adam.inl"