inline ::framework::gpu::descriptor_set_layout * ::framework::nn::sgd::get_descriptor_set_layout() const noexcept
{
	return this->descriptor_set_layout;
}

inline ::std::uint32_t(::framework::nn::sgd::step)() const noexcept
{
	return this->current_step;
}