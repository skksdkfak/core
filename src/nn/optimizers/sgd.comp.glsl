#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_scalar_block_layout : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_ARB_shading_language_include : enable

layout(local_size_x = 128) in;

layout(std140, push_constant) uniform push_constants
{
	uint n_elements;
	float loss_scale;
	float learning_rate;
	float l2_reg;
};

layout(std430, set = 0, binding = 0) buffer weights_full_precision_buffer_t
{
    float weights_full_precision_buffer[];
};

layout(std430, set = 0, binding = 1) writeonly buffer weights_buffer_t
{
    OUTPUT_TYPE weights_buffer[];
};

layout(std430, set = 0, binding = 2) readonly buffer gradients_buffer_t
{
    OUTPUT_TYPE gradients_buffer[];
};

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	if (i >= n_elements)
	{
		return;
	}

	const float weight_fp = weights_full_precision_buffer[i];
	float gradient = float(gradients_buffer[i]) / loss_scale;
	// Optional gradient clipping
	const float clip_value = 1000.0f;
	gradient = clamp(gradient, -clip_value, clip_value);
	gradient += l2_reg * weight_fp;
	float new_weight = weight_fp - learning_rate * gradient;

	weights_full_precision_buffer[i] = new_weight;
	weights_buffer[i] = OUTPUT_TYPE(new_weight);
}
