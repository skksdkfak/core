#pragma once

#include "gpu/core.hpp"

namespace framework::nn
{
	class optimizer
	{
	public:
		virtual void step(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set) = 0;

		virtual ::std::uint32_t step() const noexcept = 0;
	};
}