#include "gpu/utility.hpp"
#include "gpu_context.hpp"
#include "gpu_log.hpp"
#include "nn/mlp/network.hpp"
#include "resource/resource_manager.hpp"
#include <algorithm>
#include <array>
#include <cmath>

namespace framework::local
{
	namespace
	{
		struct initialize_parameters_push_constants
		{
			::std::uint32_t count;
			::std::uint32_t offset;
			float scale;
		};

		struct activation_push_constants
		{
			::std::uint32_t activations_offset;
			::std::uint32_t activations_gradient_offset;
			::std::uint32_t size;
			::std::uint32_t activation;
		};

		constexpr inline ::framework::linalg::matrix_layout transpose(::framework::linalg::matrix_layout matrix_layout)
		{
			return matrix_layout == ::framework::linalg::matrix_layout::row_major ? ::framework::linalg::matrix_layout::column_major : ::framework::linalg::matrix_layout::row_major;
		}
	}
}

::framework::nn::mlp::execution_policy::execution_policy(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::framework::linalg::matrix_layout input_layout, ::framework::linalg::matrix_layout output_layout, ::std::uint32_t input_width, ::std::uint32_t output_width, ::std::uint32_t padded_input_width, ::std::uint32_t padded_output_width, ::std::uint32_t hidden_width, ::std::uint32_t hidden_layer_count, ::framework::nn::activation activation, ::framework::nn::activation output_activation, ::std::uint32_t batch_alignment, ::std::uint32_t inference_batch_alignment, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, bool compute_dloss_dinput) :
	hidden_matmul_count(hidden_layer_count - 1),
	input_width(input_width),
	output_width(output_width),
	padded_input_width(padded_input_width),
	padded_output_width(padded_output_width),
	hidden_width(hidden_width),
	parameter_count(padded_input_width * hidden_width + hidden_width * hidden_width * this->hidden_matmul_count + hidden_width * padded_output_width),
	compute_dloss_dinput(compute_dloss_dinput)
{
	this->gemm = ::framework::algorithm::gemm(gpu_context, resource_manager, parameter_type, parameter_type, parameter_type, parameter_type, accumulator_type, 0, nullptr, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_hlsl_path, ::framework::algorithm::gemm::linear_combination_gemm_epilogue_glsl_path);
	this->inference_input_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(inference_batch_alignment, 128u), ::std::min(padded_input_width, 32u), false, ::framework::linalg::matrix_layout::row_major, input_layout, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->inference_hidden_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(inference_batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->inference_output_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(padded_output_width, 128u), ::std::min(inference_batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->forward_input_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(padded_input_width, 32u), false, ::framework::linalg::matrix_layout::row_major, input_layout, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->forward_hidden_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->forward_output_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(padded_output_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->backward_output_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(padded_output_width, 128u), ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_output_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_hidden_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_hidden_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_input_weights_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(padded_input_width, 128u), ::std::min(batch_alignment, 32u), true, ::framework::linalg::matrix_layout::column_major, ::framework::local::transpose(input_layout), ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_input_weights_gradient_reduce_split_k = ::framework::algorithm::gemm::reduce_split_k_executor(this->gemm, gpu_context, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);
	this->backward_output_layer_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(padded_output_width, 32u), false, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->backward_hidden_layer_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(hidden_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major);
	this->backward_input_layer_gradient_matmul = ::framework::algorithm::gemm::executor(this->gemm, gpu_context, 0u, 0u, ::std::min(padded_input_width, 128u), ::std::min(batch_alignment, 128u), ::std::min(hidden_width, 32u), false, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::column_major, ::framework::linalg::matrix_layout::row_major, ::framework::linalg::matrix_layout::row_major);

	char const * parameter_type_name = ::framework::gpu::utility::get_component_type_name(parameter_type);

	{
		::framework::resource::preprocessor_define preprocessor_defines[]
		{
			{ "parameter_t", parameter_type_name }
		};

		{
			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/mlp/activation_forward.comp.hlsl";
			hlsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->activation_forward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/mlp/activation_backward.comp.hlsl";
			hlsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->activation_backward_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}

		{
			::framework::resource::hlsl_source_info hlsl_source_info;
			hlsl_source_info.file_name = "nn/mlp/initialize_parameters.comp.hlsl";
			hlsl_source_info.source_entry_point = "main";

			::framework::resource::shader_stage_info shader_stage_info;
			shader_stage_info.hlsl_source_info = &hlsl_source_info;
			shader_stage_info.glsl_source_info = nullptr;
			shader_stage_info.slang_source_info = nullptr;
			shader_stage_info.preprocessor_define_count = static_cast<::std::uint32_t>(::std::size(preprocessor_defines));
			shader_stage_info.preprocessor_defines = preprocessor_defines;
			shader_stage_info.type_conformance_count = 0;
			shader_stage_info.type_conformances = nullptr;
			shader_stage_info.entry_point = "main";
			shader_stage_info.stage = ::framework::gpu::shader_stage_flags::compute_bit;

			::framework::resource::shader_module_info shader_module_info;
			shader_module_info.shader_stage_info_count = 1;
			shader_module_info.shader_stage_infos = &shader_stage_info;
			this->initialize_parameters_shader_module = resource_manager->load_shader_module(gpu_context->get_device(), shader_module_info);
		}
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// activations_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// activations_gradient_buffer
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 1;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->descriptor_set_layout));
	}

	{
		::framework::gpu::descriptor_set_layout_binding descriptor_set_layout_bindings[2];
		// parameters_full_precision_buffer
		descriptor_set_layout_bindings[0].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[0].binding = 0;
		descriptor_set_layout_bindings[0].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[0].hlsl_register_space = 0;
		descriptor_set_layout_bindings[0].descriptor_count = 1;
		descriptor_set_layout_bindings[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[0].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[0].immutable_samplers = nullptr;
		// parameters
		descriptor_set_layout_bindings[1].flags = ::framework::gpu::descriptor_binding_flags::none;
		descriptor_set_layout_bindings[1].binding = 1;
		descriptor_set_layout_bindings[1].hlsl_shader_register = 0;
		descriptor_set_layout_bindings[1].hlsl_register_space = 0;
		descriptor_set_layout_bindings[1].descriptor_count = 1;
		descriptor_set_layout_bindings[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
		descriptor_set_layout_bindings[1].stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		descriptor_set_layout_bindings[1].immutable_samplers = nullptr;

		::framework::gpu::descriptor_set_layout_create_info descriptor_set_layout_create_info;
		descriptor_set_layout_create_info.flags = ::framework::gpu::descriptor_set_layout_create_flags::none;
		descriptor_set_layout_create_info.binding_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layout_bindings));
		descriptor_set_layout_create_info.bindings = descriptor_set_layout_bindings;
		assert_framework_gpu_result(gpu_context->get_device()->create_descriptor_set_layout(&descriptor_set_layout_create_info, nullptr, &this->initialize_parameters_descriptor_set_layout));
	}

	assert_framework_gpu_result(gpu_log->create_descriptor_set_layout(::framework::gpu::descriptor_set_layout_create_flags::none, 1, ::framework::gpu::shader_stage_flags::all, this->gpu_log_descriptor_set_layout));

	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);
	
	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->descriptor_set_layout,
			this->gpu_log_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::activation_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->pipeline_layout));
	}

	{
		::framework::gpu::descriptor_set_layout * descriptor_set_layouts[]
		{
			this->initialize_parameters_descriptor_set_layout
		};

		::framework::gpu::push_constant_range push_constant_ranges;
		push_constant_ranges.stage_flags = ::framework::gpu::shader_stage_flags::compute_bit;
		push_constant_ranges.binding = 0;
		push_constant_ranges.offset = 0;
		push_constant_ranges.size = sizeof(struct ::framework::local::initialize_parameters_push_constants);
		push_constant_ranges.hlsl_shader_register = 0;
		push_constant_ranges.hlsl_register_space = 0;

		::framework::gpu::pipeline_layout_create_info pipeline_layout_create_info;
		pipeline_layout_create_info.flags = ::framework::gpu::pipeline_layout_create_flags::none;
		pipeline_layout_create_info.descriptor_set_layout_count = static_cast<::std::uint32_t>(::std::size(descriptor_set_layouts));
		pipeline_layout_create_info.descriptor_set_layouts = descriptor_set_layouts;
		pipeline_layout_create_info.push_constant_range_count = 1;
		pipeline_layout_create_info.push_constant_ranges = &push_constant_ranges;
		assert_framework_gpu_result(gpu_context->get_device()->create_pipeline_layout(&pipeline_layout_create_info, nullptr, &this->initialize_parameters_pipeline_layout));
	}

	::framework::gpu::pipeline_shader_stage_create_info pipeline_shader_stage_create_infos[4];
	pipeline_shader_stage_create_infos[0].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[0].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[0].module = this->activation_forward_shader_module;
	pipeline_shader_stage_create_infos[0].name = "main";
	pipeline_shader_stage_create_infos[0].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[1].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[1].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[1].module = this->activation_forward_shader_module;
	pipeline_shader_stage_create_infos[1].name = "main";
	pipeline_shader_stage_create_infos[1].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[2].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[2].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[2].module = this->activation_backward_shader_module;
	pipeline_shader_stage_create_infos[2].name = "main";
	pipeline_shader_stage_create_infos[2].specialization_info = nullptr;

	pipeline_shader_stage_create_infos[3].flags = ::framework::gpu::pipeline_shader_stage_create_flags::none;
	pipeline_shader_stage_create_infos[3].stage = ::framework::gpu::shader_stage_flags::compute_bit;
	pipeline_shader_stage_create_infos[3].module = this->initialize_parameters_shader_module;
	pipeline_shader_stage_create_infos[3].name = "main";
	pipeline_shader_stage_create_infos[3].specialization_info = nullptr;

	::framework::gpu::compute_pipeline_create_info compute_pipeline_create_infos[4];
	compute_pipeline_create_infos[0].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[0].stage = pipeline_shader_stage_create_infos[0];
	compute_pipeline_create_infos[0].layout = this->pipeline_layout;
	compute_pipeline_create_infos[0].base_pipeline = nullptr;
	compute_pipeline_create_infos[0].base_pipeline_index = -1;

	compute_pipeline_create_infos[1].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[1].stage = pipeline_shader_stage_create_infos[1];
	compute_pipeline_create_infos[1].layout = this->pipeline_layout;
	compute_pipeline_create_infos[1].base_pipeline = nullptr;
	compute_pipeline_create_infos[1].base_pipeline_index = -1;

	compute_pipeline_create_infos[2].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[2].stage = pipeline_shader_stage_create_infos[2];
	compute_pipeline_create_infos[2].layout = this->pipeline_layout;
	compute_pipeline_create_infos[2].base_pipeline = nullptr;
	compute_pipeline_create_infos[2].base_pipeline_index = -1;

	compute_pipeline_create_infos[3].flags = ::framework::gpu::pipeline_create_flags::none;
	compute_pipeline_create_infos[3].stage = pipeline_shader_stage_create_infos[3];
	compute_pipeline_create_infos[3].layout = this->initialize_parameters_pipeline_layout;
	compute_pipeline_create_infos[3].base_pipeline = nullptr;
	compute_pipeline_create_infos[3].base_pipeline_index = -1;

	::framework::gpu::pipeline * pipelines[::std::size(compute_pipeline_create_infos)];
	assert_framework_gpu_result(gpu_context->get_device()->create_compute_pipelines(nullptr, static_cast<::std::uint32_t>(::std::size(compute_pipeline_create_infos)), compute_pipeline_create_infos, nullptr, pipelines));
	this->inference_pipeline = pipelines[0], this->activation_forward_pipeline = pipelines[1], this->activation_backward_pipeline = pipelines[2], this->initialize_parameters_pipeline = pipelines[3];

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = gemm_device_parameters_buffer_alignment * 12;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::uniform_buffer_bit | ::framework::gpu::buffer_usage_flags::transfer_dst_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->gemm_parameters_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gemm_parameters_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->gemm_parameters_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gemm_parameters_buffer;
		bind_buffer_memory_info.memory = this->gemm_parameters_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::std::uint32_t const max_split_k_slices = max_batch_size / ::std::min(max_batch_size, 1u << 12);
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max({ this->padded_input_width * this->hidden_width, this->hidden_width * this->hidden_width, this->hidden_width * this->padded_output_width }) * max_split_k_slices * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->split_k_slices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->split_k_slices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->split_k_slices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->split_k_slices_buffer;
		bind_buffer_memory_info.memory = this->split_k_slices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

}

void(::framework::nn::mlp::execution_policy::initialize_dispatch_indirect_command)(::framework::gpu_context * gpu_context, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, bool inference)
{
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->initialize_dispatch_indirect_command_pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	command_buffer->dispatch(1, 1, 1);
}

void ::framework::nn::mlp::execution_policy::update_initialize_dispatch_indirect_command_descriptor_set(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[3];
	// batch_size_buffer
	descriptor_buffer_infos[0].buffer = batch_size_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 4;
	// dispatch_indirect_command_buffer
	descriptor_buffer_infos[1].buffer = this->dispatch_indirect_command_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;
	// gemm_parameters_buffer
	descriptor_buffer_infos[2].buffer = this->gemm_parameters_buffer;
	descriptor_buffer_infos[2].offset = 0;
	descriptor_buffer_infos[2].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[2].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[3];
	// batch_size_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::sampled_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// dispatch_indirect_command_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	// gemm_parameters_buffer
	write_descriptor_sets[2].dst_set = descriptor_set;
	write_descriptor_sets[2].dst_binding = 2;
	write_descriptor_sets[2].dst_array_element = 0;
	write_descriptor_sets[2].descriptor_count = 1;
	write_descriptor_sets[2].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[2].image_info = nullptr;
	write_descriptor_sets[2].buffer_info = &descriptor_buffer_infos[2];
	write_descriptor_sets[2].texel_buffer_view = nullptr;
	write_descriptor_sets[2].acceleration_structures = nullptr;

	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

::framework::coroutine::immediate_task<void>(::framework::nn::mlp::execution_policy::update_parameters)(::framework::gpu_context * gpu_context, ::framework::command_context * command_context, ::std::uint32_t batch_size, ::std::uint32_t inference_batch_size)
{
	::std::uint32_t const split_k_slices = batch_size / ::std::min(batch_size, 1u << 12);
	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->inference_input_matmul.get_device_parameters(gpu_context, this->hidden_width, inference_batch_size, this->padded_input_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 0 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->inference_hidden_matmul.get_device_parameters(gpu_context, this->hidden_width, inference_batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 1 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->inference_output_matmul.get_device_parameters(gpu_context, this->padded_output_width, inference_batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 2 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->forward_input_matmul.get_device_parameters(gpu_context, this->hidden_width, batch_size, this->padded_input_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 3 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->forward_hidden_matmul.get_device_parameters(gpu_context, this->hidden_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 4 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->forward_output_matmul.get_device_parameters(gpu_context, this->padded_output_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 5 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_output_weights_gradient_matmul.get_device_parameters(gpu_context, this->padded_output_width, this->hidden_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 6 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_output_layer_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, batch_size, this->padded_output_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 7 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_hidden_weights_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, this->hidden_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 8 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_hidden_layer_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 9 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_input_weights_gradient_matmul.get_device_parameters(gpu_context, this->hidden_width, this->padded_input_width, batch_size, split_k_slices, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 10 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
	{
		::framework::algorithm::gemm::device_parameters const gemm_device_parameters = this->backward_input_layer_gradient_matmul.get_device_parameters(gpu_context, this->padded_input_width, batch_size, this->hidden_width, 1, 1.0f, 0.0f);
		co_await command_context->update_buffer(this->gemm_parameters_buffer, &gemm_device_parameters, 11 * gemm_device_parameters_buffer_alignment, sizeof(::framework::algorithm::gemm::device_parameters));
	}
}

::framework::nn::mlp::network::network(::framework::nn::mlp::execution_policy const & execution_policy, ::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu_log * gpu_log, ::framework::gpu::component_type parameter_type, ::framework::gpu::component_type accumulator_type, ::std::uint32_t max_batch_size, ::std::uint32_t max_inference_batch_size, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::descriptor_pool * descriptor_pool) :
	execution_policy(execution_policy)
{
	::std::size_t const parameter_type_size = ::framework::gpu::utility::get_component_type_size(parameter_type);
	::std::uint32_t const input_weights_stride = execution_policy.padded_input_width * execution_policy.hidden_width;
	::std::uint32_t const hidden_weights_stride = execution_policy.hidden_width * execution_policy.hidden_width;
	::std::uint32_t const output_weights_stride = execution_policy.hidden_width * execution_policy.padded_output_width;
	::std::uint32_t const input_layer_stride = execution_policy.padded_input_width * max_batch_size;
	::std::uint32_t const hidden_layer_stride = execution_policy.hidden_width * max_batch_size;
	::std::uint32_t const output_layer_stride = execution_policy.padded_output_width * max_batch_size;
	::std::uint32_t const inference_input_layer_stride = execution_policy.padded_input_width * max_inference_batch_size;
	::std::uint32_t const inference_hidden_layer_stride = execution_policy.hidden_width * max_inference_batch_size;
	::std::uint32_t const inference_output_layer_stride = execution_policy.padded_output_width * max_inference_batch_size;
	::std::uint32_t const hidden_layer_count = execution_policy.hidden_matmul_count + 1;
	::framework::gpu::device_size const gemm_device_parameters_buffer_alignment = (sizeof(::framework::algorithm::gemm::device_parameters) + gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1) & ~(gpu_context->get_physical_device_properties().limits.min_uniform_buffer_offset_alignment - 1);
	
	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy.parameter_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->weight_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->weight_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->weight_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->weight_matrices_buffer;
		bind_buffer_memory_info.memory = this->weight_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy.parameter_count * sizeof(float);
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->full_precision_weights_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->full_precision_weights_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->full_precision_weights_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->full_precision_weights_matrices_buffer;
		bind_buffer_memory_info.memory = this->full_precision_weights_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = this->execution_policy.parameter_count * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->gradient_matrices_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->gradient_matrices_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->gradient_matrices_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->gradient_matrices_buffer;
		bind_buffer_memory_info.memory = this->gradient_matrices_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = output_layer_stride * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->dloss_doutput_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->dloss_doutput_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->output_layer_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->dloss_doutput_buffer;
		bind_buffer_memory_info.memory = this->output_layer_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = (hidden_layer_stride * hidden_layer_count + input_layer_stride) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->activations_gradient_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->activations_gradient_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->activations_gradient_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->activations_gradient_buffer;
		bind_buffer_memory_info.memory = this->activations_gradient_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(output_layer_stride, inference_output_layer_stride) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->output_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->output_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->output_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->output_buffer;
		bind_buffer_memory_info.memory = this->output_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		::framework::gpu::buffer_create_info buffer_create_info;
		buffer_create_info.flags = ::framework::gpu::buffer_create_flags::none;
		buffer_create_info.size = ::std::max(hidden_layer_stride, inference_hidden_layer_stride) * (hidden_layer_count + 1) * parameter_type_size;
		buffer_create_info.usage = ::framework::gpu::buffer_usage_flags::sampled_buffer_bit | ::framework::gpu::buffer_usage_flags::storage_buffer_bit;
		buffer_create_info.sharing_mode = ::framework::gpu::sharing_mode::exclusive;
		buffer_create_info.queue_family_index_count = 0;
		buffer_create_info.queue_family_indices = nullptr;
		buffer_create_info.initial_queue_family_index = gpu_context->get_queue_family_indices().compute;
		buffer_create_info.initial_state = ::framework::gpu::buffer_state_flags::transfer_dst_bit;
		buffer_create_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->create_buffer(&buffer_create_info, nullptr, &this->activations_buffer));

		::framework::gpu::memory_requirements memory_requirements;
		::framework::gpu::buffer_memory_requirements_info buffer_memory_requirements_info;
		buffer_memory_requirements_info.buffer = this->activations_buffer;
		gpu_context->get_device()->get_buffer_memory_requirements(&buffer_memory_requirements_info, &memory_requirements);

		::framework::gpu::memory_allocate_info memory_allocate_info;
		memory_allocate_info.flags = ::framework::gpu::memory_allocate_flags::device_address_bit;
		memory_allocate_info.device_mask = 0;
		memory_allocate_info.allocation_size = memory_requirements.size;
		memory_allocate_info.allocation_alignment = memory_requirements.alignment;
		memory_allocate_info.memory_type_index = ::framework::gpu::utility::get_memory_type_index(gpu_context->get_physical_device_memory_properties(), memory_requirements.memory_type_bits, ::framework::gpu::memory_property_flags::device_local_bit);
		memory_allocate_info.memory_dedicated_allocate_info.buffer = nullptr;
		memory_allocate_info.memory_dedicated_allocate_info.image = nullptr;
		memory_allocate_info.opaque_capture_address = 0;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_memory(&memory_allocate_info, nullptr, &this->activations_device_memory));

		::framework::gpu::bind_buffer_memory_info bind_buffer_memory_info;
		bind_buffer_memory_info.buffer = this->activations_buffer;
		bind_buffer_memory_info.memory = this->activations_device_memory;
		bind_buffer_memory_info.memory_offset = 0;
		assert_framework_gpu_result(gpu_context->get_device()->bind_buffer_memory(1, &bind_buffer_memory_info));
	}

	{
		this->gemm_descriptor_sets.resize(8ull + this->execution_policy.hidden_matmul_count * 4ull);
		::std::vector<::framework::gpu::descriptor_set_layout *> descriptor_set_layouts(this->gemm_descriptor_sets.size() + 1);
		::std::vector<::framework::gpu::descriptor_set *> descriptor_sets(this->gemm_descriptor_sets.size() + 1);
		::std::fill(descriptor_set_layouts.begin(), descriptor_set_layouts.end() - 1, this->execution_policy.gemm.get_descriptor_set_layout());
		descriptor_set_layouts.back() = this->execution_policy.gpu_log_descriptor_set_layout;

		::framework::gpu::descriptor_set_allocate_info descriptor_set_allocate_info;
		descriptor_set_allocate_info.descriptor_pool = descriptor_pool;
		descriptor_set_allocate_info.descriptor_set_count = static_cast<::std::uint32_t>(descriptor_set_layouts.size());
		descriptor_set_allocate_info.set_layouts = descriptor_set_layouts.data();
		descriptor_set_allocate_info.variable_descriptor_counts = nullptr;
		assert_framework_gpu_result(gpu_context->get_device()->allocate_descriptor_sets(&descriptor_set_allocate_info, descriptor_sets.data()));
		::std::copy(descriptor_sets.begin(), descriptor_sets.end() - 1, this->gemm_descriptor_sets.begin());
		this->gpu_log_descriptor_set = descriptor_sets.back();
	}

	{
		::std::uint32_t const descriptor_buffer_info_count = 6 * this->gemm_descriptor_sets.size();
		::std::uint32_t current_gemm_descriptor_set_index = 0;
		::std::vector<::framework::gpu::descriptor_buffer_info> descriptor_buffer_infos(descriptor_buffer_info_count);
		::std::vector<::framework::gpu::write_descriptor_set> write_descriptor_sets(descriptor_buffer_info_count);

		auto add_gemm_write_descriptor_sets = [&](::std::array<::framework::gpu::buffer *, 6> buffers, ::std::array<::framework::gpu::device_size, 6> offsets, ::std::array<::framework::gpu::device_size, 6> ranges)
		{
			::std::uint32_t const write_descriptor_sets_index = current_gemm_descriptor_set_index * 6;

			for (::std::uint32_t i = 0; i < 6; i++)
			{
				descriptor_buffer_infos[write_descriptor_sets_index + i].flags = ::framework::gpu::descriptor_buffer_info_flags::none;
				descriptor_buffer_infos[write_descriptor_sets_index + i].buffer = buffers[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].offset = offsets[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].range = ranges[i];
				descriptor_buffer_infos[write_descriptor_sets_index + i].stride = parameter_type_size;

				write_descriptor_sets[write_descriptor_sets_index + i].dst_set = this->gemm_descriptor_sets[current_gemm_descriptor_set_index];
				write_descriptor_sets[write_descriptor_sets_index + i].dst_binding = i;
				write_descriptor_sets[write_descriptor_sets_index + i].dst_array_element = 0;
				write_descriptor_sets[write_descriptor_sets_index + i].descriptor_count = 1;
				write_descriptor_sets[write_descriptor_sets_index + i].descriptor_type = i != 5 ? ::framework::gpu::descriptor_type::storage_buffer : ::framework::gpu::descriptor_type::uniform_buffer;
				write_descriptor_sets[write_descriptor_sets_index + i].image_info = nullptr;
				write_descriptor_sets[write_descriptor_sets_index + i].buffer_info = &descriptor_buffer_infos[write_descriptor_sets_index + i];
				write_descriptor_sets[write_descriptor_sets_index + i].texel_buffer_view = nullptr;
				write_descriptor_sets[write_descriptor_sets_index + i].acceleration_structures = nullptr;
			}

			current_gemm_descriptor_set_index++;
		};

		// input_weights * input = 0'th hidden layer's activations
		add_gemm_write_descriptor_sets
		(
			{ 
				this->weight_matrices_buffer,
				input_buffer,
				this->activations_buffer,
				this->activations_buffer,
				this->activations_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{ 
				0,
				0,
				0,
				0,
				0,
				0 * gemm_device_parameters_buffer_alignment
			},
			{
				input_weights_stride * parameter_type_size,
				inference_input_layer_stride * parameter_type_size,
				inference_hidden_layer_stride * parameter_type_size,
				inference_hidden_layer_stride * parameter_type_size,
				inference_hidden_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
		{
			// i'th weight matrices * i'th hidden layer's activations = (i + 1)'th hidden layer's activations
			add_gemm_write_descriptor_sets
			(
				{
					this->weight_matrices_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->execution_policy.gemm_parameters_buffer
				},
				{
					(input_weights_stride + hidden_weights_stride * i) * parameter_type_size,
					inference_hidden_layer_stride * (i + 0) * parameter_type_size,
					inference_hidden_layer_stride * (i + 1) * parameter_type_size,
					inference_hidden_layer_stride * (i + 1) * parameter_type_size,
					inference_hidden_layer_stride * (i + 1) * parameter_type_size,
					1 * gemm_device_parameters_buffer_alignment
				},
				{
					hidden_weights_stride * parameter_type_size,
					inference_hidden_layer_stride * parameter_type_size,
					inference_hidden_layer_stride * parameter_type_size,
					inference_hidden_layer_stride * parameter_type_size,
					inference_hidden_layer_stride * parameter_type_size,
					sizeof(::framework::algorithm::gemm::device_parameters)
				}
			);
		}

		// output_weights * last hidden layer's activations = output layer's activations
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_buffer,
				this->output_buffer,
				this->output_buffer,
				this->output_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				inference_hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				2 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				inference_hidden_layer_stride * parameter_type_size,
				inference_output_layer_stride * parameter_type_size,
				inference_output_layer_stride * parameter_type_size,
				inference_output_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);
		
		// input_weights * input = 0'th hidden layer's activations
		add_gemm_write_descriptor_sets
		(
			{ 
				this->weight_matrices_buffer,
				input_buffer,
				this->activations_buffer,
				this->activations_buffer,
				this->activations_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{ 
				0,
				0,
				0,
				0,
				0,
				3 * gemm_device_parameters_buffer_alignment
			},
			{
				input_weights_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
		{
			// i'th weight matrices * i'th hidden layer's activations = (i + 1)'th hidden layer's activations
			add_gemm_write_descriptor_sets
			(
				{
					this->weight_matrices_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->activations_buffer,
					this->execution_policy.gemm_parameters_buffer
				},
				{
					(input_weights_stride + hidden_weights_stride * i) * parameter_type_size,
					hidden_layer_stride * (i + 0) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					4 * gemm_device_parameters_buffer_alignment
				},
				{
					hidden_weights_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					sizeof(::framework::algorithm::gemm::device_parameters)
				}
			);
		}

		// output_weights * last hidden layer's activations = output layer's activations
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_buffer,
				this->output_buffer,
				this->output_buffer,
				this->output_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				5 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// loss_gradient * last_hidden_post_activations = output_weight's gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->dloss_doutput_buffer,
				this->activations_buffer,
				this->gradient_matrices_buffer,
				this->gradient_matrices_buffer,
				this->execution_policy.split_k_slices_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				0,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				0,
				6 * gemm_device_parameters_buffer_alignment
			},
			{
				// padded_output_width x batch_size
				this->execution_policy.padded_output_width * max_batch_size * parameter_type_size,
				// transpose(hidden_width x batch_size)
				hidden_layer_stride * parameter_type_size,
				// padded_output_width x hidden_width
				output_weights_stride * parameter_type_size,
				// padded_output_width x hidden_width
				output_weights_stride * parameter_type_size,
				::framework::gpu::whole_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// output_weight_matrix * loss_gradient = last hidden layer's activations gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->dloss_doutput_buffer,
				this->activations_gradient_buffer,
				this->activations_gradient_buffer,
				this->activations_gradient_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				(input_weights_stride + hidden_weights_stride * this->execution_policy.hidden_matmul_count) * parameter_type_size,
				0,
				0,
				0,
				0,
				7 * gemm_device_parameters_buffer_alignment
			},
			{
				output_weights_stride * parameter_type_size,
				output_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
		{
			// (i + 1)'th hidden_gradient * i'th hidden_post_activations = i'th hidden_weight's gradient
			add_gemm_write_descriptor_sets
			(
				{
					this->activations_gradient_buffer,
					this->activations_buffer,
					this->gradient_matrices_buffer,
					this->gradient_matrices_buffer,
					this->execution_policy.split_k_slices_buffer,
					this->execution_policy.gemm_parameters_buffer
				},
				{
					hidden_layer_stride * i * parameter_type_size,
					hidden_layer_stride * (this->execution_policy.hidden_matmul_count - 1 - i) * parameter_type_size,
					(input_weights_stride + hidden_weights_stride * (this->execution_policy.hidden_matmul_count - 1 - i)) * parameter_type_size,
					(input_weights_stride + hidden_weights_stride * (this->execution_policy.hidden_matmul_count - 1 - i)) * parameter_type_size,
					0,
					8 * gemm_device_parameters_buffer_alignment
				},
				{
					// hidden_width x batch_size
					hidden_layer_stride * parameter_type_size,
					// transpose(hidden_width x batch_size)
					hidden_layer_stride * parameter_type_size,
					// hidden_width x hidden_width
					hidden_weights_stride * parameter_type_size,
					// hidden_width x hidden_width
					hidden_weights_stride * parameter_type_size,
					::framework::gpu::whole_size,
					sizeof(::framework::algorithm::gemm::device_parameters)
				}
			);

			// i'th weight_matrix * (i + 1)'th layer's activations gradient = i'th layer's activations gradient
			add_gemm_write_descriptor_sets
			(
				{
					this->weight_matrices_buffer,
					this->activations_gradient_buffer,
					this->activations_gradient_buffer,
					this->activations_gradient_buffer,
					this->activations_gradient_buffer,
					this->execution_policy.gemm_parameters_buffer
				},
				{
					(input_weights_stride + hidden_weights_stride * (this->execution_policy.hidden_matmul_count - 1 - i)) * parameter_type_size,
					hidden_layer_stride * (i + 0) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					hidden_layer_stride * (i + 1) * parameter_type_size,
					9 * gemm_device_parameters_buffer_alignment
				},
				{
					hidden_weights_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					hidden_layer_stride * parameter_type_size,
					sizeof(::framework::algorithm::gemm::device_parameters)
				}
			);
		}

		// 0'th hidden_gradient * input = input_weight's gradient
		add_gemm_write_descriptor_sets
		(
			{
				this->activations_gradient_buffer,
				input_buffer,
				this->gradient_matrices_buffer,
				this->gradient_matrices_buffer,
				this->execution_policy.split_k_slices_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				0,
				10 * gemm_device_parameters_buffer_alignment
			},
			{
				// hidden_width x batch_size
				hidden_layer_stride * parameter_type_size,
				// transpose(padded_input_width x batch_size)
				::framework::gpu::whole_size,
				// hidden_width x padded_input_width
				input_weights_stride * parameter_type_size,
				// hidden_width x padded_input_width
				input_weights_stride * parameter_type_size,
				::framework::gpu::whole_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		// input_weight_matrix * 0'th hidden layer's activations gradient = dL_dinput
		add_gemm_write_descriptor_sets
		(
			{
				this->weight_matrices_buffer,
				this->activations_gradient_buffer,
				dloss_dinput_buffer,
				dloss_dinput_buffer,
				dloss_dinput_buffer,
				this->execution_policy.gemm_parameters_buffer
			},
			{
				0,
				hidden_layer_stride * this->execution_policy.hidden_matmul_count * parameter_type_size,
				0,
				0,
				0,
				11 * gemm_device_parameters_buffer_alignment
			},
			{
				input_weights_stride * parameter_type_size,
				hidden_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				input_layer_stride * parameter_type_size,
				sizeof(::framework::algorithm::gemm::device_parameters)
			}
		);

		gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets.data(), 0, nullptr);
	}

	gpu_log->write_descriptor_set(this->gpu_log_descriptor_set);
}

void ::framework::nn::mlp::network::update_initialize_parameters_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	descriptor_buffer_infos[0].buffer = this->full_precision_weights_matrices_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 4;

	descriptor_buffer_infos[1].buffer = this->weight_matrices_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	// parameters_full_precision_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// parameters_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;
	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

void ::framework::nn::mlp::network::update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * input_buffer, ::framework::gpu::buffer * dloss_dinput_buffer, ::framework::gpu::buffer * batch_size_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	::framework::gpu::descriptor_buffer_info descriptor_buffer_infos[2];
	// activations_buffer
	descriptor_buffer_infos[0].buffer = this->activations_buffer;
	descriptor_buffer_infos[0].offset = 0;
	descriptor_buffer_infos[0].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[0].stride = 2;
	// activations_gradient_buffer
	descriptor_buffer_infos[1].buffer = this->activations_gradient_buffer;
	descriptor_buffer_infos[1].offset = 0;
	descriptor_buffer_infos[1].range = ::framework::gpu::whole_size;
	descriptor_buffer_infos[1].stride = 2;

	::framework::gpu::write_descriptor_set write_descriptor_sets[2];
	// activations_buffer
	write_descriptor_sets[0].dst_set = descriptor_set;
	write_descriptor_sets[0].dst_binding = 0;
	write_descriptor_sets[0].dst_array_element = 0;
	write_descriptor_sets[0].descriptor_count = 1;
	write_descriptor_sets[0].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[0].image_info = nullptr;
	write_descriptor_sets[0].buffer_info = &descriptor_buffer_infos[0];
	write_descriptor_sets[0].texel_buffer_view = nullptr;
	write_descriptor_sets[0].acceleration_structures = nullptr;
	// activations_gradient_buffer
	write_descriptor_sets[1].dst_set = descriptor_set;
	write_descriptor_sets[1].dst_binding = 1;
	write_descriptor_sets[1].dst_array_element = 0;
	write_descriptor_sets[1].descriptor_count = 1;
	write_descriptor_sets[1].descriptor_type = ::framework::gpu::descriptor_type::storage_buffer;
	write_descriptor_sets[1].image_info = nullptr;
	write_descriptor_sets[1].buffer_info = &descriptor_buffer_infos[1];
	write_descriptor_sets[1].texel_buffer_view = nullptr;
	write_descriptor_sets[1].acceleration_structures = nullptr;

	gpu_context->get_device()->update_descriptor_sets(static_cast<::std::uint32_t>(::std::size(write_descriptor_sets)), write_descriptor_sets, 0, nullptr);
}

void ::framework::nn::mlp::network::initialize_xavier_uniform(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set)
{
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::framework::local::initialize_parameters_push_constants push_constants;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.initialize_parameters_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.initialize_parameters_pipeline_layout, 0, 1, &descriptor_set, 0, nullptr);
	push_constants.offset = 0;
	push_constants.count = this->execution_policy.padded_input_width * this->execution_policy.hidden_width;
	push_constants.scale = ::std::sqrt(6.0f / (this->execution_policy.input_width * this->execution_policy.hidden_width));
	command_buffer->push_constants(this->execution_policy.initialize_parameters_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((push_constants.count + 128 - 1) / 128, 1, 1);
	push_constants.offset += push_constants.count;
	push_constants.count = this->execution_policy.hidden_width * this->execution_policy.hidden_width;
	push_constants.scale = ::std::sqrt(6.0f / push_constants.count);
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; i++)
	{
		command_buffer->push_constants(this->execution_policy.initialize_parameters_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.count + 128 - 1) / 128, 1, 1);
		push_constants.offset += push_constants.count;
	}
	push_constants.count = this->execution_policy.hidden_width * this->execution_policy.padded_output_width;
	push_constants.scale = ::std::sqrt(6.0f / (this->execution_policy.hidden_width * this->execution_policy.output_width));
	command_buffer->push_constants(this->execution_policy.initialize_parameters_pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((push_constants.count + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
}

void ::framework::nn::mlp::network::inference(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 0;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	/* GEMM: input_weights * input = 0'th hidden layer's activations */
	this->execution_policy.inference_input_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);

	::std::uint32_t const hidden_layer_stride = this->execution_policy.hidden_width * batch_size;
	::std::uint32_t const output_layer_stride = this->execution_policy.padded_output_width * batch_size;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::framework::local::activation_push_constants push_constants;
	push_constants.activations_offset = 0;
	push_constants.size = hidden_layer_stride;
	push_constants.activation = 0;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.inference_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
	command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// hidden layers
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
	{
		/* GEMM: i'th weight matrices * i'th hidden layer's activations = (i + 1)'th hidden layer's activations */
		this->execution_policy.inference_hidden_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		push_constants.activations_offset = (i + 1) * hidden_layer_stride;
		push_constants.size = hidden_layer_stride;
		push_constants.activation = 0;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.inference_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
		command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	/* GEMM: output_weights * last hidden layer's activations = output layer's activations */
	this->execution_policy.inference_output_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_output_width, batch_size);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	if (false/*has output_activation*/)
	{
		push_constants.activations_offset = 0; // todo: here, the output_buffer should be used instead of post_activations
		push_constants.size = output_layer_stride;
		push_constants.activation = 0;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.inference_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
		command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::mlp::network::forward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 2ull + this->execution_policy.hidden_matmul_count;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	/* GEMM: input_weights * input = 0'th hidden layer's activations */
	this->execution_policy.forward_input_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);

	::std::uint32_t const hidden_layer_stride = this->execution_policy.hidden_width * batch_size;
	::std::uint32_t const output_layer_stride = this->execution_policy.padded_output_width * batch_size;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::framework::local::activation_push_constants push_constants;
	push_constants.activations_offset = 0;
	push_constants.size = hidden_layer_stride;
	push_constants.activation = 0;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.activation_forward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
	command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// hidden layers
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
	{
		/* GEMM: i'th weight matrices * i'th hidden layer's activations = (i + 1)'th hidden layer's activations */
		this->execution_policy.forward_hidden_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		push_constants.activations_offset = (i + 1) * hidden_layer_stride;
		push_constants.size = hidden_layer_stride;
		push_constants.activation = 0;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.activation_forward_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
		command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
	/* GEMM: output_weights * last hidden layer's activations = output layer's activations */
	this->execution_policy.forward_output_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_output_width, batch_size);
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	if (false/*has output_activation*/)
	{
		push_constants.activations_offset = 0; // todo: here, the output_buffer should be used instead of post_activations
		push_constants.size = output_layer_stride;
		push_constants.activation = 0;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.activation_forward_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
		command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}

void ::framework::nn::mlp::network::backward(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t batch_size)
{
	::std::uint32_t current_gemm_descriptor_set_index = 4ull + this->execution_policy.hidden_matmul_count * 2ull;

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	::std::uint32_t const split_k_slices = batch_size / ::std::min(batch_size, 1u << 12);

	/* GEMM_SPLIT_K: loss_gradient * last_hidden_post_activations = output_weight's gradient */
	this->execution_policy.backward_output_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_output_width, this->execution_policy.hidden_width, split_k_slices);
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_output_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_output_width, this->execution_policy.hidden_width);
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	/* GEMM: output_weight_matrix * loss_gradient = last layer's activations gradient */
	this->execution_policy.backward_output_layer_gradient_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	::std::uint32_t const hidden_layer_stride = this->execution_policy.hidden_width * batch_size;
	::std::uint32_t const output_layer_stride = this->execution_policy.padded_output_width * batch_size;
	::framework::gpu::descriptor_set * descriptor_sets[]
	{
		descriptor_set,
		this->gpu_log_descriptor_set
	};
	::framework::local::activation_push_constants push_constants;
	push_constants.activations_offset = hidden_layer_stride * this->execution_policy.hidden_matmul_count;
	push_constants.activations_gradient_offset = 0;
	push_constants.size = hidden_layer_stride;
	push_constants.activation = 0;
	command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.activation_backward_pipeline);
	command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
	command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
	command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);

	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}

	// hidden layers
	for (::std::uint32_t i = 0; i < this->execution_policy.hidden_matmul_count; ++i)
	{
		/* GEMM_SPLIT_K: (i + 1)'th hidden_gradient * i'th hidden_post_activations = i'th hidden_weight's gradient */
		this->execution_policy.backward_hidden_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.hidden_width, this->execution_policy.hidden_width, split_k_slices);
		{
			::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
			buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
			buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
			buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
			buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
			buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
			buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
			buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
			buffer_memory_barrier.offset = 0;
			buffer_memory_barrier.size = ::framework::gpu::whole_size;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 0;
			dependency_info.memory_barriers = nullptr;
			dependency_info.buffer_memory_barrier_count = 1;
			dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
		this->execution_policy.backward_hidden_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.hidden_width, this->execution_policy.hidden_width);
		current_gemm_descriptor_set_index++;
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		/* GEMM: i'th weight_matrix * (i + 1)'th layer's activations gradient = i'th layer's activations gradient */
		this->execution_policy.backward_hidden_layer_gradient_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index++], this->execution_policy.hidden_width, batch_size);

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}

		push_constants.activations_offset = hidden_layer_stride * (this->execution_policy.hidden_matmul_count - 1 - i);
		push_constants.activations_gradient_offset = hidden_layer_stride * (i + 1);
		push_constants.size = hidden_layer_stride;
		push_constants.activation = 0;
		command_buffer->bind_pipeline(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.activation_backward_pipeline);
		command_buffer->bind_descriptor_sets(::framework::gpu::pipeline_bind_point::compute, this->execution_policy.pipeline_layout, 0, static_cast<::std::uint32_t>(::std::size(descriptor_sets)), descriptor_sets, 0, nullptr);
		command_buffer->push_constants(this->execution_policy.pipeline_layout, ::framework::gpu::shader_stage_flags::compute_bit, 0, 0, sizeof(push_constants), &push_constants);
		command_buffer->dispatch((push_constants.size + 128 - 1) / 128, 1, 1);

		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}

	/* GEMM_SPLIT_K: 0'th hidden_gradient * input = input_weight's gradient */
	this->execution_policy.backward_input_weights_gradient_matmul.execute_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.hidden_width, this->execution_policy.padded_input_width, split_k_slices);
	{
		::framework::gpu::buffer_memory_barrier buffer_memory_barrier;
		buffer_memory_barrier.src_access_mask = ::framework::gpu::access_flags::shader_storage_write_bit;
		buffer_memory_barrier.dst_access_mask = ::framework::gpu::access_flags::shader_storage_read_bit;
		buffer_memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::compute_shader_bit;
		buffer_memory_barrier.old_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.new_state = ::framework::gpu::buffer_state_flags::storage_buffer_bit;
		buffer_memory_barrier.src_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.src_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.dst_queue_family_ownership.flags = ::framework::gpu::queue_family_ownership_flags::queue_family_ignored;
		buffer_memory_barrier.dst_queue_family_ownership.queue_family_index = 0;
		buffer_memory_barrier.buffer = this->execution_policy.split_k_slices_buffer;
		buffer_memory_barrier.offset = 0;
		buffer_memory_barrier.size = ::framework::gpu::whole_size;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 0;
		dependency_info.memory_barriers = nullptr;
		dependency_info.buffer_memory_barrier_count = 1;
		dependency_info.buffer_memory_barriers = &buffer_memory_barrier;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	this->execution_policy.backward_input_weights_gradient_reduce_split_k.execute_reduce_split_k(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.hidden_width, this->execution_policy.padded_input_width);
	current_gemm_descriptor_set_index++;
	{
		::framework::gpu::memory_barrier memory_barrier;
		memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
		memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
		memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

		::framework::gpu::dependency_info dependency_info;
		dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
		dependency_info.memory_barrier_count = 1;
		dependency_info.memory_barriers = &memory_barrier;
		dependency_info.buffer_memory_barrier_count = 0;
		dependency_info.buffer_memory_barriers = nullptr;
		dependency_info.image_memory_barrier_count = 0;
		dependency_info.image_memory_barriers = nullptr;
		command_buffer->pipeline_barrier(&dependency_info);
	}
	// Compute loss gradients w.r.t. input if desired.
	if (this->execution_policy.compute_dloss_dinput)
	{
		// GEMM: input_weight_matrix * 0'th hidden layer's activations gradient = dL_dinput
		this->execution_policy.backward_input_layer_gradient_matmul.execute(this->execution_policy.gemm, command_buffer, this->gemm_descriptor_sets[current_gemm_descriptor_set_index], this->execution_policy.padded_input_width, batch_size);
		{
			::framework::gpu::memory_barrier memory_barrier;
			memory_barrier.src_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.dst_access_mask = ::framework::gpu::access_flags::memory_read_bit | ::framework::gpu::access_flags::memory_write_bit;
			memory_barrier.src_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;
			memory_barrier.dst_stage_mask = ::framework::gpu::pipeline_stage_flags::all_commands_bit;

			::framework::gpu::dependency_info dependency_info;
			dependency_info.dependency_flags = ::framework::gpu::dependency_flags::legacy_bit;
			dependency_info.memory_barrier_count = 1;
			dependency_info.memory_barriers = &memory_barrier;
			dependency_info.buffer_memory_barrier_count = 0;
			dependency_info.buffer_memory_barriers = nullptr;
			dependency_info.image_memory_barrier_count = 0;
			dependency_info.image_memory_barriers = nullptr;
			command_buffer->pipeline_barrier(&dependency_info);
		}
	}
}
