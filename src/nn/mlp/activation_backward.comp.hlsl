struct push_constants_t
{
	uint32_t activations_offset;
	uint32_t activations_gradient_offset;
	uint32_t size;
	uint32_t activation;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] RWStructuredBuffer<parameter_t> activations_buffer : register(u0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<parameter_t> activations_gradient_buffer : register(u1, space0);

parameter_t activation_backward(in parameter_t input, in parameter_t pre_activation)
{
	return input * parameter_t(pre_activation > parameter_t(0.0));
}

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	if (dispatch_thread_id.x >= push_constants.size)
	{
		return;
	}

	const uint activations_index = push_constants.activations_offset + dispatch_thread_id.x;
	const uint activations_gradient_index = push_constants.activations_gradient_offset + dispatch_thread_id.x;
	activations_gradient_buffer[activations_gradient_index] = activation_backward(activations_gradient_buffer[activations_gradient_index], activations_buffer[activations_index]);
}
