struct push_constants_t
{
	uint32_t activations_offset;
	uint32_t activations_gradient_offset;
	uint32_t size;
	uint32_t activation;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] RWStructuredBuffer<parameter_t> activations_buffer : register(u0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<parameter_t> activations_gradient_buffer : register(u1, space0);

parameter_t relu(parameter_t val)
{
	return max(val, parameter_t(0.0));
}

parameter_t activation(in parameter_t input)
{
	return relu(input);
}

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	if (dispatch_thread_id.x >= push_constants.size)
	{
		return;
	}

	const uint index = push_constants.activations_offset + dispatch_thread_id.x;
	activations_buffer[index] = activation(activations_buffer[index]);
}
