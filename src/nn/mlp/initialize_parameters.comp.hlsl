struct push_constants_t
{
	uint32_t count;
	uint32_t offset;
	float scale;
};

[[vk::push_constant]] ConstantBuffer<push_constants_t> push_constants : register(b0, space0);

[[vk::binding(0, 0)]] RWStructuredBuffer<float> parameters_full_precision_buffer : register(u0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<parameter_t> parameters_buffer : register(u1, space0);

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return (word >> 8) * 0x1p-24;
}

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	if (dispatch_thread_id.x >= push_constants.count)
	{
		return;
	}

	const uint parameter_index = push_constants.offset + dispatch_thread_id.x;
	uint rand_state = parameter_index;
	const float parameter = rand(rand_state) * 2.0f * push_constants.scale - push_constants.scale;
	parameters_full_precision_buffer[parameter_index] = parameter;
	parameters_buffer[parameter_index] = parameter_t(parameter);
}
