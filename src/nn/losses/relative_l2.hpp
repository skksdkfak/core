#pragma once

#include "gpu/core.hpp"
#include "nn/loss.hpp"
#include <cstdint>

namespace framework
{
	class gpu_context;
}

namespace framework::resource
{
	class resource_manager;
}

namespace framework::nn
{
	class relative_l2 : public ::framework::nn::loss
	{
	public:
		relative_l2(::framework::gpu_context * gpu_context, ::framework::resource::resource_manager * resource_manager, ::framework::gpu::component_type prediction_type, ::framework::gpu::component_type pdf_type, ::framework::gpu::component_type target_type, ::std::uint32_t stride, ::std::uint32_t dims, float loss_scale, bool has_loss_buffer, bool has_pdf_buffer);

		::framework::gpu::descriptor_set_layout * get_descriptor_set_layout() const noexcept;

		void update_descriptor_sets(::framework::gpu_context * gpu_context, ::framework::gpu::buffer * size_buffer, ::framework::gpu::device_size size_offset, ::framework::gpu::buffer * predictions_buffer, ::framework::gpu::buffer * pdf_buffer, ::framework::gpu::buffer * targets_buffer, ::framework::gpu::buffer * loss_buffer, ::framework::gpu::buffer * gradients_buffer, ::framework::gpu::descriptor_set * descriptor_set);

		void evaluate(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::std::uint32_t size) override;

		void evaluate_indirect(::framework::gpu::command_buffer * command_buffer, ::framework::gpu::descriptor_set * descriptor_set, ::framework::gpu::buffer * dispatch_indirect_command_buffer, ::framework::gpu::device_size dispatch_indirect_command_offset) override;

	private:
		::framework::gpu::descriptor_set_layout * descriptor_set_layout;
		::framework::gpu::pipeline_layout * pipeline_layout;
		::framework::gpu::shader_module * shader_module;
		::framework::gpu::pipeline * pipeline;
	};
}

#include "nn/losses/relative_l2.inl"