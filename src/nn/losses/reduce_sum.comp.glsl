#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_scalar_block_layout : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable
#extension GL_EXT_debug_printf : enable

layout(local_size_x = 1) in;

layout(std140, push_constant) uniform push_constants
{
	uint n_elements;
	uint stride;
	uint dims;
	float loss_scale;
};

layout(std430, set = 0, binding = 0) readonly buffer predictions_buffer_t
{
	ELEMENT_TYPE predictions_buffer[];
};

layout(std430, set = 0, binding = 1) readonly buffer targets_buffer_t
{
	float targets_buffer[];
};

layout(std430, set = 0, binding = 2) buffer loss_buffer_t
{
	float loss_buffer[];
};

layout(std430, set = 0, binding = 3) writeonly buffer gradients_buffer_t
{
    ELEMENT_TYPE gradients_buffer[];
};

void main()
{
	return;
	float total_loss = 0.0f;
	for (uint i = 0; i < n_elements; i++)
	{
		total_loss += loss_buffer[i];
	}
	debugPrintfEXT("%f\n", total_loss, n_elements);
}
