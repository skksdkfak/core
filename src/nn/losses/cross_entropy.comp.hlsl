[[vk::constant_id(0)]] const uint32_t stride = 1;
[[vk::constant_id(1)]] const uint32_t dims = 1;
[[vk::constant_id(2)]] const float loss_scale = 1.0f;
[[vk::constant_id(3)]] const bool has_pdf_buffer = false;
[[vk::constant_id(4)]] const bool is_log_prediction = false;

[[vk::binding(0, 0)]] cbuffer size_buffer_t : register(b0, space0)
{
	uint size;
};

[[vk::binding(1, 0)]] StructuredBuffer<prediction_t> predictions_buffer : register(t0, space0);

[[vk::binding(2, 0)]] StructuredBuffer<pdf_t> pdf_buffer : register(t1, space0);

[[vk::binding(3, 0)]] StructuredBuffer<target_t> targets_buffer : register(t2, space0);

[[vk::binding(4, 0)]] RWStructuredBuffer<float> loss_buffer : register(u0, space0);

[[vk::binding(5, 0)]] RWStructuredBuffer<gradient_t> gradients_buffer : register(u1, space0);

[numthreads(128, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	const uint i = dispatch_thread_id.x;
	const uint intra_elem_idx = i % stride;
	const uint inter_elem_idx = i / stride;
	if (inter_elem_idx >= size)
	{
		return;
	}

	if (intra_elem_idx >= dims)
	{
		loss_buffer[i] = 0;
		gradients_buffer[i] = gradient_t(0);
		return;
	}

	const uint target_idx = inter_elem_idx * dims + intra_elem_idx;
	const uint n_total = size * dims;
	const float prediction_tmp = float(predictions_buffer[i]);
	const float log_prediction = is_log_prediction ? prediction_tmp : log(prediction_tmp);
	const float prediction = is_log_prediction ? exp(prediction_tmp) : prediction_tmp;
	const float target = float(targets_buffer[target_idx]);
	const float pdf = has_pdf_buffer ? float(pdf_buffer[target_idx]) : 1.0f;
	const float factor = -target / pdf / n_total;
	const float loss = factor * log_prediction;
	const float gradient = factor / prediction;

	loss_buffer[i] = loss;
	gradients_buffer[i] = gradient_t(loss_scale * gradient);
}
