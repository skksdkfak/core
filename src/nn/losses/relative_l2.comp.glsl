#version 450 core

#pragma use_vulkan_memory_model

#extension GL_EXT_scalar_block_layout : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable

layout(constant_id = 0) const uint stride = 1;
layout(constant_id = 1) const uint dims = 1;
layout(constant_id = 2) const float loss_scale = 1.0f;
layout(constant_id = 3) const bool has_loss_buffer = false;
layout(constant_id = 4) const bool has_pdf_buffer = false;

layout(local_size_x = 128) in;

layout(set = 0, binding = 0) uniform size_buffer_t
{
	uint size;
};

layout(std430, set = 0, binding = 1) readonly buffer predictions_buffer_t
{
	prediction_t predictions_buffer[];
};

layout(std430, set = 0, binding = 2) readonly buffer pdf_buffer_t
{
	pdf_t pdf_buffer[];
};

layout(std430, set = 0, binding = 3) readonly buffer targets_buffer_t
{
	target_t targets_buffer[];
};

layout(std430, set = 0, binding = 4) writeonly buffer loss_buffer_t
{
	float loss_buffer[];
};

layout(std430, set = 0, binding = 5) writeonly buffer gradients_buffer_t
{
	gradient_t gradients_buffer[];
};

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint intra_elem_idx = i % stride;
	const uint inter_elem_idx = i / stride;
	if (inter_elem_idx >= size)
	{
		return;
	}

	if (intra_elem_idx >= dims)
	{
		if (has_loss_buffer)
		{
			loss_buffer[i] = 0;
		}
		gradients_buffer[i] = prediction_t(0);
		return;
	}

	const uint target_idx = inter_elem_idx * dims + intra_elem_idx;
	const uint n_total = size * dims;
	const float prediction = float(predictions_buffer[i]);
	const float prediction_sq_plus_epsilon = prediction * prediction + 0.01f;
	const float pdf = has_pdf_buffer ? float(pdf_buffer[target_idx]) : 1.0f;
	const float difference = prediction - float(targets_buffer[target_idx]);
	const float gradient = 2 * difference / prediction_sq_plus_epsilon / pdf;

	if (has_loss_buffer)
	{
		loss_buffer[i] = difference * difference / prediction_sq_plus_epsilon / pdf / n_total;
	}

	gradients_buffer[i] = prediction_t(loss_scale * gradient / n_total);
}
