#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"

struct push_constant_t
{
	uint resample_pass;
};

[[vk::push_constant]] ConstantBuffer<push_constant_t> resample_pass : register(b1, space0);

[numthreads(8, 8, 1)]
[shader("compute")]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	//image[dispatchThreadID.xy] = pow(image[dispatchThreadID.xy], 2.2);
	g_initial_samples[dispatchThreadID.x] = g_resampled_samples[dispatchThreadID.x];
	//const uint gid = gl_GlobalInvocationID.y * 1280 + gl_GlobalInvocationID.x;
	//rand_state = gid + camera.seed;
	//const float kReuseRadius = 10.0;
	//reservoir_t r;
	//
	//if (resample_pass % 2 == 0)
	//{
	//	r = g_initial_samples[gid];
	//}
	//else
	//{
	//	//r = g_resampled_samples[gid];
	//}
	//
	////if (resample_pass != 234234234)
	////return;
	//	//if (r.M > 0 && resample_pass == 0)
	//	//{
	//	//	const vec2 pixelCenter = vec2(gl_GlobalInvocationID.xy) + vec2(0.5);
	//	//	const vec2 inUV = pixelCenter / vec2(1280, 720);
	//	//	vec2 d = inUV * 2.0 - 1.0;
	//	//	vec4 origin = camera.viewInverse * vec4(0, 0, 0, 1);
	//	//	vec4 target = camera.projInverse * vec4(d.x, d.y, 1, 1);
	//	//	vec4 direction = camera.viewInverse * vec4(normalize(target.xyz), 0);
	//	//
	//	//	rayQueryEXT query;
	//	//	rayQueryInitializeEXT(query, acceleration_structure, gl_RayFlagsNoOpaqueEXT, 0xff, r.z.xv, 0.01, normalize(r.z.xs - r.z.xv), length(r.z.xs - r.z.xv) * 1.9);
	//	//
	//	//	while (rayQueryProceedEXT(query))
	//	//	{
	//	//		//rayQueryConfirmIntersectionEXT(query);
	//	//		imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(1, 0, 0, 0));
	//	//		//return;
	//	//	}
	//	//
	//	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(0, 1, 0, 0));
	//	//	//if (rayQueryGetIntersectionTypeEXT(query, true) != gl_RayQueryCommittedIntersectionNoneEXT)
	//	//	//{
	//	//	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(rayQueryGetIntersectionTEXT(query, true) / 100.0));
	//	//	//}
	//	//	//else
	//	//	//{
	//	//	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(1,0,0,0));
	//	//	//}
	//	//}
	//	//return;
	//for (int reuse_idx = 0; reuse_idx < 0; ++reuse_idx)
	//{
	//	// Convert from [0, 1) to [-1, 1)
	//	vec2 rand_offset = vec2(rand(rand_state), rand(rand_state)) * 2.0 - 1.0;
	//	ivec2 current_pixel_pos = ivec2(gl_GlobalInvocationID.xy) + ivec2(rand_offset * kReuseRadius);
	//	uint current_pixel_index = current_pixel_pos.y * 1280 + current_pixel_pos.x;
	//
	//	if (gid == current_pixel_index)
	//	{
	//		continue;
	//	}
	//
	//	reservoir_t current_reservoir;
	//
	//	if (resample_pass % 2 == 0)
	//	{
	//		current_reservoir = g_initial_samples[current_pixel_index];
	//	}
	//	else
	//	{
	//		current_reservoir = g_resampled_samples[current_pixel_index];
	//	}
	//
	//	if (abs(current_reservoir.ray_length - r.ray_length) / r.ray_length > 0.5)
	//	{
	//		continue;
	//	}
	//
	//	// Bilaterally filter the neighbor reservoir
	//	const float kNormalThreshold = 0.9;
	//	float bilateral_weight = dot(r.normal, current_reservoir.normal);
	//	if (bilateral_weight < kNormalThreshold)
	//	{
	//		continue;
	//	}
	//
	//	//if (current_reservoir.W > 0 && r.M > 0)
	//	{
	//		float target_pdf = luminance(current_reservoir.z.radiance);
	//
	//		//float3 OffsetB = current_reservoir.z.xs - current_reservoir.z.xv;
	//		//float3 OffsetA = current_reservoir.z.xs - r.position;
	//		//// Discard back-face
	//		//if (dot(r.normal, OffsetA) < 0 || dot(current_reservoir.z.nv, r.normal) < 0.8)
	//		//{
	//		//	continue;
	//		//}
	//		//
	//		//float RB2 = dot(OffsetB, OffsetB);
	//		//float RA2 = dot(OffsetA, OffsetA);
	//		//OffsetB = normalize(OffsetB);
	//		//OffsetA = normalize(OffsetA);
	//		//
	//		//float cosA = dot(r.normal, OffsetB);
	//		//float cosB = dot(current_reservoir.z.nv, OffsetB);
	//		//float cosPhiA = -dot(OffsetA, current_reservoir.z.ns);
	//		//float cosPhiB = -dot(OffsetB, current_reservoir.z.ns);
	//		//
	//		//// Discard extreme samples to prevent fireflies
	//		//if (cosA <= 0 || cosB <= 0 || cosPhiA <= 0 || cosPhiB <= 0 || RA2 <= 0 || RB2 <= 0)
	//		//{
	//		//	continue;
	//		//}
	//		//
	//		//float k = RB2 * cosPhiA / (RA2 * cosPhiB);
	//		//target_pdf *= clamp(k, 0, 20);
	//		//
	//		//rayQueryEXT query;
	//		//rayQueryInitializeEXT(query, acceleration_structure, gl_RayFlagsTerminateOnFirstHitEXT, 0xff, r.position, 0.01, normalize(current_reservoir.z.xs - r.position), length(current_reservoir.z.xs - r.position) * 0.9);
	//		//
	//		//while (rayQueryProceedEXT(query))
	//		//{
	//		//}
	//		//
	//		//if (rayQueryGetIntersectionTypeEXT(query, true) != gl_RayQueryCommittedIntersectionNoneEXT)
	//		//{
	//		//	target_pdf = 0;
	//		//	continue;
	//		//}
	//
	//		merge_reservoir(r, current_reservoir, target_pdf);
	//	}
	//}
	//
	//float denom = r.M * luminance(r.z.radiance);
	//r.W = denom > 0 ? r.w / denom : 0;
	//
	//if (resample_pass % 2 == 0)
	//{
	//	g_resampled_samples[gid] = r;
	//}
	//else
	//{
	//	g_initial_samples[gid] = r;
	//}
	//
	//if (resample_pass < 4)
	//{
	//	//return;
	//}
	//
	//vec4 output_color = vec4(r.visible_point_throughput * r.z.radiance * r.W, 0.0);
	//if (camera.sample_count > 1)
	//{
	//	vec4 accumulated = imageLoad(accumulator, ivec2(gl_GlobalInvocationID.xy)) + output_color;
	//	imageStore(accumulator, ivec2(gl_GlobalInvocationID.xy), accumulated);
	//	output_color = accumulated / camera.sample_count;
	//}
	//else
	//{
	//	imageStore(accumulator, ivec2(gl_GlobalInvocationID.xy), output_color);
	//}
	//
	//imageStore(image, ivec2(gl_GlobalInvocationID.xy), output_color);
}
