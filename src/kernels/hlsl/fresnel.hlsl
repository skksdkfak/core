#pragma once

#include "complex.hlsl"
#include "math.hlsl"
#include "spectrum.hlsl"

namespace framework
{
	// Schlick's approximation to Fresnel term
	// f90 should be 1.0, except for the trick used by Schuler (see 'shadowedF90' function)
	float3 evalFresnelSchlick(float3 f0, float f90, float NdotS)
	{
		return f0 + (f90 - f0) * pow(1.0f - NdotS, 5.0f);
	}

	// Schlick's approximation to Fresnel term calculated using spherical gaussian approximation
	// Source: https://seblagarde.wordpress.com/2012/06/03/spherical-gaussien-approximation-for-blinn-phong-phong-and-fresnel/ by Lagarde
	float3 evalFresnelSchlickSphericalGaussian(float3 f0, float f90, float NdotV)
	{
		return f0 + (f90 - f0) * exp2((-5.55473f * NdotV - 6.983146f) * NdotV);
	}

	// Schlick's approximation to Fresnel term with Hoffman's improvement using the Lazanyi's error term
	// Source: "Fresnel Equations Considered Harmful" by Hoffman
	// Also see slides http://renderwonk.com/publications/mam2019/naty_mam2019.pdf for examples and explanation of f82 term
	float3 evalFresnelHoffman(float3 f0, float f82, float f90, float NdotS)
	{
		const float alpha = 6.0f; //< Fixed to 6 in order to put peak angle for Lazanyi's error term at 82 degrees (f82)
		float3 a = 17.6513846f * (f0 - f82) + 8.166666f * (float3(1.0f, 1.0f, 1.0f) - f0);
		return saturate(f0 + (f90 - f0) * pow(1.0f - NdotS, 5.0f) - a * NdotS * pow(1.0f - NdotS, alpha));
	}

	float3 evalFresnel(float3 f0, float f90, float NdotS)
	{
		// Default is Schlick's approximation
		return ::framework::evalFresnelSchlick(f0, f90, NdotS);
	}

	// Attenuates F90 for very low F0 values
	// Source: "An efficient and Physically Plausible Real-Time Shading Model" in ShaderX7 by Schuler
	// Also see section "Overbright highlights" in Hoffman's 2010 "Crafting Physically Motivated Shading Models for Game Development" for discussion
	// IMPORTANT: Note that when F0 is calculated using metalness, it's value is never less than MIN_DIELECTRICS_F0, and therefore,
	// this adjustment has no effect. To be effective, F0 must be authored separately, or calculated in different way. See main text for discussion.
	float shadowedF90(float3 F0)
	{
		// This scaler value is somewhat arbitrary, Schuler used 60 in his article. In here, we derive it from MIN_DIELECTRICS_F0 so
		// that it takes effect for any reflectance lower than least reflective dielectrics
		//const float t = 60.0f;
		const float t = (1.0f / MIN_DIELECTRICS_F0);
		return min(1.0f, t * luminance(F0));
	}

	float fresnel_complex(float cos_theta_i, ::framework::complex<float> eta_i, ::framework::complex<float> eta_t)
	{
		cos_theta_i = clamp(cos_theta_i, 0, 1);
		// Compute complex cos_theta_t for Fresnel equations using Snell's law
		float sin_2_theta_i = 1 - ::framework::square(cos_theta_i);
		::framework::complex<float> sin_2_theta_t = ::framework::complex<float>::create(sin_2_theta_i) / ::framework::square(eta_t);
		::framework::complex<float> cos_theta_t = ::framework::sqrt(::framework::complex<float>::create(1) - sin_2_theta_t);
		::framework::complex<float> cos_theta_i_complex = ::framework::complex<float>::create(cos_theta_i);

		::framework::complex<float> r_parl = (eta_t * cos_theta_i_complex - eta_i * cos_theta_t) / (eta_t * cos_theta_i_complex + eta_i * cos_theta_t);
		::framework::complex<float> r_perp = (eta_i * cos_theta_i_complex - eta_t * cos_theta_t) / (eta_i * cos_theta_i_complex + eta_t * cos_theta_t);
		return (::framework::norm(r_parl) + ::framework::norm(r_perp)) * 0.5;
	}

	::framework::sampled_spectrum fresnel_complex(float cos_theta_i, float3 eta_i, float3 eta_t, float3 k)
	{
		::framework::sampled_spectrum result;
		for (int i = 0; i < ::framework::sampled_spectrum::n; ++i)
			result.set(i, fresnel_complex(cos_theta_i, ::framework::complex<float>::create(eta_i[i]), ::framework::complex<float>::create(eta_t[i], k[i])));
		return result;
	}

	// https://seblagarde.wordpress.com/2013/04/29/memo-on-fresnel-equations/
	float3 fresnel_conductor(float cos_theta_i, float3 eta_i, float3 eta_t, float3 k)
	{
		cos_theta_i = clamp(cos_theta_i, -1, 1);
		float3 eta = eta_t / eta_i;
		float3 eta_k = k / eta_i;

		float cos_2_theta_i = cos_theta_i * cos_theta_i;
		float sin_2_theta_i = 1. - cos_2_theta_i;
		float3 eta_2 = eta * eta;
		float3 eta_k_2 = eta_k * eta_k;

		float3 t0 = eta_2 - eta_k_2 - sin_2_theta_i;
		float3 a_2_plus_b_2 = sqrt(t0 * t0 + 4 * eta_2 * eta_k_2);
		float3 t1 = a_2_plus_b_2 + cos_2_theta_i;
		float3 a = sqrt(0.5f * (a_2_plus_b_2 + t0));
		float3 t2 = 2 * cos_theta_i * a;
		float3 r_perp = (t1 - t2) / (t1 + t2);

		float3 t3 = cos_2_theta_i * a_2_plus_b_2 + sin_2_theta_i * sin_2_theta_i;
		float3 t4 = t2 * sin_2_theta_i;
		float3 r_parl = r_perp * (t3 - t4) / (t3 + t4);

		return 0.5 * (r_parl + r_perp);
	}

	/** Evaluates the Fresnel term using dieletric fresnel equations.
		Based on http://www.pbr-book.org/3ed-2018/Reflection_Models/Specular_Reflection_and_Transmission.html

		\param[in] eta_i Index of refraction of incident direction.
		\param[in] eta_t Index of refraction of transmitted direction.
		\param[in] cos_theta_i Cosine of angle between normal and incident direction.
		\param[out] cos_theta_t Cosine of angle between negative normal and transmitted direction (0 for total internal reflection).
		\return Returns Fr(eta, cos_theta_i).
	*/
	float fresnel_dielectric(float eta_i, float eta_t, float cos_theta_i, out float cos_theta_t)
	{
		float eta = eta_i / eta_t;
		float sin_2_theta_t = eta * eta * (1 - cos_theta_i * cos_theta_i);
		// Check for total internal reflection
		if (sin_2_theta_t >= 1)
		{
			cos_theta_t = 0;
			return 1;
		}

		cos_theta_t = sqrt(1 - sin_2_theta_t);

		float r_parl = (eta_t * cos_theta_i - eta_i * cos_theta_t) / (eta_t * cos_theta_i + eta_i * cos_theta_t);
		float r_perp = (eta_i * cos_theta_i - eta_t * cos_theta_t) / (eta_i * cos_theta_i + eta_t * cos_theta_t);

		return 0.5 * (r_parl * r_parl + r_perp * r_perp);
	}
}