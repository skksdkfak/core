#pragma once

namespace framework
{
	struct static_vertex_data
	{
		float3 position;
		float3 normal;
		float4 tangent;
		float2 uv;
	};

	struct packed_static_vertex_data
	{
		float3 position;
		float3 normal;
		float2 uv;

		::framework::static_vertex_data unpack()
		{
			::framework::static_vertex_data v;
			v.position = this.position;
			v.normal = this.normal;
			v.uv = this.uv;

			//v.normal.x = f16tof32(asuint(packedNormalTangent.x) & 0xffff);
			//v.normal.y = f16tof32(asuint(packedNormalTangent.x) >> 16);
			//v.normal.z = f16tof32(asuint(packedNormalTangent.y) & 0xffff);
			//v.normal = normalize(v.normal);

			//v.tangent.xyz = decodeNormal2x16(asuint(packedNormalTangent.z));
			//v.tangent.w = f16tof32(asuint(packedNormalTangent.y) >> 16);

			return v;
		}
	};

	struct vertex_data
	{
		float3 position;            ///< Position in world space.
		float3 normal;         ///< Shading normal in world space (normalized).
		float4 tangent;        ///< Shading tangent in world space (normalized). The last component is guaranteed to be +-1.0 or zero if tangents are missing.
		float2 uv;            ///< Texture coordinate.
		float3 face_normal;     ///< Face normal in world space (normalized).
		float  coneTexLODValue; ///< Texture LOD data for cone tracing. This is zero, unless getVertexDataRayCones() is used.
	};

	enum class mesh_instance_flags : uint32_t
	{
		none = 0x0,
		use_16_bit_indices = 0x1,      ///< Indices are in 16-bit format. The default is 32-bit.
	};

	struct mesh_instance_data
	{
		uint flags;
		uint vertex_offset;
		uint index_offset;
	};

	struct geometry_instance_id
	{
		uint index; ///< Global instance index. This is computed as InstanceID() + GeometryIndex().

		static ::framework::geometry_instance_id create(uint instance_id, uint geometry_index)
		{
			::framework::geometry_instance_id obj;
			obj.index = instance_id + geometry_index;
			return obj;
		}
	};
}