struct VS_INPUT
{
	float4 vInPositions	: LOCATION0;
	float4 vInTexCoords	: LOCATION1;
	uint inImageIndex	: LOCATION2;
};

struct VS_OUTPUT
{
	float4 vOutPosition	: SV_POSITION;
	float2 vOutTexCoord	: TEXCOORD0;
	uint outImageIndex	: IMAGEINDEX;
};

[shader("vertex")]
VS_OUTPUT main(VS_INPUT input, uint VertexID : SV_VertexID)
{
	VS_OUTPUT Out;

	const uint x = VertexID & 1;
	const uint y = ((VertexID >> 1) & 1) + 2;

	Out.vOutPosition = float4(input.vInPositions[x], -input.vInPositions[y], 0.0, 1.0);
	Out.vOutTexCoord = float2(input.vInTexCoords[x], input.vInTexCoords[y]);
	Out.outImageIndex = input.inImageIndex;

	return Out;
}
