#pragma once

#include "utils.hlsl"

namespace framework
{
	template <typename T>
	struct complex
	{
		static ::framework::complex<T> create(T re, T im = 0)
		{
			::framework::complex<T> result;
			result.re = re;
			result.im = im;
			return result;
		}

		::framework::complex<T> operator-()
		{
			return ::framework::complex<T>::create(-this.re, -this.im);
		}

		::framework::complex<T> operator+(::framework::complex<T> z)
		{
			return ::framework::complex<T>::create(this.re + z.re, this.im + z.im);
		}

		::framework::complex<T> operator-(::framework::complex<T> z)
		{
			return ::framework::complex<T>::create(this.re - z.re, this.im - z.im);
		}

		::framework::complex<T> operator*(::framework::complex<T> z)
		{
			return ::framework::complex<T>::create(this.re * z.re - this.im * z.im, re * z.im + this.im * z.re);
		}

		::framework::complex<T> operator/(::framework::complex<T> z)
		{
			T scale = 1 / (z.re * z.re + z.im * z.im);
			return ::framework::complex<T>::create(scale * (this.re * z.re + this.im * z.im), scale * (this.im * z.re - this.re * z.im));
		}

		T re, im;
	};

	template <typename T>
	T norm(::framework::complex<T> z)
	{
		return z.re * z.re + z.im * z.im;
	}

	template <typename T>
	T abs(::framework::complex<T> z)
	{
		return sqrt(::framework::norm(z));
	}

	template <typename T>
	::framework::complex<T> sqrt(::framework::complex<T> z)
	{
		T n = ::framework::abs(z), t1 = sqrt(T(.5) * (n + abs(z.re))), t2 = T(.5) * z.im / t1;

		if (n == 0)
			return ::framework::complex<T>::create(0);

		if (z.re >= 0)
			return ::framework::complex<T>::create(t1, t2);
		else
			return ::framework::complex<T>::create(abs(t2), ::framework::copysign(t1, z.im));
	}
}