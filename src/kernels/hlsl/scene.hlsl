#pragma once

#include "hit_info.hlsl"
#include "scene_types.hlsl"
#include "ray_tracing/descriptor_set_0.hlsl"

namespace framework
{
	struct scene
	{
		//ByteAddressBuffer indices;
		//StructuredBuffer<::framework::packed_static_vertex_data> vertices;

		bool is_object_front_face_cw(const ::framework::geometry_instance_id geometry_instance_id)
		{
			return true;
			//return (meshInstances[geometry_instance_id.index].unpack().flags & uint(MeshInstanceFlags::IsObjectFrontFaceCW)) != 0;
		}

		::framework::mesh_instance_data get_mesh_instance_data(const ::framework::geometry_instance_id geometry_instance_id)
		{
			::framework::mesh_instance_data mesh_instance_data;
			mesh_instance_data.flags = 0;
			mesh_instance_data.vertex_offset = 0;
			mesh_instance_data.index_offset = 0;

			return mesh_instance_data;
			//return meshInstances[geometry_instance_id.index].unpack();
		}

		uint3 get_local_indices(const uint index_offset, const uint triangle_index, const bool use_16_bit)
		{
			uint base_index = index_offset * 4;
			uint3 vertex_indices;
			if (use_16_bit)
			{
				base_index += triangle_index * 6;
				vertex_indices = (uint3)indices.Load<uint16_t3>(base_index);
			}
			else
			{
				base_index += triangle_index * 12;
				vertex_indices = indices.Load3(base_index);
			}

			return vertex_indices;
		}

		uint3 get_indices(const ::framework::mesh_instance_data mesh_instance_data, const uint triangle_index)
		{
			uint3 vertex_indices = this.get_local_indices(mesh_instance_data.index_offset, triangle_index, mesh_instance_data.flags & (uint)::framework::mesh_instance_flags::use_16_bit_indices);
			vertex_indices += mesh_instance_data.vertex_offset;
			return vertex_indices;
		}

		uint3 get_indices(const ::framework::geometry_instance_id geometry_instance_id, const uint triangle_index)
		{
			return this.get_indices(this.get_mesh_instance_data(geometry_instance_id), triangle_index);
		}

		::framework::static_vertex_data get_vertex(const uint index)
		{
			return vertices[index].unpack();
		}

		float3 get_face_normal_in_object_space(const ::framework::static_vertex_data vertices[3], const bool is_front_face_cw)
		{
			float3 p0 = vertices[0].position;
			float3 p1 = vertices[1].position;
			float3 p2 = vertices[2].position;
			float3 n = normalize(cross(p1 - p0, p2 - p0));
			return is_front_face_cw ? -n : n;
		}

		::framework::vertex_data compute_vertex_data(const ::framework::static_vertex_data vertices[3], const float4x4 world_matrix, const float3x3 inverse_transpose_world_matrix, const bool is_front_face_cw, const float3 barycentrics)
		{
			::framework::vertex_data v;

			v.position = vertices[0].position * barycentrics[0];
			v.position += vertices[1].position * barycentrics[1];
			v.position += vertices[2].position * barycentrics[2];

			v.normal = vertices[0].normal * barycentrics[0];
			v.normal += vertices[1].normal * barycentrics[1];
			v.normal += vertices[2].normal * barycentrics[2];

			v.tangent = vertices[0].tangent * barycentrics[0];
			v.tangent += vertices[1].tangent * barycentrics[1];
			v.tangent += vertices[2].tangent * barycentrics[2];
			//v.tangent.w = sign(v.tangent.w); // Preserve zero to indicate invalid tangent.

			v.uv = vertices[0].uv * barycentrics[0];
			v.uv += vertices[1].uv * barycentrics[1];
			v.uv += vertices[2].uv * barycentrics[2];

			v.face_normal = this.get_face_normal_in_object_space(vertices, is_front_face_cw);

			//v.position = mul(float4(v.position, 1.f), world_matrix).xyz;
			//v.normal = mul(v.normal, inverse_transpose_world_matrix);
			//v.face_normal = mul(v.face_normal, inverse_transpose_world_matrix);
			//v.tangent.xyz = mul(v.tangent.xyz, (float3x3)world_matrix);

			v.normal = normalize(v.normal);
			v.face_normal = normalize(v.face_normal);
			v.tangent.xyz = normalize(v.tangent.xyz);

			return v;
		}

		::framework::vertex_data get_vertex_data(const ::framework::geometry_instance_id geometry_instance_id, const uint triangle_index, const float3 barycentrics, out ::framework::static_vertex_data vertices[3])
		{
			const uint3 vertex_indices = this.get_indices(geometry_instance_id, triangle_index);
			vertices[0] = this.get_vertex(vertex_indices[0]);
			vertices[1] = this.get_vertex(vertex_indices[1]);
			vertices[2] = this.get_vertex(vertex_indices[2]);

			const float4x4 world_matrix = float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);//gScene.getWorldMatrix(geometry_instance_id);
			const float3x3 inverse_transpose_world_matrix = float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);//getInverseTransposeWorldMatrix(geometry_instance_id);
			const bool is_front_face_cw = false;//isObjectFrontFaceCW(geometry_instance_id);
			return this.compute_vertex_data(vertices, world_matrix, inverse_transpose_world_matrix, is_front_face_cw, barycentrics);
		}

		::framework::vertex_data get_vertex_data(const ::framework::geometry_instance_id geometry_instance_id, const uint triangle_index, const float3 barycentrics)
		{
			::framework::static_vertex_data vertices[3];
			return this.get_vertex_data(geometry_instance_id, triangle_index, barycentrics, vertices);
		}

		::framework::vertex_data get_vertex_data(::framework::triangle_hit triangle_hit)
		{
			return this.get_vertex_data(triangle_hit.instance_id, triangle_hit.primitive_index, triangle_hit.get_barycentric_weights());
		}
	};
}