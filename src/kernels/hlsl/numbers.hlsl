#pragma once

namespace framework
{
	namespace numbers
	{
		static const float e = 2.71828182845904523536f;
		static const float pi = 3.14159265f;
		static const float pi2 = 6.28318530717958647693f;
		static const float inv_pi = 0.318309886183790671538f;
		static const float inv_2_pi = 0.15915494309189533577f;
		static const float inv_4_pi = 0.07957747154594766788f;
		static const float log_sqrt_2_pi = 0.91893853320467274178032973640562f;
		static const float flt_min = 1.175494351e-38f;
		static const float flt_max = 3.402823466e+38f;
		static const float half_min = 5.96046448e-08f;
		static const float half_max = 65504.0f;
	}
}