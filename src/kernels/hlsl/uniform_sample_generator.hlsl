#pragma once

#include "sample_generator_interface.hlsl"

namespace framework
{
	/** 32-bit bit interleave (Morton code).
		\param[in] v 16-bit values in the LSBs of each component (higher bits don't matter).
		\return 32-bit value.
	*/
	uint interleave_32bit(uint2 v)
	{
		uint x = v.x & 0x0000ffff;              // x = ---- ---- ---- ---- fedc ba98 7654 3210
		uint y = v.y & 0x0000ffff;

		x = (x | (x << 8)) & 0x00FF00FF;        // x = ---- ---- fedc ba98 ---- ---- 7654 3210
		x = (x | (x << 4)) & 0x0F0F0F0F;        // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
		x = (x | (x << 2)) & 0x33333333;        // x = --fe --dc --ba --98 --76 --54 --32 --10
		x = (x | (x << 1)) & 0x55555555;        // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0

		y = (y | (y << 8)) & 0x00FF00FF;
		y = (y | (y << 4)) & 0x0F0F0F0F;
		y = (y | (y << 2)) & 0x33333333;
		y = (y | (y << 1)) & 0x55555555;

		return x | (y << 1);
	}

	/** SplitMix64 pseudorandom number generator.

		This is a fixed-increment version of Java 8's SplittableRandom generator.
		The period is 2^64 and its state size is 64 bits.
		It is a very fast generator passing BigCrush. It is recommended for use with
		other generators like xoroshiro and xorshift to initialize their state arrays.

		Steele Jr, Guy L., Doug Lea, and Christine H. Flood., "Fast Splittable Pseudorandom Number Generators",
		ACM SIGPLAN Notices 49.10 (2014): 453-472. http://dx.doi.org/10.1145/2714064.2660195.

		This code requires shader model 6.0 or above for 64-bit integer support.
	*/

	struct SplitMix64
	{
		uint64_t state;
	};

	uint64_t asuint64(uint lowbits, uint highbits)
	{
		return (uint64_t(highbits) << 32) | uint64_t(lowbits);
	}

	/** Generates the next pseudorandom number in the sequence (64 bits).
	*/
	uint64_t nextRandom64(inout SplitMix64 rng)
	{
		uint64_t z = (rng.state += 0x9E3779B97F4A7C15ull);
		z = (z ^ (z >> 30)) * 0xBF58476D1CE4E5B9ull;
		z = (z ^ (z >> 27)) * 0x94D049BB133111EBull;
		return z ^ (z >> 31);
	}

	/** Generates the next pseudorandom number in the sequence (low 32 bits).
	*/
	uint nextRandom(inout SplitMix64 rng)
	{
		return (uint)nextRandom64(rng);
	}

	/** Initialize SplitMix64 pseudorandom number generator.
		\param[in] s0 Low bits of initial state (seed).
		\param[in] s1 High bits of initial state (seed).
	*/
	SplitMix64 createSplitMix64(uint s0, uint s1)
	{
		SplitMix64 rng;
		rng.state = asuint64(s0, s1);
		return rng;
	}


	/** Implementation of the xoshiro128** 32-bit all-purpose, rock-solid generator
		written in 2018 by David Blackman and Sebastiano Vigna (vigna@acm.org).
		The state is 128 bits and the period (2^128)-1. It has a jump function that
		allows you to skip ahead 2^64 in the seqeuence.

		Note: The state must be seeded so that it is not everywhere zero.
		The recommendation is to initialize the state using SplitMix64.

		See the original public domain code: http://xoshiro.di.unimi.it/xoshiro128starstar.c
	*/

	struct Xoshiro128StarStar
	{
		uint state[4];
	};

	uint rotl(const uint x, int k)
	{
		return (x << k) | (x >> (32 - k));
	}

	/** Generates the next pseudorandom number in the sequence (32 bits).
	*/
	uint nextRandom(inout Xoshiro128StarStar rng)
	{
		const uint32_t result_starstar = rotl(rng.state[0] * 5, 7) * 9;
		const uint32_t t = rng.state[1] << 9;

		rng.state[2] ^= rng.state[0];
		rng.state[3] ^= rng.state[1];
		rng.state[1] ^= rng.state[2];
		rng.state[0] ^= rng.state[3];

		rng.state[2] ^= t;
		rng.state[3] = rotl(rng.state[3], 11);

		return result_starstar;
	}

	/** Jump function for the generator. It is equivalent to 2^64 calls to nextRandom().
		It can be used to generate 2^64 non-overlapping subsequences for parallel computations.
	*/
	void jump(inout Xoshiro128StarStar rng)
	{
		static const uint32_t JUMP[] = { 0x8764000b, 0xf542d2d3, 0x6fa035c3, 0x77f2db5b };

		uint32_t s0 = 0;
		uint32_t s1 = 0;
		uint32_t s2 = 0;
		uint32_t s3 = 0;

		for (int i = 0; i < 4; i++)
		{
			for (int b = 0; b < 32; b++)
			{
				if (JUMP[i] & (1u << b))
				{
					s0 ^= rng.state[0];
					s1 ^= rng.state[1];
					s2 ^= rng.state[2];
					s3 ^= rng.state[3];
				}
				nextRandom(rng);
			}
		}

		rng.state[0] = s0;
		rng.state[1] = s1;
		rng.state[2] = s2;
		rng.state[3] = s3;
	}

	/** Initialize Xoshiro128StarStar pseudorandom number generator.
		The initial state should be pseudorandom and must not be zero everywhere.
		It is recommended to use SplitMix64 for creating the initial state.
		\param[in] s Array of 4x 32-bit values of initial state (seed).
	*/
	Xoshiro128StarStar createXoshiro128StarStar(uint s[4])
	{
		Xoshiro128StarStar rng;
		rng.state[0] = s[0];
		rng.state[1] = s[1];
		rng.state[2] = s[2];
		rng.state[3] = s[3];
		return rng;
	}
	/** Default uniform pseudorandom number generator.

		This generator has 128 bit state and should have acceptable statistical
		properties for most rendering applications.

		This sample generator requires shader model 6.0 or above.
	*/
	struct uniform_sample_generator
	{
		/** Initializes the sample generator for a given pixel and sample number.
			\param[in] pixel Pixel id.
			\param[in] sample_number Sample number.
		*/
		static ::framework::uniform_sample_generator create(uint2 pixel, uint sample_number)
		{
			::framework::uniform_sample_generator uniform_sample_generator;

			// Use SplitMix64 generator to generate a good pseudorandom initial state.
			// The pixel coord is expected to be max 28 bits (16K^2 is the resource limit in D3D12).
			// The sample number is expected to be practically max ~28 bits, e.g. 16spp x 16M samples.
			// As long as both stay <= 32 bits, we will always have a unique initial seed.
			// This is however no guarantee that the generated sequences will never overlap,
			// but it is very unlikely. For example, with these most extreme parameters of
			// 2^56 sequences of length L, the probability of overlap is P(overlap) = L*2^-16.
			SplitMix64 rng = createSplitMix64(interleave_32bit(pixel), sample_number);
			uint64_t s0 = nextRandom64(rng);
			uint64_t s1 = nextRandom64(rng);
			uint seed[4] = { uint(s0), uint(s0 >> 32), uint(s1), uint(s1 >> 32) };

			// Create xoshiro128** pseudorandom generator.
			uniform_sample_generator.rng = createXoshiro128StarStar(seed);

			return uniform_sample_generator;
		}

		/** Returns the next sample value. This function updates the state.
		*/
		uint next()
		{
			return nextRandom(rng);
		}

		Xoshiro128StarStar rng;
	};
}