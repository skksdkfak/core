#pragma once

#include "sampling.hlsl"

namespace framework
{
	struct sampled_spectrum
	{
		static ::framework::sampled_spectrum create(float value)
		{
			::framework::sampled_spectrum result;
			result.values = value;
			return result;
		}

		static ::framework::sampled_spectrum create(float4 values)
		{
			::framework::sampled_spectrum result;
			result.values = values;
			return result;
		}

		::framework::sampled_spectrum operator+(::framework::sampled_spectrum s)
		{
			return ::framework::sampled_spectrum::create(this.values + s.values);
		}

		::framework::sampled_spectrum operator-(::framework::sampled_spectrum s)
		{
			return ::framework::sampled_spectrum::create(this.values - s.values);
		}

		::framework::sampled_spectrum operator*(::framework::sampled_spectrum s)
		{
			return ::framework::sampled_spectrum::create(this.values * s.values);
		}

		::framework::sampled_spectrum operator/(::framework::sampled_spectrum s)
		{
			return ::framework::sampled_spectrum::create(this.values / s.values);
		}

		::framework::sampled_spectrum operator*(float a)
		{
			return ::framework::sampled_spectrum::create(this.values * a);
		}

		::framework::sampled_spectrum operator/(float a)
		{
			return ::framework::sampled_spectrum::create(this.values / a);
		}

		operator bool()
		{
			return any(this.values != 0);
		}

		bool valid()
		{
			return any(this.values != 0);
		}

		::framework::sampled_spectrum inv()
		{
			return ::framework::sampled_spectrum::create(-this.values);
		}

		float average()
		{
			float sum = values[0];
			for (uint i = 1; i < ::framework::sampled_spectrum::n; i++)
				sum += values[i];
			return sum / ::framework::sampled_spectrum::n;
		}

		float operator[](int i)
		{
			return this.values[i];
		};

		float get(uint i)
		{
			return this.values[i];
		}

		void set(uint i, float v)
		{
			this.values[i] = v;
		}

		static const uint n = 1;
		vector<float, ::framework::sampled_spectrum::n> values;
	};

	struct sampled_wavelengths
	{
		static ::framework::sampled_wavelengths sample_visible(float u)
		{
			::framework::sampled_wavelengths swl;
			for (uint i = 0; i < ::framework::sampled_spectrum::n; i++)
			{
				// Compute up for i'th wavelength sample
				float up = u + float(i) / ::framework::sampled_spectrum::n;
				if (up > 1)
					up -= 1;

				swl.lambda[i] = ::framework::sample_visible_wavelengths(up);
				swl.pdf[i] = ::framework::visible_wavelengths_pdf(swl.lambda[i]);
			}
			return swl;
		}

		static ::framework::sampled_wavelengths sample_uniform(float u, float lambda_min = 360, float lambda_max = 830)
		{
			::framework::sampled_wavelengths swl;
			// Sample first wavelength using u
			swl.lambda[0] = lerp(lambda_min, lambda_max, u);

			// Initialize lambda for remaining wavelengths
			float delta = (lambda_max - lambda_min) / ::framework::sampled_spectrum::n;
			for (int i = 1; i < ::framework::sampled_spectrum::n; ++i) {
				swl.lambda[i] = swl.lambda[i - 1] + delta;
				if (swl.lambda[i] > lambda_max)
					swl.lambda[i] = lambda_min + (swl.lambda[i] - lambda_max);
			}

			// Compute PDF for sampled wavelengths
			for (int i = 0; i < ::framework::sampled_spectrum::n; ++i)
				swl.pdf[i] = 1 / (lambda_max - lambda_min);

			return swl;
		}

		void terminate_secondary()
		{
			if (this.secondary_terminated())
				return;
			// Update wavelength probabilities for termination
			for (uint i = 1; i < ::framework::sampled_spectrum::n; i++)
				this.pdf[i] = 0;
			this.pdf[0] /= ::framework::sampled_spectrum::n;
		}

		bool secondary_terminated()
		{
			for (uint i = 1; i < ::framework::sampled_spectrum::n; i++)
				if (this.pdf[i] != 0)
					return false;
			return true;
		}

		::framework::sampled_spectrum get_pdf()
		{
			return ::framework::sampled_spectrum::create(float4(this.pdf[0], this.pdf[1], this.pdf[2], this.pdf[3]));
		}

		float lambda[4], pdf[4];
	};

	struct densely_sampled_spectrum
	{
		static ::framework::densely_sampled_spectrum create(uint values_offset)
		{
			::framework::densely_sampled_spectrum result;
			result.lambda_min = 360;
			result.lambda_max = 830;
			result.values_offset = values_offset;
			return result;
		}

		::framework::sampled_spectrum sample(::framework::sampled_wavelengths lambda)
		{
			::framework::sampled_spectrum s;
			for (uint i = 0; i < ::framework::sampled_spectrum::n; i++)
			{
				uint l = round(lambda.lambda[i]);
				//float value = l >= this.lambda_min && l <= this.lambda_max ? spectrum[this.values_offset + l - this.lambda_min] : 0.f;
				//s.set(i, value);
			}
			return s;
		}

		uint lambda_min;
		uint lambda_max;
		uint values_offset;
	};

	::framework::sampled_spectrum exp(::framework::sampled_spectrum s)
	{
		return ::framework::sampled_spectrum::create(exp(s.values));
	}

	::framework::sampled_spectrum clamp_zero(::framework::sampled_spectrum s)
	{
		return ::framework::sampled_spectrum::create(max(0, s.values));
	}

	::framework::sampled_spectrum safe_div(::framework::sampled_spectrum a, ::framework::sampled_spectrum b)
	{
		::framework::sampled_spectrum r;
		for (uint i = 0; i < ::framework::sampled_spectrum::n; i++)
			r.values[i] = (b[i] != 0) ? a[i] / b[i] : 0.f;
		return r;
	}
}