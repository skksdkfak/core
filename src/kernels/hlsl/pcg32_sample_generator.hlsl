#pragma once

#include "hash.hlsl"
#include "sample_generator_interface.hlsl"

namespace framework
{
	struct pcg32_sample_generator
	{
		static const uint64_t default_state = 0x853c49e6748fea9bULL;
		static const uint64_t default_stream = 0xda3e39cb94b95bdbULL;
		static const uint64_t mult = 0x5851f42d4c957f2dULL;

		static ::framework::pcg32_sample_generator create()
		{
			::framework::pcg32_sample_generator pcg32_sample_generator;
			pcg32_sample_generator.state = ::framework::pcg32_sample_generator::default_state;
			pcg32_sample_generator.stream = ::framework::pcg32_sample_generator::default_stream;
			return pcg32_sample_generator;
		}

		static ::framework::pcg32_sample_generator create(uint64_t sequence_index)
		{
			::framework::pcg32_sample_generator pcg32_sample_generator;
			pcg32_sample_generator.set_sequence(sequence_index, ::framework::mix_bits(sequence_index));
			return pcg32_sample_generator;
		}
		
		static ::framework::pcg32_sample_generator create(uint64_t sequence_index, uint64_t offset)
		{
			::framework::pcg32_sample_generator pcg32_sample_generator;
			pcg32_sample_generator.set_sequence(sequence_index, offset);
			return pcg32_sample_generator;
		}

		uint next()
		{
			uint64_t old_state = this.state;
			this.state = old_state * ::framework::pcg32_sample_generator::mult + this.stream;
			uint32_t xor_shifted = (uint32_t)(((old_state >> 18u) ^ old_state) >> 27u);
			uint32_t rot = (uint32_t)(old_state >> 59u);
			return (xor_shifted >> rot) | (xor_shifted << ((~rot + 1u) & 31));
		}

		void advance(int64_t delta_ = (1ll << 32))
		{
			uint64_t cur_mult = ::framework::pcg32_sample_generator::mult;
			uint64_t cur_plus = this.stream;
			uint64_t acc_mult = 1u;
			uint64_t acc_plus = 0u;

			/* Even though delta is an unsigned integer, we can pass a signed
				integer to go backwards, it just goes "the long way round". */
			uint64_t delta = uint64_t(delta_);

			while (delta > 0)
			{
				if (delta & 1)
				{
					acc_mult *= cur_mult;
					acc_plus = acc_plus * cur_mult + cur_plus;
				}
				cur_plus = (cur_mult + 1) * cur_plus;
				cur_mult *= cur_mult;
				delta /= 2;
			}
			this.state = acc_mult * this.state + acc_plus;
		}

		void set_sequence(uint64_t sequence_index, uint64_t seed)
		{
			this.state = 0u;
			this.stream = (sequence_index << 1u) | 1u;
			this.next();
			this.state += seed;
			this.next();
		}

		uint64_t state;
		uint64_t stream;
	};
}