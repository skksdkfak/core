#pragma once

#include "bxdf_utils.hlsl"
#include "fresnel.hlsl"
#include "material_data.hlsl"
#include "math.hlsl"
#include "numbers.hlsl"
#include "shading.hlsl"
#include "spectrum.hlsl"
#include "uniform_sample_generator.hlsl"

/* Creative Commons CC0 Public Domain. To the extent possible under law, Jakub Boksansky has waived all copyright and related or neighboring rights to Crash Course in BRDF Implementation Code Sample. This work is published from: Germany. */

// This is a code sample accompanying the "Crash Course in BRDF Implementation" article
// v1.1, February 2021

// -------------------------------------------------------------------------
//    C++ compatibility
// -------------------------------------------------------------------------

#if __cplusplus

#pragma once

// Include additional things when compiling HLSL as C++ 
// Here we use the GLM library to support HLSL types and functions
#include "glm/glm/glm.hpp"
#include "glm/glm/gtc/constants.hpp"
#include "glm/glm/gtx/compatibility.hpp"

#define OUT_PARAMETER(X) X&

using namespace glm;

inline float rsqrt(float x) { return inversesqrt(x); }
inline float saturate(float x) { return clamp(x, 0.0f, 1.0f); }

#else
#define OUT_PARAMETER(X) inout X
#endif

// -------------------------------------------------------------------------
//    Constant Definitions
// -------------------------------------------------------------------------

#define NONE 0

// NDF definitions
#define GGX 1
#define BECKMANN 2

// Specular BRDFs
#define MICROFACET 1
#define PHONG 2

// Diffuse BRDFs
#define LAMBERTIAN 1
#define OREN_NAYAR 2
#define DISNEY 3
#define FROSTBITE 4

// BRDF types
#define DIFFUSE_TYPE 1
#define SPECULAR_TYPE 2
#define TRANSMISSION_TYPE 3

// Minimum cos(theta) for the incident and outgoing vectors.
// Some BSDF functions are not robust for cos(theta) == 0.0,
// so using a small epsilon for consistency.
static const float kMinCosTheta = 1e-6f;
static uint g_rand_state = 0;

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return float(word) / 4294967295.0f;
}

// PIs
#ifndef PI
#define PI 3.141592653589f
#endif

#ifndef TWO_PI
#define TWO_PI (2.0f * PI)
#endif

#ifndef ONE_OVER_PI
#define ONE_OVER_PI (1.0f / PI)
#endif

#ifndef ONE_OVER_TWO_PI
#define ONE_OVER_TWO_PI (1.0f / TWO_PI)
#endif

// -------------------------------------------------------------------------
//    Configuration macros (user editable - set your preferences here)
// -------------------------------------------------------------------------

// Specify what NDF (GGX or BECKMANN you want to use)
#ifndef MICROFACET_DISTRIBUTION
#define MICROFACET_DISTRIBUTION GGX
//#define MICROFACET_DISTRIBUTION BECKMANN
#endif

// Specify default BRDFs
#ifndef SPECULAR_BRDF
#define SPECULAR_BRDF MICROFACET
//#define SPECULAR_BRDF PHONG
//#define SPECULAR_BRDF NONE
#endif

#ifndef TRANSMISSION_BRDF
#define TRANSMISSION_BRDF MICROFACET
#endif

#ifndef DIFFUSE_BRDF
#define DIFFUSE_BRDF LAMBERTIAN
//#define DIFFUSE_BRDF OREN_NAYAR
//#define DIFFUSE_BRDF DISNEY
//#define DIFFUSE_BRDF FROSTBITE
//#define DIFFUSE_BRDF NONE
#endif

// Enable this to weigh diffuse by Fresnel too, otherwise specular and diffuse will be simply added together
// (this is disabled by default for Frostbite diffuse which is normalized to combine well with GGX Specular BRDF)
#if DIFFUSE_BRDF != FROSTBITE
#define COMBINE_BRDFS_WITH_FRESNEL 1
#endif

// Uncomment this to use "general" version of G1 which is not optimized and uses NDF-specific G_Lambda (can be useful for experimenting and debugging)
//#define Smith_G1 Smith_G1_General 

// Enable optimized G2 implementation which includes division by specular BRDF denominator (not available for all NDFs, check Smith_G2_Height_Correlated_GGX_Lagardemacro G2_DIVIDED_BY_DENOMINATOR if it was actually used)
#define USE_OPTIMIZED_G2 0

// Enable height correlated version of G2 term. Separable version will be used otherwise
#define USE_HEIGHT_CORRELATED_G2 0

// -------------------------------------------------------------------------
//    Automatically resolved macros based on preferences (don't edit these)
// -------------------------------------------------------------------------

// Select distribution function
#if MICROFACET_DISTRIBUTION == GGX
#define Microfacet_D GGX_D
#elif MICROFACET_DISTRIBUTION == BECKMANN
#define Microfacet_D Beckmann_D
#endif

// Select G functions (masking/shadowing) depending on selected distribution
#if MICROFACET_DISTRIBUTION == GGX
#define Smith_G_Lambda Smith_G_Lambda_GGX
#elif MICROFACET_DISTRIBUTION == BECKMANN
#define Smith_G_Lambda Smith_G_Lambda_Beckmann_Walter
#endif

#ifndef Smith_G1
// Define version of G1 optimized specifically for selected NDF
#if MICROFACET_DISTRIBUTION == GGX
#define Smith_G1 Smith_G1_GGX
#elif MICROFACET_DISTRIBUTION == BECKMANN
#define Smith_G1 Smith_G1_Beckmann_Walter
#endif
#endif

// Select default specular and diffuse BRDF functions
#if SPECULAR_BRDF == MICROFACET
#define evalSpecular evalMicrofacet
#define sampleSpecular sampleSpecularMicrofacet
#if MICROFACET_DISTRIBUTION == GGX
#define sampleSpecularHalfVector sampleGGXVNDF
#else
#define sampleSpecularHalfVector sampleBeckmannWalter
#endif
#elif SPECULAR_BRDF == PHONG
#define evalSpecular evalPhong
#define sampleSpecular sampleSpecularPhong
#define sampleSpecularHalfVector samplePhong
#else
#define evalSpecular evalVoid
#define sampleSpecular sampleSpecularVoid
#define sampleSpecularHalfVector sampleSpecularHalfVectorVoid
#endif

#if MICROFACET_DISTRIBUTION == GGX
#define specularSampleWeight specularSampleWeightGGXVNDF
#define microfacet_transmission_sample_weight microfacet_transmission_sample_weight_ggx_vndf
#define specularPdf microfacet_reflection_pdf
#define transmission_pdf microfacet_transmission_pdf
#else
#define specularSampleWeight specularSampleWeightBeckmannWalter
#define specularPdf sampleBeckmannWalterReflectionPdf
#endif

#if TRANSMISSION_BRDF == MICROFACET
#define eval_transmission eval_microfacet_transmission
#define sampleTransmission sampleMicrofacetTransmission
#endif

#if DIFFUSE_BRDF == LAMBERTIAN
#define evalDiffuse evalLambertian
#define diffuse_pdf lambertian_pdf
#define diffuse_term lambertian
#elif DIFFUSE_BRDF == OREN_NAYAR
#define evalDiffuse evalOrenNayar
#define diffuse_term orenNayar
#elif DIFFUSE_BRDF == DISNEY
#define evalDiffuse evalDisneyDiffuse
#define diffuse_term disneyDiffuse
#elif DIFFUSE_BRDF == FROSTBITE
#define evalDiffuse evalFrostbiteDisneyDiffuse
#define diffuse_term frostbiteDisneyDiffuse
#else
#define evalDiffuse evalVoid
#define evalIndirectDiffuse evalIndirectVoid
#define diffuse_term none
#endif

// -------------------------------------------------------------------------
//    Sampling
// -------------------------------------------------------------------------

// Samples a direction within a hemisphere oriented along +Z axis with a cosine-weighted distribution 
// Source: "Sampling Transformations Zoo" in Ray Tracing Gems by Shirley et al.
float3 sampleHemisphere(float2 u, OUT_PARAMETER(float) pdf) {

	float a = sqrt(u.x);
	float b = TWO_PI * u.y;

	float3 result = float3(
		a * cos(b),
		a * sin(b),
		sqrt(1.0f - u.x));

	pdf = result.z * ONE_OVER_PI;

	return result;
}

float3 sampleHemisphere(float2 u) {
	float pdf;
	return sampleHemisphere(u, pdf);
}

// For sampling of all our diffuse BRDFs we use cosine-weighted hemisphere sampling, with PDF equal to (NdotL/PI)
float diffusePdf(float NdotL) {
	return NdotL * ONE_OVER_PI;
}

// -------------------------------------------------------------------------
//    Structures
// -------------------------------------------------------------------------

enum class SampledBSDFFlags
{
	DiffuseReflection = 1,
	DiffuseTransmission = 2,
	SpecularReflection = 4,
	SpecularTransmission = 8
};

float none(const ::framework::shading_data_t data) {
	return 0.0f;
}

float3 evalVoid(const ::framework::shading_data_t data) {
	return float3(0.0f, 0.0f, 0.0f);
}

void evalIndirectVoid(const ::framework::shading_data_t data, float2 u, OUT_PARAMETER(float3) i, OUT_PARAMETER(float3) weight) {
	i = float3(0.0f, 0.0f, 1.0f);
	weight = float3(0.0f, 0.0f, 0.0f);
}

float3 sampleSpecularVoid(float3 Vlocal, float alpha, float alphaSquared, float3 specularF0, float2 u, OUT_PARAMETER(float3) weight) {
	weight = float3(0.0f, 0.0f, 0.0f);
	return float3(0.0f, 0.0f, 0.0f);
}

float3 sampleSpecularHalfVectorVoid(float3 Vlocal, float2 alpha_2d, float2 u) {
	return float3(0.0f, 0.0f, 0.0f);
}

// -------------------------------------------------------------------------
//    Lambert
// -------------------------------------------------------------------------

float lambertian(const ::framework::shading_data_t data) {
	return 1.0f;
}

float lambertian_pdf(const ::framework::shading_data_t data)
{
	return ONE_OVER_PI * data.i.z;
}

float3 evalLambertian(const ::framework::shading_data_t data, const ::framework::material_data materia)
{
	return data.diffuseReflectance * ONE_OVER_PI * data.i.z;
}

#if 0
// -------------------------------------------------------------------------
//    Phong
// -------------------------------------------------------------------------

// For derivation see "Phong Normalization Factor derivation" by Giesen
float phongNormalizationTerm(float shininess) {

	return (1.0f + shininess) * ONE_OVER_TWO_PI;
}

float3 evalPhong(const ::framework::shading_data_t data, const ::framework::material_data material)
{

	// First convert roughness to shininess (Phong exponent)
	float shininess = ::framework::beckmannAlphaToShininess(material.alpha);

	float3 R = reflect(-data.L, data.N);
	return data.specularF0 * (phongNormalizationTerm(shininess) * pow(max(0.0f, dot(R, data.V)), shininess) * data.i.z);
}

// Samples a Phong distribution lobe oriented along +Z axis
// Source: "Sampling Transformations Zoo" in Ray Tracing Gems by Shirley et al.
float3 samplePhong(float3 Vlocal, float shininess, float2 u, OUT_PARAMETER(float) pdf) {

	float cosTheta = pow(1.0f - u.x, 1.0f / (1.0f + shininess));
	float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

	float phi = TWO_PI * u.y;

	pdf = phongNormalizationTerm(shininess) * pow(cosTheta, shininess);

	return float3(
		cos(phi) * sinTheta,
		sin(phi) * sinTheta,
		cosTheta);
}

float3 samplePhong(float3 Vlocal, float2 alpha_2d, float2 u) {
	float shininess = ::framework::beckmannAlphaToShininess(dot(alpha_2d, float2(0.5f, 0.5f)));
	float pdf;
	return samplePhong(Vlocal, shininess, u, pdf);
}

// Sampling the specular BRDF based on Phong, includes normalization term
float3 sampleSpecularPhong(float3 Vlocal, float alpha, float alphaSquared, float3 specularF0, float2 u, OUT_PARAMETER(float3) weight) {

	// First convert roughness to shininess (Phong exponent)
	float shininess = ::framework::beckmannAlphaToShininess(alpha);

	float pdf;
	float3 LPhong = samplePhong(Vlocal, shininess, u, pdf);

	// Sampled LPhong is in "lobe space" - where Phong lobe is centered around +Z axis
	// We need to rotate it in direction of perfect reflection
	float3 Nlocal = float3(0.0f, 0.0f, 1.0f);
	float3 lobeDirection = reflect(-Vlocal, Nlocal);
	float3 Llocal = rotate_point(getRotationFromZAxis(lobeDirection), LPhong);

	// Calculate the weight of the sample 
	float3 Rlocal = reflect(-Llocal, Nlocal);
	float NdotL = max(0.00001f, dot(Nlocal, Llocal));
	weight = max(float3(0.0f, 0.0f, 0.0f), specularF0 * NdotL);

	// Unoptimized formula was:
	//weight = specularF0 * (phongNormalizationTerm(shininess) * pow(max(0.0f, dot(Rlocal, Vlocal)), shininess) * NdotL) / pdf;

	return Llocal;
}

// -------------------------------------------------------------------------
//    Oren-Nayar 
// -------------------------------------------------------------------------

// Based on Oren-Nayar's qualitative model
// Source: "Generalization of Lambert's Reflectance Model" by Oren & Nayar
float orenNayar(::framework::shading_data_t data, const ::framework::material_data material)
{

	// Oren-Nayar roughness (sigma) is in radians - use conversion from Beckmann roughness here
	float sigma = ::framework::beckmannAlphaToOrenNayarRoughness(material.alpha);

	float thetaV = acos(data.wo.z);
	float thetaL = acos(data.i.z);

	float alpha = max(thetaV, thetaL);
	float beta = min(thetaV, thetaL);

	// Calculate cosine of azimuth angles difference - by projecting L and V onto plane defined by N. Assume L, V, N are normalized.
	float3 l = data.L - data.i.z * data.N;
	float3 v = data.V - data.wo.z * data.N;
	float cosPhiDifference = dot(normalize(v), normalize(l));

	float sigma2 = sigma * sigma;
	float A = 1.0f - 0.5f * (sigma2 / (sigma2 + 0.33f));
	float B = 0.45f * (sigma2 / (sigma2 + 0.09f));

	return (A + B * max(0.0f, cosPhiDifference) * sin(alpha) * tan(beta));
}

float3 evalOrenNayar(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	return data.diffuseReflectance * (orenNayar(data, material) * ONE_OVER_PI * data.i.z);
}

// -------------------------------------------------------------------------
//    Disney
// -------------------------------------------------------------------------

// Disney's diffuse term
// Source "Physically-Based Shading at Disney" by Burley
float disneyDiffuse(const ::framework::shading_data_t data, const ::framework::material_data material)
{

	float FD90MinusOne = 2.0f * material.roughness * data.LdotH * data.LdotH - 0.5f;

	float FDL = 1.0f + (FD90MinusOne * pow(1.0f - data.i.z, 5.0f));
	float FDV = 1.0F + (FD90MinusOne * pow(1.0f - data.wo.z, 5.0f));

	return FDL * FDV;
}

float3 evalDisneyDiffuse(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	return data.diffuseReflectance * (disneyDiffuse(data, material) * ONE_OVER_PI * data.i.z);
}

// Frostbite's version of Disney diffuse with energy normalization.
// Source: "Moving Frostbite to Physically Based Rendering" by Lagarde & de Rousiers
float frostbiteDisneyDiffuse(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	float energyBias = 0.5f * material.roughness;
	float energyFactor = lerp(1.0f, 1.0f / 1.51f, material.roughness);

	float FD90MinusOne = energyBias + 2.0 * data.LdotH * data.LdotH * material.roughness - 1.0f;

	float FDL = 1.0f + (FD90MinusOne * pow(1.0f - data.i.z, 5.0f));
	float FDV = 1.0f + (FD90MinusOne * pow(1.0f - data.wo.z, 5.0f));

	return FDL * FDV * energyFactor;
}

float3 evalFrostbiteDisneyDiffuse(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	return data.diffuseReflectance * (frostbiteDisneyDiffuse(data, material) * ONE_OVER_PI * data.i.z);
}
#endif

// -------------------------------------------------------------------------
//    Smith G term
// -------------------------------------------------------------------------

// Function to calculate 'a' parameter for lambda functions needed in Smith G term
// This is a version for shape invariant (isotropic) NDFs
// Note: makse sure NdotS is not negative
float Smith_G_a(float alpha, float NdotS) {
	return NdotS / (max(0.00001f, alpha) * sqrt(1.0f - min(0.99999f, NdotS * NdotS)));
}

// Lambda function for Smith G term derived for GGX distribution
float Smith_G_Lambda_GGX(float a) {
	return (-1.0f + sqrt(1.0f + (1.0f / (a * a)))) * 0.5f;
}

// Lambda function for Smith G term derived for Beckmann distribution
// This is Walter's rational approximation (avoids evaluating of error function)
// Source: "Real-time Rendering", 4th edition, p.339 by Akenine-Moller et al.
// Note that this formulation is slightly optimized and different from Walter's
float Smith_G_Lambda_Beckmann_Walter(float a) {
	if (a < 1.6f) {
		return (1.0f - (1.259f - 0.396f * a) * a) / ((3.535f + 2.181f * a) * a);
		//return ((1.0f + (2.276f + 2.577f * a) * a) / ((3.535f + 2.181f * a) * a)) - 1.0f; //< Walter's original
	}
	else {
		return 0.0f;
	}
}

// Smith G1 term (masking function)
// This non-optimized version uses NDF specific lambda function (G_Lambda) resolved bia macro based on selected NDF
float Smith_G1_General(float a) {
	return 1.0f / (1.0f + Smith_G_Lambda(a));
}

// Smith G1 term (masking function) optimized for GGX distribution (by substituting G_Lambda_GGX into G1)
float Smith_G1_GGX(float a) {
	float a2 = a * a;
	return 2.0f / (sqrt((a2 + 1.0f) / a2) + 1.0f);
}

// Smith G1 term (masking function) further optimized for GGX distribution (by substituting G_a into G1_GGX)
float Smith_G1_GGX(float alpha, float NdotS, float alphaSquared, float NdotSSquared) {
	return 2.0f / (sqrt(((alphaSquared * (1.0f - NdotSSquared)) + NdotSSquared) / NdotSSquared) + 1.0f);
}

// Smith G1 term (masking function) optimized for Beckmann distribution (by substituting G_Lambda_Beckmann_Walter into G1)
// Source: "Microfacet Models for Refraction through Rough Surfaces" by Walter et al.
float Smith_G1_Beckmann_Walter(float a) {
	if (a < 1.6f) {
		return ((3.535f + 2.181f * a) * a) / (1.0f + (2.276f + 2.577f * a) * a);
	}
	else {
		return 1.0f;
	}
}

float Smith_G1_Beckmann_Walter(float alpha, float NdotS, float alphaSquared, float NdotSSquared) {
	return Smith_G1_Beckmann_Walter(Smith_G_a(alpha, NdotS));
}

// Smith G2 term (masking-shadowing function)
// Separable version assuming independent (uncorrelated) masking and shadowing, uses G1 functions for selected NDF
float Smith_G2_Separable(float alpha, float NdotL, float NdotV) {
	float aL = Smith_G_a(alpha, NdotL);
	float aV = Smith_G_a(alpha, NdotV);
	return Smith_G1(aL) * Smith_G1(aV);
}

// Smith G2 term (masking-shadowing function)
// Height correlated version - non-optimized, uses G_Lambda functions for selected NDF
float Smith_G2_Height_Correlated(float alpha, float NdotL, float NdotV) {
	float aL = Smith_G_a(alpha, NdotL);
	float aV = Smith_G_a(alpha, NdotV);
	return 1.0f / (1.0f + Smith_G_Lambda(aL) + Smith_G_Lambda(aV));
}

// Smith G2 term (masking-shadowing function) for GGX distribution
// Separable version assuming independent (uncorrelated) masking and shadowing - optimized by substituing G_Lambda for G_Lambda_GGX and 
// dividing by (4 * NdotL * NdotV) to cancel out these terms in specular BRDF denominator
// Source: "Moving Frostbite to Physically Based Rendering" by Lagarde & de Rousiers
// Note that returned value is G2 / (4 * NdotL * NdotV) and therefore includes division by specular BRDF denominator
float Smith_G2_Separable_GGX_Lagarde(float alphaSquared, float NdotL, float NdotV) {
	float a = NdotV + sqrt(alphaSquared + NdotV * (NdotV - alphaSquared * NdotV));
	float b = NdotL + sqrt(alphaSquared + NdotL * (NdotL - alphaSquared * NdotL));
	return 1.0f / (a * b);
}

// Smith G2 term (masking-shadowing function) for GGX distribution
// Height correlated version - optimized by substituing G_Lambda for G_Lambda_GGX and dividing by (4 * NdotL * NdotV) to cancel out 
// the terms in specular BRDF denominator
// Source: "Moving Frostbite to Physically Based Rendering" by Lagarde & de Rousiers
// Note that returned value is G2 / (4 * NdotL * NdotV) and therefore includes division by specular BRDF denominator
float Smith_G2_Height_Correlated_GGX_Lagarde(float alphaSquared, float NdotL, float NdotV) {
	float a = NdotV * sqrt(alphaSquared + NdotL * (NdotL - alphaSquared * NdotL));
	float b = NdotL * sqrt(alphaSquared + NdotV * (NdotV - alphaSquared * NdotV));
	return 0.5f / (a + b);
}

// Smith G2 term (masking-shadowing function) for GGX distribution
// Height correlated version - approximation by Hammon
// Source: "PBR Diffuse Lighting for GGX + Smith Microsurfaces", slide 84 by Hammon
// Note that returned value is G2 / (4 * NdotL * NdotV) and therefore includes division by specular BRDF denominator
float Smith_G2_Height_Correlated_GGX_Hammon(float alpha, float NdotL, float NdotV) {
	return 0.5f / (lerp(2.0f * NdotL * NdotV, NdotL + NdotV, alpha));
}

// A fraction G2/G1 where G2 is height correlated can be expressed using only G1 terms
// Source: "Implementing a Simple Anisotropic Rough Diffuse Material with Stochastic Evaluation", Appendix A by Heitz & Dupuy
float Smith_G2_Over_G1_Height_Correlated(float alpha, float alphaSquared, float NdotL, float NdotV) {
	float G1V = Smith_G1(alpha, NdotV, alphaSquared, NdotV * NdotV);
	float G1L = Smith_G1(alpha, NdotL, alphaSquared, NdotL * NdotL);
	return G1L / (G1V + G1L - G1V * G1L);
}

// Evaluates G2 for selected configuration (GGX/Beckmann, optimized/non-optimized, separable/height-correlated)
// Note that some paths aren't optimized too much...
// Also note that when USE_OPTIMIZED_G2 is specified, returned value will be: G2 / (4 * NdotL * NdotV) if GG-X is selected
float Smith_G2(float alpha, float alphaSquared, float NdotL, float NdotV) {

#if USE_OPTIMIZED_G2 && (MICROFACET_DISTRIBUTION == GGX)
#if USE_HEIGHT_CORRELATED_G2
#define G2_DIVIDED_BY_DENOMINATOR 1
	return Smith_G2_Height_Correlated_GGX_Lagarde(alphaSquared, NdotL, NdotV);
#else
#define G2_DIVIDED_BY_DENOMINATOR 1
	return Smith_G2_Separable_GGX_Lagarde(alphaSquared, NdotL, NdotV);
#endif
#else
#if USE_HEIGHT_CORRELATED_G2
	return Smith_G2_Height_Correlated(alpha, NdotL, NdotV);
#else
	return Smith_G2_Separable(alpha, NdotL, NdotV);
#endif
#endif

}

// -------------------------------------------------------------------------
//    Normal distribution functions
// -------------------------------------------------------------------------

float Beckmann_D(float alphaSquared, float NdotH)
{
	float cos2Theta = NdotH * NdotH;
	float numerator = exp((cos2Theta - 1.0f) / (alphaSquared * cos2Theta));
	float denominator = PI * alphaSquared * cos2Theta * cos2Theta;
	return numerator / denominator;
}

float GGX_D(float alphaSquared, float NdotH)
{
	float b = ((alphaSquared - 1.0f) * NdotH * NdotH + 1.0f);
	return alphaSquared / (PI * b * b);
}

// -------------------------------------------------------------------------
//    Microfacet model
// -------------------------------------------------------------------------

// Samples a microfacet normal for the GGX distribution using VNDF method.
// Source: "Sampling the GGX Distribution of Visible Normals" by Heitz
// See also https://hal.inria.fr/hal-00996995v1/document and http://jcgt.org/published/0007/04/01/
// Random variables 'u' must be in <0;1) interval
// PDF is 'G1(NdotV) * D'
float3 sampleGGXVNDF(float3 Ve, float2 alpha_2d, float2 u)
{
	// Section 3.2: transforming the view direction to the hemisphere configuration
	float3 Vh = normalize(float3(alpha_2d.x * Ve.x, alpha_2d.y * Ve.y, Ve.z));

	// Section 4.1: orthonormal basis (with special case if cross product is zero)
	float lensq = Vh.x * Vh.x + Vh.y * Vh.y;
	float3 T1 = lensq > 0.0f ? float3(-Vh.y, Vh.x, 0.0f) * rsqrt(lensq) : float3(1.0f, 0.0f, 0.0f);
	float3 T2 = cross(Vh, T1);

	// Section 4.2: parameterization of the projected area
	float r = sqrt(u.x);
	float phi = TWO_PI * u.y;
	float t1 = r * cos(phi);
	float t2 = r * sin(phi);
	float s = 0.5f * (1.0f + Vh.z);
	t2 = lerp(sqrt(1.0f - t1 * t1), t2, s);

	// Section 4.3: reprojection onto hemisphere
	float3 Nh = t1 * T1 + t2 * T2 + sqrt(max(0.0f, 1.0f - t1 * t1 - t2 * t2)) * Vh;

	// Section 3.4: transforming the normal back to the ellipsoid configuration
	return normalize(float3(alpha_2d.x * Nh.x, alpha_2d.y * Nh.y, max(0.0f, Nh.z)));
}

float ggx_vndf_pdf(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	return (GGX_D(material.alphaSquared, data.NdotH) * Smith_G1_GGX(material.alpha, data.wo.z, material.alphaSquared, data.wo.z * data.wo.z)) * data.VdotH / data.wo.z;
}

// PDF of sampling a reflection vector L using 'sampleGGXVNDF'.
// Note that PDF of sampling given microfacet normal is (G1 * D) when vectors are in local space (in the hemisphere around shading normal). 
// Remaining terms (1.0f / (4.0f * NdotV)) are specific for reflection case, and come from multiplying PDF by jacobian of reflection operator
float microfacet_reflection_pdf(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	return ggx_vndf_pdf(data, material) / (4.0f * data.VdotH);
}

float microfacet_transmission_pdf(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	float eta = data.eta_t / data.eta_i;
	float sqrt_denom = data.VdotH + eta * data.LdotH;
	// Compute change of variables _dwh\_dwi_ for microfacet transmission
	float dwh_dwi = abs((eta * eta * data.LdotH) / (sqrt_denom * sqrt_denom));
	return ggx_vndf_pdf(data, material) * dwh_dwi;
}

// "Walter's trick" is an adjustment of alpha value for Walter's sampling to reduce maximal weight of sample to about 4
// Source: "Microfacet Models for Refraction through Rough Surfaces" by Walter et al., page 8
float waltersTrick(float alpha, float NdotV) {
	return (1.2f - 0.2f * sqrt(abs(NdotV))) * alpha;
}

// PDF of sampling a reflection vector L using 'sampleBeckmannWalter'.
// Note that PDF of sampling given microfacet normal is (D * NdotH). Remaining terms (1.0f / (4.0f * LdotH)) are specific for
// reflection case, and come from multiplying PDF by jacobian of reflection operator
float sampleBeckmannWalterReflectionPdf(float alpha, float alphaSquared, float NdotH, float NdotV, float LdotH, float HdotV) {
	NdotH = max(0.00001f, NdotH);
	LdotH = max(0.00001f, LdotH);
	return Beckmann_D(max(0.00001f, alphaSquared), NdotH) * NdotH/* / (4.0f * LdotH)*/;
}

// Samples a microfacet normal for the Beckmann distribution using walter's method.
// Source: "Microfacet Models for Refraction through Rough Surfaces" by Walter et al.
// PDF is 'D * NdotH'
float3 sampleBeckmannWalter(float3 Vlocal, float2 alpha_2d, float2 u) {
	float alpha = dot(alpha_2d, float2(0.5f, 0.5f));

	// Equations (28) and (29) from Walter's paper for Beckmann distribution
	float tanThetaSquared = -(alpha * alpha) * log(1.0f - u.x);
	float phi = TWO_PI * u.y;

	// Calculate cosTheta and sinTheta needed for conversion to H vector
	float cosTheta = rsqrt(1.0f + tanThetaSquared);
	float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

	// Convert sampled spherical coordinates to H vector
	return normalize(float3(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta));
}

// Weight for the reflection ray sampled from GGX distribution using VNDF method
float specularSampleWeightGGXVNDF(float alpha, float alphaSquared, float NdotL, float NdotV, float HdotL, float NdotH) {
#if USE_HEIGHT_CORRELATED_G2
	return Smith_G2_Over_G1_Height_Correlated(alpha, alphaSquared, NdotL, NdotV);
#else 
	return Smith_G1_GGX(alpha, NdotL, alphaSquared, NdotL * NdotL);
#endif
}

// Weight for the transmission ray sampled from GGX distribution using VNDF method
float microfacet_transmission_sample_weight_ggx_vndf(float alpha, float alphaSquared, float NdotL, float NdotV, float HdotL, float NdotH, float eta)
{
#if USE_HEIGHT_CORRELATED_G2
	return Smith_G2_Over_G1_Height_Correlated(alpha, alphaSquared, NdotL, NdotV) * eta * eta;
#else 
	return Smith_G1_GGX(alpha, NdotL, alphaSquared, NdotL * NdotL) * eta * eta;
#endif
}

// Weight for the reflection ray sampled from Beckmann distribution using Walter's method
float specularSampleWeightBeckmannWalter(float alpha, float alphaSquared, float NdotL, float NdotV, float HdotL, float NdotH) {
	return (HdotL * Smith_G2(alpha, alphaSquared, NdotL, NdotV)) / (NdotV * NdotH);
}

// Samples a reflection ray from the rough surface using selected microfacet distribution and sampling method
// Resulting weight includes multiplication by cosine (data.i.z) term
float3 sampleSpecularMicrofacet(float3 Vlocal, float roughness, float alpha, float eta, float2 u, OUT_PARAMETER(float3) Hlocal)
{
	bool flip = Vlocal.z < 0;

	// Sample a microfacet normal (H) in local space
	if (roughness == 0.0f)
	{
		// Fast path for zero roughness (perfect reflection), also prevents NaNs appearing due to divisions by zeroes
		Hlocal = float3(0.0f, 0.0f, 1.0f);
	}
	else
	{
		// For non-zero roughness, this calls VNDF sampling for GG-X distribution or Walter's sampling for Beckmann distribution
		Hlocal = sampleSpecularHalfVector(flip ? -Vlocal : Vlocal, float2(alpha, alpha), u);
	}

	if (flip)
	{
		Hlocal = -Hlocal;
	}

	// Reflect view direction to obtain light vector
	return reflect(-Vlocal, Hlocal);
}

// Samples a refraction ray through the rough surface using selected microfacet distribution and sampling method
// Resulting weight includes multiplication by cosine (data.i.z) term
float3 sampleMicrofacetTransmission(float3 Vlocal, float roughness, float alpha, float eta, float2 u, OUT_PARAMETER(float3) Hlocal)
{
	bool flip = Vlocal.z < 0;

	// Sample a microfacet normal (H) in local space
	if (roughness == 0.0f)
	{
		// Fast path for zero roughness (perfect reflection), also prevents NaNs appearing due to divisions by zeroes
		Hlocal = float3(0.0f, 0.0f, 1.0f);
	}
	else
	{
		// For non-zero roughness, this calls VNDF sampling for GG-X distribution or Walter's sampling for Beckmann distribution
		Hlocal = sampleSpecularHalfVector(flip ? -Vlocal : Vlocal, float2(alpha, alpha), u);
	}

	if (flip)
	{
		Hlocal = -Hlocal;
	}

	return refract(-Vlocal, Hlocal, eta);
}

// Evaluates microfacet specular BRDF
float3 evalMicrofacet(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	float D = Microfacet_D(material.alphaSquared, data.NdotH);
	float G2 = Smith_G2(material.alpha, material.alphaSquared, data.i.z, data.wo.z);

#if G2_DIVIDED_BY_DENOMINATOR
	return data.F * (G2 * D * data.i.z);
#else
	return ((data.F * G2 * D) / (4.0f * data.i.z * data.wo.z)) * data.i.z;
#endif
}

float3 eval_microfacet_transmission(const ::framework::shading_data_t data, const ::framework::material_data material)
{
	float eta = data.eta_t / data.eta_i;
	float factor = 1.0 / eta;
	//float factor = 1.0;
	float cosThetaO = data.wo.z;
	float cosThetaI = data.i.z;
	float sqrtDenom = data.VdotH + eta * data.LdotH;

	float D = Microfacet_D(material.alphaSquared, data.NdotH);
	float G2 = Smith_G2(material.alpha, material.alphaSquared, data.i.z, data.wo.z);
	float c = eta * eta * data.LdotH * data.VdotH * factor * factor / (cosThetaI * cosThetaO * sqrtDenom * sqrtDenom);

#if G2_DIVIDED_BY_DENOMINATOR
	return (1. - data.F) * (G2 * D * data.i.z);
#else
	return ((1. - data.F) * G2 * D * c) * data.i.z;
#endif
}

bool Refract(float3 wi, float3 n, float eta, out float etap, out float3 wt)
{
	float cosTheta_i = dot(n, wi);
	// Potentially flip interface orientation for Snell's law
	if (cosTheta_i < 0) {
		eta = 1 / eta;
		cosTheta_i = -cosTheta_i;
		n = -n;
	}

	// Compute $\cos\,\theta_\roman{t}$ using Snell's law
	float sin2Theta_i = max(0, 1 - (cosTheta_i * cosTheta_i));
	float sin2Theta_t = sin2Theta_i / (eta * eta);
	// Handle total internal reflection case
	if (sin2Theta_t >= 1)
		return false;

	float cosTheta_t = sqrt(1 - sin2Theta_t);

	wt = -wi / eta + (cosTheta_i / eta - cosTheta_t) * float3(n);
	// Provide relative IOR along ray to caller
	etap = eta;

	return true;
}

namespace framework
{
	enum bxdf_flags
	{
		Reflection = 1 << 0,
		Transmission = 1 << 1,
		Diffuse = 1 << 2,
		Glossy = 1 << 3,
		Specular = 1 << 4,
		// Composite _BxDFFlags_ definitions
		DiffuseReflection = Diffuse | Reflection,
		DiffuseTransmission = Diffuse | Transmission,
		GlossyReflection = Glossy | Reflection,
		GlossyTransmission = Glossy | Transmission,
		SpecularReflection = Specular | Reflection,
		SpecularTransmission = Specular | Transmission,
		All = Diffuse | Glossy | Specular | Reflection | Transmission
	};

	struct bsdf_sample
	{
		float3 wi;             ///< Incident direction in world space (normalized).
		float pdf;            ///< pdf with respect to solid angle for sampling incident direction wi.
		::framework::sampled_spectrum weight;         ///< Sample weight f(wo, wi) * dot(wi, n) / pdf(wi).
		uint flags;           ///< Sampled lobe. This is a combination of ::framework::bxdf_flags flags (see BxDFTypes.slang).
		float pdf_single;

		static ::framework::bsdf_sample create(float3 wi, float pdf, ::framework::sampled_spectrum weight, uint flags, float pdf_single)
		{
			::framework::bsdf_sample result;
			result.wi = wi;
			result.pdf = pdf;
			result.weight = weight;
			result.flags = flags;
			result.pdf_single = pdf_single;
			return result;
		}

		bool is_transmission()
		{
			return (this.flags & ::framework::bxdf_flags::Transmission) != 0;
		}
	};

	/*
	struct diffuse_reflection_lambert
	{
		float3 albedo;

		float3 eval(float3 o, float3 i)
		{
			if (i.z < 0.f)
			{
				return 0.f;
			}

			return ONE_OVER_PI * albedo * i.z;
		}

		template<typename sample_generator_t>
		bool sample(float3 o, out float3 i, out float pdf, out ::framework::sampled_spectrum weight, out uint lobe, inout sample_generator_t sg)
		{
			i = sampleHemisphere(::framework::sample_next_2d(sg), pdf);
			lobe = DiffuseReflection;

			weight = albedo;
			return true;
		}

		float eval_pdf(float3 o, float3 i)
		{
			if (i.z < 0.f)
			{
				return 0.f;
			}

			return ONE_OVER_PI * i.z;
		}
	};

	struct diffuse_transmission_lambert
	{
		float3 albedo;

		float3 eval(float3 o, float3 i)
		{
			if (i.z > 0.f)
			{
				return 0.f;
			}

			return ONE_OVER_PI * albedo * -i.z;
		}

		template<typename sample_generator_t>
		bool sample(float3 o, out float3 i, out float pdf, out ::framework::sampled_spectrum weight, out uint lobe, inout sample_generator_t sg)
		{
			i = sampleHemisphere(::framework::sample_next_2d(sg), pdf);
			i.z = -i.z;
			lobe = DiffuseTransmission;

			weight = albedo;
			return true;
		}

		float eval_pdf(float3 o, float3 i)
		{
			if (i.z > 0.f)
			{
				return 0.f;
			}

			return ONE_OVER_PI * -i.z;
		}
	};

	struct specular_reflection_microfacet
	{
		::framework::shading_data_t shading_data;
		::framework::material_data material;

		float3 eval(float3 o, float3 i, bool allow_delta_eval = false)
		{
			if (i.z < 0.f)
			{
				return 0.f;
			}

			shading_data.setup_lighting_info(i);

			return evalSpecular(shading_data, material);
		}

		template<typename sample_generator_t>
		bool sample(float3 o, out float3 i, out float pdf, out ::framework::sampled_spectrum weight, out uint lobe, inout sample_generator_t sample_generator)
		{
			float eta = this.shading_data.eta_i / this.shading_data.eta_t;

			float3 wm;
			i = sampleSpecular(o, material.roughness, material.alpha, eta, ::framework::sample_next_2d(sample_generator), wm);

			this.shading_data.setup_lighting_info(i);

			float3 F = ::framework::evalFresnel(this.shading_data.specularF0, ::framework::shadowedF90(this.shading_data.specularF0), this.shading_data.LdotH);

			weight = F * specularSampleWeight(this.material.alpha, this.material.alphaSquared, i.z, o.z, this.shading_data.LdotH, this.shading_data.NdotH);
			lobe = SpecularReflection;

			pdf = specularPdf(this.shading_data, this.material);

			return true;
		}

		float eval_pdf(float3 o, float3 i, bool allow_delta_eval = false)
		{
			if (i.z < 0.f)
			{
				return 0.f;
			}

			this.shading_data.setup_lighting_info(i);

			return specularPdf(this.shading_data, this.material);
		}
	};
	*/

	struct trowbridge_reitz_distribution
	{
		float2 alpha;

		static ::framework::trowbridge_reitz_distribution create(float2 alpha)
		{
			::framework::trowbridge_reitz_distribution trowbridge_reitz_distribution;
			trowbridge_reitz_distribution.alpha = alpha;
			return trowbridge_reitz_distribution;
		}

		float d(float3 wm)
		{
			float cos_2_theta = ::framework::square(wm.z);
			float sin_2_theta = 1 - cos_2_theta;
			float tan_2_theta = sin_2_theta / cos_2_theta;
			float cos_4_theta = ::framework::square(cos_2_theta);
			float sin_theta = sqrt(sin_2_theta);
			float cos_phi = sin_theta == 0 ? 1 : clamp(wm.x / sin_theta, -1, 1);
			float sin_phi = sin_theta == 0 ? 0 : clamp(wm.y / sin_theta, -1, 1);
			float e = tan_2_theta * (::framework::square(cos_phi / this.alpha.x) + ::framework::square(sin_phi / this.alpha.y));
			return 1 / (::framework::numbers::pi * this.alpha.x * this.alpha.y * cos_4_theta * ::framework::square(1 + e));
		}

		float lambda(float3 w)
		{
			float cos_2_theta = ::framework::square(w.z);
			float sin_2_theta = 1 - cos_2_theta;
			float tan_2_theta = sin_2_theta / cos_2_theta;
			float sin_theta = sqrt(sin_2_theta);
			float cos_phi = sin_theta == 0 ? 1 : clamp(w.x / sin_theta, -1, 1);
			float sin_phi = sin_theta == 0 ? 0 : clamp(w.y / sin_theta, -1, 1);
			float alpha2 = ::framework::square(cos_phi * this.alpha.x) + ::framework::square(sin_phi * this.alpha.y);
			return (sqrt(1 + alpha2 * tan_2_theta) - 1) / 2;
		}

		float g1(float3 w)
		{
			return 1 / (1 + this.lambda(w));
		}

		float g(float3 wo, float3 wi)
		{
			return 1 / (1 + this.lambda(wo) + this.lambda(wi));
		}

		// assuming w.z is positive
		float d(float3 w, float3 wm)
		{
			return this.g1(w) / ::framework::cos_theta(w) * this.d(wm) * abs(dot(w, wm));
		}

		float pdf(float3 w, float3 wm)
		{
			return this.d(w, wm);
		}

		float3 sample_wm(float3 w, float2 u)
		{
			return sampleGGXVNDF(w, this.alpha, u);
		}

		bool effectively_smooth()
		{
			return max(this.alpha.x, this.alpha.y) < 1e-3f;
		}
	};

	struct dielectric_bxdf
	{
		::framework::shading_data_t shading_data;
		::framework::material_data material;
		uint active_lobes; ///< BSDF lobes to include for sampling and evaluation. See ::framework::bxdf_flags.
		::framework::trowbridge_reitz_distribution microfacet_distribution;

		bool has_lobe(::framework::bxdf_flags flags)
		{
			return (this.active_lobes & (uint)flags) != 0;
		}

		::framework::sampled_spectrum eval(float3 wo, float3 wi, bool allow_delta_eval = false)
		{
			float eta_i = this.shading_data.eta_i, eta_t = this.shading_data.eta_t;
			float eta = eta_i / eta_t;

			if (eta == 1 || this.microfacet_distribution.effectively_smooth())
			{
				if (allow_delta_eval)
				{
					bool is_reflection = ::framework::cos_theta(wi) > 0;
					float cos_theta_t;
					float f = ::framework::fresnel_dielectric(eta_i, eta_t, ::framework::cos_theta(wo), cos_theta_t);
					float r = f;
					float t = 1 - f;

					if (is_reflection)
					{
						return ::framework::sampled_spectrum::create(r / ::framework::cos_theta(wi));
					}
					else
					{
						return ::framework::sampled_spectrum::create(t / abs(::framework::cos_theta(wi)));
					}
				}
				else
				{
					return ::framework::sampled_spectrum::create(0.f);
				}
			}
			else
			{
				float cos_theta_i = ::framework::cos_theta(wi);
				bool is_reflection = cos_theta_i > 0;
				// Compute half-vector and make sure it's in the upper hemisphere.
				float3 wm = normalize(wi * (is_reflection ? 1 : eta_t / eta_i) + wo);
				wm *= sign(::framework::cos_theta(wm));
				float wo_dot_wm = dot(wo, wm);
				float cos_theta_t;
				float f = ::framework::fresnel_dielectric(eta_i, eta_t, wo_dot_wm, cos_theta_t);
				if (is_reflection)
				{
					return ::framework::sampled_spectrum::create(f * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) / (4 * ::framework::cos_theta(wi) * ::framework::cos_theta(wo)));
				}
				else
				{
					float denom = ::framework::square(dot(wi, wm) + dot(wo, wm) * eta);
					return ::framework::sampled_spectrum::create((1 - f) * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) * abs(dot(wi, wm) * dot(wo, wm) / (::framework::cos_theta(wi) * ::framework::cos_theta(wo) * denom)));
				}
			}
		}

		template<typename sample_generator_t>
		bool sample(float3 wo, out ::framework::bsdf_sample bsdf_sample, inout sample_generator_t sample_generator)
		{
			const bool has_reflection = this.has_lobe(SpecularReflection);
			const bool has_transmission = this.has_lobe(SpecularTransmission);

			float eta_i = this.shading_data.eta_i, eta_t = this.shading_data.eta_t;
			float eta = eta_i / eta_t;

			float3 wi;
			float pdf;
			::framework::sampled_spectrum weight;
			uint flags;
			float pdf_single = 0.f;

			if (eta == 1 || this.microfacet_distribution.effectively_smooth())
			{
				float cos_theta_t;
				float f = ::framework::fresnel_dielectric(eta_i, eta_t, ::framework::cos_theta(wo), cos_theta_t);
				float r = f;
				float t = 1 - f;
				// Compute probabilities pr and pt for sampling reflection and transmission
				float pr = r, pt = t;
				// Get a random number to decide what lobe to sample.
				float lobe_sample = ::framework::sample_next_1d(sample_generator);

				bool is_reflection = has_reflection;
				if (has_reflection && has_transmission)
				{
					is_reflection = lobe_sample < f;
				}
				else if (has_transmission && f == 1.f)
				{
					return false;
				}

				if (is_reflection)
				{
					wi = float3(-wo.x, -wo.y, wo.z);
					if (::framework::cos_theta(wi) <= 0.f)
						return false;
					weight = ::framework::sampled_spectrum::create(r / ::framework::cos_theta(wi));
					flags = SpecularReflection;
					pdf = pr / (pr + pt);
				}
				else
				{
					wi = float3(-wo.x * eta, -wo.y * eta, -cos_theta_t);
					if (::framework::cos_theta(wi) >= 0.f)
						return false;
					pdf = pt / (pr + pt);
					weight = ::framework::sampled_spectrum::create(t / abs(::framework::cos_theta(wi)));
					flags = SpecularTransmission;
				}

				weight = weight / pdf;
			}
			else
			{
				float3 wm = this.microfacet_distribution.sample_wm(wo, ::framework::sample_next_2d(sample_generator));
				shading_data.setup_wm(wm);

				float wo_dot_wm = dot(wo, wm);
				float cos_theta_t;
				float f = ::framework::fresnel_dielectric(eta_i, eta_t, wo_dot_wm, cos_theta_t);
				float r = f;
				float t = 1 - f;
				// Compute probabilities pr and pt for sampling reflection and transmission
				float pr = r, pt = t;
				// Get a random number to decide what lobe to sample.
				float lobe_sample = ::framework::sample_next_1d(sample_generator);

				bool is_reflection = has_reflection;
				if (has_reflection && has_transmission)
				{
					is_reflection = lobe_sample < f;
				}
				else if (has_transmission && f == 1.f)
				{
					return false;
				}

				if (is_reflection)
				{
					wi = reflect(-wo, wm);
					if (::framework::cos_theta(wi) <= 0.f)
						return false;
					weight = ::framework::sampled_spectrum::create(r * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) / (4 * ::framework::cos_theta(wi) * ::framework::cos_theta(wo)));
					flags = GlossyReflection;
					// Compute PDF of rough dielectric reflection
					pdf = this.microfacet_distribution.pdf(wo, wm) / (4 * abs(dot(wo, wm))) * pr / (pr + pt);
				}
				else
				{
					wi = ((eta * dot(wo, wm) - cos_theta_t) * wm - eta * wo);
					if (::framework::cos_theta(wi) >= 0.f)
						return false;
					// Compute PDF of rough dielectric transmission
					float denom = ::framework::square(dot(wi, wm) + dot(wo, wm) * eta);
					float dwm_dwi = abs(dot(wi, wm)) / denom;
					pdf = this.microfacet_distribution.pdf(wo, wm) * dwm_dwi * pt / (pr + pt);
					weight = ::framework::sampled_spectrum::create(t * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) * abs(dot(wi, wm) * dot(wo, wm) / (::framework::cos_theta(wi) * ::framework::cos_theta(wo) * denom)));
					flags = GlossyTransmission;
				}

				weight = weight / pdf;
			}

			bsdf_sample = ::framework::bsdf_sample::create(wi, pdf, weight, flags, pdf_single);

			return true;
		}

		float eval_pdf(float3 wo, float3 wi, bool allow_delta_eval = false)
		{
			float eta_i = this.shading_data.eta_i, eta_t = this.shading_data.eta_t;
			float eta = eta_i / eta_t;

			if (eta == 1 || this.microfacet_distribution.effectively_smooth())
			{
				if (allow_delta_eval)
				{
					bool is_reflection = ::framework::cos_theta(wi) > 0;
					float cos_theta_t;
					float f = ::framework::fresnel_dielectric(eta_i, eta_t, ::framework::cos_theta(wo), cos_theta_t);
					float r = f;
					float t = 1 - f;
					// Compute probabilities pr and pt for sampling reflection and transmission
					float pr = r, pt = t;

					if (is_reflection)
					{
						return pr / (pr + pt);
					}
					else
					{
						return pt / (pr + pt);
					}
				}
				else
				{
					return 0.f;
				}
			}
			else
			{
				float cos_theta_i = ::framework::cos_theta(wi);
				bool is_reflection = cos_theta_i > 0;
				// Compute half-vector and make sure it's in the upper hemisphere.
				float3 wm = normalize(wi * (is_reflection ? 1 : eta_t / eta_i) + wo);
				wm *= sign(::framework::cos_theta(wm));
				float wo_dot_wm = dot(wo, wm);
				float cos_theta_t;
				float f = ::framework::fresnel_dielectric(eta_i, eta_t, wo_dot_wm, cos_theta_t);
				float r = f;
				float t = 1 - f;
				// Compute probabilities pr and pt for sampling reflection and transmission
				float pr = r, pt = t;

				if (is_reflection)
				{
					// Compute PDF of rough dielectric reflection
					return this.microfacet_distribution.pdf(wo, wm) / (4 * abs(dot(wo, wm))) * pr / (pr + pt);
				}
				else
				{
					// Compute PDF of rough dielectric transmission
					float denom = ::framework::square(dot(wi, wm) + dot(wo, wm) * eta);
					float dwm_dwi = abs(dot(wi, wm)) / denom;
					return this.microfacet_distribution.pdf(wo, wm) * dwm_dwi * pt / (pr + pt);
				}
			}
		}
	};

	struct conductor_bxdf
	{
		::framework::shading_data_t shading_data;
		::framework::material_data material;
		uint active_lobes; ///< BSDF lobes to include for sampling and evaluation. See ::framework::bxdf_flags.
		::framework::trowbridge_reitz_distribution microfacet_distribution;

		bool has_lobe(::framework::bxdf_flags flags)
		{
			return (this.active_lobes & (uint)flags) != 0;
		}

		::framework::sampled_spectrum eval(float3 wo, float3 wi, bool allow_delta_eval = false)
		{
			if (::framework::cos_theta(wi) <= 0.f)
			{
				return ::framework::sampled_spectrum::create(0.f);
			}

			if (this.microfacet_distribution.effectively_smooth())
			{
				if (allow_delta_eval)
				{
					::framework::sampled_spectrum f = ::framework::fresnel_complex(::framework::cos_theta(wo), 1.f, float3(0.126f, 0.466, 1.342), float3(4.24f, 2.21, 1.646));
					return f / ::framework::cos_theta(wi);
				}
				else
				{
					return ::framework::sampled_spectrum::create(0.f);
				}
			}
			else
			{
				float3 wm = normalize(wo + wi);
				float wo_dot_wm = dot(wo, wm);
				::framework::sampled_spectrum f = ::framework::fresnel_complex(wo_dot_wm, 1.f, float3(0.126f, 0.466, 1.342), float3(4.24f, 2.21, 1.646));
				float cos_theta_o = ::framework::cos_theta(wo), cos_theta_i = ::framework::cos_theta(wi);
				return f * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) / (4 * cos_theta_o * cos_theta_i);
			}
		}

		template<typename sample_generator_t>
		bool sample(float3 wo, out ::framework::bsdf_sample bsdf_sample, inout sample_generator_t sample_generator)
		{
			float3 wi;
			float pdf;
			::framework::sampled_spectrum weight;
			uint flags;
			float pdf_single = 0.f;

			if (this.microfacet_distribution.effectively_smooth())
			{
				::framework::sampled_spectrum f = ::framework::fresnel_complex(::framework::cos_theta(wo), 1.f, float3(0.126f, 0.466, 1.342), float3(4.24f, 2.21, 1.646));
				wi = float3(-wo.x, -wo.y, wo.z);
				if (::framework::cos_theta(wi) <= 0.f)
					return false;
				weight = f / ::framework::cos_theta(wi);
				flags = SpecularReflection;
				pdf = 1.f;
				weight = weight / pdf;
			}
			else
			{
				float3 wm = this.microfacet_distribution.sample_wm(wo, ::framework::sample_next_2d(sample_generator));
				float wo_dot_wm = abs(dot(wo, wm));
				::framework::sampled_spectrum f = ::framework::fresnel_complex(wo_dot_wm, 1.f, float3(0.126f, 0.466, 1.342), float3(4.24f, 2.21, 1.646));
				wi = reflect(-wo, wm);
				if (::framework::cos_theta(wi) <= 0)
					return false;
				shading_data.setup_lighting_info(wi);
				weight = f * this.microfacet_distribution.d(wm) * this.microfacet_distribution.g(wo, wi) / (4 * ::framework::cos_theta(wo) * ::framework::cos_theta(wi));

				// Compute PDF of wi for microfacet reflection
				pdf = this.microfacet_distribution.pdf(wo, wm) / (4 * wo_dot_wm);
				weight = weight / pdf;
				flags = SpecularReflection;
			}

			bsdf_sample = ::framework::bsdf_sample::create(wi, pdf, weight, flags, pdf_single);

			return true;
		}

		float eval_pdf(float3 wo, float3 wi, bool allow_delta_eval = false)
		{
			if (::framework::cos_theta(wi) <= 0.f)
			{
				return 0.f;
			}

			if (this.microfacet_distribution.effectively_smooth())
			{
				if (allow_delta_eval)
				{
					return 1.f;
				}
				else
				{
					return 0.f;
				}
			}
			else
			{
				float3 wm = normalize(wo + wi);
				float wo_dot_wm = dot(wo, wm);
				return this.microfacet_distribution.pdf(wo, wm) / (4 * wo_dot_wm);
			}
		}
	};

	struct standard_bxdf_t
	{
		//diffuse_reflection_lambert diffuse_reflection;
		//diffuse_transmission_lambert diffuse_transmission;
		//specular_reflection_microfacet specular_reflection;
		dielectric_bxdf dielectric_bxdf;
		conductor_bxdf conductor_bxdf;

		float diffuse_transmission_weight;
		float specular_transmission_weight;

		float p_diffuse_reflection;     ///< Probability for sampling the diffuse BRDF.
		float p_diffuse_transmission;   ///< Probability for sampling the diffuse BTDF.
		float p_specular_reflection;    ///< Probability for sampling the specular BRDF.
		float p_specular_transmission;  ///< Probability for sampling the specular BTDF.

		void setup(::framework::shading_data_t shading_data, ::framework::material_data material)
		{
			//this.diffuse_transmission_weight = material.diffuse_transmission;
			//this.specular_transmission_weight = material.specular_transmission;
			//
			//this.diffuse_reflection.albedo = material.baseColor;
			//
			//this.diffuse_transmission.albedo = material.baseColor;
			//
			//this.specular_reflection.shading_data = shading_data;
			//this.specular_reflection.material = material;

			this.dielectric_bxdf.shading_data = shading_data;
			this.dielectric_bxdf.material = material;
			this.dielectric_bxdf.active_lobes = -1;
			this.dielectric_bxdf.microfacet_distribution.alpha = material.alpha.xx;

			this.conductor_bxdf.shading_data = shading_data;
			this.conductor_bxdf.material = material;
			this.conductor_bxdf.active_lobes = -1;
			this.conductor_bxdf.microfacet_distribution.alpha = material.alpha.xx;

			const uint active_lobes = -1;

			//// Compute sampling weights.
			//float metallic_brdf = material.metalness;
			//float specular_bsdf = (1.f - material.metalness) * material.specular_transmission;
			//float dielectric_bsdf = (1.f - material.metalness) * (1.f - material.specular_transmission);
			//
			//float diffuse_weight = luminance(material.baseColor);
			//float specular_weight = luminance(shading_data.F);
			//
			//this.p_diffuse_reflection = (active_lobes & (uint)::framework::bxdf_flags::DiffuseReflection) ? diffuse_weight * dielectric_bsdf * (1.f - material.diffuse_transmission) : 0.f;
			//this.p_diffuse_transmission = (active_lobes & (uint)::framework::bxdf_flags::DiffuseTransmission) ? diffuse_weight * dielectric_bsdf * material.diffuse_transmission : 0.f;
			//this.p_specular_reflection = (active_lobes & ((uint)::framework::bxdf_flags::SpecularReflection | (uint)::framework::bxdf_flags::DeltaReflection)) ? specular_weight * (metallic_brdf + dielectric_bsdf) : 0.f;
			//this.p_specular_transmission = (active_lobes & ((uint)::framework::bxdf_flags::SpecularReflection | (uint)::framework::bxdf_flags::DeltaReflection | (uint)::framework::bxdf_flags::SpecularTransmission | (uint)::framework::bxdf_flags::DeltaTransmission)) ? specular_bsdf : 0.f;
			//
			//float norm_factor = this.p_diffuse_reflection + this.p_diffuse_transmission + this.p_specular_reflection + this.p_specular_transmission;
			//if (norm_factor > 0.f)
			//{
			//	norm_factor = 1.f / norm_factor;
			//	this.p_diffuse_reflection *= norm_factor;
			//	this.p_diffuse_transmission *= norm_factor;
			//	this.p_specular_reflection *= norm_factor;
			//	this.p_specular_transmission *= norm_factor;
			//}
		}

		::framework::sampled_spectrum eval(float3 wo, float3 wi, uint allowed_sampled_types = -1, bool allow_delta_eval = false)
		{
			::framework::sampled_spectrum result = ::framework::sampled_spectrum::create(0.f);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::DiffuseReflection) && this.p_diffuse_reflection > 0.f && !allow_delta_eval) result += (1.f - this.specular_transmission_weight) * (1.f - this.diffuse_transmission_weight) * this.diffuse_reflection.eval(wo, wi);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::DiffuseTransmission) && this.p_diffuse_transmission > 0.f && !allow_delta_eval) result += (1.f - this.specular_transmission_weight) * this.diffuse_transmission_weight * this.diffuse_transmission.eval(wo, wi);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::SpecularReflection) && this.p_specular_reflection > 0.f) result += (1.f - this.specular_transmission_weight) * this.specular_reflection.eval(wo, wi, allow_delta_eval);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::SpecularTransmission) && this.p_specular_transmission > 0.f) result += this.specular_transmission_weight * this.dielectric_bxdf.eval(wo, wi, allow_delta_eval);
			result = this.dielectric_bxdf.eval(wo, wi, allow_delta_eval) * abs(::framework::cos_theta(wi));
			return result;
		}

		// This is an entry point for evaluation of all other BRDFs based on selected configuration (for indirect light)
		template<typename sample_generator_t>
		bool sample(inout sample_generator_t sample_generator, ::framework::shading_data_t shading_data, out ::framework::bsdf_sample bsdf_sample)
		{
			bool valid = false;

			float u_select = ::framework::sample_next_1d(sample_generator);

			//if (u_select < this.p_diffuse_reflection)
			//{
			//	valid = this.diffuse_reflection.sample(o, i, pdf, weight, lobe, sample_generator);
			//	weight /= this.p_diffuse_reflection;
			//	weight *= (1.f - this.specular_transmission_weight) * (1.f - this.diffuse_transmission_weight);
			//	pdf *= this.p_diffuse_reflection;
			//	pdfSingle = pdf;
			//	if (this.p_specular_reflection > 0.f) pdf += this.p_specular_reflection * this.specular_reflection.eval_pdf(o, i);
			//}
			//else if (u_select < this.p_diffuse_reflection + this.p_diffuse_transmission)
			//{
			//	valid = this.diffuse_transmission.sample(o, i, pdf, weight, lobe, sample_generator);
			//	weight /= this.p_diffuse_transmission;
			//	weight *= (1.f - this.specular_transmission_weight) * this.diffuse_transmission_weight;
			//	pdf *= this.p_diffuse_transmission;
			//	pdfSingle = pdf;
			//	if (this.p_specular_transmission > 0.f) pdf += this.p_specular_transmission * this.dielectric_bxdf.eval_pdf(o, i);
			//}
			//else if (u_select < this.p_diffuse_reflection + this.p_diffuse_transmission + this.p_specular_reflection)
			//{
			//	valid = this.conductor_bxdf.sample(shading_data.wo, bsdf_sample, sample_generator);
			//	//weight = this.conductor_bxdf.eval(o, i, true) / this.conductor_bxdf.eval_pdf(o, i, true);
			//	bsdf_sample.weight = bsdf_sample.weight * abs(::framework::cos_theta(bsdf_sample.wi));
			//	//weight /= this.p_specular_reflection;
			//	//weight *= (1.f - this.specular_transmission_weight);
			//	//bsdf_sample.pdf *= this.p_specular_reflection;
			//	bsdf_sample.pdf_single = bsdf_sample.pdf;
			//	//if ((lobe & (uint)lobe_type_delta) == 0)
			//	//{
			//	//	if (this.p_diffuse_reflection > 0.f) pdf += this.p_diffuse_reflection * this.diffuse_reflection.eval_pdf(o, i);
			//	//}
			//}
			//else if (this.p_specular_transmission > 0.f)
			{
				valid = this.dielectric_bxdf.sample(shading_data.wo, bsdf_sample, sample_generator);
				//weight = this.dielectric_bxdf.eval(o, i, true) / this.dielectric_bxdf.eval_pdf(o, i, true);
				bsdf_sample.weight = bsdf_sample.weight * abs(::framework::cos_theta(bsdf_sample.wi));
				//weight /= this.p_specular_transmission;
				//weight *= this.specular_transmission_weight;
				//bsdf_sample.pdf *= this.p_specular_transmission;
				bsdf_sample.pdf_single = bsdf_sample.pdf;
				//if ((lobe & (uint)lobe_type_delta) == 0)
				//{
				//	if (this.p_diffuse_transmission > 0.f) pdf += this.p_diffuse_transmission * this.diffuse_transmission.eval_pdf(o, i);
				//}
			}

			//pdf = this.eval_pdf(o, i);
			//weight = pdf > 0.f ? this.eval(o, i) / pdf : 0.f;

			bsdf_sample.wi = shading_data.from_local(bsdf_sample.wi);

			return valid;
		}

		float eval_pdf(float3 wo, float3 wi, uint allowed_sampled_types = -1, bool allow_delta_eval = false)
		{
			float pdf = 0.f;
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::DiffuseReflection) && this.p_diffuse_reflection > 0.f && !allow_delta_eval) pdf += this.p_diffuse_reflection * this.diffuse_reflection.eval_pdf(wo, wi);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::DiffuseTransmission) && this.p_diffuse_transmission > 0.f && !allow_delta_eval) pdf += this.p_diffuse_transmission * this.diffuse_transmission.eval_pdf(wo, wi);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::SpecularReflection) && this.p_specular_reflection > 0.f) pdf += this.p_specular_reflection * this.specular_reflection.eval_pdf(wo, wi, allow_delta_eval);
			//if ((allowed_sampled_types & (uint)SampledBSDFFlags::SpecularTransmission) && this.p_specular_transmission > 0.f) pdf += this.p_specular_transmission * this.dielectric_bxdf.eval_pdf(wo, wi, allow_delta_eval);
			pdf +=  this.dielectric_bxdf.eval_pdf(wo, wi, allow_delta_eval);
			return pdf;
		}
	};
}