#include "globals.hlsl"
#include "PathTracing.hlsl"

#define WG_SIZE 128

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

struct CompactTreeNode
{
	uint4 field0; // parent, left, right, triangleID
	float4 field1; // boundMin.xyz, cost
	float4 field2; // boundMax.xyz, area
};

StructuredBuffer<CompactTriangle>	triangles				: register(t0);
RWStructuredBuffer<CompactTreeNode>	radixTree				: register(u0);
RWStructuredBuffer<uint>			nodeCounter				: register(u1);

cbuffer CustomUB: register(b0)
{
	uint numTriangles;
};

float calculateBoundingBoxSurfaceArea(Bound bound)
{
	float3 size;
	size.x = bound.max.x - bound.min.x;
	size.y = bound.max.y - bound.min.y;
	size.z = bound.max.z - bound.min.z;
	return 2 * (size.x * size.y + size.x * size.z + size.y * size.z);
}

void calculateNodeBoundingBox(inout Bound bound, Bound leftBound, Bound rightBound)
{
	bound.min = min(leftBound.min, rightBound.min);
	bound.max = max(leftBound.max, rightBound.max);
}

groupshared Bound sharedBounds[WG_SIZE];

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	const uint i = dispatchThreadID.x;
	const uint numberOfTriangles = numTriangles;
	const uint firstThreadInBlock = groupID.x * WG_SIZE;
	const uint lastThreadInBlock = firstThreadInBlock + WG_SIZE - 1;
	
	if (i >= numberOfTriangles)
		return;

	uint index = i + numberOfTriangles - 1;
	uint dataIndex = radixTree[index].field0.w;

	// Set leaves left and right indices
	radixTree[index].field0.y = 0xFFFFFFFF;
	radixTree[index].field0.z = 0xFFFFFFFF;

	const float3 vertex1 = triangles[dataIndex].field0.xyz;
	const float3 vertex2 = triangles[dataIndex].field1.xyz;
	const float3 vertex3 = triangles[dataIndex].field2.xyz;
	Bound cacheBound;
	cacheBound.pad0 = 0;
	cacheBound.pad1 = 0;
	calculateLeafBoundingBox(vertex1, vertex2, vertex3, cacheBound);
	sharedBounds[groupThreadID.x] = cacheBound;
	radixTree[index].field1.xyz = cacheBound.min;
	radixTree[index].field2.xyz = cacheBound.max;

	GroupMemoryBarrier();

	radixTree[index].field2.w = calculateBoundingBoxSurfaceArea(cacheBound);

	uint lastNode = index;
	uint current = radixTree[index].field0.x;
	while (current != 0xFFFFFFFF)
	{
		// In the counters array, we have stored the id of the thread that processed the other
		// children of this node
		uint childThreadId;
		InterlockedExchange(nodeCounter[current], i, childThreadId);

		// The first thread to reach a node will just die
		if (childThreadId == 0xFFFFFFFF)
		{
			return;
		}

		Bound childBound;
		if (childThreadId >= firstThreadInBlock && childThreadId <= lastThreadInBlock)
		{
			// If both child nodes were processed by the same block, we can reuse the values
			// cached in shared memory
			uint childThreadIdInBlock = childThreadId - firstThreadInBlock;
			childBound = sharedBounds[childThreadIdInBlock];
		}
		else
		{
			// The children were processed in different blocks, so we have to find out if the one
			// that was not processed by this thread was the left or right one
			uint childIndex = radixTree[current].field0.y;
			if (childIndex == lastNode)
			{
				childIndex = radixTree[current].field0.z;
			}

			childBound.min = radixTree[childIndex].field1.xyz;
			childBound.max = radixTree[childIndex].field2.xyz;
		}

		GroupMemoryBarrier();

		// Update node bounding box
		calculateNodeBoundingBox(cacheBound, cacheBound, childBound);
		sharedBounds[groupThreadID.x] = cacheBound;
		radixTree[current].field1.xyz = cacheBound.min;
		radixTree[current].field2.xyz = cacheBound.max;

		GroupMemoryBarrier();
		
		// Calculate surface area
		radixTree[current].field2.w = calculateBoundingBoxSurfaceArea(cacheBound);

		// Update last processed node
		lastNode = current;

		// Update current node pointer
		current = radixTree[current].field0.x;
	}
}