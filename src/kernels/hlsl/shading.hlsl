#pragma once

#include "bxdf_utils.hlsl"
#include "fresnel.hlsl"
#include "material_data.hlsl"
#include "scene_types.hlsl"
#include "quaternion.hlsl"

namespace framework
{
	// Data needed to evaluate BRDF (surface and material properties at given point + configuration of light and normal vectors)
	struct shading_data_t
	{
		// Material properties
		float3 specularF0;
		float3 diffuseReflectance;

		// Commonly used terms for BRDF evaluation
		float3 F; //< Fresnel term

		// Vectors in normal's local space
		float3 wo; //< Direction to viewer (or opposite direction of incident ray)
		float3 i; //< Direction to light (or direction of reflecting ray)
		float3 h; //< Half vector (microfacet normal)
		float3 n; //< normal, always float3(0.f, 0.f, 1.0f)

		float LdotH;
		float NdotH;
		float VdotH;

		float eta_i;
		float eta_t;

		// True when wo/i is backfacing wrt. shading normal n
		bool o_backfacing;
		bool i_backfacing;

		// Quaternion of local space basis
		float4 q_rotation_to_z;

		// Precalculates commonly used terms in BRDF evaluation
		// Clamps around dot products prevent NaNs and ensure numerical stability, but make sure to
		// correctly ignore rays outside of the sampling hemisphere, by using 'o_backfacing' and 'i_backfacing' flags
		void setup(::framework::vertex_data vertex_data, float3 wo, float ior, ::framework::material_data material)
		{
			this.o_backfacing = dot(vertex_data.face_normal, wo) < 0.f;
			this.n = float3(0.f, 0.f, 1.f);

			// Transform view direction into local space of our sampling routines 
			// (local space is oriented so that its positive Z axis points along the shading normal)
			this.q_rotation_to_z = ::framework::get_rotation_to_z_axis(dot(vertex_data.normal, wo) < 0.f ? -vertex_data.normal : vertex_data.normal);
			this.wo = ::framework::rotate_point(this.q_rotation_to_z, wo);

			// View vector should be always at the same side with normal
			if (this.o_backfacing)
			{
				this.eta_i = material.ior;
				this.eta_t = ior;
			}
			else
			{
				this.eta_i = ior;
				this.eta_t = material.ior;
			}

			// Unpack material properties
			this.specularF0 = ::framework::baseColorToSpecularF0(material.baseColor, material.metalness);
			this.diffuseReflectance = ::framework::baseColorToDiffuseReflectance(material.baseColor, material.metalness);

			// Pre-calculate some more BRDF terms
			this.F = ::framework::evalFresnel(this.specularF0, ::framework::shadowedF90(this.specularF0), this.wo.z);
		}

		void setup_wm(float3 wm)
		{
			this.h = wm;
			this.NdotH = max(0.00001f, min(1.0f, abs(dot(this.h, this.n))));
			this.VdotH = max(0.00001f, min(1.0f, abs(dot(this.h, this.wo))));
		}

		void setup_lighting_info(float3 i)
		{
			this.i = i;
			this.i_backfacing = this.i.z <= 0.0f;
			this.LdotH = max(0.00001f, min(1.0f, abs(dot(this.h, this.i))));
			this.F = ::framework::evalFresnel(this.specularF0, ::framework::shadowedF90(this.specularF0), this.VdotH);
		}

		float3 to_local(float3 v)
		{
			return normalize(::framework::rotate_point(this.q_rotation_to_z, v));
		}

		float3 from_local(float3 v)
		{
			return normalize(::framework::rotate_point(::framework::invert_rotation(this.q_rotation_to_z), v));
		}
	};
}