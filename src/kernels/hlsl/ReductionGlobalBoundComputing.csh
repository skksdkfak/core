#include "globals.hlsl"
#include "PathTracing.hlsl"

#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM 64

RWStructuredBuffer<Bound>			bounds		: register(u0);

cbuffer CustomUB : register(b0)
{
	uint numElements;
};

groupshared Bound localBounds[WG_SIZE];

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	Bound localBound;
	localBound.min = FLT_MAX;
	localBound.pad0 = 0;
	localBound.max = FLT_MIN;
	localBound.pad1 = 0;

	localBounds[groupThreadID.x] = localBound;

	GroupMemoryBarrierWithGroupSync();

	for (uint i = groupThreadID.x * ELEMENTS_PER_WORK_ITEM + groupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM; i < numElements, i < groupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM + groupThreadID.x * ELEMENTS_PER_WORK_ITEM + ELEMENTS_PER_WORK_ITEM; i++)
	{
		Bound bound = bounds[i];
		localBound.min = min(localBound.min, bound.min);
		localBound.max = max(localBound.max, bound.max);
	}

	localBounds[groupThreadID.x] = localBound;

	GroupMemoryBarrierWithGroupSync();

	if (groupThreadID.x == 0)
	{
		localBound = localBounds[0];
		for (uint i = 1; i < WG_SIZE; i++)
		{
			merge_bounds(localBounds[i], localBound, localBound);
		}
		bounds[groupID.x] = localBound;
	}
}