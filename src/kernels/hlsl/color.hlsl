#pragma once

#include "ray_tracing/descriptor_set_0.hlsl"

namespace framework
{
	static const int sRGBToSpectrumTable_Res = 64;

	struct rgb_sigmoid_polynomial
	{
		static ::framework::rgb_sigmoid_polynomial create(float c0, float c1, float c2)
		{
			::framework::rgb_sigmoid_polynomial result;
			result.c0 = c0;
			result.c1 = c1;
			result.c2 = c2;
			return result;
		}

		float operator()(float lambda)
		{
			float t = lambda;
			float x = t * (t * c0 + c1) + c2;
			float s = .5f + x / (2 * sqrt(1 + ::framework::square(x)));

			return s;
		}

		float c0, c1, c2;
	};

	int FindInterval(float z)
	{
		int size = (int)::framework::sRGBToSpectrumTable_Res - 2, first = 1;
		while (size > 0)
		{
			// Evaluate predicate at midpoint and update _first_ and _size_
			int half = (int)size >> 1, middle = first + half;
			bool predResult = rgb_spectrum_table_scale[middle] < z;
			first = predResult ? middle + 1 : first;
			size = predResult ? size - (half + 1) : half;
		}
		return (int)clamp((int)first - 1, 0, ::framework::sRGBToSpectrumTable_Res - 2);
	}

	::framework::rgb_sigmoid_polynomial test1(float3 rgb)
	{
		//return ::framework::rgb_sigmoid_polynomial::create(0.000666365842, -0.660717070, 157.618164);
		// Handle uniform rgb values
		if (rgb[0] == rgb[1] && rgb[1] == rgb[2])
			return ::framework::rgb_sigmoid_polynomial::create(0, 0, (rgb[0] - .5f) / sqrt(rgb[0] * (1 - rgb[0])));

		// Find maximum component and compute remapped component values
		int maxc = (rgb[0] > rgb[1]) ? ((rgb[0] > rgb[2]) ? 0 : 2) : ((rgb[1] > rgb[2]) ? 1 : 2);
		float z = rgb[maxc];
		float x = rgb[(maxc + 1) % 3] * (::framework::sRGBToSpectrumTable_Res - 1) / z;
		float y = rgb[(maxc + 2) % 3] * (::framework::sRGBToSpectrumTable_Res - 1) / z;

		// Compute integer indices and offsets for coefficient interpolation
		int xi = min((int)x, ::framework::sRGBToSpectrumTable_Res - 2), yi = min((int)y, ::framework::sRGBToSpectrumTable_Res - 2), zi = FindInterval(z);
		float dx = x - xi, dy = y - yi, dz = (z - rgb_spectrum_table_scale[zi]) / (rgb_spectrum_table_scale[zi + 1] - rgb_spectrum_table_scale[zi]);

		const uint offset_0 = 3;
		const uint offset_1 = offset_0 * ::framework::sRGBToSpectrumTable_Res;
		const uint offset_2 = offset_1 * ::framework::sRGBToSpectrumTable_Res;
		const uint offset_3 = offset_2 * ::framework::sRGBToSpectrumTable_Res;

		// Trilinearly interpolate sigmoid polynomial coefficients c
		float c[3];
		for (int i = 0; i < 3; ++i)
		{
#			define co(dx, dy, dz) rgb_spectrum_table_data[maxc * offset_3 + (zi + dz) * offset_2 + (yi + dy) * offset_1 + (xi + dx) * offset_0 + i]
			c[i] = lerp(
				lerp(lerp(co(0, 0, 0), co(1, 0, 0), dx),
					lerp(co(0, 1, 0), co(1, 1, 0), dx), dy),
				lerp(lerp(co(0, 0, 1), co(1, 0, 1), dx),
					lerp(co(0, 1, 1), co(1, 1, 1), dx), dy), dz);
		}

		return ::framework::rgb_sigmoid_polynomial::create(c[0], c[1], c[2]);
	}
}