#include "globals.hlsl"
#include "PathTracing.hlsl"

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

StructuredBuffer<Bound>				globalBound				: register(t0);
StructuredBuffer<CompactTriangle>	triangles				: register(t1);
RWStructuredBuffer<uint>			morton_codes			: register(u0);
RWStructuredBuffer<uint>			primitive_indices		: register(u1);

cbuffer CustomUB: register(b0)
{
	uint numTriangles;
};

float3 getTriangleCentroid(float3 vertex1, float3 vertex2, float3 vertex3)
{
	Bound bound;
	bound.pad0 = 0;
	bound.pad1 = 0;
	calculateLeafBoundingBox(vertex1, vertex2, vertex3, bound);
	return (bound.max + bound.min) * 0.5f;
}

float3 normalizeCentroid(float3 c, Bound bound)
{
	float3 normalized;
	normalized.x = (c.x - bound.min.x) / (bound.max.x - bound.min.x);
	normalized.y = (c.y - bound.min.y) / (bound.max.y - bound.min.y);
	normalized.z = (c.z - bound.min.z) / (bound.max.z - bound.min.z);
	return normalized;
}

uint expandBits(uint value)
{
	value = (value * 0x00010001u) & 0xFF0000FFu;
	value = (value * 0x00000101u) & 0x0F00F00Fu;
	value = (value * 0x00000011u) & 0xC30C30C3u;
	value = (value * 0x00000005u) & 0x49249249u;
	return value;
}

uint calculateMortonCode(float3 p)
{
	// Discretize the unit cube into a 10 bit integer
	uint3 discretized;
	discretized.x = (uint)min(max(p.x * 1024.0f, 0.0f), 1023.0f);
	discretized.y = (uint)min(max(p.y * 1024.0f, 0.0f), 1023.0f);
	discretized.z = (uint)min(max(p.z * 1024.0f, 0.0f), 1023.0f);

	discretized.x = expandBits(discretized.x);
	discretized.y = expandBits(discretized.y);
	discretized.z = expandBits(discretized.z);

	return discretized.x * 4 + discretized.y * 2 + discretized.z;
}

[numthreads(64, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	const uint thxId = dispatchThreadID.x;
	if (thxId >= numTriangles)
		return;

	primitive_indices[thxId] = thxId;

	const float3 vertex1 = triangles[thxId].field0.xyz;
	const float3 vertex2 = triangles[thxId].field1.xyz;
	const float3 vertex3 = triangles[thxId].field2.xyz;
	float3 triangleCentroid = getTriangleCentroid(vertex1, vertex2, vertex3);
	float3 normalizedCentroid = normalizeCentroid(triangleCentroid, globalBound[0]);
	const uint mortonCode = calculateMortonCode(normalizedCentroid);

	morton_codes[thxId] = mortonCode;
}