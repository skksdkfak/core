#pragma once

namespace framework
{
	template<typename sample_generator_t>
	float sample_next_1d(inout sample_generator_t sample_generator)
	{
		// Use upper 24 bits and divide by 2^24 to get a number u in [0,1).
		// In floating-point precision this also ensures that 1.0-u != 0.0.
		uint bits = sample_generator.next();
		return (bits >> 8) * 0x1p-24;
	}

	template<typename sample_generator_t>
	float2 sample_next_2d(inout sample_generator_t sample_generator)
	{
		float2 sample;
		// Don't use the float2 initializer to ensure consistent order of evaluation.
		sample.x = ::framework::sample_next_1d(sample_generator);
		sample.y = ::framework::sample_next_1d(sample_generator);
		return sample;
	}

	template<typename sample_generator_t>
	float3 sample_next_3d(inout sample_generator_t sample_generator)
	{
		float3 sample;
		// Don't use the float3 initializer to ensure consistent order of evaluation.
		sample.x = ::framework::sample_next_1d(sample_generator);
		sample.y = ::framework::sample_next_1d(sample_generator);
		sample.z = ::framework::sample_next_1d(sample_generator);
		return sample;
	}
}