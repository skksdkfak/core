#pragma once

#include "bxdf.hlsl"
#include "phase_function.hlsl"

namespace framework
{
	struct interaction
	{
		float3 pi;
		float3 n;
		float3 wo;
		float time;
		bool medium;

		static ::framework::interaction create(float3 p)
		{
			::framework::interaction result;
			result.pi = p;
			return result;
		}

		::framework::ray spawn_ray_to(::framework::interaction it)
		{
			::framework::ray r = ::framework::spawn_ray_to(this.pi, this.n, this.time, it.pi, it.n);
			//r.medium = GetMedium(r.d);
			return r;
		}
	};

	struct medium_interaction : ::framework::interaction
	{
		::framework::henyey_greenstein_phase_function phase;
	};

	struct surface_interaction : ::framework::interaction
	{
		::framework::standard_bxdf_t bsdf;
	};

	struct interaction_
	{
		// todo: wrap into union when it'll be available in HLSL
		::framework::medium_interaction medium_interaction;
		::framework::surface_interaction surface_interaction;

		template <typename T>
		bool is()
		{
			return T::type_index == this.tag;
		}

		uint tag;
	};
}