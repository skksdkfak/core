#pragma once

#include "phase_function.hlsl"
#include "spectrum.hlsl"

namespace framework
{
	struct ray_majorant_segment
	{
		float t_min, t_max;
		::framework::sampled_spectrum sigma_maj;
	};

	struct medium_properties
	{
		::framework::sampled_spectrum sigma_a, sigma_s;
		::framework::henyey_greenstein_phase_function phase;
		::framework::sampled_spectrum Le;
	};

	struct homogeneous_majorant_iterator
	{
		static ::framework::homogeneous_majorant_iterator create(float t_min, float t_max, ::framework::sampled_spectrum sigma_maj)
		{
			::framework::homogeneous_majorant_iterator result;
			result.seg.t_min = t_min;
			result.seg.t_max = t_max;
			result.seg.sigma_maj = sigma_maj;
			result.called = false;
			return result;
		}

		bool next(out ::framework::ray_majorant_segment seg)
		{
			if (this.called)
				return false;
			this.called = true;
			seg = this.seg;
			return true;
		}

		::framework::ray_majorant_segment seg;
		bool called;
	};

}