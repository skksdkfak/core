#pragma once

namespace framework
{
	// Calculates rotation quaternion from input vector to the vector (0, 0, 1)
	// Input vector must be normalized!
	float4 get_rotation_to_z_axis(float3 input)
	{
		// Handle special case when input is exact or near opposite of (0, 0, 1)
		if (input.z < -0.99999f) return float4(1.0f, 0.0f, 0.0f, 0.0f);

		return normalize(float4(input.y, -input.x, 0.0f, 1.0f + input.z));
	}

	// Calculates rotation quaternion from vector (0, 0, 1) to the input vector
	// Input vector must be normalized!
	float4 get_rotation_from_z_axis(float3 input)
	{
		// Handle special case when input is exact or near opposite of (0, 0, 1)
		if (input.z < -0.99999f) return float4(1.0f, 0.0f, 0.0f, 0.0f);

		return normalize(float4(-input.y, input.x, 0.0f, 1.0f + input.z));
	}

	// Returns the quaternion with inverted rotation
	float4 invert_rotation(float4 q)
	{
		return float4(-q.x, -q.y, -q.z, q.w);
	}

	// Optimized point rotation using quaternion
	// Source: https://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
	float3 rotate_point(float4 q, float3 v)
	{
		const float3 qAxis = float3(q.x, q.y, q.z);
		return 2.0f * dot(qAxis, v) * qAxis + (q.w * q.w - dot(qAxis, qAxis)) * v + 2.0f * q.w * cross(qAxis, v);
	}
}