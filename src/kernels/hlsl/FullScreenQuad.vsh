struct VS_OUTPUT
{
	float4 vPosition    : SV_POSITION;
	float2 texCoord		: TEXCOORD0;
};

VS_OUTPUT main(uint uVID : SV_VertexID, uint instanceID : SV_InstanceID)
{
	VS_OUTPUT Out;

	Out.texCoord = float2(uVID & 2, (uVID << 1) & 2);
	Out.texCoord.y = 1.0f - Out.texCoord.y;
	Out.vPosition = float4(lerp(float2(-1, 1), float2(1, -1), Out.texCoord), 0, 1);
	
	return Out;
}