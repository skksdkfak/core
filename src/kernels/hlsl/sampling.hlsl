#pragma once

#include "math.hlsl"

namespace framework
{
	float sample_visible_wavelengths(float u)
	{
		return 538 - 138.888889f * ::framework::atanh(0.85691062f - 1.82750197f * u);
	}

	float visible_wavelengths_pdf(float lambda)
	{
		if (lambda < 360 || lambda > 830)
			return 0;
		return 0.0039398042f / ::framework::square(cosh(0.0072f * (lambda - 538)));
	}

	float sample_exponential(float u, float a)
	{
		return -log(1 - u) / a;
	}

	template<int n_weights>
	uint sample_discrete(float weights[n_weights], float u)
	{
		// Compute sum of weights
		float sum_weights = 0;
		[unroll]
		for (uint i = 0; i < n_weights; i++)
			sum_weights += weights[i];

		// Compute rescaled $u'$ sample
		float up = u * sum_weights;
		//if (up == sum_weights)
		//	up = NextFloatDown(up);

		// Find offset in weights corresponding to u
		uint offset = 0;
		float sum = 0;
		while (sum + weights[offset] <= up)
		{
			sum += weights[offset++];
		}

		return offset;
	}
}