#pragma once

#include "scene_types.hlsl"

namespace framework
{
	struct geometry_hit
	{
		::framework::geometry_instance_id instance_id;
		uint primitive_index;
		float2 barycentrics;

		float3 get_barycentric_weights()
		{
			return float3(1.f - this.barycentrics.x - this.barycentrics.y, this.barycentrics.x, this.barycentrics.y);
		}
	};

	struct triangle_hit : ::framework::geometry_hit
	{
	};

	struct hit_info
	{
		static const uint data_size = 4;

		uint data[::framework::hit_info::data_size];

		static ::framework::hit_info create(::framework::triangle_hit triangle_hit)
		{
			::framework::hit_info hit_info;
			hit_info.data[0] = triangle_hit.instance_id.index;
			hit_info.data[1] = triangle_hit.primitive_index;
			hit_info.data[2] = asuint(triangle_hit.barycentrics.x);
			hit_info.data[3] = asuint(triangle_hit.barycentrics.y);
			return hit_info;
		}

		::framework::triangle_hit get_triangle_hit()
		{
			::framework::triangle_hit triangle_hit;
			triangle_hit.instance_id.index = this.data[0];
			triangle_hit.primitive_index = this.data[1];
			triangle_hit.barycentrics.x = asfloat(this.data[2]);
			triangle_hit.barycentrics.y = asfloat(this.data[3]);
			return triangle_hit;
		}
	};
}