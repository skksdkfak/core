#pragma once

#include "math.hlsl"

// Specifies minimal reflectance for dielectrics (when metalness is zero)
// Nothing has lower reflectance than 2%, but we use 4% to have consistent results with UE4, Frostbite, et al.
#define MIN_DIELECTRICS_F0 0.04f

namespace framework
{
	// Converts Phong's exponent (shininess) to Beckmann roughness (alpha)
	// Source: "Microfacet Models for Refraction through Rough Surfaces" by Walter et al.
	float shininessToBeckmannAlpha(float shininess) {
		return sqrt(2.0f / (shininess + 2.0f));
	}

	// Converts Beckmann roughness (alpha) to Phong's exponent (shininess)
	// Source: "Microfacet Models for Refraction through Rough Surfaces" by Walter et al.
	float beckmannAlphaToShininess(float alpha) {
		return 2.0f / min(0.9999f, max(0.0002f, (alpha * alpha))) - 2.0f;
	}

	// Converts Beckmann roughness (alpha) to Oren-Nayar roughness (sigma)
	// Source: "Moving Frostbite to Physically Based Rendering" by Lagarde & de Rousiers
	float beckmannAlphaToOrenNayarRoughness(float alpha) {
		return 0.7071067f * atan(alpha);
	}

	float3 baseColorToSpecularF0(float3 baseColor, float metalness) {
		return lerp(float3(MIN_DIELECTRICS_F0, MIN_DIELECTRICS_F0, MIN_DIELECTRICS_F0), baseColor, metalness);
	}

	float3 baseColorToDiffuseReflectance(float3 baseColor, float metalness)
	{
		return baseColor * (1.0f - metalness);
	}

	float cos_theta(float3 w)
	{
		return w.z;
	}

	float abs_cos_theta(float3 w)
	{
		return abs(w.z);
	}

	float cos_2_theta(float3 w)
	{
		return ::framework::square(w.z);
	}

	float sin_2_theta(float3 w)
	{
		return max(0, 1 - ::framework::cos_2_theta(w));
	}

	float sin_theta(float3 w)
	{
		return sqrt(::framework::sin_2_theta(w));
	}

	float tan_2_theta(float3 w)
	{
		return ::framework::sin_2_theta(w) / ::framework::cos_2_theta(w);
	}

	//float cos_phi(float3 w)
	//{
	//	float sin_theta = ::framework::sin_theta(w);
	//	return sin_theta == 0 ? 1 : clamp(w.x / sin_theta, -1, 1);
	//}
	//
	//float sin_phi(float3 w)
	//{
	//	float sinTheta = SinTheta(w);
	//	return (sinTheta == 0) ? 0 : Clamp(w.y / sinTheta, -1, 1);
	//}
}