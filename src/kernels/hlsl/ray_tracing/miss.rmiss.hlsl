#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/common.hlsl"
#include "utils.hlsl"

[shader("miss")]
void miss(inout ::framework::payload_t payload)
{
    payload.hit_t = -1.0;

    //const float f = (0.5f + 0.5f * WorldRayDirection().y) * 1.0;

    //p.color = float3(1, 0, 0);

    //float2 uv = ::framework::cartesian_to_spherical(WorldRayDirection()) / float2(2.0 * ::framework::numbers::pi, ::framework::numbers::pi);
    ////p.color = float3(DispatchRaysIndex()) / float3(DispatchRaysDimensions());
    ////p.color = float3(uv, 0);
    //p.color = textures[0].SampleLevel(linear_sampler, uv, 0);
    ////p.color = radiance_ibl_texture.SampleLevel(linear_sampler, WorldRayDirection(), 0);
    ////p.color = float3(0.846, 0.933, 0.949) * exp(0.5f + 0.5f * WorldRayDirection().y) * 5 - 5;
	////p.refracted = false;
}