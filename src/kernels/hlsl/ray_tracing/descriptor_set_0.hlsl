#pragma once

#include "scene_types.hlsl"
#include "ray_tracing/common.hlsl"

[[vk::binding(0, 0)]] RaytracingAccelerationStructure raytracing_acceleration_structure : register(t0, space0);

[[vk::binding(1, 0)]] RWStructuredBuffer<::framework::path_reservoir_t> g_initial_samples : register(u1, space0);

[[vk::binding(2, 0)]] RWStructuredBuffer<::framework::path_reservoir_t> g_resampled_samples : register(u2, space0);

[[vk::binding(3, 0)]] RWTexture2D<float4> accumulator : register(u0, space0);

[[vk::binding(4, 0)]] SamplerState linear_sampler : register(s0, space0);

[[vk::binding(5, 0)]] StructuredBuffer<uint> materials : register(t1, space0);

[[vk::binding(6, 0)]] ByteAddressBuffer indices : register(t2, space0);

[[vk::binding(7, 0)]] StructuredBuffer<::framework::packed_static_vertex_data> vertices : register(t3, space0);

[[vk::binding(8, 0)]] StructuredBuffer<float> rgb_spectrum_table_scale : register(t4, space0);

[[vk::binding(9, 0)]] StructuredBuffer<float> rgb_spectrum_table_data : register(t5, space0);

[[vk::binding(10, 0)]] StructuredBuffer<float> spectrum : register(t6, space0);

[[vk::binding(11, 0)]] Texture2D<float4> textures[] : register(t7, space0);