#pragma once

#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/path_state.hlsl"
#include "bxdf.hlsl"
#include "color.hlsl"
#include "interaction.hlsl"
#include "light.hlsl"
#include "material_data.hlsl"
#include "medium.hlsl"
#include "numbers.hlsl"
#include "phase_function.hlsl"
#include "ray.hlsl"
#include "sampling.hlsl"
#include "scene.hlsl"
#include "shading.hlsl"
#include "spectrum.hlsl"
#include "utils.hlsl"

static ::framework::sampled_spectrum sigma_a = ::framework::sampled_spectrum::create(0.0011f);
static ::framework::sampled_spectrum sigma_s = ::framework::sampled_spectrum::create(1.0f);
static ::framework::sampled_spectrum sigma_t = sigma_a + sigma_s;

namespace framework
{
	struct path_tracer
	{
		::framework::scene scene;

		static ::framework::path_tracer create(::framework::scene scene)
		{
			::framework::path_tracer path_tracer;
			path_tracer.scene = scene;
			return path_tracer;
		}

		void generate_path(const uint path_id, out ::framework::path_state path_state)
		{
			path_state.id = path_id;
			path_state.flags = 0;
			path_state.length = 0;
			path_state.throughput = ::framework::sampled_spectrum::create(1.f);
			path_state.r_u = ::framework::sampled_spectrum::create(1.f);
			path_state.r_l = ::framework::sampled_spectrum::create(1.f);
			path_state.lighting = ::framework::sampled_spectrum::create(0.f);
			path_state.set_active();

			const uint2 pixel = path_state.get_pixel();

			path_state.sample_generator = ::framework::uniform_sample_generator::create(pixel, constant_buffer.seed);

			float u = ::framework::sample_next_1d(path_state.sample_generator);
			path_state.lambda = ::framework::sampled_wavelengths::sample_visible(u);

			const float2 pixel_center = float2(pixel)+float2(0.5, 0.5);
			const float2 uv = pixel_center / float2(DispatchRaysDimensions().xy);
			float2 d = uv * 2.0 - 1.0;
			float4 target = mul(constant_buffer.projInverse, float4(d.x, d.y, 1, 1));

			path_state.origin = mul(constant_buffer.viewInverse, float4(0, 0, 0, 1)).xyz;
			path_state.direction = mul(constant_buffer.viewInverse, float4(normalize(target.xyz), 0)).xyz;
		}

		void next_vertex(inout ::framework::path_state path_state)
		{
			if (path_state.length > 16)
			{
				path_state.terminate();
				return;
			}

			float t_max = 1.#INF;
			RayDesc ray_desc = path_state.get_scatter_ray().get_ray_desc(t_max);
			::framework::payload_t payload;
			TraceRay(raytracing_acceleration_structure, RAY_FLAG_FORCE_OPAQUE, 0xff, 0, 1, 0, ray_desc, payload);

			path_state.length++;

			if (payload.hit_t >= 0.f)
			{
				path_state.hit_t = payload.hit_t;
				path_state.set_hit(payload.hit_info);
			}
			else
			{
				path_state.hit_t = t_max;
				path_state.clear_hit();
			}
		}

		void handle_hit(inout ::framework::path_state path_state)
		{
			bool valid = this.generate_surface_scatter_ray(path_state);

			if (!valid)
			{
				path_state.terminate();
			}
		}

		bool sample_t_maj(inout ::framework::path_state path_state, float3 p, ::framework::medium_properties mp, ::framework::sampled_spectrum sigma_maj, ::framework::sampled_spectrum t_maj)
		{
			// Handle medium scattering event for ray
			if (!(bool)path_state.throughput.valid())
			{
				path_state.terminate();
				return false;
			}

			// Add emission from medium scattering event
			{
				// todo
			}

			// Compute medium event probabilities for interaction
			float p_absorb = mp.sigma_a[0] / sigma_maj[0];
			float p_scatter = mp.sigma_s[0] / sigma_maj[0];
			float p_null = max(0, 1 - p_absorb - p_scatter);

			// Sample medium scattering event type and update path
			float um = ::framework::sample_next_1d(path_state.sample_generator);

			float weights[] = { p_absorb, p_scatter, p_null };
			uint mode = ::framework::sample_discrete(weights, um);
			if (mode == 0)
			{
				// Handle absorption along ray path
				path_state.terminate();
				return false;
			}
			else if (mode == 1)
			{
				//// Handle scattering along ray path
				//// Stop path sampling if maximum depth has been reached
				//if (depth++ >= maxDepth)
				//{
				//	path_state.terminate();
				//	return false;
				//}

				// Update beta and r_u for real scattering event
				float pdf = t_maj[0] * mp.sigma_s[0];
				path_state.throughput = path_state.throughput * t_maj * mp.sigma_s / pdf;
				path_state.r_u = path_state.r_u * t_maj * mp.sigma_s / pdf;

				if ((bool)path_state.throughput.valid() && (bool)path_state.r_u.valid())
				{
					//// Sample direct lighting at volume scattering event
					//MediumInteraction intr(p, -ray.d, ray.time, ray.medium, mp.phase);
					//L += SampleLd(intr, nullptr, lambda, sampler, path_state.throughput, path_state.r_u);

					// Sample new direction at real scattering event
					float pdf;
					float weight;
					float3 w_i;
					bool valid = mp.phase.sample(path_state.direction, w_i, pdf, weight, path_state.sample_generator);
					if (!valid || pdf == 0)
					{
						path_state.terminate();
					}
					else
					{
						// Update ray path state for indirect volume scattering
						path_state.throughput = path_state.throughput * weight;
						path_state.r_l = path_state.r_u / pdf;
						//prevIntrContext = LightSampleContext(intr);
						path_state.set_scattered(true);
						path_state.origin = p;
						path_state.direction = w_i;
						//specularBounce = false;
						//anyNonSpecularBounces = true;
					}
				}

				return false;
			}
			else
			{
				// Handle null scattering along ray path
				::framework::sampled_spectrum sigma_n = ::framework::clamp_zero(sigma_maj - mp.sigma_a - mp.sigma_s);
				float pdf = t_maj[0] * sigma_n[0];
				path_state.throughput = path_state.throughput * t_maj * sigma_n / pdf;
				if (pdf == 0)
					path_state.throughput = ::framework::sampled_spectrum::create(0.f);
				path_state.r_u = path_state.r_u * t_maj * sigma_n / pdf;
				path_state.r_l = path_state.r_l * t_maj * sigma_maj / pdf;
				return (bool)path_state.throughput.valid() && (bool)path_state.r_u.valid();
			}

			return true;
		}

		::framework::sampled_spectrum sample_t_maj(inout ::framework::path_state path_state, ::framework::ray ray, float t_max)
		{
			::framework::homogeneous_majorant_iterator iter = ::framework::homogeneous_majorant_iterator::create(0, t_max, sigma_a + sigma_s);

			// Generate ray majorant samples until termination
			::framework::sampled_spectrum t_maj = ::framework::sampled_spectrum::create(1.f);
			bool done = false;
			while (!done)
			{
				::framework::ray_majorant_segment seg;
				if (!iter.next(seg))
				{
					return t_maj;
				}

				// Handle zero-valued majorant for current segment
				{
					//todo
				}

				// Generate samples along current majorant segment
				float t_min = seg.t_min;
				while (true)
				{
					// Try to generate sample along current majorant segment
					float t = t_min + ::framework::sample_exponential(::framework::sample_next_1d(path_state.sample_generator), seg.sigma_maj[0]);
					if (t < seg.t_max)
					{
						t_maj = t_maj * ::framework::exp(seg.sigma_maj * (-(t - t_min)));

						float g = 0.0;
						::framework::medium_properties mp;
						mp.sigma_a = sigma_a;
						mp.sigma_s = sigma_s;
						mp.phase = ::framework::henyey_greenstein_phase_function::create(g);
						mp.Le = ::framework::sampled_spectrum::create(1.0f);
						if (!this.sample_t_maj(path_state, ray(t), mp, seg.sigma_maj, t_maj))
						{
							done = true;
							break;
						}
						t_maj = ::framework::sampled_spectrum::create(1.f);
						t_min = t;
					}
					else
					{
						// Handle sample past end of majorant segment
						float dt = seg.t_max - t_min;
						// Handle infinite dt for ray majorant segment
						//if (IsInf(dt))
						//	dt = std::numeric_limits<Float>::max();

						t_maj = t_maj * ::framework::exp(seg.sigma_maj * (-dt));
						break;
					}
				}
			}

			return ::framework::sampled_spectrum::create(1.f);
		}

		void handle_free_path(inout ::framework::path_state path_state)
		{
			path_state.set_scattered(false);

			::framework::sampled_spectrum t_maj = this.sample_t_maj(path_state, path_state.get_scatter_ray(), path_state.hit_t);

			// Handle terminated, scattered, and unscattered medium rays
			//if (path_state.is_terminated() || !(bool)path_state.throughput || !(bool)path_state.r_u)
			//	return L;
			if (!path_state.is_scattered())
			{
				path_state.throughput = path_state.throughput * t_maj / t_maj[0];
				path_state.r_u = path_state.r_u * t_maj / t_maj[0];
				path_state.r_l = path_state.r_l * t_maj / t_maj[0];
			}
		}

		::framework::sampled_spectrum sample_ld(inout ::framework::path_state path_state, ::framework::interaction interaction, ::framework::sampled_spectrum r_p)
		{
			// Estimate light-sampled direct illumination at interaction
			// Initialize _LightSampleContext_ for volumetric light sampling
			::framework::light_sample_context ctx;
			ctx.pi = interaction.get_position();

			::framework::point_light light;
			light.scale = 1.f;
			// Sample a point on the light source
			::framework::light_li_sample ls;
			bool ls_valid = light.sample_li(ctx, ls);
			if (!ls_valid)
				return ::framework::sampled_spectrum::create(0.f);
			float light_pdf = ls.pdf;

			// Evaluate BSDF or phase function for light sample direction
			float scatter_pdf;
			::framework::sampled_spectrum f_hat;
			float3 wo = interaction.get_wo(), wi = ls.wi;
			if (interaction is ::framework::surface_interaction)
			{
				::framework::surface_interaction surface_interaction3 = (interaction as ::framework::surface_interaction).value;
				// Update f_hat and scatter_pdf accounting for the BSDF
				//::framework::standard_bxdf_t bsdf = surface_interaction.bsdf;
				//f_hat = bsdf.eval(wo, wi)/* * AbsDot(wi, interaction.AsSurface().shading.n)*/;
				//scatter_pdf = bsdf.eval_pdf(wo, wi);
			}
			else
			{
				::framework::medium_interaction medium_interaction = (interaction as ::framework::medium_interaction).value;
				// Update f_hat and scatter_pdf accounting for the phase function
				::framework::phase_function phase = medium_interaction.phase;
				f_hat = ::framework::sampled_spectrum(phase.p(wo, wi));
				scatter_pdf = phase.pdf(wo, wi);
			}

			if (!f_hat.valid())
				return ::framework::sampled_spectrum::create(0.f);

			// Declare path state variables for ray to light source
			::framework::ray light_ray = ::framework::spawn_ray_to(interaction, ls.p_light);
			bool medium = /*interaction.medium*/true;
			::framework::sampled_spectrum t_ray = ::framework::sampled_spectrum::create(1.f);
			::framework::sampled_spectrum r_u = ::framework::sampled_spectrum::create(1.f);
			::framework::sampled_spectrum r_l = ::framework::sampled_spectrum::create(1.f);

			while (all(light_ray.direction != float3(0, 0, 0)))
			{
				float t_max = 999999999.f;
				RayDesc ray_desc = path_state.get_scatter_ray().get_ray_desc(t_max);
				::framework::payload_t payload;
				TraceRay(raytracing_acceleration_structure, RAY_FLAG_NONE, 0xff, 0, 1, 0, ray_desc, payload);

				if (payload.hit_t >= 0.f)
				{
					t_max = payload.hit_t;
					::framework::triangle_hit triangle_hit = payload.hit_info.get_triangle_hit();
					::framework::vertex_data vertex_data = this.scene.get_vertex_data(triangle_hit);
				}

				//// Handle opaque surface along ray's path
				//if (si && si->interaction.material)
				//      return ::framework::sampled_spectrum::create(0.f);

				// Update transmittance for current ray segment
				if (medium)
				{
					ld_sample_t_maj_callback callback;
					callback.t_ray;
					callback.r_l;
					callback.r_u;
					::framework::sampled_spectrum t_maj = ::framework::sample_t_maj(path_state.sample_generator, light_ray, t_max, callback);

					// Update transmittance estimate for final segment
					t_ray = t_ray * t_maj / t_maj.get(0);
					r_l = r_l * t_maj / t_maj.get(0);
					r_u = r_u * t_maj / t_maj.get(0);
				}

				// Generate next ray segment or return final transmittance
				if (!t_ray.valid())
					return ::framework::sampled_spectrum::create(0.f);
				if (payload.hit_t < 0.f)
					break;
				light_ray = ::framework::spawn_ray_to(interaction, ls.p_light);
				medium = !medium;
			}

			// Return path contribution function estimate for direct lighting
			r_l = r_l * r_p * light_pdf;
			r_u = r_u * r_p * scatter_pdf;
			if (/*IsDeltaLight(light.Type())*/true)
				return path_state.throughput * f_hat * t_ray * ls.l / r_l.average();
			else
				return path_state.throughput * f_hat * t_ray * ls.l / (r_l + r_u).average();
		}

		void handle_miss(inout ::framework::path_state path_state)
		{
			float2 uv = ::framework::cartesian_to_spherical(path_state.direction) / float2(2.0 * ::framework::numbers::pi, ::framework::numbers::pi);

			::framework::sampled_spectrum s;

			float3 rgb = textures[0].SampleLevel(linear_sampler, uv, 0);

			float m = max(rgb.r, max(rgb.g, rgb.b));
			float scale = 2 * m;
			if (scale > 0)
				rgb = rgb / scale;
			else
				rgb = 0.f;

			::framework::rgb_sigmoid_polynomial c = ::framework::test1(rgb);
			for (uint i = 0; i < ::framework::sampled_spectrum::n; i++)
			{
				s.set(i, scale * c(path_state.lambda.lambda[i]));
			}

			::framework::densely_sampled_spectrum illuminant = ::framework::densely_sampled_spectrum::create(471 * 0);

			path_state.lighting = path_state.lighting + path_state.throughput * s * illuminant.sample(path_state.lambda) * 0.00935831666;

			path_state.terminate();
		}

		bool generate_surface_scatter_ray(inout ::framework::path_state path_state)
		{
			path_state.lambda.terminate_secondary();

			::framework::material_data material;
			material.baseColor = 1.f/*payload.color*/;
			material.metalness = 0.0f;
			material.emissive = float3(0.0, 0.0, 0.0);
			material.roughness = /*constant_buffer.ray_eps*/0.0f;
			material.alpha = max(0.00001f, material.roughness * material.roughness);
			material.alphaSquared = max(0.00001f, material.alpha * material.alpha);
			material.diffuse_transmission = 0.f;
			material.specular_transmission = 0.f;
			material.transmissivness = 0.0;
			material.opacity = 1.0;
			//material.ior = 1.f;
			material.ior = lerp(1.46, 1.3, (path_state.lambda.lambda[0] - 360) / (830 - 360));

			const float3 w_o = -path_state.direction;
			const float3 hit_position = path_state.origin + path_state.direction * path_state.hit_t;

			::framework::triangle_hit triangle_hit = path_state.hit_info.get_triangle_hit();
			::framework::vertex_data vertex_data = this.scene.get_vertex_data(triangle_hit);

			::framework::shading_data_t shading_data;
			shading_data.setup(vertex_data, w_o, 1.f, material);

			::framework::standard_bxdf_t bsdf_data;
			bsdf_data.setup(shading_data, material);

			::framework::bsdf_sample bsdf_sample;
			bool valid = bsdf_data.sample(path_state.sample_generator, shading_data, bsdf_sample);

			if (valid)
			{
				path_state.direction = bsdf_sample.wi;
				path_state.throughput = path_state.throughput * bsdf_sample.weight;
				// Offset a new ray origin from the hitpoint to prevent self-intersections
				path_state.origin = ::framework::offset_ray(hit_position, vertex_data.face_normal, bsdf_sample.wi);

				if (dot(vertex_data.face_normal, w_o) * dot(vertex_data.face_normal, bsdf_sample.wi) < 0.f)
				{
					path_state.set_free_path(!path_state.is_free_path());
				}
			}

			return valid;
		}

		void write_output(::framework::path_state path_state)
		{
			const uint2 pixel = path_state.get_pixel();

			float3 lighting = 0.f;

			::framework::densely_sampled_spectrum r_bar = ::framework::densely_sampled_spectrum::create(471 * 1);
			::framework::densely_sampled_spectrum g_bar = ::framework::densely_sampled_spectrum::create(471 * 2);
			::framework::densely_sampled_spectrum b_bar = ::framework::densely_sampled_spectrum::create(471 * 3);

			path_state.lighting = ::framework::safe_div(path_state.lighting, path_state.lambda.get_pdf());
			lighting = float3(
				(r_bar.sample(path_state.lambda) * path_state.lighting).average(),
				(g_bar.sample(path_state.lambda) * path_state.lighting).average(),
				(b_bar.sample(path_state.lambda) * path_state.lighting).average()
				);

			float3x3 output_rgb_from_sensor_rgb =
			{
				3.24044490, -1.53713417, -0.498530030,
				-0.969267547, 1.87601352, 0.0415561050,
				0.0556435324, -0.204026386, 1.05722809
			};

			float filter_weight = 0.942312479;
			float3 rgb_sum = filter_weight * lighting;
			float weight_sum = filter_weight;

			if (weight_sum != 0)
				rgb_sum /= weight_sum;

			lighting = mul(output_rgb_from_sensor_rgb, lighting);

			float3 output_color;

			if (constant_buffer.sample_count > 1)
			{
				float4 accumulated = accumulator[pixel] + float4(lighting / samples_per_pixel, 0.0);
				//output_color = lerp(image[pixel], float4(color, 1.0), 1.0 / float(constant_buffer.sample_count + 1));
				accumulator[pixel] = accumulated;
				output_color = accumulated.rgb / constant_buffer.sample_count;
			}
			else
			{
				output_color = lighting / samples_per_pixel;
				accumulator[pixel] = float4(output_color, 0.0);
				//g_initial_samples[pixel] = g_resampled_samples[dispatchThreadID.x];
			}

			float exposure = 7.5f;
			float gamma = 2.2f;

			// Tone mapping
			output_color = uncharted_to_tonemap(output_color * exposure);
			output_color = output_color * (1.0f / uncharted_to_tonemap(11.2f));
			// Gamma correction
			output_color = pow(output_color, 1.0f / gamma);

			image[pixel] = float4(output_color, 1.f);
		}
	};
}