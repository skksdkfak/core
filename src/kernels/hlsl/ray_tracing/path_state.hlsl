#pragma once

#include "spectrum.hlsl"
#include "ray.hlsl"
#include "uniform_sample_generator.hlsl"

namespace framework
{
	enum class path_flags
	{
		active = 0x0001, ///< Path is active/terminated.
		hit = 0x0002, ///< Result of the scatter ray (0 = miss, 1 = hit).
		free_path = 0x0004, ///< This is a free path (volume sampling).
		scattered = 0x0008, ///< The ray has been scattered in volume.
	};

	struct path_state
	{
		uint id; ///< Path ID encodes (pixel, sample_index) with 12 bits each for pixel x|y and 8 bits for sample index.
		uint16_t flags; ///< Flags indicating the current status. This can be multiple ::framework::path_flags flags OR'ed together.
		uint16_t length; ///< Path length (0 at origin, 1 at first secondary hit, etc.).

		// Scatter ray
		float3 origin; ///< Origin of the scatter ray.
		float3 direction; ///< Scatter ray normalized direction.
		float hit_t; ///< Scatter ray hit_t.
		::framework::hit_info hit_info;
		::framework::sampled_wavelengths lambda;
		::framework::sampled_spectrum throughput;
		::framework::sampled_spectrum r_u;
		::framework::sampled_spectrum r_l;
		::framework::sampled_spectrum lighting; ///< Accumulated path contribution.
		::framework::uniform_sample_generator sample_generator;

		bool has_flag(::framework::path_flags flag)
		{
			return (this.flags & (uint16_t)flag) != 0;
		}

		void set_flag(::framework::path_flags flag, bool value)
		{
			if (value)
			{
				this.flags |= (uint16_t)flag;
			}
			else
			{
				this.flags &= ~(uint16_t)flag;
			}
		}

		bool is_terminated()
		{
			return !this.is_active();
		}

		bool is_active()
		{
			return this.has_flag(::framework::path_flags::active);
		}

		bool is_hit()
		{
			return this.has_flag(::framework::path_flags::hit);
		}

		bool is_free_path()
		{
			return this.has_flag(::framework::path_flags::free_path);
		}

		bool is_scattered()
		{
			return this.has_flag(::framework::path_flags::scattered);
		}

		void terminate()
		{
			this.set_flag(::framework::path_flags::active, false);
		}

		void set_active()
		{
			this.set_flag(::framework::path_flags::active, true);
		}

		void set_hit(::framework::hit_info hit_info)
		{
			this.hit_info = hit_info;
			this.set_flag(::framework::path_flags::hit, true);
		}

		void clear_hit()
		{
			this.set_flag(::framework::path_flags::hit, false);
		}

		void set_free_path(bool value)
		{
			this.set_flag(::framework::path_flags::free_path, value);
		}

		void set_scattered(bool value)
		{
			this.set_flag(::framework::path_flags::scattered, value);
		}

		uint2 get_pixel()
		{
			return uint2(id, id >> 12) & 0xfff;
		}

		::framework::ray get_scatter_ray()
		{
			::framework::ray ray;
			ray.origin = this.origin;
			ray.direction = this.direction;
			return ray;
		}

		uint get_sample_index()
		{
			return id >> 24;
		}
	};
}