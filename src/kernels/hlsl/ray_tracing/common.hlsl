#pragma once

#include "radiometry.hlsl"
#include "hit_info.hlsl"

namespace framework
{
	struct payload_t
	{
		[[vk::location(0)]] float hit_t;
		[[vk::location(1)]] ::framework::hit_info hit_info;
	};

	struct constant_buffer_t
	{
		float4x4 viewInverse;
		float4x4 projInverse;
		float ray_eps;
		uint seed;
		uint sample_count;
	};

	struct sample_t
	{
		float3 xv;
		float3 nv;
		float3 xs;
		float3 ns;
		float3 radiance;
	};

	struct path_reservoir_t
	{
		float M;
		float weight;
		float3 F; // cached integrand (always updated after a new path is chosen in RIS)
		// todo: remove
		float3 visible_point_throughput;
		float3 position;
		float3 normal;
		float ray_length;

		void init()
		{
			this.M = 0.f;
			this.weight = 0.f;
			this.F = 0.f;
		}

		void finalize_ris()
		{
			float p_hat = path_reservoir_t::to_scalar(this.F);
			if (p_hat == 0.f || this.M == 0.f)
			{
				this.weight = 0.f;
			}
			else
			{
				this.weight = this.weight / (p_hat * this.M);
			}
		}

		static float to_scalar(float3 color)
		{
			return luminance(color);
		}
	};

	//void update_reservoir(inout reservoir_t r, sample_t s, float w)
	//{
	//	//r.w += w;
	//	//++r.M;
	//	//if (rand(rand_state) < w / r.w)
	//	{
	//		//r.z = s;
	//		// todo: revert
	//		//r.z.xs = s.xs;
	//		//r.z.ns = s.ns;
	//		//r.z.radiance = s.radiance;
	//	}
	//}
	//
	//void merge_reservoir(inout reservoir_t r0, in reservoir_t r1, float p_hat)
	//{
	//	//uint M0 = r0.M;
	//	//update_reservoir(r0, r1.z, p_hat * r1.W * r1.M);
	//	//r0.M = M0 + r1.M;
	//}
}