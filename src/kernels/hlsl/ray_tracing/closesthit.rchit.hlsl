#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/common.hlsl"
#include "scene.hlsl"
#include "utils.hlsl"

struct attributes
{
	float2 barycentrics;
};

[[vk::shader_record_ext]]
cbuffer shader_record_buffer
{
	uint diffuse_texture;
	uint64_t indices_addr;
	uint64_t vertices_addr;
	//StructuredBuffer<uint> indices;
	//StructuredBuffer<vertex_device_layout> vertices1;
};

static const float3 albedo = float3(1, 1, 1);
static const float max_extinction = 100.0f;
static const float g_ior = 1.1;
static const uint max_interactions = 128;
static uint g_rand_state = 0;

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return float(word) / 4294967295.0f;
}

float get_extinction(float3 p)
{
	return max_extinction;
}

bool sample_interaction(inout float3 ray_origin, float3 ray_direction)
{
	RayDesc ray_desc;
	ray_desc.Origin = ray_origin;
	ray_desc.Direction = ray_direction;
	ray_desc.TMin = 0.0;

	float t = 0.0f;
	do {
		t -= log(1.0f - rand(g_rand_state)) / max_extinction;
		ray_desc.TMax = ray_desc.TMin + t;

		//ray_desc.Origin = ray_origin + ray_direction * t;

		// Instantiate ray query object.
		// Template parameter allows driver to generate a specialized
		// implementation.
		RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> ray_query;

		// Set up a trace. No work is done yet.
		ray_query.TraceRayInline(raytracing_acceleration_structure, 0, 0xff, ray_desc);

		// Proceed() below is where behind-the-scenes traversal happens,
		// including the heaviest of any driver inlined code.
		// In this simplest of scenarios, Proceed() only needs
		// to be called once rather than a loop:
		// Based on the template specialization above,
		// traversal completion is guaranteed.
		while (ray_query.Proceed());

		// Examine and act on the result of the traversal.
		// Was a hit committed?
		if (ray_query.CommittedStatus() == COMMITTED_TRIANGLE_HIT)
		{
			ray_origin += ray_direction * (ray_query.CommittedRayT());

			return false;
		}

	} while (get_extinction(ray_desc.Origin) < rand(g_rand_state) * max_extinction);

	//ray_origin = ray_desc.Origin;
	ray_origin += ray_direction * t;

	return true;
}

float SinSquaredThetaT(float VdotN, float eta)
{
	return eta * eta * (1 - VdotN * VdotN);
}

float Fresnel(float IORi, float IORo, float VdotN, float sinSquaredThetaT)
{
	// Check for total internal reflection
	if (sinSquaredThetaT > 1.0f) {
		return 1.0f;
	}

	float cosThetaT = sqrt(1.0f - sinSquaredThetaT);
	float R_perpendicular = (IORi * VdotN - IORo * cosThetaT) / (IORi * VdotN + IORo * cosThetaT);
	float R_parallel = (IORo * VdotN - IORi * cosThetaT) / (IORo * VdotN + IORi * cosThetaT);

	return 0.5f * (R_perpendicular * R_perpendicular + R_parallel * R_parallel);
}

float3 refract(const float3 V, const float3 N, float VdotN, float eta, float sinSquaredThetaT) {
	return (eta * VdotN - sqrt(1.0f - sinSquaredThetaT)) * N - eta * V;
}

//vertex_device_layout load_vertex_device_layout(uint addr)
//{
//	vertex_device_layout vertex;
//	vertex.position.x = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 0));
//	vertex.position.y = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 1));
//	vertex.position.z = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 2));
//	vertex.normal.x = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 3));
//	vertex.normal.y = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 4));
//	vertex.normal.z = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 5));
//	vertex.uv.x = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 6));
//	vertex.uv.y = asfloat(vk::RawBufferLoad(vertices + addr * 8 + 7));
//
//	return vertex;
//}

[shader("closesthit")]
void closesthit_rchit(inout ::framework::payload_t payload, in ::attributes attributes)
{
	::framework::scene scene;
	//scene.indices = indices;
	//scene.vertices = vertices;

	::framework::triangle_hit triangle_hit;
	triangle_hit.instance_id = ::framework::geometry_instance_id::create(InstanceID(), GeometryIndex());
	triangle_hit.primitive_index = PrimitiveIndex();
	triangle_hit.barycentrics = attributes.barycentrics;

	payload.hit_t = RayTCurrent();
	payload.hit_info = ::framework::hit_info::create(triangle_hit);

#if 0
	uint3 LaunchID = DispatchRaysIndex();
	uint3 LaunchSize = DispatchRaysDimensions();

	uint rand_state = LaunchSize.x * LaunchID.y + LaunchID.x + constant_buffer.seed;
	g_rand_state = rand_state;

	uint index0 = indices[PrimitiveIndex() * 3 + 0];
	uint index1 = indices[PrimitiveIndex() * 3 + 1];
	uint index2 = indices[PrimitiveIndex() * 3 + 2];

	float3 barycentric_coords = float3(1.0f - attributes.barycentrics.x - attributes.barycentrics.y, attributes.barycentrics.x, attributes.barycentrics.y);

	vertex_device_layout v0 = vertices[index0];
	vertex_device_layout v1 = vertices[index1];
	vertex_device_layout v2 = vertices[index2];

	float3 position = v0.position * barycentric_coords.x + v1.position * barycentric_coords.y + v2.position * barycentric_coords.z;
	float3 normal = v0.normal * barycentric_coords.x + v1.normal * barycentric_coords.y + v2.normal * barycentric_coords.z;
	float2 uv = v0.uv * barycentric_coords.x + v1.uv * barycentric_coords.y + v2.uv * barycentric_coords.z;
	uv.y = 1.0 - uv.y;

	p.distance = RayTCurrent();
#endif

#if 0
	float3 geometric_normal = normalize(cross(normalize(v1.position - v0.position), normalize(v2.position - v0.position)));
	bool hit_front_face = true;
	float VdotN = dot(-ObjectRayDirection(), geometric_normal);
	float IORo = g_ior;
	float IORi = 1.0;
	if (VdotN < 0.0f)
	{
		hit_front_face = false;
		IORo = 1.0f;
		IORi = g_ior;
		normal = -normal;
		geometric_normal = -geometric_normal;
		VdotN = -VdotN;
	}

	float eta = IORi / IORo;
	float sinSquaredThetaT = SinSquaredThetaT(VdotN, eta);
	float fresnel = Fresnel(IORi, IORo, VdotN, sinSquaredThetaT);

	//if (rand(g_rand_state) <= fresnel)
	if (true)
	{
		// Reflect
		p.direction = reflect(ObjectRayDirection(), normal);
		p.normal = normal;
		p.geometric_normal = geometric_normal;
		p.refracted = false;
		//interaction.SampledLobe = BSDFLobe::SpecularReflection;
		//interaction.IORo = IORi;
	}
	else
	{
		// Refract
		//p.direction = WorldRayDirection();
		p.direction = refract(-ObjectRayDirection(), normal, VdotN, eta, sinSquaredThetaT);
		p.refracted = hit_front_face;
		p.normal = -normal;
		//interaction.SampledLobe = BSDFLobe::SpecularTransmission;
		//interaction.IORo = IORo;
	}

	//p.refracted = hit_front_face;
	p.color = abs(geometric_normal);
	//p.normal = dot(WorldRayDirection(), geometric_normal) > 0.0f ? geometric_normal : -geometric_normal;
	//p.normal = geometric_normal;
#elif 0
	////float3 ray_origin = WorldRayOrigin() + WorldRayDirection() * RayTCurrent() + normal * 0.001 * sign(dot(WorldRayDirection(), normal));
	float3 ray_direction = WorldRayDirection();

	//float3 color1 = float3(197.0 / 255.0, 140.0 / 255.0, 133.0 / 255.0);
	//float3 throughput = float3(1, 1, 1);
	//unsigned int num_interactions = 0;
	//while (sample_interaction(ray_origin, ray_direction))
	//{
	//	throughput *= 0.9;

	//	// Sample isotropic phase function.
	//	const float phi = (float)(2.0 * M_PI) * rand(g_rand_state);
	//	const float cos_theta = 1.0f - 2.0f * rand(g_rand_state);
	//	const float sin_theta = sqrt(1.0f - cos_theta * cos_theta);
	//	ray_direction = float3(cos(phi) * sin_theta, sin(phi) * sin_theta, cos_theta);
	//	//ray_direction = normalize(-normal + float3(r * cos(theta), r * sin(theta), u));

	//	if (num_interactions++ >= max_interactions)
	//	{
	//		//throughput.r = 0.0;
	//		break;
	//	}
	//}

	float3 color = textures[diffuse_texture].SampleLevel(linear_sampler, uv, 0).rgb;
	color = float3(197.0 / 255.0, 140.0 / 255.0, 133.0 / 255.0);

	const float inv_pi = 0.31830988618;

	const float theta = 6.2831853 * rand(g_rand_state);  // Random in [0, 2pi]
	const float u = 2.0 * rand(g_rand_state) - 1.0;  // Random in [-1, 1]
	const float r = sqrt(1.0 - u * u);

	p.normal = normal;
	//p.color = color * inv_pi;
	p.color = color;
	p.direction = normalize(normal + float3(r * cos(theta), r * sin(theta), u));
	//p.distance = throughput.r;
	p.refracted = false;
#endif
}
