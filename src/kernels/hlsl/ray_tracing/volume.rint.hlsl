#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/common.hlsl"
#include "utils.hlsl"

//struct vertex_device_layout
//{
//	float3 position;
//	float3 normal;
//	float2 uv;
//};
//
//struct Payload
//{
//	[[vk::location(0)]] float3 color;
//	[[vk::location(1)]] float3 normal;
//	[[vk::location(2)]] float distance;
//};
//
//struct CameraProperties
//{
//	float4x4 viewInverse;
//	float4x4 projInverse;
//	float ray_eps;
//	uint seed;
//	uint sample_count;
//};
//
//cbuffer cam : register(b0, space1)
//{
//	CameraProperties cam;
//};
//
//SamplerState linear_sampler : register(s0, space0);
//
//Texture2D<float4> textures[] : register(t1, space0);
//
//cbuffer material : register(b0, space2)
//{
//	uint diffuse_texture;
//};
//
//StructuredBuffer<uint> indices : register(t, space2);
//
//StructuredBuffer<vertex_device_layout> vertices : register(t1, space2);
//
//struct test_attr
//{
//	float test;
//};
//
//static const float max_extinction = 70.0f;
//static const float M_PI = 3.14159265f;
//
//// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
//float rand(inout uint rand_state)
//{
//	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
//	rand_state = rand_state * 747796405 + 1;
//	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
//	word = (word >> 22) ^ word;
//	return float(word) / 4294967295.0f;
//}
//
//bool intersect_volume_box(inout float tmin, float3 raypos, float3 raydir)
//{
//	const float x0 = (-0.5 - raypos.x) / raydir.x;
//	const float y0 = (-0.5 - raypos.y) / raydir.y;
//	const float z0 = (-0.5 - raypos.z) / raydir.z;
//	const float x1 = (0.5 - raypos.x) / raydir.x;
//	const float y1 = (0.5 - raypos.y) / raydir.y;
//	const float z1 = (0.5 - raypos.z) / raydir.z;
//
//	tmin = max(max(max(min(z0, z1), min(y0, y1)), min(x0, x1)), 0.0f);
//	const float tmax = min(min(max(z0, z1), max(y0, y1)), max(x0, x1));
//	return (tmin < tmax);
//}
//
//bool in_volume(float3 pos)
//{
//	return max(abs(pos.x), max(abs(pos.y), abs(pos.z))) < 0.5;
//}
//
//float get_extinction(float3 p)
//{
//	if (false)
//	{
//		float3 pos = p + float3(0.5, 0.5, 0.5);
//		const unsigned int steps = 3;
//		for (unsigned int i = 0; i < steps; ++i) {
//			pos *= 3.0f;
//			const int s =
//				((int)pos.x & 1) + ((int)pos.y & 1) + ((int)pos.z & 1);
//			if (s >= 2)
//				return 0.0f;
//		}
//		return max_extinction;
//	}
//	else
//	{
//		const float r = 0.5 * (0.5 - abs(p.y));
//		const float a = (float)(M_PI * 8.0) * p.y;
//		const float dx = (cos(a) * r - p.x) * 2.0f;
//		const float dy = (sin(a) * r - p.z) * 2.0f;
//		return pow(max((1.0f - dx * dx - dy * dy), 0.0f), 8.0f) * max_extinction;
//	}
//}

[shader("intersection")]
void volume_rint()
{
	//uint3 LaunchID = DispatchRaysIndex();
	//uint3 LaunchSize = DispatchRaysDimensions();
	//
	//uint rand_state = LaunchSize.x * LaunchID.y + LaunchID.x + cam.seed;
	//
	//float3 ray_origin = WorldRayOrigin();
	//float3 ray_direction = WorldRayDirection();
	//
	//float t;
	//if (intersect_volume_box(t, ray_origin, ray_direction))
	//{
	//	float3 pos;
	//	do {
	//		t -= log(1.0f - rand(rand_state)) / max_extinction;
	//
	//		pos = ray_origin + ray_direction * t;
	//		if (!in_volume(pos))
	//			return;
	//
	//	} while (get_extinction(pos) < rand(rand_state) * max_extinction);
	//
	//	test_attr attr = (test_attr)0;
	//
	//	ReportHit(t, 0, attr);
	//}
}