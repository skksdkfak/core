#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/common.hlsl"
#include "utils.hlsl"

struct attributes
{
    float2 barycentrics;
};
//struct Attribute
//{
//  float2 attribs;
//};
//
//struct vertex_device_layout
//{
//    float3 position;
//    float3 normal;
//    float2 uv;
//};
//
//struct Payload
//{
//    [[vk::location(0)]] float3 color;
//    [[vk::location(1)]] float3 direction;
//    [[vk::location(2)]] float distance;
//    [[vk::location(3)]] float3 normal;
//    [[vk::location(4)]] bool refracted;
//};
//
//struct CameraProperties
//{
//    float4x4 viewInverse;
//    float4x4 projInverse;
//    float ray_eps;
//    uint seed;
//    uint sample_count;
//};
//
//cbuffer cam : register(b0, space1)
//{
//    CameraProperties cam;
//};
//
//SamplerState linear_sampler : register(s0, space0);
//
//Texture2D<float4> textures[] : register(t1, space0);
//
//cbuffer material : register(b0, space2)
//{
//    uint diffuse_texture;
//};
//
//StructuredBuffer<uint> indices : register(t0, space2);
//
//StructuredBuffer<vertex_device_layout> vertices : register(t1, space2);
//
//uint xxhash32(in uint p)
//{
//    const uint prime32_2 = 2246822519u, prime32_3 = 3266489917u;
//    const uint prime32_4 = 668265263u, prime32_5 = 374761393u;
//
//    uint h32 = p + prime32_5;
//    h32 = prime32_4 * ((h32 << 17) | (h32 >> (32 - 17)));
//    h32 = prime32_2 * (h32 ^ (h32 >> 15));
//    h32 = prime32_3 * (h32 ^ (h32 >> 13));
//
//    return h32 ^ (h32 >> 16);
//}
//
//float3 hash31(uint x)
//{
//    uint n = xxhash32(x);
//    uint3 rz = uint3(n, n * 16807U, n * 48271U); //see: http://random.mat.sbg.ac.at/results/karl/server/node4.html
//    return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU)) / float(0x7fffffff);
//}
//
//static const float3 albedo = float3(1, 1, 1);
//static const float max_extinction = 1000.0f;
//static const uint max_interactions = 128;
//static const float M_PI = 3.14159265f;
//static uint g_rand_state = 0;
//
//// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
//float rand(inout uint rand_state)
//{
//    // Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
//    rand_state = rand_state * 747796405 + 1;
//    uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
//    word = (word >> 22) ^ word;
//    return float(word) / 4294967295.0f;
//}
//
//bool intersect_volume_box(inout float tmin, float3 raypos, float3 raydir)
//{
//    const float x0 = (-100.0f - raypos.x) / raydir.x;
//    const float y0 = (-100.0f - raypos.y) / raydir.y;
//    const float z0 = (-100.0f - raypos.z) / raydir.z;
//    const float x1 = (100.0f - raypos.x) / raydir.x;
//    const float y1 = (100.0f - raypos.y) / raydir.y;
//    const float z1 = (100.0f - raypos.z) / raydir.z;
//
//    tmin = max(max(max(min(z0, z1), min(y0, y1)), min(x0, x1)), 0.0f);
//    const float tmax = min(min(max(z0, z1), max(y0, y1)), max(x0, x1));
//    return (tmin < tmax);
//}
//
//bool in_volume(float3 pos)
//{
//    return max(abs(pos.x), max(abs(pos.y), abs(pos.z))) < 100.0f;
//}
//
//float get_extinction(float3 p)
//{
//    if (true)
//    {
//        float3 pos = p + float3(100.0f, 100.0f, 100.0f);
//        const unsigned int steps = 3;
//        for (unsigned int i = 0; i < steps; ++i) {
//            pos *= 3.0f;
//            const int s =
//                ((int)pos.x & 1) + ((int)pos.y & 1) + ((int)pos.z & 1);
//            if (s >= 2)
//                return 0.0f;
//        }
//        return max_extinction;
//    }
//    else
//    {
//        const float r = 100.0f * (100.0f - abs(p.y));
//        const float a = (float)(M_PI * 8.0) * p.y;
//        const float dx = (cos(a) * r - p.x) * 2.0f;
//        const float dy = (sin(a) * r - p.z) * 2.0f;
//        return pow(max((1.0f - dx * dx - dy * dy), 0.0f), 8.0f) * max_extinction;
//    }
//}
//
//bool sample_interaction(inout float3 ray_pos, float3 ray_dir)
//{
//    float t = 0.0f;
//    float3 pos;
//    do {
//        t -= log(1.0f - rand(g_rand_state)) / max_extinction;
//
//        pos = ray_pos + ray_dir * t;
//        if (!in_volume(pos))
//            return false;
//
//    } while (get_extinction(pos) < rand(g_rand_state) * max_extinction);
//
//    ray_pos = pos;
//    return true;
//}
//
//float3 trace_volume(inout float3 ray_pos, inout float3 ray_dir)
//{
//    float t0;
//    float w = 1.0f;
//    if (intersect_volume_box(t0, ray_pos, ray_dir)) {
//
//        ray_pos += ray_dir * t0;
//
//        unsigned int num_interactions = 0;
//        while (sample_interaction(ray_pos, ray_dir))
//        {
//            // Is the path length exeeded?
//            if (num_interactions++ >= max_interactions)
//                return float3(0.0f, 0.0f, 0.0f);
//
//            w *= albedo;
//            // Russian roulette absorption
//            if (w < 0.2f) {
//                if (rand(g_rand_state) > w * 5.0f) {
//                    return float3(0.0f, 0.0f, 0.0f);
//                }
//                w = 0.2f;
//            }
//
//            // Sample isotropic phase function.
//            const float phi = (float)(2.0 * M_PI) * rand(g_rand_state);
//            const float cos_theta = 1.0f - 2.0f * rand(g_rand_state);
//            const float sin_theta = sqrt(1.0f - cos_theta * cos_theta);
//            ray_dir = float3(
//                cos(phi) * sin_theta,
//                sin(phi) * sin_theta,
//                cos_theta);
//        }
//    }
//
//    // Lookup environment.
//    const float f = (100.0f + 100.0f * ray_dir.y) * w;
//    return float3(f, f, f);
//}

[shader("closesthit")]
void volume_rchit(inout ::framework::payload_t payload, in ::attributes attributes)
{
    //uint3 LaunchID = DispatchRaysIndex();
    //uint3 LaunchSize = DispatchRaysDimensions();
    //
    //uint rand_state = LaunchSize.x * LaunchID.y + LaunchID.x + cam.seed;
    //float3 ray_pos = WorldRayOrigin();
    //float3 ray_dir = WorldRayDirection();
    //
    //// Sample isotropic phase function.
    //const float phi = (float)(2.0 * M_PI) * rand(rand_state);
    //const float cos_theta = 1.0f - 2.0f * rand(rand_state);
    //const float sin_theta = sqrt(1.0f - cos_theta * cos_theta);
    //
    ////p.color = trace_volume(ray_pos, ray_dir);
    //p.color = float3(0.1, 0.75, 1);
    //p.direction = float3(cos(phi) * sin_theta, sin(phi) * sin_theta, cos_theta);
    //p.distance = RayTCurrent();
    //p.normal = p.direction;
    //p.refracted = false;
}
