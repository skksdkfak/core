#include "ray_tracing/descriptor_set_0.hlsl"
#include "ray_tracing/descriptor_set_1.hlsl"
#include "ray_tracing/path_tracer.hlsl"
#include "ray_tracing/path_state.hlsl"
#include "ray_tracing/common.hlsl"
#include "radiometry.hlsl"
#include "bxdf.hlsl"
#include "shading.hlsl"
#include "material_data.hlsl"
#include "phase_function.hlsl"
#include "scene.hlsl"
#include "utils.hlsl"
#include "numbers.hlsl"

[shader("raygeneration")]
void raygen()
{
	::framework::scene scene;
	//scene.indices = indices;
	//scene.vertices = vertices;

	const uint3 pixel_index = DispatchRaysIndex();

	for (uint j = 0; j < samples_per_pixel; j++)
	{
		::framework::path_state path_state;
		::framework::path_tracer path_tracer = ::framework::path_tracer::create(scene);

		uint path_id = pixel_index.x | (pixel_index.y << 12) | (constant_buffer.seed << 24);
		path_tracer.generate_path(path_id, path_state);

		while (path_state.is_active())
		{
			path_tracer.next_vertex(path_state);

			if (path_state.is_free_path())
			{
				path_tracer.handle_free_path(path_state);

				if (path_state.is_scattered())
				{
					continue;
				}
			}

			if (path_state.is_hit())
			{
				path_tracer.handle_hit(path_state);
			}
			else
			{
				path_tracer.handle_miss(path_state);
			}
		};

		path_tracer.write_output(path_state);
	}
}
