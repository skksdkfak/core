#pragma once

#include "ray_tracing/common.hlsl"

static const uint samples_per_pixel = 1;

[[vk::binding(0, 1)]] RWTexture2D<float4> image : register(u0, space1);

[[vk::binding(1, 1)]] ConstantBuffer<::framework::constant_buffer_t> constant_buffer : register(b0, space0);