#pragma once

namespace framework
{
    static const uint c1 = 0xcc9e2d51u;
    static const uint c2 = 0x1b873593u;

	uint xxhash32(in uint p)
	{
		const uint prime32_2 = 2246822519u, prime32_3 = 3266489917u;
		const uint prime32_4 = 668265263u, prime32_5 = 374761393u;

		uint h32 = p + prime32_5;
		h32 = prime32_4 * ((h32 << 17) | (h32 >> (32 - 17)));
		h32 = prime32_2 * (h32 ^ (h32 >> 15));
		h32 = prime32_3 * (h32 ^ (h32 >> 13));

		return h32 ^ (h32 >> 16);
	}

	float3 hash31(uint x)
	{
		uint n = xxhash32(x);
		uint3 rz = uint3(n, n * 16807U, n * 48271U);
		return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU)) / float(0x7fffffff);
	}

    uint rotl(uint x, uint r)
    {
	    return (x << r) | (x >> (32u - r));
    }

    uint rotr(uint x, uint r)
    {
	    return (x >> r) | (x << (32u - r));
    }

    uint fmix(uint h)
    {
        h ^= h >> 16;
        h *= 0x85ebca6bu;
        h ^= h >> 13;
        h *= 0xc2b2ae35u;
        h ^= h >> 16;
        return h;
    }

    template<uint len>
    uint32_t murmur_hash_2(uint32_t key[len], uint32_t seed)
    {
        const uint32_t m = 0x5bd1e995;
        const int r = 24;

        uint32_t h = seed ^ (len * 4);

        [unroll]
        for (uint i = 0; i < len; i++)
        {
            uint32_t k = key[i];
            
            k *= m;
            k ^= k >> r;
            k *= m;

            h *= m;
            h ^= k;
        }

        h ^= h >> 13;
        h *= m;
        h ^= h >> 15;

        return h;
    }
    
    uint32_t murmur_hash_2(uint a, float3 b)
    {
        uint32_t key[4] = { a, asuint(b.x), asuint(b.y), asuint(b.z) };
        return ::framework::murmur_hash_2(key, 0);
    }
    
    uint32_t murmur_hash_2(float3 b)
    {
        uint32_t key[3] = { asuint(b.x), asuint(b.y), asuint(b.z) };
        return ::framework::murmur_hash_2(key, 0);
    }

	uint murmur3(uint3 seed)
    {
        uint h = 0u;
        uint k = seed.x;

        k *= c1;
        k = rotl(k,15u);
        k *= c2;

        h ^= k;
        h = rotl(h,13u);
        h = h*5u+0xe6546b64u;

        k = seed.y;

        k *= c1;
        k = rotl(k,15u);
        k *= c2;

        h ^= k;
        h = rotl(h,13u);
        h = h*5u+0xe6546b64u;

        k = seed.z;

        k *= c1;
        k = rotl(k,15u);
        k *= c2;

        h ^= k;
        h = rotl(h,13u);
        h = h*5u+0xe6546b64u;

        h ^= 12u;

        return fmix(h);
    }
    
	uint murmur3(float3 seed)
    {
        return ::framework::murmur3(asuint(seed));
    }

    inline uint64_t mix_bits(uint64_t v)
    {
        v ^= (v >> 31);
        v *= 0x7fb5d329728ea185;
        v ^= (v >> 27);
        v *= 0x81dadef4bc2dd44d;
        v ^= (v >> 33);
        return v;
    }
}