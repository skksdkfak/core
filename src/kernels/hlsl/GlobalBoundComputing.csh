#include "globals.hlsl"
#include "PathTracing.hlsl"

#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM 64

struct CompactTriangle
{
	float4 field0; // vert0.xyz, t0.x
	float4 field1; // vert1.xyz, t1.y
	float4 field2; // vert2.xyz, materialID
	float4 field3; // t1.xy, t2.xy
	float3x4 tbn;
};

StructuredBuffer<CompactTriangle>	triangles	: register(t0);
RWStructuredBuffer<Bound>			bounds		: register(u0);

cbuffer CustomUB : register(b0)
{
	uint numElements;
};

groupshared Bound localBounds[WG_SIZE];

[numthreads(WG_SIZE, 1, 1)]
void main(uint3 dispatchThreadID : SV_DispatchThreadID, uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	Bound bound;
	bound.min = FLT_MAX;
	bound.pad0 = 0;
	bound.max = FLT_MIN;
	bound.pad1 = 0;

	localBounds[groupThreadID.x] = bound;
	
	GroupMemoryBarrierWithGroupSync();

	for (uint i = groupThreadID.x * ELEMENTS_PER_WORK_ITEM + groupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM; i < numElements, i < groupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM + groupThreadID.x * ELEMENTS_PER_WORK_ITEM + ELEMENTS_PER_WORK_ITEM; i++)
	{
		float3 vertices[3];
		vertices[0] = triangles[i].field0.xyz;
		vertices[1] = triangles[i].field1.xyz;
		vertices[2] = triangles[i].field2.xyz;
		[unroll]
		for (uint vIdx = 0; vIdx < 3; vIdx++)
		{
			bound.min = min(bound.min, vertices[vIdx]);
			bound.max = max(bound.max, vertices[vIdx]);
		}
	}

	localBounds[groupThreadID.x] = bound;

	GroupMemoryBarrierWithGroupSync();

	if (groupThreadID.x == 0)
	{
		bound = localBounds[0];
		for (uint i = 1; i < WG_SIZE; i++)
		{
			merge_bounds(localBounds[i], bound, bound);
		}
		bounds[groupID.x] = bound;
	}
}