#pragma once

#include "numbers.hlsl"
#include "utils.hlsl"

namespace framework
{
	/** Evaluates the anisotropic Henyey-Greenstein phase function.
		Note: This function reduces to isotropic phase function at g = 0 and has singularities at g = -1 and g = 1.
		\param[in] cos_theta Cosine between unscattered and scattered direction.
		\param[in] g Anisotropy parameter in (-1, 1), where positive values promote forward scattering.
	*/
	float eval_henyey_greenstein(const float cos_theta, const float g)
	{
		const float denom = 1 + g * g + 2 * g * cos_theta;
		return ::framework::numbers::inv_4_pi * (1 - g * g) / (denom * sqrt(denom));
	}

	/** Isotropic phase function.
		f_p(wo, wi) = 1 / 4PI
	*/
	struct isotropic_phasefunction
	{
		float eval(const float3 wo, const float3 wi)
		{
			return ::framework::numbers::inv_4_pi;
		}

		template<typename sample_generator_t>
		bool sample(const float3 wo, out float3 wi, out float pdf, out float weight, inout sample_generator_t sample_generator)
		{
			wi = ::framework::sample_sphere(::framework::sample_next_2d(sample_generator));
			pdf = ::framework::numbers::inv_4_pi;
			weight = 1.f;
			return true;
		}

		float eval_pdf(const float3 wo, const float3 wi)
		{
			return ::framework::numbers::inv_4_pi;
		}
	};

	/** Henyey-Greenstein phase function.
		Note: This function reduces to isotropic phase function at g = 0 and has singularities at g = -1 and g = 1.
	*/
	struct henyey_greenstein_phase_function
	{
		float g;

		static ::framework::henyey_greenstein_phase_function create(float g)
		{
			::framework::henyey_greenstein_phase_function henyey_greenstein_phase_function;
			henyey_greenstein_phase_function.g = g;
			return henyey_greenstein_phase_function;
		}

		float eval(const float3 wo, const float3 wi)
		{
			return ::framework::eval_henyey_greenstein(dot(-wo, wi), this.g);
		}

		template<typename sample_generator_t>
		bool sample(const float3 wo, out float3 wi, out float pdf, out float weight, inout sample_generator_t sample_generator)
		{
			const float2 u = ::framework::sample_next_2d(sample_generator);

			float cos_theta;
			if (abs(this.g) < 1e-3f)
			{
				cos_theta = 1.f - 2.f * u.x;
			}
			else
			{
				const float sqr = (1.f - this.g * this.g) / (1.f - this.g + 2.f * this.g * u.x);
				cos_theta = (1.f + this.g * this.g - sqr * sqr) / (2.f * this.g);
			}

			const float sin_theta = sqrt(max(0.f, 1.f - cos_theta * cos_theta));
			const float phi = ::framework::numbers::pi2 * u.y;
			const float3 w = float3(sin_theta * cos(phi), sin_theta * sin(phi), cos_theta);

			const float3 dir = -wo;
			const float3 T = ::framework::perp_stark(dir);
			const float3 B = cross(dir, T);

			wi = normalize(w.x * T + w.y * B + w.z * dir);
			pdf = ::framework::eval_henyey_greenstein(cos_theta, this.g);
			weight = 1.f;
			return true;
		}

		float eval_pdf(const float3 wo, const float3 wi)
		{
			return ::framework::eval_henyey_greenstein(dot(-wo, wi), this.g);
		}
	};
}
