#pragma once

namespace framework
{
	struct material_data
	{
		float3 baseColor;
		float metalness;

		float3 emissive;

		// Roughnesses
		float roughness;    //< perceptively linear roughness (artist's input)
		float alpha;        //< linear roughness - often 'alpha' in specular BRDF equations
		float alphaSquared; //< alpha squared - pre-calculated value commonly used in BRDF equations

		float diffuse_transmission; ///< Diffuse transmission.
		float specular_transmission; ///< Specular transmission.
		float transmissivness;
		float opacity;
		float ior;
	};
}