#pragma once

#include "numbers.hlsl"

namespace framework
{
	float2 cartesian_to_spherical(float3 cartesian_coords)
	{
		float phi = atan2(cartesian_coords.z, cartesian_coords.x);
		phi = phi < 0.0 ? (phi + 2 * ::framework::numbers::pi) : phi;
		float theta = acos(cartesian_coords.y);

		return float2(phi, theta);
	}

	float3 spherical_direction(float sin_theta, float cos_theta, float phi)
	{
		return float3(sin_theta * cos(phi), sin_theta * sin(phi), cos_theta);
	}

	float3 spherical_direction(float sin_theta, float cos_theta, float phi, const float3 x, const float3 y, const float3 z)
	{
		return sin_theta * cos(phi) * x + sin_theta * sin(phi) * y + cos_theta * z;
	}

	/** Generate a vector that is orthogonal to the input vector.
		This can be used to invent a tangent frame for meshes that don't have real tangents/bitangents.
		\param[in] u Unit vector.
		\return v Unit vector that is orthogonal to u.
	*/
	float3 perp_stark(float3 u)
	{
		float3 a = abs(u);
		uint uyx = (a.x - a.y) < 0 ? 1 : 0;
		uint uzx = (a.x - a.z) < 0 ? 1 : 0;
		uint uzy = (a.y - a.z) < 0 ? 1 : 0;
		uint xm = uyx & uzx;
		uint ym = (1 ^ xm) & uzy;
		uint zm = 1 ^ (xm | ym);  // 1 ^ (xm & ym)
		float3 v = normalize(cross(u, float3(xm, ym, zm)));
		return v;
	}

	/** Uniform sampling of the unit sphere using spherical coordinates.
		\param[in] u Uniform random numbers in [0,1)^2.
		\return Sampled point on the unit sphere.
	*/
	float3 sample_sphere(float2 u)
	{
		float phi = ::framework::numbers::pi2 * u.y;
		float cosTheta = 1.0f - 2.0f * u.x;
		float sinTheta = sqrt(max(0.0f, 1.0f - cosTheta * cosTheta));
		return float3(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
	}

	template <typename T>
	float copysign(const T x, const T y)
	{
		return abs(x) * sign(y);
	}
}

// http://jcgt.org/published/0003/02/01/paper.pdf
float2 octahedron_encode(float3 v)
{
	// Project the sphere onto the octahedron, and then onto the xy plane
	float2 p = v.xy * (1.0 / (abs(v.x) + abs(v.y) + abs(v.z)));
	// Reflect the folds of the lower hemisphere over the diagonals
	return (v.z <= 0.0) ? ((1.0 - abs(p.yx)) * sign(p)) : p;
}

// http://jcgt.org/published/0003/02/01/paper.pdf
float3 octahedron_decode(float2 e)
{
	float3 v = float3(e.xy, 1.0 - abs(e.x) - abs(e.y));
	if (v.z < 0) v.xy = (1.0 - abs(v.yx)) * sign(v.xy);
	return normalize(v);
}

// From http://filmicgames.com/archives/75
float3 uncharted_to_tonemap(float3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
}

void ortho_basis(out float3 v_x, out float3 v_y, const float3 n)
{
	v_y = float3(0, 0, 0);

	if (n.x < 0.6f && n.x > -0.6f)
	{
		v_y.x = 1.f;
	}
	else if (n.y < 0.6f && n.y > -0.6f)
	{
		v_y.y = 1.f;
	}
	else if (n.z < 0.6f && n.z > -0.6f)
	{
		v_y.z = 1.f;
	}
	else
	{
		v_y.x = 1.f;
	}
	v_x = normalize(cross(v_y, n));
	v_y = normalize(cross(n, v_x));
}

uint xxhash32(in uint p)
{
	const uint prime32_2 = 2246822519u, prime32_3 = 3266489917u;
	const uint prime32_4 = 668265263u, prime32_5 = 374761393u;

	uint h32 = p + prime32_5;
	h32 = prime32_4 * ((h32 << 17) | (h32 >> (32 - 17)));
	h32 = prime32_2 * (h32 ^ (h32 >> 15));
	h32 = prime32_3 * (h32 ^ (h32 >> 13));

	return h32 ^ (h32 >> 16);
}

float3 hash31(uint x)
{
	uint n = xxhash32(x);
	uint3 rz = uint3(n, n * 16807U, n * 48271U); //see: http://random.mat.sbg.ac.at/results/karl/server/node4.html
	return float3((rz >> 1) & uint3(0x7fffffffU, 0x7fffffffU, 0x7fffffffU)) / float(0x7fffffff);
}