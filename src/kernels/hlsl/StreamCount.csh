#include "globals.hlsl"



#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x
#define GROUP_LDS_BARRIER GroupMemoryBarrierWithGroupSync()
#define GROUP_MEM_FENCE GroupMemoryBarrier()
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define AtomInc(x) InterlockedAdd(x, 1)
#define AtomInc1(x, out) InterlockedAdd(x, 1, out)
#define AtomAdd(x, inc) InterlockedAdd(x, inc)

uint4 SELECT_UINT4(uint4 b, uint4 a, uint4 condition) { return  uint4(((condition).x) ? a.x : b.x, ((condition).y) ? a.y : b.y, ((condition).z) ? a.z : b.z, ((condition).w) ? a.w : b.w); }


#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define CHECK_BOUNDARY 1

//#define NV_GPU 1

//	Cypress
#define nPerWI 16
//	Cayman
//#define nPerWI 20

#define GET_GROUP_SIZE WG_SIZE

cbuffer SortCB : register(b0)
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

cbuffer PushConstants : register(b1)
{
	uint m_startBit;
	uint m_currentBitset;
};

#define KEY_VALUE_SEPARATELY

#if defined(KEY_VALUE_TWIN)
typedef struct
{
	uint m_key;
	uint m_value;
}SortData;

StructuredBuffer<SortData> gSrc : register(t0);
#elif defined(KEY_VALUE_SEPARATELY)
StructuredBuffer<uint> gSrc : register(t0);
#endif

RWStructuredBuffer<uint> histogramOut : register(u0);

groupshared uint localHistogramMat[NUM_BUCKET*WG_SIZE];

#define MY_HISTOGRAM(idx) localHistogramMat[(idx)*WG_SIZE+lIdx]

[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	uint gIdx = GET_GLOBAL_IDX;
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	uint wgSize = GET_GROUP_SIZE;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;

	for (uint i = 0; i<NUM_BUCKET; i++)
	{
		MY_HISTOGRAM(i) = 0;
	}

	GROUP_LDS_BARRIER;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM*WG_SIZE;
	uint localKey;

	uint nBlocks = (nAligned) / blockSize - nBlocksPerWG*wgIdx;

	uint addr = blockSize*nBlocksPerWG*wgIdx + ELEMENTS_PER_WORK_ITEM*lIdx;

	for (uint iblock = 0; iblock<min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		//	MY_HISTOGRAM( localKeys.x ) ++ is much expensive than atomic add as it requires read and write while atomics can just add on AMD
		//	Using registers didn't perform well. It seems like use localKeys to address requires a lot of alu ops
		//	AMD: AtomInc performs better while NV prefers ++
		for (uint i = 0; i<ELEMENTS_PER_WORK_ITEM; i++)
		{
#if defined(CHECK_BOUNDARY)
			if (addr + i < n)
#endif
			{
#if defined(KEY_VALUE_TWIN)
				localKey = (gSrc[addr + i].m_key >> startBit) & 0xf;
#elif defined(KEY_VALUE_SEPARATELY)
				localKey = (gSrc[addr + i] >> startBit) & 0xf;
#endif
#if defined(NV_GPU)
				MY_HISTOGRAM(localKey)++;
#else
				AtomInc(MY_HISTOGRAM(localKey));
#endif
			}
		}
	}
	GROUP_LDS_BARRIER;

	if (lIdx < NUM_BUCKET)
	{
		uint sum = 0;
		for (uint i = 0; i<GET_GROUP_SIZE; i++)
		{
			sum += localHistogramMat[lIdx*WG_SIZE + (i + lIdx) % GET_GROUP_SIZE];
		}
		histogramOut[lIdx*nWGs + wgIdx] = sum;
	}
}