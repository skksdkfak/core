struct vs_input
{
	float4 positions : LOCATION0;
	uint band_offset : LOCATION1;
	float depth : LOCATION2;
};

struct vs_output
{
	float4 position : SV_POSITION;
	float2 tex_coord : TEXCOORD0;
	nointerpolation uint band_offset : BAND_OFFSET;
};

[shader("vertex")]
vs_output main(vs_input input, uint vertex_id : SV_VertexID)
{
	vs_output output;
	
	const uint u = vertex_id & 1;
	const uint v = (vertex_id >> 1) & 1;

	output.position = float4(input.positions[u], input.positions[v + 2], input.depth, 1.0);
	output.tex_coord = float2(u, v);
	output.band_offset = input.band_offset;

	return output;
}
