#include "globals.hlsl"



typedef uint u32;

#define GET_GROUP_IDX groupIdx.x
#define GET_LOCAL_IDX localIdx.x
#define GET_GLOBAL_IDX globalIdx.x
#define GROUP_LDS_BARRIER GroupMemoryBarrierWithGroupSync()
#define GROUP_MEM_FENCE GroupMemoryBarrier()
#define DEFAULT_ARGS uint3 globalIdx : SV_DispatchThreadID, uint3 localIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID
#define AtomInc(x) InterlockedAdd(x, 1)
#define AtomInc1(x, out) InterlockedAdd(x, 1, out)
#define AtomAdd(x, inc) InterlockedAdd(x, inc)

#define make_uint4 uint4
#define make_uint2 uint2

uint4 SELECT_UINT4(uint4 b, uint4 a, uint4 condition) { return  make_uint4(((condition).x) ? a.x : b.x, ((condition).y) ? a.y : b.y, ((condition).z) ? a.z : b.z, ((condition).w) ? a.w : b.w); }

#define NUM_BITS_TUPLES 1
#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define CHECK_BOUNDARY 1

//#define NV_GPU 1

//	Cypress
#define nPerWI 16
//	Cayman
//#define nPerWI 20

#define GET_GROUP_SIZE WG_SIZE

cbuffer SortCB : register(b0)
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

cbuffer PushConstants : register(b1)
{
	uint m_startBit;
	uint m_currentBitset;
};

#define KEY_VALUE_SEPARATELY

#if defined(KEY_VALUE_TWIN)
typedef struct
{
	uint m_key;
	uint m_value;
}SortData;

StructuredBuffer<SortData> gSrc : register(t0);
StructuredBuffer<uint> rHistogram : register(t1);
#elif defined(KEY_VALUE_SEPARATELY)
StructuredBuffer<uint> rHistogram : register(t0);
StructuredBuffer<uint> gSrcKey[NUM_BITS_TUPLES] : register(t1);
StructuredBuffer<uint> gSrcVal[NUM_BITS_TUPLES] : register(t[1 + NUM_BITS_TUPLES]);
#endif


groupshared uint ldsSortData[NUM_BITS_TUPLES][WG_SIZE*ELEMENTS_PER_WORK_ITEM + 16];

u32 localPrefixSum(u32 pData, uint currentBitset, uint lIdx, inout uint totalSum, int wgSize /*64 or 128*/)
{
	{	//	Set data
		ldsSortData[currentBitset][lIdx] = 0;
		ldsSortData[currentBitset][lIdx + wgSize] = pData;
	}

	GROUP_LDS_BARRIER;

	{	//	Prefix sum
		int idx = 2 * lIdx + (wgSize + 1);
#if defined(USE_2LEVEL_REDUCE)
		if (lIdx < 64)
		{
			u32 u0, u1, u2;
			u0 = ldsSortData[currentBitset][idx - 3];
			u1 = ldsSortData[currentBitset][idx - 2];
			u2 = ldsSortData[currentBitset][idx - 1];
			AtomAdd(ldsSortData[currentBitset][idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[currentBitset][idx - 12];
			u1 = ldsSortData[currentBitset][idx - 8];
			u2 = ldsSortData[currentBitset][idx - 4];
			AtomAdd(ldsSortData[currentBitset][idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[currentBitset][idx - 48];
			u1 = ldsSortData[currentBitset][idx - 32];
			u2 = ldsSortData[currentBitset][idx - 16];
			AtomAdd(ldsSortData[currentBitset][idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[currentBitset][idx - 1] += ldsSortData[currentBitset][idx - 2];
			GROUP_MEM_FENCE;
		}
#else
		if (lIdx < 64)
		{
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 1];
			GROUP_MEM_FENCE;
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 2];
			GROUP_MEM_FENCE;
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 4];
			GROUP_MEM_FENCE;
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 8];
			GROUP_MEM_FENCE;
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 16];
			GROUP_MEM_FENCE;
			ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 32];
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[currentBitset][idx] += ldsSortData[currentBitset][idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[currentBitset][idx - 1] += ldsSortData[currentBitset][idx - 2];
			GROUP_MEM_FENCE;
		}
#endif
	}

	GROUP_LDS_BARRIER;

	totalSum = ldsSortData[currentBitset][wgSize * 2 - 1];
	u32 addValue = ldsSortData[currentBitset][lIdx + wgSize - 1];
	return addValue;
}

u32 unpack4Key(u32 key, int keyIdx) { return (key >> (keyIdx * 8)) & 0xff; }

u32 bit8Scan(u32 v)
{
	return (v << 8) + (v << 16) + (v << 24);
}

groupshared uint ldsSortVal[NUM_BITS_TUPLES][WG_SIZE*ELEMENTS_PER_WORK_ITEM + 16];

void sort4Bits1KeyValue(inout u32 sortData[NUM_BITS_TUPLES][4], inout uint sortVal[NUM_BITS_TUPLES][4], int startBit, uint currentBitset, int lIdx)
{
	for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += 2)
	{
		uint4 b = make_uint4((sortData[currentBitset][0] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][1] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][2] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][3] >> (startBit + ibit)) & 0x3);

		u32 key4;
		u32 sKeyPacked[4] = { 0, 0, 0, 0 };
		{
			sKeyPacked[0] |= 1 << (8 * b.x);
			sKeyPacked[1] |= 1 << (8 * b.y);
			sKeyPacked[2] |= 1 << (8 * b.z);
			sKeyPacked[3] |= 1 << (8 * b.w);

			key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
		}

		u32 rankPacked;
		u32 sumPacked;
		{
			rankPacked = localPrefixSum(key4, currentBitset, lIdx, sumPacked, WG_SIZE);
		}

		GROUP_LDS_BARRIER;

		u32 newOffset[4] = { 0,0,0,0 };
		{
			u32 sumScanned = bit8Scan(sumPacked);

			u32 scannedKeys[4];
			scannedKeys[0] = 1 << (8 * b.x);
			scannedKeys[1] = 1 << (8 * b.y);
			scannedKeys[2] = 1 << (8 * b.z);
			scannedKeys[3] = 1 << (8 * b.w);
			{	//	4 scans at once
				u32 sum4 = 0;
				for (int ie = 0; ie < 4; ie++)
				{
					u32 tmp = scannedKeys[ie];
					scannedKeys[ie] = sum4;
					sum4 += tmp;
				}
			}

			{
				u32 sumPlusRank = sumScanned + rankPacked;
				{	u32 ie = b.x;
				scannedKeys[0] += sumPlusRank;
				newOffset[0] = unpack4Key(scannedKeys[0], ie);
				}
				{	u32 ie = b.y;
				scannedKeys[1] += sumPlusRank;
				newOffset[1] = unpack4Key(scannedKeys[1], ie);
				}
				{	u32 ie = b.z;
				scannedKeys[2] += sumPlusRank;
				newOffset[2] = unpack4Key(scannedKeys[2], ie);
				}
				{	u32 ie = b.w;
				scannedKeys[3] += sumPlusRank;
				newOffset[3] = unpack4Key(scannedKeys[3], ie);
				}
			}
		}


		GROUP_LDS_BARRIER;

		u32 dstAddr = 4 * lIdx;
		for (int ib = 0; ib < NUM_BITS_TUPLES; ib++)
		{
			ldsSortData[ib][newOffset[0]] = sortData[ib][0];
			ldsSortData[ib][newOffset[1]] = sortData[ib][1];
			ldsSortData[ib][newOffset[2]] = sortData[ib][2];
			ldsSortData[ib][newOffset[3]] = sortData[ib][3];

			ldsSortVal[ib][newOffset[0]] = sortVal[ib][0];
			ldsSortVal[ib][newOffset[1]] = sortVal[ib][1];
			ldsSortVal[ib][newOffset[2]] = sortVal[ib][2];
			ldsSortVal[ib][newOffset[3]] = sortVal[ib][3];

			GROUP_LDS_BARRIER;

			sortData[ib][0] = ldsSortData[ib][dstAddr + 0];
			sortData[ib][1] = ldsSortData[ib][dstAddr + 1];
			sortData[ib][2] = ldsSortData[ib][dstAddr + 2];
			sortData[ib][3] = ldsSortData[ib][dstAddr + 3];

			sortVal[ib][0] = ldsSortVal[ib][dstAddr + 0];
			sortVal[ib][1] = ldsSortVal[ib][dstAddr + 1];
			sortVal[ib][2] = ldsSortVal[ib][dstAddr + 2];
			sortVal[ib][3] = ldsSortVal[ib][dstAddr + 3];

			GROUP_LDS_BARRIER;
		}
	}
}

void sort4Bits1(inout u32 sortData[NUM_BITS_TUPLES][4], int startBit, uint currentBitset, int lIdx)
{
	/*
	for(uint ibit=0; ibit<BITS_PER_PASS; ibit+=2)
	{
	uint4 b = make_uint4((sortData[0]>>(startBit+ibit)) & 0x3,
	(sortData[1]>>(startBit+ibit)) & 0x3,
	(sortData[2]>>(startBit+ibit)) & 0x3,
	(sortData[3]>>(startBit+ibit)) & 0x3);

	u32 key4;
	u32 sKeyPacked[4] = { 0, 0, 0, 0 };
	{
	sKeyPacked[0] |= 1<<(8*b.x);
	sKeyPacked[1] |= 1<<(8*b.y);
	sKeyPacked[2] |= 1<<(8*b.z);
	sKeyPacked[3] |= 1<<(8*b.w);

	key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
	}

	u32 rankPacked;
	u32 sumPacked;
	{
	rankPacked = localPrefixSum64VSingle( key4, lIdx, sumPacked );
	}

	//		GROUP_LDS_BARRIER;

	u32 sum[4] = { unpack4Key( sumPacked,0 ), unpack4Key( sumPacked,1 ), unpack4Key( sumPacked,2 ), unpack4Key( sumPacked,3 ) };

	{
	u32 sum4 = 0;
	for(int ie=0; ie<4; ie++)
	{
	u32 tmp = sum[ie];
	sum[ie] = sum4;
	sum4 += tmp;
	}
	}

	u32 newOffset[4] = { 0,0,0,0 };

	for(int ie=0; ie<4; ie++)
	{
	uint4 key = extractKeys( b, ie );
	uint4 scannedKey = key;
	prefixScanVectorEx( scannedKey );
	uint offset = sum[ie] + unpack4Key( rankPacked, ie );
	uint4 dstAddress = make_uint4( offset, offset, offset, offset ) + scannedKey;

	newOffset[0] += dstAddress.x*key.x;
	newOffset[1] += dstAddress.y*key.y;
	newOffset[2] += dstAddress.z*key.z;
	newOffset[3] += dstAddress.w*key.w;
	}



	{
	ldsSortData[newOffset[0]] = sortData[0];
	ldsSortData[newOffset[1]] = sortData[1];
	ldsSortData[newOffset[2]] = sortData[2];
	ldsSortData[newOffset[3]] = sortData[3];

	//			GROUP_LDS_BARRIER;

	sortData[0] = ldsSortData[lIdx*4+0];
	sortData[1] = ldsSortData[lIdx*4+1];
	sortData[2] = ldsSortData[lIdx*4+2];
	sortData[3] = ldsSortData[lIdx*4+3];

	//			GROUP_LDS_BARRIER;
	}
	}
	*/
	for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += 2)
	{
		uint4 b = make_uint4((sortData[currentBitset][0] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][1] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][2] >> (startBit + ibit)) & 0x3,
			(sortData[currentBitset][3] >> (startBit + ibit)) & 0x3);

		u32 key4;
		u32 sKeyPacked[4] = { 0, 0, 0, 0 };
		{
			sKeyPacked[0] |= 1 << (8 * b.x);
			sKeyPacked[1] |= 1 << (8 * b.y);
			sKeyPacked[2] |= 1 << (8 * b.z);
			sKeyPacked[3] |= 1 << (8 * b.w);

			key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
		}

		u32 rankPacked;
		u32 sumPacked;
		{
			rankPacked = localPrefixSum(key4, currentBitset, lIdx, sumPacked, WG_SIZE);
		}

		GROUP_LDS_BARRIER;

		u32 newOffset[4] = { 0,0,0,0 };
		{
			u32 sumScanned = bit8Scan(sumPacked);

			u32 scannedKeys[4];
			scannedKeys[0] = 1 << (8 * b.x);
			scannedKeys[1] = 1 << (8 * b.y);
			scannedKeys[2] = 1 << (8 * b.z);
			scannedKeys[3] = 1 << (8 * b.w);
			{	//	4 scans at once
				u32 sum4 = 0;
				for (int ie = 0; ie < 4; ie++)
				{
					u32 tmp = scannedKeys[ie];
					scannedKeys[ie] = sum4;
					sum4 += tmp;
				}
			}

			{
				u32 sumPlusRank = sumScanned + rankPacked;
				{	u32 ie = b.x;
				scannedKeys[0] += sumPlusRank;
				newOffset[0] = unpack4Key(scannedKeys[0], ie);
				}
				{	u32 ie = b.y;
				scannedKeys[1] += sumPlusRank;
				newOffset[1] = unpack4Key(scannedKeys[1], ie);
				}
				{	u32 ie = b.z;
				scannedKeys[2] += sumPlusRank;
				newOffset[2] = unpack4Key(scannedKeys[2], ie);
				}
				{	u32 ie = b.w;
				scannedKeys[3] += sumPlusRank;
				newOffset[3] = unpack4Key(scannedKeys[3], ie);
				}
			}
		}
		
		GROUP_LDS_BARRIER;

		u32 dstAddr = 4 * lIdx;
		for (int ib = 0; ib < NUM_BITS_TUPLES; ib++)
		{
			ldsSortData[ib][newOffset[0]] = sortData[ib][0];
			ldsSortData[ib][newOffset[1]] = sortData[ib][1];
			ldsSortData[ib][newOffset[2]] = sortData[ib][2];
			ldsSortData[ib][newOffset[3]] = sortData[ib][3];

			GROUP_LDS_BARRIER;

			sortData[ib][0] = ldsSortData[ib][dstAddr + 0];
			sortData[ib][1] = ldsSortData[ib][dstAddr + 1];
			sortData[ib][2] = ldsSortData[ib][dstAddr + 2];
			sortData[ib][3] = ldsSortData[ib][dstAddr + 3];

			GROUP_LDS_BARRIER;
		}
	}
}


groupshared u32 localHistogramToCarry[NUM_BUCKET];
groupshared u32 localHistogram[NUM_BUCKET * 2];
#define SET_HISTOGRAM(setIdx, key, currentBitset) ldsSortData[currentBitset][(setIdx)*NUM_BUCKET+key]


//StructuredBuffer<u32> gSrc : register( t0 );

#if defined(KEY_VALUE_TWIN)
RWStructuredBuffer<SortData> gDst : register(u0);
#elif defined(KEY_VALUE_SEPARATELY)
RWStructuredBuffer<u32> gDstKey[NUM_BITS_TUPLES] : register(u0);
RWStructuredBuffer<u32> gDstValue[NUM_BITS_TUPLES] : register(u[NUM_BITS_TUPLES]);
#endif


[numthreads(WG_SIZE, 1, 1)]
void main(DEFAULT_ARGS)
{
	u32 gIdx = GET_GLOBAL_IDX;
	u32 lIdx = GET_LOCAL_IDX;
	u32 wgIdx = GET_GROUP_IDX;
	u32 wgSize = GET_GROUP_SIZE;

	const int n = m_n;
	const int nAligned = m_nAligned;
	const int nWGs = m_nWGs;
	const int startBit = m_startBit;
	const int nBlocksPerWG = m_nBlocksPerWG;
	const uint currentBitset = m_currentBitset;

	if (lIdx < (NUM_BUCKET))
	{
		localHistogramToCarry[lIdx] = rHistogram[lIdx*nWGs + wgIdx];
	}

	GROUP_LDS_BARRIER;

	const int blockSize = ELEMENTS_PER_WORK_ITEM*WG_SIZE;

	int nBlocks = nAligned / blockSize - nBlocksPerWG*wgIdx;

	int addr = blockSize*nBlocksPerWG*wgIdx + ELEMENTS_PER_WORK_ITEM*lIdx;

	for (int iblock = 0; iblock < min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		u32 myHistogram = 0;

		uint sortData[NUM_BITS_TUPLES][ELEMENTS_PER_WORK_ITEM];
		uint sortVal[NUM_BITS_TUPLES][ELEMENTS_PER_WORK_ITEM];

		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
#if defined(CHECK_BOUNDARY)
		{
#if defined(KEY_VALUE_TWIN)
			sortData[i] = (addr + i < n) ? gSrc[addr + i].m_key : 0xffffffff;
			sortVal[i] = (addr + i < n) ? gSrc[addr + i].m_value : 0xffffffff;
#elif defined(KEY_VALUE_SEPARATELY)
			for (int ib = 0; ib < NUM_BITS_TUPLES; ib++)
			{
				sortData[ib][i] = (addr + i < n) ? gSrcKey[ib][addr + i] : 0xffffffff;
				sortVal[ib][i] = (addr + i < n) ? gSrcVal[ib][addr + i] : 0xffffffff;
			}
#endif
		}
#else
		{
			for (int ib = 0; ib < NUM_BITS_TUPLES; ib++)
			{
				sortData[ib][i] = gSrcKey[ib][addr + i];
				sortVal[ib][i] = gSrcVal[ib][addr + i];
			}
		}
#endif

		sort4Bits1KeyValue(sortData, sortVal, startBit, currentBitset, lIdx);

		u32 keys[ELEMENTS_PER_WORK_ITEM];
		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
			keys[i] = (sortData[currentBitset][i] >> startBit) & 0xf;

		{	//	create histogram
			u32 setIdx = lIdx / 16;
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[lIdx] = 0;
			}
			ldsSortData[currentBitset][lIdx] = 0;
			GROUP_LDS_BARRIER;

			for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
#if defined(CHECK_BOUNDARY)
				if (addr + i < n)
#endif
					AtomInc(SET_HISTOGRAM(setIdx, keys[i], currentBitset));

			GROUP_LDS_BARRIER;

			uint hIdx = NUM_BUCKET + lIdx;
			if (lIdx < NUM_BUCKET)
			{
				u32 sum = 0;
				for (int i = 0; i < WG_SIZE / 16; i++)
				{
					sum += SET_HISTOGRAM(i, lIdx, currentBitset);
				}
				myHistogram = sum;
				localHistogram[hIdx] = sum;
			}
			GROUP_LDS_BARRIER;

#if defined(USE_2LEVEL_REDUCE)
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[hIdx] = localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;

				u32 u0, u1, u2;
				u0 = localHistogram[hIdx - 3];
				u1 = localHistogram[hIdx - 2];
				u2 = localHistogram[hIdx - 1];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;
				u0 = localHistogram[hIdx - 12];
				u1 = localHistogram[hIdx - 8];
				u2 = localHistogram[hIdx - 4];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;
			}
#else
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[hIdx] = localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 2];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 4];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 8];
				GROUP_MEM_FENCE;
			}
#endif
			GROUP_LDS_BARRIER;
		}

		{
			for (int ie = 0; ie < ELEMENTS_PER_WORK_ITEM; ie++)
			{
				int dataIdx = ELEMENTS_PER_WORK_ITEM*lIdx + ie;
				int binIdx = keys[ie];
				int groupOffset = localHistogramToCarry[binIdx];
				int myIdx = dataIdx - localHistogram[NUM_BUCKET + binIdx];
#if defined(CHECK_BOUNDARY)
				if (addr + ie < n)
#endif
				{
#if defined(KEY_VALUE_TWIN)
					gDst[groupOffset + myIdx].m_key = sortData[ie];
					gDst[groupOffset + myIdx].m_value = sortVal[ie];
#elif defined(KEY_VALUE_SEPARATELY)
					for (int ib = 0; ib < NUM_BITS_TUPLES; ib++)
					{
						gDstKey[ib][groupOffset + myIdx] = sortData[ib][ie];
						gDstValue[ib][groupOffset + myIdx] = sortVal[ib][ie];
					}
#endif
				}
			}
		}

		GROUP_LDS_BARRIER;

		if (lIdx < NUM_BUCKET)
		{
			localHistogramToCarry[lIdx] += myHistogram;
		}
		GROUP_LDS_BARRIER;
	}
	/*
	GROUP_LDS_BARRIER;

	const int blockSize = ELEMENTS_PER_WORK_ITEM*WG_SIZE;
	//	for(int addr=ELEMENTS_PER_WORK_ITEM*gIdx; addr<n; addr+=stride)
	[loop]
	for(int addr = blockSize*nBlocksPerWG*wgIdx+ELEMENTS_PER_WORK_ITEM*lIdx;
	addr<min(blockSize*nBlocksPerWG*(wgIdx+1), n);
	addr+=blockSize )
	{
	u32 myHistogram = 0;

	u32 sortData[ELEMENTS_PER_WORK_ITEM];
	{
	for(int i=0; i<ELEMENTS_PER_WORK_ITEM; i++)
	sortData[i] = gSrc[ addr+i ];
	}

	sort4Bits1(sortData, startBit, lIdx);

	u32 keys[4];
	for(int i=0; i<4; i++)
	keys[i] = (sortData[i]>>startBit) & 0xf;

	{	//	create histogram
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[lIdx] = 0;
	localHistogram[NUM_BUCKET+lIdx] = 0;
	}
	//			GROUP_LDS_BARRIER;

	AtomInc( localHistogram[NUM_BUCKET+keys[0]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[1]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[2]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[3]] );

	//			GROUP_LDS_BARRIER;

	uint hIdx = NUM_BUCKET+lIdx;
	if( lIdx < NUM_BUCKET )
	{
	myHistogram = localHistogram[hIdx];
	}
	//			GROUP_LDS_BARRIER;

	#if defined(USE_2LEVEL_REDUCE)
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[hIdx] = localHistogram[hIdx-1];
	GROUP_MEM_FENCE;

	u32 u0, u1, u2;
	u0 = localHistogram[hIdx-3];
	u1 = localHistogram[hIdx-2];
	u2 = localHistogram[hIdx-1];
	AtomAdd( localHistogram[hIdx], u0 + u1 + u2 );
	GROUP_MEM_FENCE;
	u0 = localHistogram[hIdx-12];
	u1 = localHistogram[hIdx-8];
	u2 = localHistogram[hIdx-4];
	AtomAdd( localHistogram[hIdx], u0 + u1 + u2 );
	GROUP_MEM_FENCE;
	}
	#else
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[hIdx] = localHistogram[hIdx-1];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-1];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-2];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-4];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-8];
	GROUP_MEM_FENCE;
	}
	#endif

	//			GROUP_LDS_BARRIER;
	}

	{
	for(int ie=0; ie<ELEMENTS_PER_WORK_ITEM; ie++)
	{
	int dataIdx = 4*lIdx+ie;
	int binIdx = keys[ie];
	int groupOffset = localHistogramToCarry[binIdx];
	int myIdx = dataIdx - localHistogram[NUM_BUCKET+binIdx];
	gDst[ groupOffset + myIdx ] = sortData[ie];
	}
	}

	//		GROUP_LDS_BARRIER;

	if( lIdx < NUM_BUCKET )
	{
	localHistogramToCarry[lIdx] += myHistogram;
	}
	//		GROUP_LDS_BARRIER;

	}
	*/
}


