#pragma once

#include "interaction.hlsl"
#include "spectrum.hlsl"

namespace framework
{
	struct light_li_sample
	{
		::framework::sampled_spectrum l;
		float3 wi;
		float pdf;
		::framework::interaction p_light;

		static ::framework::light_li_sample create(::framework::sampled_spectrum l, float3 wi, float pdf, ::framework::interaction p_light)
		{
			::framework::light_li_sample result;
			result.l = l;
			result.wi = wi;
			result.pdf = pdf;
			result.p_light = p_light;
			return result;
		}
	};

	struct light_sample_context
	{
		float3 pi;
		float3 n, ns;
	};

	struct point_light
	{
		float scale;

		bool sample_li(::framework::light_sample_context ctx, out ::framework::light_li_sample light_li_sample)
		{
			float3 p = float3(10, 10, 10);
			float3 wi = p - ctx.pi;
			float distance_squared = dot(wi, wi);
			wi /= sqrt(distance_squared);
			::framework::sampled_spectrum li = ::framework::sampled_spectrum::create(10.f) * this.scale / distance_squared;
			light_li_sample = ::framework::light_li_sample::create(li, wi, 1, ::framework::interaction::create(p));
			return true;
		}
	};
}