#pragma once

namespace framework
{
	template<typename T>
	T square(T v)
	{
		return v * v;
	}

	template<typename T>
	T atanh(T v)
	{
		return 0.5 * log((1 + v) / (1 - v));
	}
}