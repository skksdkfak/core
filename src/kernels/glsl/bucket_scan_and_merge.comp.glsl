#version 460

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_ballot : enable
#extension GL_GOOGLE_include_directive : enable

#include "hybrid_radix_sort.glsl"

layout(local_size_x = BUCKET_SCAN_WG_SIZE) in;

layout(std140, push_constant) uniform PushConsts
{
    uint dstSet;
};

layout(binding = 0) uniform Consts
{
    uint size;
    uint nWGs;
    uint startBit;
    uint nBuckets;
    uint nScanBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer BucketHistogramIn
{
    uint gBucketHistogramIn[];
};

layout(std430, binding = 2) writeonly buffer BucketHistogramOut
{
    uint gBucketHistogramOut[];
};

layout(std430, binding = 3) buffer SubBucketHistogram
{
    uint gSubBucketHistogram[];
};

layout(std430, binding = 4) writeonly buffer BlockAssignments
{
    BlockAssignment blockAssignments[];
};

layout(std430, binding = 5) writeonly buffer BucketAssignments
{
    BucketAssignment bucketAssignments[];
};

layout(std430, binding = 6) buffer HybridRadixSortDispatchIndirectCommand
{
    DispatchIndirectCommand hybridRadixSortDispathIndirectCommands[][3];
};

shared uint ldsBucketHistogram[NUM_BUCKET * BUCKET_SCAN_NUM_WIS_PER_BLOCK];

void main()
{
    const uint blockSize = BUCKET_SCAN_NUM_WIS_PER_BLOCK * NUM_BUCKET;
    for (uint iblock = 0; iblock < nScanBlocksPerWG; ++iblock)
    {
        const uint threadBucketSum = 0;
        const uint blockOffset = nScanBlocksPerWG * blockSize * gl_WorkGroupID.x + iblock * blockSize;
        const uint currentBlockSize = min(blockSize, nBuckets - blockOffset);
        for (uint idx = gl_LocalInvocationIndex.x; idx < currentBlockSize; idx += gl_WorkGroupSize.x)
        {
            ldsBucketHistogram[idx] = gSubBucketHistogram[blockOffset + idx];
        }

        groupMemoryBarrier();
        barrier();

        const uint begin = gl_LocalInvocationIndex.x * NUM_BUCKET;
        const uint end = min(begin + NUM_BUCKET, currentBlockSize);
        if (begin < end)
        {
            uint idx, bucketSize;
            uint sum = gBucketHistogramIn[blockOffset / NUM_BUCKET + gl_LocalInvocationIndex.x];
            uint mergeSum = 0;
            uint count = 0;
            for (idx = begin; idx < end; ++idx)
            {
                bucketSize = ldsBucketHistogram[idx];
                ldsBucketHistogram[idx] = sum;
                mergeSum += bucketSize;
                if (mergeSum > MBT)
                {
                    if (count > 0 && (mergeSum -= bucketSize) > 0)
                    {
                        const uint bucketAssignmentIdx = atomicAdd(hybridRadixSortDispathIndirectCommands[dstSet][2].x, 1);
                        bucketAssignments[bucketAssignmentIdx] = BucketAssignment(
                            sum - mergeSum,
                            mergeSum,
                            uint(count > 1)
                        );
                    }

                    if (bucketSize > MBT)
                    {
                        if (bucketSize > LST)
                        {
                            const uint quotient = bucketSize / KPB;
                            const uint remainder = bucketSize % KPB;
                            const uint b_id = atomicAdd(hybridRadixSortDispathIndirectCommands[dstSet][1].x, 1);
                            gBucketHistogramOut[b_id] = sum;

                            uint blockAssignmentIdx = atomicAdd(hybridRadixSortDispathIndirectCommands[dstSet][0].x, quotient + uint(remainder > 0));
                            uint k_offset = sum;
                            for (uint i = 0; i < quotient; ++i, ++blockAssignmentIdx, k_offset += KPB)
                            {
                                blockAssignments[blockAssignmentIdx] = BlockAssignment(
                                    k_offset,
                                    KPB,
                                    b_id
                                );
                            }
                            if (remainder > 0)
                            {
                                blockAssignments[blockAssignmentIdx] = BlockAssignment(
                                    k_offset,
                                    remainder,
                                    b_id
                                );
                            }
                        }
                        else
                        {
                            const uint bucketAssignmentIdx = atomicAdd(hybridRadixSortDispathIndirectCommands[dstSet][2].x, 1);
                            bucketAssignments[bucketAssignmentIdx] = BucketAssignment(
                                sum,
                                bucketSize,
                                0
                            );
                        }

                        mergeSum = 0;
                        count = 0;
                    }
                    else
                    {
                        mergeSum = bucketSize;
                        count = 1;
                    }
                }
                else
                {
                    ++count;
                }
                sum += bucketSize;
            }

            if (count > 0 && mergeSum > 0 && mergeSum <= MBT)
            {
                const uint bucketAssignmentIdx = atomicAdd(hybridRadixSortDispathIndirectCommands[dstSet][2].x, 1);
                bucketAssignments[bucketAssignmentIdx] = BucketAssignment(
                    sum - mergeSum,
                    mergeSum,
                    uint(count > 1)
                );
            }
        }

        groupMemoryBarrier();
        barrier();

        for (uint idx = gl_LocalInvocationIndex.x; idx < min(blockSize, nBuckets - blockOffset); idx += gl_WorkGroupSize.x)
        {
            gSubBucketHistogram[blockOffset + idx] = ldsBucketHistogram[idx];
        }
    }
}