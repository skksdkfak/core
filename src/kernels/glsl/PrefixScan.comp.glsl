#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable



#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define GROUP_MEM_FENCE groupMemoryBarrier()
#define AtomAdd(x, inc) atomicAdd(x, inc)

#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

//	Cypress
//#define nPerWI 16
//	Cayman
//#define nPerWI 20
//	NV
#define nPerWI 1024

#define nPerLane (nPerWI / 4)

layout(local_size_x = 128) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) buffer Histogram
{
	uint wHistogram1[];
};

shared uint ldsSortData[WG_SIZE * ELEMENTS_PER_WORK_ITEM + 16];

uint prefixScanVectorEx(inout uvec4 data)
{
	uint sum = 0;
	uint tmp = data.x;
	data.x = sum;
	sum += tmp;

	tmp = data.y;
	data.y = sum;
	sum += tmp;

	tmp = data.z;
	data.z = sum;
	sum += tmp;

	tmp = data.w;
	data.w = sum;
	sum += tmp;

	return sum;
}

uint localPrefixSum(uint pData, uint lIdx, inout uint totalSum, int wgSize /*64 or 128*/)
{
	{	//	Set data
		ldsSortData[lIdx] = 0;
		ldsSortData[lIdx + wgSize] = pData;
	}

	GROUP_LDS_BARRIER;

	{	//	Prefix sum
		uint idx = 2 * lIdx + (wgSize + 1);
#if defined(USE_2LEVEL_REDUCE)
		if (lIdx < 64)
		{
			uint u0, u1, u2;
			u0 = ldsSortData[idx - 3];
			u1 = ldsSortData[idx - 2];
			u2 = ldsSortData[idx - 1];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 12];
			u1 = ldsSortData[idx - 8];
			u2 = ldsSortData[idx - 4];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 48];
			u1 = ldsSortData[idx - 32];
			u2 = ldsSortData[idx - 16];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[idx] += ldsSortData[idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[idx - 1] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
		}
#else
		if (lIdx < 64)
		{
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 1]);
			GROUP_MEM_FENCE;
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 2]);
			GROUP_MEM_FENCE;
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 4]);
			GROUP_MEM_FENCE;
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 8]);
			GROUP_MEM_FENCE;
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 16]);
			GROUP_MEM_FENCE;
			AtomAdd(ldsSortData[idx], ldsSortData[idx - 32]);
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				AtomAdd(ldsSortData[idx], ldsSortData[idx - 64]);
				GROUP_MEM_FENCE;
			}

			AtomAdd(ldsSortData[idx - 1], ldsSortData[idx - 2]);
			GROUP_MEM_FENCE;
		}
#endif
	}

	GROUP_LDS_BARRIER;

	totalSum = ldsSortData[wgSize * 2 - 1];
	uint addValue = ldsSortData[lIdx + wgSize - 1];
	return addValue;
}

//__attribute__((reqd_work_group_size(128,1,1)))
uvec4 localPrefixSum128V(uvec4 pData, uint lIdx, inout uint totalSum)
{
	uint s4 = prefixScanVectorEx(pData);
	uint rank = localPrefixSum(s4, lIdx, totalSum, 128);
	return pData + uvec4(rank, rank, rank, rank);
}

//__attribute__((reqd_work_group_size(64,1,1)))
uvec4 localPrefixSum64V(uvec4 pData, uint lIdx, inout uint totalSum)
{
	uint s4 = prefixScanVectorEx(pData);
	uint rank = localPrefixSum(s4, lIdx, totalSum, 64);
	return pData + uvec4(rank, rank, rank, rank);
}

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	const uint nWGs = m_nWGs;

	uint data[nPerWI];
	uint histogramIdx;
	for (uint i = 0; i < nPerWI; i++)
	{
		data[i] = 0;
		histogramIdx = nPerWI * lIdx + i;
		if (histogramIdx < NUM_BUCKET * nWGs)
			data[i] = wHistogram1[histogramIdx];
	}

	uvec4 myData = uvec4(0, 0, 0, 0);

	for (uint i = 0; i < nPerLane; i++)
	{
		myData.x += data[nPerLane * 0 + i];
		myData.y += data[nPerLane * 1 + i];
		myData.z += data[nPerLane * 2 + i];
		myData.w += data[nPerLane * 3 + i];
	}

	uint totalSum;
	uvec4 scanned = localPrefixSum128V(myData, lIdx, totalSum);

	//	for(int j=0; j<4; j++) //	somehow it introduces a lot of branches
	{	int j = 0;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 1;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 2;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 3;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}

	for (int i = 0; i<nPerLane; i++)
	{
		data[nPerLane * 0 + i] += scanned.x;
		data[nPerLane * 1 + i] += scanned.y;
		data[nPerLane * 2 + i] += scanned.z;
		data[nPerLane * 3 + i] += scanned.w;
	}

	for (int i = 0; i < nPerWI; i++)
	{
		histogramIdx = nPerWI * lIdx + i;
		if (histogramIdx < NUM_BUCKET * nWGs)
			wHistogram1[histogramIdx] = data[i];
	}
}