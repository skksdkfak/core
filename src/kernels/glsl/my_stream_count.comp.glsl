#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define AtomInc(x) atomicAdd(x, 1)

#define WG_SIZE 256
#define ELEMENTS_PER_WORK_GROUP 1024
#define ELEMENTS_PER_WORK_ITEM (ELEMENTS_PER_WORK_GROUP / WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1 << BITS_PER_PASS)

#define CHECK_BOUNDARY 1

layout(local_size_x = WG_SIZE) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer Data
{
    uint gSrc[];
};

layout(std430, binding = 2) writeonly buffer Histogram
{
	uint histogramOut[];
};

shared uint localHistogramMat[NUM_BUCKET];

#define MY_HISTOGRAM(idx) localHistogramMat[idx]

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;
	
	if (lIdx < NUM_BUCKET)
	{
		localHistogramMat[lIdx] = 0;
	}
	GROUP_LDS_BARRIER;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM * WG_SIZE;

	const uint nBlocks = nAligned / blockSize - nBlocksPerWG * wgIdx;

	uint addr = blockSize * nBlocksPerWG * wgIdx + ELEMENTS_PER_WORK_ITEM * lIdx;

	uint localKey;
	for (uint iblock = 0; iblock < min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		for (uint i = 0; i<ELEMENTS_PER_WORK_ITEM; i++)
		{
			if (addr + i < n)
			{
				localKey = (gSrc[addr + i] >> startBit) & 0xf;
				AtomInc(localHistogramMat[localKey]);
			}
		}
	}
	GROUP_LDS_BARRIER;

	if (lIdx < NUM_BUCKET)
	{
		histogramOut[lIdx * nWGs + wgIdx] = localHistogramMat[lIdx];
	}
}