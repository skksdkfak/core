#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F
#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM 64

layout(local_size_x = WG_SIZE) in;

struct Bound
{
	vec3 min;
	float pad0;
	vec3 max;
	float pad1;
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numElements;
};

layout (std430, binding = 0) buffer Bounds
{
    Bound bounds[];
};

void merge_bounds(in Bound b1, in Bound b2, inout Bound b3)
{
	b3.min = min(b1.min, b2.min);
	b3.max = max(b1.max, b2.max);
}

shared Bound localBounds[WG_SIZE];

void main()
{
	Bound localBound;
	localBound.min = vec3(FLT_MAX);
	localBound.max = vec3(FLT_MIN);
	
	localBounds[gl_LocalInvocationID.x] = localBound;

	groupMemoryBarrier();
	barrier();

	for (uint i = gl_LocalInvocationID.x * ELEMENTS_PER_WORK_ITEM + gl_WorkGroupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM; i < numElements, i < gl_WorkGroupID.x * WG_SIZE * ELEMENTS_PER_WORK_ITEM + gl_LocalInvocationID.x * ELEMENTS_PER_WORK_ITEM + ELEMENTS_PER_WORK_ITEM; i++)
	{
		Bound bound = bounds[i];
		localBound.min = min(localBound.min, bound.min);
		localBound.max = max(localBound.max, bound.max);
	}
	
	localBounds[gl_LocalInvocationID.x] = localBound;
		
	groupMemoryBarrier();
	barrier();
	
	if (gl_LocalInvocationID.x == 0)
	{
		localBound = localBounds[0];
		for (uint i = 1; i < WG_SIZE; i++)
		{
			merge_bounds(localBounds[i], localBound, localBound);
		}
		bounds[gl_WorkGroupID.x] = localBound;
	}
}