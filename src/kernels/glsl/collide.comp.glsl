#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_debug_printf : enable

#define WG_SIZE 128
#define INTERSECT_STACK_SIZE 128

layout(local_size_x = WG_SIZE) in;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

struct LinkedList
{
	uint next;
	uint val;
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numPrimitives;
	float delta_time;
};

layout (std430, binding = 0) buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 1) writeonly buffer NewVelocity
{
    vec4 newVelocities[];
};

layout (std430, binding = 2) readonly buffer OldVelocity
{
    vec4 oldVelocities[];
};

layout (std430, binding = 3) readonly buffer NeighborsList
{
    LinkedList neighborsList[];
};

layout (std430, binding = 4) readonly buffer NeighborsHeadList
{
	uint neighborsHeadList[];
};

layout(std430, binding = 5) buffer NeighborsCounter
{
	uint neighborsCounter;
};

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint numberOfPrimitives = numPrimitives;
	
	if (i >= numberOfPrimitives)
		return;
		
	const float spring = 100.f;
	const float damping = 0.0f;
	const float shear = 0.0f;
	const float attraction = 100.f;
	const CompactSphere compactSphere = spheres[i];
	const vec4 oldVelocityA = oldVelocities[i];
    vec3 force = vec3(0.0f);
	uint nodeIndex = neighborsHeadList[i];
	while (nodeIndex != 0xFFFFFFFF)
	{
		LinkedList linkedListNode = neighborsList[nodeIndex];
		nodeIndex = linkedListNode.next;
		const CompactSphere neighbor = spheres[linkedListNode.val];
		const vec4 oldVelocityB = oldVelocities[linkedListNode.val];
		
		// calculate relative position
		vec3 relPos = neighbor.field0.xyz - compactSphere.field0.xyz;

		float dist = length(relPos);
		float collideDist = compactSphere.field0.w + neighbor.field0.w;
        vec3 norm = relPos / max(dist, exp2(-80.0f));

        // relative velocity
        vec3 relVel = oldVelocityB.xyz - oldVelocityA.xyz;

        // relative tangential velocity
        vec3 tanVel = relVel - (dot(relVel, norm) * norm);
		
        // spring force
        force += spring * (collideDist - dist) * norm;
        // dashpot (damping) force
        force += damping * relVel;
        // tangential shear force
        force += shear * tanVel;
        // attraction
        force += attraction * relPos;

		//uint tmp = atomicAdd(neighborsCounter, -1);
		//if (tmp == -1)
		//	debugPrintfEXT("-1", tmp);
	}
	
	// apply gravity force
    force += vec3(0.0f, -0.03f, 0.0f);
	
	const float bbox_size = 2.0f;
    /*if (next_position.x < -bbox_size + compactSphere.field0.w)
    {
        next_position.x = -bbox_size + compactSphere.field0.w;
        velocity.x = velocity.x * -0.5f;
    }
    if (next_position.x > bbox_size + compactSphere.field0.w)
    {
        next_position.x = bbox_size - compactSphere.field0.w;
        velocity.x = velocity.x * -0.5f;
    }
    if (next_position.z < -bbox_size + compactSphere.field0.w)
    {
        next_position.z = -bbox_size + compactSphere.field0.w;
        velocity.z = velocity.z * -0.5f;
    }
    if (next_position.z > bbox_size + compactSphere.field0.w)
    {
        force.y += 1.0;
    }*/
    if (compactSphere.field0.y < -0.5f + compactSphere.field0.w)
    {
        //force.y += 1.0;
    }

	float mass = 1.0;
	vec3 acceleration = force / mass;

	vec3 delta_position = oldVelocityA.xyz;
	vec3 current_position = compactSphere.field0.xyz;
	vec3 next_position = compactSphere.field0.xyz + delta_position + acceleration * delta_time * delta_time;

	spheres[i].field0.xyz = next_position;
	newVelocities[i].xyz = next_position - current_position;
}