#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_basic : enable
#extension GL_KHR_shader_subgroup_arithmetic : enable

#define SIZE 67108864
#define WG_SIZE 256
#define BLOCK_SIZE (8 * WG_SIZE)
#define LST 20

layout(local_size_x = WG_SIZE) in;

layout (std430, binding = 0) buffer Data
{
	uint gData[];
};

//shared uint ldsData[256];
shared uint ldsData[BLOCK_SIZE];
shared uint ldsRangeBeginOffset[256];
shared uint ldsHistogram[256];
shared uint ldsWarpSums[gl_WorkGroupSize.x / 32];

void main()
{
	const uint lIdx = gl_LocalInvocationID.x;
	const uint wgIdx = gl_WorkGroupID.x;
//	// memory optimal
//	uint idx = wgIdx * 256 * 256 + lIdx;
//	uint sum = 0;
//	for (uint i = 0; i < 256; i++)
//	{
//		uint srcIdx = idx + i * 256;
//		if (srcIdx < SIZE) {
//			uint tmp = gData[srcIdx];
//			gData[srcIdx] = sum;
//			sum += tmp;
//		}
//		groupMemoryBarrier();
//		barrier();
//	}

//    // memory optimal
//    uint localData[256];
//	uint idx = wgIdx * 256 * 256 + lIdx;
//	uint sum = 0;
//	for (uint i = 0; i < 256; i++)
//	{
//		uint srcIdx = idx + i * 256;
//		if (srcIdx < SIZE) {
//            localData[i]= gData[srcIdx];
//		}
//	}
//
//	for (uint i = 0; i < 256; i++)
//	{
//		uint srcIdx = idx + i * 256;
//		if (srcIdx < SIZE) {
//            uint tmp = localData[i];
//            gData[srcIdx] = sum;
//            sum += tmp;
//		}
//	}

//	// memory optimal + lds
//	uint idx = wgIdx * 256 * 256 + lIdx;
//	uint sum = 0;
//	for (uint i = 0; i < 256; i++)
//	{
//		uint srcIdx = idx + i * 256;
//		if (srcIdx < SIZE)
//		{
//			ldsData[i] = gData[srcIdx];
//		}
//
//		if (lIdx == 0)
//		{
//			for (uint j = 0; j < 256; ++j)
//			{
//				uint tmp = ldsData[j];
//				ldsData[j] = sum;
//				sum += tmp;
//			}
//		}
//
//		groupMemoryBarrier();
//		barrier();
//
//		if (srcIdx < SIZE)
//		{
//			gData[srcIdx] = ldsData[i];
//		}
//	}

//	uint globalOffset = wgIdx * 256 * 256;
//	for (uint iblock = 0; iblock < (256 * 256 + BLOCK_SIZE - 1) / BLOCK_SIZE; ++iblock)
//	{
//		const uint blockOffset = globalOffset + iblock * BLOCK_SIZE;
//		for (uint idx = gl_LocalInvocationIndex.x; idx < BLOCK_SIZE; idx += gl_WorkGroupSize.x)
//		{
//			const uint addr = blockOffset + idx;
//			ldsData[idx] = addr < SIZE ? gData[addr] : 0;
//		}
//
//		groupMemoryBarrier();
//		barrier();
//
//
//		uint idx, tmp;
//		uint sum = 0;
//		uint count = 0;
//		const uint begin = gl_LocalInvocationIndex.x * 256;
//		const uint end = min(begin + 256, BLOCK_SIZE);
//		for (tmp, idx = begin; idx < end; ++idx)
//		{
//			tmp = ldsData[idx];
//			sum += tmp;
//			if (sum > LST)
//			{
//				if (count > 1)
//				{
//					const uint dstMergeIdx = idx - count;
//					const uint hiIdx = dstMergeIdx >> 5;
//					const uint loIdx = dstMergeIdx & 31u;
//					ldsData[idx - count] = sum - tmp;
//					//mergedBuckets[hiIdx] |= 1u << loIdx;
//				}
//
//				if (tmp > LST)
//				{
//					sum = 0;
//					count = 0;
//				}
//				else
//				{
//					sum = tmp;
//					count = 1;
//				}
//			}
//			else
//			{
//				if (count > 0)
//				{
//					ldsData[idx] = 0;
//				}
//				++count;
//			}
//		}
//
//		if (sum <= LST && count > 1 && begin < end)
//		{
//			const uint dstMergeIdx = idx - count;
//			const uint hiIdx = dstMergeIdx >> 5;
//			const uint loIdx = dstMergeIdx & 31u;
//			ldsData[idx - count] = sum;
//			//mergedBuckets[hiIdx] |= 1u << loIdx;
//		}
//
//		groupMemoryBarrier();
//		barrier();
//
//
//
//		for (uint idx = gl_LocalInvocationIndex.x; idx < BLOCK_SIZE; idx += gl_WorkGroupSize.x)
//		{
//			uint sum = 0;
//			const uint addr = blockOffset + idx;
//			if (addr < SIZE)
//			{
//				uint tmp = ldsData[idx];
//				gData[addr] =  sum;
//				sum += tmp;
//			}
//		}
//
//		groupMemoryBarrier();
//		barrier();
//	}

//	// memory unoptimal
//	uint idx = wgIdx * 256 * 256 + lIdx * 256;
//	uint sum = 0;
//	for (uint i = 0; i < 256; i++)
//	{
//		uint srcIdx = idx + i;
//		if (srcIdx < SIZE) {
//			uint tmp = gData[srcIdx];
//			gData[srcIdx] = sum;
//			sum += tmp;
//		}
//	}

//	uint mergeRange = 0;
//	for (uint offset = 1, ldsOffset = 0; offset < 256; ldsOffset += 256 / offset, offset <<= 1)
//	{
//
//		const uint idx = lIdx * offset;
//		if (idx < 256 || mergeRange > LST)
//		{
//			const uint rangeSize = ldsData[ldsOffset + idx + offset] - ldsData[ldsOffset + idx] + ldsHistogram[idx + offset];
//			ldsData[ldsOffset + 256 / offset + idx] = rangeSize;
//			if (rangeSize > LST)
//				ldsHistogram[idx + offset] = mergeRange;
//			mergeRange = rangeSize;
//		}
//
//		groupMemoryBarrier();
//		barrier();
//	}

	// v0.01 merge via scan
//	uint rangeBeginOffset = 0;
//	for (uint offset = 2; offset < 256; offset <<= 1)
//	{
//		if (lIdx % offset == 0 && lIdx < 256)
//		{
//			const uint rangeEndIdx = offset - 1 + lIdx;
//			const uint rangeBegin = ldsData[rangeBeginOffset + lIdx];
//			const uint rangeEnd = ldsData[rangeEndIdx] + ldsHistogram[rangeEndIdx];
//			const uint rangeSize = rangeEnd - rangeBegin;
//			if (rangeSize > LST)
//			{
//				if (ldsHistogram[rangeEndIdx] < LST)
//				{
//					rangeBeginOffset = rangeEndIdx;
//				}
//			}
//			else
//			{
//				ldsHistogram[rangeBeginOffset + lIdx] = rangeSize;
//			}
//		}
//
//		groupMemoryBarrier();
//		barrier();
//	}

//	if (lIdx == 1)
//		ldsHistogram[lIdx] = 18;
//	else if (lIdx == 3)
//		ldsHistogram[lIdx] = 3;
//	else if (lIdx == 4)
//		ldsHistogram[lIdx] = 17;
//	else if (lIdx == 5)
//		ldsHistogram[lIdx] = 8;
//	else if (lIdx == 8)
//		ldsHistogram[lIdx] = 2;
//	else if (lIdx == 12)
//		ldsHistogram[lIdx] = 20;
//	else
//		ldsHistogram[lIdx] = 1;
//
//	uint mergeData[/*log(255)*/8 * 2];
////	const uint data = gData[wgIdx * 256 + lIdx];
////	ldsHistogram[lIdx] = data == 228884324 ? 3 : 1;
//
//	uint rangeBeginOffset = ldsRangeBeginOffset[lIdx] = lIdx;
//
//	groupMemoryBarrier();
//	barrier();
//
//	for (uint offset = 1; offset < 256; offset <<= 1)
//	{
//		if (lIdx % (offset << 1) == 0 && lIdx < 256)
//		{
//			const uint rangeEndIdx = offset + lIdx;
//			const uint rangeBegin = ldsHistogram[rangeBeginOffset];
//			const uint rangeEnd = ldsHistogram[rangeEndIdx];
//			const uint rangeSize = rangeBegin + rangeEnd;
//			const uint rangeEndOffset = ldsRangeBeginOffset[rangeEndIdx];
//			if (rangeSize <= LST)
//			{
//				const uint mergeOffset = uint(log2(offset >> 1)) * 2;
//				mergeData[mergeOffset] = rangeBegin;
//				mergeData[mergeOffset + 1] = rangeEnd;
//				ldsHistogram[rangeBeginOffset] = rangeSize;
//				ldsHistogram[rangeEndIdx] = 0;
//			}
//
//			if (rangeEndOffset != rangeEndIdx || rangeSize >= LST)
//			{
//				rangeBeginOffset = ldsRangeBeginOffset[lIdx] = rangeEndOffset;
//			}
//		}
//
//		if (offset > 2 && false)
//		{
//			const uint prevOffset = (offset >> 1);
//			if (lIdx % prevOffset == 0 && lIdx > 0 && lIdx < 256)
//			{
//				const uint size = ldsHistogram[lIdx];
//				if (size > 0)
//				{
//					const uint leftNeighbor = ldsHistogram[lIdx - prevOffset];
//					const uint rightNeighbor = ldsHistogram[lIdx + prevOffset];
//					uint mergeOffset = uint(log2(prevOffset)) * 2;
//					do
//					{
//						--mergeOffset;
//						if (leftNeighbor + mergeData[mergeOffset] <= LST && rightNeighbor + mergeData[mergeOffset + 1] <= LST)
//						{
//							ldsHistogram[lIdx - prevOffset] += mergeData[mergeOffset];
//							ldsHistogram[lIdx + prevOffset] += mergeData[mergeOffset + 1];
//							ldsHistogram[lIdx] = 0;
//							break;
//						}
//					} while (mergeOffset > 1);
//				}
//			}
//		}
//
//		groupMemoryBarrier();
//		barrier();
//	}
//
//	gData[wgIdx * 256 + lIdx] = ldsHistogram[lIdx];

//	const uint sum = subgroupAdd(gl_SubgroupInvocationID);
//	if (subgroupElect())
//	{
//		atomicAdd(gData[wgIdx], sum);
//	}


	uint warpSum = subgroupAdd(gl_SubgroupInvocationID);
	if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
	{
		ldsWarpSums[gl_SubgroupID] = warpSum;
	}
	groupMemoryBarrier();
	barrier();

	if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		uint blockSum = subgroupAdd(ldsWarpSums[gl_SubgroupInvocationID]);
		if (subgroupElect())
		{
			atomicAdd(gData[wgIdx], blockSum);
		}
	}
}