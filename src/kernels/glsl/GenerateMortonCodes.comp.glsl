#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define WG_SIZE 64

layout(local_size_x = WG_SIZE) in;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

struct Bound
{
	vec3 min;
	float pad0;
	vec3 max;
	float pad1;
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numPrimitives;
};

layout (std430, binding = 0) readonly buffer Bounds
{
    Bound bounds[];
};

layout (std430, binding = 1) readonly buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 2) buffer MortonCodes
{
    uint morton_codes[];
};

layout (std430, binding = 3) buffer PrimitiveIndices
{
    uint primitive_indices[];
};

void calculateLeafBoundingBox(vec3 vertex1, vec3 vertex2, vec3 vertex3, inout Bound bound)
{
	bound.min = min(vertex1, min(vertex2, vertex3));
	bound.max = max(vertex1, max(vertex2, vertex3));
}

vec3 getTriangleCentroid(vec3 vertex1, vec3 vertex2, vec3 vertex3)
{
	Bound bound;
	calculateLeafBoundingBox(vertex1, vertex2, vertex3, bound);
	return (bound.max + bound.min) * 0.5f;
}

vec3 normalizeCentroid(vec3 c, Bound bound)
{
	vec3 normalized;
	normalized.x = (c.x - bound.min.x) / (bound.max.x - bound.min.x);
	normalized.y = (c.y - bound.min.y) / (bound.max.y - bound.min.y);
	normalized.z = (c.z - bound.min.z) / (bound.max.z - bound.min.z);
	return normalized;
}

uint expandBits(uint value)
{
	value = (value * 0x00010001u) & 0xFF0000FFu;
	value = (value * 0x00000101u) & 0x0F00F00Fu;
	value = (value * 0x00000011u) & 0xC30C30C3u;
	value = (value * 0x00000005u) & 0x49249249u;
	return value;
}

uint calculateMortonCode(vec3 p)
{
	// Discretize the unit cube into a 10 bit integer
	uvec3 discretized;
	discretized.x = uint(min(max(p.x * 1024.0f, 0.0f), 1023.0f));
	discretized.y = uint(min(max(p.y * 1024.0f, 0.0f), 1023.0f));
	discretized.z = uint(min(max(p.z * 1024.0f, 0.0f), 1023.0f));

	discretized.x = expandBits(discretized.x);
	discretized.y = expandBits(discretized.y);
	discretized.z = expandBits(discretized.z);

	return discretized.x * 4 + discretized.y * 2 + discretized.z;
}

void main()
{
	const uint thxId = gl_GlobalInvocationID.x;
	if (thxId >= numPrimitives)
		return;

	primitive_indices[thxId] = thxId;
	
	const vec3 sphereCentroid = spheres[thxId].field0.xyz;
	const vec3 normalizedCentroid = normalizeCentroid(sphereCentroid, bounds[0]);
	const uint mortonCode = calculateMortonCode(normalizedCentroid);

	morton_codes[thxId] = mortonCode;
}