#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(local_size_x = 256) in;

struct CompactTreeNode
{
	uvec4 field0; // parent, left, right, triangleID
	vec4 field1; // boundMin.xyz, rightmostLeafInLeftSubtree
	vec4 field2; // boundMax.xyz, rightmostLeafInRightSubtree
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numTriangles;
};

layout(std430, binding = 0) readonly buffer SortedKeys
{
    uint gSortedKeys[];
};

layout(std430, binding = 1) readonly buffer SortedIndices
{
    uint gSortedIndices[];
};

layout(std430, binding = 2) writeonly buffer RadixTree
{
    CompactTreeNode gRadixTree[];
};

// From http://embeddedgurus.com/state-space/2014/09/fast-deterministic-and-portable-counting-leading-zeros/
//int clz(int x)
//{
//	x |= x >> 1;
//	// the next 5 statements will turn all bits after the
//	// leading zero's into a 1-bit
//	x |= x >> 2;
//	x |= x >> 4;
//	x |= x >> 8;
//	x |= x >> 16;
//	// compute the Hamming weight of x ... and return 32
//	// minus the computed result'
//	x -= ((x >> 1) & 0x55555555U);
//	x = (x & 0x33333333U) + ((x >> 2) & 0x33333333U);
//	return 32U - ((((x + (x >> 4)) & 0x0F0F0F0FU) * 0x01010101U) >> 24);
//}

//int clz(int x)
//{
//	return 31 - uint(log2(x));
//}

int clz(int x)
{
	return 31 - findMSB(x);
}

//#define clz(x) (31 - firstbithigh(x))

// Longest common prefix for morton code
int longestCommonPrefix(uint numberOfElements, int index1, int index2, uint key1)
{
	// No need to check the upper bound, since i+1 will be at most numberOfElements - 1 (one 
	// thread per internal node)
	if (index2 < 0 || index2 >= numberOfElements)
		return 0; 

	uint key2 = gSortedKeys[index2];

	if (key1 == key2)
		return 32 + clz(index1 ^ index2);
	
	return clz(int(key1 ^ key2));
}

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint numberOfTriangles = numTriangles;
	const uint leavesOffset = numberOfTriangles - 1;
	if (i >= leavesOffset)
		return;

	const uint key1 = gSortedKeys[i];

	const int lcp1 = longestCommonPrefix(numberOfTriangles, int(i), int(i) + 1, key1);
	const int lcp2 = longestCommonPrefix(numberOfTriangles, int(i), int(i) - 1, key1);

	const int direction = sign(lcp1 - lcp2);

	// Compute upper bound for the length of the range
	//const int minLcp = direction > 0 ? lcp2 : direction < 0 ? lcp1 : 32 + clz(int(i) ^ int(i));
	const int minLcp = longestCommonPrefix(numberOfTriangles, int(i), int(i) - direction, key1);
	int lMax = 128;
	while (longestCommonPrefix(numberOfTriangles, int(i), int(i) + lMax * direction, key1) > minLcp)
	{
		lMax *= 4;
	}

	// Find other end using binary search
	int l = 0;
	int t = lMax;
	while (t > 1)
	{
		t = t / 2;
		if (longestCommonPrefix(numberOfTriangles, int(i), int(i) + (l + t) * direction, key1) > minLcp)
		{
			l += t;
		}
	}
	const int j = int(i) + l * direction;

	// Find the split position using binary search
	const int nodeLcp = longestCommonPrefix(numberOfTriangles, int(i), j, key1);
	int s = 0;
	int divisor = 2;
	t = l;
	const int maxDivisor = 1 << (32 - clz(l));
	while (divisor <= maxDivisor)
	{
		t = (l + divisor - 1) / divisor;
		if (longestCommonPrefix(numberOfTriangles, int(i), int(i) + (s + t) * direction, key1) > nodeLcp)
		{
			s += t;
		}
		divisor *= 2;
	}
	const int splitPosition = int(i) + s * direction + min(direction, 0);
	
	int leftIndex;
	int rightIndex;
	const int min = min(int(i), j);
	const int max = max(int(i), j);

	// Update left child pointer
	if (min == splitPosition)
	{
		// Children is a leaf, add the number of internal nodes to the index
		int leafIndex = splitPosition + int(leavesOffset);
		leftIndex = leafIndex;

		// Set the leaf data index
		gRadixTree[leafIndex].field0.w = gSortedIndices[splitPosition];
	}
	else
	{
		leftIndex = splitPosition;
	}

	// Update right child pointer
	if (max == (splitPosition + 1))
	{
		// Children is a leaf, add the number of internal nodes to the index
		int leafIndex = splitPosition + 1 + int(leavesOffset);
		rightIndex = leafIndex;

		// Set the leaf data index
		gRadixTree[leafIndex].field0.w = gSortedIndices[splitPosition + 1];
	}
	else
	{
		rightIndex = splitPosition + 1;
	}

	// Update children indices
	gRadixTree[i].field0.y = leftIndex;
	gRadixTree[i].field0.z = rightIndex;
	gRadixTree[i].field1.w = splitPosition;
	gRadixTree[i].field2.w = max;

	// Set parent nodes
	gRadixTree[leftIndex].field0.x = i;
	gRadixTree[rightIndex].field0.x = i;

	gRadixTree[i].field0.w = 0xFFFFFFFF;

	// Set the parent of the root node to 0xFFFFFFFF
	if (i == 0)
	{
		gRadixTree[0].field0.x = 0xFFFFFFFF;
	}
}