#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define WG_SIZE 128

layout(local_size_x = WG_SIZE) in;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

struct CompactTreeNode
{
	uvec4 field0; // parent, left, right, triangleID
	vec4 field1; // boundMin.xyz, rightmostLeafInLeftSubtree
	vec4 field2; // boundMax.xyz, rightmostLeafInRightSubtree
};

struct Bound
{
	vec3 min;
	float pad0;
	vec3 max;
	float pad1;
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numPrimitives;
};

layout (std430, binding = 0) readonly buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 1) buffer RadixTree
{
    CompactTreeNode radixTree[];
};

layout (std430, binding = 2) buffer NodeCounter
{
    uint nodeCounter[];
};

void calculateNodeBoundingBox(inout Bound bound, const Bound leftBound, const Bound rightBound)
{
	bound.min = min(leftBound.min, rightBound.min);
	bound.max = max(leftBound.max, rightBound.max);
}

void calculateLeafBoundingBox(vec3 vertex1, vec3 vertex2, vec3 vertex3, inout Bound bound)
{
	bound.min = min(vertex1, min(vertex2, vertex3));
	bound.max = max(vertex1, max(vertex2, vertex3));
}

shared Bound sharedBounds[WG_SIZE];

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint numberOfPrimitives = numPrimitives;
	const uint firstThreadInBlock = gl_WorkGroupID.x * WG_SIZE;
	const uint lastThreadInBlock = firstThreadInBlock + WG_SIZE - 1;
	
	if (i >= numberOfPrimitives)
		return;

	const uint index = i + numberOfPrimitives - 1;
	const uint dataIndex = radixTree[index].field0.w;

	// Set leaves left and right indices
	radixTree[index].field0.y = 0xFFFFFFFF;
	radixTree[index].field0.z = 0xFFFFFFFF;
	
	CompactSphere sphere = spheres[dataIndex];
	Bound cacheBound;
	cacheBound.min = sphere.field0.xyz - sphere.field0.w;
	cacheBound.max = sphere.field0.xyz + sphere.field0.w;
	sharedBounds[gl_LocalInvocationID.x] = cacheBound;
	radixTree[index].field1.xyz = cacheBound.min;
	radixTree[index].field2.xyz = cacheBound.max;
	
	groupMemoryBarrier();

	uint lastNode = index;
	uint current = radixTree[index].field0.x;
	while (current != 0xFFFFFFFF)
	{
		// In the counters array, we have stored the id of the thread that processed the other
		// children of this node
		uint childThreadId = atomicExchange(nodeCounter[current], i);

		// The first thread to reach a node will just die
		if (childThreadId == 0xFFFFFFFF)
		{
			return;
		}

		Bound childBound;
		if (childThreadId >= firstThreadInBlock && childThreadId <= lastThreadInBlock)
		{
			// If both child nodes were processed by the same block, we can reuse the values
			// cached in shared memory
			uint childThreadIdInBlock = childThreadId - firstThreadInBlock;
			childBound = sharedBounds[childThreadIdInBlock];
		}
		else
		{
			// The children were processed in different blocks, so we have to find out if the one
			// that was not processed by this thread was the left or right one
			uint childIndex = radixTree[current].field0.y;
			if (childIndex == lastNode)
			{
				childIndex = radixTree[current].field0.z;
			}

			childBound.min = radixTree[childIndex].field1.xyz;
			childBound.max = radixTree[childIndex].field2.xyz;
		}
		
		groupMemoryBarrier();

		// Update node bounding box
		calculateNodeBoundingBox(cacheBound, cacheBound, childBound);
		sharedBounds[gl_LocalInvocationID.x] = cacheBound;
		radixTree[current].field1.xyz = cacheBound.min;
		radixTree[current].field2.xyz = cacheBound.max;
		
		groupMemoryBarrier();

		// Update last processed node
		lastNode = current;

		// Update current node pointer
		current = radixTree[current].field0.x;
	}
}