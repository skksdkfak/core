#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_arithmetic: enable
#extension GL_KHR_shader_subgroup_ballot: enable

#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define GROUP_MEM_FENCE groupMemoryBarrier()
#define AtomAdd(x, inc) atomicAdd(x, inc)

#define WG_SIZE 256
#define ELEMENTS_PER_WORK_GROUP 1024
#define ELEMENTS_PER_WORK_ITEM (ELEMENTS_PER_WORK_GROUP / WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1 << BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define nPerWI 16

#define nPerLane (nPerWI / 4)

layout(local_size_x = 1024) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) buffer Histogram
{
	uint wHistogram1[];
};

uint prefixScanVectorEx(inout uvec4 data)
{
	uint sum = 0;
	uint tmp = data.x;
	data.x = sum;
	sum += tmp;

	tmp = data.y;
	data.y = sum;
	sum += tmp;
	
	tmp = data.z;
	data.z = sum;
	sum += tmp;

	tmp = data.w;
	data.w = sum;
	sum += tmp;

	return sum;
}

shared uint ldsWarpSums[gl_WorkGroupSize.x / 32];

uint localPrefixSum(uint data)
{
	uint warpSum = subgroupExclusiveAdd(data);
    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        ldsWarpSums[gl_SubgroupID] = warpSum + data;
    }
	GROUP_LDS_BARRIER;

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		ldsWarpSums[gl_SubgroupInvocationID] = subgroupExclusiveAdd(ldsWarpSums[gl_SubgroupInvocationID]);
	}
	GROUP_LDS_BARRIER;

	uint prefixSum = warpSum + subgroupBroadcastFirst(ldsWarpSums[gl_SubgroupID]);
	GROUP_LDS_BARRIER;

	return prefixSum;
}

uvec4 localPrefixSum128V(uvec4 pData, uint lIdx, inout uint totalSum)
{
	uint s4 = prefixScanVectorEx(pData);
	uint rank = localPrefixSum(s4);
	return pData + uvec4(rank);
}

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	const uint nWGs = m_nWGs;

	uint data[nPerWI];
	uint histogramIdx;
	for (uint i = 0; i < nPerWI; i++)
	{
		data[i] = 0;
		histogramIdx = nPerWI * lIdx + i;
		if (histogramIdx < NUM_BUCKET * nWGs)
			data[i] = wHistogram1[histogramIdx];
	}

	uvec4 myData = uvec4(0, 0, 0, 0);

	for (uint i = 0; i < nPerLane; i++)
	{
		myData.x += data[nPerLane * 0 + i];
		myData.y += data[nPerLane * 1 + i];
		myData.z += data[nPerLane * 2 + i];
		myData.w += data[nPerLane * 3 + i];
	}

	uint totalSum;
	uvec4 scanned = localPrefixSum128V(myData, lIdx, totalSum);

	//	for(int j=0; j<4; j++) //	somehow it introduces a lot of branches
	{	int j = 0;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 1;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 2;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}
	{	int j = 3;
	uint sum = 0;
	for (int i = 0; i<nPerLane; i++)
	{
		uint tmp = data[nPerLane*j + i];
		data[nPerLane*j + i] = sum;
		sum += tmp;
	}
	}

	for (int i = 0; i<nPerLane; i++)
	{
		data[nPerLane * 0 + i] += scanned.x;
		data[nPerLane * 1 + i] += scanned.y;
		data[nPerLane * 2 + i] += scanned.z;
		data[nPerLane * 3 + i] += scanned.w;
	}

	for (int i = 0; i < nPerWI; i++)
	{
		histogramIdx = nPerWI * lIdx + i;
		if (histogramIdx < NUM_BUCKET * nWGs)
			wHistogram1[histogramIdx] = data[i];
	}
}