#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_arithmetic: enable
#extension GL_KHR_shader_subgroup_ballot: enable
#extension GL_KHR_shader_subgroup_shuffle: enable

#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define GROUP_MEM_FENCE groupMemoryBarrier()
#define AtomInc(x) atomicAdd(x, 1)
#define AtomAdd(x, inc) atomicAdd(x, inc)

#define WG_SIZE 256
#define ELEMENTS_PER_WORK_GROUP 1024
#define ELEMENTS_PER_WORK_ITEM (ELEMENTS_PER_WORK_GROUP / WG_SIZE)
#define BITS_PER_PASS 8
#define NUM_BUCKET (1 << BITS_PER_PASS)
#define BITS_PER_LOCAL_SORT_PASS 2
#define NUM_LOCAL_SORT_BUCKET (1 << BITS_PER_LOCAL_SORT_PASS)

layout(local_size_x = WG_SIZE) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer Histogram
{
	uint rHistogram[];
};

layout(std430, binding = 2) readonly buffer SrcKeys
{
    uint gSrcKey[];
};

layout(std430, binding = 3) readonly buffer SrcValues
{
    uint gSrcVal[];
};

layout(std430, binding = 4) writeonly buffer DstKeys
{
    uint gDstKey[];
};

layout(std430, binding = 5) writeonly buffer DstValues
{
    uint gDstValue[];
};

shared uint ldsWarpSums[gl_WorkGroupSize.x / 32];

uint localPrefixSum(uint data, inout uint totalSum)
{
	const uint warpSum = subgroupExclusiveAdd(data);
    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        ldsWarpSums[gl_SubgroupID] = warpSum + data;
    }
	GROUP_LDS_BARRIER;

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		ldsWarpSums[gl_SubgroupInvocationID] = subgroupExclusiveAdd(ldsWarpSums[gl_SubgroupInvocationID]);
	}
	GROUP_LDS_BARRIER;

	const uint prefixSum = warpSum + subgroupBroadcastFirst(ldsWarpSums[gl_SubgroupID]);
	GROUP_LDS_BARRIER;

	if (gl_LocalInvocationIndex.x == gl_WorkGroupSize.x - 1)
	{
		ldsWarpSums[0] = prefixSum + data;
	}
	GROUP_LDS_BARRIER;

	totalSum = subgroupBroadcastFirst(ldsWarpSums[0]);
	GROUP_LDS_BARRIER;
	
	return prefixSum;
}

uint localPrefixSum(uint data)
{
	uint warpSum = subgroupExclusiveAdd(data);
    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        ldsWarpSums[gl_SubgroupID] = warpSum + data;
    }
	GROUP_LDS_BARRIER;

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		ldsWarpSums[gl_SubgroupInvocationID] = subgroupExclusiveAdd(ldsWarpSums[gl_SubgroupInvocationID]);
	}
	GROUP_LDS_BARRIER;

	uint prefixSum = warpSum + subgroupBroadcastFirst(ldsWarpSums[gl_SubgroupID]);
	GROUP_LDS_BARRIER;

	return prefixSum;
}

shared uint localHistogram[WG_SIZE * NUM_LOCAL_SORT_BUCKET];
shared uint ldsSortIdx[WG_SIZE * ELEMENTS_PER_WORK_ITEM];
shared uint ldsSortKey[WG_SIZE * ELEMENTS_PER_WORK_ITEM];
//shared uint ldsSortVal[WG_SIZE * ELEMENTS_PER_WORK_ITEM];

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;

	const uint localOffset = ELEMENTS_PER_WORK_ITEM * lIdx;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM * WG_SIZE;

	uint nBlocks = nAligned / blockSize - nBlocksPerWG * wgIdx;

	uint addr = blockSize * nBlocksPerWG * wgIdx + localOffset;
	
	uint sortIdxData[ELEMENTS_PER_WORK_ITEM];
	uint sortData[ELEMENTS_PER_WORK_ITEM];
	for (uint iblock = 0; iblock < min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
		{
			ldsSortIdx[localOffset + i] = sortIdxData[i] = localOffset + i;
			ldsSortKey[localOffset + i] = (addr + i < n) ? gSrcKey[addr + i] : 0xffffffff;
			//ldsSortVal[localOffset + i] = (addr + i < n) ? gSrcVal[addr + i] : 0xffffffff;
		}
		GROUP_LDS_BARRIER;
		
		const uint dstAddr = 4 * lIdx;
		for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += BITS_PER_LOCAL_SORT_PASS)
		{
			for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
			{
				localHistogram[i * WG_SIZE + lIdx] = 0;
			}
			GROUP_LDS_BARRIER;
		
			for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
			{
				sortData[i] = (ldsSortKey[sortIdxData[i]] >> (startBit + ibit)) & 0x3;
				++localHistogram[sortData[i] * WG_SIZE + lIdx];
			}
			GROUP_LDS_BARRIER;
			
			uint sum = 0;
			uint data[NUM_LOCAL_SORT_BUCKET];
			for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
			{
				uint tmp = localHistogram[lIdx * NUM_LOCAL_SORT_BUCKET + i];
//				data[i] = tmp; // was wrong, check it if needed
				data[i] = sum;
				sum += tmp;
			}

			sum = localPrefixSum(sum);

			for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
			{
				localHistogram[lIdx * NUM_LOCAL_SORT_BUCKET + i] = data[i] + sum;
			}
			GROUP_LDS_BARRIER;
		
			for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
			{
				uint dst_offset = localHistogram[sortData[i] * WG_SIZE + lIdx]++;
				ldsSortIdx[dst_offset] = sortIdxData[i];
			}
			GROUP_LDS_BARRIER;
		
			for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
			{
				sortIdxData[i] = ldsSortIdx[dstAddr + i];
			}
			GROUP_LDS_BARRIER;
		}
		
		for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
		{
			// TODO: CHECK BOUNDARY
			gDstKey[addr + i] = ldsSortKey[sortIdxData[i]];
			gDstValue[addr + i] = (addr + sortIdxData[i] < n) ? gSrcVal[addr + sortIdxData[i]] : 0xffffffff;
		}
		GROUP_LDS_BARRIER;
	}
}