#version 450 core

layout(location = 0) in vec4 input_position;
layout(location = 1) in vec4 input_uv;
layout(location = 2) in uint input_image_index;
layout(location = 3) in float input_depth;

layout(location = 0) out vec2 output_uv;
layout(location = 1) flat out uint output_image_index;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main(void)
{
	const uint x = gl_VertexIndex & 1;
	const uint y = ((gl_VertexIndex >> 1) & 1) + 2;

	gl_Position = vec4(input_position[x], input_position[y], input_depth, 1.0);
	output_uv = vec2(input_uv[x], input_uv[y]);
	output_image_index = input_image_index;
}