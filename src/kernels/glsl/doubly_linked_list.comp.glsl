#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define WG_SIZE 128
#define INTERSECT_STACK_SIZE 128

layout(local_size_x = WG_SIZE) in;

struct DoublyLinkedListNode
{
	uint prev;
	uint next;
	uint val;
};

layout (std430, binding = 0) buffer DoublyLinkedList
{
	DoublyLinkedListNode doublyLinkedList[];
};

uint GetNodeIndex(in uint node)
{
	return node & ~0x80000000;
}

bool IsNodeMarked(in uint node)
{
	return bool(node >> 31);
}

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	//if (i == 0 || i == 16777215)
	if (i == 0 || i == 3 || i == 4 || i == 28 || i == 29 || i == 30 || i == 32 || i == 48 || i == 16777215)
	//if (!(i == 3 || i == 4 || i == 5))
		return;

	uint link, old, prev, next = atomicXor(doublyLinkedList[i].next, 0x80000000), curr = i;
	if (IsNodeMarked(next))
		return;
	while (true)
	{
		prev = atomicOr(doublyLinkedList[curr].prev, 0x80000000);

		link = curr;
		do
		{
			old = link;
			link = atomicCompSwap(doublyLinkedList[prev].next, old, curr | 0x80000000u);
			if (link == (curr | 0x80000000u) || (!IsNodeMarked(link) && link != curr))
				return;
		} while (link != old);

		old = curr;
		while (old != atomicCompSwap(doublyLinkedList[next].prev, old, prev))
		{
			old = next;
			next = GetNodeIndex(doublyLinkedList[next].next);
		}

		link = curr | 0x80000000u;
		do
		{
			old = link;
			link = atomicCompSwap(doublyLinkedList[prev].next, old, next);
			if (GetNodeIndex(link) == next || IsNodeMarked(link))
				return;
		} while (link != old);

		old = curr;
		curr = prev;
	}
}