float copysign(float x, float y)
{
	return abs(x) * sign(y);
}