
const uint gpu_log_descriptor_set = 1;

layout(std430, set = gpu_log_descriptor_set, binding = 0) buffer gpu_log_buffer_t
{
	uint gpu_log_buffer[];
};

layout(std430, set = gpu_log_descriptor_set, binding = 1) buffer gpu_log_counter_buffer_t
{
	uint gpu_log_counter_buffer;
};

void set_pixel_index(uvec2 pixel_index)
{
	//pixel_index = pixel_index;
}

void gpu_log_print(float value)
{
	uint log_index = atomicAdd(gpu_log_counter_buffer, 1);
	gpu_log_buffer[log_index] = floatBitsToUint(value);
}