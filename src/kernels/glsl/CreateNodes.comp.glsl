#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define WG_SIZE 64

layout(local_size_x = WG_SIZE) in;

struct CompactTreeNode
{
	uvec4 field0; // parent, left, right, triangleID
	vec4 field1; // boundMin.xyz, cost
	vec4 field2; // boundMax.xyz, area
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numTriangles;
};

layout (std430, binding = 0) buffer RadixTree
{
    CompactTreeNode radixTreeNodes[];
};

layout (set = 0, binding = 1, rgba32f) uniform imageBuffer nodes;

void main()
{
	const uint threadIndex = gl_GlobalInvocationID.x;
	const uint offset = numTriangles - 1;
	if (threadIndex >= offset)
		return;

	ivec2 cidx = { int(radixTreeNodes[threadIndex].field0.y), int(radixTreeNodes[threadIndex].field0.z) };
	vec3 leftMin = radixTreeNodes[cidx[0]].field1.xyz;
	vec3 leftMax = radixTreeNodes[cidx[0]].field2.xyz;
	vec3 rightMin = radixTreeNodes[cidx[1]].field1.xyz;
	vec3 rightMax = radixTreeNodes[cidx[1]].field2.xyz;

	const int nodeIndex = int(threadIndex) * 4;
	imageStore(nodes, nodeIndex + 0, vec4(leftMin.x, leftMax.x, leftMin.y, leftMax.y));
	imageStore(nodes, nodeIndex + 1, vec4(rightMin.x, rightMax.x, rightMin.y, rightMax.y));
	imageStore(nodes, nodeIndex + 2, vec4(leftMin.z, leftMax.z, rightMin.z, rightMax.z));
	imageStore(nodes, nodeIndex + 3, vec4(cidx[0] < offset ? intBitsToFloat(cidx[0] * 4) : uintBitsToFloat(~(radixTreeNodes[cidx[0]].field0.w)), cidx[1] < offset ? intBitsToFloat(cidx[1] * 4) : uintBitsToFloat(~(radixTreeNodes[cidx[1]].field0.w)), 0, 0));
}