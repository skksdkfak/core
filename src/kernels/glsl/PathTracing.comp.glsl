#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_ARB_shader_ballot : require
#extension GL_ARB_shader_group_vote : require
#extension GL_EXT_debug_printf : enable

layout (local_size_x = 16, local_size_y = 16) in;

#define INFINITY 1e10
#define PI 3.1415926536f
#define EntrypointSentinel 0x76543210
#define INTERSECT_STACK_SIZE 64

struct AABB
{
	vec3 min;
	vec3 max;
};

struct Ray
{
	vec3 origin;
	float tmin;
	vec3 direction;
	float tmax;
};

struct Hit
{
	uint primitiveID;
	float distance;
	vec2 barycentricUV;
};

struct CompactTreeNode
{
	uvec4 field0; // parent, left, right, triangleID
	vec4 field1; // boundMin.xyz, rightmostLeafInLeftSubtree
	vec4 field2; // boundMax.xyz, rightmostLeafInRightSubtree
};

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

layout (std140, push_constant) uniform PushConsts 
{
	uint samples;
	uint bounces;
	uint primitiveCount;
};

layout (binding = 0) uniform Camera
{
	mat4 viewMatrix;
	mat4 projMatrix;
	mat4 viewProjMatrix;
	mat4 invViewProjMatrix;
	vec4 position;
	vec4 direction;
	float nearClipDistance;
	float farClipDistance;
	float nearFarClipDistance;
};

layout (binding = 1) uniform Various
{
	vec4	vTime;
	vec4	vSinTime;
	vec4	vCosTime;
	vec4	vDeltaTime;
	vec4	vScreenResolution;
};

layout(std430, binding = 2) readonly buffer RadixTree
{
    CompactTreeNode radixTreeNodes[];
};

layout (std430, binding = 3) readonly buffer Spheres
{
    CompactSphere spheres[];
};

layout (binding = 4, rgba8) uniform writeonly image2D resultImage;

layout (set = 0, binding = 5, rgba32f) uniform imageBuffer nodes;

float random(vec2 p)
{
	return fract(cos(dot(p, vec2(23.14069263277926, 2.665144142690225)))*123456.);
}

bool RayBoxIntersection(vec3 ray_pos, in vec3 inv_dir, in AABB box, inout float mint, inout float maxt)
{
	const vec3 n = (box.min - ray_pos) * inv_dir;
	const vec3 f = (box.max - ray_pos) * inv_dir;

	const vec3 tmin = min(f, n);
	const vec3 tmax = max(f, n);

	mint = max(tmin.x, max(tmin.y, tmin.z));
	maxt = min(tmax.x, min(tmax.y, tmax.z));

	return (mint <= maxt) && (maxt > 0.f);
}

float IntersectSphere(in Ray r, in vec3 pos, in float rad)
{
	vec3 op = pos - r.origin;   
	float t, epsilon = 0.01f;
	float b = dot(op, r.direction);
	float disc = b*b - dot(op, op) + rad*rad; // discriminant of quadratic formula
	if (disc < 0)
		return 0;
	else
		disc = sqrt(disc);

	t = b - disc;
	if (t > epsilon)
	{
		return t;
		}
		else
		{
			t = b + disc;

			if (t > epsilon)
			return t;
			else
			0.0f;
		}
}

#define INFINITY 1e10
#define PI 3.1415926536f
#define EntrypointSentinel 0x76543210
#define STACK_SIZE 64

float copysignf(float x, float y)
{
	return x * y < 0 ? -1.0f : 1.0f;
}

float max4(float a, float b, float c, float d)
{
	return max(max(max(a, b), c), d);
}

float min4(float a, float b, float c, float d)
{
	return min(min(min(a, b), c), d);
}

void swap_int(inout int a, inout int b)
{
	int t = a;
	a = b;
	b = t;
}

bool intersectBVH(in Ray r, inout Hit hit)
{
	const bool anyHit = false;

	// Traversal stack in CUDA thread-local memory.
	int traversalStack[STACK_SIZE];

	// Live state during traversal, stored in registers.
	int     rayidx;                 // Ray index.
	float   origx, origy, origz;    // Ray origin.
	float   dirx, diry, dirz;       // Ray direction.
	float   tmin;                   // t-value from which the ray starts. Usually 0.
	float   idirx, idiry, idirz;    // 1 / dir
	float   oodx, oody, oodz;       // orig / dir

	int		stackPtr;               // Current position in traversal stack.
	int     leafAddr;               // First postponed leaf, non-negative if none.
	int     nodeAddr;               // Non-negative: current internal node, negative: second postponed leaf.
	int     hitIndex;               // Triangle index of the closest intersection, -1 if none.
	float   hitT;                   // t-value of the closest intersection.

	// Initialize.
	{
		// Fetch ray.
		origx = r.origin.x, origy = r.origin.y, origz = r.origin.z;
		dirx = r.direction.x, diry = r.direction.y, dirz = r.direction.z;
		tmin = 0.0f;

		float ooeps = exp2(-80.0f); // Avoid div by zero.
		idirx = 1.0f / (abs(r.direction.x) > ooeps ? r.direction.x : copysignf(ooeps, r.direction.x));
		idiry = 1.0f / (abs(r.direction.y) > ooeps ? r.direction.y : copysignf(ooeps, r.direction.y));
		idirz = 1.0f / (abs(r.direction.z) > ooeps ? r.direction.z : copysignf(ooeps, r.direction.z));
		oodx = origx * idirx, oody = origy * idiry, oodz = origz * idirz;

		// Setup traversal.
		traversalStack[0] = EntrypointSentinel; // Bottom-most entry.
		stackPtr = 0;
		leafAddr = 0;   // No postponed leaf.
		nodeAddr = 0;   // Start from the root.
		hitIndex = -1;  // No triangle intersected so far.
		hitT = INFINITY; // tmax
	}

	// Traversal loop.
	while (nodeAddr != EntrypointSentinel)
	{
		// Traverse internal nodes until all SIMD lanes have found a leaf.
		bool searchingLeaf = true;
		//while (nodeAddr >= 0 && nodeAddr != EntrypointSentinel)
		while (uint(nodeAddr) < uint(EntrypointSentinel))   // functionally equivalent, but faster
		{
			// Fetch AABBs of the two child nodes.
			vec4 n0xy = imageLoad(nodes, nodeAddr); // (c0.lo.x, c0.hi.x, c0.lo.y, c0.hi.y)
			vec4 n1xy = imageLoad(nodes, nodeAddr + 1); // (c1.lo.x, c1.hi.x, c1.lo.y, c1.hi.y)
			vec4 nz = imageLoad(nodes, nodeAddr + 2); // (c0.lo.z, c0.hi.z, c1.lo.z, c1.hi.z)

			// Intersect the ray against the child nodes.
			float c0lox = n0xy.x * idirx - oodx;
			float c0hix = n0xy.y * idirx - oodx;
			float c0loy = n0xy.z * idiry - oody;
			float c0hiy = n0xy.w * idiry - oody;
			float c0loz = nz.x   * idirz - oodz;
			float c0hiz = nz.y   * idirz - oodz;
			float c1loz = nz.z   * idirz - oodz;
			float c1hiz = nz.w   * idirz - oodz;
			float c0min = max4(tmin, min(c0lox, c0hix), min(c0loy, c0hiy), min(c0loz, c0hiz));
			float c0max = min4(hitT, max(c0lox, c0hix), max(c0loy, c0hiy), max(c0loz, c0hiz));
			float c1lox = n1xy.x * idirx - oodx;
			float c1hix = n1xy.y * idirx - oodx;
			float c1loy = n1xy.z * idiry - oody;
			float c1hiy = n1xy.w * idiry - oody;
			float c1min = max4(tmin, min(c1lox, c1hix), min(c1loy, c1hiy), min(c1loz, c1hiz));
			float c1max = min4(hitT, max(c1lox, c1hix), max(c1loy, c1hiy), max(c1loz, c1hiz));

			bool traverseChild0 = (c0max >= c0min);
			bool traverseChild1 = (c1max >= c1min);

			// Neither child was intersected => pop stack.
			if (!traverseChild0 && !traverseChild1)
			{
				nodeAddr = traversalStack[stackPtr];
				stackPtr--;
			}
			// Otherwise => fetch child pointers.
			else
			{
				ivec2 cnodes = floatBitsToInt(imageLoad(nodes, nodeAddr + 3).xy);
				nodeAddr = (traverseChild0) ? cnodes.x : cnodes.y;

				// Both children were intersected => push the farther one.
				if (traverseChild0 && traverseChild1)
				{
					if (c1min < c0min)
						swap_int(nodeAddr, cnodes.y);
					stackPtr++;
					traversalStack[stackPtr] = cnodes.y;
				}
			}

			// First leaf => postpone and continue traversal.
			if (nodeAddr < 0 && leafAddr >= 0)
			{
				searchingLeaf = false;
				leafAddr = nodeAddr;
				nodeAddr = traversalStack[stackPtr];
				stackPtr--;
			}

			// All SIMD lanes have found a leaf => process them.
			if (!anyInvocationARB(searchingLeaf))
				break;
		}

		// Process postponed leaf nodes.
		while (leafAddr < 0)
		{
			// Intersect the ray against each triangle using Sven Woop's algorithm.
			//for (int triAddr = ~leafAddr;; triAddr += 3)
			int triAddr = ~leafAddr;
			{
				const CompactSphere sphere = spheres[triAddr];
				float hitSphereDist = IntersectSphere(r, sphere.field0.xyz, sphere.field0.w);
				if ((hitSphereDist > tmin) && (hitSphereDist < hitT))
				{
					hit.distance = hitT = hitSphereDist;
					hit.primitiveID = hitIndex = triAddr;
					if (hitSphereDist == 123123.414243)
					debugPrintfEXT("%f is a negative integer", hitSphereDist);
				}
			} // triangle

			  // Another leaf was postponed => process it as well.
			leafAddr = nodeAddr;
			if (nodeAddr < 0)
			{
				nodeAddr = traversalStack[stackPtr];
				stackPtr--;
			}
		} // leaf
	} // traversal

	return (hitIndex != -1);
}

bool intersectBVH1(in Ray r, inout Hit hit)
{
	uint stack[INTERSECT_STACK_SIZE];
	int ptr = 0;
	stack[ptr++] = 0xFFFFFFFF;
	uint idx = 0;
	const vec3 invdir = 1.0f / r.direction;
	hit.distance = INFINITY;
	const uint leafs = primitiveCount;
	const float scalingFactor = 0.00f;

	uint primitiveID;
	AABB lbox;
	AABB rbox;
	float leftMin;
	float rightMin;
	float leftMax;
	float rightMax;
	CompactTreeNode node;
	while (idx != 0xFFFFFFFF)
	{
		node = radixTreeNodes[idx];
		if (idx < leafs)
		{
			lbox.min = radixTreeNodes[node.field0.y].field1.xyz - scalingFactor;
			lbox.max = radixTreeNodes[node.field0.y].field2.xyz + scalingFactor;
			rbox.min = radixTreeNodes[node.field0.z].field1.xyz - scalingFactor;
			rbox.max = radixTreeNodes[node.field0.z].field2.xyz + scalingFactor;

			bool traverseLeftChild = RayBoxIntersection(r.origin, invdir, lbox, leftMin, leftMax);
			bool traverseRightChild = RayBoxIntersection(r.origin, invdir, rbox, rightMin, rightMax);
			traverseLeftChild = traverseLeftChild && (hit.distance >= leftMin);
			traverseRightChild = traverseRightChild && (hit.distance >= rightMin);

			if (traverseLeftChild && traverseRightChild)
			{
				uint deferred;
				if (leftMin > rightMin)
				{
					idx = node.field0.z;
					deferred = node.field0.y;
				}
				else
				{
					idx = node.field0.y;
					deferred = node.field0.z;
				}
				stack[ptr++] = deferred;
				continue;
			}
			else if (traverseLeftChild)
			{
				idx = node.field0.y;
				continue;
			}
			else if (traverseRightChild)
			{
				idx = node.field0.z;
				continue;
			}
		}
		else
		{
			primitiveID = node.field0.w;
			const CompactSphere sphere = spheres[primitiveID];
			float hitSphereDist = IntersectSphere(r, sphere.field0.xyz, sphere.field0.w + scalingFactor);
			if ((hitSphereDist > 0.01f) && (hitSphereDist < hit.distance))
			{
				hit.distance = hitSphereDist;
				hit.primitiveID = primitiveID;

				if (hitSphereDist == 123123.414243)
					debugPrintfEXT("%f is a negative integer", hitSphereDist);
			}
		}
		idx = stack[--ptr];
	}

	return hit.distance < INFINITY;
}

bool intersectBVH2(in Ray r, inout Hit hit)
{
	bool intersected = false;
	uint stack[INTERSECT_STACK_SIZE];
	int ptr = 0;
	uint idx = 0;
	const vec3 invdir = 1.0f / r.direction;
	hit.distance = INFINITY;
	const uint leafs = primitiveCount - 1;
	const float scalingFactor = 0.00f;

	uint primitiveID;
	AABB lbox;
	AABB rbox;
	float leftMin;
	float rightMin;
	float leftMax;
	float rightMax;
	CompactTreeNode node;
	while (true)
	{
		node = radixTreeNodes[idx];
		if (idx < leafs)
		{
			lbox.min = radixTreeNodes[node.field0.y].field1.xyz - scalingFactor;
			lbox.max = radixTreeNodes[node.field0.y].field2.xyz + scalingFactor;
			rbox.min = radixTreeNodes[node.field0.z].field1.xyz - scalingFactor;
			rbox.max = radixTreeNodes[node.field0.z].field2.xyz + scalingFactor;

			bool traverseLeftChild = RayBoxIntersection(r.origin, invdir, lbox, leftMin, leftMax);
			bool traverseRightChild = RayBoxIntersection(r.origin, invdir, rbox, rightMin, rightMax);
			traverseLeftChild = traverseLeftChild && (hit.distance >= leftMin);
			traverseRightChild = traverseRightChild && (hit.distance >= rightMin);

			if (traverseLeftChild && traverseRightChild)
			{
				uint deferred;
				if (leftMin > rightMin)
				{
					idx = node.field0.z;
					deferred = node.field0.y;
				}
				else
				{
					idx = node.field0.y;
					deferred = node.field0.z;
				}
				stack[ptr++] = deferred;
				continue;
			}
			else if (traverseLeftChild)
			{
				idx = node.field0.y;
				continue;
			}
			else if (traverseRightChild)
			{
				idx = node.field0.z;
				continue;
			}
		}
		else
		{
			primitiveID = node.field0.w;
			const CompactSphere sphere = spheres[primitiveID];
			float hitSphereDist = IntersectSphere(r, sphere.field0.xyz, sphere.field0.w + scalingFactor);
			if ((hitSphereDist > 0.01f) && (hitSphereDist < hit.distance))
			{
				hit.distance = hitSphereDist;
				hit.primitiveID = primitiveID;
				intersected = true;
			}
		}

		if (ptr == 0)
		{
			break;
		}

		idx = stack[--ptr];
	}

	return intersected;
}

void main()
{
	const uint screenWidth = uint(vScreenResolution.x);
	const uint screenHeight = uint(vScreenResolution.y);
	if (gl_GlobalInvocationID.x >= screenWidth || gl_GlobalInvocationID.y >= screenHeight)
		return;

	const uint thxId = gl_GlobalInvocationID.x + (screenWidth * gl_GlobalInvocationID.y);

	vec2 uv = { float(gl_GlobalInvocationID.x) / screenWidth, float(gl_GlobalInvocationID.y) / screenHeight };
	uv = 2.0 * uv - 1.0;
	uv.y = -uv.y;
	vec4 rayDirection = invViewProjMatrix * vec4(uv, 0.0f, 1.0f);
	rayDirection /= rayDirection.w;

	vec4 origin = vec4(position.xyz, 0.0f);
	vec4 direction = vec4(normalize(rayDirection.xyz - position.xyz), INFINITY);

	vec3 accucolor = vec3(0.0f);

	Hit hit;
	for (int s = 0; s < samples; s++)
	{
		Ray ray;
		ray.origin = origin.xyz;
		ray.direction = direction.xyz;
		ray.tmin = origin.w;
		ray.tmax = direction.w;

		vec3 mask = vec3(1.0f);

		for (int b = 0; b < bounces; b++)
		{
			if (!intersectBVH(ray, hit))
			{
				vec3 emit = vec3(1.0f, 1.0f, 1.0f);
				accucolor += mask * emit;
				break;
			}
			vec3 unitVector;
			float e = 1.0f;
			float f = 2 * PI * random(gl_GlobalInvocationID.xy + vCosTime.w + b);
			float cosTheta = pow(1.0f - random(gl_GlobalInvocationID.xy + vCosTime.w + b + 1), 1.0f / (e + 1.0f));
			float sinTheta = sqrt(1.0f - cosTheta * cosTheta);
			unitVector.x = sinTheta * cos(f);
			unitVector.y = sinTheta * sin(f);
			unitVector.z = cosTheta;

			ray.origin += ray.direction * hit.distance;
			CompactSphere sphere = spheres[hit.primitiveID];
			vec3 n = normalize(vec3(ray.origin.x - sphere.field0.x, ray.origin.y - sphere.field0.y, ray.origin.z - sphere.field0.z));
			vec3 w = normalize(dot(n, ray.direction) < 0 ? n : n * -1);
			vec3 u = normalize(cross((abs(w.x) > .1 ? vec3(0, 1, 0) : vec3(1, 0, 0)), w));
			vec3 v = cross(w, u);
			ray.direction = u * unitVector.x + v * unitVector.y + w * unitVector.z;
			ray.origin += ray.direction * 0.001f;

			vec3 emit = vec3(0.0f, 0.0f, 0.0f);
			vec4 diffuse = sphere.field1/*vec4(1.0f, 1.0f, 1.0f, 1.0f)*/;

			accucolor += mask * emit;
			mask *= diffuse.rgb;
		}
	}

	imageStore(resultImage, ivec2(gl_GlobalInvocationID.xy), vec4(accucolor / samples, 0.0));
}