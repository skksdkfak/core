#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_arithmetic: enable
#extension GL_KHR_shader_subgroup_ballot: enable
#extension GL_KHR_shader_subgroup_shuffle: enable
#extension GL_KHR_shader_subgroup_shuffle_relative: enable
#extension GL_AMD_gpu_shader_int16: enable
#extension GL_EXT_control_flow_attributes: enable
#extension GL_EXT_shader_16bit_storage : enable
#extension GL_GOOGLE_include_directive : enable

#include "hybrid_radix_sort.glsl"

#define RADIX_BITS 4
#define NUM_LOCAL_SORT_BUCKET (1 << RADIX_BITS)

#define PACKING_RATIO 32 / 16
#define LOG_PACKING_RATIO 1

#define LOG_COUNTER_LANES (RADIX_BITS - LOG_PACKING_RATIO)
#define COUNTER_LANES (1 << LOG_COUNTER_LANES)

// The number of packed counters per thread (plus one for padding)
#define PADDED_COUNTER_LANES (COUNTER_LANES + 1)
#define RAKING_SEGMENT PADDED_COUNTER_LANES

layout(local_size_x = WG_SIZE) in;

layout(binding = 0) uniform Consts
{
	uint size;
	uint nWGs;
	uint startBit;
	uint nBuckets;
	uint nScanBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer BucketAssignments
{
	BucketAssignment gBucketAssignments[];
};

layout(std430, binding = 2) buffer SrcKeys
{
	uint src_keys[];
};

layout(std430, binding = 3) writeonly buffer DstKeys
{
	uint dst_keys[];
};

layout(std430, binding = 4) buffer src_values_buffer
{
	uint src_values[];
};

layout(std430, binding = 5) writeonly buffer dst_values_buffer
{
	uint dst_values[];
};

shared uint ldsWarpAggregates[gl_WorkGroupSize.x / 32];

uint blockPrefixPackedCounter(const uint block_aggregate)
{
	uint block_prefix = 0;

	// Propagate totals in packed fields
	[[unroll]]
	for (uint i = 1; i < PACKING_RATIO; ++i)
	{
		block_prefix += block_aggregate << (16 * i);
	}

	return block_prefix;
}

uint localPrefixSum(uint data)
{
	const uint inclusive_sum = subgroupInclusiveAdd(data);
	const uint exclusive_sum = inclusive_sum - data;
	// Last lane in each warp shares its warp-aggregate
	if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
	{
		ldsWarpAggregates[gl_SubgroupID] = inclusive_sum;
	}

	groupMemoryBarrier();
	barrier();

	uint block_prefix = 0;
	if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < gl_WorkGroupSize.x / gl_SubgroupSize)
	{
		uint warp_sum = ldsWarpAggregates[gl_SubgroupInvocationID];

		[[unroll]]
		for (uint i = 1; i <= gl_WorkGroupSize.x / gl_SubgroupSize; i <<= 1)
		{
			uint n = subgroupShuffleUp(warp_sum, i);
			if (gl_SubgroupInvocationID >= i)
				warp_sum += n;
		}

		if (gl_SubgroupInvocationID == gl_WorkGroupSize.x / gl_SubgroupSize - 1)
		{
			block_prefix = blockPrefixPackedCounter(warp_sum);
		}

		ldsWarpAggregates[gl_SubgroupInvocationID] = warp_sum + subgroupShuffle(block_prefix, gl_WorkGroupSize.x / gl_SubgroupSize - 1);
	}

	groupMemoryBarrier();
	barrier();

	return exclusive_sum + (gl_SubgroupID > 0 ? ldsWarpAggregates[gl_SubgroupID - 1] : subgroupShuffle(block_prefix, gl_WorkGroupSize.x / gl_SubgroupSize - 1));
}

shared uint lds_histogram[WG_SIZE * RAKING_SEGMENT];
shared uint lds_keys[WG_SIZE * KEYS_PER_THREAD];
shared uint lds_values[WG_SIZE * KEYS_PER_THREAD];

void main()
{
	const uint lIdx = gl_LocalInvocationID.x;
	const uint wgIdx = gl_WorkGroupID.x;
	const BucketAssignment bucketAssignment = gBucketAssignments[wgIdx];
	uint keys[KEYS_PER_THREAD];
	uint values[KEYS_PER_THREAD];
	uint ranks[KEYS_PER_THREAD];
	uint thread_prefixes[KEYS_PER_THREAD]; // For each key, the count of previous keys in this tile having the same digit
	uint digit_counters[KEYS_PER_THREAD]; // For each key, the byte-offset of its corresponding digit counter in smem
	uint cached_segment[RAKING_SEGMENT]; // Copy of raking segment, promoted to registers

	// Load a linear segment of items into a warp-striped arrangement across the thread block
	#pragma unroll
	for (uint i = 0; i < KEYS_PER_THREAD; i++)
	{
		const uint item_offset = i * WG_SIZE + lIdx;
		if (item_offset < bucketAssignment.k_count)
		{
			const uint src_addr = bucketAssignment.k_offs + item_offset;
			lds_keys[item_offset] =  src_keys[src_addr];
			lds_values[item_offset] = src_values[src_addr];
		}
		else
		{
			lds_keys[item_offset] = 0xffffffffu;
		}
	}

	//region Transposes data items from warp-striped arrangement to blocked arrangement
	subgroupBarrier();

	#pragma unroll
	for (uint i = 0; i < KEYS_PER_THREAD; i++)
	{
		const uint item_offset = lIdx * KEYS_PER_THREAD + i;
		keys[i] = lds_keys[item_offset];
		values[i] = lds_values[item_offset];
	}
	//endregion

	uint begin_bit = 0;
	while (true)
	{
		//region Reset shared memory digit counters
		#pragma unroll
		for (uint i = 0; i < PADDED_COUNTER_LANES; ++i)
		{
			lds_histogram[i * WG_SIZE + lIdx] = 0;
		}
		//endregion

		groupMemoryBarrier();
		barrier();

		for (uint i = 0; i < KEYS_PER_THREAD; ++i)
		{
			const uint digit = (keys[i] >> begin_bit) & 0xfu;
			const uint sub_counter = digit >> LOG_COUNTER_LANES;
			const uint counter_lane = digit & (COUNTER_LANES - 1u);

			const uint raking_segment_lane = (counter_lane * WG_SIZE) + lIdx;
			const uint packed_counter = lds_histogram[raking_segment_lane];
			digit_counters[i] = (raking_segment_lane << 1) | sub_counter;
			thread_prefixes[i] = (packed_counter >> (sub_counter * 16)) & 0xffffu;
			lds_histogram[raking_segment_lane] = packed_counter + (1 << (sub_counter * 16));
		}

		groupMemoryBarrier();
		barrier();

		//region Performs upsweep raking reduction
		uint raking_partial = 0;
		#pragma unroll
		for (int i = 0; i < RAKING_SEGMENT; ++i)
		{
			cached_segment[i] = lds_histogram[lIdx * RAKING_SEGMENT + i];
			raking_partial += cached_segment[i];
		}
		//endregion

		raking_partial = localPrefixSum(raking_partial);

		//region Downsweep scan with exclusive partial
		uint inclusive;
		uint exclusive = raking_partial;
		//[[unroll]]
		for (int i = 0; i < RAKING_SEGMENT; ++i)
		{
			inclusive = exclusive + cached_segment[i];
			cached_segment[i] = exclusive;
			exclusive = inclusive;
			lds_histogram[lIdx * RAKING_SEGMENT + i] = cached_segment[i];
		}
		//endregion

		groupMemoryBarrier();
		barrier();

		//region Extract the local ranks of each key
		#pragma unroll
		for (uint i = 0; i < KEYS_PER_THREAD; ++i)
		{
			const uint counter_index = digit_counters[i];
			const uint packed_counter = lds_histogram[counter_index >> 1];
			const uint sub_counter = counter_index & 1u;
			// Add in thread block exclusive prefix
			ranks[i] = thread_prefixes[i] + (packed_counter >> (sub_counter * 16)) & 0xffffu;
		}
		//endregion

		begin_bit += RADIX_BITS;

		#pragma unroll
		for (uint i = 0; i < KEYS_PER_THREAD; ++i)
		{
			const uint item_offset = ranks[i];
			lds_keys[item_offset] = keys[i];
			lds_values[item_offset] = values[i];
		}

		groupMemoryBarrier();
		barrier();

		if (begin_bit >= (bool(bucketAssignment.is_merged) ? startBit + 8 : startBit))
		{
			//region Exchanges data items annotated by rank into striped arrangement
			#pragma unroll
			for (uint i = 0; i < KEYS_PER_THREAD; ++i)
			{
				const uint item_offset = i * WG_SIZE + lIdx;
				if (item_offset < bucketAssignment.k_count)
				{
					const uint dst_addr = bucketAssignment.k_offs + item_offset;
					const uint key = lds_keys[item_offset];
					const uint value = lds_values[item_offset];
					src_keys[dst_addr] = key;
					dst_keys[dst_addr] = key;
					src_values[dst_addr] = value;
					dst_values[dst_addr] = value;
				}
			}
			//endregion

			// Quit
			break;
		}

		//region Exchanges data items annotated by rank into blocked arrangement
		#pragma unroll
		for (uint i = 0; i < KEYS_PER_THREAD; ++i)
		{
			const uint item_offset = lIdx * KEYS_PER_THREAD + i;
			keys[i] = lds_keys[item_offset];
			values[i] = lds_values[item_offset];
		}
		//endregion
	}
}