#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_shader_subgroup_ballot : enable
#extension GL_NV_shader_subgroup_partitioned : enable

#include "hybrid_radix_sort.glsl"

layout(local_size_x = WG_SIZE) in;

layout(std140, binding = 0) uniform Consts
{
    uint size;
    uint nWGs;
    uint startBit;
    uint nBuckets;
    uint nScanBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer BlockAssignments
{
    BlockAssignment gBlockAssignments[];
};

layout(std430, binding = 2) readonly buffer SrcKeys
{
    uint gSrcKeys[];
};

layout(std430, binding = 3) writeonly buffer BlockHistogram
{
    uint gBlockHistogram[];
};

layout(std430, binding = 4) buffer SubBucketHistogram
{
    uint gSubBucketHistogram[];
};

shared uint localHistogram[NUM_BUCKET];

void main()
{
    const uint lIdx = gl_LocalInvocationID.x;
    const uint wgIdx = gl_WorkGroupID.x;
    const BlockAssignment blockAssignment = { KPB * wgIdx, min(KPB * wgIdx + KPB, size) - KPB * wgIdx, 0 };

    if (lIdx < NUM_BUCKET)
    {
        localHistogram[lIdx] = 0;
    }

    groupMemoryBarrier();
    barrier();

    #ifdef USE_THREAD_REDUCTION_AND_ATOMICS
    const uint segmentSize = WG_SIZE * SORTING_NETWORK_SIZE;
    const uint blockEnd = blockAssignment.k_offs + blockAssignment.k_count;
    for (uint segment = blockAssignment.k_offs + lIdx; segment < blockEnd; segment += segmentSize)
    {
        uint digit_count = 0;
        uint digits[SORTING_NETWORK_SIZE];
        const uint segmentEnd = min(segment + segmentSize, blockEnd);
        for (uint i = segment; i < segmentEnd; i += WG_SIZE)
        {
            const uint radix = (gSrcKeys[i] >> startBit) & 0xff;
            digits[digit_count++] = radix;
        }

        for (uint i = digit_count; i < SORTING_NETWORK_SIZE; ++i)
        {
            digits[i] = 0xffffffffu;
        }

        #if (SORTING_NETWORK_SIZE == 9)
        sortPair(digits[0], digits[1]);
        sortPair(digits[1], digits[2]);
        sortPair(digits[0], digits[1]);
        sortPair(digits[0], digits[3]);
        sortPair(digits[3], digits[6]);
        sortPair(digits[0], digits[3]);
        sortPair(digits[1], digits[3]);
        sortPair(digits[2], digits[4]);
        sortPair(digits[2], digits[3]);

        sortPair(digits[3], digits[4]);
        sortPair(digits[4], digits[5]);
        sortPair(digits[3], digits[4]);
        sortPair(digits[1], digits[4]);
        sortPair(digits[4], digits[7]);
        sortPair(digits[1], digits[4]);
        sortPair(digits[4], digits[6]);
        sortPair(digits[5], digits[6]);

        sortPair(digits[6], digits[7]);
        sortPair(digits[7], digits[8]);
        sortPair(digits[6], digits[7]);
        sortPair(digits[5], digits[8]);
        sortPair(digits[2], digits[5]);
        sortPair(digits[5], digits[7]);

        sortPair(digits[2], digits[5]);
        sortPair(digits[2], digits[6]);
        #elif (SORTING_NETWORK_SIZE == 8)
        sortPair(digits[0], digits[1]);
        sortPair(digits[0], digits[2]);
        sortPair(digits[1], digits[2]);
        sortPair(digits[1], digits[5]);
        sortPair(digits[1], digits[4]);
        sortPair(digits[2], digits[4]);
        sortPair(digits[3], digits[4]);

        sortPair(digits[2], digits[3]);
        sortPair(digits[1], digits[3]);
        sortPair(digits[5], digits[6]);
        sortPair(digits[2], digits[6]);
        sortPair(digits[3], digits[6]);
        sortPair(digits[3], digits[5]);

        sortPair(digits[4], digits[5]);
        sortPair(digits[4], digits[6]);
        sortPair(digits[0], digits[4]);

        sortPair(digits[6], digits[7]);
        sortPair(digits[5], digits[7]);
        sortPair(digits[3], digits[7]);
        #endif

        uint digit_counter = 1;
        uint prev_digit = digits[0];
        for (uint i = 1; i < digit_count; ++i)
        {
            uint digit = digits[i];
            if (prev_digit == digit)
            {
                ++digit_counter;
            }
            else
            {
                atomicAdd(localHistogram[prev_digit], digit_counter);
                digit_counter = 1;
                prev_digit = digit;
            }
        }
        atomicAdd(localHistogram[prev_digit], digit_counter);
    }
    #else
    for (uint i = blockAssignment.k_offs + lIdx; i < blockAssignment.k_offs + blockAssignment.k_count; i += WG_SIZE)
    {
        const uint radix = (gSrcKeys[i] >> startBit) & 0xff;
        #ifdef USE_SUBGROUP_PARTITION_AND_ATOMICS
        const uvec4 subgroupPartition = subgroupPartitionNV(radix);
        const uint digits = subgroupBallotBitCount(subgroupPartition);
        if (digits > 1)
        {
            if (subgroupBallotFindMSB(subgroupPartition) == gl_SubgroupInvocationID)
            {
                atomicAdd(localHistogram[radix], digits);
            }
        }
        else
        {
            atomicAdd(localHistogram[radix], 1);
        }
        #else
        atomicAdd(localHistogram[radix], 1);
        #endif
    }
    #endif

    groupMemoryBarrier();
    barrier();

    if (lIdx < NUM_BUCKET)
    {
        const uint sum = localHistogram[lIdx];
        gBlockHistogram[NUM_BUCKET * wgIdx + lIdx] = sum;
        atomicAdd(gSubBucketHistogram[lIdx], sum);
    }
}