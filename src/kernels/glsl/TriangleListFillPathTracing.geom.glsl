#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F
#define u_dim 4096
#define u_width u_dim
#define u_height u_dim

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (location = 0) in vec3 v_vertex[];
//layout (location = 1) in vec3 inNormal[];
//layout (location = 2) in vec3 inBinormal[];
//layout (location = 3) in vec3 inTangent[];
layout (location = 4) in vec2 inUV[];

layout (location = 0) out vec3 f_pos;
layout (location = 1) flat out int f_axis;   //indicate which axis the projection uses
layout (location = 2) flat out vec4 f_AABB;
layout (location = 4) out vec2 outUV;

struct AABB
{
	vec3 min;
	vec3 max;
};

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
	vec4 field2; // velocity.xyzw
};

layout (binding = 0) uniform UBO 
{
	mat4 mMVPx;
	mat4 mMVPy;
	mat4 mMVPz;
	mat4 mObject2World;
	mat4 mWorld2Object;
	mat4 mModelView;
	mat4 mModelViewProjection;
	mat4 mTransposeModelView;
	mat4 mInverseTransposeModelView;
};

layout (std430, binding = 1) buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 2) restrict buffer Counter
{
    uint trianglesCounter;
};

vec4 TransformPos(vec4 pos)
{
	const vec3 voxelizePosition = vec3(0.0f, 0.0f, 0.0f);
	const float voxelizeSize = 14.285714285714285714285714285714f;
	//return vec4((pos.xyz - voxelizePosition + voxelizeSize / 2) / voxelizeSize, pos.w);
	return pos * /*0.029f*/0.01f;
}
vec4 TransformPosition(vec2 pos)
{
	return vec4((pos) * 2 - 1, 0, 1);
}

void main(void)
{
	/*vec3 faceNormal = normalize( cross( v_vertex[1]-v_vertex[0], v_vertex[2]-v_vertex[0] ) );
	float NdotXAxis = abs( faceNormal.x );
	float NdotYAxis = abs( faceNormal.y );
	float NdotZAxis = abs( faceNormal.z );
	mat4 proj;

	//Find the axis the maximize the projected area of this triangle
	if( NdotXAxis > NdotYAxis && NdotXAxis > NdotZAxis )
    {
	    proj = mMVPx;
		f_axis = 1;
	}
	else if( NdotYAxis > NdotXAxis && NdotYAxis > NdotZAxis  )
    {
	    proj = mMVPy;
		f_axis = 2;
    }
	else
    {
	    proj = mMVPz;
		f_axis = 3;
	}

	vec4 pos[3];

	//transform vertices to clip space
	pos[0] = proj * gl_in[0].gl_Position;
	pos[1] = proj * gl_in[1].gl_Position;
	pos[2] = proj * gl_in[2].gl_Position;

	//Next we enlarge the triangle to enable conservative rasterization
	vec4 AABB;
	vec2 hPixel = vec2( 1.0/u_width, 1.0/u_height );
	float pl = 1.4142135637309 / u_width ;
	
	//calculate AABB of this triangle
	AABB.xy = pos[0].xy;
	AABB.zw = pos[0].xy;

	AABB.xy = min( pos[1].xy, AABB.xy );
	AABB.zw = max( pos[1].xy, AABB.zw );
	
	AABB.xy = min( pos[2].xy, AABB.xy );
	AABB.zw = max( pos[2].xy, AABB.zw );

	//Enlarge half-pixel
	AABB.xy -= hPixel;
	AABB.zw += hPixel;

	f_AABB = AABB;

	//find 3 triangle edge plane
    vec3 e0 = vec3( pos[1].xy - pos[0].xy, 0 );
	vec3 e1 = vec3( pos[2].xy - pos[1].xy, 0 );
	vec3 e2 = vec3( pos[0].xy - pos[2].xy, 0 );
	vec3 n0 = cross( e0, vec3(0,0,1) );
	vec3 n1 = cross( e1, vec3(0,0,1) );
	vec3 n2 = cross( e2, vec3(0,0,1) );

	//dilate the triangle
	pos[0].xy = pos[0].xy + pl*( (e2.xy/dot(e2.xy,n0.xy)) + (e0.xy/dot(e0.xy,n2.xy)) );
	pos[1].xy = pos[1].xy + pl*( (e0.xy/dot(e0.xy,n1.xy)) + (e1.xy/dot(e1.xy,n0.xy)) );
	pos[2].xy = pos[2].xy + pl*( (e1.xy/dot(e1.xy,n2.xy)) + (e2.xy/dot(e2.xy,n1.xy)) );

    //gl_Position = proj * gl_in[0].gl_Position;
	gl_Position = pos[0];
	f_pos = pos[0].xyz;
	//f_normal = v_normal[0];
	//f_texcoord = v_texcoord[0];
	EmitVertex();

	//gl_Position = proj * gl_in[1].gl_Position;
	gl_Position = pos[1];
	f_pos = pos[1].xyz;
	//f_normal = v_normal[1];
	//f_texcoord = v_texcoord[1];
	EmitVertex();

	//gl_Position = proj * gl_in[2].gl_Position;
	gl_Position = pos[2];
	f_pos = pos[2].xyz;
	//f_normal = v_normal[2];
	//f_texcoord = v_texcoord[2];
	EmitVertex();

	EndPrimitive();*/
	
	vec4 vOutput[3];
	vec4 vLastPos[3];
	vLastPos[0] = TransformPos(vec4(v_vertex[0], 0));
	vLastPos[1] = TransformPos(vec4(v_vertex[1], 0));
	vLastPos[2] = TransformPos(vec4(v_vertex[2], 0));
	
	AABB aabb = { vec3(FLT_MAX), vec3(FLT_MIN) };
	#pragma unroll 3
	for (uint i = 0; i < 3; i++)
	{
		aabb.min = min(aabb.min, vLastPos[i].xyz);
		aabb.max = max(aabb.max, vLastPos[i].xyz);
	}
	
	#pragma unroll 3
	for (uint i = 0; i < 3; i++)
	{
		vLastPos[i].xyz -= aabb.min;
	}

	vec3 Delta1 = vLastPos[0].xyz - vLastPos[1].xyz;
	vec3 Delta2 = vLastPos[0].xyz - vLastPos[2].xyz;

	float S1 = length(cross(vec3(Delta1.xy, 0), vec3(Delta2.xy, 0)));
	float S2 = length(cross(vec3(Delta1.xz, 0), vec3(Delta2.xz, 0)));
	float S3 = length(cross(vec3(Delta1.yz, 0), vec3(Delta2.yz, 0)));

	if (S1 > S2)
	{
		if (S1 > S3)
		{
			vOutput[0] = TransformPosition(vLastPos[0].xy);
			vOutput[1] = TransformPosition(vLastPos[1].xy);
			vOutput[2] = TransformPosition(vLastPos[2].xy);
		}
		else
		{
			vOutput[0] = TransformPosition(vLastPos[0].yz);
			vOutput[1] = TransformPosition(vLastPos[1].yz);
			vOutput[2] = TransformPosition(vLastPos[2].yz);
		}
	}
	else
	{
		if (S2 > S3)
		{
			vOutput[0] = TransformPosition(vLastPos[0].xz);
			vOutput[1] = TransformPosition(vLastPos[1].xz);
			vOutput[2] = TransformPosition(vLastPos[2].xz);
		}
		else
		{
			vOutput[0] = TransformPosition(vLastPos[0].yz);
			vOutput[1] = TransformPosition(vLastPos[1].yz);
			vOutput[2] = TransformPosition(vLastPos[2].yz);
		}
	}

	// Bloat triangle in normalized device space with the texel size of the currently bound 
	// render-target. In this way pixels, which would have been discarded due to the low 
	// resolution of the currently bound render-target, will still be rasterized. 
	/*vec2 side0N = normalize(vOutput[1].xy - vOutput[0].xy);
	vec2 side1N = normalize(vOutput[2].xy - vOutput[1].xy);
	vec2 side2N = normalize(vOutput[0].xy - vOutput[2].xy);
	const float texelSize = 1.0f / u_dim;
	vOutput[0].xy += normalize(-side0N + side2N) * texelSize;
	vOutput[1].xy += normalize(side0N - side1N) * texelSize;
	vOutput[2].xy += normalize(side1N - side2N) * texelSize;*/
	
	#pragma unroll 3
	for (uint i = 0; i < 3; i++)
	{
		f_pos = v_vertex[i].xyz;
		outUV = inUV[i];
		gl_Position = vOutput[i];
		EmitVertex();
	}

	EndPrimitive();
	/*const uint primitiveID = atomicAdd(trianglesCounter, 3);
	vec3 p;
	{
	const CompactSphere sphere = { vec4(vOutput[0].xyz, 0.025f) };
	spheres[primitiveID] = sphere;
	}
	{
	const CompactSphere sphere = { vec4(vOutput[1].xyz, 0.025f) };
	spheres[primitiveID + 1] = sphere;
	}
	{
	const CompactSphere sphere = { vec4(vOutput[2].xyz, 0.025f) };
	spheres[primitiveID + 2] = sphere;
	}*/
}