#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_nonuniform_qualifier : require
#extension GL_GOOGLE_include_directive : enable

#include "hybrid_radix_sort.glsl"

layout(std140, push_constant) uniform PushConsts
{
    uint dstSet;
};

layout(std430, binding = 0) buffer HybridRadixSortUbo
{
    HybridRadixSortUB hybridRadixSortUbo[];
};

layout(std430, binding = 1) buffer HybridRadixSortDispatchIndirectCommand
{
    DispatchIndirectCommand hybridRadixSortDispathIndirectCommands[][3];
};

void main()
{
    const uint srcSet = uint(!bool(dstSet));
    const uint nBuckets = hybridRadixSortDispathIndirectCommands[dstSet][1].x * NUM_BUCKET;

    const uint prefixScanBlockSize = BUCKET_SCAN_NUM_WIS_PER_BLOCK * NUM_BUCKET;
    const uint nPrefixScanBlocks = (nBuckets + prefixScanBlockSize - 1) / prefixScanBlockSize;
    const uint nPrefixScanWGs = min(nPrefixScanBlocks, BUCKET_SCAN_NUM_WGS);

    hybridRadixSortUbo[dstSet].size = hybridRadixSortUbo[srcSet].size;
    hybridRadixSortUbo[dstSet].nWGs = hybridRadixSortDispathIndirectCommands[dstSet][0].x;
    hybridRadixSortUbo[dstSet].startBit = hybridRadixSortUbo[srcSet].startBit - BITS_PER_PASS;
    hybridRadixSortUbo[dstSet].nBuckets = nBuckets;
    hybridRadixSortUbo[dstSet].nScanBlocksPerWG = nPrefixScanBlocks < BUCKET_SCAN_NUM_WGS ? 1 : (nPrefixScanBlocks + nPrefixScanWGs - 1) / nPrefixScanWGs;

    hybridRadixSortDispathIndirectCommands[srcSet][0].x = 0;
    hybridRadixSortDispathIndirectCommands[dstSet][1].x = nPrefixScanWGs;
    hybridRadixSortDispathIndirectCommands[srcSet][1].x = 0;
    hybridRadixSortDispathIndirectCommands[dstSet][2].x = 0;
}