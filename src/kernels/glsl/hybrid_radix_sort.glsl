#define BITS_PER_PASS 8
#define NUM_BUCKET (1 << BITS_PER_PASS)
#define WG_SIZE 256
#define KPT 9 // number of keys per thread
#define KPB (WG_SIZE * KPT) // number of keys per block
#define BUCKET_SCAN_NUM_WIS_PER_BLOCK 40
#define BUCKET_SCAN_WG_SIZE 1024
#define BUCKET_SCAN_NUM_WGS 1024
#define KEYS_PER_THREAD 18
#define LST (256 * KEYS_PER_THREAD)
#define MBT LST/*4608*/

//#define USE_SUBGROUP_PARTITION_AND_ATOMICS

//#define USE_THREAD_REDUCTION_AND_ATOMICS
#define SORTING_NETWORK_SIZE 9 // 8 or 9

struct HybridRadixSortUB
{
	uint size;
	uint nWGs;
	uint startBit;
	uint nBuckets;

	uint nScanBlocksPerWG;

	uint _pad[59];
};

struct BlockAssignment
{
	uint k_offs;
	uint k_count;
	uint b_id;
};

struct BucketAssignment
{
	uint k_offs;
	uint k_count;
	uint is_merged;
};

struct DispatchIndirectCommand
{
	uint x;
	uint y;
	uint z;
};

void sortPair(inout uint a, inout uint b)
{
	//	const uint _a = a;
	//	const uint _b = b;
	//	const uint condition = uint(a > b);
	//	const uint mask = 0u - condition;
	//	a = (_b & mask) | (_a & ~mask);
	//	b = (_a & mask) | (_b & ~mask);

	const uint t = a;
	a = min(a, b);
	b = max(t, b);
	//    if (a > b)
	//    {
	//        uint t = a;
	//        a = b;
	//        b = t;
	//    }
}