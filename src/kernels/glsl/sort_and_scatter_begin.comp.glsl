#version 460

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_ballot : enable
#extension GL_GOOGLE_include_directive : enable

#include "hybrid_radix_sort.glsl"

layout(local_size_x = WG_SIZE) in;

layout(binding = 0) uniform Consts
{
	uint size;
	uint nWGs;
	uint startBit;
	uint nBuckets;
	uint nScanBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer BlockAssignments
{
	BlockAssignment gBlockAssignments[];
};

layout(std430, binding = 2) readonly buffer SrcKeys
{
	uint gSrcKeys[];
};

layout(std430, binding = 3) readonly buffer BlockHistogram
{
	uint gBlockHistogram[];
};

layout(std430, binding = 4) buffer SubBucketHistogram
{
	uint gSubBucketHistogram[];
};

layout(std430, binding = 5) writeonly buffer DstKeys
{
	uint gDstKeys[];
};

layout(std430, binding = 6) readonly buffer src_values_buffer
{
	uint src_values[];
};

layout(std430, binding = 7) writeonly buffer dst_values_buffer
{
	uint dst_values[];
};

shared uint chunkCounter;
shared uint lds_keys[KPB];
shared uint lds_values[KPB];
shared uint localBlockHistogram[NUM_BUCKET];
shared uint globalBlockHistogram[NUM_BUCKET];

void main()
{
	const uint lIdx = gl_LocalInvocationID.x;
	const uint wgIdx = gl_WorkGroupID.x;
	uint myBlockHistogram;
	if (lIdx < NUM_BUCKET)
	{
		myBlockHistogram = gBlockHistogram[NUM_BUCKET * wgIdx + lIdx];
	}

	if (lIdx == 0)
	{
		chunkCounter = 0;
	}

	groupMemoryBarrier();
	barrier();

	if (lIdx < NUM_BUCKET)
	{
		localBlockHistogram[lIdx] = atomicAdd(chunkCounter, myBlockHistogram);
		globalBlockHistogram[lIdx] = atomicAdd(gSubBucketHistogram[lIdx], myBlockHistogram);
	}

	groupMemoryBarrier();
	barrier();

	const uint begin = KPB * wgIdx;
	const uint end = min(KPB * wgIdx + KPB, size);
	for (uint i = begin + lIdx; i < end; i += WG_SIZE)
	{
		const uint key = gSrcKeys[i];
		const uint radix = (key >> startBit) & 0xffu;
		const uint dstAddr = atomicAdd(localBlockHistogram[radix], 1); // TODO use look-ahead of two approach
		lds_keys[dstAddr] = key;
		lds_values[dstAddr] = src_values[i];
	}

	groupMemoryBarrier();
	barrier();

	for (uint i = lIdx; i < end - begin; i += WG_SIZE)
	{
		const uint key = lds_keys[i];
		const uint radix = (key >> startBit) & 0xffu;
		const uint dstAddr = atomicAdd(globalBlockHistogram[radix], 1); // TODO use look-ahead of two approach
		gDstKeys[dstAddr] = key;
		dst_values[dstAddr] = lds_values[i];
	}
}