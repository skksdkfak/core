#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define WG_SIZE 128
#define INTERSECT_STACK_SIZE 128

layout(local_size_x = WG_SIZE) in;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

layout (std140, push_constant) uniform PushConsts 
{
	uint numPrimitives;
	float deltaTime;
};

layout (std430, binding = 0) buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 1) buffer NewVelocity
{
    vec4 newVelocities[];
};

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint numberOfPrimitives = numPrimitives;
	
	if (i >= numberOfPrimitives)
		return;

	const float bbox_size = 2.0f;
	CompactSphere compactSphere = spheres[i];
	vec3 velocity = newVelocities[i].xyz;
	compactSphere.field0.xyz += velocity * deltaTime;
	/*if (length(compactSphere.field0.xyz) > 10.0f)
	{
        compactSphere.field0.xyz += normalize(compactSphere.field0.xyz) * (10.0f - compactSphere.field0.w);
        velocity *= -0.5f;
	}*
    /*if (compactSphere.field0.x < -bbox_size + compactSphere.field0.w)
    {
        compactSphere.field0.x = -bbox_size + compactSphere.field0.w;
        velocity.x = velocity.x * -0.5f;
    }
    if (compactSphere.field0.x > bbox_size + compactSphere.field0.w)
    {
        compactSphere.field0.x = bbox_size - compactSphere.field0.w;
        velocity.x = velocity.x * -0.5f;
    }
    if (compactSphere.field0.z < -bbox_size + compactSphere.field0.w)
    {
        compactSphere.field0.z = -bbox_size + compactSphere.field0.w;
        velocity.z = velocity.z * -0.5f;
    }
    if (compactSphere.field0.z > bbox_size + compactSphere.field0.w)
    {
        compactSphere.field0.z = bbox_size - compactSphere.field0.w;
        velocity.z = velocity.z * -0.5f;
    }*/
    if (compactSphere.field0.y < -0.5f + compactSphere.field0.w)
    {
        compactSphere.field0.y = -0.5f + compactSphere.field0.w;
        velocity.y = velocity.y * -0.5f;
    }
	
	spheres[i].field0.xyz += velocity * deltaTime;
    newVelocities[i].xyz = velocity;
}