#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_KHR_shader_subgroup_arithmetic: enable
#extension GL_KHR_shader_subgroup_ballot: enable
#extension GL_KHR_shader_subgroup_shuffle: enable



#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define GROUP_MEM_FENCE groupMemoryBarrier()
#define AtomInc(x) atomicAdd(x, 1)
#define AtomAdd(x, inc) atomicAdd(x, inc)

#define WG_SIZE 256
#define ELEMENTS_PER_WORK_GROUP 1024
#define ELEMENTS_PER_WORK_ITEM (ELEMENTS_PER_WORK_GROUP / WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1 << BITS_PER_PASS)
#define BITS_PER_LOCAL_SORT_PASS 2
#define NUM_LOCAL_SORT_BUCKET (1 << BITS_PER_LOCAL_SORT_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define CHECK_BOUNDARY 1

#define GET_GROUP_SIZE WG_SIZE

layout(local_size_x = WG_SIZE) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer Histogram
{
	uint rHistogram[];
};

layout(std430, binding = 2) readonly buffer SrcKeys
{
    uint gSrcKey[];
};

layout(std430, binding = 3) readonly buffer SrcValues
{
    uint gSrcVal[];
};

layout(std430, binding = 4) writeonly buffer DstKeys
{
    uint gDstKey[];
};

layout(std430, binding = 5) writeonly buffer DstValues
{
    uint gDstValue[];
};

shared uint localHistogramToCarry[NUM_BUCKET];
shared uint localHistogram[WG_SIZE * NUM_LOCAL_SORT_BUCKET];
shared uint ldsSortKey[WG_SIZE * ELEMENTS_PER_WORK_ITEM];
shared uint ldsSortVal[WG_SIZE * ELEMENTS_PER_WORK_ITEM];
shared uint ldsWarpSums[gl_WorkGroupSize.x / 32];
shared uint ldsSortData[WG_SIZE * 4];

uint localPrefixSum(uint pData, uint lIdx, int wgSize /*64 or 128*/)
{
	{	//	Set data
		ldsSortData[lIdx] = 0;
		ldsSortData[lIdx + wgSize] = pData;
	}

	GROUP_LDS_BARRIER;

	{	//	Prefix sum
		uint idx = lIdx + wgSize;
		if (lIdx < NUM_BUCKET)
		{
			ldsSortData[idx] = ldsSortData[idx - 1];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 1];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 4];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 8];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 16];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 32];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 64];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 128];
			GROUP_MEM_FENCE;
		}
	}

	GROUP_LDS_BARRIER;

	uint addValue = ldsSortData[lIdx + wgSize];
	return addValue;
}

uint localPrefixSum(uint data)
{
	uint warpSum = subgroupExclusiveAdd(data);
    if (gl_SubgroupInvocationID == gl_SubgroupSize - 1)
    {
        ldsWarpSums[gl_SubgroupID] = warpSum + data;
    }
	GROUP_LDS_BARRIER;

    if (gl_SubgroupID == 0 && gl_SubgroupInvocationID < (gl_WorkGroupSize.x / gl_SubgroupSize))
	{
		ldsWarpSums[gl_SubgroupInvocationID] = subgroupExclusiveAdd(ldsWarpSums[gl_SubgroupInvocationID]);
	}
	GROUP_LDS_BARRIER;

	uint prefixSum = warpSum + subgroupBroadcastFirst(ldsWarpSums[gl_SubgroupID]);

	return prefixSum;
}

void sort4BitsKeyValue(inout uint sortKey[ELEMENTS_PER_WORK_ITEM], inout uint sortVal[ELEMENTS_PER_WORK_ITEM], uint startBit, uint lIdx)
{
	const uint dstAddr = ELEMENTS_PER_WORK_ITEM * lIdx;
	for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += BITS_PER_LOCAL_SORT_PASS)
	{
		for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
		{
			localHistogram[i * WG_SIZE + lIdx] = 0;
		}
		
		for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
		{
			uint localKey = (sortKey[i] >> (startBit + ibit)) & 0x3;
			++localHistogram[localKey * WG_SIZE + lIdx];
		}
		GROUP_LDS_BARRIER;
			
		uint sum = 0;
		uint data[NUM_LOCAL_SORT_BUCKET];
		for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
		{
			uint tmp = localHistogram[lIdx * NUM_LOCAL_SORT_BUCKET + i];
			data[i] = sum;
			sum += tmp;
		}
		
		sum = localPrefixSum(sum/*, lIdx, WG_SIZE*/);

		for (uint i = 0; i < NUM_LOCAL_SORT_BUCKET; ++i)
		{
			localHistogram[lIdx * NUM_LOCAL_SORT_BUCKET + i] = data[i] + sum;
		}
		GROUP_LDS_BARRIER;
		
		for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
		{
			uint localKey = (sortKey[i] >> (startBit + ibit)) & 0x3;
			uint dst_offset = localHistogram[localKey * WG_SIZE + lIdx]++;
			ldsSortKey[dst_offset] = sortKey[i];
			ldsSortVal[dst_offset] = sortVal[i];
		}
		GROUP_LDS_BARRIER;
		
		for (uint i = 0; i < ELEMENTS_PER_WORK_ITEM; ++i)
		{
			sortKey[i] = ldsSortKey[dstAddr + i];
			sortVal[i] = ldsSortVal[dstAddr + i];
		}
	}
}

//shared uint ldsSortData[NUM_BUCKET];
//#define SET_HISTOGRAM(setIdx, key) ldsSortData[(setIdx)*NUM_BUCKET+key]

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	uint wgSize = GET_GROUP_SIZE;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;

	if (lIdx < NUM_BUCKET)
	{
		localHistogramToCarry[lIdx] = rHistogram[lIdx*nWGs + wgIdx];
	}

	GROUP_LDS_BARRIER;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM * WG_SIZE;

	const uint nBlocks = nAligned / blockSize - nBlocksPerWG * wgIdx;

	uint addr = blockSize * nBlocksPerWG * wgIdx + ELEMENTS_PER_WORK_ITEM * lIdx;

	for (int iblock = 0; iblock < min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		uint myHistogram = 0;

		uint sortData[ELEMENTS_PER_WORK_ITEM];
		uint sortVal[ELEMENTS_PER_WORK_ITEM];

		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
#if defined(CHECK_BOUNDARY)
		{
			sortData[i] = (addr + i < n) ? gSrcKey[addr + i] : 0xffffffff;
			sortVal[i] = (addr + i < n) ? gSrcVal[addr + i] : 0xffffffff;
		}
#else
		{
				sortData[i] = gSrcKey[addr + i];
				sortVal[i] = gSrcVal[addr + i];
		}
#endif

		sort4BitsKeyValue(sortData, sortVal, startBit, lIdx);

		uint keys[ELEMENTS_PER_WORK_ITEM];
		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
			keys[i] = (sortData[i] >> startBit) & 0xf;

		{	//	create histogram
			uint setIdx = lIdx / 16;
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[lIdx] = 0;
				/*for (int i = 0; i < 16; ++i)
				{
					ldsSortData[lIdx * 16 + i] = 0;
				}*/
			}
			GROUP_LDS_BARRIER;

			for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
			{
				if (addr + i < n)
				{
					AtomInc(localHistogram[keys[i]]);
				}
			}
			GROUP_LDS_BARRIER;
			
			int hIdx = int(lIdx);
			if (lIdx < NUM_BUCKET)
			{
				/*uint sum = 0;
				for (int i = 0; i < WG_SIZE / 16; ++i)
				{
					sum += SET_HISTOGRAM(i, lIdx);
				}
				myHistogram = sum;
				localHistogram[hIdx] = sum;*/
				myHistogram = localHistogram[hIdx];
			}
			GROUP_LDS_BARRIER;
			
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[hIdx] = hIdx - 1 < 0 ? 0 : localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;

				uint u0, u1, u2;
				u0 = hIdx - 3 < 0 ? 0 : localHistogram[hIdx - 3];
				u1 = hIdx - 2 < 0 ? 0 : localHistogram[hIdx - 2];
				u2 = hIdx - 1 < 0 ? 0 : localHistogram[hIdx - 1];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;

				u0 = hIdx - 12 < 0 ? 0 : localHistogram[hIdx - 12];
				u1 = hIdx - 8 < 0 ? 0 : localHistogram[hIdx - 8];
				u2 = hIdx - 4 < 0 ? 0 : localHistogram[hIdx - 4];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;
			}
			GROUP_LDS_BARRIER;
					
			/*if (lIdx < NUM_BUCKET)
			{
				myHistogram = localHistogram[lIdx];
			}
			GROUP_LDS_BARRIER;
			uint sum = localPrefixSum(myHistogram);
			GROUP_LDS_BARRIER;
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[lIdx] = sum;
			}
			GROUP_LDS_BARRIER;*/
		}

		{
			for (int ie = 0; ie < ELEMENTS_PER_WORK_ITEM; ie++)
			{
				const uint dataIdx = ELEMENTS_PER_WORK_ITEM * lIdx + ie;
				const uint binIdx = keys[ie];
				const uint groupOffset = localHistogramToCarry[binIdx];
				const uint myIdx = dataIdx - localHistogram[binIdx];
#if defined(CHECK_BOUNDARY)
				if (addr + ie < n)
#endif
				{
					gDstKey[groupOffset + myIdx] = sortData[ie];
					gDstValue[groupOffset + myIdx] = sortVal[ie];
				}
			}
		}

		GROUP_LDS_BARRIER;

		if (lIdx < NUM_BUCKET)
		{
			localHistogramToCarry[lIdx] += myHistogram;
		}
		//GROUP_LDS_BARRIER;
	}
}