#version 450 core

#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_nonuniform_qualifier : enable

layout(location = 0) in vec2 input_uv;
layout(location = 1) flat in uint input_image_index;

layout(set = 0, binding = 0) uniform sampler primitive_sampler;
layout(set = 0, binding = 1) uniform texture2D primitive_textures[];

layout(location = 0) out vec4 output_fragment_color;

void main(void)
{
	output_fragment_color = vec4(texture(sampler2D(primitive_textures[input_image_index], primitive_sampler), input_uv).rgb, 0.8f);
}