#version 450

#define FLT_MIN 1.175494351e-38F
#define FLT_MAX 3.402823466e+38F
#define u_width 128
#define u_height 128

layout (location = 0) in vec3 f_pos;
//layout (location = 1) in vec3 inNormal;
//layout (location = 2) in vec3 inBinormal;
//layout (location = 3) in vec3 inTangent;
layout (location = 4) in vec2 inUV;

layout (location = 1) flat in int f_axis;   //indicate which axis the projection uses
layout (location = 2) flat in vec4 f_AABB;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

layout (std430, binding = 1) buffer Spheres
{
    CompactSphere spheres[];
};

layout (std430, binding = 2) restrict buffer Counter
{
    uint trianglesCounter;
};

layout (set = 0, binding = 3) uniform sampler2D samplerColor;

void main()
{
    /*if( f_pos.x < f_AABB.x || f_pos.y < f_AABB.y || f_pos.x > f_AABB.z || f_pos.y > f_AABB.w )
	   discard;

	uvec4 temp = uvec4( gl_FragCoord.x, gl_FragCoord.y, u_width * gl_FragCoord.z, 0 ) ;
	uvec4 texcoord;
	if( f_axis == 1 )
	{
	    texcoord.x = u_width - temp.z;
		texcoord.z = temp.x;
		texcoord.y = temp.y;
	}
	else if( f_axis == 2 )
    {
	    texcoord.z = temp.y;
		texcoord.y = u_width-temp.z;
		texcoord.x = temp.x;
	}
	else
	{
	    texcoord = temp;
	}*/

	vec3 p;
	const CompactSphere sphere = { vec4(f_pos.xyz, /*64.0f / 1024.0f*/0.03f), texture(samplerColor, inUV)/*, vec4(0.0f)vec4(0.0f, -0.01f, 0.0f, 0.0f)*/ };
	const uint primitiveID = atomicAdd(trianglesCounter, 1);	
	spheres[primitiveID] = sphere;
}