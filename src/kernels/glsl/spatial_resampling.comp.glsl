#version 460

#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_ray_query : enable

uint rand_state;

// Steps the RNG and returns a floating-point value between 0 and 1 inclusive.
float rand(inout uint rand_state)
{
	// Condensed version of pcg_output_rxs_m_xs_32_32, with simple conversion to floating-point [0,1].
	rand_state = rand_state * 747796405 + 1;
	uint word = ((rand_state >> ((rand_state >> 28) + 4)) ^ rand_state) * 277803737;
	word = (word >> 22) ^ word;
	return float(word) / 4294967295.0f;
}

struct sample_t
{
	vec3 xv;
	vec3 nv;
	vec3 xs;
	vec3 ns;
	vec3 radiance;
};

struct reservoir_t
{
	sample_t z;
	float w;
	uint M;
	float W;
	// todo: remove
	vec3 visible_point_throughput;
	vec3 position;
	vec3 normal;
	float ray_length;
};

void update_reservoir(inout reservoir_t r, sample_t s, float w)
{
	r.w += w;
	++r.M;
	if (rand(rand_state) < w / r.w)
	{
		r.z = s;
		// todo: revert
		//r.z.xs = s.xs;
		//r.z.ns = s.ns;
		//r.z.radiance = s.radiance;
	}
}

void merge_reservoir(inout reservoir_t r0, in reservoir_t r1, float p_hat)
{
	uint M0 = r0.M;
	update_reservoir(r0, r1.z, p_hat * r1.W * r1.M);
	r0.M = M0 + r1.M;
}

float luminance(vec3 color)
{
	return dot(color, vec3(0.299, 0.587, 0.114));
}

layout(std140, push_constant) uniform PushConsts
{
	uint resample_pass;
};

layout(set = 0, binding = 0) uniform accelerationStructureEXT acceleration_structure;
layout(set = 0, binding = 1) uniform CameraProperties
{
	mat4 viewInverse;
	mat4 projInverse;
	float ray_eps;
	uint seed;
	uint sample_count;
} camera;

layout(set = 0, binding = 2) buffer initial_samples_buffer
{
	reservoir_t g_initial_samples[];
};

layout(set = 0, binding = 3) buffer resampled_samples_buffer
{
	reservoir_t g_resampled_samples[];
};

layout(set = 0, binding = 4, rgba8) uniform image2D accumulator;

layout(set = 1, binding = 0, rgba8) uniform image2D image;

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
	const uint gid = gl_GlobalInvocationID.y * 1280 + gl_GlobalInvocationID.x;
	rand_state = gid + camera.seed;
	const float kReuseRadius = 10.0;
	reservoir_t r;

	if (resample_pass % 2 == 0)
	{
		r = g_initial_samples[gid];
	}
	else
	{
		//r = g_resampled_samples[gid];
	}

//if (resample_pass != 234234234)
//return;
	//if (r.M > 0 && resample_pass == 0)
	//{
	//	const vec2 pixelCenter = vec2(gl_GlobalInvocationID.xy) + vec2(0.5);
	//	const vec2 inUV = pixelCenter / vec2(1280, 720);
	//	vec2 d = inUV * 2.0 - 1.0;
	//	vec4 origin = camera.viewInverse * vec4(0, 0, 0, 1);
	//	vec4 target = camera.projInverse * vec4(d.x, d.y, 1, 1);
	//	vec4 direction = camera.viewInverse * vec4(normalize(target.xyz), 0);
	//
	//	rayQueryEXT query;
	//	rayQueryInitializeEXT(query, acceleration_structure, gl_RayFlagsNoOpaqueEXT, 0xff, r.z.xv, 0.01, normalize(r.z.xs - r.z.xv), length(r.z.xs - r.z.xv) * 1.9);
	//
	//	while (rayQueryProceedEXT(query))
	//	{
	//		//rayQueryConfirmIntersectionEXT(query);
	//		imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(1, 0, 0, 0));
	//		//return;
	//	}
	//
	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(0, 1, 0, 0));
	//	//if (rayQueryGetIntersectionTypeEXT(query, true) != gl_RayQueryCommittedIntersectionNoneEXT)
	//	//{
	//	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(rayQueryGetIntersectionTEXT(query, true) / 100.0));
	//	//}
	//	//else
	//	//{
	//	//	imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(1,0,0,0));
	//	//}
	//}
	//return;
	for (int reuse_idx = 0; reuse_idx < 0; ++reuse_idx)
	{
		// Convert from [0, 1) to [-1, 1)
		vec2 rand_offset = vec2(rand(rand_state), rand(rand_state)) * 2.0 - 1.0;
		ivec2 current_pixel_pos = ivec2(gl_GlobalInvocationID.xy) + ivec2(rand_offset * kReuseRadius);
		uint current_pixel_index = current_pixel_pos.y * 1280 + current_pixel_pos.x;

		if (gid == current_pixel_index)
		{
			continue;
		}

		reservoir_t current_reservoir;
		
		if (resample_pass % 2 == 0)
		{
			current_reservoir = g_initial_samples[current_pixel_index];
		}
		else
		{
			current_reservoir = g_resampled_samples[current_pixel_index];
		}
		
		if (abs(current_reservoir.ray_length - r.ray_length) / r.ray_length > 0.5)
		{
			continue;
		}

		// Bilaterally filter the neighbor reservoir
		const float kNormalThreshold = 0.9;
		float bilateral_weight = dot(r.normal, current_reservoir.normal);
		if (bilateral_weight < kNormalThreshold)
		{
			continue;
		}

		//if (current_reservoir.W > 0 && r.M > 0)
		{
			float target_pdf = luminance(current_reservoir.z.radiance);

			//vec3 OffsetB = current_reservoir.z.xs - current_reservoir.z.xv;
			//vec3 OffsetA = current_reservoir.z.xs - r.position;
			//// Discard back-face
			//if (dot(r.normal, OffsetA) < 0 || dot(current_reservoir.z.nv, r.normal) < 0.8)
			//{
			//	continue;
			//}
			//
			//float RB2 = dot(OffsetB, OffsetB);
			//float RA2 = dot(OffsetA, OffsetA);
			//OffsetB = normalize(OffsetB);
			//OffsetA = normalize(OffsetA);
			//
			//float cosA = dot(r.normal, OffsetB);
			//float cosB = dot(current_reservoir.z.nv, OffsetB);
			//float cosPhiA = -dot(OffsetA, current_reservoir.z.ns);
			//float cosPhiB = -dot(OffsetB, current_reservoir.z.ns);
			//
			//// Discard extreme samples to prevent fireflies
			//if (cosA <= 0 || cosB <= 0 || cosPhiA <= 0 || cosPhiB <= 0 || RA2 <= 0 || RB2 <= 0)
			//{
			//	continue;
			//}
			//
			//float k = RB2 * cosPhiA / (RA2 * cosPhiB);
			//target_pdf *= clamp(k, 0, 20);
			//
			//rayQueryEXT query;
			//rayQueryInitializeEXT(query, acceleration_structure, gl_RayFlagsTerminateOnFirstHitEXT, 0xff, r.position, 0.01, normalize(current_reservoir.z.xs - r.position), length(current_reservoir.z.xs - r.position) * 0.9);
			//
			//while (rayQueryProceedEXT(query))
			//{
			//}
			//
			//if (rayQueryGetIntersectionTypeEXT(query, true) != gl_RayQueryCommittedIntersectionNoneEXT)
			//{
			//	target_pdf = 0;
			//	continue;
			//}

			merge_reservoir(r, current_reservoir, target_pdf);
		}
	}

	float denom = r.M * luminance(r.z.radiance);
	r.W = denom > 0 ? r.w / denom : 0;

	if (resample_pass % 2 == 0)
	{
		g_resampled_samples[gid] = r;
	}
	else
	{
		g_initial_samples[gid] = r;
	}

	if (resample_pass < 4)
	{
		//return;
	}

	vec4 output_color = vec4(r.visible_point_throughput * r.z.radiance * r.W, 0.0);
	if (camera.sample_count > 1)
	{
		vec4 accumulated = imageLoad(accumulator, ivec2(gl_GlobalInvocationID.xy)) + output_color;
		imageStore(accumulator, ivec2(gl_GlobalInvocationID.xy), accumulated);
		output_color = accumulated / camera.sample_count;
	}
	else
	{
		imageStore(accumulator, ivec2(gl_GlobalInvocationID.xy), output_color);
	}

	imageStore(image, ivec2(gl_GlobalInvocationID.xy), output_color);
}
