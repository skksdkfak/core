#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_debug_printf : enable

#define WG_SIZE 128
#define INTERSECT_STACK_SIZE 64

layout(local_size_x = WG_SIZE) in;

struct CompactSphere
{
	vec4 field0; // pos.xyz, radius
	vec4 field1; // color.rgba
};

struct CompactTreeNode
{
	uvec4 field0; // parent, left, right, triangleID
	vec4 field1; // boundMin.xyz, rightmostLeafInLeftSubtree
	vec4 field2; // boundMax.xyz, rightmostLeafInRightSubtree
};

struct LinkedList
{
	uint next;
	uint val;
};

struct AABB
{
	vec3 min;
	vec3 max;
};

layout(std140, push_constant) uniform PushConsts
{
	uint numPrimitives;
};

layout(std430, binding = 0) readonly buffer RadixTree
{
	CompactTreeNode radixTree[];
};

layout(std430, binding = 1) readonly buffer Spheres
{
	CompactSphere spheres[];
};

layout(std430, binding = 2) buffer NeighborsCounter
{
	uint neighborsCounter;
};

layout(std430, binding = 3) buffer NeighborsList
{
	LinkedList neighborsList[];
};

layout(std430, binding = 4) buffer NeighborsHeadList
{
	uint neighborsHeadList[];
};

float SqDistPointAABB(vec3 p, AABB b)
{
	float sqDist = 0.0f;
#pragma unroll 3
	for (int i = 0; i < 3; i++)
	{
		// For each axis count any excess distance outside box extents 
		float v = p[i];
		if (v < b.min[i])
			sqDist += (b.min[i] - v) * (b.min[i] - v);
		if (v > b.max[i])
			sqDist += (v - b.max[i]) * (v - b.max[i]);
	}
	return sqDist;
}

// Returns true if sphere s intersects AABB b, false otherwise 
bool TestSphereAABB(CompactSphere s, AABB b)
{
	// Compute squared distance between sphere center and AABB
	float sqDist = SqDistPointAABB(s.field0.xyz, b);
	// Sphere and AABB intersect if the (squared) distance 
	// between them is less than the (squared) sphere radius 
	return sqDist <= s.field0.w * s.field0.w;
}

bool TestAABBAABB(AABB a, AABB b)
{
	// Exit with no intersection if separated along an axis 
	if (a.max[0] < b.min[0] || a.min[0] > b.max[0] ||
		a.max[1] < b.min[1] || a.min[1] > b.max[1] ||
		a.max[2] < b.min[2] || a.min[2] > b.max[2])
		return false;
	// Overlapping on all axes means AABBs are intersecting 
	return true;
}

void main()
{
	const uint i = gl_GlobalInvocationID.x;
	const uint numberOfPrimitives = numPrimitives;

	if (i >= numberOfPrimitives)
		return;

	const uint leavesOffset = numberOfPrimitives - 1;
	const uint queryLeafIndex = leavesOffset + i;
	CompactTreeNode queryLeaf = radixTree[queryLeafIndex];
	const CompactSphere queryPrimitive = spheres[queryLeaf.field0.w];
	const AABB leafBound = { queryLeaf.field1.xyz, queryLeaf.field2.xyz };
	uint stack[INTERSECT_STACK_SIZE];
	uint neighbors[16];
	uint neighbor_count = 0;
	int ptr = 0;
	stack[ptr++] = 0xFFFFFFFF;
	uint idx = 0;
	const uint leafs = numberOfPrimitives;

	AABB lbox;
	AABB rbox;
	CompactTreeNode node;
	while (idx != 0xFFFFFFFF)
	{
		node = radixTree[idx];
		if (idx < leafs)
		{
			lbox.min = radixTree[node.field0.y].field1.xyz;
			lbox.max = radixTree[node.field0.y].field2.xyz;
			rbox.min = radixTree[node.field0.z].field1.xyz;
			rbox.max = radixTree[node.field0.z].field2.xyz;

			//const bool traverseLeftChild = node.field0.y > i && TestSphereAABB(queryLeaf, lbox);
			//const bool traverseRightChild = node.field1.w > i && TestSphereAABB(queryLeaf, rbox);
			const bool traverseLeftChild = node.field1.w > i && TestAABBAABB(leafBound, lbox);
			const bool traverseRightChild = node.field2.w > i && TestAABBAABB(leafBound, rbox);

			if (traverseLeftChild && traverseRightChild)
			{
				idx = node.field0.z;
				stack[ptr++] = node.field0.y;
				continue;
			}
			else if (traverseLeftChild)
			{
				idx = node.field0.y;
				continue;
			}
			else if (traverseRightChild)
			{
				idx = node.field0.z;
				continue;
			}
		}
		else
		{
			const uint leafIndex = node.field0.w;
			const CompactSphere sphere = spheres[leafIndex];
			if (distance(queryPrimitive.field0.xyz, sphere.field0.xyz) < queryPrimitive.field0.w + sphere.field0.w)
			{
				neighbors[neighbor_count++] = leafIndex;

				if (leafIndex == -1)
					debugPrintfEXT("", leafIndex);
			}
		}
		idx = stack[--ptr];
	}

	if (neighbor_count == 0)
	{
		return;
	}

	uint neighborIndex = atomicAdd(neighborsCounter, neighbor_count * 2);

	for (uint i = 0; i < neighbor_count; i++)
	{
		const uint leafIndex = neighbors[i];

		neighborsList[neighborIndex].val = queryLeaf.field0.w;
		neighborsList[neighborIndex].next = atomicExchange(neighborsHeadList[leafIndex], neighborIndex);

		neighborIndex++;
	}

	uint range_begin = neighborIndex;

	for (uint i = 0; i < neighbor_count - 1; i++)
	{
		const uint leafIndex = neighbors[i];

		neighborsList[neighborIndex].val = leafIndex;
		neighborsList[neighborIndex].next = neighborIndex + 1;

		neighborIndex++;
	}

	// handle last element
	{
		const uint leafIndex = neighbors[neighbor_count - 1];

		neighborsList[neighborIndex].val = leafIndex;
		neighborsList[neighborIndex].next = atomicExchange(neighborsHeadList[queryLeaf.field0.w], range_begin);
	}
}