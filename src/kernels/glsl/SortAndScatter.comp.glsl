#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable



#define GET_GROUP_IDX gl_WorkGroupID.x
#define GET_LOCAL_IDX gl_LocalInvocationIndex.x
#define GROUP_LDS_BARRIER groupMemoryBarrier(); barrier()
#define GROUP_MEM_FENCE groupMemoryBarrier()
#define AtomInc(x) atomicAdd(x, 1)
#define AtomAdd(x, inc) atomicAdd(x, inc)

#define WG_SIZE 64
#define ELEMENTS_PER_WORK_ITEM (256/WG_SIZE)
#define BITS_PER_PASS 4
#define NUM_BUCKET (1<<BITS_PER_PASS)

//	this isn't optimization for VLIW. But just reducing writes. 
#define USE_2LEVEL_REDUCE 1

#define CHECK_BOUNDARY 1

#define GET_GROUP_SIZE WG_SIZE

layout(local_size_x = WG_SIZE) in;

layout (std140, push_constant) uniform PushConsts 
{
	uint m_startBit;
};

layout(binding = 0) uniform Consts
{
	uint m_n;
	uint m_nAligned;
	uint m_nWGs;
	uint m_nBlocksPerWG;
};

layout(std430, binding = 1) readonly buffer Histogram
{
	uint rHistogram[];
};

layout(std430, binding = 2) readonly buffer SrcKeys
{
    uint gSrcKey[];
};

layout(std430, binding = 3) readonly buffer SrcValues
{
    uint gSrcVal[];
};

shared uint ldsSortData[WG_SIZE*ELEMENTS_PER_WORK_ITEM + 16];

uint localPrefixSum(uint pData, uint lIdx, inout uint totalSum, int wgSize /*64 or 128*/)
{
	{	//	Set data
		ldsSortData[lIdx] = 0;
		ldsSortData[lIdx + wgSize] = pData;
	}

	GROUP_LDS_BARRIER;

	{	//	Prefix sum
		uint idx = 2 * lIdx + (wgSize + 1);
#if defined(USE_2LEVEL_REDUCE)
		if (lIdx < 64)
		{
			uint u0, u1, u2;
			u0 = ldsSortData[idx - 3];
			u1 = ldsSortData[idx - 2];
			u2 = ldsSortData[idx - 1];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 12];
			u1 = ldsSortData[idx - 8];
			u2 = ldsSortData[idx - 4];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;

			u0 = ldsSortData[idx - 48];
			u1 = ldsSortData[idx - 32];
			u2 = ldsSortData[idx - 16];
			AtomAdd(ldsSortData[idx], u0 + u1 + u2);
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[idx] += ldsSortData[idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[idx - 1] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
		}
#else
		if (lIdx < 64)
		{
			ldsSortData[idx] += ldsSortData[idx - 1];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 4];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 8];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 16];
			GROUP_MEM_FENCE;
			ldsSortData[idx] += ldsSortData[idx - 32];
			GROUP_MEM_FENCE;
			if (wgSize > 64)
			{
				ldsSortData[idx] += ldsSortData[idx - 64];
				GROUP_MEM_FENCE;
			}

			ldsSortData[idx - 1] += ldsSortData[idx - 2];
			GROUP_MEM_FENCE;
		}
#endif
	}

	GROUP_LDS_BARRIER;

	totalSum = ldsSortData[wgSize * 2 - 1];
	uint addValue = ldsSortData[lIdx + wgSize - 1];
	return addValue;
}

uint unpack4Key(uint key, uint keyIdx) { return (key >> (keyIdx * 8)) & 0xff; }

uint bit8Scan(uint v)
{
	return (v << 8) + (v << 16) + (v << 24);
}

shared uint ldsSortVal[WG_SIZE*ELEMENTS_PER_WORK_ITEM + 16];

void sort4Bits1KeyValue(inout uint sortData[4], inout uint sortVal[4], uint startBit, uint lIdx)
{
	for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += 2)
	{
		uvec4 b = uvec4((sortData[0] >> (startBit + ibit)) & 0x3,
			(sortData[1] >> (startBit + ibit)) & 0x3,
			(sortData[2] >> (startBit + ibit)) & 0x3,
			(sortData[3] >> (startBit + ibit)) & 0x3);

		uint key4;
		uint sKeyPacked[4] = { 0, 0, 0, 0 };
		{
			sKeyPacked[0] |= 1 << (8 * b.x);
			sKeyPacked[1] |= 1 << (8 * b.y);
			sKeyPacked[2] |= 1 << (8 * b.z);
			sKeyPacked[3] |= 1 << (8 * b.w);

			key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
		}

		uint rankPacked;
		uint sumPacked;
		{
			rankPacked = localPrefixSum(key4, lIdx, sumPacked, WG_SIZE);
		}

		GROUP_LDS_BARRIER;

		uint newOffset[4] = { 0,0,0,0 };
		{
			uint sumScanned = bit8Scan(sumPacked);

			uint scannedKeys[4];
			scannedKeys[0] = 1 << (8 * b.x);
			scannedKeys[1] = 1 << (8 * b.y);
			scannedKeys[2] = 1 << (8 * b.z);
			scannedKeys[3] = 1 << (8 * b.w);
			{	//	4 scans at once
				uint sum4 = 0;
				for (int ie = 0; ie < 4; ie++)
				{
					uint tmp = scannedKeys[ie];
					scannedKeys[ie] = sum4;
					sum4 += tmp;
				}
			}

			{
				uint sumPlusRank = sumScanned + rankPacked;
				{	uint ie = b.x;
				scannedKeys[0] += sumPlusRank;
				newOffset[0] = unpack4Key(scannedKeys[0], ie);
				}
				{	uint ie = b.y;
				scannedKeys[1] += sumPlusRank;
				newOffset[1] = unpack4Key(scannedKeys[1], ie);
				}
				{	uint ie = b.z;
				scannedKeys[2] += sumPlusRank;
				newOffset[2] = unpack4Key(scannedKeys[2], ie);
				}
				{	uint ie = b.w;
				scannedKeys[3] += sumPlusRank;
				newOffset[3] = unpack4Key(scannedKeys[3], ie);
				}
			}
		}


		GROUP_LDS_BARRIER;

		uint dstAddr = 4 * lIdx;
		ldsSortData[newOffset[0]] = sortData[0];
		ldsSortData[newOffset[1]] = sortData[1];
		ldsSortData[newOffset[2]] = sortData[2];
		ldsSortData[newOffset[3]] = sortData[3];

		ldsSortVal[newOffset[0]] = sortVal[0];
		ldsSortVal[newOffset[1]] = sortVal[1];
		ldsSortVal[newOffset[2]] = sortVal[2];
		ldsSortVal[newOffset[3]] = sortVal[3];

		GROUP_LDS_BARRIER;

		sortData[0] = ldsSortData[dstAddr + 0];
		sortData[1] = ldsSortData[dstAddr + 1];
		sortData[2] = ldsSortData[dstAddr + 2];
		sortData[3] = ldsSortData[dstAddr + 3];

		sortVal[0] = ldsSortVal[dstAddr + 0];
		sortVal[1] = ldsSortVal[dstAddr + 1];
		sortVal[2] = ldsSortVal[dstAddr + 2];
		sortVal[3] = ldsSortVal[dstAddr + 3];

		GROUP_LDS_BARRIER;
	}
}

void sort4Bits1(inout uint sortData[4], int startBit, int lIdx)
{
	/*
	for(uint ibit=0; ibit < BITS_PER_PASS; ibit += 2)
	{
	uvec4 b = uvec4((sortData[0] >> (startBit + ibit)) & 0x3,
	(sortData[1] >> (startBit + ibit)) & 0x3,
	(sortData[2] >> (startBit + ibit)) & 0x3,
	(sortData[3] >> (startBit + ibit)) & 0x3);

	uint key4;
	uint sKeyPacked[4] = { 0, 0, 0, 0 };
	{
	sKeyPacked[0] |= 1<<(8*b.x);
	sKeyPacked[1] |= 1<<(8*b.y);
	sKeyPacked[2] |= 1<<(8*b.z);
	sKeyPacked[3] |= 1<<(8*b.w);

	key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
	}

	uint rankPacked;
	uint sumPacked;
	{
	rankPacked = localPrefixSum64VSingle( key4, lIdx, sumPacked );
	}

	//		GROUP_LDS_BARRIER;

	uint sum[4] = { unpack4Key( sumPacked,0 ), unpack4Key( sumPacked,1 ), unpack4Key( sumPacked,2 ), unpack4Key( sumPacked,3 ) };

	{
	uint sum4 = 0;
	for(int ie=0; ie<4; ie++)
	{
	uint tmp = sum[ie];
	sum[ie] = sum4;
	sum4 += tmp;
	}
	}

	uint newOffset[4] = { 0,0,0,0 };

	for(int ie=0; ie<4; ie++)
	{
	uvec4 key = extractKeys( b, ie );
	uvec4 scannedKey = key;
	prefixScanVectorEx( scannedKey );
	uint offset = sum[ie] + unpack4Key( rankPacked, ie );
	uvec4 dstAddress = uvec4( offset, offset, offset, offset ) + scannedKey;

	newOffset[0] += dstAddress.x*key.x;
	newOffset[1] += dstAddress.y*key.y;
	newOffset[2] += dstAddress.z*key.z;
	newOffset[3] += dstAddress.w*key.w;
	}



	{
	ldsSortData[newOffset[0]] = sortData[0];
	ldsSortData[newOffset[1]] = sortData[1];
	ldsSortData[newOffset[2]] = sortData[2];
	ldsSortData[newOffset[3]] = sortData[3];

	//			GROUP_LDS_BARRIER;

	sortData[0] = ldsSortData[lIdx*4+0];
	sortData[1] = ldsSortData[lIdx*4+1];
	sortData[2] = ldsSortData[lIdx*4+2];
	sortData[3] = ldsSortData[lIdx*4+3];

	//			GROUP_LDS_BARRIER;
	}
	}
	*/
	for (uint ibit = 0; ibit < BITS_PER_PASS; ibit += 2)
	{
		uvec4 b = uvec4((sortData[0] >> (startBit + ibit)) & 0x3,
			(sortData[1] >> (startBit + ibit)) & 0x3,
			(sortData[2] >> (startBit + ibit)) & 0x3,
			(sortData[3] >> (startBit + ibit)) & 0x3);

		uint key4;
		uint sKeyPacked[4] = { 0, 0, 0, 0 };
		{
			sKeyPacked[0] |= 1 << (8 * b.x);
			sKeyPacked[1] |= 1 << (8 * b.y);
			sKeyPacked[2] |= 1 << (8 * b.z);
			sKeyPacked[3] |= 1 << (8 * b.w);

			key4 = sKeyPacked[0] + sKeyPacked[1] + sKeyPacked[2] + sKeyPacked[3];
		}

		uint rankPacked;
		uint sumPacked;
		{
			rankPacked = localPrefixSum(key4, lIdx, sumPacked, WG_SIZE);
		}

		GROUP_LDS_BARRIER;

		uint newOffset[4] = { 0,0,0,0 };
		{
			uint sumScanned = bit8Scan(sumPacked);

			uint scannedKeys[4];
			scannedKeys[0] = 1 << (8 * b.x);
			scannedKeys[1] = 1 << (8 * b.y);
			scannedKeys[2] = 1 << (8 * b.z);
			scannedKeys[3] = 1 << (8 * b.w);
			{	//	4 scans at once
				uint sum4 = 0;
				for (int ie = 0; ie < 4; ie++)
				{
					uint tmp = scannedKeys[ie];
					scannedKeys[ie] = sum4;
					sum4 += tmp;
				}
			}

			{
				uint sumPlusRank = sumScanned + rankPacked;
				{	uint ie = b.x;
				scannedKeys[0] += sumPlusRank;
				newOffset[0] = unpack4Key(scannedKeys[0], ie);
				}
				{	uint ie = b.y;
				scannedKeys[1] += sumPlusRank;
				newOffset[1] = unpack4Key(scannedKeys[1], ie);
				}
				{	uint ie = b.z;
				scannedKeys[2] += sumPlusRank;
				newOffset[2] = unpack4Key(scannedKeys[2], ie);
				}
				{	uint ie = b.w;
				scannedKeys[3] += sumPlusRank;
				newOffset[3] = unpack4Key(scannedKeys[3], ie);
				}
			}
		}
		
		GROUP_LDS_BARRIER;

		uint dstAddr = 4 * lIdx;
		ldsSortData[newOffset[0]] = sortData[0];
		ldsSortData[newOffset[1]] = sortData[1];
		ldsSortData[newOffset[2]] = sortData[2];
		ldsSortData[newOffset[3]] = sortData[3];

		GROUP_LDS_BARRIER;

		sortData[0] = ldsSortData[dstAddr + 0];
		sortData[1] = ldsSortData[dstAddr + 1];
		sortData[2] = ldsSortData[dstAddr + 2];
		sortData[3] = ldsSortData[dstAddr + 3];

		GROUP_LDS_BARRIER;
	}
}

shared uint localHistogramToCarry[NUM_BUCKET];
shared uint localHistogram[NUM_BUCKET * 2];
#define SET_HISTOGRAM(setIdx, key) ldsSortData[(setIdx)*NUM_BUCKET+key]

layout(std430, binding = 4) writeonly buffer DstKeys
{
    uint gDstKey[];
};

layout(std430, binding = 5) writeonly buffer DstValues
{
    uint gDstValue[];
};

void main()
{
	uint lIdx = GET_LOCAL_IDX;
	uint wgIdx = GET_GROUP_IDX;
	uint wgSize = GET_GROUP_SIZE;

	const uint n = m_n;
	const uint nAligned = m_nAligned;
	const uint nWGs = m_nWGs;
	const uint startBit = m_startBit;
	const uint nBlocksPerWG = m_nBlocksPerWG;

	if (lIdx < NUM_BUCKET)
	{
		localHistogramToCarry[lIdx] = rHistogram[lIdx*nWGs + wgIdx];
	}

	GROUP_LDS_BARRIER;

	const uint blockSize = ELEMENTS_PER_WORK_ITEM * WG_SIZE;

	const uint nBlocks = nAligned / blockSize - nBlocksPerWG * wgIdx;

	uint addr = blockSize * nBlocksPerWG * wgIdx + ELEMENTS_PER_WORK_ITEM * lIdx;

	for (int iblock = 0; iblock < min(nBlocksPerWG, nBlocks); iblock++, addr += blockSize)
	{
		uint myHistogram = 0;

		uint sortData[ELEMENTS_PER_WORK_ITEM];
		uint sortVal[ELEMENTS_PER_WORK_ITEM];

		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
#if defined(CHECK_BOUNDARY)
		{
			sortData[i] = (addr + i < n) ? gSrcKey[addr + i] : 0xffffffff;
			sortVal[i] = (addr + i < n) ? gSrcVal[addr + i] : 0xffffffff;
		}
#else
		{
				sortData[i] = gSrcKey[addr + i];
				sortVal[i] = gSrcVal[addr + i];
		}
#endif

		sort4Bits1KeyValue(sortData, sortVal, startBit, lIdx);

		uint keys[ELEMENTS_PER_WORK_ITEM];
		for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
			keys[i] = (sortData[i] >> startBit) & 0xf;

		{	//	create histogram
			uint setIdx = lIdx / 16;
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[lIdx] = 0;
			}
			ldsSortData[lIdx] = 0;
			GROUP_LDS_BARRIER;

			for (int i = 0; i < ELEMENTS_PER_WORK_ITEM; i++)
#if defined(CHECK_BOUNDARY)
				if (addr + i < n)
#endif
					AtomInc(SET_HISTOGRAM(setIdx, keys[i]));

			GROUP_LDS_BARRIER;

			uint hIdx = NUM_BUCKET + lIdx;
			if (lIdx < NUM_BUCKET)
			{
				uint sum = 0;
				for (int i = 0; i < WG_SIZE / 16; i++)
				{
					sum += SET_HISTOGRAM(i, lIdx);
				}
				myHistogram = sum;
				localHistogram[hIdx] = sum;
			}
			GROUP_LDS_BARRIER;

#if defined(USE_2LEVEL_REDUCE)
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[hIdx] = localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;

				uint u0, u1, u2;
				u0 = localHistogram[hIdx - 3];
				u1 = localHistogram[hIdx - 2];
				u2 = localHistogram[hIdx - 1];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;
				u0 = localHistogram[hIdx - 12];
				u1 = localHistogram[hIdx - 8];
				u2 = localHistogram[hIdx - 4];
				AtomAdd(localHistogram[hIdx], u0 + u1 + u2);
				GROUP_MEM_FENCE;
			}
#else
			if (lIdx < NUM_BUCKET)
			{
				localHistogram[hIdx] = localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 1];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 2];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 4];
				GROUP_MEM_FENCE;
				localHistogram[hIdx] += localHistogram[hIdx - 8];
				GROUP_MEM_FENCE;
			}
#endif
			GROUP_LDS_BARRIER;
		}

		{
			for (int ie = 0; ie < ELEMENTS_PER_WORK_ITEM; ie++)
			{
				uint dataIdx = ELEMENTS_PER_WORK_ITEM*lIdx + ie;
				uint binIdx = keys[ie];
				uint groupOffset = localHistogramToCarry[binIdx];
				uint myIdx = dataIdx - localHistogram[NUM_BUCKET + binIdx];
#if defined(CHECK_BOUNDARY)
				if (addr + ie < n)
#endif
				{
					gDstKey[groupOffset + myIdx] = sortData[ie];
					gDstValue[groupOffset + myIdx] = sortVal[ie];
				}
			}
		}

		GROUP_LDS_BARRIER;

		if (lIdx < NUM_BUCKET)
		{
			atomicAdd(localHistogramToCarry[lIdx], myHistogram);
		}
		GROUP_LDS_BARRIER;
	}
	/*
	GROUP_LDS_BARRIER;

	const int blockSize = ELEMENTS_PER_WORK_ITEM*WG_SIZE;
	//	for(int addr=ELEMENTS_PER_WORK_ITEM*gIdx; addr<n; addr+=stride)
	[loop]
	for(int addr = blockSize*nBlocksPerWG*wgIdx+ELEMENTS_PER_WORK_ITEM*lIdx;
	addr<min(blockSize*nBlocksPerWG*(wgIdx+1), n);
	addr+=blockSize )
	{
	uint myHistogram = 0;

	uint sortData[ELEMENTS_PER_WORK_ITEM];
	{
	for(int i=0; i<ELEMENTS_PER_WORK_ITEM; i++)
	sortData[i] = gSrc[ addr+i ];
	}

	sort4Bits1(sortData, startBit, lIdx);

	uint keys[4];
	for(int i=0; i<4; i++)
	keys[i] = (sortData[i]>>startBit) & 0xf;

	{	//	create histogram
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[lIdx] = 0;
	localHistogram[NUM_BUCKET+lIdx] = 0;
	}
	//			GROUP_LDS_BARRIER;

	AtomInc( localHistogram[NUM_BUCKET+keys[0]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[1]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[2]] );
	AtomInc( localHistogram[NUM_BUCKET+keys[3]] );

	//			GROUP_LDS_BARRIER;

	uint hIdx = NUM_BUCKET+lIdx;
	if( lIdx < NUM_BUCKET )
	{
	myHistogram = localHistogram[hIdx];
	}
	//			GROUP_LDS_BARRIER;

	#if defined(USE_2LEVEL_REDUCE)
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[hIdx] = localHistogram[hIdx-1];
	GROUP_MEM_FENCE;

	uint u0, u1, u2;
	u0 = localHistogram[hIdx-3];
	u1 = localHistogram[hIdx-2];
	u2 = localHistogram[hIdx-1];
	AtomAdd( localHistogram[hIdx], u0 + u1 + u2 );
	GROUP_MEM_FENCE;
	u0 = localHistogram[hIdx-12];
	u1 = localHistogram[hIdx-8];
	u2 = localHistogram[hIdx-4];
	AtomAdd( localHistogram[hIdx], u0 + u1 + u2 );
	GROUP_MEM_FENCE;
	}
	#else
	if( lIdx < NUM_BUCKET )
	{
	localHistogram[hIdx] = localHistogram[hIdx-1];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-1];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-2];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-4];
	GROUP_MEM_FENCE;
	localHistogram[hIdx] += localHistogram[hIdx-8];
	GROUP_MEM_FENCE;
	}
	#endif

	//			GROUP_LDS_BARRIER;
	}

	{
	for(int ie=0; ie<ELEMENTS_PER_WORK_ITEM; ie++)
	{
	int dataIdx = 4*lIdx+ie;
	int binIdx = keys[ie];
	int groupOffset = localHistogramToCarry[binIdx];
	int myIdx = dataIdx - localHistogram[NUM_BUCKET+binIdx];
	gDst[ groupOffset + myIdx ] = sortData[ie];
	}
	}

	//		GROUP_LDS_BARRIER;

	if( lIdx < NUM_BUCKET )
	{
	localHistogramToCarry[lIdx] += myHistogram;
	}
	//		GROUP_LDS_BARRIER;

	}
	*/
}