#pragma once

#include "algorithm.slang"
#include "math.slang"
#include "spectrum_system.slang"

namespace framework
{
	struct rgb_sigmoid_polynomial
	{
		__init(float c0, float c1, float c2)
		{
			this.c0 = c0;
			this.c1 = c1;
			this.c2 = c2;
		}

		float solve(float lambda)
		{
			float t = lambda;
			float x = t * (t * this.c0 + this.c1) + this.c2;
			float s = .5f + x / (2 * sqrt(1 + framework::square(x)));

			return s;
		}

		float c0, c1, c2;
	};

	struct to_rgb_coeffs_predicate : framework::find_interval_predicate
	{
		__init(uint offset, float z)
		{
			this.offset = offset;
			this.z = z;
		}

		bool call(int i)
		{
			return g_spectrum.rgb_spectrum_table[this.offset + i] < z;
		}
		
		uint offset;
		float z;
	};

	struct rgb_to_spectrum_table
	{
		static const uint res = 64;
		static const uint offset_0 = 3;
		static const uint offset_1 = offset_0 * framework::rgb_to_spectrum_table::res;
		static const uint offset_2 = offset_1 * framework::rgb_to_spectrum_table::res;
		static const uint offset_3 = offset_2 * framework::rgb_to_spectrum_table::res;

		framework::rgb_sigmoid_polynomial to_rgb_coeffs(float3 rgb)
		{
			// Handle uniform rgb values
			if (rgb[0] < 1.0f && rgb[0] == rgb[1] && rgb[1] == rgb[2])
			{
				return framework::rgb_sigmoid_polynomial(0, 0, (rgb[0] - .5f) / sqrt(rgb[0] * (1 - rgb[0])));
			}

			// Find maximum component and compute remapped component values
			int maxc = (rgb[0] > rgb[1]) ? ((rgb[0] > rgb[2]) ? 0 : 2) : ((rgb[1] > rgb[2]) ? 1 : 2);
			float z = rgb[maxc];
			float x = rgb[(maxc + 1) % 3] * (framework::rgb_to_spectrum_table::res - 1) / z;
			float y = rgb[(maxc + 2) % 3] * (framework::rgb_to_spectrum_table::res - 1) / z;

			// Compute integer indices and offsets for coefficient interpolation
			int xi = min((int)x, framework::rgb_to_spectrum_table::res - 2);
			int yi = min((int)y, framework::rgb_to_spectrum_table::res - 2);
			int zi = framework::find_interval(framework::rgb_to_spectrum_table::res, framework::to_rgb_coeffs_predicate(this.offset, z));
			float dx = x - xi;
			float dy = y - yi;
			float dz = (z - g_spectrum.rgb_spectrum_table[this.offset + zi]) / (g_spectrum.rgb_spectrum_table[this.offset + zi + 1] - g_spectrum.rgb_spectrum_table[this.offset + zi]);

			// Trilinearly interpolate sigmoid polynomial coefficients c
			float c[3];
			for (int i = 0; i < 3; ++i)
			{
				uint offset = i + this.offset + framework::rgb_to_spectrum_table::res;
				#define co(dx, dy, dz) g_spectrum.rgb_spectrum_table[maxc * offset_3 + (zi + dz) * offset_2 + (yi + dy) * offset_1 + (xi + dx) * offset_0 + offset]
				c[i] = lerp(
					lerp(lerp(co(0, 0, 0), co(1, 0, 0), dx),
						lerp(co(0, 1, 0), co(1, 1, 0), dx), dy),
					lerp(lerp(co(0, 0, 1), co(1, 0, 1), dx),
						lerp(co(0, 1, 1), co(1, 1, 1), dx), dy), dz);
			}

			return framework::rgb_sigmoid_polynomial(c[0], c[1], c[2]);
		}

		uint offset;
		uint3 padding;
	};
}