#include "ray_tracing/aov.slang"
#include "gpu_log.slang"
#include "ray_tracing/path_state.slang"
#include "ray_tracing/path_tracer.slang"

[[vk::binding(0, 0)]]
ParameterBlock<framework::path_tracer> g_path_tracer : register(space0);

[[vk::binding(8, 0)]]
RWStructuredBuffer<framework::hit_info> visualize_hit_info_buffer : register(u7, space0);

[[vk::binding(0, 6)]]
ParameterBlock<framework::aov> g_aov : register(space6);

[[vk::binding(16, 0)]]
RWTexture2D<float4> ground_truth_distribution_image : register(u15, space0);

extension framework::path_tracer
{
	void generate_path_hemisphere(const uint2 pixel_index, const uint sample_index, out framework::path_state path_state)
	{
		path_state.id = pixel_index.x | (pixel_index.y << 12) | (sample_index << 24);
		path_state.flags = 0;
		path_state.length = 0;
		path_state.throughput = framework::sampled_spectrum(1.f);
		path_state.r_u = framework::sampled_spectrum(1.f);
		path_state.r_l = framework::sampled_spectrum(1.f);
		path_state.lighting = framework::sampled_spectrum(0.f);
		path_state.eta_scale = 1.0f;
		path_state.set_active();
		path_state.set_free_path(false);
		path_state.set_medium(g_scene.medium_system.get_medium(0));

		path_state.sample_generator = framework::uniform_sample_generator(pixel_index, sample_index * framework::samples_per_pixel + this.parameters.seed);

		float u = framework::sample_next_1d(path_state.sample_generator);
		path_state.lambda = framework::sampled_wavelengths::sample_uniform(u);

#if 0
		float2 jitter = framework::sample_next_2d(path_state.sample_generator);
		const float2 pixel_center = float2(pixel_index) + jitter;
		const float2 sample = pixel_center / float2(DispatchRaysDimensions().xy);
		float3 target = mul(float4(0, 0, 1, 1), g_scene.camera.camera_data.projection_inverse).xyz;
		const float3 view_forward = mul(float4(normalize(target), 0), g_scene.camera.camera_data.view_inverse).xyz;
		framework::frame shading_frame = framework::frame::from_z(view_forward);
		const float3 local_direction = framework::sample_uniform_hemisphere(sample);
		path_state.origin = mul(float4(0, 0, 0, 1), g_scene.camera.camera_data.view_inverse).xyz;
		path_state.direction = shading_frame.from_local(local_direction);
#else
		framework::surface_interaction interaction = framework::load_shading_data(visualize_hit_info_buffer[0], float3(0.0f, 1.0f, 0.0f), 1.f);
		
		float2 jitter = framework::sample_next_2d(path_state.sample_generator);
		const float2 pixel_center = float2(pixel_index) + jitter;
		const float2 sample = pixel_center / float2(DispatchRaysDimensions().xy);
		framework::frame shading_frame = framework::frame::from_z(interaction.n);
		const float3 local_direction = framework::sample_uniform_hemisphere(sample);
		path_state.origin = framework::offset_ray(interaction.position, interaction.n);
		path_state.direction = shading_frame.from_local(local_direction);
#endif
	}

	void write_ground_truth_distribution(const uint2 pixel, float3 rgb)
	{
		ground_truth_distribution_image[pixel] = float4(rgb, 0.f);
	}
}

[shader("raygeneration")]
void main()
{
	framework::seed = g_path_tracer.parameters.seed;
	const uint2 pixel_index = DispatchRaysIndex().xy;
	g_gpu_log.set_current_invocation_index(DispatchRaysIndex());
	g_aov.set_pixel_index(pixel_index.xy);
	for (uint j = 0; j < framework::samples_per_pixel; j++)
	{
		framework::path_state path_state;
#if 0
		g_path_tracer.generate_path(pixel_index, j, path_state);
#else
		g_path_tracer.generate_path_hemisphere(pixel_index, j, path_state);
#endif
		g_path_tracer.li(path_state);
		g_path_tracer.add_sample(path_state, 2.0f);
	}

	float3 rgb = g_path_tracer.get_pixel_rgb();
	g_path_tracer.write_ground_truth_distribution(pixel_index, rgb);
}
