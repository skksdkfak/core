#include "gpu/gpu.hlsl"
#include "numeric.hlsl"
#include "ray_tracing/nis_path_integrator/config.hlsl"

struct push_constants_t
{
	bool inference;
};

[[vk::push_constant]]
ConstantBuffer<push_constants_t> push_constants : register(b1, space0);

[[vk::binding(10, 0)]]
RWStructuredBuffer<uint32_t> batch_size_buffer : register(u9, space0);

[[vk::binding(13, 0)]]
RWStructuredBuffer<uint32_t> inference_batch_size_buffer : register(u12, space0);

[[vk::binding(14, 0)]]
RWStructuredBuffer<::framework::gpu::dispatch_indirect_command> dispatch_indirect_command_buffer : register(u13, space0);

[numthreads(1, 1, 1)]
[shader("compute")]
void main(uint3 dispatch_thread_id : SV_DispatchThreadID)
{
	if (push_constants.inference)
	{
		const uint32_t inference_batch_size = ::framework::numeric::next_multiple(inference_batch_size_buffer[0], 256u);
		inference_batch_size_buffer[0] = inference_batch_size;
		// concatenate_features
		dispatch_indirect_command_buffer[2] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(inference_batch_size, 128u), 1u, 1u);
	}
	else
	{
		const uint32_t batch_size = batch_size_buffer[0];
		const uint32_t training_batch_size = ::framework::numeric::previous_multiple(min(batch_size, ::framework::max_batch_size), 256u);
		batch_size_buffer[0] = training_batch_size;
		// light_field_loss
		dispatch_indirect_command_buffer[3] = ::framework::gpu::dispatch_indirect_command::create(::framework::numeric::div_round_up(training_batch_size * 16u, 128u), 1u, 1u);
	}
}
