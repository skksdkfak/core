#pragma once

namespace framework
{
	static const uint32_t max_batch_size = 1u << 20;
	static const uint32_t max_training_sample_pool_size = 1u << 22;
	static const uint32_t samples_per_pixel = 1u;
	static const uint32_t max_path_length = 4u;
	static const uint32_t sample_buffer_stride = 16u;
	static const uint32_t x_a_row_index = 52u;
	static const uint32_t nis_spatial_feature_count = 3u;
	static const uint32_t nis_spatial_feature_stride = 4u;
	static const uint32_t light_field_one_blob_encode_features_count = 5u;
	static const uint32_t light_field_one_blob_encode_features_stride = 8u;
	static const float radiance_target_scale = 1024.0f;
}