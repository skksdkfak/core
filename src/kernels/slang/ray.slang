#pragma once

#include "numbers.slang"

namespace framework
{
	struct ray
	{
		float3 origin;
		float3 direction;
		float t_max;
		float time;

		static framework::ray create(float3 origin, float3 direction, float t_max, float time)
		{
			framework::ray result;
			result.origin = origin;
			result.direction = direction;
			result.t_max = t_max;
			result.time = time;
			return result;
		}
		
		static framework::ray create(float3 origin, float3 direction)
		{
			framework::ray result;
			result.origin = origin;
			result.direction = direction;
			result.t_max = framework::numbers::flt_max;
			result.time = 0.0f;
			return result;
		}

		float3 p(float t)
		{
			return this.origin + this.direction * t;
		}

		RayDesc get_ray_desc()
		{
			RayDesc ray_desc;
			ray_desc.Origin = this.origin;
			ray_desc.Direction = this.direction;
			ray_desc.TMin = 0.0f;
			ray_desc.TMax = this.t_max;
			return ray_desc;
		}
	};

	// Clever offset_ray function from Ray Tracing Gems chapter 6
	// Offsets the ray origin from current position p, along normal n (which must be geometric normal)
	// so that no self-intersection can occur.
	float3 offset_ray(const float3 p, const float3 n)
	{
		static const float origin = 1.0f / 32.0f;
		static const float float_scale = 1.0f / 65536.0f;
		static const float int_scale = 256.0f;

		int3 of_i = int3(int_scale * n.x, int_scale * n.y, int_scale * n.z);

		float3 p_i = float3(
			asfloat(asint(p.x) + ((p.x < 0) ? -of_i.x : of_i.x)),
			asfloat(asint(p.y) + ((p.y < 0) ? -of_i.y : of_i.y)),
			asfloat(asint(p.z) + ((p.z < 0) ? -of_i.z : of_i.z)));

		return float3(abs(p.x) < origin ? p.x + float_scale * n.x : p_i.x,
			abs(p.y) < origin ? p.y + float_scale * n.y : p_i.y,
			abs(p.z) < origin ? p.z + float_scale * n.z : p_i.z);
	}

	float3 offset_ray(const float3 p, const float3 n, const float3 w)
	{
		return framework::offset_ray(p, dot(w, n) < 0 ? -n : n);
	}

	float3 offset_ray(const float3 p, const float3 n, const bool flip_normal)
	{
		return framework::offset_ray(p, flip_normal ? -n : n);
	}

	framework::ray spawn_ray_to(float3 p_from, float3 n_from, float time, float3 p_to, float3 n_to)
	{
		float3 pf = framework::offset_ray(p_from, n_from, p_to - p_from);
		float3 pt = framework::offset_ray(p_to, n_to, pf - p_to);
		return framework::ray::create(pf, normalize(pt - pf), length(pt - pf), time);
	}

	framework::ray spawn_ray_to(float3 p_from, float3 n_from, float time, float3 p_to)
	{
		float3 pf = framework::offset_ray(p_from, n_from, p_to - p_from);
		return framework::ray::create(pf, normalize(p_to - pf), length(p_to - pf), time);
	}
}