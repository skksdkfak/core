inline ::framework::profiler::scoped_timestamp::scoped_timestamp(::framework::profiler & profiler, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags begin_timestamp_pipeline_stage, ::framework::gpu::pipeline_stage_flags end_timestamp_pipeline_stage, ::std::string const & label) :
	profiler(profiler),	command_buffer(command_buffer),	end_timestamp_pipeline_stage(end_timestamp_pipeline_stage),	timestamp_index(profiler.begin_timestamp(command_buffer, begin_timestamp_pipeline_stage, label))
{
	this->profiler.increment_current_depth();
}

inline ::framework::profiler::scoped_timestamp::~scoped_timestamp()
{
	this->profiler.decrement_current_depth();
	this->profiler.end_timestamp(this->command_buffer, this->end_timestamp_pipeline_stage, this->timestamp_index);
}

inline ::std::uint32_t(::framework::profiler::scoped_timestamp::get_statistics_index)() const noexcept
{
	return this->timestamp_index >> 1;
}

inline ::framework::profiler::manual_timestamp::manual_timestamp(::framework::profiler & profiler, ::framework::gpu::command_buffer * command_buffer, ::framework::gpu::pipeline_stage_flags begin_timestamp_pipeline_stage, ::framework::gpu::pipeline_stage_flags end_timestamp_pipeline_stage, ::std::string const && label) :
	profiler(profiler),	command_buffer(command_buffer), begin_timestamp_pipeline_stage(begin_timestamp_pipeline_stage),	end_timestamp_pipeline_stage(end_timestamp_pipeline_stage), label(label)
{
}

inline void ::framework::profiler::manual_timestamp::begin_timestamp()
{
	this->profiler.increment_current_depth();
	this->timestamp_index = this->profiler.begin_timestamp(this->command_buffer, this->begin_timestamp_pipeline_stage, this->label);
}

inline void ::framework::profiler::manual_timestamp::end_timestamp()
{
	this->profiler.decrement_current_depth();
	this->profiler.end_timestamp(this->command_buffer, this->end_timestamp_pipeline_stage, this->timestamp_index);
}

inline ::std::uint32_t(::framework::profiler::manual_timestamp::get_statistics_index)() const noexcept
{
	return this->timestamp_index >> 1;
}

inline void ::framework::profiler::increment_current_depth() noexcept
{
	this->current_depth++;
}

inline void ::framework::profiler::decrement_current_depth() noexcept
{
	this->current_depth--;
}

inline ::std::vector<struct ::framework::profiler::statistics> const & ::framework::profiler::get_statistics(::std::uint32_t frame_in_flight_index) const noexcept
{
	return this->frame_data[frame_in_flight_index].statistics;
}

inline bool ::framework::profiler::get_query_pool_host_commands() const noexcept
{
	return this->query_pool_host_commands;
}