@echo off

set msbuild_exe=


for /f "delims=" %%i in ('"%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere" -prerelease -latest -products * -requires Microsoft.Component.MSBuild -property installationPath') do (
	if exist "%%i\MSBuild\Current\Bin\MSBuild.exe" (
		set msbuild_exe="%%i\MSBuild\Current\Bin\MSBuild.exe"
		goto Succeeded
	)
)

:Succeeded
exit /B 0