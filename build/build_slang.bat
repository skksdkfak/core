@echo off

pushd "%~dp0.."

call "%~dp0get_msbuild_path.bat"

pushd "%~dp0..\external\slang"
call "premake.bat" vs2019 --deps=true --arch=x64
popd

%msbuild_exe% /nologo /verbosity:quiet external\slang\slang.sln -target:slang /p:Configuration=Release /p:Platform=x64

popd
exit /B 0